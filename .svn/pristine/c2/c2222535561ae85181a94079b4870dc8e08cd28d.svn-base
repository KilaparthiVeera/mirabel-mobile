package com.mirabeltechnologies.magazinemanager.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.models.LogInModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.receiver.DeviceBootReceiver;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.FirebaseJobDispatcherClass;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;
import java.util.List;

import static com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog.alertDialog;

/**
 * @author venkat
 */
public class LoginActivity extends AppCompatActivity {
    public static final String TAG = LoginActivity.class.getSimpleName();

    private MMSharedPreferences sharedPreferences;
    private Context applicationContext, activityContext;

    private MMEditText mEmailView, mPasswordView;
    private MMButton logInButton, continueButton, cancelButton;
    private MMTextView autoLogInText;
    private String emailId, password;
    private boolean isAutoLogIn = false;
    public static boolean isAlertDialogShown = false;
    private LogInModel logInModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        try {
            applicationContext = getApplicationContext();
            activityContext = LoginActivity.this;

            sharedPreferences = new MMSharedPreferences(applicationContext);

            logInModel = new LogInModel(activityContext, this, Constants.RequestFrom.LOGIN_PAGE);

            mEmailView = (MMEditText) findViewById(R.id.email);
            mEmailView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if (actionId == R.id.login || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                        attemptLogin();
                        return true;
                    }

                    return false;
                }
            });

            mPasswordView = (MMEditText) findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                    if (actionId == R.id.login || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                        attemptLogin();
                        return true;
                    }

                    return false;
                }
            });

            logInButton = (MMButton) findViewById(R.id.login_button);
            logInButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    attemptLogin();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            final String savedEmailId = sharedPreferences.getString(Constants.SP_EMAIL_ID);
            final String savedPassword = sharedPreferences.getString(Constants.SP_PASSWORD);

            if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                setContentView(R.layout.activity_autologin);

                autoLogInText = (MMTextView) findViewById(R.id.autoLogInText);
                autoLogInText.setText("Do you want to continue with login account: " + savedEmailId);

                continueButton = (MMButton) findViewById(R.id.continue_button);
                continueButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        isAutoLogIn = true;
                        authenticateUser(savedEmailId, savedPassword);

                        Log.e("veera",savedEmailId+"  "+savedPassword);
                    }
                });

                cancelButton = (MMButton) findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        clearCacheAndNavigateToLogIn();
                    }
                });
            } else {
                isAutoLogIn = false;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && alertDialog != null) {
            alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        emailId = mEmailView.getText().toString();
        password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(emailId)) {
            mEmailView.setError(getString(R.string.email_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utility.isValidEmail(emailId)) { // Checking for a valid email address.
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.password_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first form field with an error.
            focusView.requestFocus();
        } else {
            authenticateUser(emailId, password);
        }
    }

    public void authenticateUser(String email, String pwd) {
        MMProgressDialog.showProgressDialog(activityContext);
        logInModel.validateUserLogin(email, pwd);
    }

    public void logInError(int responseCode) {
        MMProgressDialog.hideProgressDialog();
        if (responseCode == Constants.MM_INVALID_LOGIN) {
            MMAlertDialog.showAlertDialog(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login));
        } else if (responseCode == Constants.MM_LOGIN_SUCCESS_BUT_NO_CLIENTS) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients));
        } else if (responseCode == Constants.MM_PASSWORD_STRONG) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_not_strong));
        } else if (responseCode == Constants.MM_PASSWORD_EXPIRED) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_expired));
        } else if (responseCode == Constants.MM_PENDING_VERIFICATION) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_pending_verification));
        } else if (responseCode == Constants.MM_LOGIN_ERROR) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.login_error));
           // doLogout(getResources().getString(R.string.login_error));
        } else if (responseCode == Constants.MM_INVALID_REQUEST) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.invalid_request));
        } else {
            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.common_login_response));
        }
    }
    public void updateLoggedInUserName(String firstName, String lastName) {
        String formattedRepName = "", formattedUserName = "";

        if (firstName.length() > 0 && lastName.length() > 0) {
            formattedRepName = lastName + ", " + firstName;
            formattedUserName = firstName + " " + lastName;
        } else if (firstName.length() > 0) {
            formattedRepName = firstName;
            formattedUserName = firstName;
        } else if (lastName.length() > 0) {
            formattedRepName = lastName;
            formattedUserName = lastName;
        }

        sharedPreferences.putString(Constants.SP_LOGGED_IN_REP_NAME, formattedRepName);
        sharedPreferences.putString(Constants.SP_LOGGED_IN_USER_NAME, formattedUserName);
    }

    public void updateClientsList(List<ClientInfo> clientList) {
        try {
            if (clientList.size() == 0) {
                MMProgressDialog.hideProgressDialog();
                MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients_with_mm_access));
            } else {
                ClientInfo.clientList = clientList;

                if (clientList.size() == 1 && clientList.get(0).getClientId() == 0) {
                    MMProgressDialog.hideProgressDialog();
                    sharedPreferences.clear();
                    MMAlertDialog.showAlertDialog(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login));
                } else {
                    //Saving username & password in shared preferences if it's first time log in
                    if (!isAutoLogIn) {
                        sharedPreferences.putString(Constants.SP_EMAIL_ID, emailId);
                        sharedPreferences.putString(Constants.SP_PASSWORD, password);
                    }

                    //Load Dashboard directly with single client
                    if (clientList.size() == 1) {
                        getClientEncryptedId(clientList.get(0));
                    } else {
                        int savedSelectedClientId = sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID);

                        if (savedSelectedClientId <= 0) {
                            MMProgressDialog.hideProgressDialog();

                            Intent clientsListIntent = new Intent(LoginActivity.this, ClientsListActivity.class);
                            clientsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            clientsListIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(clientsListIntent);
                            finish();
                            //overridePendingTransition(R.anim.top_in, R.anim.bottom_out);
                        } else {
                            int savedSelectedClientIndex = getIndexOfClientIdFromArrayList(clientList, savedSelectedClientId);
                            getClientEncryptedId(clientList.get(savedSelectedClientIndex));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getIndexOfClientIdFromArrayList(List<ClientInfo> list, int clientId) {
        int index = 0;

        for (int i = 0; i < list.size(); i++) {
            ClientInfo clientInfo = list.get(i);
            if (clientId == clientInfo.getClientId()) {
                index = i;
                break;
            }
        }

        return index;
    }

    public void getClientEncryptedId(ClientInfo selectedClientDetails) {
        try {
            ClientInfo selectedClient = selectedClientDetails;

            ClientInfo.selectedClientId = selectedClient.getClientId();

            EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
            String encryptedId = des.getEncryptedStringForSelectedClient(selectedClient);

            sharedPreferences.putString(Constants.SP_ENCRYPTED_CLIENT_KEY, encryptedId);
            sharedPreferences.putString(Constants.SP_SELECTED_SUB_DOMAIN, selectedClient.getSubDomain());
            sharedPreferences.putInt(Constants.SP_SELECTED_CLIENT_ID, selectedClient.getClientId());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_NAME, selectedClient.getClientName());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_URL, selectedClient.getClientURL());
            sharedPreferences.putInt(Constants.SP_LOGGED_IN_REP_ID, selectedClient.getEmployeeId());
            sharedPreferences.putString(Constants.SP_SELECTED_SITE_TYPE, selectedClient.getSiteType());
            sharedPreferences.putBoolean(Constants.SP_IS_DS_USER, selectedClient.isDSUser());
            MMProgressDialog.hideProgressDialog();

            loadMainActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void loadMainActivity() {
        // In order to load website config initially, we are resetting value.
        sharedPreferences.putBoolean(Constants.SP_IS_WEBSITE_CONFIG_LOADED, false);

        Intent mainActivityIntent = new Intent(LoginActivity.this, DashboardActivity.class);
        //mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        //mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // if you keep these flags white screen is coming on Intent navigation
        startActivity(mainActivityIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }


    public void clearCacheAndNavigateToLogIn() {
        try {

            MMSettingsPreferences mmSettingsPreferences = new MMSettingsPreferences(getApplicationContext());
            MMLocalDataBase db = new MMLocalDataBase(LoginActivity.this);
            //removeNotificationsFromNotificationTrayOnUserLogout
            BuildNotification buildNotification = new BuildNotification(LoginActivity.this);
            buildNotification.removeNotificationsFromNotificationTrayOnUserLogout();
            //check and clear running services
            //clear scheduled notifications(alarms) .This can be done , if we have the ids of the scheduled notifications.
            ArrayList<Integer> pendingIntentIdList = db.getAllScheduledNotifications();
            if (pendingIntentIdList != null) {
                PendingIntent pendingIntent;
                AlarmManager manager;
                Intent alarmintent;
                for (Integer notification_id : pendingIntentIdList) {
                    alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                    alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                    pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                    manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    //cancel the alarm manager of the pending intent
                    manager.cancel(pendingIntent);

                }
            }
            //disable boot receiver after user logout.
            disableDeviceBootReceiver();
            //clear all user store data.
            sharedPreferences.clear();
            //  mmSettingsPreferences.clear();
            //truncate  database tables
            db.truncateTableNotificationIds();
            //clear jobschedulers
            FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(LoginActivity.this);
            firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);
            ClientInfo.clientList = new ArrayList<ClientInfo>();
            //Reloading LogIn Page
            finish();
            startActivity(getIntent());
            overridePendingTransition(R.anim.bottom_in, R.anim.top_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }





}

