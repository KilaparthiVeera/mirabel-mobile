<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context="com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactOrdersActivity">

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@color/color_primary"
        android:minHeight="?attr/actionBarSize"
        android:orientation="horizontal">

        <ImageView
            android:layout_width="@dimen/header_back_icon_width"
            android:layout_height="wrap_content"
            android:layout_alignParentLeft="true"
            android:layout_centerVertical="true"
            android:layout_marginLeft="@dimen/header_back_icon_margin_left"
            android:onClick="closeActivity"
            android:src="@drawable/icon_back_arrow" />

        <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_centerInParent="true"
            android:gravity="center"
            android:text="Orders"
            android:textColor="@color/white"
            android:textSize="@dimen/activity_header_title_font_size"
            android:textStyle="bold" />

    </RelativeLayout>

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="@dimen/list_view_header_height"
        android:background="@color/list_view_header_background"
        android:orientation="horizontal">

        <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center_vertical"
            android:layout_marginLeft="@dimen/margin_10"
            android:text="Year - Net (Orders)"
            android:textColor="@color/list_view_header_title_color"
            android:textSize="@dimen/list_view_header_title_font_size"
            android:textStyle="bold" />

    </LinearLayout>

    <ListView
        android:id="@+id/contact_orders_list_view"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:background="@color/white"
        android:divider="@color/line_separator_color"
        android:dividerHeight="@dimen/list_view_divider_height"
        android:listSelector="@android:color/transparent" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:background="@color/list_view_footer_background"
        android:orientation="vertical"
        android:padding="@dimen/padding_7">

        <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_gravity="center_vertical"
            android:text="Total"
            android:textColor="@color/white"
            android:textSize="@dimen/activity_header_title_font_size"
            android:textStyle="bold" />

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="@dimen/margin_3"
            android:orientation="horizontal"
            android:weightSum="1">

            <LinearLayout
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="0.5"
                android:orientation="vertical">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:layout_width="@dimen/contact_orders_summary_field_width"
                        android:layout_height="wrap_content"
                        android:gravity="left"
                        android:text="Orders : "
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size" />

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:id="@+id/co_total_orders_count"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:gravity="left"
                        android:text="0"
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size"
                        android:textStyle="bold" />

                </LinearLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:layout_width="@dimen/contact_orders_summary_field_width"
                        android:layout_height="wrap_content"
                        android:gravity="left"
                        android:text="Barter  : "
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size" />

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:id="@+id/co_total_barter_value"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:gravity="left"
                        android:text="$0.00"
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size"
                        android:textStyle="bold" />

                </LinearLayout>

            </LinearLayout>

            <LinearLayout
                android:layout_width="0dp"
                android:layout_height="wrap_content"
                android:layout_weight="0.5"
                android:orientation="vertical">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:layout_width="@dimen/contact_orders_summary_field_width"
                        android:layout_height="wrap_content"
                        android:gravity="right"
                        android:text="Gross : "
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size" />

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:id="@+id/co_total_gross_value"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:gravity="right"
                        android:text="$0.00"
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size"
                        android:textStyle="bold" />

                </LinearLayout>

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="horizontal">

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:layout_width="@dimen/contact_orders_summary_field_width"
                        android:layout_height="wrap_content"
                        android:gravity="right"
                        android:text="Net : "
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size" />

                    <com.mirabeltechnologies.magazinemanager.customviews.MMTextView
                        android:id="@+id/co_total_net_value"
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:gravity="right"
                        android:text="$0.00"
                        android:textColor="@color/white"
                        android:textSize="@dimen/contact_orders_summary_font_size"
                        android:textStyle="bold" />

                </LinearLayout>

            </LinearLayout>

        </LinearLayout>

    </LinearLayout>

</LinearLayout>
