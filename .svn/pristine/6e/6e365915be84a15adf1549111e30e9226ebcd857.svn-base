package com.mirabeltechnologies.magazinemanager.activities;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.asynctasks.UploadFileAsyncTask;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.beans.NewTicket;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.NewTicketModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.receiver.DeviceBootReceiver;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.FirebaseJobDispatcherClass;
import com.mirabeltechnologies.magazinemanager.util.RealPathUtil;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog.alertDialog;

public class NewTicketActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, AlertDialogSelectionListener {
    public static final String TAG = NewTicketActivity.class.getSimpleName();

    private MMEditText create_ticket_name, create_ticket_email, create_ticket_cc_email, create_ticket_subject, create_ticket_description;
    private Spinner create_ticket_category_spinner;
    private MMButton create_ticket_attach_file, cancel_button, submit_button;
    private MMTextView create_ticket_selected_file_name;
    private LinearLayout create_ticket_selected_file_layout;

    private Utility utility;
    private NewTicketModel newTicketModel;
    private NewTicket newTicketBean;
    private Map<String, String> categoriesHashMap;
    private ArrayList<String> categoriesKeyValues;
    private SpinnerAdapter categoriesSpinnerAdapter;
    private String deviceName = "", selectedCategory, selectedFilePath = "", selectedFileName = "";
    private boolean isAttachmentSelected = false;
    public static boolean isAlertDialogShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_ticket);

        try {
            activityContext = NewTicketActivity.this;

            utility = Utility.getInstance();

            newTicketModel = new NewTicketModel(activityContext, this);

            newTicketBean = new NewTicket();
            newTicketBean.setCompanyName(selectedClientName);
            newTicketBean.setUrl(selectedClientURL);

            // Preparing Data for Spinners
            categoriesHashMap = new LinkedHashMap<>();
            categoriesHashMap.put("None", "0");
            categoriesHashMap.put("Error: Calendar", "10114");
            /*categoriesHashMap.put("Error: Contacts", "10006");
            categoriesHashMap.put("Error: CRM", "10007");
            categoriesHashMap.put("Error: Notes / Activities", "10008");
            categoriesHashMap.put("Error: Orders", "10009");*/
            categoriesHashMap.put("Error: Other", "10150");
            categoriesHashMap.put("General Question", "10157");
            categoriesHashMap.put("Performance", "10158");
            categoriesHashMap.put("Training/Consulting", "10166");
            categoriesHashMap.put("Wish List Request", "10167");

            categoriesKeyValues = new ArrayList<>(categoriesHashMap.keySet());

            // Initializing UI References
            create_ticket_name = (MMEditText) findViewById(R.id.create_ticket_name);
            create_ticket_email = (MMEditText) findViewById(R.id.create_ticket_email);
            create_ticket_cc_email = (MMEditText) findViewById(R.id.create_ticket_cc_email);
            create_ticket_category_spinner = (Spinner) findViewById(R.id.create_ticket_category_spinner);
            create_ticket_subject = (MMEditText) findViewById(R.id.create_ticket_subject);
            create_ticket_description = (MMEditText) findViewById(R.id.create_ticket_description);
            create_ticket_selected_file_name = (MMTextView) findViewById(R.id.create_ticket_selected_file_name);
            create_ticket_selected_file_layout = (LinearLayout) findViewById(R.id.create_ticket_selected_file_layout);

            create_ticket_attach_file = (MMButton) findViewById(R.id.create_ticket_attach_file);
            create_ticket_attach_file.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    chooseFile();
                }
            });

            cancel_button = (MMButton) findViewById(R.id.create_ticket_cancel_button);
            cancel_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeActivity(v);
                }
            });

            submit_button = (MMButton) findViewById(R.id.create_ticket_submit_button);
            submit_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createNewTicketByValidateFields();
                }
            });

            categoriesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, categoriesKeyValues);
            categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            create_ticket_category_spinner.setAdapter(categoriesSpinnerAdapter);
            create_ticket_category_spinner.setSelection(0, false);
            create_ticket_category_spinner.setOnItemSelectedListener(this);

            // Pre Populating Logged In User Details
            create_ticket_name.setText(loggedInUserName);
            create_ticket_email.setText(loggedInUserEmail);

            // Setting default values
            selectedCategory = categoriesKeyValues.get(0);
            newTicketBean.setCategoryKey(selectedCategory);
            newTicketBean.setCategoryValue(categoriesHashMap.get(selectedCategory));

            deviceName = Build.MANUFACTURER + " " + Build.MODEL + " " + Build.VERSION.RELEASE + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
            newTicketBean.setBrowser(deviceName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    /* AdapterView.OnItemSelectedListener Methods Starts Here */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedCategory = categoriesKeyValues.get(position);

        newTicketBean.setCategoryKey(selectedCategory);
        newTicketBean.setCategoryValue(categoriesHashMap.get(selectedCategory));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);
        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    /* AdapterView.OnItemSelectedListener Methods Ends Here */

    public void showValidationAlert(String message) {
        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), message);
    }

    public void createNewTicketByValidateFields() {
        try {
            newTicketBean.setSubject(create_ticket_subject.getText().toString().trim());
            newTicketBean.setDescription(create_ticket_description.getText().toString().trim());
            newTicketBean.setCcEmail(create_ticket_cc_email.getText().toString().trim());
            newTicketBean.setName(create_ticket_name.getText().toString().trim());
            newTicketBean.setEmail(create_ticket_email.getText().toString().trim());

            boolean canWeSendRequest = false;
            String errorMessage = null;
            View focusView = null;

            if (newTicketBean.getCategoryKey().equals("None")) {
                errorMessage = "Please choose category.";
                focusView = create_ticket_category_spinner;
                canWeSendRequest = false;
                showValidationAlert(errorMessage);
                return;
            } else if (newTicketBean.getSubject().isEmpty()) {
                errorMessage = "You must specify a summary of the issue.";
                focusView = create_ticket_subject;
                canWeSendRequest = false;
            } else if (newTicketBean.getDescription().isEmpty()) {
                errorMessage = "Description is required.";
                focusView = create_ticket_description;
                canWeSendRequest = false;
            } /*else if (newTicketBean.getName().isEmpty()) {
                errorMessage = "Name is required.";
                focusView = create_ticket_name;
                canWeSendRequest = false;
            } else if (newTicketBean.getEmail().isEmpty()) {
                errorMessage = "Email is required.";
                focusView = create_ticket_email;
                canWeSendRequest = false;
            } */ else if (newTicketBean.getEmail().length() > 0 && !Utility.isValidEmail(newTicketBean.getEmail())) {
                errorMessage = "Please enter valid email.";
                focusView = create_ticket_email;
                canWeSendRequest = false;
            } else {
                canWeSendRequest = true;
            }

            if (!canWeSendRequest) {
                if (focusView != null) {
                    ((MMEditText) focusView).setError(errorMessage);
                    focusView.requestFocus();
                }
            } else {
                MMProgressDialog.showProgressDialog(activityContext);
                newTicketModel.createNewTicket(newTicketBean);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCreateNewTicketStatus(final JSONObject response) {
        MMProgressDialog.hideProgressDialog();

        try {
            String newlyCreatedTicketId = response.getString("id");
            String newlyCreatedTicketKey = response.getString("key");
            //String newlyCreatedTicketUrl = response.getString("self");

            if (isAttachmentSelected) {
                UploadFileAsyncTask uploadFileAsyncTask = new UploadFileAsyncTask(activityContext, NewTicketActivity.this, newlyCreatedTicketId, newlyCreatedTicketKey, selectedFileName, selectedFilePath);
                uploadFileAsyncTask.execute();
            }

            MMAlertDialog alertDialog = new MMAlertDialog();
            alertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            alertDialog.showAlertDialogWithCallback(this, "", getResources().getString(R.string.ticket_feedback), false);
            alertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    // We are closing this activity.
                    onBackPressed();
                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewTicketFailed() {
        if (MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.hideProgressDialog();

        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), getResources().getString(R.string.ticket_creation_failed));
    }

    public void cancelFileSelection(View v) {
        isAttachmentSelected = false;
        create_ticket_selected_file_layout.setVisibility(View.GONE);
        create_ticket_selected_file_name.setText("");
    }

    private Intent getFileChooserIntent() {
        String[] mimeTypes = {"image/*"};

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            //intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");

            intent.setType("image/*");

            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";

            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }

            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }

        return intent;
    }

    public void chooseFile() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_READ_EXTERNAL_STORAGE, Constants.PERMISSIONS_READ_EXTERNAL_STORAGE, Constants.READ_EXTERNAL_STORAGE_REQUEST_CODE)) {
            Intent intent = getFileChooserIntent();

            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(intent, Constants.CHOOSE_DOCUMENT_TO_UPLOAD_REQUEST_CODE);
            } else {
                displayToast(getResources().getString(R.string.no_support_for_read_external_storage));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.READ_EXTERNAL_STORAGE_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    chooseFile();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_READ_EXTERNAL_STORAGE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_READ_EXTERNAL_STORAGE);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.need_read_external_storage_permission_title), getResources().getString(R.string.need_read_external_storage_permission), true);
                    } else {
                        displayToast(getResources().getString(R.string.read_external_storage_permission_not_given));
                    }
                }
                break;
        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_READ_EXTERNAL_STORAGE) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_READ_EXTERNAL_STORAGE, Constants.READ_EXTERNAL_STORAGE_REQUEST_CODE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            try {
                Uri selectedDocumentUri = data.getData();

                String fileType = utility.getMimeType(activityContext, selectedDocumentUri);

                if (fileType.equalsIgnoreCase("JPG") || fileType.equalsIgnoreCase("JPEG") || fileType.equalsIgnoreCase("PNG")) {
                    boolean isGooglePhotosUri = false;
                    String picturePath = null;

                    if (selectedDocumentUri.toString().startsWith("content://com.google.android.apps.photos.content")) {
                        try {
                            InputStream inputStream = getContentResolver().openInputStream(selectedDocumentUri);
                            if (inputStream != null) {
                                Bitmap pictureBitmap = BitmapFactory.decodeStream(inputStream);
                                picturePath = RealPathUtil.getRealPath(activityContext, utility.getImageUri(activityContext, pictureBitmap));
                                isGooglePhotosUri = true;
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    } else {
                        picturePath = RealPathUtil.getRealPath(activityContext, selectedDocumentUri);
                    }

                    updateSelectedFilePath(picturePath, fileType, isGooglePhotosUri);

                } else {
                    displayLongToast("Please select image with JPG/JPEG OR PNG formats only.");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void updateSelectedFilePath(String filePath, String fileType, boolean isExternalDoc) {
        try {
            this.selectedFilePath = filePath;

            if (selectedFilePath != null && selectedFilePath != "") {
                selectedFileName = selectedFilePath.substring(selectedFilePath.lastIndexOf("/") + 1);
            } else {
                isAttachmentSelected = false;
                displayToast("An error occurred while selecting image to upload.");
            }

            isAttachmentSelected = true;
            create_ticket_selected_file_name.setText(selectedFileName);
            create_ticket_selected_file_layout.setVisibility(View.VISIBLE);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateFileUploadStatus(final String response) {
        try {
            if (response.equals(Constants.STATUS_FAILURE)) {
                Utility.displayToast(activityContext, "Attachment uploading failed for Jira Ticket..!", Toast.LENGTH_LONG);
            } else {
                JSONArray responseArray = new JSONArray(response);

                if (responseArray != null && responseArray.length() > 0) {
                    Utility.logResult(TAG, "Attachment uploaded successfully for Jira Ticket..!");
                } else {
                    Utility.displayToast(activityContext, "Attachment uploading failed for Jira Ticket..!", Toast.LENGTH_LONG);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doLogout(String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NewTicketActivity.this, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle("Session  Error");
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    MMLocalDataBase db = new MMLocalDataBase(NewTicketActivity.this);
                    //removeNotificationsFromNotificationTrayOnUserLogout
                    BuildNotification buildNotification = new BuildNotification(NewTicketActivity.this);
                    buildNotification.removeNotificationsFromNotificationTrayOnUserLogout();
                    //check and clear running services

                    //clear scheduled notifications(alarms) .This can be done , if we have the ids of the scheduled notifications.
                    ArrayList<Integer> pendingIntentIdList = db.getAllScheduledNotifications();
                    if (pendingIntentIdList != null) {
                        PendingIntent pendingIntent;
                        AlarmManager manager;
                        Intent alarmintent;
                        for (Integer notification_id : pendingIntentIdList) {
                            alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                            alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                            manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            //cancel the alarm manager of the pending intent
                            manager.cancel(pendingIntent);

                        }
                    }

                    //disable boot receiver after user logout.
                    disableDeviceBootReceiver();
                    //clear all user store data.
                    sharedPreferences.clear();
                    // mmSettingsPreferences.clear();
                    //truncate  database tables
                    db.truncateTableNotificationIds();

                    //clear jobschedulers
                    FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(NewTicketActivity.this);
                    firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);

                    ClientInfo.clientList = new ArrayList<ClientInfo>();
                    Intent logInIntent = new Intent(activityContext, LoginActivity.class);
                    logInIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(logInIntent);
                    overridePendingTransition(R.anim.left_in, R.anim.right_out);

                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(NewTicketActivity.this.getResources().getDimension(R.dimen.alert_dialog_text_font_size));
            isAlertDialogShown = true;
        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }
    public void disableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
