package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.NewActivity;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.receiver.DeviceBootReceiver;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.FirebaseJobDispatcherClass;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog.alertDialog;

public class BaseActivity extends AppCompatActivity {


    protected Context applicationContext, activityContext;
    protected MMSharedPreferences sharedPreferences;
    protected String encryptedClientKey, selectedSubDomain, loggedInRepName, loggedInUserName, siteType, selectedClientName, selectedClientURL, loggedInUserEmail;
    protected boolean isDSUser = false;
    protected int selectedClientId, loggedInRepId;
    protected GlobalContent globalContent;
    protected NetworkConnectionDetector networkConnectionDetector;
    protected LayoutInflater layoutInflater;
    protected MMTextView no_records_found_text_view;
    protected Pattern emailIdPattern, newLineAndBreakPattern, htmlTagsPattern;
    public static boolean isAlertDialogShown = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            applicationContext = getApplicationContext();
            activityContext = BaseActivity.this;

            sharedPreferences = new MMSharedPreferences(applicationContext);
            encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);
            selectedSubDomain = sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
            siteType = sharedPreferences.getString(Constants.SP_SELECTED_SITE_TYPE);
            isDSUser = sharedPreferences.getBoolean(Constants.SP_IS_DS_USER);
            selectedClientId = sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID);
            selectedClientName = sharedPreferences.getString(Constants.SP_SELECTED_CLIENT_NAME);
            selectedClientURL = sharedPreferences.getString(Constants.SP_SELECTED_CLIENT_URL);
            loggedInRepId = sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID);
            loggedInRepName = sharedPreferences.getString(Constants.SP_LOGGED_IN_REP_NAME);
            loggedInUserName = sharedPreferences.getString(Constants.SP_LOGGED_IN_USER_NAME);
            loggedInUserEmail = sharedPreferences.getString(Constants.SP_EMAIL_ID);

            globalContent = GlobalContent.getInstance();
            networkConnectionDetector = new NetworkConnectionDetector(activityContext);

            layoutInflater = (LayoutInflater) activityContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            no_records_found_text_view = (MMTextView) layoutInflater.inflate(R.layout.no_records_found_textview, null);

            emailIdPattern = Pattern.compile("<[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})>");
            newLineAndBreakPattern = Pattern.compile("\\n{1,}|<br \\/>");
            htmlTagsPattern = Pattern.compile("\\<(.*?)\\>");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
            displayLongToast(getResources().getString(R.string.error_no_internet));
        }
    }

    public void displayLongToast(String message) {
        Utility.displayToast(activityContext, message, Toast.LENGTH_LONG);
    }

    public void displayToast(String message) {
        Utility.displayToast(this, message, Toast.LENGTH_SHORT);
    }

    public void onVolleyErrorResponse(VolleyError error) {
        if (MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.hideProgressDialog();

        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), getResources().getString(R.string.error_failure));
    }

    public void showMMErrorResponse(String message) {
        if (MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.hideProgressDialog();

        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), message);
    }

    public static String getDTTicks() {
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        return des.getEncryptedValueWithOutReplacingCharacters(String.valueOf(System.currentTimeMillis()));
    }

    public void openEmailComposer(Activity activity, Constants.RequestFrom requestFrom, String customerId, String toEmailId, boolean isPrivate) {
        if (Utility.isValidEmail(toEmailId)) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("text/plain");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{toEmailId});

            if (emailIntent.resolveActivity(getPackageManager()) != null) {
                startActivity(Intent.createChooser(emailIntent, "Send E-mail..."));

                // Tracking phone calls in activity log
                NewActivity newActivityBean = new NewActivity(customerId);
                newActivityBean.setNotes("");
                newActivityBean.setActionType("email");
                newActivityBean.setAssignedByRepId(String.valueOf(loggedInRepId));
                newActivityBean.setAssignedByRepName(loggedInRepName);
                newActivityBean.setAssignedToRepId(String.valueOf(loggedInRepId));
                newActivityBean.setAssignedToRepName(loggedInRepName);
                newActivityBean.setPrivate(isPrivate);

                ActivitiesModel activitiesModel = new ActivitiesModel(activityContext, activity, requestFrom);
                activitiesModel.createNewActivity(encryptedClientKey, getDTTicks(), newActivityBean);
            } else {
                displayToast("Sorry..! No E-mail application found on your device to open.");
            }
        } else {
            displayToast("Invalid E-mail.");
        }
    }

    public void makePhoneCall(Activity activity, Constants.RequestFrom requestFrom, String customerId, String phoneNumber, String extension, boolean isPrivate) {
        RequestCallPermission();
        if (Utility.isValidPhoneNumber(phoneNumber)) {
            //Intent dialIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phoneNumber, null));

            Intent dialIntent;
            String activityNotes;

            if (extension.isEmpty()) {
                dialIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phoneNumber));
                activityNotes = String.format("A call was initiated to (%s) from mobile app.", phoneNumber);
            } else {
                dialIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phoneNumber));
                activityNotes = String.format("A call was initiated to (%s x%s) from mobile app.", phoneNumber, extension);
            }

            if (dialIntent.resolveActivity(getPackageManager()) != null) {
                if (ContextCompat.checkSelfPermission(activityContext, Constants.PERMISSION_CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                    startActivity(dialIntent);

                    // Tracking phone calls in activity log
                    NewActivity newActivityBean = new NewActivity(customerId);
                    newActivityBean.setNotes(activityNotes);
                    newActivityBean.setActionType("call");
                    newActivityBean.setAssignedByRepId(String.valueOf(loggedInRepId));
                    newActivityBean.setAssignedByRepName(loggedInRepName);
                    newActivityBean.setAssignedToRepId(String.valueOf(loggedInRepId));
                    newActivityBean.setAssignedToRepName(loggedInRepName);
                    newActivityBean.setPrivate(false); // all activity logs are public by default when we dial phone number through out app.

                    ActivitiesModel activitiesModel = new ActivitiesModel(activityContext, activity, requestFrom);
                    activitiesModel.createNewActivity(encryptedClientKey, getDTTicks(), newActivityBean);
                }
            } else {
                displayToast(getResources().getString(R.string.no_support_for_phone_call));
            }
        } else {
            displayToast("Invalid Phone Number.");
        }
    }


    public void RequestCallPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {

        } else {
            String permissions[] = {"Manifest.permission.CALL_PHONE"};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissions, 100);
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }


    public void openMapWithLocation(final String address) {

        final CharSequence[] items = { "Googel Maps", "Waze Maps", "Cancel" };

        AlertDialog.Builder builder = new AlertDialog.Builder(BaseActivity.this);
        builder.setTitle("Show Address ");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Googel Maps")) {
                        googleIntent(address);

                } else if (items[item].equals("Waze Maps")) {
                        wazeIntent(address);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });

        Dialog d = builder.show();
        int textViewId = d.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
        TextView tv = (TextView) d.findViewById(textViewId);
        tv.setTextColor(getResources().getColor(R.color.color_primary));

    }

    private void googleIntent(String address) {
        try
        {
            // Launch Waze to look for address:
            Uri mapIntentUri = Uri.parse("google.navigation:q=" + address);
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, mapIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            startActivity(mapIntent);
        }
        catch ( ActivityNotFoundException ex  )
        {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=com.google.android.apps.maps")));
        }
    }

    private void wazeIntent(String address) {
        try
        {
            // Launch Waze to look for address:
            String url = "https://waze.com/ul?q="+address+"&navigate=yes&zoom=17";
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( url ) );
            intent.setPackage("com.waze");
            startActivity( intent );
        }
        catch ( ActivityNotFoundException ex  )
        {
            // If Waze is not installed, open it in Google Play:
            Intent intent = new Intent( Intent.ACTION_VIEW, Uri.parse( "market://details?id=com.waze" ) );
            startActivity(intent);
        }

    }


    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public int getResourceIdForContactTitle(String title) {
        int resourceId;

        switch (title) {
            case "Ad Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Agency Contact":
                resourceId = R.drawable.icon_agency_contact;
                break;
            case "Current-Future Contracts":
                resourceId = R.drawable.icon_company_future_orders;
                break;
            case "Past-Expired Contracts":
                resourceId = R.drawable.icon_company_expired_orders;
                break;
            case "Company":
                resourceId = R.drawable.icon_company_no_orders;
                break;
            case "Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            case "Child Contact":
                resourceId = R.drawable.icon_child_contact;
                break;
            default:
                resourceId = R.drawable.icon_sub_contact;
                break;
        }

        return resourceId;
    }

    public int getContactTypeIcon(Long parentId, boolean isAdAgency, String isLastContractStatus) {
        int resourceId;
        String imageName = "";

        if (isAdAgency) {
            if (parentId == 0 || parentId == -1) {
                imageName = "Agency.png";
                resourceId = R.drawable.icon_agency;
            } else {
                imageName = "agencycontact.png";
                resourceId = R.drawable.icon_agency_contact;
            }
        } else {
            if (parentId == 0 || parentId == -1) {
                if (isLastContractStatus.toLowerCase().equals("future")) {
                    imageName = "companyFutureOrders.png";
                    resourceId = R.drawable.icon_company_future_orders;
                } else if (isLastContractStatus.toLowerCase().equals("past")) {
                    imageName = "companyExpiredOrders.png";
                    resourceId = R.drawable.icon_company_expired_orders;
                } else {
                    imageName = "companyNoOrders.png";
                    resourceId = R.drawable.icon_company_no_orders;
                }
            } else {
                imageName = "subcontact1.png";
                resourceId = R.drawable.icon_sub_contact;
            }
        }

        return resourceId;
    }

    public String getIdForValueFromArrayList(ArrayList<MasterData> list, String value, String listType) {
        String id = "0";

        if (listType.equals(Constants.LIST_TYPE_MASTER_DATA)) {
            for (MasterData masterData : list) {
                if (masterData.getName().equalsIgnoreCase(value)) {
                    id = masterData.getId();
                    break;
                }
            }
        }

        return id;
    }

    public ArrayList removeObjectFromArrayListByName(ArrayList<MasterData> list, String value, String listType) {
        if (listType.equals(Constants.LIST_TYPE_MASTER_DATA)) {
            for (MasterData masterData : list) {
                if (masterData.getName().equalsIgnoreCase(value)) {
                    list.remove(masterData);
                    break;
                }
            }
        }

        return list;
    }

    public boolean checkForBillingContact(List<Contact> subContactsList) {
        boolean isBillingContact = false;

        for (Contact contact : subContactsList) {
            if (contact.getJobDescription().equalsIgnoreCase(Constants.CONTACT_TYPE_BILLING)) {
                isBillingContact = true;
                break;
            }
        }

        return isBillingContact;
    }

    public ArrayList removeObjectFromRepsList(ArrayList<RepData> list, String value) {
        for (RepData repData : list) {
            if (repData.getId().equalsIgnoreCase(value)) {
                list.remove(repData);
                break;
            }
        }

        return list;
    }

    public boolean checkForLoggedInRep(List<RepData> repDataList, String loggedInRepId) {
        boolean isExists = false;

        for (RepData repData : repDataList) {
            if (repData.getId().equalsIgnoreCase(loggedInRepId)) {
                isExists = true;
                break;
            }
        }

        return isExists;
    }

    public void doLogout(String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BaseActivity.this, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle("Session  Error");
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                    MMLocalDataBase db = new MMLocalDataBase(BaseActivity.this);
                    //removeNotificationsFromNotificationTrayOnUserLogout
                    BuildNotification buildNotification = new BuildNotification(BaseActivity.this);
                    buildNotification.removeNotificationsFromNotificationTrayOnUserLogout();
                    //check and clear running services

                    //clear scheduled notifications(alarms) .This can be done , if we have the ids of the scheduled notifications.
                    ArrayList<Integer> pendingIntentIdList = db.getAllScheduledNotifications();
                    if (pendingIntentIdList != null) {
                        PendingIntent pendingIntent;
                        AlarmManager manager;
                        Intent alarmintent;
                        for (Integer notification_id : pendingIntentIdList) {
                            alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                            alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                            manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            //cancel the alarm manager of the pending intent
                            manager.cancel(pendingIntent);

                        }
                    }

                    //disable boot receiver after user logout.
                    disableDeviceBootReceiver();
                    //clear all user store data.
                    sharedPreferences.clear();
                    // mmSettingsPreferences.clear();
                    //truncate  database tables
                    db.truncateTableNotificationIds();

                    //clear jobschedulers
                    FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(BaseActivity.this);
                    firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);

                    ClientInfo.clientList = new ArrayList<ClientInfo>();
                    Intent logInIntent = new Intent(activityContext, LoginActivity.class);
                    logInIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(logInIntent);
                    overridePendingTransition(R.anim.left_in, R.anim.right_out);

                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();
            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(BaseActivity.this.getResources().getDimension(R.dimen.alert_dialog_text_font_size));
            isAlertDialogShown = true;
        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public void disableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

}

