package com.mirabeltechnologies.magazinemanager.activities;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactOrderItemsListActivity;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;

/**
 * Created by venkat on 10/26/17.
 * <p>
 * All Activities which are having List View with Pagination should extend this Activity in order to reuse code which we have implemented in this class.
 * </p>
 */
public class ListViewBaseActivity extends BaseActivity {
    // We will update these 2 values in each sub class to know which activity it is.
    protected Activity currentActivity;
    protected Constants.RequestFrom requestFrom;
    protected ListView list_view;
    protected MMTextView page_number_display;
    protected ImageView left_nav_arrow, right_nav_arrow;
    protected Drawable left_nav_arrow_active_drawable, left_nav_arrow_inactive_drawable, right_nav_arrow_active_drawable, right_nav_arrow_inactive_drawable;
    protected int orientation;
    protected boolean isLoading = false, isInitialRequest = false, isNavigationButtonClicked = false, isAllRecordsLoaded = false, isScrollListenerAssignedToListView = false;
    protected int currentPageNumber, totalNoOfPages, totalNoOfRecords, alreadyLoadedNoOfRecords, navigationPageNumber, lastUpdatedFooterPageNumber, lastVisibleItemIndex, lastUpdatedVisibleItemPageNo;
    protected ArrayList<Integer> alreadyLoadedPageNumbersList;
    protected ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        orientation = Utility.getDeviceOrientation(activityContext);

        left_nav_arrow_active_drawable = ContextCompat.getDrawable(activityContext, R.drawable.left_nav_arrow_active);
        left_nav_arrow_inactive_drawable = ContextCompat.getDrawable(activityContext, R.drawable.left_nav_arrow_inactive);
        right_nav_arrow_active_drawable = ContextCompat.getDrawable(activityContext, R.drawable.right_nav_arrow_active);
        right_nav_arrow_inactive_drawable = ContextCompat.getDrawable(activityContext, R.drawable.right_nav_arrow_inactive);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    public void resetValues() {
        // Removing no records found message if already added to list view.
        if (list_view.getHeaderViewsCount() > 0) {
            list_view.removeHeaderView(no_records_found_text_view);
        }

        isInitialRequest = true;
        currentPageNumber = 1;
        totalNoOfPages = -1;
        totalNoOfRecords = 0;
        navigationPageNumber = 0;
        lastUpdatedFooterPageNumber = 0;
        isNavigationButtonClicked = false;
        isAllRecordsLoaded = false;
        alreadyLoadedNoOfRecords = 0;
        isLoading = false;

        if (alreadyLoadedPageNumbersList != null) {
            alreadyLoadedPageNumbersList.clear();
            alreadyLoadedPageNumbersList = null;
        }
        alreadyLoadedPageNumbersList = new ArrayList();
    }

    public void noRecordsFound() {
        try {
            totalNoOfRecords = 0;
            totalNoOfPages = 0;
            updateFooterPageLabels(0);
            list_view.addHeaderView(no_records_found_text_view);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void updateFooterPageLabels(int pageNo) {
        if (pageNo == 0 || alreadyLoadedPageNumbersList.contains(pageNo)) {
            int startIndex = ((pageNo - 1) * Constants.PAGE_SIZE_INT) + 1;
            int endIndex = pageNo * Constants.PAGE_SIZE_INT;

            if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE || requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE || requestFrom == Constants.RequestFrom.CALLBACKS_PAGE) {
                startIndex = ((pageNo - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
                endIndex = pageNo * Constants.ACTIVITY_PAGE_SIZE_INT;
            } else if (requestFrom == Constants.RequestFrom.CONTACT_ORDERS_PAGE || requestFrom == Constants.RequestFrom.ORDERS_PAGE) {
                startIndex = ((pageNo - 1) * Constants.ORDERS_PAGE_SIZE_INT) + 1;
                endIndex = pageNo * Constants.ORDERS_PAGE_SIZE_INT;
            }
            else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS) {
                startIndex = ((pageNo - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
                endIndex = pageNo * Constants.ACTIVITY_PAGE_SIZE_INT;
            }
            if (pageNo == 0)
                startIndex = 0;

            if (alreadyLoadedNoOfRecords < endIndex)
                endIndex = alreadyLoadedNoOfRecords;
            if(requestFrom==Constants.RequestFrom.NEARBY_CONTACTS){

                page_number_display.setText(String.format(" %d of %d Records",  totalNoOfRecords, totalNoOfRecords));

            }else {
                page_number_display.setText(String.format("Page %d of %d | %d - %d of %d", pageNo, totalNoOfPages, startIndex, endIndex, totalNoOfRecords));

            }
            lastUpdatedFooterPageNumber = pageNo;
            navigationPageNumber = pageNo;
            checkNavigationButtons(pageNo);
}
    }

    // Method for enabling and disabling page navigation buttons
    public void checkNavigationButtons(int pageNumber) {
        if (totalNoOfPages <= 0) {
            left_nav_arrow.setImageDrawable(left_nav_arrow_inactive_drawable);
            left_nav_arrow.setEnabled(false);

            right_nav_arrow.setImageDrawable(right_nav_arrow_inactive_drawable);
            right_nav_arrow.setEnabled(false);

            return;
        }

        if (pageNumber == 1) {
            left_nav_arrow.setImageDrawable(left_nav_arrow_inactive_drawable);
            left_nav_arrow.setEnabled(false);
        } else {
            left_nav_arrow.setImageDrawable(left_nav_arrow_active_drawable);
            left_nav_arrow.setEnabled(true);
        }

        if (pageNumber == totalNoOfPages) {
            right_nav_arrow.setImageDrawable(right_nav_arrow_inactive_drawable);
            right_nav_arrow.setEnabled(false);
        } else {
            right_nav_arrow.setImageDrawable(right_nav_arrow_active_drawable);
            right_nav_arrow.setEnabled(true);
        }
    }

    public void goToPage(View sender) {
        isNavigationButtonClicked = true;

        if (sender.getTag().equals("1")) {
            if (navigationPageNumber > 1)
                navigationPageNumber = navigationPageNumber - 1;
        } else {
            if (navigationPageNumber < totalNoOfPages)
                navigationPageNumber = navigationPageNumber + 1;
        }

        navigateToListViewPosition(navigationPageNumber);
    }

    public void navigateToListViewPosition(final int targetPageNo) {
        if (alreadyLoadedPageNumbersList.contains(targetPageNo)) {
            int targetPosition = (int) Constants.PAGE_SIZE_FLOAT * (navigationPageNumber - 1);

            if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE || requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE || requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                targetPosition = (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT * (navigationPageNumber - 1);
            else if (requestFrom == Constants.RequestFrom.CONTACT_ORDERS_PAGE || requestFrom == Constants.RequestFrom.ORDERS_PAGE)
                targetPosition = (int) Constants.ORDERS_PAGE_SIZE_FLOAT * (navigationPageNumber - 1);
            else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS )
                targetPosition = (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT * (navigationPageNumber - 1);

            list_view.setSelection(targetPosition);
            updateFooterPageLabels(navigationPageNumber);
            checkNavigationButtons(navigationPageNumber);

            list_view.post(new Runnable() {
                @Override
                public void run() {
                    isNavigationButtonClicked = false;
                }
            });

        } else {
            if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE) {
                ((MyContactsActivity) currentActivity).getContacts(targetPageNo);
            } else if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE) {
                ((RepContactsActivity) currentActivity).getRepContacts(targetPageNo);
            } /*else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE) {
                ((SearchActivitiesScreen) currentActivity).getActivities(targetPageNo);
            }*/ else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE) {
                ((TaskListActivity) currentActivity).getTaskList(targetPageNo);
            } else if (requestFrom == Constants.RequestFrom.CONTACT_ORDERS_PAGE) {
                ((ContactOrderItemsListActivity) currentActivity).getContactOrderItemsList(targetPageNo);
            } else if (requestFrom == Constants.RequestFrom.ORDERS_PAGE) {
                ((OrdersActivity) currentActivity).getOrders(targetPageNo);
            } else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE) {
                ((CallbacksActivity) currentActivity).getCallbacks(targetPageNo);
            } else if (requestFrom == Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE) {
                ((MyLast100ContactsActivity) currentActivity).getMyLast100Contacts(targetPageNo);
            }
            else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS) {
                ((NearbyContactsActivity) currentActivity).getNearContacts(targetPageNo);
            }
        }
    }
}
