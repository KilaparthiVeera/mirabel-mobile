package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Order implements Serializable {
    @SerializedName("OrderID")
    @Expose
    private String orderId;
    @SerializedName("CustomerID")
    @Expose
    private String customerId;
    @SerializedName("Customer")
    @Expose
    private String customer;
    @SerializedName("Product")
    @Expose
    private String product;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("Net")
    @Expose
    private String net;
    @SerializedName("Barter")
    @Expose
    private String barter;
    @SerializedName("SubProductTypeID")
    @Expose
    private String subProductTypeId; // Possible values 3 - Digital && 1 - Print
    @SerializedName("SalesRep")
    @Expose
    private String salesRep;
    @SerializedName("TotalNet")
    @Expose
    private String totalNet;
    @SerializedName("TotalBarter")
    @Expose
    private String totalBarter;
    @SerializedName("TotalCount")
    @Expose
    private String totalCount;

    public String getOrderId() {
        return orderId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getCustomer() {
        return customer;
    }

    public String getProduct() {
        return product;
    }

    public String getDescription() {
        return description;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getNet() {
        return net;
    }

    public String getBarter() {
        return barter;
    }

    public String getSubProductTypeId() {
        return subProductTypeId;
    }

    public String getSalesRep() {
        return salesRep;
    }

    public String getTotalNet() {
        return totalNet;
    }

    public String getTotalBarter() {
        return totalBarter;
    }

    public String getTotalCount() {
        return totalCount;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId='" + orderId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", customer='" + customer + '\'' +
                ", product='" + product + '\'' +
                ", description='" + description + '\'' +
                ", startDate='" + startDate + '\'' +
                ", net='" + net + '\'' +
                ", barter='" + barter + '\'' +
                ", subProductTypeId='" + subProductTypeId + '\'' +
                ", salesRep='" + salesRep + '\'' +
                ", totalNet='" + totalNet + '\'' +
                ", totalBarter='" + totalBarter + '\'' +
                ", totalCount='" + totalCount + '\'' +
                '}';
    }
}
