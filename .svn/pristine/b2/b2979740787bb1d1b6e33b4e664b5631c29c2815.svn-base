package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.MyLast100ContactsActivity;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.Notification;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkat on 10/6/17.
 */

public class DashboardModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;

    public DashboardModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void getRecentContacts(final String encryptedClientKey, String dtTicks, final boolean loadNotifications, final int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.PAGE_SIZE_INT;

                String getRecentContactsReqURL = String.format("%s/%s?contacttype=RECENT&startindex=%d&lastindex=%d&dtticks=%s", Constants.MAIN_URL, Constants.GET_RECENT_OR_ALL_CONTACTS, startIndex, endIndex, dtTicks);

                JsonObjectRequest getRecentContactsReq = new JsonObjectRequest(Request.Method.GET, getRecentContactsReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        int noOfRecentContacts = response.getInt("Count");

                                        Type recentContactsListType = new TypeToken<ArrayList<Contact>>() {
                                        }.getType();

                                        List<Contact> recentContactsList = new Gson().fromJson(response.getString("Contacts"), recentContactsListType);

                                        for (int i = 0; i < recentContactsList.size(); i++) {
                                            Contact contact = recentContactsList.get(i);

                                            if (contact.getTitle().equals("Child Contact")) {
                                                if (i > 0) {
                                                    Contact preContact = recentContactsList.get(i - 1);

                                                    if (contact.getParentId().equals(preContact.getCustomerId()) || (preContact.getTitle().equals("Child Contact") && contact.getParentId().equals(preContact.getParentId()))) {
                                                        contact.setTitle("Child Contact");
                                                    } else {
                                                        contact.setTitle("Contact");
                                                    }
                                                } else {
                                                    contact.setTitle("Contact");
                                                }
                                            }
                                        }

                                        if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                            ((DashboardActivity) activity).updateRecentContacts(noOfRecentContacts, recentContactsList, true, loadNotifications);
                                        else if (requestFrom == Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE)
                                            ((MyLast100ContactsActivity) activity).updateMyLast100ContactsList(recentContactsList, noOfRecentContacts);
                                    } else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                                ((DashboardActivity) activity).updateRecentContacts(0, null, true, loadNotifications);
                                            else if (requestFrom == Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE)
                                                ((MyLast100ContactsActivity) activity).updateMyLast100ContactsList(null, 0);
                                        } else {
                                            if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                                ((DashboardActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                            else if (requestFrom == Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE)
                                                ((MyLast100ContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                    ((DashboardActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE)
                                    ((MyLast100ContactsActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getRecentContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getDashboardNotifications(final String encryptedClientKey, String dtTicks, String activityType, final boolean resetRadioButton) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getDashboardNotificationsReqURL = String.format("%s/%s?activityType=%s&start=0&limit=15&dtticks=%s", Constants.MAIN_URL, Constants.GET_DASHBOARD_NOTIFICATIONS, activityType, dtTicks);

                JsonObjectRequest getDashboardNotificationsReq = new JsonObjectRequest(Request.Method.GET, getDashboardNotificationsReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        Type notificationsListType = new TypeToken<ArrayList<Notification>>() {
                                        }.getType();

                                        List<Notification> notifications = new Gson().fromJson(response.getString("ActivityDetails"), notificationsListType);
                                        ((DashboardActivity) activity).updateNotifications(notifications, true, resetRadioButton);
                                    } else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                            ((DashboardActivity) activity).updateNotifications(null, true, resetRadioButton);
                                        else
                                            ((DashboardActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((DashboardActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getDashboardNotificationsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
