package com.mirabeltechnologies.magazinemanager.beans;

import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.io.Serializable;

/**
 * Created by venkat on 12/29/17.
 */

public class SearchActivitiesFilter implements Serializable {
    private String filterOption;
    private int filterOptionIndex;
    private String companyName;
    private String companyId;
    private String dateType; // Possible Values : lstDateCreated, lstDateScheduled, lstDateCompleted
    private int dateTypeIndex;
    private String fromDate; // Format : MM/DD/YYYY
    private String toDate; // Format : MM/DD/YYYY
    private String Keyword;
    private String assignedToRepId;
    private String assignedToRepName;
    private int isCreatedByOrAssignedBy; // 0 - Created By and 1 - Assigned By
    private int isCreatedByOrAssignedByIndex;
    private String createdByOrAssignedByRepId;
    private String createdByOrAssignedByRepName;
    private String departmentId;
    private String department;
    private String activityTypeId;
    private String activityType;

    public SearchActivitiesFilter(String actType) {
        if (actType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
            this.setFilterOption("1");
        } else if (actType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
            this.setFilterOption("2");
        } else if (actType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
            this.setFilterOption("3");
        } else if (actType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
            this.setFilterOption("1,2,3,5,6");
        } else if (actType.equals(Constants.ACTIVITY_TYPE_CALLS_MEETINGS)) {
            this.setFilterOption("2,3");
        }

        this.filterOptionIndex = 0;
        this.companyName = "";
        this.companyId = "";
        this.dateType = "lstDateCreated";
        this.dateTypeIndex = 0;
        this.fromDate = "";
        this.toDate = "";
        this.Keyword = "";
        this.assignedToRepId = "-1";
        this.assignedToRepName = "All Reps";
        this.isCreatedByOrAssignedBy = 0;
        this.isCreatedByOrAssignedByIndex = 0;
        this.createdByOrAssignedByRepId = "-1";
        this.createdByOrAssignedByRepName = "All Reps";
        this.departmentId = "-1";
        this.department = "All Departments";
        this.activityTypeId = "";
        this.activityType = "";
    }

    public String getFilterOption() {
        return filterOption;
    }

    public void setFilterOption(String filterOption) {
        this.filterOption = filterOption;
    }

    public int getFilterOptionIndex() {
        return filterOptionIndex;
    }

    public void setFilterOptionIndex(int filterOptionIndex) {
        this.filterOptionIndex = filterOptionIndex;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public int getDateTypeIndex() {
        return dateTypeIndex;
    }

    public void setDateTypeIndex(int dateTypeIndex) {
        this.dateTypeIndex = dateTypeIndex;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getKeyword() {
        return Keyword;
    }

    public void setKeyword(String keyword) {
        Keyword = keyword;
    }

    public String getAssignedToRepId() {
        return assignedToRepId;
    }

    public void setAssignedToRepId(String assignedToRepId) {
        this.assignedToRepId = assignedToRepId;
    }

    public String getAssignedToRepName() {
        return assignedToRepName;
    }

    public void setAssignedToRepName(String assignedToRepName) {
        this.assignedToRepName = assignedToRepName;
    }

    public int getIsCreatedByOrAssignedBy() {
        return isCreatedByOrAssignedBy;
    }

    public void setIsCreatedByOrAssignedBy(int isCreatedByOrAssignedBy) {
        this.isCreatedByOrAssignedBy = isCreatedByOrAssignedBy;
    }

    public int getIsCreatedByOrAssignedByIndex() {
        return isCreatedByOrAssignedByIndex;
    }

    public void setIsCreatedByOrAssignedByIndex(int isCreatedByOrAssignedByIndex) {
        this.isCreatedByOrAssignedByIndex = isCreatedByOrAssignedByIndex;
    }

    public String getCreatedByOrAssignedByRepId() {
        return createdByOrAssignedByRepId;
    }

    public void setCreatedByOrAssignedByRepId(String createdByOrAssignedByRepId) {
        this.createdByOrAssignedByRepId = createdByOrAssignedByRepId;
    }

    public String getCreatedByOrAssignedByRepName() {
        return createdByOrAssignedByRepName;
    }

    public void setCreatedByOrAssignedByRepName(String createdByOrAssignedByRepName) {
        this.createdByOrAssignedByRepName = createdByOrAssignedByRepName;
    }

    public String getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(String departmentId) {
        this.departmentId = departmentId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    @Override
    public String toString() {
        return "SearchActivitiesFilter{" +
                "filterOption='" + filterOption + '\'' +
                ", filterOptionIndex=" + filterOptionIndex +
                ", companyName='" + companyName + '\'' +
                ", companyId='" + companyId + '\'' +
                ", dateType='" + dateType + '\'' +
                ", dateTypeIndex=" + dateTypeIndex +
                ", fromDate='" + fromDate + '\'' +
                ", toDate='" + toDate + '\'' +
                ", Keyword='" + Keyword + '\'' +
                ", assignedToRepId='" + assignedToRepId + '\'' +
                ", assignedToRepName='" + assignedToRepName + '\'' +
                ", isCreatedByOrAssignedBy=" + isCreatedByOrAssignedBy +
                ", isCreatedByOrAssignedByIndex=" + isCreatedByOrAssignedByIndex +
                ", createdByOrAssignedByRepId='" + createdByOrAssignedByRepId + '\'' +
                ", createdByOrAssignedByRepName='" + createdByOrAssignedByRepName + '\'' +
                ", departmentId='" + departmentId + '\'' +
                ", department='" + department + '\'' +
                ", activityTypeId='" + activityTypeId + '\'' +
                ", activityType='" + activityType + '\'' +
                '}';
    }
}
