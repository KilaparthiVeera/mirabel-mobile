package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

public class SearchOrdersFilter implements Serializable {
    private String salesRepId;
    private String salesRepName;
    private String productType;
    private int productTypeValue;
    private int productTypeIndex;
    private String businessUnitId;
    private String businessUnit;
    private String productId;
    private String product;
    private String magazineId;
    private String magazine;
    private String rateCardId;
    private String rateCard;
    private String productsValue;
    private int productsIndex;
    private String issueYearId;
    private String issueYear;
    private String issueId;
    private String issue;
    private int dateRangeIndex;
    private String dateRangeValue;
    private String fromDate; // Format : yyyy-MM-dd
    private String displayFromDate; // Format : MM/dd/y
    private String toDate; // Format : yyyy-MM-dd
    private String displayToDate; // Format : MM/dd/y
    private String amountType;
    private int amountTypeValue;
    private int amountTypeIndex;

    public SearchOrdersFilter() {
        this.salesRepId = "-1";
        this.salesRepName = "";
        this.productType = "All";
        this.productTypeValue = -1;
        this.productTypeIndex = 0;
        this.businessUnitId = "-1";
        this.businessUnit = "";
        this.productId = "-1";
        this.product = "";
        this.magazineId = "-1";
        this.magazine = "";
        this.rateCardId = "0";
        this.rateCard = "";
        this.productsValue = "with issues";
        this.productsIndex = 0;
        this.issueYearId = "0";
        this.issueYear = "All Years";
        this.issueId = "0";
        this.issue = "All Issues";
        this.dateRangeIndex = 0;
        this.dateRangeValue = "";
        this.fromDate = "";
        this.displayFromDate = "";
        this.toDate = "";
        this.displayToDate = "";
        this.amountType = "Use Net on Issue/Start Date";
        this.amountTypeValue = 1;
        this.amountTypeIndex = 1;
    }

    public String getSalesRepId() {
        return salesRepId;
    }

    public void setSalesRepId(String salesRepId) {
        this.salesRepId = salesRepId;
    }

    public String getSalesRepName() {
        return salesRepName;
    }

    public void setSalesRepName(String salesRepName) {
        this.salesRepName = salesRepName;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public int getProductTypeValue() {
        return productTypeValue;
    }

    public void setProductTypeValue(int productTypeValue) {
        this.productTypeValue = productTypeValue;
    }

    public int getProductTypeIndex() {
        return productTypeIndex;
    }

    public void setProductTypeIndex(int productTypeIndex) {
        this.productTypeIndex = productTypeIndex;
    }

    public String getBusinessUnitId() {
        return businessUnitId;
    }

    public void setBusinessUnitId(String businessUnitId) {
        this.businessUnitId = businessUnitId;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getMagazineId() {
        return magazineId;
    }

    public void setMagazineId(String magazineId) {
        this.magazineId = magazineId;
    }

    public String getMagazine() {
        return magazine;
    }

    public void setMagazine(String magazine) {
        this.magazine = magazine;
    }

    public String getRateCardId() {
        return rateCardId;
    }

    public void setRateCardId(String rateCardId) {
        this.rateCardId = rateCardId;
    }

    public String getRateCard() {
        return rateCard;
    }

    public void setRateCard(String rateCard) {
        this.rateCard = rateCard;
    }

    public String getProductsValue() {
        return productsValue;
    }

    public void setProductsValue(String productsValue) {
        this.productsValue = productsValue;
    }

    public int getProductsIndex() {
        return productsIndex;
    }

    public void setProductsIndex(int productsIndex) {
        this.productsIndex = productsIndex;
    }

    public String getIssueYearId() {
        return issueYearId;
    }

    public void setIssueYearId(String issueYearId) {
        this.issueYearId = issueYearId;
    }

    public String getIssueYear() {
        return issueYear;
    }

    public void setIssueYear(String issueYear) {
        this.issueYear = issueYear;
    }

    public String getIssueId() {
        return issueId;
    }

    public void setIssueId(String issueId) {
        this.issueId = issueId;
    }

    public String getIssue() {
        return issue;
    }

    public void setIssue(String issue) {
        this.issue = issue;
    }

    public int getDateRangeIndex() {
        return dateRangeIndex;
    }

    public void setDateRangeIndex(int dateRangeIndex) {
        this.dateRangeIndex = dateRangeIndex;
    }

    public String getDateRangeValue() {
        return dateRangeValue;
    }

    public void setDateRangeValue(String dateRangeValue) {
        this.dateRangeValue = dateRangeValue;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getDisplayFromDate() {
        return displayFromDate;
    }

    public void setDisplayFromDate(String displayFromDate) {
        this.displayFromDate = displayFromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getDisplayToDate() {
        return displayToDate;
    }

    public void setDisplayToDate(String displayToDate) {
        this.displayToDate = displayToDate;
    }

    public String getAmountType() {
        return amountType;
    }

    public void setAmountType(String amountType) {
        this.amountType = amountType;
    }

    public int getAmountTypeValue() {
        return amountTypeValue;
    }

    public void setAmountTypeValue(int amountTypeValue) {
        this.amountTypeValue = amountTypeValue;
    }

    public int getAmountTypeIndex() {
        return amountTypeIndex;
    }

    public void setAmountTypeIndex(int amountTypeIndex) {
        this.amountTypeIndex = amountTypeIndex;
    }

    @Override
    public String toString() {
        return "SearchOrdersFilter{" +
                "salesRepId='" + salesRepId + '\'' +
                ", salesRepName='" + salesRepName + '\'' +
                ", productType='" + productType + '\'' +
                ", productTypeValue=" + productTypeValue +
                ", productTypeIndex=" + productTypeIndex +
                ", businessUnitId='" + businessUnitId + '\'' +
                ", businessUnit='" + businessUnit + '\'' +
                ", productId='" + productId + '\'' +
                ", product='" + product + '\'' +
                ", magazineId='" + magazineId + '\'' +
                ", magazine='" + magazine + '\'' +
                ", rateCardId='" + rateCardId + '\'' +
                ", rateCard='" + rateCard + '\'' +
                ", productsValue='" + productsValue + '\'' +
                ", productsIndex=" + productsIndex +
                ", issueYearId='" + issueYearId + '\'' +
                ", issueYear='" + issueYear + '\'' +
                ", issueId='" + issueId + '\'' +
                ", issue='" + issue + '\'' +
                ", dateRangeIndex=" + dateRangeIndex +
                ", dateRangeValue='" + dateRangeValue + '\'' +
                ", fromDate='" + fromDate + '\'' +
                ", displayFromDate='" + displayFromDate + '\'' +
                ", toDate='" + toDate + '\'' +
                ", displayToDate='" + displayToDate + '\'' +
                ", amountType='" + amountType + '\'' +
                ", amountTypeValue=" + amountTypeValue +
                ", amountTypeIndex=" + amountTypeIndex +
                '}';
    }
}
