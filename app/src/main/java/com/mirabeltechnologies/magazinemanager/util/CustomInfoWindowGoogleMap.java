package com.mirabeltechnologies.magazinemanager.util;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.LocateContactOnMapActivity;
import com.mirabeltechnologies.magazinemanager.beans.MyItem;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {

    private Context context;
    List<MyItem> itemlist;
    String type;

    public CustomInfoWindowGoogleMap(Context ctx, ArrayList<MyItem> itemlist,String type) {

        this.context = ctx;
        this.itemlist=itemlist;
        this.type=type;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        return null;
    }
    @Override
    public View getInfoContents(Marker marker) {
        View myContentsView = ((Activity) context).getLayoutInflater()
                .inflate(R.layout.custom_marker_details_layout, null);
        TextView title = myContentsView.findViewById(R.id.title);
        TextView address_text = myContentsView.findViewById(R.id.address_text);
        if(type.equals("item"))
        {

            StringBuilder result = new StringBuilder();
            for (int i=0;i<itemlist.size();i++){

                if(marker.getPosition().equals(itemlist.get(i).getPosition())){

                    result.append(itemlist.get(i).getTitle()).append( ", " );

                }
            }
            String title_withoutLastComma = result.substring( 0, result.length( ) - ", ".length( ) );
            title.setText(title_withoutLastComma);
            address_text.setText(marker.getSnippet());
        }
        else {
            StringBuilder result = new StringBuilder();
            for (int i=0;i<itemlist.size();i++){

                result.append(itemlist.get(i).getTitle()).append( ", " );

            }
            String title_withoutLastComma = result.substring( 0, result.length( ) - ", ".length( ) );
            title.setText(title_withoutLastComma);
        }
        return myContentsView;
    }

}