package com.mirabeltechnologies.magazinemanager.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.util.Patterns;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.google.gson.Gson;
import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALL_ADVANCE_SCHEDULE_TIME;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETING;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETING_ADVANCE_SCHEDULE_TIME;


/**
 * Created by venkat.
 *
 * @author venkat
 */
public class Utility {

    private static Utility instance;

    public static Utility getInstance() {
        if (instance == null)
            instance = new Utility();
        return instance;
    }

    public static int getDeviceOrientation(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    public static boolean isLandscapeOrientation(Context context) {
        if (context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            return true;
        else
            return false;
    }

    public static int getDeviceWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDeviceHeight(Context context) {
        return context.getResources().getDisplayMetrics().heightPixels;
    }

    public static void displayToast(Context context, String message, int length) {
        Toast.makeText(context, message, length).show();
    }

    public static void logResult(String tag, String message) {
        Log.d(tag, message);
    }

    //Method to convert UTC time to Local time
    public static String formatTime(long timeInMillSecs, String format) {
        Date utcTime = new Date(timeInMillSecs);
        DateFormat df = new SimpleDateFormat(format);
        return df.format(utcTime);
    }

    public static String formatDate(Date date, String format) {
        DateFormat df = new SimpleDateFormat(format);
        return df.format(date);
    }

    public static Date convertStringToDate(String stringDate, String format) {
        Date date = null;
        try {
            DateFormat df = new SimpleDateFormat(format);
            date = df.parse(stringDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return date;
    }

    public static boolean isValidEmail(String emailId) {
        /*boolean flag = false;

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailId);

        if (matcher.matches())
            flag = true;
        else
            flag = false;

        return flag;*/

        // We can use below code also.
        return Patterns.EMAIL_ADDRESS.matcher(emailId).matches();
    }

    public static boolean isValidPhoneNumber(String phoneNumber) {
        return Patterns.PHONE.matcher(phoneNumber).matches();
    }

    public static int getPixelFromDips(Resources resources, float pixels) {
        // Get the screen's density scale
        final float scale = resources.getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (pixels * scale + 0.5f);
    }

    public static String getHexaStringForColor(int colorCode) {
        return String.format("#%06X", (0xFFFFFF & colorCode));
    }

    /**
     * @param values
     * @param delimiter
     * @return
     */
    public static String convertArrayListToString(ArrayList values, String delimiter) {
        StringBuilder sb = new StringBuilder();

        for (Object obj : values) {
            sb.append(obj);

            if (values.size() > 1)
                sb.append(delimiter);
        }

        if (values.size() > 1)
            sb.setLength(sb.length() - 1);

        return sb.toString();
    }

    /**
     * Checks if the device is a tablet or a phone
     * <p/>
     * Note:
     * 320dp : a typical phone screen (240x320 ldpi, 320x480 mdpi, 480x800 hdpi, etc).
     * 480dp : a tweener tablet like the Streak (480x800 mdpi).
     * 600dp : a 7” tablet (600x1024 mdpi).
     * 720dp : a 10” tablet (720x1280 mdpi, 800x1280 mdpi, etc).
     *
     * @param context The context.
     * @return Returns true if the device is a Tablet
     */
    public static boolean isTabletDevice(Context context) {
        boolean isTablet = false;

        try {
            float smallestWidth = context.getResources().getConfiguration().smallestScreenWidthDp;

            // if smallestWidth >= 600 - Device is a 7" tablet & if smallestWidth >= 720 - Device is a 10" tablet
            if (smallestWidth >= 600) {
                isTablet = true;
            }

            //isTablet = (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return isTablet;
    }

    public static boolean isLargeScreen(Context context) {
        if (context.getResources().getDisplayMetrics().widthPixels > Constants.DEVICE_MIN_WIDTH)
            return true;
        else
            return false;
    }

    @SuppressWarnings("deprecation")
    public static Spanned fromHtml(String html) {
        Spanned result;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            result = Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY);
        } else {
            result = Html.fromHtml(html);
        }

        return result;
    }

    public static Spanned getFormattedKeyAndColouredNormalText(String key, String text, String colorCode) {
        return fromHtml(String.format("%s : <font color='%s'> %s </font>", key, colorCode, text));
    }

    public static Spanned getFormattedColouredNormalText(String text, String colorCode) {
        return fromHtml(String.format("<font color='%s'> %s </font>", colorCode, text));
    }

    public static Spanned getFormattedColouredBoldText(String text, String colorCode) {
        return fromHtml(String.format("<b><font color='%s'> %s </font></b>", colorCode, text));
    }

    public static Spanned getFormattedKeyAndColouredBoldText(String key, String text, String colorCode) {
        return fromHtml(String.format("%s : <b><font color='%s'> %s </font></b>", key, colorCode, text));
    }

    public static Spanned getFormattedKeyAndColouredNumber(String key, int value, String colorCode) {
        return fromHtml(String.format("%s : <font color='%s'> %,d </font>", key, colorCode, value));
    }

    public static Spanned getFormattedColouredTextWithUnderscore(String value, String colorCode) {
        return fromHtml(String.format("<u><font color='%s'>%s</font></u>", colorCode, value));
    }

    public static Spanned getFormattedTextWithUnderscore(String value) {
        return fromHtml(String.format("<u>%s</u>", value));
    }

    public static Spanned getFormattedKeyAndColouredTextWithUnderscore(String key, String value, String colorCode) {
        return fromHtml(String.format("%s : <u><font color='%s'>%s</font></u>", key, colorCode, value));
    }

    public static Spanned getFormattedKeyAndColouredNumberWithUnderscore(String key, int value, String colorCode) {
        return fromHtml(String.format("%s : <u><font color='%s'>%s</font></u>", key, colorCode, value));
    }

    public static int getCurrentYear() {
        Calendar cal = Calendar.getInstance();
        return cal.get(Calendar.YEAR);
    }

    /**
     * Checks if the app has permission to do required operation
     * <p>
     * If the app does not has permission then the user will be prompted to grant permissions
     *
     * @param activity
     */
    @TargetApi(23)
    public static boolean verifyPermissions(Activity activity, String checkPermission, String[] requiredPermissions, int permissionRequestCode) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // Check if we have write permission
            int permission = ContextCompat.checkSelfPermission(activity, checkPermission);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(activity, requiredPermissions, permissionRequestCode);
            } else
                return true;

        } else {
            return true;
        }

        return false;
    }

    @TargetApi(23)
    public static boolean verifyPermissionsFromNotification(Context context, String checkPermission, String[] requiredPermissions, int permissionRequestCode) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            // Check if we have write permission
            int permission = ContextCompat.checkSelfPermission(context, checkPermission);

           /* if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(activity, requiredPermissions, permissionRequestCode);
            } else
                return true;
*/
        } else {
            return true;
        }

        return false;
    }


    public static String getMimeType(Context context, Uri uri) {
        String extension;

        //Check uri format to avoid null
        if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
            //If scheme is a content
            final MimeTypeMap mime = MimeTypeMap.getSingleton();
            extension = mime.getExtensionFromMimeType(context.getContentResolver().getType(uri));
        } else {
            //If scheme is a File
            //This will replace white spaces with %20 and also other special characters. This will avoid returning null values on file name with spaces and special characters.
            extension = MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(new File(uri.getPath())).toString());

        }

        return extension;
    }

    public Uri getImageUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "img", null);
        return Uri.parse(path);
    }


    public static byte[] serializeObject(Object obj) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream os = new ObjectOutputStream(out);
        os.writeObject(obj);
        return out.toByteArray();
    }

    public static Object deserializeObject(byte[] data) throws IOException, ClassNotFoundException {
        ByteArrayInputStream in = new ByteArrayInputStream(data);
        ObjectInputStream is = new ObjectInputStream(in);
        return is.readObject();
    }


    public String objectToStringConverter(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }


    public Object stringToObjectConverter(String string) {
        Gson gson = new Gson();
        return gson.fromJson(string, Object.class);
    }


    public static int lastFiveDigitsOfTaskId(String taskId) {
        String reqdStr;
        if (taskId.length() > 4) {
            reqdStr = taskId.substring(taskId.length() - 5);
        } else {
            reqdStr = taskId;
        }
        return Integer.parseInt(reqdStr);
    }


    public static Long getScheduledMillis(String type, Long millisec) {
        Long requiredmillis = null;
        //logic to notify the user before scheduled time i.e, calls -->10 mins and meetings ---> 15 mins the scheduled time.
        //if time of scheduling is less than 10min or 15 min , then the notification should be scheduled  for the next minute and trigger.
        switch (type) {
            case MEETING: {
                if (millisec - System.currentTimeMillis() > (MEETING_ADVANCE_SCHEDULE_TIME * 60 * 1000)) {
                    //schedule 15 min before
                    requiredmillis = millisec - (MEETING_ADVANCE_SCHEDULE_TIME * 60 * 1000);
                } else {
                    requiredmillis = System.currentTimeMillis() + (60 * 1000);
                }
                break;
            }
            case CALL: {
                if (millisec - System.currentTimeMillis() > (CALL_ADVANCE_SCHEDULE_TIME * 60 * 1000)) {
                    //schedule 5 min before
                    requiredmillis = millisec - (CALL_ADVANCE_SCHEDULE_TIME * 60 * 1000);
                } else {
                    requiredmillis = System.currentTimeMillis() + (60 * 1000);
                }
                break;
            }
        }
        return requiredmillis;

    }


    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


    public String getCurrntTime() {
        Date dateeeTime = Calendar.getInstance().getTime();
        DateFormat date1 = new SimpleDateFormat("dd/MM/yyyy HH:mm a");
        return date1.format(dateeeTime);
    }

    public static String getDTTicks() {
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        return des.getEncryptedValueWithOutReplacingCharacters(String.valueOf(System.currentTimeMillis()));
    }
}
