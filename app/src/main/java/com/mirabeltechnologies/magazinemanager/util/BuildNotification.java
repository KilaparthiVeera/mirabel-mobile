package com.mirabeltechnologies.magazinemanager.util;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.TypedValue;
import android.view.View;
import android.widget.RemoteViews;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.receiver.NotificationActionsReceiver;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import static androidx.core.app.NotificationCompat.*;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_CLOSE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_DETAILS;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_EVENT_DELETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_MARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_NOTIFICATION_FORCE_CLOSE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_SNOOZE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_UNMARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_ID;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_TYPE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.PHONE_NUMBER;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.REPEATS_SWITCH;


public class BuildNotification {
    private NotificationManager notificationManager;
    private int notificationId;

    private Context context;
    private Utility utility;
    private MMSettingsPreferences mmSettingsPreferences;

    public BuildNotification(Context context) {
        this.context = context;
        utility = new Utility();
        mmSettingsPreferences = new MMSettingsPreferences(context);
        notificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);
    }


    private NotificationManager getNotificationManager() {
        if (notificationManager == null) {
            return notificationManager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        } else {
            return notificationManager;
        }
    }


    //remove the notification from notification tray
    public void removeNotificationsFromNotificationTrayOnUserLogout() {
        getNotificationManager().cancelAll();

    }

    public void removeNotificationFromNotificationTray(int id) {
        getNotificationManager().cancel(id);

    }


    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_mm_notification_white : R.drawable.ic_mm_notification_white;
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    public void createNotificationChannel() {
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(
                Constants.CALLS_AND_MEETINGS_CHANNEL_ID, Constants.CALLS_AND_MEETINGS_CHANNEL_NAME, importance);
        mChannel.setShowBadge(true);
        mChannel.setLightColor(Color.GREEN);
        getNotificationManager().createNotificationChannel(mChannel);

    }


   /* public void createNotification(ActivityData activityData) {
        String message;
        String type;
        String mobileNumber;
        type = activityData.getType();
        mobileNumber = activityData.getPhone();
        Long activityId = activityData.getActivityId();
        notificationId = utility.lastFiveDigitsOfTaskId(String.valueOf(activityId));
        String time = activityData.getDateScheduled();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = simpledateformat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        String timeStr = sdfTime.format(date);


        String title = "" + type +
                " - " + activityData.getCompanyName();

        message = ""
                + timeStr +
                " - " + activityData.getSpecialNotes();
        Builder alamNotificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            alamNotificationBuilder = new Builder(context, Constants.CALLS_AND_MEETINGS_CHANNEL_ID);
        } else {
            alamNotificationBuilder = new Builder(context);
        }
        alamNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        alamNotificationBuilder.setColorized(true);
        *//* alamNotificationBuilder.setContentTitle(type);*//*
        alamNotificationBuilder.setContentTitle(title);
        alamNotificationBuilder.setContentText(message);
        alamNotificationBuilder.setVisibility(VISIBILITY_PUBLIC);
        alamNotificationBuilder.setPriority(PRIORITY_DEFAULT);
        alamNotificationBuilder.setShowWhen(true);
        alamNotificationBuilder.setSmallIcon(getNotificationIcon());
        alamNotificationBuilder.setLights(Color.BLUE, 500, 500);
        alamNotificationBuilder.setDeleteIntent(forceClosePendingIntent());
        *//* alamNotificationBuilder.setAutoCancel(true);*//*
        alamNotificationBuilder.setColor(ContextCompat.getColor(context, R.color.color_primary_dark));
        alamNotificationBuilder.setBadgeIconType(R.mipmap.ic_launcher);
        //check weather the device has call feature by default or not and act accordingly
        Intent calIntent = new Intent(Intent.ACTION_DIAL);
        calIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = context.getPackageManager();
        if (mobileNumber.length() != 0 && type.equals(CALL) && calIntent.resolveActivity(packageManager) != null) {
            alamNotificationBuilder.addAction(R.drawable.ic_mm_notification_call, "CALL", callPendingIntent(activityData.getPhone(), activityData));
            alamNotificationBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_mm_notification_call_android));
            alamNotificationBuilder.setContentIntent(callPendingIntent(activityData.getPhone(), activityData));
        } else {
            alamNotificationBuilder.setContentIntent(detailsPendingIntent(activityData));
        }
        if (mmSettingsPreferences.getBooleanValue(REPEATS_SWITCH)) {
            alamNotificationBuilder.addAction(R.drawable.ic_mm_notification_snooze, SNOOZE, snoozePendingIntent(activityData));
        }
        alamNotificationBuilder.addAction(R.drawable.ic_mm_notification_close, CANCEL, closePendingIntent());

        if (verifyMarkAsCompleteFeature(activityData) == 1 || verifyMarkAsCompleteFeature(activityData) == 5) {
            alamNotificationBuilder.addAction(R.drawable.mark_completed, "Mark", markAsCompletePendingIntent(activityData));
        }
        getNotificationManager().notify(notificationId, alamNotificationBuilder.build());

    }*/


    public void createNotification(ActivityData activityData) {

        System.out.println("===inside createNotification");
        String message;
        String type;
        String mobileNumber;
        type = activityData.getType();
        mobileNumber = activityData.getPhone();
        Long activityId = activityData.getActivityId();
        notificationId = utility.lastFiveDigitsOfTaskId(String.valueOf(activityId));
        String time = activityData.getDateScheduled();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = simpledateformat.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
        String timeStr = sdfTime.format(date);


        String title = "" + type +
                " - " + activityData.getCompanyName();

        message = ""
                + timeStr +
                " - " + activityData.getSpecialNotes();
        Builder alamNotificationBuilder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            alamNotificationBuilder = new Builder(context, Constants.CALLS_AND_MEETINGS_CHANNEL_ID);
        } else {
            alamNotificationBuilder = new Builder(context);
        }
        //  alamNotificationBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));
        alamNotificationBuilder.setColorized(true);
        //  alamNotificationBuilder.setContentTitle(title);
        //  alamNotificationBuilder.setContentText(message);
        alamNotificationBuilder.setVisibility(VISIBILITY_PUBLIC);
        alamNotificationBuilder.setPriority(PRIORITY_DEFAULT);
        alamNotificationBuilder.setShowWhen(true);
        alamNotificationBuilder.setSmallIcon(getNotificationIcon());
        alamNotificationBuilder.setLights(Color.BLUE, 500, 500);
        alamNotificationBuilder.setDeleteIntent(forceClosePendingIntent());
        alamNotificationBuilder.setColor(ContextCompat.getColor(context, R.color.color_primary_dark));
        alamNotificationBuilder.setBadgeIconType(R.mipmap.ic_launcher);
        alamNotificationBuilder.setStyle(new NotificationCompat.DecoratedCustomViewStyle());


        RemoteViews notificationLayout = new RemoteViews(context.getPackageName(), R.layout.custom_notification_layout_small);
        RemoteViews notificationLayoutExpanded = new RemoteViews(context.getPackageName(), R.layout.custom_notification_layout_large);

        //set text for title
        notificationLayout.setTextViewText(R.id.tv_title_small_notification, title);
        notificationLayout.setTextColor(R.id.tv_title_small_notification, context.getResources().getColor(R.color.notification_title_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationLayout.setTextViewTextSize(R.id.tv_title_small_notification, TypedValue.COMPLEX_UNIT_SP, 14);
        }

        //set message for message content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationLayout.setTextViewTextSize(R.id.tv_message_small_notification, TypedValue.COMPLEX_UNIT_SP, 14);
        }
        notificationLayout.setTextViewText(R.id.tv_message_small_notification, message);
        notificationLayout.setTextColor(R.id.tv_message_small_notification, context.getResources().getColor(R.color.notification_message_color));


        //set text for title
        notificationLayoutExpanded.setTextViewText(R.id.tv_title, title);
        notificationLayoutExpanded.setTextColor(R.id.tv_title, context.getResources().getColor(R.color.notification_title_color));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationLayoutExpanded.setTextViewTextSize(R.id.tv_title, TypedValue.COMPLEX_UNIT_SP, 14);
        }

        //set message for message content
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            notificationLayoutExpanded.setTextViewTextSize(R.id.tv_message, TypedValue.COMPLEX_UNIT_SP, 14);
        }
        notificationLayoutExpanded.setTextViewText(R.id.tv_message, message);
        notificationLayoutExpanded.setTextColor(R.id.tv_message, context.getResources().getColor(R.color.notification_message_color));


       alamNotificationBuilder.setCustomContentView(notificationLayout);
       alamNotificationBuilder.setCustomBigContentView(notificationLayoutExpanded);


        //check weather the device has call feature by default or not and act accordingly
        Intent calIntent = new Intent(Intent.ACTION_DIAL);
        calIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PackageManager packageManager = context.getPackageManager();
        if (mobileNumber.length() != 0 && type.equals(CALL) && calIntent.resolveActivity(packageManager) != null) {
            notificationLayoutExpanded.setViewVisibility(R.id.tv_call, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_call, callPendingIntent(activityData.getPhone(), activityData));

            alamNotificationBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_mm_notification_call_android));
            alamNotificationBuilder.setContentIntent(callPendingIntent(activityData.getPhone(), activityData));
        } else {
            notificationLayoutExpanded.setViewVisibility(R.id.tv_call, View.GONE);
            alamNotificationBuilder.setContentIntent(detailsPendingIntent(activityData));
        }
        if (mmSettingsPreferences.getBooleanValue(REPEATS_SWITCH)) {
            notificationLayoutExpanded.setViewVisibility(R.id.tv_snooze, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_snooze, snoozePendingIntent(activityData));
        }
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_cancel, closePendingIntent());

        //mark to complete for calls and meetings
        if (verifyMarkAsCompleteFeature(activityData) == 1 || verifyMarkAsCompleteFeature(activityData) == 5) {
            notificationLayoutExpanded.setViewVisibility(R.id.tv_mark, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_mark, markAsCompletePendingIntent(activityData));
        }

       /* //delete for calls and meetings
        if (verifyMarkAsCompleteFeature(activityData) == 3) {
            notificationLayoutExpanded.setViewVisibility(R.id.tv_delete, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_delete, deleteEvent(activityData));

        }*/

        if (verifyMarkAsCompleteFeature(activityData) == 6) {
           /* notificationLayoutExpanded.setViewVisibility(R.id.tv_unmark, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_unmark, unmarkPendingIntent(activityData));
*/
            notificationLayoutExpanded.setViewVisibility(R.id.tv_delete, View.VISIBLE);
            notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_delete, deleteEvent(activityData));
        }



      /*  // Apply the layouts to the notification
        notificationLayoutExpanded.setViewVisibility(R.id.tv_call, View.VISIBLE);
        Intent i1 = new Intent(NotificationActivity.this, NotificationTargetActivity.class);
        i1.setAction("startNewActivity1");
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_call, PendingIntent.getActivity(getApplicationContext(), 1, i1, 0));
        //  startActivity(i1);
        notificationLayoutExpanded.setViewVisibility(R.id.tv_cancel, View.VISIBLE);
        Intent i2 = new Intent(NotificationActivity.this, NotificationTargetActivity.class);
        i2.setAction("startNewActivity2");
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_cancel, PendingIntent.getActivity(getApplicationContext(), 2, i2, 0));
        //  startActivity(i2);
        notificationLayoutExpanded.setViewVisibility(R.id.tv_snooze, View.VISIBLE);
        Intent i3 = new Intent(NotificationActivity.this, NotificationTargetActivity.class);
        i3.setAction("startNewActivity3");
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_snooze, PendingIntent.getActivity(getApplicationContext(), 3, i3, 0));
        //  startActivity(i3);
        notificationLayoutExpanded.setViewVisibility(R.id.tv_mark, View.VISIBLE);
        Intent i4 = new Intent(NotificationActivity.this, NotificationTargetActivity.class);
        i4.setAction("startNewActivity4");
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_mark, PendingIntent.getActivity(getApplicationContext(), 4, i4, 0));
        // startActivity(i4);
        notificationLayoutExpanded.setViewVisibility(R.id.tv_delete, View.VISIBLE);
        Intent i5 = new Intent(NotificationActivity.this, NotificationTargetActivity.class);
        i5.setAction("startNewActivity5");
        notificationLayoutExpanded.setOnClickPendingIntent(R.id.tv_delete, PendingIntent.getActivity(getApplicationContext(), 5, i5, 0));
        //  startActivity(i5);
*/


        getNotificationManager().notify(notificationId, alamNotificationBuilder.build());

    }


    private PendingIntent closePendingIntent() {
        Intent closeIntent = getRequiredIntent(notificationId);
        closeIntent.setAction(ACTION_CLOSE);
        int randomClosePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomClosePendingIntent, closeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent forceClosePendingIntent() {
        Intent forcecloseIntent = getRequiredIntent(notificationId);
        forcecloseIntent.setAction(ACTION_NOTIFICATION_FORCE_CLOSE);
        int randomForceClosePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomForceClosePendingIntent, forcecloseIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    private PendingIntent detailsPendingIntent(ActivityData activityData) {
        Intent gotoInsidePageIntent = getRequiredIntent(notificationId);
        gotoInsidePageIntent.setAction(ACTION_DETAILS);
        gotoInsidePageIntent.putExtra(EVENT_DATA, activityData);
        int randomDetailsPendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomDetailsPendingIntent, gotoInsidePageIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent callPendingIntent(String mobileNumber, ActivityData activityData) {
        Intent callIntent = getRequiredIntent(notificationId);
        callIntent.setAction(ACTION_CALL);
        callIntent.putExtra(PHONE_NUMBER, mobileNumber);

        callIntent.putExtra(EVENT_DATA, activityData);
        int randomCallPendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomCallPendingIntent, callIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent snoozePendingIntent(ActivityData activityData) {
        Intent snoozeIntent = getRequiredIntent(notificationId);
        snoozeIntent.setAction(ACTION_SNOOZE);
        snoozeIntent.putExtra(EVENT_DATA, activityData);
        snoozeIntent.putExtra(NOTIFICATION_TYPE, activityData.getType());
        int randomSnoozePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomSnoozePendingIntent, snoozeIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    private PendingIntent markAsCompletePendingIntent(ActivityData activityData) {
        Intent markIntent = getRequiredIntent(notificationId);
        markIntent.setAction(ACTION_MARK);
        markIntent.putExtra(EVENT_DATA, activityData);
        markIntent.putExtra(NOTIFICATION_TYPE, activityData.getType());
        int randomSnoozePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomSnoozePendingIntent, markIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }


    private PendingIntent unmarkPendingIntent(ActivityData activityData) {
        Intent unMarkIntent = getRequiredIntent(notificationId);
        unMarkIntent.setAction(ACTION_UNMARK);
        unMarkIntent.putExtra(EVENT_DATA, activityData);
        unMarkIntent.putExtra(NOTIFICATION_TYPE, activityData.getType());
        int randomSnoozePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomSnoozePendingIntent, unMarkIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    private PendingIntent deleteEvent(ActivityData activityData) {
        Intent deleteIntent = getRequiredIntent(notificationId);
        deleteIntent.setAction(ACTION_EVENT_DELETE);
        deleteIntent.putExtra(EVENT_DATA, activityData);
        deleteIntent.putExtra(NOTIFICATION_TYPE, activityData.getType());
        int randomSnoozePendingIntent = new Random().nextInt(150) * 37;
        return PendingIntent.getBroadcast(context, randomSnoozePendingIntent, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private int verifyMarkAsCompleteFeature(ActivityData activityData) {
        SwipeActivityReturnType swipeActivityReturnType = new SwipeActivityReturnType();
        return swipeActivityReturnType.getItemViewTypeForSwiping(activityData);
    }


    private Intent getRequiredIntent(int notification_id) {
        Intent intent = new Intent(context, NotificationActionsReceiver.class);
        intent.putExtra(NOTIFICATION_ID, notification_id);
        return intent;
    }

}
