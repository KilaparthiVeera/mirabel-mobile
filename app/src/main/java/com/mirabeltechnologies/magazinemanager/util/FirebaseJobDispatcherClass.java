package com.mirabeltechnologies.magazinemanager.util;

import android.content.Context;
import android.os.Build;

import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.Trigger;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.services.NotificationEventsScheduleJobService;

public class FirebaseJobDispatcherClass {

    private FirebaseJobDispatcher dispatcher;
    Context context;
    private Job fetchNotificationsJob;

    public FirebaseJobDispatcherClass(Context context) {
        this.context = context;
        dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
    }
    public void scheduleFirebaseJobDispatcher() {
        if (fetchNotificationsJob == null) {

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                fetchNotificationsJob = dispatcher.newJobBuilder()
                        // the JobService that will be called
                        .setService(NotificationEventsScheduleJobService.class)
                        // uniquely identifies the job
                        .setTag(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG)
                        // one-off job
                        .setRecurring(true)
                        // don't persist past a device reboot
                        .setLifetime(Lifetime.FOREVER)
                        // start between 10 and 300 seconds from now i.e 0.3 to 5 min
                        .setTrigger(Trigger.executionWindow(30, 300))
                        // don't overwrite an existing job with the same tag
                        //  .setReplaceCurrent(true)
                        .setReplaceCurrent(true)
                        // retry with exponential backoff
                        .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                        // constraints that need to be satisfied for the job to run
                        .setConstraints(Constraint.ON_ANY_NETWORK)
                        .build();
                dispatcher.mustSchedule(fetchNotificationsJob);
            } else {
                fetchNotificationsJob = dispatcher.newJobBuilder()
                        // the JobService that will be called
                        .setService(NotificationEventsScheduleJobService.class)
                        // uniquely identifies the job
                        .setTag(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG)
                        // one-off job
                        .setRecurring(false)
                        // don't persist past a device reboot
                        .setLifetime(Lifetime.FOREVER)
                        // start between 30 and 300 seconds from now i.e 30 sec to 5 min
                        .setTrigger(Trigger.executionWindow(30, 300))
                        // don't overwrite an existing job with the same tag
                        //  .setReplaceCurrent(true)
                        .setReplaceCurrent(true)
                        // retry with exponential backoff
                        .setRetryStrategy(RetryStrategy.DEFAULT_LINEAR)
                        // constraints that need to be satisfied for the job to run
                        .setConstraints(Constraint.ON_ANY_NETWORK)
                        .build();
                dispatcher.mustSchedule(fetchNotificationsJob);


            }
        }
    }


    //cancel all the job dispatchers with the requested tag name.
    public void cancelFirebaseJobDispatcher(String tag) {
        if (dispatcher != null) {
            dispatcher.cancel(tag);
        } else {
            dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
            dispatcher.cancel(tag);
        }
    }

    public void cancelAllScheduledFirebaseJobDispatchers() {
        if (dispatcher != null) {
            dispatcher.cancelAll();
        } else {
            dispatcher = new FirebaseJobDispatcher(new GooglePlayDriver(context));
            dispatcher.cancelAll();
        }
    }


}
