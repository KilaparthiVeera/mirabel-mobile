package com.mirabeltechnologies.magazinemanager.util;

import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
public class SwipeActivityReturnType {


    public int getItemViewTypeForSwiping(ActivityData activityData) {
        boolean showMarkComplete = false;
        boolean showUnMark = false;
        boolean showDelete = false;
        switch (activityData.getType()) {
            case "Call":
                if (activityData.getCanEditNotes() == 0) {
                    showMarkComplete = false;
                } else {
                    if (activityData.getCanEdit() == 0) { // additional rep security configuration
                        if (activityData.getCompleted().equals("False")) {
                            showMarkComplete = true;
                        } else {
                            showMarkComplete = false;

                        }
                    } else {
                        showMarkComplete = false;
                    }
                }
                if (activityData.getCanDeleteNotes() == 0 || activityData.getCanDelete() == 1 || activityData.getCompleted().equals("False")) {
                    showDelete = false;
                } else {
                    showDelete = true;
                }
                break;
            case "Note":
                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    showDelete = false;
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        showDelete = true;
                    else
                        showDelete = false;
                }
                break;
            case "Meeting":
                if (activityData.getCanEditNotes() == 0) { // user account level access
                    showMarkComplete = false;
                } else {
                    if (activityData.getCanEdit() == 0) { // additional rep security configuration
                        if (activityData.getCompleted().equals("False")) {
                            showUnMark = true;
                        } else {
                            //unmark  activity
                            showUnMark = false;
                        }
                        showMarkComplete = true;
                    } else {
                        showMarkComplete = false;
                    }
                }
                if (activityData.getCanDeleteNotes() == 0 || activityData.getCanDelete() == 1 || activityData.getCompleted().equals("False")) {
                    showDelete = false;
                    // to align edit icon as vertically center we used Visibility GONE
                } else {
                    showDelete = true;
                }
                break;
            case "Email":
                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    showDelete = false;
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        showDelete = true;
                    else {
                        showDelete = false;
                    }
                }
                break;
            case "Mass Email":
                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    showDelete = false;
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        showDelete = true;
                    else
                        showDelete = false;
                }
                break;
        }

        if (activityData.getType().equals("Call") || activityData.getType().equals("Note") || activityData.getType().equals("Email") || activityData.getType().equals("Mass Email")) {
            if (!showMarkComplete && !showDelete) {
                return 0;
            } else if (showMarkComplete && !showDelete) {
                return 1;
            } else if (!showMarkComplete && showDelete) {
                return 3;
            } else {
                return 0;
            }
        } else {
            if (!showMarkComplete && !showDelete && !showUnMark) {
                return 0;
            } else if (showDelete && showMarkComplete) {
                return 6;
            } else if (showMarkComplete) {
                return 5;
            } else {
                return 0;
            }
        }


    }
}
