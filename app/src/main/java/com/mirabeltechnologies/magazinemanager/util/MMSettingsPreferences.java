package com.mirabeltechnologies.magazinemanager.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Display;

import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.util.Map;

public class MMSettingsPreferences {


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Context context;

    public MMSettingsPreferences(Context context) {
        this.context = context;
        if (sharedPreferences == null) {
            sharedPreferences = context.getSharedPreferences(Constants.MM_SETTINGS_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        }

    }


    public void putBooleanValue(String keyName, boolean value) {
        this.editor = sharedPreferences.edit();
        editor.putBoolean(keyName, value);
        editor.apply();

    }

    public boolean getBooleanValue(String keyName) {
        return sharedPreferences.getBoolean(keyName, false);
    }


    public void putStringValue(String keyName, String value) {
        this.editor = sharedPreferences.edit();
        editor.putString(keyName, value);
        editor.apply();
    }

    public String getStringValue(String keyName) {

        return sharedPreferences.getString(keyName, null);
    }

    public void putIntegerValue(String keyName, int value) {
        this.editor = sharedPreferences.edit();
        editor.putInt(keyName, value);
        editor.apply();
    }

    public int getIntegerValue(String keyName) {

        return sharedPreferences.getInt(keyName, 0);
    }

    public boolean checkKeyAvailability(String key) {
        Map<String, ?> keys = sharedPreferences.getAll();
        for (Map.Entry<String, ?> entry : keys.entrySet()) {
            if (entry.getKey().equals(key)) {
                return true;
            }

        }
        return false;
    }


    /**
     * Method to clear all the stored data in shared preference
     */
    public void clear() {
        this.editor = this.sharedPreferences.edit();
        this.editor.clear();
        this.editor.commit(); // commit changes
    }
}
