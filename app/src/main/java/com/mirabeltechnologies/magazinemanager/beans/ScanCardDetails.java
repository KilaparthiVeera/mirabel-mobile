package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ScanCardDetails {

    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Domain")
    @Expose
    private List<String> domain = null;
    @SerializedName("Email")
    @Expose
    private List<String> email = null;
    @SerializedName("InputID")
    @Expose
    private String inputID;
    @SerializedName("InputText")
    @Expose
    private String inputText;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Phone")
    @Expose
    private List<String> phone = null;
    @SerializedName("State")
    @Expose
    private String state;

    @SerializedName("State Code")
    @Expose
    private String StateCode;
    @SerializedName("Title")
    @Expose
    private List<String> title = null;
    @SerializedName("ZipCode")
    @Expose
    private List<String> zipCode = null;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public List<String> getDomain() {
        return domain;
    }

    public void setDomain(List<String> domain) {
        this.domain = domain;
    }

    public List<String> getEmail() {
        return email;
    }

    public void setEmail(List<String> email) {
        this.email = email;
    }

    public String getInputID() {
        return inputID;
    }

    public void setInputID(String inputID) {
        this.inputID = inputID;
    }

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPhone() {
        return phone;
    }

    public void setPhone(List<String> phone) {
        this.phone = phone;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<String> getTitle() {
        return title;
    }

    public void setTitle(List<String> title) {
        this.title = title;
    }

    public List<String> getZipCode() {
        return zipCode;
    }

    public void setZipCode(List<String> zipCode) {
        this.zipCode = zipCode;
    }
    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    @Override
    public String toString() {
        return "ScanCardDetails{" +
                "city='" + city + '\'' +
                ", companyName='" + companyName + '\'' +
                ", country='" + country + '\'' +
                ", domain=" + domain +
                ", email=" + email +
                ", inputID='" + inputID + '\'' +
                ", inputText='" + inputText + '\'' +
                ", name='" + name + '\'' +
                ", phone=" + phone +
                ", state='" + state + '\'' +
                ", title=" + title +
                ", zipCode=" + zipCode +
                '}';
    }
}
