package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 10/19/17.
 */

public class ActivityDetails implements Serializable {
    @SerializedName("CustomerID")
    @Expose
    private String customerId;
    @SerializedName("ActivityOrTaskID")
    @Expose
    private String activityOrTaskId;
    @SerializedName("TaskNameOrNotes")
    @Expose
    private String taskNameOrNotes;
    @SerializedName("ActivityType")
    @Expose
    private String activityType;
    @SerializedName("DateTime")
    @Expose
    private String dateTime;
    @Expose
    private String formatDate="";
    @Expose
    private String modifiedNotes;
    @Expose
    private boolean hasHTMLTags;

    public String getFormatDate() {
        return formatDate;
    }

    public void setFormatDate(String formatDate) {
        this.formatDate = formatDate;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getActivityOrTaskId() {
        return activityOrTaskId;
    }

    public void setActivityOrTaskId(String activityOrTaskId) {
        this.activityOrTaskId = activityOrTaskId;
    }

    public String getTaskNameOrNotes() {
        return taskNameOrNotes;
    }

    public void setTaskNameOrNotes(String taskNameOrNotes) {
        this.taskNameOrNotes = taskNameOrNotes;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getModifiedNotes() {
        return modifiedNotes;
    }

    public void setModifiedNotes(String modifiedNotes) {
        this.modifiedNotes = modifiedNotes;
    }

    public boolean isHasHTMLTags() {
        return hasHTMLTags;
    }

    public void setHasHTMLTags(boolean hasHTMLTags) {
        this.hasHTMLTags = hasHTMLTags;
    }

    @NonNull
    @Override
    public String toString() {
        return "ActivityDetails{" +
                "customerId='" + customerId + '\'' +
                ", activityOrTaskId='" + activityOrTaskId + '\'' +
                ", taskNameOrNotes='" + taskNameOrNotes + '\'' +
                ", activityType='" + activityType + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", formatDate='" + formatDate + '\'' +
                ", modifiedNotes='" + modifiedNotes + '\'' +
                ", hasHTMLTags=" + hasHTMLTags +
                '}';
    }
}
