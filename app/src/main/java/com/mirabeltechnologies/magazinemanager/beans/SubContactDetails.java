package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SubContactDetails implements Serializable {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private Object firstName;
    @SerializedName("MiddleName")
    @Expose
    private Object middleName;
    @SerializedName("LastName")
    @Expose
    private Object lastName;
    @SerializedName("SalesRepID")
    @Expose
    private Integer salesRepID;
    @SerializedName("ContactFullName")
    @Expose
    private String contactFullName;
    @SerializedName("FullNameWithCompany")
    @Expose
    private String fullNameWithCompany;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("Phone")
    @Expose
    private Object phone;
    @SerializedName("PhoneExt")
    @Expose
    private Object phoneExt;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("CellPhone")
    @Expose
    private Object cellPhone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getFirstName() {
        return firstName;
    }

    public void setFirstName(Object firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public Object getLastName() {
        return lastName;
    }

    public void setLastName(Object lastName) {
        this.lastName = lastName;
    }

    public Integer getSalesRepID() {
        return salesRepID;
    }

    public void setSalesRepID(Integer salesRepID) {
        this.salesRepID = salesRepID;
    }

    public String getContactFullName() {
        return contactFullName;
    }

    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }

    public String getFullNameWithCompany() {
        return fullNameWithCompany;
    }

    public void setFullNameWithCompany(String fullNameWithCompany) {
        this.fullNameWithCompany = fullNameWithCompany;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public Object getPhone() {
        return phone;
    }

    public void setPhone(Object phone) {
        this.phone = phone;
    }

    public Object getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(Object phoneExt) {
        this.phoneExt = phoneExt;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public Object getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Object cellPhone) {
        this.cellPhone = cellPhone;
    }

    @Override
    public String toString() {
        return "SubContactDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName=" + firstName +
                ", middleName=" + middleName +
                ", lastName=" + lastName +
                ", salesRepID=" + salesRepID +
                ", contactFullName='" + contactFullName + '\'' +
                ", fullNameWithCompany='" + fullNameWithCompany + '\'' +
                ", isDefault=" + isDefault +
                ", phone=" + phone +
                ", phoneExt=" + phoneExt +
                ", email=" + email +
                ", cellPhone=" + cellPhone +
                '}';
    }
}
