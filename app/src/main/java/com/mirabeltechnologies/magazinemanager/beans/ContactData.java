package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 3/2/18.
 */

public class ContactData implements Serializable {
    @SerializedName("Contacts")
    @Expose
    private String noOfContacts;
    @SerializedName("ContactInfo")
    @Expose
    private String contactInfo;
    @SerializedName("ContactDetails")
    @Expose
    private String contactDetails;
    @SerializedName("CompanyAccountInfo")
    @Expose
    private String companyAccountInfo;
    @SerializedName("ContactGroups")
    @Expose
    private String contactGroups;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("BillingInfo")
    @Expose
    private String billingInfo;
    @SerializedName("FinancialInfo")
    @Expose
    private String financialInfo;
    @SerializedName("Opportunities")
    @Expose
    private String opportunities;
    @SerializedName("Proposals")
    @Expose
    private String proposals;
    @SerializedName("CommunicationInfo")
    @Expose
    private String communicationInfo;
    @SerializedName("Orders")
    @Expose
    private String noOfOrders;
    @SerializedName("BillingampFinancial")
    @Expose
    private String billingampFinancial;
    @SerializedName("CustomFields")
    @Expose
    private String customFields;

    @SerializedName("AdAgencies")
    @Expose
    private String adAgencies;
    @SerializedName("RelatedCustomers")
    @Expose
    private String relatedCustomers;
    @SerializedName("ClientFiles")
    @Expose
    private String clientFiles;
    @SerializedName("Complist")
    @Expose
    private String complist;

    public String getNoOfContacts() {
        return noOfContacts;
    }

    public String getContactInfo() {
        return contactInfo;
    }

    public String getContactDetails() {
        return contactDetails;
    }

    public String getCompanyAccountInfo() {
        return companyAccountInfo;
    }

    public String getAddress() {
        return address;
    }

    public String getBillingInfo() {
        return billingInfo;
    }

    public String getFinancialInfo() {
        return financialInfo;
    }

    public String getOpportunities() {
        return opportunities;
    }

    public String getProposals() {
        return proposals;
    }

    public String getCommunicationInfo() {
        return communicationInfo;
    }

    public String getNoOfOrders() {
        return noOfOrders;
    }

    public String getBillingampFinancial() {
        return billingampFinancial;
    }

    public String getCustomFields() {
        return customFields;
    }

    public String getContactGroups() {
        return contactGroups;
    }

    public String getAdAgencies() {
        return adAgencies;
    }

    public String getRelatedCustomers() {
        return relatedCustomers;
    }

    public String getClientFiles() {
        return clientFiles;
    }

    public String getComplist() {
        return complist;
    }

    @Override
    public String toString() {
        return "ContactData{" +
                "noOfContacts='" + noOfContacts + '\'' +
                ", contactInfo='" + contactInfo + '\'' +
                ", contactDetails='" + contactDetails + '\'' +
                ", companyAccountInfo='" + companyAccountInfo + '\'' +
                ", address='" + address + '\'' +
                ", billingInfo='" + billingInfo + '\'' +
                ", financialInfo='" + financialInfo + '\'' +
                ", opportunities='" + opportunities + '\'' +
                ", proposals='" + proposals + '\'' +
                ", communicationInfo='" + communicationInfo + '\'' +
                ", noOfOrders='" + noOfOrders + '\'' +
                ", billingampFinancial='" + billingampFinancial + '\'' +
                ", customFields='" + customFields + '\'' +
                ", contactGroups='" + contactGroups + '\'' +
                ", adAgencies='" + adAgencies + '\'' +
                ", relatedCustomers='" + relatedCustomers + '\'' +
                ", clientFiles='" + clientFiles + '\'' +
                ", complist='" + complist + '\'' +
                '}';
    }
}
