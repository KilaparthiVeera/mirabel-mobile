package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

/**
 * Created by venkat on 11/15/17.
 */

public class NewContact implements Serializable {
    private int clientId;
    private String customerId;
    private Long parentId;
    private String companyName;
    private String firstName;
    private String lastName;
    private String repId;
    private String repName;
    private String category;
    private String categoryId;
    private String priority;
    private String priorityId;
    private String contactType;
    private String contactTypeId;
    private String jobTitle;
    private String emailId;
    private String phoneNumber;
    private String address1;
    private String address2;
    private String zipCode;
    private String city;
    private String state;
    private String county;
    private String country;
    private String websiteURL;
    private String facebookId;
    private String googlePlusURL;
    private String linkedInURL;
    private String twitterURL;
    private String stateCode;
    private boolean updateSubContacts;

    public NewContact() {
        this.clientId = 0;
        this.customerId = "";
        this.parentId = 0L;
        this.companyName = "";
        this.firstName = "";
        this.lastName = "";
        this.repId = "";
        this.repName = "";
        this.category = "";
        this.categoryId = "0";
        this.priority = "";
        this.priorityId = "0";
        this.contactType = "";
        this.contactTypeId = "0";
        this.jobTitle = "";
        this.emailId = "";
        this.phoneNumber = "";
        this.address1 = "";
        this.address2 = "";
        this.zipCode = "";
        this.city = "";
        this.state = "";
        this.county = "";
        this.country = "";
        this.websiteURL = "";
        this.facebookId = "";
        this.googlePlusURL = "";
        this.linkedInURL = "";
        this.twitterURL = "";
        this.updateSubContacts = false;
    }

    public NewContact(DetailContact contact, boolean isAddingSubContact) {
        this.clientId = 0;
        this.customerId = contact.getCustomersId();
        this.parentId = Long.parseLong(contact.getCustomersId());
        this.companyName = contact.getCompanyName();
        this.repId = String.valueOf(contact.getRepId());
        this.repName = contact.getRepName();
        this.category = contact.getCustomerType();
        this.categoryId = contact.getCustomerTypeId();
        this.priority = contact.getPriorityName();
        this.priorityId = String.valueOf(contact.getPriority());
        this.address1 = contact.getAddress1();
        this.address2 = contact.getAddress2();
        this.zipCode = contact.getZip();
        this.city = contact.getCity();
        this.state = contact.getState();
        this.county = contact.getCounty();
        this.country = contact.getCountry();
        this.websiteURL = contact.getUrl();
        this.updateSubContacts = false;

        if (isAddingSubContact) {
            this.firstName = "";
            this.lastName = "";
            this.contactType = "";
            this.contactTypeId = "0";
            this.jobTitle = "";
            this.emailId = "";
            this.phoneNumber = "";
            this.facebookId = "";
            this.googlePlusURL = "";
            this.linkedInURL = "";
            this.twitterURL = "";
        } else {
            this.firstName = contact.getFirstName();
            this.lastName = contact.getLastName();
            this.contactType = contact.getJobDescription();
            this.contactTypeId = "0";
            this.jobTitle = contact.getTitle();
            this.emailId = contact.getEmail();
            this.phoneNumber = contact.getPhone();
            this.facebookId = contact.getFacebookId();
            this.googlePlusURL = contact.getGooglePlus();
            this.linkedInURL = contact.getLinkedIn();
            this.twitterURL = contact.getTwitterHandle();
        }
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(String priorityId) {
        this.priorityId = priorityId;
    }

    public String getContactType() {
        return contactType;
    }

    public void setContactType(String contactType) {
        this.contactType = contactType;
    }

    public String getContactTypeId() {
        return contactTypeId;
    }

    public void setContactTypeId(String contactTypeId) {
        this.contactTypeId = contactTypeId;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getGooglePlusURL() {
        return googlePlusURL;
    }

    public void setGooglePlusURL(String googlePlusURL) {
        this.googlePlusURL = googlePlusURL;
    }

    public String getLinkedInURL() {
        return linkedInURL;
    }

    public void setLinkedInURL(String linkedInURL) {
        this.linkedInURL = linkedInURL;
    }

    public String getTwitterURL() {
        return twitterURL;
    }

    public void setTwitterURL(String twitterURL) {
        this.twitterURL = twitterURL;
    }

    public boolean isUpdateSubContacts() {
        return updateSubContacts;
    }

    public void setUpdateSubContacts(boolean updateSubContacts) {
        this.updateSubContacts = updateSubContacts;
    }

    @Override
    public String toString() {
        return "NewContact{" +
                "clientId=" + clientId +
                ", customerId='" + customerId + '\'' +
                ", parentId='" + parentId + '\'' +
                ", companyName='" + companyName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", repId='" + repId + '\'' +
                ", repName='" + repName + '\'' +
                ", category='" + category + '\'' +
                ", categoryId='" + categoryId + '\'' +
                ", priority='" + priority + '\'' +
                ", priorityId='" + priorityId + '\'' +
                ", contactType='" + contactType + '\'' +
                ", contactTypeId='" + contactTypeId + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", emailId='" + emailId + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", county='" + county + '\'' +
                ", country='" + country + '\'' +
                ", websiteURL='" + websiteURL + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", googlePlusURL='" + googlePlusURL + '\'' +
                ", linkedInURL='" + linkedInURL + '\'' +
                ", twitterURL='" + twitterURL + '\'' +
                ", updateSubContacts='" + updateSubContacts + '\'' +
                '}';
    }
}
