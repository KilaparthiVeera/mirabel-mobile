package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 10/9/17.
 */

public class Contact implements Serializable {

    @SerializedName("CustomerID")
    @Expose
    private String customerId;
    @SerializedName("CustomersID") // Sub Contact Customer Id
    @Expose
    private String customersId;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("ContactName")
    @Expose
    private String contactName;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneExtn")
    @Expose
    private String phoneExtn;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("RepName")
    @Expose
    private String repName;
    @SerializedName("Priority")
    @Expose
    private String priority;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ParentID")
    @Expose
    private String parentId;
    @SerializedName("CanViewEmployeeID")
    @Expose
    private String canViewEmployeeId; // 0 - No Access, 1 - Read & Write Access, 2 - Read Access Only
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ImageName")
    @Expose
    private String imageName;
    @SerializedName("JobDescription")
    @Expose
    private String jobDescription;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomersId() {
        return customersId;
    }

    public void setCustomersId(String customersId) {
        this.customersId = customersId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneExtn() {
        return phoneExtn;
    }

    public void setPhoneExtn(String phoneExtn) {
        this.phoneExtn = phoneExtn;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCanViewEmployeeId() {
        return canViewEmployeeId;
    }

    public void setCanViewEmployeeId(String canViewEmployeeId) {
        this.canViewEmployeeId = canViewEmployeeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "customerId='" + customerId + '\'' +
                ", customersId='" + customersId + '\'' +
                ", companyName='" + companyName + '\'' +
                ", contactName='" + contactName + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", city='" + city + '\'' +
                ", repName='" + repName + '\'' +
                ", priority='" + priority + '\'' +
                ", email='" + email + '\'' +
                ", parentId='" + parentId + '\'' +
                ", canViewEmployeeId='" + canViewEmployeeId + '\'' +
                ", title='" + title + '\'' +
                ", imageName='" + imageName + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                '}';
    }
}
