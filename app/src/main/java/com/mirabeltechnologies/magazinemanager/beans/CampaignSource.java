package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CampaignSource {

    @SerializedName("Source")
    @Expose
    private String source;

    private Boolean isSelected=false;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    @NonNull
    @Override
    public String toString() {
        return "CampaignSource{" +
                "source='" + source + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
