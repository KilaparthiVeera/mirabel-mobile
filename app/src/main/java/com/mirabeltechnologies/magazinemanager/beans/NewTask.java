package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

/**
 * Created by venkat on 3/21/18.
 */

public class NewTask implements Serializable {
    private int taskId;
    private String taskName;
    private String description;
    private int assignedToRepId;
    private String assignedToRepName;
    private int assignedByRepId;
    private String assignedByRepName;
    private int priorityId;
    private String priority;
    private int projectId;
    private String project;
    private String dueDate;
    private boolean isPrivate;

    public NewTask() {
        this.taskId = 0;
        this.taskName = "";
        this.description = "";
        this.assignedToRepId = -1;
        this.assignedToRepName = "";
        this.assignedByRepId = -1;
        this.assignedByRepName = "";
        this.priorityId = -1;
        this.priority = "";
        this.projectId = -1;
        this.project = "";
        this.dueDate = "";
        this.isPrivate = false;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAssignedToRepId() {
        return assignedToRepId;
    }

    public void setAssignedToRepId(int assignedToRepId) {
        this.assignedToRepId = assignedToRepId;
    }

    public String getAssignedToRepName() {
        return assignedToRepName;
    }

    public void setAssignedToRepName(String assignedToRepName) {
        this.assignedToRepName = assignedToRepName;
    }

    public int getAssignedByRepId() {
        return assignedByRepId;
    }

    public void setAssignedByRepId(int assignedByRepId) {
        this.assignedByRepId = assignedByRepId;
    }

    public String getAssignedByRepName() {
        return assignedByRepName;
    }

    public void setAssignedByRepName(String assignedByRepName) {
        this.assignedByRepName = assignedByRepName;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    @Override
    public String toString() {
        return "NewTask{" +
                "taskId=" + taskId +
                ", taskName='" + taskName + '\'' +
                ", description='" + description + '\'' +
                ", assignedToRepId=" + assignedToRepId +
                ", assignedToRepName='" + assignedToRepName + '\'' +
                ", assignedByRepId=" + assignedByRepId +
                ", assignedByRepName='" + assignedByRepName + '\'' +
                ", priorityId=" + priorityId +
                ", priority='" + priority + '\'' +
                ", projectId=" + projectId +
                ", project='" + project + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", isPrivate=" + isPrivate +
                '}';
    }
}
