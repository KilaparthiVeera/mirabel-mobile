package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SaveList {

    @SerializedName("Value")
    @Expose
    private String value;
    @SerializedName("Display")
    @Expose
    private String display;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    @Override
    public String toString() {
        return "SaveList{" +
                "value='" + value + '\'' +
                ", display='" + display + '\'' +
                '}';
    }
}
