package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OpportunitiesSearchFilter implements Serializable {

    @SerializedName("CustomerID")
    @Expose
    private String customerID;

    private String repName;
    private String repId;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("OppName")
    @Expose
    private String oppName;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("BusinessUnit")
    @Expose
    private String businessUnit;
    @SerializedName("Source")
    @Expose
    private String source;
    @SerializedName("Products")
    @Expose
    private String products;
    @SerializedName("LossReason")
    @Expose
    private String lossReason;
    @SerializedName("AssignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("Arth")
    @Expose
    private String arth;
    @SerializedName("Stage")
    @Expose
    private String stage;
    @SerializedName("CreatedBy")
    @Expose
    private String createdBy;
    @SerializedName("CreatedFrom")
    @Expose
    private String createdFrom;
    @SerializedName("CreatedTo")
    @Expose
    private String createdTo;
    @SerializedName("CloseFrom")
    @Expose
    private String closeFrom;
    @SerializedName("CloseTo")
    @Expose
    private String closeTo;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("UserID")
    @Expose
    private Integer userID;
    @SerializedName("Probability")
    @Expose
    private String probability;

    private String probabilityName;
    private String probabilityId;

    @SerializedName("ListId")
    @Expose
    private Integer listId;
    @SerializedName("Action")
    @Expose
    private Object action;
    @SerializedName("PageSize")
    @Expose
    private Integer pageSize;
    @SerializedName("CurPage")
    @Expose
    private Integer curPage;
    @SerializedName("SortBy")
    @Expose
    private String sortBy;
    @SerializedName("ViewType")
    @Expose
    private Integer viewType;
    @SerializedName("AdvSearch")
    @Expose
    private OpportunityAdvSearch advSearch;


    public String getProbabilityId() {
        return probabilityId;
    }

    public void setProbabilityId(String probabilityId) {
        this.probabilityId = probabilityId;
    }

    public String getProbabilityName() {
        return probabilityName;
    }

    public void setProbabilityName(String probabilityName) {
        this.probabilityName = probabilityName;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getOppName() {
        return oppName;
    }

    public void setOppName(String oppName) {
        this.oppName = oppName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getProducts() {
        return products;
    }

    public void setProducts(String products) {
        this.products = products;
    }

    public String getLossReason() {
        return lossReason;
    }

    public void setLossReason(String lossReason) {
        this.lossReason = lossReason;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getArth() {
        return arth;
    }

    public void setArth(String arth) {
        this.arth = arth;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getCreatedFrom() {
        return createdFrom;
    }

    public void setCreatedFrom(String createdFrom) {
        this.createdFrom = createdFrom;
    }

    public String getCreatedTo() {
        return createdTo;
    }

    public void setCreatedTo(String createdTo) {
        this.createdTo = createdTo;
    }

    public String getCloseFrom() {
        return closeFrom;
    }

    public void setCloseFrom(String closeFrom) {
        this.closeFrom = closeFrom;
    }

    public String getCloseTo() {
        return closeTo;
    }

    public void setCloseTo(String closeTo) {
        this.closeTo = closeTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getProbability() {
        return probability;
    }

    public void setProbability(String probability) {
        this.probability = probability;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }

    public Object getAction() {
        return action;
    }

    public void setAction(Object action) {
        this.action = action;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getCurPage() {
        return curPage;
    }

    public void setCurPage(Integer curPage) {
        this.curPage = curPage;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public Integer getViewType() {
        return viewType;
    }

    public void setViewType(Integer viewType) {
        this.viewType = viewType;
    }

    public OpportunityAdvSearch getAdvSearch() {
        return advSearch;
    }

    public void setAdvSearch(OpportunityAdvSearch advSearch) {
        this.advSearch = advSearch;
    }

    @NonNull
    @Override
    public String toString() {
        return "OpportunitiesSearchFilter{" +
                "customerID='" + customerID + '\'' +
                ", repName='" + repName + '\'' +
                ", customerName='" + customerName + '\'' +
                ", oppName='" + oppName + '\'' +
                ", type='" + type + '\'' +
                ", businessUnit='" + businessUnit + '\'' +
                ", source='" + source + '\'' +
                ", products='" + products + '\'' +
                ", lossReason='" + lossReason + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", arth='" + arth + '\'' +
                ", stage='" + stage + '\'' +
                ", createdBy='" + createdBy + '\'' +
                ", createdFrom='" + createdFrom + '\'' +
                ", createdTo='" + createdTo + '\'' +
                ", closeFrom='" + closeFrom + '\'' +
                ", closeTo='" + closeTo + '\'' +
                ", status='" + status + '\'' +
                ", userID=" + userID +
                ", probability='" + probability + '\'' +
                ", listId=" + listId +
                ", action=" + action +
                ", pageSize=" + pageSize +
                ", curPage=" + curPage +
                ", sortBy='" + sortBy + '\'' +
                ", viewType=" + viewType +
                ", advSearch=" + advSearch +
                '}';
    }
}
