package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 3/16/18.
 */

public class Task implements Serializable {
    @SerializedName("AssignedTo")
    @Expose
    private String assignedToRepName;
    @SerializedName("AssociatedProject")
    @Expose
    private int projectId;
    @SerializedName("CanViewEmployeeID")
    @Expose
    private int canViewEmployeeId;
    @SerializedName("Completed")
    @Expose
    private int completed;
    @SerializedName("CreatedBy")
    @Expose
    private String assignedByRepName;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("DueDate")
    @Expose
    private String dueDate;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Priority")
    @Expose
    private String priority;
    @SerializedName("PriorityId")
    @Expose
    private int priorityId;
    @SerializedName("PrivateNotes")
    @Expose
    private boolean privateNotes;
    @SerializedName("Project")
    @Expose
    private String projectName;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("TaskId")
    @Expose
    private long taskId;
    @SerializedName("gsEmpID_Assigned")
    @Expose
    private long assignedToRepId;
    @SerializedName("gsEmpID_Created")
    @Expose
    private long assignedByRepId;
    @Expose
    private String modifiedDescription;
    @Expose
    private boolean hasHTMLTags;

    public String getAssignedToRepName() {
        return assignedToRepName;
    }

    public void setAssignedToRepName(String assignedToRepName) {
        this.assignedToRepName = assignedToRepName;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getCanViewEmployeeId() {
        return canViewEmployeeId;
    }

    public void setCanViewEmployeeId(int canViewEmployeeId) {
        this.canViewEmployeeId = canViewEmployeeId;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public String getAssignedByRepName() {
        return assignedByRepName;
    }

    public void setAssignedByRepName(String assignedByRepName) {
        this.assignedByRepName = assignedByRepName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    public boolean isPrivateNotes() {
        return privateNotes;
    }

    public void setPrivateNotes(boolean privateNotes) {
        this.privateNotes = privateNotes;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getAssignedToRepId() {
        return assignedToRepId;
    }

    public void setAssignedToRepId(long assignedToRepId) {
        this.assignedToRepId = assignedToRepId;
    }

    public long getAssignedByRepId() {
        return assignedByRepId;
    }

    public void setAssignedByRepId(long assignedByRepId) {
        this.assignedByRepId = assignedByRepId;
    }

    public String getModifiedDescription() {
        return modifiedDescription;
    }

    public void setModifiedDescription(String modifiedDescription) {
        this.modifiedDescription = modifiedDescription;
    }

    public boolean isHasHTMLTags() {
        return hasHTMLTags;
    }

    public void setHasHTMLTags(boolean hasHTMLTags) {
        this.hasHTMLTags = hasHTMLTags;
    }

    @Override
    public String toString() {
        return "Task{" +
                "assignedToRepName='" + assignedToRepName + '\'' +
                ", projectId=" + projectId +
                ", canViewEmployeeId=" + canViewEmployeeId +
                ", completed=" + completed +
                ", assignedByRepName='" + assignedByRepName + '\'' +
                ", description='" + description + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", name='" + name + '\'' +
                ", priority='" + priority + '\'' +
                ", priorityId=" + priorityId +
                ", privateNotes=" + privateNotes +
                ", projectName='" + projectName + '\'' +
                ", startDate='" + startDate + '\'' +
                ", taskId=" + taskId +
                ", assignedToRepId=" + assignedToRepId +
                ", assignedByRepId=" + assignedByRepId +
                ", modifiedDescription='" + modifiedDescription + '\'' +
                ", hasHTMLTags=" + hasHTMLTags +
                '}';
    }
}
