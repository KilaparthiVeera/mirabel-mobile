package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactDetails {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private Object middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("SalesRepID")
    @Expose
    private Integer salesRepID;
    @SerializedName("ContactFullName")
    @Expose
    private String contactFullName;
    @SerializedName("FullNameWithCompany")
    @Expose
    private String fullNameWithCompany;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneExt")
    @Expose
    private String phoneExt;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("CellPhone")
    @Expose
    private String cellPhone;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Object getMiddleName() {
        return middleName;
    }

    public void setMiddleName(Object middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSalesRepID() {
        return salesRepID;
    }

    public void setSalesRepID(Integer salesRepID) {
        this.salesRepID = salesRepID;
    }

    public String getContactFullName() {
        return contactFullName;
    }

    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }

    public String getFullNameWithCompany() {
        return fullNameWithCompany;
    }

    public void setFullNameWithCompany(String fullNameWithCompany) {
        this.fullNameWithCompany = fullNameWithCompany;
    }

    public Boolean getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }


    @Override
    public String toString() {
        return "ContactDetails{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName=" + middleName +
                ", lastName='" + lastName + '\'' +
                ", salesRepID=" + salesRepID +
                ", contactFullName='" + contactFullName + '\'' +
                ", fullNameWithCompany='" + fullNameWithCompany + '\'' +
                ", isDefault=" + isDefault +
                ", phone='" + phone + '\'' +
                ", phoneExt='" + phoneExt + '\'' +
                ", email='" + email + '\'' +
                ", cellPhone='" + cellPhone + '\'' +
                '}';
    }
}
