package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 1/2/18.
 */

public class ActivityData implements Serializable {
    @SerializedName("AccountExec")
    @Expose
    private String accountExec;
    @SerializedName("ActivityID")
    @Expose
    private Long activityId;
    @SerializedName("AssignedBy")
    @Expose
    private String assignedBy;
    @SerializedName("AssignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("CalendarEventID")
    @Expose
    private String calendarEventId;
    @SerializedName("Callback")
    @Expose
    private String callback;
    @SerializedName("CanDelete")
    @Expose
    private int canDelete;// this is based on additional rep security configured in web.
    @SerializedName("CanEdit")
    @Expose
    private int canEdit;// this is based on additional rep security configured in web.
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Completed")
    @Expose
    private String completed;
    @SerializedName("CustomerID")
    @Expose
    private Long customerId;
    @SerializedName("CustomerIcon")
    @Expose
    private String customerIcon;
    @SerializedName("CustomerType")
    @Expose
    private String customerType;
    @SerializedName("DateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("DateCompleted")
    @Expose
    private String dateCompleted;
    @SerializedName("DateScheduled")
    @Expose
    private String dateScheduled;
    @SerializedName("DateStarted")
    @Expose
    private String dateStarted;
    @SerializedName("Day")
    @Expose
    private int day;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("EmployeeID")
    @Expose
    private Long employeeId;
    @SerializedName("Fax")
    @Expose
    private String fax;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("IsAgency")
    @Expose
    private Long isAgency;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Meeting")
    @Expose
    private String meeting;
    @SerializedName("MeetingName")
    @Expose
    private String meetingName;
    @SerializedName("MeetingType")
    @Expose
    private String meetingType;
    @SerializedName("Month")
    @Expose
    private int month;
    @SerializedName("ParentID")
    @Expose
    private Long parentId;
    @SerializedName("PastOrFuture")
    @Expose
    private String pastOrFuture;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("SpecialNotes")
    @Expose
    private String specialNotes;
    @SerializedName("St")
    @Expose
    private String state;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("Street2")
    @Expose
    private String street2;
    @SerializedName("Total")
    @Expose
    private int total;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Year")
    @Expose
    private int year;
    @SerializedName("Zip")
    @Expose
    private String zip;
    private int canDeleteNotes; // this is user account level i.e. we can modify this in user account in web.
    private int canEditNotes; // this is user account level i.e. we can modify this in user account in web.
    @Expose
    private String modifiedNotes;
    @Expose
    private boolean hasHTMLTags;
    @Expose
    private String fullNotes;

    public String getAccountExec() {
        return accountExec;
    }

    public void setAccountExec(String accountExec) {
        this.accountExec = accountExec;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public String getAssignedBy() {
        return assignedBy;
    }

    public void setAssignedBy(String assignedBy) {
        this.assignedBy = assignedBy;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getCalendarEventId() {
        return calendarEventId;
    }

    public void setCalendarEventId(String calendarEventId) {
        this.calendarEventId = calendarEventId;
    }

    public String getCallback() {
        return callback;
    }

    public void setCallback(String callback) {
        this.callback = callback;
    }

    public int getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(int canDelete) {
        this.canDelete = canDelete;
    }

    public int getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(int canEdit) {
        this.canEdit = canEdit;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompleted() {
        return completed;
    }

    public void setCompleted(String completed) {
        this.completed = completed;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerIcon() {
        return customerIcon;
    }

    public void setCustomerIcon(String customerIcon) {
        this.customerIcon = customerIcon;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateCompleted() {
        return dateCompleted;
    }

    public void setDateCompleted(String dateCompleted) {
        this.dateCompleted = dateCompleted;
    }

    public String getDateScheduled() {
        return dateScheduled;
    }

    public void setDateScheduled(String dateScheduled) {
        this.dateScheduled = dateScheduled;
    }

    public String getDateStarted() {
        return dateStarted;
    }

    public void setDateStarted(String dateStarted) {
        this.dateStarted = dateStarted;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public Long getIsAgency() {
        return isAgency;
    }

    public void setIsAgency(Long isAgency) {
        this.isAgency = isAgency;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMeeting() {
        return meeting;
    }

    public void setMeeting(String meeting) {
        this.meeting = meeting;
    }

    public String getMeetingName() {
        return meetingName;
    }

    public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getPastOrFuture() {
        return pastOrFuture;
    }

    public void setPastOrFuture(String pastOrFuture) {
        this.pastOrFuture = pastOrFuture;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSpecialNotes() {
        return specialNotes;
    }

    public void setSpecialNotes(String specialNotes) {
        this.specialNotes = specialNotes;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getStreet2() {
        return street2;
    }

    public void setStreet2(String street2) {
        this.street2 = street2;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public int getCanDeleteNotes() {
        return canDeleteNotes;
    }

    public void setCanDeleteNotes(int canDeleteNotes) {
        this.canDeleteNotes = canDeleteNotes;
    }

    public int getCanEditNotes() {
        return canEditNotes;
    }

    public void setCanEditNotes(int canEditNotes) {
        this.canEditNotes = canEditNotes;
    }

    public String getModifiedNotes() {
        return modifiedNotes;
    }

    public void setModifiedNotes(String modifiedNotes) {
        this.modifiedNotes = modifiedNotes;
    }

    public boolean isHasHTMLTags() {
        return hasHTMLTags;
    }

    public void setHasHTMLTags(boolean hasHTMLTags) {
        this.hasHTMLTags = hasHTMLTags;
    }

    public String getFullNotes() {
        return fullNotes;
    }

    public void setFullNotes(String fullNotes) {
        this.fullNotes = fullNotes;
    }

    @Override
    public String toString() {
        return "ActivityData{" +
                "accountExec='" + accountExec + '\'' +
                ", activityId=" + activityId +
                ", assignedBy='" + assignedBy + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", calendarEventId='" + calendarEventId + '\'' +
                ", callback='" + callback + '\'' +
                ", canDelete=" + canDelete +
                ", canEdit=" + canEdit +
                ", city='" + city + '\'' +
                ", companyName='" + companyName + '\'' +
                ", completed='" + completed + '\'' +
                ", customerId=" + customerId +
                ", customerIcon='" + customerIcon + '\'' +
                ", customerType='" + customerType + '\'' +
                ", dateAdded='" + dateAdded + '\'' +
                ", dateCompleted='" + dateCompleted + '\'' +
                ", dateScheduled='" + dateScheduled + '\'' +
                ", dateStarted='" + dateStarted + '\'' +
                ", day=" + day +
                ", email='" + email + '\'' +
                ", employeeId=" + employeeId +
                ", fax='" + fax + '\'' +
                ", firstName='" + firstName + '\'' +
                ", isAgency=" + isAgency +
                ", lastName='" + lastName + '\'' +
                ", meeting='" + meeting + '\'' +
                ", meetingName='" + meetingName + '\'' +
                ", meetingType='" + meetingType + '\'' +
                ", month=" + month +
                ", parentId=" + parentId +
                ", pastOrFuture='" + pastOrFuture + '\'' +
                ", phone='" + phone + '\'' +
                ", specialNotes='" + specialNotes + '\'' +
                ", state='" + state + '\'' +
                ", street='" + street + '\'' +
                ", street2='" + street2 + '\'' +
                ", total=" + total +
                ", type='" + type + '\'' +
                ", year=" + year +
                ", zip='" + zip + '\'' +
                ", canDeleteNotes=" + canDeleteNotes +
                ", canEditNotes=" + canEditNotes +
                ", modifiedNotes='" + modifiedNotes + '\'' +
                ", hasHTMLTags=" + hasHTMLTags +
                ", fullNotes='" + fullNotes + '\'' +
                '}';
    }
}
