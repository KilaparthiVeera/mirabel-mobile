package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 11/6/17.
 */

public class CompanyData implements Serializable {

    @SerializedName("Display")
    @Expose
    private String name;
    @SerializedName("Value")
    @Expose
    private String id;

    private Boolean isSelected=true;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NonNull
    @Override
    public String toString() {
        return "CompanyData{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
