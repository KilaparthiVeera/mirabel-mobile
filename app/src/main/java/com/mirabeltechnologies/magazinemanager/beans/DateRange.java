package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

public class DateRange implements Serializable {
    private int index;
    private String value;

    public DateRange(int index, String value) {
        this.index = index;
        this.value = value;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "DateRange{" +
                "index=" + index +
                ", value='" + value + '\'' +
                '}';
    }
}
