package com.mirabeltechnologies.magazinemanager.beans;

public class Post_BusinessCard {
    String inputText;
    String inputid;

    public String getInputText() {
        return inputText;
    }

    public void setInputText(String inputText) {
        this.inputText = inputText;
    }

    public String getInputid() {
        return inputid;
    }

    public void setInputid(String inputid) {
        this.inputid = inputid;
    }

    @Override
    public String toString() {
        return "Post_BusinessCard{" +
                "inputText='" + inputText + '\'' +
                ", inputid='" + inputid + '\'' +
                '}';
    }
}
