package com.mirabeltechnologies.magazinemanager.beans;

import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 10/11/17.
 */

public class GlobalContent implements Serializable {
    private static GlobalContent instance;
    private List<Contact> recentContactsList;
    private String currentlySelectedActivityInDashboard;
    private List<Notification> dashboardNotifications;
    public static Constants.OrientationType orientation;
    private List<Contact> myContacts;
    private List<SaveList> saveLists;
    private List<Contact> repContactsList;
    private List<ActivityData> activitiesSearchResults;
    private List<Task> taskListSearchResults;
    private List<CallbackData> callbacks;
    private List<Contact> myLast100ContactsList;
    private List<NearbyContacts> nearByContactsList;

    public static GlobalContent getInstance() {
        if (instance == null)
            instance = new GlobalContent();

        return instance;
    }

    public List<Contact> getRecentContactsList() {
        return recentContactsList;
    }

    public void setRecentContactsList(List<Contact> recentContactsList) {
        this.recentContactsList = recentContactsList;
    }

    public String getCurrentlySelectedActivityInDashboard() {
        return currentlySelectedActivityInDashboard;
    }

    public void setCurrentlySelectedActivityInDashboard(String currentlySelectedActivityInDashboard) {
        this.currentlySelectedActivityInDashboard = currentlySelectedActivityInDashboard;
    }

    public List<Notification> getDashboardNotifications() {
        return dashboardNotifications;
    }

    public void setDashboardNotifications(List<Notification> dashboardNotifications) {
        this.dashboardNotifications = dashboardNotifications;
    }


    public void setMySaveList(List<SaveList> saveContactsList) {
        if (saveContactsList == null) {
            if (this.saveLists != null) {
                this.saveLists.clear();
                this.saveLists = null;
            }

            this.saveLists = new ArrayList<>();
        } else {
            if (this.saveLists != null && this.saveLists.size() > 0)
                this.saveLists.addAll(saveContactsList);
            else
                this.saveLists = saveContactsList;
        }
    }

    public List<SaveList> getSaveLists() {
        return saveLists;
    }

    public List<Contact> getMyContacts() {
        return myContacts;
    }

    public void setMyContacts(List<Contact> contactsList) {
        if (contactsList == null) {
            if (this.myContacts != null) {
                this.myContacts.clear();
                this.myContacts = null;
            }

            this.myContacts = new ArrayList<>();
        } else {
            if (this.myContacts != null && this.myContacts.size() > 0)
                this.myContacts.addAll(contactsList);
            else
                this.myContacts = contactsList;
        }
    }

    public List<Contact> getRepContactsList() {
        return repContactsList;
    }

    public void setRepContactsList(List<Contact> repContacts) {
        if (repContacts == null) {
            if (this.repContactsList != null) {
                this.repContactsList.clear();
                this.repContactsList = null;
            }

            this.repContactsList = new ArrayList<>();
        } else {
            if (this.repContactsList != null && this.repContactsList.size() > 0)
                this.repContactsList.addAll(repContacts);
            else
                this.repContactsList = repContacts;
        }
    }

    public List<ActivityData> getActivitiesSearchResults() {
        return activitiesSearchResults;
    }

    public void setActivitiesSearchResults(List<ActivityData> searchResults) {
        if (searchResults == null) {
            if (this.activitiesSearchResults != null) {
                this.activitiesSearchResults.clear();
                this.activitiesSearchResults = null;
            }

            this.activitiesSearchResults = new ArrayList<>();
        } else {
            if (this.activitiesSearchResults != null && this.activitiesSearchResults.size() > 0)
                this.activitiesSearchResults.addAll(searchResults);
            else
                this.activitiesSearchResults = searchResults;
        }
    }

    public List<Task> getTaskListSearchResults() {
        return taskListSearchResults;
    }

    public void setTaskListSearchResults(List<Task> taskList) {
        if (taskList == null) {
            if (this.taskListSearchResults != null) {
                this.taskListSearchResults.clear();
                this.taskListSearchResults = null;
            }

            this.taskListSearchResults = new ArrayList<>();
        } else {
            if (this.taskListSearchResults != null && this.taskListSearchResults.size() > 0)
                this.taskListSearchResults.addAll(taskList);
            else
                this.taskListSearchResults = taskList;
        }
    }

    public List<CallbackData> getCallbacks() {
        return callbacks;
    }

    public void setCallbacks(List<CallbackData> callbacksList) {
        if (callbacksList == null) {
            if (this.callbacks != null) {
                this.callbacks.clear();
                this.callbacks = null;
            }

            this.callbacks = new ArrayList<>();
        } else {
            if (this.callbacks != null && this.callbacks.size() > 0)
                this.callbacks.addAll(callbacksList);
            else
                this.callbacks = callbacksList;
        }
    }

    public List<Contact> getMyLast100ContactsList() {
        return myLast100ContactsList;
    }

    public void setMyLast100ContactsList(List<Contact> contactsList) {
        if (contactsList == null) {
            if (this.myLast100ContactsList != null) {
                this.myLast100ContactsList.clear();
                this.myLast100ContactsList = null;
            }

            this.myLast100ContactsList = new ArrayList<>();
        } else {
            if (this.myLast100ContactsList != null && this.myLast100ContactsList.size() > 0)
                this.myLast100ContactsList.addAll(contactsList);
            else
                this.myLast100ContactsList = contactsList;
        }
    }

    public List<NearbyContacts> getNearestContactList() {
        return nearByContactsList;
    }


    public void setNearestContactList(List<NearbyContacts> nearByContactsList) {

        if (nearByContactsList == null) {
            if (this.nearByContactsList != null) {
                this.nearByContactsList.clear();
                this.nearByContactsList = null;
            }

            this.nearByContactsList = new ArrayList<>();
        } else {
            if (this.nearByContactsList != null && this.nearByContactsList.size() > 0)
                this.nearByContactsList.addAll(nearByContactsList);
            else
                this.nearByContactsList = nearByContactsList;
        }
    }

}
