package com.mirabeltechnologies.magazinemanager.beans;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
    private LatLng mPosition;
    private String mTitle;
    private String mSnippet;
    private String cId;

    public MyItem() {

    }

    public MyItem(LatLng mPosition) {
        this.mPosition = mPosition;
        mTitle = null;
        mSnippet = null;
    }

    public MyItem(LatLng mPosition, String title, String snippet,String cId) {
        this.mPosition = mPosition;
        mTitle = title;
        mSnippet = snippet;
        this.cId=cId;
    }

    public String getcId() {
        return cId;
    }

    public void setcId(String cId) {
        this.cId = cId;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }


    /**
     * Set the title of the marker
     *
     * @param title string to be set as title
     */
    public void setTitle(String title) {
        mTitle = title;
    }

    /**
     * Set the description of the marker
     *
     * @param snippet string to be set as snippet
     */
    public void setSnippet(String snippet) {
        mSnippet = snippet;
    }

    @Override
    public String toString() {
        return "MyItem{" +
                "mPosition=" + mPosition +
                ", mTitle='" + mTitle + '\'' +
                ", mSnippet='" + mSnippet + '\'' +
                ", cId='" + cId + '\'' +
                '}';
    }
}
