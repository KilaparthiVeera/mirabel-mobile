package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 10/23/17.
 */

public class Notification implements Serializable {
    @SerializedName("ActivityOrTaskID")
    @Expose
    private String activityOrTaskId;
    @SerializedName("CustomerID")
    @Expose
    private String customerId;
    @SerializedName("FromName")
    @Expose
    private String fromName;
    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("Message")
    @Expose
    private String message;
    @SerializedName("DateTime")
    @Expose
    private String dateTime;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Done")
    @Expose
    private String done;
    @SerializedName("Viewed")
    @Expose
    private String viewed;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("RepID")
    @Expose
    private String repId;

    public String getActivityOrTaskId() {
        return activityOrTaskId;
    }

    public void setActivityOrTaskId(String activityOrTaskId) {
        this.activityOrTaskId = activityOrTaskId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDone() {
        return done;
    }

    public void setDone(String done) {
        this.done = done;
    }

    public String getViewed() {
        return viewed;
    }

    public void setViewed(String viewed) {
        this.viewed = viewed;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "activityOrTaskId='" + activityOrTaskId + '\'' +
                ", customerId='" + customerId + '\'' +
                ", fromName='" + fromName + '\'' +
                ", subject='" + subject + '\'' +
                ", message='" + message + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", time='" + time + '\'' +
                ", type='" + type + '\'' +
                ", done='" + done + '\'' +
                ", viewed='" + viewed + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", repId='" + repId + '\'' +
                '}';
    }
}
