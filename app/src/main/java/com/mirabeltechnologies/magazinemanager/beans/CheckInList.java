package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CheckInList {
    @SerializedName("UserCheckInID")
    @Expose
    private Integer userCheckInID;
    @SerializedName("User")
    @Expose
    private CheckUser user;
    @SerializedName("Customer")
    @Expose
    private Customer customer;
    @SerializedName("TransactionDate")
    @Expose
    private String transactionDate;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Note")
    @Expose
    private String note;

    public Integer getUserCheckInID() {
        return userCheckInID;
    }

    public void setUserCheckInID(Integer userCheckInID) {
        this.userCheckInID = userCheckInID;
    }

    public CheckUser getUser() {
        return user;
    }

    public void setUser(CheckUser user) {
        this.user = user;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
