package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CalendarEvent implements Serializable {
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("TimeSlot")
    @Expose
    private String timeSlot;
    @SerializedName("TimeSlotName")
    @Expose
    private String timeSlotName;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("gsActivityID")
    @Expose
    private String eventId;
    @Expose
    private int timeSlotNumber;
    @Expose
    private boolean isHeader;
    @Expose
    private String modifiedTimeSlotName;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getTimeSlotName() {
        return timeSlotName;
    }

    public void setTimeSlotName(String timeSlotName) {
        this.timeSlotName = timeSlotName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public int getTimeSlotNumber() {
        return timeSlotNumber;
    }

    public void setTimeSlotNumber(int timeSlotNumber) {
        this.timeSlotNumber = timeSlotNumber;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public String getModifiedTimeSlotName() {
        return modifiedTimeSlotName;
    }

    public void setModifiedTimeSlotName(String modifiedTimeSlotName) {
        this.modifiedTimeSlotName = modifiedTimeSlotName;
    }

    @Override
    public String toString() {
        return "CalendarEvent{" +
                " eventId='" + eventId + '\'' +
                ", time='" + time + '\'' +
                ", timeSlot='" + timeSlot + '\'' +
                ", timeSlotNumber=" + timeSlotNumber +
                ", timeSlotName='" + timeSlotName + '\'' +
                ", title='" + title + '\'' +
                ", isHeader=" + isHeader +
                ", modifiedTimeSlotName='" + modifiedTimeSlotName + '\'' +
                '}';
    }
}
