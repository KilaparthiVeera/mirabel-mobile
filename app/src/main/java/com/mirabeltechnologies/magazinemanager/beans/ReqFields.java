package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 11/15/17.
 */

public class ReqFields implements Serializable {
    @SerializedName("DisplayName")
    @Expose
    private String displayName;
    @SerializedName("DisplayPage")
    @Expose
    private String displayPage;
    @SerializedName("FeatureName")
    @Expose
    private String featureName;
    @SerializedName("FieldID")
    @Expose
    private int fieldId;
    @SerializedName("FieldName")
    @Expose
    private String fieldName;
    @SerializedName("Required")
    @Expose
    private boolean required;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayPage() {
        return displayPage;
    }

    public void setDisplayPage(String displayPage) {
        this.displayPage = displayPage;
    }

    public String getFeatureName() {
        return featureName;
    }

    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public int getFieldId() {
        return fieldId;
    }

    public void setFieldId(int fieldId) {
        this.fieldId = fieldId;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    @Override
    public String toString() {
        return "ReqFields{" +
                "displayName='" + displayName + '\'' +
                ", displayPage='" + displayPage + '\'' +
                ", featureName='" + featureName + '\'' +
                ", fieldId=" + fieldId +
                ", fieldName='" + fieldName + '\'' +
                ", required=" + required +
                '}';
    }
}
