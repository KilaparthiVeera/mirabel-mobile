package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SendBusinessRequest {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fileStreamData")
    @Expose
    private String fileStreamData;
    @SerializedName("FileUploadSource")
    @Expose
    private Integer fileUploadSource;
    @SerializedName("ClientID")
    @Expose
    private String clientID;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFileStreamData() {
        return fileStreamData;
    }

    public void setFileStreamData(String fileStreamData) {
        this.fileStreamData = fileStreamData;
    }

    public Integer getFileUploadSource() {
        return fileUploadSource;
    }

    public void setFileUploadSource(Integer fileUploadSource) {
        this.fileUploadSource = fileUploadSource;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

}
