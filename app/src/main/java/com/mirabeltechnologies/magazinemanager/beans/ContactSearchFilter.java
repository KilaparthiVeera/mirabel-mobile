package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

/**
 * Created by venkat on 11/1/17.
 */

public class ContactSearchFilter implements Serializable {
    private String company;
    private String name;
    private String phone;
    private String email;
    private String repId;
    private String repName;

    public ContactSearchFilter() {
        this.company = "";
        this.name = "";
        this.phone = "";
        this.email = "";
        this.repId = "-1";
        this.repName = "";
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRepId() {
        return repId;
    }

    public void setRepId(String repId) {
        this.repId = repId;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    @Override
    public String toString() {
        return "ContactSearchFilter{" +
                "company='" + company + '\'' +
                ", name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", email='" + email + '\'' +
                ", repId='" + repId + '\'' +
                ", repName='" + repName + '\'' +
                '}';
    }
}
