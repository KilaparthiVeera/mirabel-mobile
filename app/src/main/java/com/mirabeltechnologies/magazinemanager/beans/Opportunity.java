package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Opportunity implements Serializable {

    @SerializedName("Deleted")
    @Expose
    private Boolean deleted;
    @SerializedName("Source")
    @Expose
    private String source;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("ROIAnalysis")
    @Expose
    private Boolean rOIAnalysis;
    @SerializedName("BudgetConfirmed")
    @Expose
    private Boolean budgetConfirmed;
    @SerializedName("NextStep")
    @Expose
    private String nextStep;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("IsWon")
    @Expose
    private Boolean isWon;
    @SerializedName("OppLossReasonDetails")
    @Expose
    private OppLossReasonDetails oppLossReasonDetails;
    @SerializedName("AssignedTODetails")
    @Expose
    private AssignDetails assignedTODetails;
    @SerializedName("BusinessUnitDetails")
    @Expose
    private BusinessUnitDetails businessUnitDetails;
    @SerializedName("OppTypeDetails")
    @Expose
    private OppTypeDetails oppTypeDetails;
    @SerializedName("SubContactDetails")
    @Expose
    private SubContactDetails subContactDetails;
    @SerializedName("ProductDetails")
    @Expose
    private ProductDetails productDetails;
    @SerializedName("OwnerDetails")
    @Expose
    private OwnerDetails ownerDetails;
    @SerializedName("ArthmeticOP")
    @Expose
    private Object arthmeticOP;
    @SerializedName("ActualCloseDateTo")
    @Expose
    private Object actualCloseDateTo;
    @SerializedName("CloseDateFrom")
    @Expose
    private String closeDateFrom;
    @SerializedName("CreatedDateTo")
    @Expose
    private String createdDateTo;
    @SerializedName("ClosedDateTo")
    @Expose
    private String closedDateTo;
    @SerializedName("LoggedInRep")
    @Expose
    private String loggedInRep;
    @SerializedName("LastActivity")
    @Expose
    private LastActivity lastActivity;
    @SerializedName("ProposalID")
    @Expose
    private String proposalID;
    @SerializedName("IsConvertedToContract")
    @Expose
    private Boolean isConvertedToContract;
    @SerializedName("ForecastRevenue")
    @Expose
    private Double forecastRevenue;
    @SerializedName("ListId")
    @Expose
    private Integer listId;
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("CloseDate")
    @Expose
    private String closeDate;
    @SerializedName("ActualCloseDate")
    @Expose
    private Object actualCloseDate;
    @SerializedName("Amount")
    @Expose
    private Double amount;
    @SerializedName("CanView")
    @Expose
    private String canView;
    @SerializedName("Probability")
    @Expose
    private Integer probability;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("ModifiedDate")
    @Expose
    private String modifiedDate;
    @SerializedName("AssignedTo")
    @Expose
    private String assignedTo;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("OppStageDetails")
    @Expose
    private OppStageDetails oppStageDetails;

    @SerializedName("ContactDetails")
    @Expose
    private ContactDetails contactDetails;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getrOIAnalysis() {
        return rOIAnalysis;
    }

    public void setrOIAnalysis(Boolean rOIAnalysis) {
        this.rOIAnalysis = rOIAnalysis;
    }

    public Boolean getBudgetConfirmed() {
        return budgetConfirmed;
    }

    public void setBudgetConfirmed(Boolean budgetConfirmed) {
        this.budgetConfirmed = budgetConfirmed;
    }

    public String getNextStep() {
        return nextStep;
    }

    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Boolean getWon() {
        return isWon;
    }

    public void setWon(Boolean won) {
        isWon = won;
    }

    public OppLossReasonDetails getOppLossReasonDetails() {
        return oppLossReasonDetails;
    }

    public void setOppLossReasonDetails(OppLossReasonDetails oppLossReasonDetails) {
        this.oppLossReasonDetails = oppLossReasonDetails;
    }

    public AssignDetails getAssignedTODetails() {
        return assignedTODetails;
    }

    public void setAssignedTODetails(AssignDetails assignedTODetails) {
        this.assignedTODetails = assignedTODetails;
    }

    public BusinessUnitDetails getBusinessUnitDetails() {
        return businessUnitDetails;
    }

    public void setBusinessUnitDetails(BusinessUnitDetails businessUnitDetails) {
        this.businessUnitDetails = businessUnitDetails;
    }

    public OppTypeDetails getOppTypeDetails() {
        return oppTypeDetails;
    }

    public void setOppTypeDetails(OppTypeDetails oppTypeDetails) {
        this.oppTypeDetails = oppTypeDetails;
    }

    public SubContactDetails getSubContactDetails() {
        return subContactDetails;
    }

    public void setSubContactDetails(SubContactDetails subContactDetails) {
        this.subContactDetails = subContactDetails;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public OwnerDetails getOwnerDetails() {
        return ownerDetails;
    }

    public void setOwnerDetails(OwnerDetails ownerDetails) {
        this.ownerDetails = ownerDetails;
    }

    public Object getArthmeticOP() {
        return arthmeticOP;
    }

    public void setArthmeticOP(Object arthmeticOP) {
        this.arthmeticOP = arthmeticOP;
    }

    public Object getActualCloseDateTo() {
        return actualCloseDateTo;
    }

    public void setActualCloseDateTo(Object actualCloseDateTo) {
        this.actualCloseDateTo = actualCloseDateTo;
    }

    public String getCloseDateFrom() {
        return closeDateFrom;
    }

    public void setCloseDateFrom(String closeDateFrom) {
        this.closeDateFrom = closeDateFrom;
    }

    public String getCreatedDateTo() {
        return createdDateTo;
    }

    public void setCreatedDateTo(String createdDateTo) {
        this.createdDateTo = createdDateTo;
    }

    public String getClosedDateTo() {
        return closedDateTo;
    }

    public void setClosedDateTo(String closedDateTo) {
        this.closedDateTo = closedDateTo;
    }

    public String getLoggedInRep() {
        return loggedInRep;
    }

    public void setLoggedInRep(String loggedInRep) {
        this.loggedInRep = loggedInRep;
    }

    public LastActivity getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(LastActivity lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getProposalID() {
        return proposalID;
    }

    public void setProposalID(String proposalID) {
        this.proposalID = proposalID;
    }

    public Boolean getConvertedToContract() {
        return isConvertedToContract;
    }

    public void setConvertedToContract(Boolean convertedToContract) {
        isConvertedToContract = convertedToContract;
    }

    public double getForecastRevenue() {
        return forecastRevenue;
    }

    public void setForecastRevenue(double forecastRevenue) {
        this.forecastRevenue = forecastRevenue;
    }

    public Integer getListId() {
        return listId;
    }

    public void setListId(Integer listId) {
        this.listId = listId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public Object getActualCloseDate() {
        return actualCloseDate;
    }

    public void setActualCloseDate(Object actualCloseDate) {
        this.actualCloseDate = actualCloseDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCanView() {
        return canView;
    }

    public void setCanView(String canView) {
        this.canView = canView;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(String modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(String assignedTo) {
        this.assignedTo = assignedTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public OppStageDetails getOppStageDetails() {
        return oppStageDetails;
    }

    public void setOppStageDetails(OppStageDetails oppStageDetails) {
        this.oppStageDetails = oppStageDetails;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }


    @NonNull
    @Override
    public String toString() {
        return "Opportunity{" +
                "deleted=" + deleted +
                ", source='" + source + '\'' +
                ", description='" + description + '\'' +
                ", rOIAnalysis=" + rOIAnalysis +
                ", budgetConfirmed=" + budgetConfirmed +
                ", nextStep='" + nextStep + '\'' +
                ", notes='" + notes + '\'' +
                ", isWon=" + isWon +
                ", oppLossReasonDetails=" + oppLossReasonDetails +
                ", assignedTODetails=" + assignedTODetails +
                ", businessUnitDetails=" + businessUnitDetails +
                ", oppTypeDetails=" + oppTypeDetails +
                ", subContactDetails=" + subContactDetails +
                ", productDetails=" + productDetails +
                ", ownerDetails=" + ownerDetails +
                ", arthmeticOP=" + arthmeticOP +
                ", actualCloseDateTo=" + actualCloseDateTo +
                ", closeDateFrom=" + closeDateFrom +
                ", createdDateTo=" + createdDateTo +
                ", closedDateTo=" + closedDateTo +
                ", loggedInRep=" + loggedInRep +
                ", lastActivity=" + lastActivity +
                ", proposalID='" + proposalID + '\'' +
                ", isConvertedToContract=" + isConvertedToContract +
                ", forecastRevenue=" + forecastRevenue +
                ", listId=" + listId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", closeDate='" + closeDate + '\'' +
                ", actualCloseDate=" + actualCloseDate +
                ", amount=" + amount +
                ", canView='" + canView + '\'' +
                ", probability=" + probability +
                ", createdDate='" + createdDate + '\'' +
                ", modifiedDate='" + modifiedDate + '\'' +
                ", assignedTo='" + assignedTo + '\'' +
                ", status='" + status + '\'' +
                ", oppStageDetails=" + oppStageDetails +
                ", contactDetails=" + contactDetails +
                '}';
    }
}
