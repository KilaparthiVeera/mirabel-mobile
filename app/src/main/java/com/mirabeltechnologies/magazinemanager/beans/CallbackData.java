package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 05/28/18.
 */

public class CallbackData implements Serializable {
    @SerializedName("gsActivitiesID")
    @Expose
    private Long activityId;
    @SerializedName("CustomerID")
    @Expose
    private Long customerId;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("notes")
    @Expose
    private String notes;
    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("PhoneExtn")
    @Expose
    private String phoneExtn;
    @SerializedName("ContactName")
    @Expose
    private String contactName;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("RepName")
    @Expose
    private String repName;
    @SerializedName("Priority")
    @Expose
    private String priority;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ParentID")
    @Expose
    private String parentId;
    @SerializedName("CanViewEmployeeID")
    @Expose
    private String canViewEmployeeID;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("ImageName")
    @Expose
    private String imageName;
    @Expose
    private String modifiedNotes;
    @Expose
    private boolean hasHTMLTags;

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPhoneExtn() {
        return phoneExtn;
    }

    public void setPhoneExtn(String phoneExtn) {
        this.phoneExtn = phoneExtn;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getCanViewEmployeeID() {
        return canViewEmployeeID;
    }

    public void setCanViewEmployeeID(String canViewEmployeeID) {
        this.canViewEmployeeID = canViewEmployeeID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public String getModifiedNotes() {
        return modifiedNotes;
    }

    public void setModifiedNotes(String modifiedNotes) {
        this.modifiedNotes = modifiedNotes;
    }

    public boolean isHasHTMLTags() {
        return hasHTMLTags;
    }

    public void setHasHTMLTags(boolean hasHTMLTags) {
        this.hasHTMLTags = hasHTMLTags;
    }

    @Override
    public String toString() {
        return "CallbackData{" +
                "activityId=" + activityId +
                ", customerId=" + customerId +
                ", companyName='" + companyName + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", notes='" + notes + '\'' +
                ", date='" + date + '\'' +
                ", phoneExtn='" + phoneExtn + '\'' +
                ", contactName='" + contactName + '\'' +
                ", phone='" + phone + '\'' +
                ", city='" + city + '\'' +
                ", repName='" + repName + '\'' +
                ", priority='" + priority + '\'' +
                ", email='" + email + '\'' +
                ", parentId='" + parentId + '\'' +
                ", canViewEmployeeID='" + canViewEmployeeID + '\'' +
                ", title='" + title + '\'' +
                ", imageName='" + imageName + '\'' +
                ", modifiedNotes='" + modifiedNotes + '\'' +
                ", hasHTMLTags=" + hasHTMLTags +
                '}';
    }
}
