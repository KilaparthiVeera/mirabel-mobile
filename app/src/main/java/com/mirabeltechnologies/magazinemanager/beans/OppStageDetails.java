package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class OppStageDetails implements Serializable {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Stage")
    @Expose
    private String stage;
    @SerializedName("Description")
    @Expose
    private Object description;
    @SerializedName("PercentClosed")
    @Expose
    private Integer percentClosed;
    @SerializedName("SortOrder")
    @Expose
    private Integer sortOrder;
    private Boolean isSelected=false;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public Object getDescription() {
        return description;
    }

    public void setDescription(Object description) {
        this.description = description;
    }

    public Integer getPercentClosed() {
        return percentClosed;
    }

    public void setPercentClosed(Integer percentClosed) {
        this.percentClosed = percentClosed;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString() {
        return "OppStageDetails{" +
                "id=" + id +
                ", stage='" + stage + '\'' +
                ", description=" + description +
                ", percentClosed=" + percentClosed +
                ", sortOrder=" + sortOrder +
                ", isSelected=" + isSelected +
                '}';
    }
}
