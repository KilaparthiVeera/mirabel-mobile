package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastActivity {

    @SerializedName("NewValue")
    @Expose
    private String newValue;
    @SerializedName("OldValue")
    @Expose
    private String oldValue;
    @SerializedName("LastActivity")
    @Expose
    private String lastActivity;
    @SerializedName("OpportunityField")
    @Expose
    private String opportunityField;
    @SerializedName("Display")
    @Expose
    private String display;

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getLastActivity() {
        return lastActivity;
    }

    public void setLastActivity(String lastActivity) {
        this.lastActivity = lastActivity;
    }

    public String getOpportunityField() {
        return opportunityField;
    }

    public void setOpportunityField(String opportunityField) {
        this.opportunityField = opportunityField;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

}
