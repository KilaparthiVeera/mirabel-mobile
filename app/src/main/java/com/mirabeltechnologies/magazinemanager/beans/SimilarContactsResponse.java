package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SimilarContactsResponse {
    @SerializedName("content")
    @Expose
    private SimilarContent content;

    public SimilarContent getContent() {
        return content;
    }

    public void setContent(SimilarContent content) {
        this.content = content;
    }
}
