package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CalendarEventDetails implements Serializable {
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("MeetingName")
    @Expose
    private String meetingType;
    @SerializedName("MeetingType")
    @Expose
    private int meetingTypeId;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("IsAllDay")
    @Expose
    private boolean isAllDay;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("RRule")
    @Expose
    private String recurrenceRule;
    @SerializedName("RRuleDetails")
    @Expose
    private RecurrenceRules recurrenceRuleDetails;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMeetingType() {
        return meetingType;
    }

    public void setMeetingType(String meetingType) {
        this.meetingType = meetingType;
    }

    public int getMeetingTypeId() {
        return meetingTypeId;
    }

    public void setMeetingTypeId(int meetingTypeId) {
        this.meetingTypeId = meetingTypeId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isAllDay() {
        return isAllDay;
    }

    public void setAllDay(boolean allDay) {
        isAllDay = allDay;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getRecurrenceRule() {
        return recurrenceRule;
    }

    public void setRecurrenceRule(String recurrenceRule) {
        this.recurrenceRule = recurrenceRule;
    }

    public RecurrenceRules getRecurrenceRuleDetails() {
        return recurrenceRuleDetails;
    }

    public void setRecurrenceRuleDetails(RecurrenceRules recurrenceRuleDetails) {
        this.recurrenceRuleDetails = recurrenceRuleDetails;
    }

    @Override
    public String toString() {
        return "CalendarEventDetails{" +
                "title='" + title + '\'' +
                ", meetingType='" + meetingType + '\'' +
                ", meetingTypeId=" + meetingTypeId +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", isAllDay=" + isAllDay +
                ", notes='" + notes + '\'' +
                ", recurrenceRule='" + recurrenceRule + '\'' +
                ", recurrenceRuleDetails=" + recurrenceRuleDetails +
                '}';
    }
}
