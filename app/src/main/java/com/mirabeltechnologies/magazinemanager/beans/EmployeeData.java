package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 12/22/17.
 */

public class EmployeeData implements Serializable {
    @SerializedName("EmployeeId")
    @Expose
    private String employeeId;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("NoteSettings")
    @Expose
    private int noteSettings;
    @SerializedName("CanEditNotes")
    @Expose
    private int canEditNotes;
    @SerializedName("CanDeleteNotes")
    @Expose
    private int canDeleteNotes;
    @SerializedName("CanCreateContracts")
    @Expose
    private boolean canCreateContracts;
    @SerializedName("Admin")
    @Expose
    private boolean isAdmin;

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNoteSettings() {
        return noteSettings;
    }

    public void setNoteSettings(int noteSettings) {
        this.noteSettings = noteSettings;
    }

    public int getCanEditNotes() {
        return canEditNotes;
    }

    public void setCanEditNotes(int canEditNotes) {
        this.canEditNotes = canEditNotes;
    }

    public int getCanDeleteNotes() {
        return canDeleteNotes;
    }

    public void setCanDeleteNotes(int canDeleteNotes) {
        this.canDeleteNotes = canDeleteNotes;
    }

    public boolean isCanCreateContracts() {
        return canCreateContracts;
    }

    public void setCanCreateContracts(boolean canCreateContracts) {
        this.canCreateContracts = canCreateContracts;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public String toString() {
        return "EmployeeData{" +
                "employeeId='" + employeeId + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", noteSettings=" + noteSettings +
                ", canEditNotes=" + canEditNotes +
                ", canDeleteNotes=" + canDeleteNotes +
                ", canCreateContracts=" + canCreateContracts +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
