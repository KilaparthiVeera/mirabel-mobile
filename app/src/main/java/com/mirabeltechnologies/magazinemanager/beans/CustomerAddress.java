package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 10/20/17.
 */

public class CustomerAddress implements Serializable {
    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("AddressCountyInternational")
    @Expose
    private String addressCountyInternational;
    @SerializedName("AddressInfo")
    @Expose
    private String addressInfo;
    @SerializedName("AddressTitle")
    @Expose
    private String addressTitle;
    @SerializedName("AddressType")
    @Expose
    private String addressType;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("County")
    @Expose
    private String county;
    @SerializedName("CustomerID")
    @Expose
    private Long customerId;
    @SerializedName("International")
    @Expose
    private String international;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Zipcode")
    @Expose
    private String zipcode;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddressCountyInternational() {
        return addressCountyInternational;
    }

    public void setAddressCountyInternational(String addressCountyInternational) {
        this.addressCountyInternational = addressCountyInternational;
    }

    public String getAddressInfo() {
        return addressInfo;
    }

    public void setAddressInfo(String addressInfo) {
        this.addressInfo = addressInfo;
    }

    public String getAddressTitle() {
        return addressTitle;
    }

    public void setAddressTitle(String addressTitle) {
        this.addressTitle = addressTitle;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    @Override
    public String toString() {
        return "CustomerAddress{" +
                "address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", addressCountyInternational='" + addressCountyInternational + '\'' +
                ", addressInfo='" + addressInfo + '\'' +
                ", addressTitle='" + addressTitle + '\'' +
                ", addressType='" + addressType + '\'' +
                ", city='" + city + '\'' +
                ", county='" + county + '\'' +
                ", customerId=" + customerId +
                ", international='" + international + '\'' +
                ", state='" + state + '\'' +
                ", zipcode='" + zipcode + '\'' +
                '}';
    }
}
