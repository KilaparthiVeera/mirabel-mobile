package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class RecurrenceRules implements Serializable {
    @SerializedName("Pattern")
    @Expose
    private String pattern;
    @SerializedName("RepeatEvery")
    @Expose
    private int repeatEvery;
    @SerializedName("RepeatDays")
    @Expose
    private String repeatDays;
    @SerializedName("RepeatBy")
    @Expose
    private int repeatBy;
    @SerializedName("RepeatByOption")
    @Expose
    private int repeatByOption;
    @SerializedName("RepeatByText")
    @Expose
    private String repeatByText;
    @SerializedName("RuleType")
    @Expose
    private int ruleType;
    @SerializedName("UntilDate")
    @Expose
    private String untilDate;
    @SerializedName("Occurrences")
    @Expose
    private int occurrences;

    public RecurrenceRules() {
        this.pattern = "Daily";
        this.repeatEvery = 1;
        this.repeatDays = "";
        this.repeatBy = 0;
        this.repeatByOption = 0;
        this.repeatByText = "";
        this.ruleType = 2;
        this.untilDate = "";
        this.occurrences = 1;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public int getRepeatEvery() {
        return repeatEvery;
    }

    public void setRepeatEvery(int repeatEvery) {
        this.repeatEvery = repeatEvery;
    }

    public String getRepeatDays() {
        return repeatDays;
    }

    public void setRepeatDays(String repeatDays) {
        this.repeatDays = repeatDays;
    }

    public int getRepeatBy() {
        return repeatBy;
    }

    public void setRepeatBy(int repeatBy) {
        this.repeatBy = repeatBy;
    }

    public int getRepeatByOption() {
        return repeatByOption;
    }

    public void setRepeatByOption(int repeatByOption) {
        this.repeatByOption = repeatByOption;
    }

    public String getRepeatByText() {
        return repeatByText;
    }

    public void setRepeatByText(String repeatByText) {
        this.repeatByText = repeatByText;
    }

    public int getRuleType() {
        return ruleType;
    }

    public void setRuleType(int ruleType) {
        this.ruleType = ruleType;
    }

    public String getUntilDate() {
        return untilDate;
    }

    public void setUntilDate(String untilDate) {
        this.untilDate = untilDate;
    }

    public int getOccurrences() {
        return occurrences;
    }

    public void setOccurrences(int occurrences) {
        this.occurrences = occurrences;
    }

    @Override
    public String toString() {
        return "RecurrenceRules{" +
                "pattern='" + pattern + '\'' +
                ", repeatEvery=" + repeatEvery +
                ", repeatDays='" + repeatDays + '\'' +
                ", repeatBy=" + repeatBy +
                ", repeatByOption=" + repeatByOption +
                ", repeatByText=" + repeatByText +
                ", ruleType=" + ruleType +
                ", untilDate='" + untilDate + '\'' +
                ", occurrences=" + occurrences +
                '}';
    }
}
