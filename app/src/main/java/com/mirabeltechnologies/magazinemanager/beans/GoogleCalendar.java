package com.mirabeltechnologies.magazinemanager.beans;

public class GoogleCalendar {

    private int event_id;
    private String title, organizer, dtstart, dtend,descerption;

    public String getDescerption() {
        return descerption;
    }

    public void setDescerption(String descerption) {
        this.descerption = descerption;
    }

    public int getEvent_id() {
        return event_id;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOrganizer() {
        return organizer;
    }

    public void setOrganizer(String organizer) {
        this.organizer = organizer;
    }

    public String getDtstart() {
        return dtstart;
    }

    public void setDtstart(String dtstart) {
        this.dtstart = dtstart;
    }

    public String getDtend() {
        return dtend;
    }

    public void setDtend(String dtend) {
        this.dtend = dtend;
    }

    @Override
    public String toString() {
        return "GoogleCalendar{" +
                "event_id=" + event_id +
                ", title='" + title + '\'' +
                ", organizer='" + organizer + '\'' +
                ", dtstart='" + dtstart + '\'' +
                ", dtend='" + dtend + '\'' +
                ", descerption='" + descerption + '\'' +
                '}';
    }
}
