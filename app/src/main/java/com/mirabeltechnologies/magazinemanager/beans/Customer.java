package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("JobDescription")
    @Expose
    private String JobDescription;
    @SerializedName("RepName")
    @Expose
    private String RepName;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("MiddleName")
    @Expose
    private String middleName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("SalesRepID")
    @Expose
    private Integer salesRepID;
    @SerializedName("ContactName")
    @Expose
    private String contactName;
    @SerializedName("ContactFullName")
    @Expose
    private String contactFullName;
    @SerializedName("FullNameWithCompany")
    @Expose
    private String fullNameWithCompany;
    @SerializedName("IsDefault")
    @Expose
    private Boolean isDefault;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneExt")
    @Expose
    private Object phoneExt;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("CellPhone")
    @Expose
    private Object cellPhone;
    @SerializedName("InActive")
    @Expose
    private Boolean inActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getSalesRepID() {
        return salesRepID;
    }

    public void setSalesRepID(Integer salesRepID) {
        this.salesRepID = salesRepID;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactFullName() {
        return contactFullName;
    }

    public void setContactFullName(String contactFullName) {
        this.contactFullName = contactFullName;
    }

    public String getFullNameWithCompany() {
        return fullNameWithCompany;
    }

    public void setFullNameWithCompany(String fullNameWithCompany) {
        this.fullNameWithCompany = fullNameWithCompany;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Object getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(Object phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Object getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(Object cellPhone) {
        this.cellPhone = cellPhone;
    }

    public Boolean getInActive() {
        return inActive;
    }

    public void setInActive(Boolean inActive) {
        this.inActive = inActive;
    }

    public String getJobDescription() {
        return JobDescription;
    }

    public void setJobDescription(String jobDescription) {
        JobDescription = jobDescription;
    }

    public String getRepName() {
        return RepName;
    }

    public void setRepName(String repName) {
        RepName = repName;
    }
}
