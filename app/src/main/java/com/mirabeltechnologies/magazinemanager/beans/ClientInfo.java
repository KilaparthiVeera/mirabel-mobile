package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 04/10/17.
 */
public class ClientInfo implements Serializable {
    public static List<ClientInfo> clientList = new ArrayList<ClientInfo>();
    public static int selectedClientId;

    private int clientId;
    private String clientName;
    private String clientURL;
    private String subDomain;
    private String repUserName;
    private int employeeId;
    private String siteType;
    private boolean isDSUser;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientURL() {
        return clientURL;
    }

    public void setClientURL(String clientURL) {
        this.clientURL = clientURL;
    }

    public String getSubDomain() {
        return subDomain;
    }

    public void setSubDomain(String subDomain) {
        this.subDomain = subDomain;
    }

    public String getRepUserName() {
        return repUserName;
    }

    public void setRepUserName(String repUserName) {
        this.repUserName = repUserName;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public boolean isDSUser() {
        return isDSUser;
    }

    public void setDSUser(boolean DSUser) {
        isDSUser = DSUser;
    }

    public static List<ClientInfo> getClientList() {
        return clientList;
    }

    public static void setClientList(List<ClientInfo> clientList) {
        ClientInfo.clientList = clientList;
    }

    @Override
    public String toString() {
        return "ClientInfo{" +
                "clientId=" + clientId +
                ", clientName='" + clientName + '\'' +
                ", clientURL='" + clientURL + '\'' +
                ", subDomain='" + subDomain + '\'' +
                ", repUserName='" + repUserName + '\'' +
                ", employeeId=" + employeeId +
                ", siteType='" + siteType + '\'' +
                ", isDSUser=" + isDSUser +
                '}';
    }
}