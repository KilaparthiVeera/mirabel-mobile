package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

public class NewTicket implements Serializable {
    private String companyName;
    private String name;
    private String email;
    private String ccEmail;
    private String url;
    private String browser;
    private String categoryKey;
    private String categoryValue;
    private String subject;
    private String description;

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCcEmail() {
        return ccEmail;
    }

    public void setCcEmail(String ccEmail) {
        this.ccEmail = ccEmail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryValue() {
        return categoryValue;
    }

    public void setCategoryValue(String categoryValue) {
        this.categoryValue = categoryValue;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "NewTicket{" +
                "companyName='" + companyName + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", ccEmail='" + ccEmail + '\'' +
                ", url='" + url + '\'' +
                ", browser='" + browser + '\'' +
                ", categoryKey='" + categoryKey + '\'' +
                ", categoryValue='" + categoryValue + '\'' +
                ", subject='" + subject + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
