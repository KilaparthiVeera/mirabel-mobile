package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpportunityResult {


    @SerializedName("TotIds")
    @Expose
    private Integer totIds;
    @SerializedName("TotOppAmt")
    @Expose
    private Double totOppAmt;
    @SerializedName("Open")
    @Expose
    private Integer open;
    @SerializedName("Won")
    @Expose
    private Integer won;
    @SerializedName("Lost")
    @Expose
    private Integer lost;
    @SerializedName("WinTotal")
    @Expose
    private Double winTotal;
    @SerializedName("WinRatio")
    @Expose
    private Double winRatio;

    public Integer getTotIds() {
        return totIds;
    }

    public void setTotIds(Integer totIds) {
        this.totIds = totIds;
    }

    public double getTotOppAmt() {
        return totOppAmt;
    }

    public void setTotOppAmt(double totOppAmt) {
        this.totOppAmt = totOppAmt;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public Integer getWon() {
        return won;
    }

    public void setWon(Integer won) {
        this.won = won;
    }

    public Integer getLost() {
        return lost;
    }

    public void setLost(Integer lost) {
        this.lost = lost;
    }

    public double getWinTotal() {
        return winTotal;
    }

    public void setWinTotal(double winTotal) {
        this.winTotal = winTotal;
    }

    public double getWinRatio() {
        return winRatio;
    }

    public void setWinRatio(double winRatio) {
        this.winRatio = winRatio;
    }

    @NonNull
    @Override
    public String toString() {
        return "OpportunityResult{" +
                "totIds=" + totIds +
                ", totOppAmt=" + totOppAmt +
                ", open=" + open +
                ", won=" + won +
                ", lost=" + lost +
                ", winTotal=" + winTotal +
                ", winRatio=" + winRatio +
                '}';
    }
}
