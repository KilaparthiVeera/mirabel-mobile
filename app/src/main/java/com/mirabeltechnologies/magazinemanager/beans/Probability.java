package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Probability {

    private String probabilityId;
    private String probabilityName;
    private Boolean isSelected=false;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public String getProbabilityId() {
        return probabilityId;
    }

    public void setProbabilityId(String probabilityId) {
        this.probabilityId = probabilityId;
    }

    public String getProbabilityName() {
        return probabilityName;
    }

    public void setProbabilityName(String probabilityName) {
        this.probabilityName = probabilityName;
    }

    @NonNull
    @Override
    public String toString() {
        return "{" +
                "probabilityId='" + probabilityId + '\'' +
                ", probabilityName='" + probabilityName + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }
}
