package com.mirabeltechnologies.magazinemanager.beans;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.maps.android.clustering.ClusterItem;

import java.util.List;

public class NearbyContacts implements Parcelable {
    @SerializedName("PageIndex")
    @Expose
    private Integer pageIndex;
    @SerializedName("AutoID")
    @Expose
    private Integer autoID;
    @SerializedName("gsCustomersID")
    @Expose
    private Integer gsCustomersID;
    @SerializedName("Customer")
    @Expose
    private String customer;
    @SerializedName("OrderCustomer")
    @Expose
    private String orderCustomer;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ParentID")
    @Expose
    private Integer parentID;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneExtn")
    @Expose
    private String phoneExtn;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("RepId")
    @Expose
    private Integer repId;
    @SerializedName("pastOrFuture")
    @Expose
    private String pastOrFuture;
    @SerializedName("LastCompList")
    @Expose
    private String lastCompList;
    @SerializedName("Category")
    @Expose
    private String category;
    @SerializedName("JobTitle")
    @Expose
    private String jobTitle;
    @SerializedName("Address 1")
    @Expose
    private String address1;
    @SerializedName("Address 2")
    @Expose
    private String address2;
    @SerializedName("State/Region")
    @Expose
    private String stateRegion;
    @SerializedName("Zip/Postal Code")
    @Expose
    private String zipPostalCode;
    @SerializedName("County")
    @Expose
    private String county;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Prefix")
    @Expose
    private String prefix;
    @SerializedName("Suffix")
    @Expose
    private Object suffix;
    @SerializedName("Fax_")
    @Expose
    private String fax;
    @SerializedName("homephone")
    @Expose
    private Object homephone;
    @SerializedName("URL")
    @Expose
    private String uRL;
    @SerializedName("Phone 2")
    @Expose
    private String phone2;
    @SerializedName("Do Not Fax")
    @Expose
    private Boolean doNotFax;
    @SerializedName("Do Not Email")
    @Expose
    private Boolean doNotEmail;
    @SerializedName("canViewEmployeeID")
    @Expose
    private Integer canViewEmployeeID;
    @SerializedName("RepName")
    @Expose
    private String repName;
    @SerializedName("IsAgency")
    @Expose
    private String isAgency;
    @SerializedName("JobDescription")
    @Expose
    private String jobDescription;
    @SerializedName("BudgetPlan")
    @Expose
    private String budgetPlan;
    @SerializedName("BudgetPlanNum")
    @Expose
    private Integer budgetPlanNum;
    @SerializedName("LastContract")
    @Expose
    private Object lastContract;
    private boolean isCheck=false;

    public boolean isCheck() {
        return isCheck;
    }

    public void setCheck(boolean check) {
        isCheck = check;
    }

    public NearbyContacts() {
    }

    protected NearbyContacts(Parcel in) {
        if (in.readByte() == 0) {
            pageIndex = null;
        } else {
            pageIndex = in.readInt();
        }
        if (in.readByte() == 0) {
            autoID = null;
        } else {
            autoID = in.readInt();
        }
        if (in.readByte() == 0) {
            gsCustomersID = null;
        } else {
            gsCustomersID = in.readInt();
        }
        customer = in.readString();
        orderCustomer = in.readString();
        name = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        if (in.readByte() == 0) {
            parentID = null;
        } else {
            parentID = in.readInt();
        }
        phone = in.readString();
        phoneExtn = in.readString();
        city = in.readString();
        priority = in.readString();
        if (in.readByte() == 0) {
            repId = null;
        } else {
            repId = in.readInt();
        }
        pastOrFuture = in.readString();
        lastCompList = in.readString();
        category = in.readString();
        jobTitle = in.readString();
        address1 = in.readString();
        address2 = in.readString();
        stateRegion = in.readString();
        zipPostalCode = in.readString();
        county = in.readString();
        country = in.readString();
        prefix = in.readString();
        fax = in.readString();
        uRL = in.readString();
        phone2 = in.readString();
        byte tmpDoNotFax = in.readByte();
        doNotFax = tmpDoNotFax == 0 ? null : tmpDoNotFax == 1;
        byte tmpDoNotEmail = in.readByte();
        doNotEmail = tmpDoNotEmail == 0 ? null : tmpDoNotEmail == 1;
        if (in.readByte() == 0) {
            canViewEmployeeID = null;
        } else {
            canViewEmployeeID = in.readInt();
        }
        repName = in.readString();
        isAgency = in.readString();
        jobDescription = in.readString();
        budgetPlan = in.readString();
        if (in.readByte() == 0) {
            budgetPlanNum = null;
        } else {
            budgetPlanNum = in.readInt();
        }
    }

    public static final Creator<NearbyContacts> CREATOR = new Creator<NearbyContacts>() {
        @Override
        public NearbyContacts createFromParcel(Parcel in) {
            return new NearbyContacts(in);
        }

        @Override
        public NearbyContacts[] newArray(int size) {
            return new NearbyContacts[size];
        }
    };

    public Integer getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(Integer pageIndex) {
        this.pageIndex = pageIndex;
    }

    public Integer getAutoID() {
        return autoID;
    }

    public void setAutoID(Integer autoID) {
        this.autoID = autoID;
    }

    public Integer getGsCustomersID() {
        return gsCustomersID;
    }

    public void setGsCustomersID(Integer gsCustomersID) {
        this.gsCustomersID = gsCustomersID;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getOrderCustomer() {
        return orderCustomer;
    }

    public void setOrderCustomer(String orderCustomer) {
        this.orderCustomer = orderCustomer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getParentID() {
        return parentID;
    }

    public void setParentID(Integer parentID) {
        this.parentID = parentID;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneExtn() {
        return phoneExtn;
    }

    public void setPhoneExtn(String phoneExtn) {
        this.phoneExtn = phoneExtn;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Integer getRepId() {
        return repId;
    }

    public void setRepId(Integer repId) {
        this.repId = repId;
    }

    public String getPastOrFuture() {
        return pastOrFuture;
    }

    public void setPastOrFuture(String pastOrFuture) {
        this.pastOrFuture = pastOrFuture;
    }

    public String getLastCompList() {
        return lastCompList;
    }

    public void setLastCompList(String lastCompList) {
        this.lastCompList = lastCompList;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getStateRegion() {
        return stateRegion;
    }

    public void setStateRegion(String stateRegion) {
        this.stateRegion = stateRegion;
    }

    public String getZipPostalCode() {
        return zipPostalCode;
    }

    public void setZipPostalCode(String zipPostalCode) {
        this.zipPostalCode = zipPostalCode;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public Object getSuffix() {
        return suffix;
    }

    public void setSuffix(Object suffix) {
        this.suffix = suffix;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public Object getHomephone() {
        return homephone;
    }

    public void setHomephone(Object homephone) {
        this.homephone = homephone;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

    public String getPhone2() {
        return phone2;
    }

    public void setPhone2(String phone2) {
        this.phone2 = phone2;
    }

    public Boolean getDoNotFax() {
        return doNotFax;
    }

    public void setDoNotFax(Boolean doNotFax) {
        this.doNotFax = doNotFax;
    }

    public Boolean getDoNotEmail() {
        return doNotEmail;
    }

    public void setDoNotEmail(Boolean doNotEmail) {
        this.doNotEmail = doNotEmail;
    }

    public Integer getCanViewEmployeeID() {
        return canViewEmployeeID;
    }

    public void setCanViewEmployeeID(Integer canViewEmployeeID) {
        this.canViewEmployeeID = canViewEmployeeID;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getIsAgency() {
        return isAgency;
    }

    public void setIsAgency(String isAgency) {
        this.isAgency = isAgency;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getBudgetPlan() {
        return budgetPlan;
    }

    public void setBudgetPlan(String budgetPlan) {
        this.budgetPlan = budgetPlan;
    }

    public Integer getBudgetPlanNum() {
        return budgetPlanNum;
    }

    public void setBudgetPlanNum(Integer budgetPlanNum) {
        this.budgetPlanNum = budgetPlanNum;
    }

    public Object getLastContract() {
        return lastContract;
    }

    public void setLastContract(Object lastContract) {
        this.lastContract = lastContract;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (pageIndex == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(pageIndex);
        }
        if (autoID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(autoID);
        }
        if (gsCustomersID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(gsCustomersID);
        }
        dest.writeString(customer);
        dest.writeString(orderCustomer);
        dest.writeString(name);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        if (parentID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(parentID);
        }
        dest.writeString(phone);
        dest.writeString(phoneExtn);
        dest.writeString(city);
        dest.writeString(priority);
        if (repId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(repId);
        }
        dest.writeString(pastOrFuture);
        dest.writeString(lastCompList);
        dest.writeString(category);
        dest.writeString(jobTitle);
        dest.writeString(address1);
        dest.writeString(address2);
        dest.writeString(stateRegion);
        dest.writeString(zipPostalCode);
        dest.writeString(county);
        dest.writeString(country);
        dest.writeString(prefix);
        dest.writeString(fax);
        dest.writeString(uRL);
        dest.writeString(phone2);
        dest.writeByte((byte) (doNotFax == null ? 0 : doNotFax ? 1 : 2));
        dest.writeByte((byte) (doNotEmail == null ? 0 : doNotEmail ? 1 : 2));
        if (canViewEmployeeID == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(canViewEmployeeID);
        }
        dest.writeString(repName);
        dest.writeString(isAgency);
        dest.writeString(jobDescription);
        dest.writeString(budgetPlan);
        if (budgetPlanNum == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(budgetPlanNum);
        }
    }

    @Override
    public String toString() {
        return "NearbyContacts{" +
                "pageIndex=" + pageIndex +
                ", autoID=" + autoID +
                ", gsCustomersID=" + gsCustomersID +
                ", customer='" + customer + '\'' +
                ", orderCustomer='" + orderCustomer + '\'' +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", parentID=" + parentID +
                ", phone='" + phone + '\'' +
                ", phoneExtn='" + phoneExtn + '\'' +
                ", city='" + city + '\'' +
                ", priority='" + priority + '\'' +
                ", repId=" + repId +
                ", pastOrFuture='" + pastOrFuture + '\'' +
                ", lastCompList='" + lastCompList + '\'' +
                ", category='" + category + '\'' +
                ", jobTitle='" + jobTitle + '\'' +
                ", address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", stateRegion='" + stateRegion + '\'' +
                ", zipPostalCode='" + zipPostalCode + '\'' +
                ", county='" + county + '\'' +
                ", country='" + country + '\'' +
                ", prefix='" + prefix + '\'' +
                ", suffix=" + suffix +
                ", fax='" + fax + '\'' +
                ", homephone=" + homephone +
                ", uRL='" + uRL + '\'' +
                ", phone2='" + phone2 + '\'' +
                ", doNotFax=" + doNotFax +
                ", doNotEmail=" + doNotEmail +
                ", canViewEmployeeID=" + canViewEmployeeID +
                ", repName='" + repName + '\'' +
                ", isAgency='" + isAgency + '\'' +
                ", jobDescription='" + jobDescription + '\'' +
                ", budgetPlan='" + budgetPlan + '\'' +
                ", budgetPlanNum=" + budgetPlanNum +
                ", lastContract=" + lastContract +
                ", isCheck=" + isCheck +
                '}';
    }
/* private final LatLng mPosition;
    @Override
    public LatLng getPosition() {
        return mPosition;
    }*/
}










