package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BusinessUnitDetails {
    @SerializedName("Details")
    @Expose
    private Object details;
    @SerializedName("Products")
    @Expose
    private Object products;
    @SerializedName("ListOfProducts")
    @Expose
    private Object listOfProducts;
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;

    public Object getDetails() {
        return details;
    }

    public void setDetails(Object details) {
        this.details = details;
    }

    public Object getProducts() {
        return products;
    }

    public void setProducts(Object products) {
        this.products = products;
    }

    public Object getListOfProducts() {
        return listOfProducts;
    }

    public void setListOfProducts(Object listOfProducts) {
        this.listOfProducts = listOfProducts;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "BusinessUnitDetails{" +
                "details=" + details +
                ", products=" + products +
                ", listOfProducts=" + listOfProducts +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
