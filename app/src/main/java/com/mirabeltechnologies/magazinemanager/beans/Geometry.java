package com.mirabeltechnologies.magazinemanager.beans;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by veera on 10/30/2017.
 */

public class Geometry {
    @SerializedName("location")
    @Expose
    private POstalLocation location;

    public POstalLocation getLocation() {
        return location;
    }
    public void setLocation(POstalLocation location) {
        this.location = location;
    }


}
