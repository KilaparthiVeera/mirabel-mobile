package com.mirabeltechnologies.magazinemanager.beans;

import java.io.Serializable;

/**
 * Created by venkat on 3/16/18.
 */

public class SearchTaskListFilter implements Serializable {
    private int assignedToRepId;
    private String assignedToRepName;
    private int assignedByRepId;
    private String assignedByRepName;
    private int priorityId;
    private String priority;
    private int projectId;
    private String project;
    private int status; // Possible Values : 0 - All, 1 - To Do & 2 - Completed

    public SearchTaskListFilter() {
        this.assignedToRepId = -1;
        this.assignedToRepName = "";
        this.assignedByRepId = -1;
        this.assignedByRepName = "";
        this.priorityId = -1;
        this.priority = "";
        this.projectId = -1;
        this.project = "";
        this.status = 1;
    }

    public int getAssignedToRepId() {
        return assignedToRepId;
    }

    public void setAssignedToRepId(int assignedToRepId) {
        this.assignedToRepId = assignedToRepId;
    }

    public String getAssignedToRepName() {
        return assignedToRepName;
    }

    public void setAssignedToRepName(String assignedToRepName) {
        this.assignedToRepName = assignedToRepName;
    }

    public int getAssignedByRepId() {
        return assignedByRepId;
    }

    public void setAssignedByRepId(int assignedByRepId) {
        this.assignedByRepId = assignedByRepId;
    }

    public String getAssignedByRepName() {
        return assignedByRepName;
    }

    public void setAssignedByRepName(String assignedByRepName) {
        this.assignedByRepName = assignedByRepName;
    }

    public int getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(int priorityId) {
        this.priorityId = priorityId;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    @Override
    public String toString() {
        return "SearchTaskListFilter{" +
                "assignedToRepId=" + assignedToRepId +
                ", assignedToRepName='" + assignedToRepName + '\'' +
                ", assignedByRepId=" + assignedByRepId +
                ", assignedByRepName='" + assignedByRepName + '\'' +
                ", priorityId=" + priorityId +
                ", priority='" + priority + '\'' +
                ", projectId=" + projectId +
                ", project='" + project + '\'' +
                ", status=" + status +
                '}';
    }
}
