package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Content {

    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("List")
    @Expose
    private List<CheckInList> list;
    @SerializedName("Data")
    @Expose
    private Object data;
    @SerializedName("Value")
    @Expose
    private Integer value;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<CheckInList> getList() {
        return list;
    }

    public void setList(List<CheckInList> list) {
        this.list = list;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

}
