package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 11/21/17.
 */

public class MasterData implements Serializable {
    @SerializedName("Display")
    @Expose
    private String name;
    @SerializedName("Value")
    @Expose
    private String id;

    private Boolean isSelected=false;

    public Boolean getSelected() {
        return isSelected;
    }

    public void setSelected(Boolean selected) {
        isSelected = selected;
    }

    public MasterData(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "MasterData{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
