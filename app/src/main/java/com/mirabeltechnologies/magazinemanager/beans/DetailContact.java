package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by venkat on 10/13/17.
 */

public class DetailContact implements Serializable {

    @SerializedName("Address1")
    @Expose
    private String address1;
    @SerializedName("Address2")
    @Expose
    private String address2;
    @SerializedName("AlternatePhone")
    @Expose
    private String alternatePhone;
    @SerializedName("CanViewEmployeeID")
    @Expose
    private int canViewEmployeeId;
    @SerializedName("CellPhone")
    @Expose
    private String cellPhone;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("ContactName")
    @Expose
    private String contactName;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("County")
    @Expose
    private String county;
    @SerializedName("CustomerImageUrl")
    @Expose
    private String customerImageUrl;
    @SerializedName("CustomerName")
    @Expose
    private String customerName;
    @SerializedName("CustomerType")
    @Expose
    private String customerType;
    @SerializedName("CustomerTypeID")
    @Expose
    private String customerTypeId;
    @SerializedName("CustomersID")
    @Expose
    private String customersId;
    @SerializedName("DateAdded")
    @Expose
    private String dateAdded;
    @SerializedName("DonotEmail")
    @Expose
    private Boolean donotEmail;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Email2")
    @Expose
    private String email2;
    @SerializedName("FacebookId")
    @Expose
    private String facebookId;
    @SerializedName("Fax")
    @Expose
    private String fax;
    @SerializedName("FirstName")
    @Expose
    private String firstName;
    @SerializedName("GooglePlus")
    @Expose
    private String googlePlus;
    @SerializedName("HomeFax")
    @Expose
    private String homeFax;
    @SerializedName("International")
    @Expose
    private String international;
    @SerializedName("IsAdAgency")
    @Expose
    private Boolean isAdAgency;
    @SerializedName("JobDescription")
    @Expose
    private String jobDescription;
    @SerializedName("LastName")
    @Expose
    private String lastName;
    @SerializedName("LinkedIn")
    @Expose
    private String linkedIn;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("NickName")
    @Expose
    private String nickName;
    @SerializedName("ParentID")
    @Expose
    private Long parentId;
    @SerializedName("PartnerRepCount")
    @Expose
    private Long partnerRepCount;
    @SerializedName("PartnerReps")
    @Expose
    private String partnerReps;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("PhoneExt")
    @Expose
    private String phoneExt;
    @SerializedName("Prefix")
    @Expose
    private String prefix;
    @SerializedName("PriorityName")
    @Expose
    private String priorityName;
    @SerializedName("RepId")
    @Expose
    private Long repId;
    @SerializedName("RepName")
    @Expose
    private String repName;
    @SerializedName("Spouse")
    @Expose
    private String spouse;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Suffix")
    @Expose
    private String suffix;
    @SerializedName("Title")
    @Expose
    private String title;
    @SerializedName("TotalRecords")
    @Expose
    private Long totalRecords;
    @SerializedName("TwitterHandle")
    @Expose
    private String twitterHandle;
    @SerializedName("Url")
    @Expose
    private String url;
    @SerializedName("birthdate")
    @Expose
    private String birthdate;
    @SerializedName("homeCity")
    @Expose
    private String homeCity;
    @SerializedName("homeInternational")
    @Expose
    private String homeInternational;
    @SerializedName("homeSt")
    @Expose
    private String homeSt;
    @SerializedName("homeStreet")
    @Expose
    private String homeStreet;
    @SerializedName("homeStreet2")
    @Expose
    private String homeStreet2;
    @SerializedName("homeZip")
    @Expose
    private String homeZip;
    @SerializedName("homephone")
    @Expose
    private String homephone;
    @SerializedName("isLastContractStatus")
    @Expose
    private String isLastContractStatus;
    @SerializedName("priority")
    @Expose
    private Long priority;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("HasBillingContact")
    @Expose
    private Boolean hasBillingContact;
    @SerializedName("HasContacts")
    @Expose
    private Boolean hasContacts;
    @Expose
    private ContactData contactData;
    @Expose
    private List<ContactOrder> contactOrders;

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAlternatePhone() {
        return alternatePhone;
    }

    public void setAlternatePhone(String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    public int getCanViewEmployeeId() {
        return canViewEmployeeId;
    }

    public void setCanViewEmployeeId(int canViewEmployeeId) {
        this.canViewEmployeeId = canViewEmployeeId;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    @Override
    public String toString() {
        return "DetailContact{" +
                "address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", alternatePhone='" + alternatePhone + '\'' +
                ", canViewEmployeeId=" + canViewEmployeeId +
                ", cellPhone='" + cellPhone + '\'' +
                ", city='" + city + '\'' +
                ", companyName='" + companyName + '\'' +
                ", contactName='" + contactName + '\'' +
                ", country='" + country + '\'' +
                ", county='" + county + '\'' +
                ", customerImageUrl='" + customerImageUrl + '\'' +
                ", customerName='" + customerName + '\'' +
                ", customerType='" + customerType + '\'' +
                ", customerTypeId='" + customerTypeId + '\'' +
                ", customersId='" + customersId + '\'' +
                ", dateAdded='" + dateAdded + '\'' +
                ", donotEmail=" + donotEmail +
                ", email='" + email + '\'' +
                ", email2='" + email2 + '\'' +
                ", facebookId='" + facebookId + '\'' +
                ", fax='" + fax + '\'' +
                ", firstName='" + firstName + '\'' +
                ", googlePlus='" + googlePlus + '\'' +
                ", homeFax='" + homeFax + '\'' +
                ", international='" + international + '\'' +
                ", isAdAgency=" + isAdAgency +
                ", jobDescription='" + jobDescription + '\'' +
                ", lastName='" + lastName + '\'' +
                ", linkedIn='" + linkedIn + '\'' +
                ", name='" + name + '\'' +
                ", nickName='" + nickName + '\'' +
                ", parentId=" + parentId +
                ", partnerRepCount=" + partnerRepCount +
                ", partnerReps='" + partnerReps + '\'' +
                ", phone='" + phone + '\'' +
                ", phoneExt='" + phoneExt + '\'' +
                ", prefix='" + prefix + '\'' +
                ", priorityName='" + priorityName + '\'' +
                ", repId=" + repId +
                ", repName='" + repName + '\'' +
                ", spouse='" + spouse + '\'' +
                ", state='" + state + '\'' +
                ", suffix='" + suffix + '\'' +
                ", title='" + title + '\'' +
                ", totalRecords=" + totalRecords +
                ", twitterHandle='" + twitterHandle + '\'' +
                ", url='" + url + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", homeCity='" + homeCity + '\'' +
                ", homeInternational='" + homeInternational + '\'' +
                ", homeSt='" + homeSt + '\'' +
                ", homeStreet='" + homeStreet + '\'' +
                ", homeStreet2='" + homeStreet2 + '\'' +
                ", homeZip='" + homeZip + '\'' +
                ", homephone='" + homephone + '\'' +
                ", isLastContractStatus='" + isLastContractStatus + '\'' +
                ", priority=" + priority +
                ", zip='" + zip + '\'' +
                ", hasBillingContact=" + hasBillingContact +
                ", hasContacts=" + hasContacts +
                ", contactData=" + contactData +
                ", contactOrders=" + contactOrders +
                '}';
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getCustomerImageUrl() {
        return customerImageUrl;
    }

    public void setCustomerImageUrl(String customerImageUrl) {
        this.customerImageUrl = customerImageUrl;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomersId() {
        return customersId;
    }

    public void setCustomersId(String customersId) {
        this.customersId = customersId;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Boolean getDonotEmail() {
        return donotEmail;
    }

    public void setDonotEmail(Boolean donotEmail) {
        this.donotEmail = donotEmail;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getFacebookId() {
        return facebookId;
    }

    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGooglePlus() {
        return googlePlus;
    }

    public void setGooglePlus(String googlePlus) {
        this.googlePlus = googlePlus;
    }

    public String getHomeFax() {
        return homeFax;
    }

    public void setHomeFax(String homeFax) {
        this.homeFax = homeFax;
    }

    public String getInternational() {
        return international;
    }

    public void setInternational(String international) {
        this.international = international;
    }

    public Boolean getAdAgency() {
        return isAdAgency;
    }

    public void setAdAgency(Boolean adAgency) {
        isAdAgency = adAgency;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLinkedIn() {
        return linkedIn;
    }

    public void setLinkedIn(String linkedIn) {
        this.linkedIn = linkedIn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Long getPartnerRepCount() {
        return partnerRepCount;
    }

    public void setPartnerRepCount(Long partnerRepCount) {
        this.partnerRepCount = partnerRepCount;
    }

    public String getPartnerReps() {
        return partnerReps;
    }

    public void setPartnerReps(String partnerReps) {
        this.partnerReps = partnerReps;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoneExt() {
        return phoneExt;
    }

    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPriorityName() {
        return priorityName;
    }

    public void setPriorityName(String priorityName) {
        this.priorityName = priorityName;
    }

    public Long getRepId() {
        return repId;
    }

    public void setRepId(Long repId) {
        this.repId = repId;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(Long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getTwitterHandle() {
        return twitterHandle;
    }

    public void setTwitterHandle(String twitterHandle) {
        this.twitterHandle = twitterHandle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getHomeCity() {
        return homeCity;
    }

    public void setHomeCity(String homeCity) {
        this.homeCity = homeCity;
    }

    public String getHomeInternational() {
        return homeInternational;
    }

    public void setHomeInternational(String homeInternational) {
        this.homeInternational = homeInternational;
    }

    public String getHomeSt() {
        return homeSt;
    }

    public void setHomeSt(String homeSt) {
        this.homeSt = homeSt;
    }

    public String getHomeStreet() {
        return homeStreet;
    }

    public void setHomeStreet(String homeStreet) {
        this.homeStreet = homeStreet;
    }

    public String getHomeStreet2() {
        return homeStreet2;
    }

    public void setHomeStreet2(String homeStreet2) {
        this.homeStreet2 = homeStreet2;
    }

    public String getHomeZip() {
        return homeZip;
    }

    public void setHomeZip(String homeZip) {
        this.homeZip = homeZip;
    }

    public String getHomephone() {
        return homephone;
    }

    public void setHomephone(String homephone) {
        this.homephone = homephone;
    }

    public String getIsLastContractStatus() {
        return isLastContractStatus;
    }

    public void setIsLastContractStatus(String isLastContractStatus) {
        this.isLastContractStatus = isLastContractStatus;
    }

    public Long getPriority() {
        return priority;
    }

    public void setPriority(Long priority) {
        this.priority = priority;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public Boolean getHasBillingContact() {
        return hasBillingContact;
    }

    public void setHasBillingContact(Boolean hasBillingContact) {
        this.hasBillingContact = hasBillingContact;
    }

    public Boolean getHasContacts() {
        return hasContacts;
    }

    public void setHasContacts(Boolean hasContacts) {
        this.hasContacts = hasContacts;
    }

    public ContactData getContactData() {
        return contactData;
    }

    public void setContactData(ContactData contactData) {
        this.contactData = contactData;
    }

    public List<ContactOrder> getContactOrders() {
        return contactOrders;
    }

    public void setContactOrders(List<ContactOrder> contactOrders) {
        this.contactOrders = contactOrders;
    }

}
