package com.mirabeltechnologies.magazinemanager.beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by venkat on 12/21/17.
 */

public class NewActivity implements Parcelable {
    private String customerId;
    private String actionType; // possible values note, call or meeting
    private String activityTypeId;
    private String activityType;
    private String date;
    private String time;
    private String assignedByRepId;
    private String assignedByRepName;
    private String assignedToRepId;
    private String assignedToRepName;
    private String notes;
    private boolean isPrivate;
    private boolean isAddToTaskList;

    public NewActivity(String customerId) {
        this.customerId = customerId;
        this.actionType = "";
        this.activityTypeId = "-1";
        this.activityType = "";
        this.date = "";
        this.time = "";
        this.assignedByRepId = "";
        this.assignedByRepName = "";
        this.assignedToRepId = "";
        this.assignedToRepName = "";
        this.notes = "";
        this.isPrivate = false;
        this.isAddToTaskList = false;
    }

    protected NewActivity(Parcel in) {
        customerId = in.readString();
        actionType = in.readString();
        activityTypeId = in.readString();
        activityType = in.readString();
        date = in.readString();
        time = in.readString();
        assignedByRepId = in.readString();
        assignedByRepName = in.readString();
        assignedToRepId = in.readString();
        assignedToRepName = in.readString();
        notes = in.readString();
        isPrivate = in.readByte() != 0;
        isAddToTaskList = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(customerId);
        dest.writeString(actionType);
        dest.writeString(activityTypeId);
        dest.writeString(activityType);
        dest.writeString(date);
        dest.writeString(time);
        dest.writeString(assignedByRepId);
        dest.writeString(assignedByRepName);
        dest.writeString(assignedToRepId);
        dest.writeString(assignedToRepName);
        dest.writeString(notes);
        dest.writeByte((byte) (isPrivate ? 1 : 0));
        dest.writeByte((byte) (isAddToTaskList ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NewActivity> CREATOR = new Creator<NewActivity>() {
        @Override
        public NewActivity createFromParcel(Parcel in) {
            return new NewActivity(in);
        }

        @Override
        public NewActivity[] newArray(int size) {
            return new NewActivity[size];
        }
    };

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActivityTypeId() {
        return activityTypeId;
    }

    public void setActivityTypeId(String activityTypeId) {
        this.activityTypeId = activityTypeId;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getAssignedByRepId() {
        return assignedByRepId;
    }

    public void setAssignedByRepId(String assignedByRepId) {
        this.assignedByRepId = assignedByRepId;
    }

    public String getAssignedByRepName() {
        return assignedByRepName;
    }

    public void setAssignedByRepName(String assignedByRepName) {
        this.assignedByRepName = assignedByRepName;
    }

    public String getAssignedToRepId() {
        return assignedToRepId;
    }

    public void setAssignedToRepId(String assignedToRepId) {
        this.assignedToRepId = assignedToRepId;
    }

    public String getAssignedToRepName() {
        return assignedToRepName;
    }

    public void setAssignedToRepName(String assignedToRepName) {
        this.assignedToRepName = assignedToRepName;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public boolean isAddToTaskList() {
        return isAddToTaskList;
    }

    public void setAddToTaskList(boolean addToTaskList) {
        isAddToTaskList = addToTaskList;
    }

    @Override
    public String toString() {
        return "NewActivity{" +
                "customerId='" + customerId + '\'' +
                ", actionType='" + actionType + '\'' +
                ", activityTypeId='" + activityTypeId + '\'' +
                ", activityType='" + activityType + '\'' +
                ", date='" + date + '\'' +
                ", time='" + time + '\'' +
                ", assignedByRepId='" + assignedByRepId + '\'' +
                ", assignedByRepName='" + assignedByRepName + '\'' +
                ", assignedToRepId='" + assignedToRepId + '\'' +
                ", assignedToRepName='" + assignedToRepName + '\'' +
                ", notes='" + notes + '\'' +
                ", isPrivate=" + isPrivate +
                ", isAddToTaskList=" + isAddToTaskList +
                '}';
    }
}
