package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductDetails {

    @SerializedName("AvgPageRate")
    @Expose
    private Double avgPageRate;
    @SerializedName("ScheduleDetails")
    @Expose
    private Object scheduleDetails;
    @SerializedName("Circulation")
    @Expose
    private Double circulation;
    @SerializedName("AccountQB")
    @Expose
    private Object accountQB;
    @SerializedName("NEP")
    @Expose
    private Double nep;
    @SerializedName("ListOfRateCards")
    @Expose
    private Object listOfRateCards;
    @SerializedName("ListOfMagazines")
    @Expose
    private Object listOfMagazines;
    @SerializedName("MappedRateCards")
    @Expose
    private Object mappedRateCards;
    @SerializedName("DefaultRateCardID")
    @Expose
    private Integer defaultRateCardID;
    @SerializedName("MappedMagazines")
    @Expose
    private Object mappedMagazines;
    @SerializedName("BusinessUnitDetails")
    @Expose
    private Object businessUnitDetails;
    @SerializedName("ListOfProductsByBusinessUnit")
    @Expose
    private Object listOfProductsByBusinessUnit;
    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Abbreviation")
    @Expose
    private Object abbreviation;
    @SerializedName("SubProductType")
    @Expose
    private Object subProductType;
    @SerializedName("IsSelected")
    @Expose
    private Boolean isSelected;
    @SerializedName("IsActive")
    @Expose
    private Boolean isActive;
    @SerializedName("IsOrdersDependency")
    @Expose
    private Boolean isOrdersDependency;
    @SerializedName("IsProposalDependency")
    @Expose
    private Boolean isProposalDependency;

    public double getAvgPageRate() {
        return avgPageRate;
    }

    public void setAvgPageRate(double avgPageRate) {
        this.avgPageRate = avgPageRate;
    }

    public Object getScheduleDetails() {
        return scheduleDetails;
    }

    public void setScheduleDetails(Object scheduleDetails) {
        this.scheduleDetails = scheduleDetails;
    }

    public double getCirculation() {
        return circulation;
    }

    public void setCirculation(double circulation) {
        this.circulation = circulation;
    }

    public Object getAccountQB() {
        return accountQB;
    }

    public void setAccountQB(Object accountQB) {
        this.accountQB = accountQB;
    }

    public double getNep() {
        return nep;
    }

    public void setNep(double nep) {
        this.nep = nep;
    }

    public Object getListOfRateCards() {
        return listOfRateCards;
    }

    public void setListOfRateCards(Object listOfRateCards) {
        this.listOfRateCards = listOfRateCards;
    }

    public Object getListOfMagazines() {
        return listOfMagazines;
    }

    public void setListOfMagazines(Object listOfMagazines) {
        this.listOfMagazines = listOfMagazines;
    }

    public Object getMappedRateCards() {
        return mappedRateCards;
    }

    public void setMappedRateCards(Object mappedRateCards) {
        this.mappedRateCards = mappedRateCards;
    }

    public Integer getDefaultRateCardID() {
        return defaultRateCardID;
    }

    public void setDefaultRateCardID(Integer defaultRateCardID) {
        this.defaultRateCardID = defaultRateCardID;
    }

    public Object getMappedMagazines() {
        return mappedMagazines;
    }

    public void setMappedMagazines(Object mappedMagazines) {
        this.mappedMagazines = mappedMagazines;
    }

    public Object getBusinessUnitDetails() {
        return businessUnitDetails;
    }

    public void setBusinessUnitDetails(Object businessUnitDetails) {
        this.businessUnitDetails = businessUnitDetails;
    }

    public Object getListOfProductsByBusinessUnit() {
        return listOfProductsByBusinessUnit;
    }

    public void setListOfProductsByBusinessUnit(Object listOfProductsByBusinessUnit) {
        this.listOfProductsByBusinessUnit = listOfProductsByBusinessUnit;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(Object abbreviation) {
        this.abbreviation = abbreviation;
    }

    public Object getSubProductType() {
        return subProductType;
    }

    public void setSubProductType(Object subProductType) {
        this.subProductType = subProductType;
    }

    public Boolean getIsSelected() {
        return isSelected;
    }

    public void setIsSelected(Boolean isSelected) {
        this.isSelected = isSelected;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Boolean getIsOrdersDependency() {
        return isOrdersDependency;
    }

    public void setIsOrdersDependency(Boolean isOrdersDependency) {
        this.isOrdersDependency = isOrdersDependency;
    }

    public Boolean getIsProposalDependency() {
        return isProposalDependency;
    }

    public void setIsProposalDependency(Boolean isProposalDependency) {
        this.isProposalDependency = isProposalDependency;
    }

    @Override
    public String toString() {
        return "ProductDetails{" +
                "avgPageRate=" + avgPageRate +
                ", scheduleDetails=" + scheduleDetails +
                ", circulation=" + circulation +
                ", accountQB=" + accountQB +
                ", nep=" + nep +
                ", listOfRateCards=" + listOfRateCards +
                ", listOfMagazines=" + listOfMagazines +
                ", mappedRateCards=" + mappedRateCards +
                ", defaultRateCardID=" + defaultRateCardID +
                ", mappedMagazines=" + mappedMagazines +
                ", businessUnitDetails=" + businessUnitDetails +
                ", listOfProductsByBusinessUnit=" + listOfProductsByBusinessUnit +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", abbreviation=" + abbreviation +
                ", subProductType=" + subProductType +
                ", isSelected=" + isSelected +
                ", isActive=" + isActive +
                ", isOrdersDependency=" + isOrdersDependency +
                ", isProposalDependency=" + isProposalDependency +
                '}';
    }
}
