package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by venkat on 3/26/18.
 */

public class ContactOrderItem implements Serializable {
    @SerializedName("Barter")
    @Expose
    private float barter;
    @SerializedName("BillingContact")
    @Expose
    private String billingContact;
    @SerializedName("CompanyName")
    @Expose
    private String companyName;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Gross")
    @Expose
    private float gross;
    @SerializedName("ID")
    @Expose
    private int ContractId;
    @SerializedName("Net")
    @Expose
    private float net;
    @SerializedName("OrdersCount")
    @Expose
    private int ordersCount;
    @SerializedName("Product")
    @Expose
    private String product;
    @SerializedName("ProductID")
    @Expose
    private int productId;
    @SerializedName("ProductType")
    @Expose
    private String productType;
    @SerializedName("ProductionCharges")
    @Expose
    private float productionCharges;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("SubProductTypeID")
    @Expose
    private String subProductTypeID;// Possible values 3 - Digital && 1 - Print

    public float getBarter() {
        return barter;
    }

    public String getBillingContact() {
        return billingContact;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getDescription() {
        return description;
    }

    public float getGross() {
        return gross;
    }

    public int getContractId() {
        return ContractId;
    }

    public float getNet() {
        return net;
    }

    public int getOrdersCount() {
        return ordersCount;
    }

    public String getProduct() {
        return product;
    }

    public int getProductId() {
        return productId;
    }

    public String getProductType() {
        return productType;
    }

    public float getProductionCharges() {
        return productionCharges;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getSubProductTypeID() {
        return subProductTypeID;
    }

    @Override
    public String toString() {
        return "ContactOrderItem{" +
                "barter=" + barter +
                ", billingContact='" + billingContact + '\'' +
                ", companyName='" + companyName + '\'' +
                ", description='" + description + '\'' +
                ", gross=" + gross +
                ", ContractId=" + ContractId +
                ", net=" + net +
                ", ordersCount=" + ordersCount +
                ", product='" + product + '\'' +
                ", productId=" + productId +
                ", productType='" + productType + '\'' +
                ", productionCharges=" + productionCharges +
                ", startDate='" + startDate + '\'' +
                ", subProductTypeID='" + subProductTypeID + '\'' +
                '}';
    }
}
