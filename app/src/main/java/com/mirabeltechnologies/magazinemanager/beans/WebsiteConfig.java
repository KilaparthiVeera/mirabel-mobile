package com.mirabeltechnologies.magazinemanager.beans;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class WebsiteConfig implements Serializable {
    @SerializedName("LimitCustomerAddByRep")
    @Expose
    private boolean limitCustomerAddByRep;
    @SerializedName("LimitCustomerSearchByRep")
    @Expose
    private boolean limitCustomerSearchByRep;

    public boolean isLimitCustomerAddByRep() {
        return limitCustomerAddByRep;
    }

    public void setLimitCustomerAddByRep(boolean limitCustomerAddByRep) {
        this.limitCustomerAddByRep = limitCustomerAddByRep;
    }

    public boolean isLimitCustomerSearchByRep() {
        return limitCustomerSearchByRep;
    }

    public void setLimitCustomerSearchByRep(boolean limitCustomerSearchByRep) {
        this.limitCustomerSearchByRep = limitCustomerSearchByRep;
    }

    @Override
    public String toString() {
        return "WebsiteConfig{" +
                "limitCustomerAddByRep=" + limitCustomerAddByRep +
                ", limitCustomerSearchByRep=" + limitCustomerSearchByRep +
                '}';
    }
}
