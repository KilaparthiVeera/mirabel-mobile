package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

public class PostalDetails implements Comparable<PostalDetails> {

    private float distance;
    private String pcode;

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public String getPcode() {
        return pcode;
    }

    public void setPcode(String pcode) {

        this.pcode = pcode;
    }

    @NonNull
    @Override
    public String toString() {
        return "PostalDetails{" +
                "distance=" + distance +
                ", pcode='" + pcode + '\'' +
                '}';
    }


    @Override
    public int compareTo(@NonNull PostalDetails comparestu) {

        int compareage= (int) comparestu.getDistance();
        /* For Ascending order*/
        return (int) this.distance-compareage;
    }
}
