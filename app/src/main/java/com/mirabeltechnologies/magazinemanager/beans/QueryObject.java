package com.mirabeltechnologies.magazinemanager.beans;

/**
 * Created by venkat on 12/1/17.
 */

public class QueryObject {
    private String keyword;
    private String typeOfFilter;

    public QueryObject() {

    }

    public QueryObject(String keyword, String typeOfFilter) {
        this.keyword = keyword;
        this.typeOfFilter = typeOfFilter;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getTypeOfFilter() {
        return typeOfFilter;
    }

    public void setTypeOfFilter(String typeOfFilter) {
        this.typeOfFilter = typeOfFilter;
    }

    @Override
    public String toString() {
        return "QueryObject{" +
                "keyword='" + keyword + '\'' +
                ", typeOfFilter='" + typeOfFilter + '\'' +
                '}';
    }
}
