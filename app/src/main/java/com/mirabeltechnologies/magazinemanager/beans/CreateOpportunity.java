package com.mirabeltechnologies.magazinemanager.beans;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CreateOpportunity implements Serializable {

    @SerializedName("ID")
    @Expose
    private Integer id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Status")
    @Expose
    private String status;
    @SerializedName("CloseDate")
    @Expose
    private String closeDate;
    @SerializedName("Amount")
    @Expose
    private String amount;
    @SerializedName("Probability")
    @Expose
    private Integer probability;
    @SerializedName("Source")
    @Expose
    private String source;
    @SerializedName("Notes")
    @Expose
    private String notes;
    @SerializedName("NextStep")
    @Expose
    private String nextStep;
    @SerializedName("CreatedDate")
    @Expose
    private String createdDate;
    @SerializedName("ModfiedDate")
    @Expose
    private String modfiedDate;
    @SerializedName("OppStageDetails")
    @Expose
    private OppStageDetails oppStageDetails;
    @SerializedName("ContactDetails")
    @Expose
    private ContactDetails contactDetails;
    @SerializedName("SubContactDetails")
    @Expose
    private SubContactDetails subContactDetails;
    @SerializedName("ProductDetails")
    @Expose
    private ProductDetails productDetails;
    @SerializedName("OwnerDetails")
    @Expose
    private OwnerDetails ownerDetails;
    @SerializedName("AssignedTODetails")
    @Expose
    private AssignDetails assignedTODetails;
    @SerializedName("OppTypeDetails")
    @Expose
    private OppTypeDetails oppTypeDetails;
    @SerializedName("BusinessUnitDetails")
    @Expose
    private BusinessUnitDetails businessUnitDetails;
    @SerializedName("OppLossReasonDetails")
    @Expose
    private OppLossReasonDetails oppLossReasonDetails;
    @SerializedName("ProposalID")
    @Expose
    private String proposalID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(String closeDate) {
        this.closeDate = closeDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNextStep() {
        return nextStep;
    }

    public void setNextStep(String nextStep) {
        this.nextStep = nextStep;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getModfiedDate() {
        return modfiedDate;
    }

    public void setModfiedDate(String modfiedDate) {
        this.modfiedDate = modfiedDate;
    }

    public OppStageDetails getOppStageDetails() {
        return oppStageDetails;
    }

    public void setOppStageDetails(OppStageDetails oppStageDetails) {
        this.oppStageDetails = oppStageDetails;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public SubContactDetails getSubContactDetails() {
        return subContactDetails;
    }

    public void setSubContactDetails(SubContactDetails subContactDetails) {
        this.subContactDetails = subContactDetails;
    }

    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    public OwnerDetails getOwnerDetails() {
        return ownerDetails;
    }

    public void setOwnerDetails(OwnerDetails ownerDetails) {
        this.ownerDetails = ownerDetails;
    }

    public AssignDetails getAssignedTODetails() {
        return assignedTODetails;
    }

    public void setAssignedTODetails(AssignDetails assignedTODetails) {
        this.assignedTODetails = assignedTODetails;
    }

    public OppTypeDetails getOppTypeDetails() {
        return oppTypeDetails;
    }

    public void setOppTypeDetails(OppTypeDetails oppTypeDetails) {
        this.oppTypeDetails = oppTypeDetails;
    }

    public BusinessUnitDetails getBusinessUnitDetails() {
        return businessUnitDetails;
    }

    public void setBusinessUnitDetails(BusinessUnitDetails businessUnitDetails) {
        this.businessUnitDetails = businessUnitDetails;
    }

    public OppLossReasonDetails getOppLossReasonDetails() {
        return oppLossReasonDetails;
    }

    public void setOppLossReasonDetails(OppLossReasonDetails oppLossReasonDetails) {
        this.oppLossReasonDetails = oppLossReasonDetails;
    }

    public String getProposalID() {
        return proposalID;
    }

    public void setProposalID(String proposalID) {
        this.proposalID = proposalID;
    }


    @NonNull
    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", closeDate='" + closeDate + '\'' +
                ", amount='" + amount + '\'' +
                ", probability=" + probability +
                ", source='" + source + '\'' +
                ", notes='" + notes + '\'' +
                ", nextStep='" + nextStep + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", modfiedDate='" + modfiedDate + '\'' +
                ", oppStageDetails=" + oppStageDetails +
                ", contactDetails=" + contactDetails +
                ", subContactDetails=" + subContactDetails +
                ", productDetails=" + productDetails +
                ", ownerDetails=" + ownerDetails +
                ", assignedTODetails=" + assignedTODetails +
                ", oppTypeDetails=" + oppTypeDetails +
                ", businessUnitDetails=" + businessUnitDetails +
                ", oppLossReasonDetails=" + oppLossReasonDetails +
                ", proposalID='" + proposalID + '\'' +
                '}';
    }
}
