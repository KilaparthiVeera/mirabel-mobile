package com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.content.ContextCompat;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.AssignRepData;

import java.util.ArrayList;
import java.util.List;

public class AssignRepMultipleSearchFilter extends AppCompatSpinner implements DialogInterface.OnCancelListener {

    private List<AssignRepData> items;
    //private boolean[] selected;
    private String defaultText;
    MyAdapter adapter;
   public static String spinnerText;


    public AssignRepMultipleSearchFilter(@NonNull Context context) {
        super(context);
    }

    public AssignRepMultipleSearchFilter(Context arg0, AttributeSet arg1) {
        super(arg0, arg1);
    }

    public AssignRepMultipleSearchFilter(Context arg0, AttributeSet arg1, int arg2) {
        super(arg0, arg1, arg2);
    }


    @Override
    public void onCancel(DialogInterface dialog) {
        // refresh text on spinner

        StringBuilder spinnerBuffer = new StringBuilder();

        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getSelected()) {
                spinnerBuffer.append(items.get(i).getName());
                spinnerBuffer.append(", ");
            }
        }

        spinnerText = spinnerBuffer.toString();
        if (spinnerText.length() > 2)
            spinnerText = spinnerText.substring(0, spinnerText.length() - 2);
        else
            spinnerText = defaultText;


        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(getContext(), R.layout.spinner_row_item,
                new String[]{spinnerText});
        setAdapter(adapterSpinner);

        if (adapter != null)
            adapter.notifyDataSetChanged();

    }

    @Override
    public boolean performClick() {
        TextView textView = new TextView(getContext());
        textView.setText(defaultText);
        textView.setGravity(Gravity.CENTER);
        textView.setPadding(30, 30, 30, 30);
        textView.setTextSize(18F);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        textView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.color_primary));
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setCustomTitle(textView);
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = inflater.inflate(R.layout.alert_dialog_listview_search, null);
        builder.setView(view);


        final ListView listView = view.findViewById(R.id.alertSearchListView);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setFastScrollEnabled(false);
        adapter = new MyAdapter(getContext(), items);
        listView.setAdapter(adapter);


        EditText editText = view.findViewById(R.id.alertSearchEditText);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        //builder.setMultiChoiceItems(items.toArray(new CharSequence[items.size()]), selected, this);
        builder.setPositiveButton(android.R.string.ok,
                (DialogInterface dialog, int which) -> {
//              items = (ArrayList<KeyPairBoolData>) adapter.arrayList;
                    Log.i("TAG", " ITEMS : " + items.size());
                    dialog.cancel();
                });

        builder.setNeutralButton(R.string.clear_all_label, (dialogInterface, which) -> {
            for (int i = 0; i < items.size(); i++) {
                items.get(i).setSelected(false);
            }
            dialogInterface.cancel();

        });
        builder.setOnCancelListener(this);
        builder.show();
        return true;
    }

    public void setItems(List<AssignRepData> items, String allText, int position) {

        this.items = items;
        this.defaultText = allText;

        ArrayAdapter<String> adapterSpinner = new ArrayAdapter<>(getContext(),
                R.layout.spinner_row_item,
                new String[]{defaultText});
        setAdapter(adapterSpinner);
        if (position != -1) {
            items.get(position).setSelected(true);
            //listener.onItemsSelected(items);
            onCancel(null);
        }
    }

    public List<AssignRepData> getSelectedItems() {
        List<AssignRepData> oppTypeDetails = new ArrayList<>();
        if(items!=null && !items.isEmpty()){
            for (int i = 0; i < items.size(); ++i) {
                if (items.get(i).getSelected()) {
                    oppTypeDetails.add(items.get(i));
                }
            }
        }
        return oppTypeDetails;
    }

    public static class MyAdapter extends BaseAdapter implements Filterable {

        List<AssignRepData> arrayList;
        List<AssignRepData> mOriginalValues; // Original Values
        LayoutInflater inflater;

        public MyAdapter(Context context, List<AssignRepData> arrayList) {
            this.arrayList = arrayList;
            inflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return arrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private static class ViewHolder {

            TextView textView;
            CheckBox checkBox;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {


           ViewHolder holder;

            if (convertView == null) {

                holder = new ViewHolder();
                convertView = inflater.inflate(R.layout.multiselect_row_layout, null);
                holder.textView = convertView.findViewById(R.id.alertTextView);
                holder.checkBox = convertView.findViewById(R.id.alertCheckbox);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            final AssignRepData data = arrayList.get(position);

            holder.textView.setText(data.getName());
            holder.checkBox.setChecked(data.getSelected());

            holder.checkBox.setOnClickListener(v -> {
                int len = arrayList.size();
                for (int i = 0; i < len; i++) {
                    if (i == position) {
                        data.setSelected(!data.getSelected());
                        Log.i("TAG", "On Click Selected : " + data.getName() + " : " + data.getSelected());
                        break;
                    }
                }
            });


            convertView.setOnClickListener(v -> {
                ViewHolder temp = (ViewHolder) v.getTag();
                temp.checkBox.setChecked(!temp.checkBox.isChecked());

                int len = arrayList.size();
                for (int i = 0; i < len; i++) {
                    if (i == position) {
                        data.setSelected(!data.getSelected());
                        Log.i("TAG", "On Click Selected : " + data.getName() + " : " + data.getSelected());
                        break;
                    }
                }
            });

            holder.checkBox.setTag(holder);

            return convertView;
        }

        @SuppressLint("DefaultLocale")
        @Override
        public Filter getFilter() {
            return new Filter() {
                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    arrayList = (List<AssignRepData>) results.values; // has the filtered values
                    notifyDataSetChanged();  // notifies the data with new filtered values
                }

                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults results = new FilterResults();        // Holds the results of a filtering operation in values
                    List<AssignRepData> FilteredArrList = new ArrayList<>();

                    if (mOriginalValues == null) {
                        mOriginalValues = new ArrayList<>(arrayList); // saves the original data in mOriginalValues
                    }


                    if (constraint == null || constraint.length() == 0) {

                        // set the Original result to return
                        results.count = mOriginalValues.size();
                        results.values = mOriginalValues;
                    } else {
                        constraint = constraint.toString().toLowerCase();
                        for (int i = 0; i < mOriginalValues.size(); i++) {
                            Log.i("TAG", "Filter : " + mOriginalValues.get(i).getName() + " -> " + mOriginalValues.get(i).getSelected());
                            String data = mOriginalValues.get(i).getName();
                            if (data.toLowerCase().contains(constraint.toString())) {
                                FilteredArrList.add(mOriginalValues.get(i));
                            }
                        }
                        // set the Filtered result to return
                        results.count = FilteredArrList.size();
                        results.values = FilteredArrList;
                    }
                    return results;
                }
            };
        }
    }
}


