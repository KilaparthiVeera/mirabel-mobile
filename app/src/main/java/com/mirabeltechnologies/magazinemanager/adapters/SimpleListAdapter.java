package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.RightSideDrawerListActivity;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.List;

/**
 * Created by venkat on 24/06/16.
 */
public class SimpleListAdapter extends ArrayAdapter {
    private Context context;
    private int resourceId;
    private LayoutInflater layoutInflater;
    private final List values;
    private Constants.SelectionType selectionType;

    public SimpleListAdapter(Context ctx, int resource, List objects, Constants.SelectionType selectionType) {
        super(ctx, resource, objects);

        this.context = ctx;
        this.resourceId = resource;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.values = objects;
        this.selectionType = selectionType;
    }

    static class ViewHolder {
        MMTextView textView;
    }

    @Override
    public int getCount() {
        if (values.size() <= 0)
            return 1;
        else
            return values.size();
    }

    @Override
    public Object getItem(int position) {
        return values.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(resourceId, parent, false);

                viewHolder = new ViewHolder();
                viewHolder.textView = (MMTextView) convertView.findViewById(R.id.textView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            if (values.size() <= 0) {
                viewHolder.textView.setText("No Data Found.");
                viewHolder.textView.setGravity(Gravity.CENTER);
            } else {
                viewHolder.textView.setGravity(Gravity.LEFT);

                if (selectionType == Constants.SelectionType.CLIENT) {
                    ClientInfo clientInfo = (ClientInfo) getItem(position);
                    viewHolder.textView.setText(clientInfo.getClientName());

                    if (position == 0) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.white));
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.client_list_text_color));
                    }
                } else if (selectionType == Constants.SelectionType.REP) {
                    RepData repData = (RepData) getItem(position);
                    viewHolder.textView.setText(repData.getName());

                    if (position == RightSideDrawerListActivity.rightSideDrawerSelectedIndex) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                } else {
                    viewHolder.textView.setText(getItem(position).toString());

                    if (position == RightSideDrawerListActivity.rightSideDrawerSelectedIndex) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
