package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrderItem;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 3/22/18.
 */

public class ContactOrderItemsListAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;
    private List<ContactOrderItem> orderItemsList;

    public ContactOrderItemsListAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#205889"; // R.color.activity_list_value_color
        this.orderItemsList = new ArrayList<>();
    }

    public void setOrderItemsList(List<ContactOrderItem> itemsList) {
        this.orderItemsList = itemsList;
    }

    static class ContactOrderItemsListViewHolder {
        ImageView order_type_icon;
        MMTextView order_item_product, order_item_description, order_item_billing_contact, order_item_date, order_item_pro_charge, order_item_barter, order_item_gross, order_item_net;
    }

    @Override
    public int getCount() {
        return orderItemsList.size();
    }

    @Override
    public ContactOrderItem getItem(int position) {
        return orderItemsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            ContactOrderItemsListViewHolder contactOrderItemsListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contact_order_items_list_row_item, null);

                contactOrderItemsListViewHolder = new ContactOrderItemsListViewHolder();
                contactOrderItemsListViewHolder.order_type_icon = (ImageView) convertView.findViewById(R.id.order_type_icon);
                contactOrderItemsListViewHolder.order_item_product = (MMTextView) convertView.findViewById(R.id.order_item_product);
                contactOrderItemsListViewHolder.order_item_description = (MMTextView) convertView.findViewById(R.id.order_item_description);
                contactOrderItemsListViewHolder.order_item_billing_contact = (MMTextView) convertView.findViewById(R.id.order_item_billing_contact);
                contactOrderItemsListViewHolder.order_item_date = (MMTextView) convertView.findViewById(R.id.order_item_date);
                contactOrderItemsListViewHolder.order_item_pro_charge = (MMTextView) convertView.findViewById(R.id.order_item_pro_charge);
                contactOrderItemsListViewHolder.order_item_barter = (MMTextView) convertView.findViewById(R.id.order_item_barter);
                contactOrderItemsListViewHolder.order_item_gross = (MMTextView) convertView.findViewById(R.id.order_item_gross);
                contactOrderItemsListViewHolder.order_item_net = (MMTextView) convertView.findViewById(R.id.order_item_net);

                convertView.setTag(contactOrderItemsListViewHolder);
            } else {
                contactOrderItemsListViewHolder = (ContactOrderItemsListViewHolder) convertView.getTag();
            }

            final ContactOrderItem orderItem = getItem(position);

            if (orderItem.getSubProductTypeID().equals(Constants.ORDER_TYPE_PRINT))
                contactOrderItemsListViewHolder.order_type_icon.setImageResource(R.drawable.order_print);
            else
                contactOrderItemsListViewHolder.order_type_icon.setImageResource(R.drawable.order_digital);

            contactOrderItemsListViewHolder.order_item_product.setText(utility.getFormattedKeyAndColouredNormalText("Product", orderItem.getProduct(), valueColorCode));
            contactOrderItemsListViewHolder.order_item_description.setText(utility.getFormattedKeyAndColouredNormalText("Description", orderItem.getDescription(), valueColorCode));
            contactOrderItemsListViewHolder.order_item_billing_contact.setText(utility.getFormattedKeyAndColouredNormalText("Billing Contact", orderItem.getBillingContact(), valueColorCode));
            contactOrderItemsListViewHolder.order_item_date.setText(utility.getFormattedKeyAndColouredNormalText("Date", orderItem.getStartDate(), valueColorCode));
            contactOrderItemsListViewHolder.order_item_pro_charge.setText(utility.getFormattedKeyAndColouredNormalText("Prod. Charge", String.format("$%,.2f", orderItem.getProductionCharges()), valueColorCode));
            contactOrderItemsListViewHolder.order_item_barter.setText(utility.getFormattedKeyAndColouredNormalText("Barter", String.format("$%,.2f", orderItem.getBarter()), valueColorCode));
            contactOrderItemsListViewHolder.order_item_gross.setText(utility.getFormattedKeyAndColouredNormalText("Gross", String.format("$%,.2f", orderItem.getGross()), valueColorCode));
            contactOrderItemsListViewHolder.order_item_net.setText(utility.getFormattedKeyAndColouredNormalText("Net", String.format("$%,.2f", orderItem.getNet()), valueColorCode));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
