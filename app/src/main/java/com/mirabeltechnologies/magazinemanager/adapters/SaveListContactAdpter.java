package com.mirabeltechnologies.magazinemanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.LocateContactOnMapActivity;
import com.mirabeltechnologies.magazinemanager.activities.SaveListActivity;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;

import java.util.ArrayList;
import java.util.List;

public class SaveListContactAdpter extends BaseAdapter {

    private Context context;
    private GlobalContent globalContent;
    private LayoutInflater layoutInflater;
    private Activity activity;
    public List<NearbyContacts> arrayList= new ArrayList<>();

    public SaveListContactAdpter(Context context, SaveListActivity testInNearContact) {
        this.context = context;
        this.globalContent = GlobalContent.getInstance();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity=testInNearContact;
    }

    @Override
    public int getCount() {
        if (globalContent.getNearestContactList() != null && globalContent.getNearestContactList().size() > 0)
            return globalContent.getNearestContactList().size();
        else
            return 0;
    }

    @Override
    public NearbyContacts getItem(int position) {
        return globalContent.getNearestContactList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class SaveViewHolder {
        
        LinearLayout contact_item;
        TextView tv_employee_name;
        TextView tv_address;
        TextView tv_company_name;
        ImageView img_location;
        CheckBox checkbox;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            SaveListContactAdpter.SaveViewHolder holder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.save_list_item, null);

                holder = new SaveListContactAdpter.SaveViewHolder();

                holder. contact_item = convertView.findViewById(R.id.contact_item);
                holder.tv_employee_name = convertView.findViewById(R.id.tv_employee_name);
                holder.tv_address = convertView.findViewById(R.id.tv_address);
                holder.tv_company_name = convertView.findViewById(R.id.tv_company_name);
                holder.img_location=convertView.findViewById(R.id.img_location);
                holder.checkbox=convertView.findViewById(R.id.checkbox);
                convertView.setTag(holder);

            } else {

                holder = (SaveListContactAdpter.SaveViewHolder) convertView.getTag();
            }
            final NearbyContacts nearbyContacts = getItem(position);

            holder.tv_company_name.setText(nearbyContacts.getCustomer());
            holder.tv_employee_name.setText(nearbyContacts.getName());
            holder.checkbox.setChecked(nearbyContacts.isCheck());
            String add1=nearbyContacts.getAddress1() != null ?nearbyContacts.getAddress1() : "";
            String add2=nearbyContacts.getAddress2() != null ?nearbyContacts.getAddress2() : "";
            String city=nearbyContacts.getCity() != null ?nearbyContacts.getCity() : "";
            String county=nearbyContacts.getCounty() != null ?nearbyContacts.getCounty() : "";
            String state=nearbyContacts.getStateRegion() != null ?nearbyContacts.getStateRegion() : "";
            String country=nearbyContacts.getCountry() != null ?nearbyContacts.getCountry() : "";
            String zip=nearbyContacts.getZipPostalCode() != null ?nearbyContacts.getZipPostalCode() : "";

            String address=add1+","+add2+","+city+","+county+","+state+","+country+","+zip;

           // String address=nearbyContacts.getAddress1().concat(" ,"+nearbyContacts.getAddress2()).concat(",").concat(nearbyContacts.getCity().concat(",").concat(nearbyContacts.getCounty().concat(",").concat(nearbyContacts.getCountry().concat(",")).concat(nearbyContacts.getZipPostalCode())));
            address= address.replaceAll(",,,"," ");
            address= address.replaceAll(",,",",");
            address= address.replaceFirst(",", "");

            holder.tv_address.setText(address);

            // holder.tv_address.setText(nearbyContacts.getAddress1().concat(nearbyContacts.getAddress2()).concat(nearbyContacts.getCity().concat(",").concat(nearbyContacts.getCountry().concat(",").concat(nearbyContacts.getZipPostalCode()))));
            holder.img_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String add1=nearbyContacts.getAddress1() != null ?nearbyContacts.getAddress1() : "";
                    String add2=nearbyContacts.getAddress2() != null ?nearbyContacts.getAddress2() : "";
                    String city=nearbyContacts.getCity() != null ?nearbyContacts.getCity() : "";
                    String county=nearbyContacts.getCounty() != null ?nearbyContacts.getCounty() : "";
                    String state=nearbyContacts.getStateRegion() != null ?nearbyContacts.getStateRegion() : "";
                    String country=nearbyContacts.getCountry() != null ?nearbyContacts.getCountry() : "";
                    String zip=nearbyContacts.getZipPostalCode() != null ?nearbyContacts.getZipPostalCode() : "";

                    String address=add1+","+add2+","+city+","+county+","+state+","+country+","+zip;

            if(address!=null && !address.isEmpty() && !address.equals(",,,,,,")){
                Intent intent = new Intent(context, LocateContactOnMapActivity.class);
                intent.putExtra("address", address);
                intent.putExtra("name", nearbyContacts.getName());
                intent.putExtra("company",nearbyContacts.getCustomer());
                intent.putExtra("custId",nearbyContacts.getGsCustomersID());
                intent.putExtra("repId",nearbyContacts.getRepId());
                intent.putExtra("Type", "PlotOne");
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
/*
                if(globalContent.getNearestContactList().get(position).getCountry().equals(DashboardActivity.country)
                        || globalContent.getNearestContactList().get(position).getStateRegion().equals(DashboardActivity.state)
                        ||globalContent.getNearestContactList().get(position).getCity().equals(DashboardActivity.city)){

                    Intent intent = new Intent(context, LocateContactOnMapActivity.class);
                    intent.putExtra("address", address);
                    intent.putExtra("name", nearbyContacts.getName());
                    intent.putExtra("company",nearbyContacts.getCustomer());
                    intent.putExtra("custId",nearbyContacts.getGsCustomersID());
                    intent.putExtra("repId",nearbyContacts.getRepId());
                    intent.putExtra("Type", "PlotOne");
                    context.startActivity(intent);
                    activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
                else {
                    Toast.makeText(activity, "selected Contact is Out Of Range", Toast.LENGTH_SHORT).show();
                }*/

            }
            else {

                String title = "Address Contacts";
                String message = "No valid Address Found ";
                // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
                MMAlertDialog.showAlertDialog(activity, title, message);
            }
                }
            });

            SaveListContactAdpter.SaveViewHolder finalHolder = holder;
            holder.checkbox.setOnClickListener(v -> {
                if(finalHolder.checkbox.isChecked()){
                    String add11 =nearbyContacts.getAddress1() != null ?nearbyContacts.getAddress1() : "";
                    String add21 =nearbyContacts.getAddress2() != null ?nearbyContacts.getAddress2() : "";
                    String city1 =nearbyContacts.getCity() != null ?nearbyContacts.getCity() : "";
                    String county1 =nearbyContacts.getCounty() != null ?nearbyContacts.getCounty() : "";
                    String state1 =nearbyContacts.getStateRegion() != null ?nearbyContacts.getStateRegion() : "";
                    String country1 =nearbyContacts.getCountry() != null ?nearbyContacts.getCountry() : "";
                    String zip1 =nearbyContacts.getZipPostalCode() != null ?nearbyContacts.getZipPostalCode() : "";

                    String address1 = add11 +","+ add21 +","+ city1 +","+ county1 +","+ state1 +","+ country1 +","+ zip1;
                    Log.e("veera", address1);

                    if(address1.equals(",,,,,,")) {
                        String title = "Address Contacts";
                        String message = "No valid Address Found ";
                        // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
                        MMAlertDialog.showAlertDialog(activity, title, message);
                        finalHolder.checkbox.setChecked(false);
                    }
                    else if(DashboardActivity.lastLocation==null || DashboardActivity.lastLocation.equals("")){

                        globalContent.getNearestContactList().get(position).setCheck(true);
                        arrayList.add(getItem(position));
                        Toast.makeText(activity, "Current Location details are not fetched .", Toast.LENGTH_SHORT).show();
                      //  MMAlertDialog.showAlertDialog(activity, activity.getResources().getString(R.string.location_alert),"Current Location details are not fetched .");
                       /* finalHolder.checkbox.setChecked(false);
                         MMAlertDialog.showAlertDialog(activity, activity.getResources().getString(R.string.location_alert),"Sorry..! Something went wrong, Please try again after some time.");*/
                    }
                    else if(country1.toLowerCase().equals(DashboardActivity.country.toLowerCase())
                            || country1.toLowerCase().equals(DashboardActivity.shortcode.toLowerCase())
                            || country1.replaceAll("\\s", "").toLowerCase().equals(DashboardActivity.full_shortcode.toLowerCase())
                            || state1.toLowerCase().equals(DashboardActivity.state.toLowerCase())
                            ||city1.toLowerCase().equals(DashboardActivity.city.toLowerCase())
                            ||zip1.equals(DashboardActivity.zipcode)){
                        globalContent.getNearestContactList().get(position).setCheck(true);
                        arrayList.add(getItem(position));
                    }
                    else {
                        finalHolder.checkbox.setChecked(false);
                        String title = "Address Contacts";
                        String message = "Address looks out of range to draw direction . please select map viewer icon see the location in map ";
                        // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
                        MMAlertDialog.showAlertDialog(activity, title, message);
                    }
                }
                else if(!finalHolder.checkbox.isChecked()) {
                    for( int i=0;i<arrayList.size();i++){

                        if(arrayList.get(i).getGsCustomersID().equals(getItem(position).getGsCustomersID())){
                            globalContent.getNearestContactList().get(position).setCheck(false);
                            arrayList.remove(getItem(position));
                        }
                    }
                }

                Log.e("ram",""+getItem(position).toString());
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}

