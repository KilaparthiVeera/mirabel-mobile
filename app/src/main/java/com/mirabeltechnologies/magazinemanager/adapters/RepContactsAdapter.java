package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;

/**
 * Created by venkat on 10/18/17.
 */

public class RepContactsAdapter extends ContactsListViewBaseAdapter {
    private PhoneCallListener listener;

    public RepContactsAdapter(Context context, PhoneCallListener phoneCallListener) {
        super(context);

        this.listener = phoneCallListener;
    }

    @Override
    public int getCount() {
        if (globalContent.getRepContactsList() != null && globalContent.getRepContactsList().size() > 0)
            return globalContent.getRepContactsList().size();
        else
            return 0;
    }

    @Override
    public Contact getItem(int position) {
        return globalContent.getRepContactsList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactsListViewHolder repContactsListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contacts_list_row_item, null);

                repContactsListViewHolder = new ContactsListViewHolder();
                repContactsListViewHolder.contact_type = (ImageView) convertView.findViewById(R.id.contact_type);
                repContactsListViewHolder.company_name = (MMTextView) convertView.findViewById(R.id.company_name);
                repContactsListViewHolder.employee_name = (MMTextView) convertView.findViewById(R.id.employee_name);
                repContactsListViewHolder.phone_icon = (ImageView) convertView.findViewById(R.id.phone_icon);

                convertView.setTag(repContactsListViewHolder);
            } else {
                repContactsListViewHolder = (ContactsListViewHolder) convertView.getTag();
            }

            final Contact contact = getItem(position);

            repContactsListViewHolder.contact_type.setImageResource(getResourceIdForContactTitle(contact.getTitle()));

            if (contact.getCompanyName().isEmpty()) {
                repContactsListViewHolder.company_name.setText(R.string.company_name);
                repContactsListViewHolder.company_name.setTextColor(company_name_empty_title_color);
            } else {
                repContactsListViewHolder.company_name.setText(contact.getCompanyName());
                repContactsListViewHolder.company_name.setTextColor(company_name_title_color);
            }

            if (contact.getName().isEmpty()) {
                repContactsListViewHolder.employee_name.setText(R.string.name);
                repContactsListViewHolder.employee_name.setTextColor(company_name_empty_title_color);
            } else {
                repContactsListViewHolder.employee_name.setText(contact.getName());
                repContactsListViewHolder.employee_name.setTextColor(employee_name_title_color);
            }

            if (contact.getPhone().length() > 0) {
                repContactsListViewHolder.phone_icon.setVisibility(View.VISIBLE);
                repContactsListViewHolder.phone_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.takeConfirmationToMakePhoneCall(contact.getCustomerId(), contact.getPhone(), contact.getPhoneExtn() == null ? "" : contact.getPhoneExtn());
                    }
                });
            } else {
                repContactsListViewHolder.phone_icon.setVisibility(View.GONE);
                repContactsListViewHolder.phone_icon.setOnClickListener(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
