package com.mirabeltechnologies.magazinemanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.LocateContactOnMapActivity;
import com.mirabeltechnologies.magazinemanager.activities.NearbyContactsActivity;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;

import java.util.ArrayList;
import java.util.List;

public class NearContactAdpter extends BaseAdapter {

    private Context context;
    private GlobalContent globalContent;
    private LayoutInflater layoutInflater;
    Activity activity;
    public List<NearbyContacts> arrayList= new ArrayList<>();

    public NearContactAdpter(Context context, NearbyContactsActivity testInNearContact) {
        this.context = context;
        this.globalContent = GlobalContent.getInstance();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.activity=testInNearContact;
    }

    @Override
    public int getCount() {
        if (globalContent.getNearestContactList() != null && globalContent.getNearestContactList().size() > 0)
            return globalContent.getNearestContactList().size();
        else
            return 0;
    }

    @Override
    public NearbyContacts getItem(int position) {
        return globalContent.getNearestContactList().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    static class NearViewHolder {
        LinearLayout contact_item;
        TextView tv_employee_name;
        TextView tv_address;
        TextView tv_company_name;
        ImageView img_location;
        CheckBox checkbox;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            NearViewHolder holder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.nearby_contact_list_item, null);

                holder = new NearContactAdpter.NearViewHolder();

                holder. contact_item = convertView.findViewById(R.id.contact_item);
                holder.tv_employee_name = convertView.findViewById(R.id.tv_employee_name);
                holder.tv_address = convertView.findViewById(R.id.tv_address);
                holder.tv_company_name = convertView.findViewById(R.id.tv_company_name);
                holder.img_location=convertView.findViewById(R.id.img_location);
                holder.checkbox=convertView.findViewById(R.id.checkbox);
                convertView.setTag(holder);

            } else {

                holder = (NearContactAdpter.NearViewHolder) convertView.getTag();
            }
             final NearbyContacts nearbyContacts = getItem(position);

                holder.tv_company_name.setText(nearbyContacts.getCustomer());
            holder.tv_employee_name.setText(nearbyContacts.getName());
            holder.checkbox.setChecked(nearbyContacts.isCheck());

            String add1=nearbyContacts.getAddress1() != null ?nearbyContacts.getAddress1() : "";
            String add2=nearbyContacts.getAddress2() != null ?nearbyContacts.getAddress2() : "";
            String city=nearbyContacts.getCity() != null ?nearbyContacts.getCity() : "";
            String county=nearbyContacts.getCounty() != null ?nearbyContacts.getCounty() : "";
            String state=nearbyContacts.getStateRegion() != null ?nearbyContacts.getStateRegion() : "";
            String country=nearbyContacts.getCountry() != null ?nearbyContacts.getCountry() : "";
            String zip=nearbyContacts.getZipPostalCode() != null ?nearbyContacts.getZipPostalCode() : "";

            String address=add1+","+add2+","+city+","+county+","+state+","+country+","+zip;
            // String address=nearbyContacts.getAddress1().concat(" ,"+nearbyContacts.getAddress2()).concat(",").concat(nearbyContacts.getCity().concat(",").concat(nearbyContacts.getCounty().concat(",").concat(nearbyContacts.getCountry().concat(",")).concat(nearbyContacts.getZipPostalCode())));
            address= address.replaceAll(",,,"," ");
            address= address.replaceAll(",,",",");
            address= address.replaceFirst(",", "");

            holder.tv_address.setText(address);

           // holder.tv_address.setText(nearbyContacts.getAddress1().concat(nearbyContacts.getAddress2()).concat(nearbyContacts.getCity().concat(",").concat(nearbyContacts.getCountry().concat(",").concat(nearbyContacts.getZipPostalCode()))));
            holder.img_location.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String add1=nearbyContacts.getAddress1() != null ?nearbyContacts.getAddress1() : "";
                    String add2=nearbyContacts.getAddress2() != null ?nearbyContacts.getAddress2() : "";
                    String city=nearbyContacts.getCity() != null ?nearbyContacts.getCity() : "";
                    String county=nearbyContacts.getCounty() != null ?nearbyContacts.getCounty() : "";
                    String state=nearbyContacts.getStateRegion() != null ?nearbyContacts.getStateRegion() : "";
                    String country=nearbyContacts.getCountry() != null ?nearbyContacts.getCountry() : "";
                    String zip=nearbyContacts.getZipPostalCode() != null ?nearbyContacts.getZipPostalCode() : "";

                    String address=add1+","+add2+","+city+","+county+","+state+","+country+","+zip;

                    if(address!=null && !address.isEmpty() && !address.equals(",,,,,,")){
                        Intent intent = new Intent(context, LocateContactOnMapActivity.class);
                        intent.putExtra("address", address);
                        intent.putExtra("name", nearbyContacts.getName());
                        intent.putExtra("company",nearbyContacts.getCustomer());
                        intent.putExtra("custId",nearbyContacts.getGsCustomersID());
                        intent.putExtra("repId",nearbyContacts.getRepId());
                        intent.putExtra("Type", "PlotOne");
                        context.startActivity(intent);
                        activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    }else {
                        Toast.makeText(activity, "No Address Found", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            NearViewHolder finalHolder = holder;
            holder.checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(finalHolder.checkbox.isChecked()){

                      globalContent.getNearestContactList().get(position).setCheck(true);

                            arrayList.add(getItem(position));
                    }
                    else if(!finalHolder.checkbox.isChecked()) {

                        for( int i=0;i<arrayList.size();i++){

                            if(arrayList.get(i).getGsCustomersID().equals(getItem(position).getGsCustomersID())){
                                globalContent.getNearestContactList().get(position).setCheck(false);
                                arrayList.remove(getItem(position));
                            }
                        }
                    }

                    Log.e("ram",""+getItem(position).toString());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
