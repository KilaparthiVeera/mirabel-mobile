package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.asynctasks.GetQueryAsyncTask;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.QueryObject;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactsAdvSearchListener;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by venkat on 11/03/17.
 */

public class AdvSearchSuggestionsAdapter extends BaseAdapter implements Filterable {
    private Context context;
    private ContactsAdvSearchListener listener;
    private String encryptedClientKey;
    private LayoutInflater layoutInflater;
    private ArrayList advSearchFilteredSuggestions;
    private int typeOfFilter = 0; // 0 - Company Name, 1 - Name & 2 - Email
    private float dropdown_item_font_size = 13.0f;

    public AdvSearchSuggestionsAdapter(Context context, ContactsAdvSearchListener advSearchListener, String clientKey) {
        this.context = context;
        this.listener = advSearchListener;
        this.encryptedClientKey = clientKey;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.advSearchFilteredSuggestions = new ArrayList<>();
        this.dropdown_item_font_size = context.getResources().getDimension(R.dimen.simple_spinner_dropdown_item_text_font_size);
    }

    public void setTypeOfFilter(int typeOfFilter) {
        this.typeOfFilter = typeOfFilter;
    }

    static class AdvSearchPredictionsViewHolder {
        TextView textView;
    }

    @Override
    public int getCount() {
        return advSearchFilteredSuggestions.size();
    }

    @Override
    public Object getItem(int position) {
        return advSearchFilteredSuggestions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        try {
            AdvSearchPredictionsViewHolder advSearchPredictionsViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(android.R.layout.simple_spinner_dropdown_item, viewGroup, false);

                advSearchPredictionsViewHolder = new AdvSearchPredictionsViewHolder();
                advSearchPredictionsViewHolder.textView = (TextView) convertView.findViewById(android.R.id.text1);
                advSearchPredictionsViewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.black));
                advSearchPredictionsViewHolder.textView.setTextSize(dropdown_item_font_size);

                convertView.setTag(advSearchPredictionsViewHolder);

            } else {
                advSearchPredictionsViewHolder = (AdvSearchPredictionsViewHolder) convertView.getTag();
            }

            if (typeOfFilter == 0) {
                CompanyData companyData = (CompanyData) getItem(position);
                advSearchPredictionsViewHolder.textView.setText(companyData.getName());
            } else {
                String suggestion = getItem(position).toString();
                advSearchPredictionsViewHolder.textView.setText(suggestion);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults filterResults = new FilterResults();

                if (charSequence != null) {
                    try {
                        if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                            QueryObject queryObject = new QueryObject();
                            queryObject.setKeyword(charSequence.toString());

                            if (typeOfFilter == 0)
                                queryObject.setTypeOfFilter("company");
                            else if (typeOfFilter == 1)
                                queryObject.setTypeOfFilter("name");
                            else if (typeOfFilter == 2)
                                queryObject.setTypeOfFilter("email");

                            if (listener != null)
                                listener.updateSearchKeyword(queryObject.getKeyword(), typeOfFilter);

                            String responseStr = new GetQueryAsyncTask(encryptedClientKey).execute(queryObject).get();

                            if (responseStr != null) {
                                try {
                                    JSONObject response = new JSONObject(responseStr);

                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (typeOfFilter == 0) {
                                            Type companyNameSuggestionsListType = new TypeToken<ArrayList<CompanyData>>() {
                                            }.getType();

                                            ArrayList<CompanyData> companyNameSuggestionsList = new Gson().fromJson(response.getString("CustomerQuery"), companyNameSuggestionsListType);

                                            // Assign the data to the FilterResults
                                            filterResults.values = companyNameSuggestionsList;
                                            filterResults.count = companyNameSuggestionsList.size();
                                        } else {
                                            Type suggestionsListType = new TypeToken<ArrayList<String>>() {
                                            }.getType();

                                            ArrayList<String> suggestionsList = new Gson().fromJson(response.getString("QueryResult"), suggestionsListType);

                                            // Assign the data to the FilterResults
                                            filterResults.values = suggestionsList;
                                            filterResults.count = suggestionsList.size();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if (results != null && results.count > 0) {
                    advSearchFilteredSuggestions = (ArrayList) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }
        };

        return filter;
    }
}
