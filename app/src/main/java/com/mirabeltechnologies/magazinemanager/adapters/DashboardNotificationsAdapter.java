package com.mirabeltechnologies.magazinemanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.EventDetailsActivity;
import com.mirabeltechnologies.magazinemanager.beans.Notification;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;
import java.util.List;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;

/**
 * Created by venkat on 10/23/17.
 */

public class DashboardNotificationsAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<Notification> notifications;
    Activity activity;

    public DashboardNotificationsAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.notifications = new ArrayList<>();
        activity = (Activity) context;
    }

    public void setNotifications(List<Notification> notificationsList) {
        this.notifications = notificationsList;
    }

    static class DashboardNotificationsViewHolder {
        MMTextView subject, date_time;
    }

    @Override
    public int getCount() {
        if (notifications == null)
            return 0;
        else
            return notifications.size();
    }

    @Override
    public Notification getItem(int position) {
        return notifications.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            DashboardNotificationsViewHolder notificationsViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.dashboard_notifications_row_item, null);
                notificationsViewHolder = new DashboardNotificationsViewHolder();
                notificationsViewHolder.subject = (MMTextView) convertView.findViewById(R.id.dn_subject);
                notificationsViewHolder.date_time = (MMTextView) convertView.findViewById(R.id.dn_date_time);
                convertView.setTag(notificationsViewHolder);
            } else {
                notificationsViewHolder = (DashboardNotificationsViewHolder) convertView.getTag();
            }

            final Notification notification = getItem(position);

            notificationsViewHolder.subject.setText(getFormattedSubject(notification));
            notificationsViewHolder.date_time.setText(notification.getDateTime());

            if (Boolean.parseBoolean(notification.getViewed())) {
                notificationsViewHolder.subject.setTypeface(Typeface.DEFAULT);
            } else {
                notificationsViewHolder.subject.setTypeface(Typeface.DEFAULT_BOLD);
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent openDetailsIntent = new Intent(context, EventDetailsActivity.class);
                    openDetailsIntent.putExtra(EVENT_DATA, notification);
                    openDetailsIntent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.RequestFrom.DASHBOARD_NOTIFICATIONS);
                    context.startActivity(openDetailsIntent);
                    activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }


    public String getFormattedSubject(Notification notification) {
        StringBuffer subject = new StringBuffer(notification.getSubject());
        StringBuffer formattedSubject = new StringBuffer("");

        if (notification.getType().equals("Callback")) {
            if (notification.getTime().contains("12:00AM") || notification.getTime().contains("12:00 AM"))
                formattedSubject.append("Call");
            else
                formattedSubject.append("Call " + notification.getTime());

            formattedSubject.append(" - " + notification.getFromName());
        } else if (notification.getType().equals("Meeting")) {
            if (notification.getTime().contains("12:00AM") || notification.getTime().contains("12:00 AM"))
                formattedSubject.append("Meeting");
            else
                formattedSubject.append("Meeting " + notification.getTime());

            if (notification.getFromName().length() == 0)
                formattedSubject.append(" - " + subject);
            else
                formattedSubject.append(" - " + notification.getFromName());
        } else if (notification.getType().equals("Task")) {
            formattedSubject.append("Task - " + subject);
        } else {
            formattedSubject = subject;
        }

        return formattedSubject.toString();
    }
}
