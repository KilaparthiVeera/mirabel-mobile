package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;


/**
 * Created by venkat on 10/10/17.
 */

public class RecentContactsAdapter extends ContactsListViewBaseAdapter {
    private PhoneCallListener listener;

    public RecentContactsAdapter(Context context, PhoneCallListener phoneCallListener) {
        super(context);

        this.listener = phoneCallListener;

        this.company_name_title_color = ContextCompat.getColor(context, R.color.recent_contacts_company_name_title_color);
        this.company_name_empty_title_color = ContextCompat.getColor(context, R.color.recent_contacts_company_name_empty_title_color);
        this.employee_name_title_color = ContextCompat.getColor(context, R.color.recent_contacts_employee_name_title_color);
    }

    @Override
    public int getCount() {
        return contactsList.size();
    }

    @Override
    public Contact getItem(int position) {
        return contactsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactsListViewHolder recentContactsViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.recent_contacts_row_item, null);

                recentContactsViewHolder = new ContactsListViewHolder();
                recentContactsViewHolder.contact_type = (ImageView) convertView.findViewById(R.id.contact_type);
                recentContactsViewHolder.company_name = (MMTextView) convertView.findViewById(R.id.company_name);
                recentContactsViewHolder.employee_name = (MMTextView) convertView.findViewById(R.id.employee_name);
                recentContactsViewHolder.phone_icon = (ImageView) convertView.findViewById(R.id.phone_icon);

                convertView.setTag(recentContactsViewHolder);
            } else {
                recentContactsViewHolder = (ContactsListViewHolder) convertView.getTag();
            }

            final Contact contact = getItem(position);

            recentContactsViewHolder.contact_type.setImageResource(getResourceIdForContactTitle(contact.getTitle()));

            if (contact.getCompanyName().isEmpty()) {
                recentContactsViewHolder.company_name.setText(R.string.company_name);
                recentContactsViewHolder.company_name.setTextColor(company_name_empty_title_color);
            } else {
                recentContactsViewHolder.company_name.setText(contact.getCompanyName());
                recentContactsViewHolder.company_name.setTextColor(company_name_title_color);
            }

            if (contact.getName().isEmpty()) {
                recentContactsViewHolder.employee_name.setText(R.string.name);
                recentContactsViewHolder.employee_name.setTextColor(company_name_empty_title_color);
            } else {
                recentContactsViewHolder.employee_name.setText(contact.getName());
                recentContactsViewHolder.employee_name.setTextColor(employee_name_title_color);
            }

            if (contact.getPhone().length() > 0) {
                recentContactsViewHolder.phone_icon.setVisibility(View.VISIBLE);
                recentContactsViewHolder.phone_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener != null)
                            listener.takeConfirmationToMakePhoneCall(contact.getCustomerId(), contact.getPhone(), contact.getPhoneExtn() == null ? "" : contact.getPhoneExtn());
                    }
                });
            } else {
                recentContactsViewHolder.phone_icon.setVisibility(View.GONE);
                recentContactsViewHolder.phone_icon.setOnClickListener(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
