package com.mirabeltechnologies.magazinemanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CheckInList;

import org.jsoup.helper.StringUtil;

import java.util.List;

public class CheckInHistoryAdopter extends RecyclerView.Adapter<CheckInHistoryAdopter.CheckInHolder>{

    Activity activity;
    List<CheckInList> list;
    Context context;
    String loggedInUserName;

    public CheckInHistoryAdopter(Activity activity, List<CheckInList> list, Context context, String loggedInUserName) {
        this.activity = activity;
        this.list = list;
        this.context = context;
        this.loggedInUserName = loggedInUserName;
    }

    @NonNull
    @Override
    public CheckInHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new CheckInHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.checkin_list, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CheckInHolder holder, int position) {
        holder.txt_1.setText(list.get(position).getCustomer().getContactFullName());
        holder.txt_2.setText(list.get(position).getTransactionDate());
        holder.txt_3.setText(list.get(position).getNote());
        holder.txt_4.setText(list.get(position).getLocation());
        holder.txt_5.setText(!StringUtil.isBlank(list.get(position).getUser().getName()) ? list.get(position).getUser().getName() : loggedInUserName);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class CheckInHolder extends RecyclerView.ViewHolder {
        public CardView card_view;
        public TextView txt_1,txt_2,txt_3,txt_4,txt_5;
        public CheckInHolder(View itemView) {
            super(itemView);
            this.card_view = itemView.findViewById(R.id.card_view);
            txt_1 = itemView.findViewById(R.id.txt_1);
            txt_2 = itemView.findViewById(R.id.txt_2);
            txt_3 = itemView.findViewById(R.id.txt_3);
            txt_4 = itemView.findViewById(R.id.txt_4);
            txt_5 = itemView.findViewById(R.id.txt_5);

        }
    }

}
