package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CustomerAddress;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 10/20/17.
 */

public class ContactAddressesAdapter extends ContactsListViewBaseAdapter {

    protected List<CustomerAddress> addressesList;

    public ContactAddressesAdapter(Context context) {
        super(context);

        this.addressesList = new ArrayList<>();
    }

    public void setAddressesList(List<CustomerAddress> addressesList) {
        this.addressesList = addressesList;
    }

    static class ContactAddressesListViewHolder {
        MMTextView address_type, formatted_address;
    }

    @Override
    public int getCount() {
        return addressesList.size();
    }

    @Override
    public CustomerAddress getItem(int position) {
        return addressesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactAddressesListViewHolder addressesListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contact_addresses_row_item, null);

                addressesListViewHolder = new ContactAddressesListViewHolder();
                addressesListViewHolder.address_type = (MMTextView) convertView.findViewById(R.id.address_type);
                addressesListViewHolder.formatted_address = (MMTextView) convertView.findViewById(R.id.formatted_address);

                convertView.setTag(addressesListViewHolder);
            } else {
                addressesListViewHolder = (ContactAddressesListViewHolder) convertView.getTag();
            }

            final CustomerAddress address = getItem(position);

            addressesListViewHolder.address_type.setText(address.getAddressType().replace("Address", " Address"));
            addressesListViewHolder.formatted_address.setText(getFormattedAddress(address));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public String getFormattedAddress(CustomerAddress address) {
        String formatted_address = "";

        if (address.getAddress1().length() > 0)
            formatted_address += address.getAddress1() + "\n";

        if (address.getAddress2().length() > 0)
            formatted_address += address.getAddress2() + "\n";

        if (address.getCity().length() > 0)
            formatted_address += address.getCity();

        if (address.getState().length() > 0) {
            if (address.getCity().length() > 0)
                formatted_address += ", ";

            formatted_address += address.getState();
        }

        if (address.getZipcode().length() > 0) {
            if (address.getCity().length() > 0 || address.getState().length() > 0)
                formatted_address += " ";

            formatted_address += address.getZipcode() + "\n";
        } else
            formatted_address += "\n";

        int noOfCharsToTruncate = 1; // to remove last 1 character only i.e. \n

        if (address.getCounty().length() > 0) {
            formatted_address += address.getCounty() + ", ";
            noOfCharsToTruncate = 2; // to remove last 2 characters comma and space.
        }

        if (address.getInternational().length() > 0) {
            formatted_address += address.getInternational() + ", ";
            noOfCharsToTruncate = 2;
        }

        if (formatted_address.length() > 0) {
            formatted_address = formatted_address.substring(0, formatted_address.length() - noOfCharsToTruncate);
        }

        return formatted_address;
    }
}
