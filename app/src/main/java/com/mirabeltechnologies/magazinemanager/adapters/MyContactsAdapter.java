package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;

/**
 * Created by venkat on 10/18/17.
 */

public class MyContactsAdapter extends ContactsListViewBaseAdapter {
    private PhoneCallListener listener;

    public MyContactsAdapter(Context context, PhoneCallListener phoneCallListener) {
        super(context);

        this.listener = phoneCallListener;
    }

    @Override
    public int getCount() {
        if (globalContent.getMyContacts() != null && globalContent.getMyContacts().size() > 0)
            return globalContent.getMyContacts().size();
        else
            return 0;
    }

    @Override
    public Contact getItem(int position) {
        return globalContent.getMyContacts().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactsListViewHolder myContactsListViewHolder;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contacts_list_row_item, null);

                myContactsListViewHolder = new ContactsListViewHolder();
                myContactsListViewHolder.contact_type = convertView.findViewById(R.id.contact_type);
                myContactsListViewHolder.company_name = convertView.findViewById(R.id.company_name);
                myContactsListViewHolder.employee_name = convertView.findViewById(R.id.employee_name);
                myContactsListViewHolder.phone_icon = convertView.findViewById(R.id.phone_icon);

                convertView.setTag(myContactsListViewHolder);
            } else {
                myContactsListViewHolder = (ContactsListViewHolder) convertView.getTag();
            }

            final Contact contact = getItem(position);

            myContactsListViewHolder.contact_type.setImageResource(getResourceIdForContactTitle(contact.getTitle()));

            if (contact.getCompanyName().isEmpty()) {
                myContactsListViewHolder.company_name.setText(R.string.company_name);
                myContactsListViewHolder.company_name.setTextColor(company_name_empty_title_color);
            } else {
                myContactsListViewHolder.company_name.setText(contact.getCompanyName());
                myContactsListViewHolder.company_name.setTextColor(company_name_title_color);
            }

            if (contact.getName().isEmpty()) {
                myContactsListViewHolder.employee_name.setText(R.string.name);
                myContactsListViewHolder.employee_name.setTextColor(company_name_empty_title_color);
            } else {
                myContactsListViewHolder.employee_name.setText(contact.getName());
                myContactsListViewHolder.employee_name.setTextColor(employee_name_title_color);
            }

            if (contact.getPhone().length() > 0) {
                myContactsListViewHolder.phone_icon.setVisibility(View.VISIBLE);
                myContactsListViewHolder.phone_icon.setOnClickListener(v -> {
                    if (listener != null)
                        listener.takeConfirmationToMakePhoneCall(contact.getCustomerId(), contact.getPhone(), contact.getPhoneExtn() == null ? "" : contact.getPhoneExtn());
                });
            } else {
                myContactsListViewHolder.phone_icon.setVisibility(View.GONE);
                myContactsListViewHolder.phone_icon.setOnClickListener(null);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
