package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ActivityDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactActivitiesListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 10/19/17.
 */

public class ContactActivitiesAdapter extends ContactsListViewBaseAdapter {
    protected List<ActivityDetails> activitiesList;
    private String activityType;
    private ViewMoreClickListener viewMoreClickListener;
    private ContactActivitiesListener contactActivitiesListener;
    private int canViewEmployeeId = 0;

    public ContactActivitiesAdapter(Context context, String activityType, ViewMoreClickListener viewMoreClickListener, ContactActivitiesListener contactActivitiesListener, int canViewEmployeeId) {
        super(context);

        this.activitiesList = new ArrayList<>();
        this.activityType = activityType;
        this.viewMoreClickListener = viewMoreClickListener;
        this.contactActivitiesListener = contactActivitiesListener;
        this.canViewEmployeeId = canViewEmployeeId;
    }

    public void setActivitiesList(List<ActivityDetails> activitiesList) {
        this.activitiesList = activitiesList;
    }

    static class ContactActivitiesListViewHolder {
        ImageView activity_type_icon;
        MMTextView activity_notes, activity_date_time;
    }

    @Override
    public int getCount() {
        return activitiesList.size();
    }

    @Override
    public ActivityDetails getItem(int position) {
        return activitiesList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            ContactActivitiesListViewHolder activitiesListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contact_activities_row_item, null);

                activitiesListViewHolder = new ContactActivitiesListViewHolder();
                activitiesListViewHolder.activity_type_icon = (ImageView) convertView.findViewById(R.id.activity_type);
                activitiesListViewHolder.activity_notes = (MMTextView) convertView.findViewById(R.id.activity_notes);
                activitiesListViewHolder.activity_date_time = (MMTextView) convertView.findViewById(R.id.activity_date_time);

                convertView.setTag(activitiesListViewHolder);
            } else {
                activitiesListViewHolder = (ContactActivitiesListViewHolder) convertView.getTag();
            }

            final ActivityDetails activityDetails = getItem(position);

            //activitiesListViewHolder.activity_notes.setText(Jsoup.parse(activityDetails.getTaskNameOrNotes()).text());
            activitiesListViewHolder.activity_notes.setText(activityDetails.getModifiedNotes());
            activitiesListViewHolder.activity_date_time.setText(activityDetails.getDateTime());

            addViewMoreToTextView(position, activitiesListViewHolder.activity_notes, activityDetails.getModifiedNotes(), activityDetails.isHasHTMLTags(), Constants.VIEW_MORE_EXPAND_TEXT, 3, viewMoreClickListener);

            if (activityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_notes);
            } else if (activityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_call);
            } else if (activityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_meetings);
            } else if (activityType.equals(Constants.ACTIVITY_TYPE_EMAILS)) {
                if (activityDetails.getActivityType().equals("Email"))
                    activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_email);
                else
                    activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_mass_email);

                //activitiesListViewHolder.activity_type_icon.setVisibility(View.VISIBLE);
            } else if (activityType.equals(Constants.ACTIVITY_TYPE_TASKS)) {
                if (String.valueOf(canViewEmployeeId).equals(Constants.CONTACT_READ_ONLY_ACCESS)) {
                    activitiesListViewHolder.activity_type_icon.setVisibility(View.GONE);
                } else {
                    activitiesListViewHolder.activity_type_icon.setVisibility(View.VISIBLE);

                    activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.mark_completed);

                    activitiesListViewHolder.activity_type_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (contactActivitiesListener != null)
                                contactActivitiesListener.markTask(position, Long.parseLong(activityDetails.getActivityOrTaskId()), Constants.TASK_ACTION_MARK_COMPLETE);
                        }
                    });
                }
            } /*else {
                activitiesListViewHolder.activity_type_icon.setVisibility(View.GONE);
            }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
