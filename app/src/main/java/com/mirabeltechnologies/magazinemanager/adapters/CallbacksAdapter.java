package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CallbackData;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.CallbacksListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.util.Utility;

/**
 * Created by venkat on 05/28/18.
 */

public class CallbacksAdapter extends BaseAdapter {
    private Context context;
    private CallbacksListener listener;
    private ViewMoreClickListener viewMoreClickListener;
    private GlobalContent globalContent;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;

    public CallbacksAdapter(Context context, CallbacksListener callbacksListener, ViewMoreClickListener viewMoreClickListener) {
        this.context = context;
        this.listener = callbacksListener;
        this.viewMoreClickListener = viewMoreClickListener;
        this.globalContent = GlobalContent.getInstance();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#205889"; // R.color.activity_list_value_color
    }

    static class CallbacksListViewHolder {
        MMTextView company_name, notes, time, phone_number, city, rep_name, email, priority;
        ImageView mark_icon;
    }

    @Override
    public int getCount() {
        if (globalContent.getCallbacks() != null && globalContent.getCallbacks().size() > 0)
            return globalContent.getCallbacks().size();
        else
            return 0;
    }

    @Override
    public CallbackData getItem(int position) {
        return globalContent.getCallbacks().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            CallbacksListViewHolder callbacksListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.callbacks_list_row_item, null);

                callbacksListViewHolder = new CallbacksListViewHolder();
                callbacksListViewHolder.company_name = (MMTextView) convertView.findViewById(R.id.callback_company_name);
                callbacksListViewHolder.notes = (MMTextView) convertView.findViewById(R.id.callback_notes);
                callbacksListViewHolder.time = (MMTextView) convertView.findViewById(R.id.callback_time);
                callbacksListViewHolder.phone_number = (MMTextView) convertView.findViewById(R.id.callback_phone_number);
                callbacksListViewHolder.city = (MMTextView) convertView.findViewById(R.id.callback_city);
                callbacksListViewHolder.rep_name = (MMTextView) convertView.findViewById(R.id.callback_rep_name);
                callbacksListViewHolder.email = (MMTextView) convertView.findViewById(R.id.callback_email);
                callbacksListViewHolder.priority = (MMTextView) convertView.findViewById(R.id.callback_priority);
                callbacksListViewHolder.mark_icon = (ImageView) convertView.findViewById(R.id.callback_mark_icon);

                convertView.setTag(callbacksListViewHolder);

            } else {
                callbacksListViewHolder = (CallbacksListViewHolder) convertView.getTag();
            }

            final CallbackData callbackData = getItem(position);

            String companyName = callbackData.getCompanyName();
            if (callbackData.getName().length() > 0)
                companyName = String.format("%s (%s)", companyName, callbackData.getName());

            Drawable contactTypeDrawable = context.getResources().getDrawable(getResourceIdForContactTitle(callbackData.getTitle()));
            contactTypeDrawable.setBounds(0, 0, callbacksListViewHolder.company_name.getLineHeight(), callbacksListViewHolder.company_name.getLineHeight());
            ImageSpan contactTypeImageSpan = new ImageSpan(contactTypeDrawable);
            SpannableStringBuilder companyNameBuilder = new SpannableStringBuilder();
            companyNameBuilder.append("Company Name :  ");
            companyNameBuilder.setSpan(contactTypeImageSpan, companyNameBuilder.length() - 1, companyNameBuilder.length(), 0);
            companyNameBuilder.append(" ");
            companyNameBuilder.append(utility.getFormattedColouredTextWithUnderscore(companyName, valueColorCode));
            callbacksListViewHolder.company_name.setText(companyNameBuilder);

            callbacksListViewHolder.notes.setText(utility.getFormattedKeyAndColouredNormalText("Notes", callbackData.getModifiedNotes(), valueColorCode));
            callbacksListViewHolder.time.setText(utility.getFormattedKeyAndColouredNormalText("Call Back", callbackData.getDate(), valueColorCode));
            callbacksListViewHolder.city.setText(utility.getFormattedKeyAndColouredNormalText("City", callbackData.getCity(), valueColorCode));
            callbacksListViewHolder.rep_name.setText(utility.getFormattedKeyAndColouredNormalText("Rep Name", callbackData.getRepName(), valueColorCode));

            if (callbackData.getCanViewEmployeeID().equals(Constants.CONTACT_NO_ACCESS)) {
                callbacksListViewHolder.phone_number.setText(utility.getFormattedKeyAndColouredNormalText("Phone", "---", valueColorCode));
                callbacksListViewHolder.email.setText(utility.getFormattedKeyAndColouredNormalText("Email", "---", valueColorCode));
            } else {
                String phoneNumber = callbackData.getPhone();
                if (callbackData.getPhoneExtn() != null)
                    phoneNumber = phoneNumber + " ext " + callbackData.getPhoneExtn();

                callbacksListViewHolder.phone_number.setText(utility.getFormattedKeyAndColouredNormalText("Phone", phoneNumber, valueColorCode));
                callbacksListViewHolder.email.setText(utility.getFormattedKeyAndColouredTextWithUnderscore("Email", callbackData.getEmail(), valueColorCode));

                if (callbackData.getEmail().length() > 0) {
                    callbacksListViewHolder.email.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (listener != null)
                                listener.chooseEmailClient(String.valueOf(callbackData.getCustomerId()), callbackData.getEmail());
                        }
                    });
                }
            }

            callbacksListViewHolder.priority.setText(utility.getFormattedKeyAndColouredNormalText("Priority", callbackData.getPriority(), valueColorCode));

            // adding more to notes text view at end of 3rd line to show complete notes in pop up when we click on it.
            addViewMoreToTextView(position, callbacksListViewHolder.notes, "Notes : ", callbackData.getModifiedNotes(), callbackData.isHasHTMLTags(), Constants.VIEW_MORE_EXPAND_TEXT, 3, viewMoreClickListener);

            callbacksListViewHolder.company_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.openContactDetailPage(callbackData.getCustomerId(), Long.parseLong(callbackData.getParentId()), callbackData.getCanViewEmployeeID());
                }
            });

            callbacksListViewHolder.mark_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.markActivity(position, callbackData.getActivityId(), callbackData.getCanViewEmployeeID());
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public int getResourceIdForContactTitle(String title) {
        int resourceId;

        switch (title) {
            case "Ad Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Agency Contact":
                resourceId = R.drawable.icon_agency_contact;
                break;
            case "Current-Future Contracts":
                resourceId = R.drawable.icon_company_future_orders;
                break;
            case "Past-Expired Contracts":
                resourceId = R.drawable.icon_company_expired_orders;
                break;
            case "Company":
                resourceId = R.drawable.icon_company_no_orders;
                break;
            case "Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            case "Child Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            default:
                resourceId = R.drawable.icon_sub_contact;
                break;
        }

        return resourceId;
    }

    public void addViewMoreToTextView(final int position, final TextView textView, final String titleText, String notes, final boolean showViewMore, final String expandText, final int maxLine, final ViewMoreClickListener listener) {
        try {
            final String text = titleText + notes;

            textView.post(new Runnable() {
                @Override
                public void run() {
                    SpannableStringBuilder truncatedSpannableString;
                    int startIndex;

                    if (textView.getLineCount() > maxLine) {
                        int lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                        String displayText = text.substring(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                        startIndex = displayText.indexOf(expandText);

                        truncatedSpannableString = new SpannableStringBuilder(displayText);

                        textView.setText(truncatedSpannableString);

                        // We are rechecking in order to resolve issue with alignment problem i.e. text is aligning in more than max no. of lines.
                        if (textView.getLineCount() > maxLine) {
                            lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                            displayText = text.subSequence(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                            truncatedSpannableString = new SpannableStringBuilder(displayText);
                            startIndex = displayText.indexOf(expandText);
                        }

                        truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });

                    } else if (showViewMore) {
                        String displayText = text + " " + expandText;
                        startIndex = displayText.indexOf(expandText);
                        truncatedSpannableString = new SpannableStringBuilder(displayText);
                        textView.setText(truncatedSpannableString);

                        truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
