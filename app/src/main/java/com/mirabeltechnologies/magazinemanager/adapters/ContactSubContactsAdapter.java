package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

/**
 * Created by venkat on 10/18/17.
 */

public class ContactSubContactsAdapter extends ContactsListViewBaseAdapter {

    public ContactSubContactsAdapter(Context context) {
        super(context);
    }

    @Override
    public int getCount() {
        return contactsList.size();
    }

    @Override
    public Contact getItem(int position) {
        return contactsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactsListViewHolder subContactsListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contacts_list_row_item, null);

                subContactsListViewHolder = new ContactsListViewHolder();
                subContactsListViewHolder.contact_type = (ImageView) convertView.findViewById(R.id.contact_type);
                subContactsListViewHolder.company_name = (MMTextView) convertView.findViewById(R.id.company_name);
                subContactsListViewHolder.employee_name = (MMTextView) convertView.findViewById(R.id.employee_name);

                convertView.setTag(subContactsListViewHolder);
            } else {
                subContactsListViewHolder = (ContactsListViewHolder) convertView.getTag();
            }

            final Contact contact = getItem(position);

            subContactsListViewHolder.contact_type.setImageResource(R.drawable.icon_sub_contact);

            if (contact.getCompanyName().isEmpty()) {
                subContactsListViewHolder.company_name.setText(R.string.company_name);
                subContactsListViewHolder.company_name.setTextColor(company_name_empty_title_color);
            } else {
                subContactsListViewHolder.company_name.setText(contact.getCompanyName());
                subContactsListViewHolder.company_name.setTextColor(company_name_title_color);
            }

            if (contact.getContactName().isEmpty()) {
                subContactsListViewHolder.employee_name.setText(R.string.name);
                subContactsListViewHolder.employee_name.setTextColor(company_name_empty_title_color);
            } else {
                subContactsListViewHolder.employee_name.setText(contact.getContactName());
                subContactsListViewHolder.employee_name.setTextColor(employee_name_title_color);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
