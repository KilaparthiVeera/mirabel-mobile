package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.NavDrawerItem;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;

/**
 * Created by venkat on 04/10/17.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private LayoutInflater layoutInflater;

    public NavigationDrawerAdapter(Context context, ArrayList<NavDrawerItem> items) {
        this.context = context;
        this.navDrawerItems = items;
        this.layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    public void delete(int position) {
        navDrawerItems.remove(position);
        notifyItemRemoved(position);
    }

    public void setNavDrawerItems(ArrayList<NavDrawerItem> navDrawerItems) {
        this.navDrawerItems = navDrawerItems;

        // reloading navigationDrawerAdapter items
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.nav_drawer_list_item, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem item = navDrawerItems.get(position);
        holder.icon.setImageResource(item.getIcon());
        holder.title.setText(item.getTitle());
    }

    @Override
    public int getItemCount() {
        return navDrawerItems.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        MMTextView title;

        public MyViewHolder(View itemView) {
            super(itemView);

            icon = (ImageView) itemView.findViewById(R.id.nav_drawer_icon);
            title = (MMTextView) itemView.findViewById(R.id.nav_drawer_title);
        }
    }
}
