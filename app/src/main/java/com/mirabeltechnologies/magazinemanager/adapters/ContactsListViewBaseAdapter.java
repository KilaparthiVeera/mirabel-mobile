package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import androidx.core.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 10/19/17.
 */

public abstract class ContactsListViewBaseAdapter extends BaseAdapter {
    protected Context context;
    protected LayoutInflater layoutInflater;
    protected List<Contact> contactsList;
    protected GlobalContent globalContent;
    protected int company_name_title_color, company_name_empty_title_color, employee_name_title_color;

    public ContactsListViewBaseAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.contactsList = new ArrayList<>();
        this.globalContent = GlobalContent.getInstance();

        this.company_name_title_color = ContextCompat.getColor(context, R.color.sub_contacts_company_name_title_color);
        this.company_name_empty_title_color = ContextCompat.getColor(context, R.color.sub_contacts_company_name_empty_title_color);
        this.employee_name_title_color = ContextCompat.getColor(context, R.color.sub_contacts_employee_name_title_color);
    }

    public void setContactsList(List<Contact> contacts) {
        this.contactsList = contacts;
    }

    static class ContactsListViewHolder {
        ImageView contact_type, phone_icon;
        MMTextView company_name, employee_name;
    }

    public int getResourceIdForContactTitle(String title) {
        int resourceId;

        switch (title) {
            case "Ad Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Agency Contact":
                resourceId = R.drawable.icon_agency_contact;
                break;
            case "Current-Future Contracts":
                resourceId = R.drawable.icon_company_future_orders;
                break;
            case "Past-Expired Contracts":
                resourceId = R.drawable.icon_company_expired_orders;
                break;
            case "Company":
                resourceId = R.drawable.icon_company_no_orders;
                break;
            case "Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            case "Child Contact":
                resourceId = R.drawable.icon_child_contact;
                break;
            default:
                resourceId = R.drawable.icon_sub_contact;
                break;
        }

        return resourceId;
    }

    public void addViewMoreToTextView(final int position, final TextView textView, final String text, final boolean showViewMore, final String expandText, final int maxLine, final ViewMoreClickListener listener) {
        try {
            textView.post(new Runnable() {
                @Override
                public void run() {
                    SpannableStringBuilder truncatedSpannableString;
                    int startIndex;

                    if (textView.getLineCount() > maxLine) {
                        int lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                        String displayText = text.substring(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                        startIndex = displayText.indexOf(expandText);

                        truncatedSpannableString = new SpannableStringBuilder(displayText);

                        textView.setText(truncatedSpannableString);

                        // We are rechecking in order to resolve issue with alignment problem i.e. text is aligning in more than max no. of lines.
                        if (textView.getLineCount() > maxLine) {
                            lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                            displayText = text.subSequence(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                            truncatedSpannableString = new SpannableStringBuilder(displayText);
                            startIndex = displayText.indexOf(expandText);
                        }

                        //truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.color_primary)), startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });

                    } else if (showViewMore) {
                        String displayText = text + " " + expandText;
                        startIndex = displayText.indexOf(expandText);
                        truncatedSpannableString = new SpannableStringBuilder(displayText);
                        textView.setText(truncatedSpannableString);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
