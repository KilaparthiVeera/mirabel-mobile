package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.beans.Task;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.TaskListListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.util.Utility;

/**
 * Created by venkat on 3/16/18.
 */

public class TaskListAdapter extends BaseAdapter {
    private Context context;
    private TaskListListener listener;
    private ViewMoreClickListener viewMoreClickListener;
    private GlobalContent globalContent;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;

    public TaskListAdapter(Context context, TaskListListener taskListListener, ViewMoreClickListener viewMoreClickListener) {
        this.context = context;
        this.listener = taskListListener;
        this.viewMoreClickListener = viewMoreClickListener;
        this.globalContent = GlobalContent.getInstance();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#205889"; // R.color.activity_list_value_color
    }

    static class TaskListViewHolder {
        MMTextView task_name, description, priority, project, assigned_by, assigned_to, due_date, start_date, task_private;
        ImageView mark_unmark_icon, delete_icon;
    }

    @Override
    public int getCount() {
        if (globalContent.getTaskListSearchResults() != null && globalContent.getTaskListSearchResults().size() > 0)
            return globalContent.getTaskListSearchResults().size();
        else
            return 0;
    }

    @Override
    public Task getItem(int position) {
        return globalContent.getTaskListSearchResults().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            TaskListViewHolder taskListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.task_list_row_item, null);

                taskListViewHolder = new TaskListViewHolder();
                taskListViewHolder.task_name = (MMTextView) convertView.findViewById(R.id.task);
                taskListViewHolder.description = (MMTextView) convertView.findViewById(R.id.task_description);
                taskListViewHolder.priority = (MMTextView) convertView.findViewById(R.id.task_priority);
                taskListViewHolder.project = (MMTextView) convertView.findViewById(R.id.task_project);
                taskListViewHolder.assigned_by = (MMTextView) convertView.findViewById(R.id.task_assigned_by);
                taskListViewHolder.assigned_to = (MMTextView) convertView.findViewById(R.id.task_assigned_to);
                taskListViewHolder.due_date = (MMTextView) convertView.findViewById(R.id.task_due_date);
                taskListViewHolder.start_date = (MMTextView) convertView.findViewById(R.id.task_start_date);
                taskListViewHolder.task_private = (MMTextView) convertView.findViewById(R.id.task_private);
                taskListViewHolder.mark_unmark_icon = (ImageView) convertView.findViewById(R.id.task_mark_unmark_icon);
                taskListViewHolder.delete_icon = (ImageView) convertView.findViewById(R.id.delete_task_icon);

                convertView.setTag(taskListViewHolder);
            } else {
                taskListViewHolder = (TaskListViewHolder) convertView.getTag();
            }

            final Task task = getItem(position);

            taskListViewHolder.task_name.setText(utility.getFormattedKeyAndColouredNormalText("Task", task.getName(), valueColorCode));
            taskListViewHolder.description.setText(utility.getFormattedKeyAndColouredNormalText("Description", task.getModifiedDescription(), valueColorCode));
            taskListViewHolder.priority.setText(utility.getFormattedKeyAndColouredNormalText("Priority", task.getPriority(), valueColorCode));
            taskListViewHolder.project.setText(utility.getFormattedKeyAndColouredNormalText("Project", task.getProjectName(), valueColorCode));
            taskListViewHolder.assigned_by.setText(utility.getFormattedKeyAndColouredNormalText("Assigned By", task.getAssignedByRepName(), valueColorCode));
            taskListViewHolder.assigned_to.setText(utility.getFormattedKeyAndColouredNormalText("Assigned To", task.getAssignedToRepName(), valueColorCode));
            taskListViewHolder.due_date.setText(utility.getFormattedKeyAndColouredNormalText("Due Date", task.getDueDate(), valueColorCode));
            taskListViewHolder.start_date.setText(utility.getFormattedKeyAndColouredNormalText("Start Date", task.getStartDate(), valueColorCode));
            taskListViewHolder.task_private.setText(utility.getFormattedKeyAndColouredNormalText("Private", task.isPrivateNotes() ? "Yes" : "No", valueColorCode));

            // adding more to notes text view at end of 3rd line to show complete notes in pop up when we click on it.
            addViewMoreToTextView(position, taskListViewHolder.description, "Description : ", task.getModifiedDescription(), task.isHasHTMLTags(), Constants.VIEW_MORE_EXPAND_TEXT, 3, viewMoreClickListener);

            if (String.valueOf(task.getCanViewEmployeeId()).equals(Constants.CONTACT_READ_WRITE_ACCESS)) {
                if (task.getCompleted() == 0) {
                    taskListViewHolder.mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                } else {
                    taskListViewHolder.mark_unmark_icon.setImageResource(R.drawable.mark_uncompleted);
                }

                taskListViewHolder.mark_unmark_icon.setVisibility(View.VISIBLE);
                taskListViewHolder.delete_icon.setVisibility(View.VISIBLE);
            } else {
                taskListViewHolder.mark_unmark_icon.setVisibility(View.GONE);
                taskListViewHolder.delete_icon.setVisibility(View.GONE);
            }

            taskListViewHolder.mark_unmark_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (task.getCompleted() == 0)
                            listener.markTask(position, task.getTaskId(), Constants.TASK_ACTION_MARK_COMPLETE);
                        else
                            listener.markTask(position, task.getTaskId(), Constants.TASK_ACTION_MARK_UN_COMPLETE);
                    }
                }
            });

            taskListViewHolder.delete_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.deleteTask(position, task.getTaskId(), Constants.TASK_ACTION_DELETE);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void addViewMoreToTextView(final int position, final TextView textView, final String titleText, String notes, final boolean showViewMore, final String expandText, final int maxLine, final ViewMoreClickListener listener) {
        try {
            final String text = titleText + notes;

            textView.post(new Runnable() {
                @Override
                public void run() {
                    SpannableStringBuilder truncatedSpannableString;
                    int startIndex;

                    if (textView.getLineCount() > maxLine) {
                        try {
                            int lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                            String displayText = text.substring(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                            startIndex = displayText.indexOf(expandText);

                            truncatedSpannableString = new SpannableStringBuilder(displayText);

                            textView.setText(truncatedSpannableString);

                            // We are rechecking in order to resolve issue with alignment problem i.e. text is aligning in more than max no. of lines.
                            if (textView.getLineCount() > maxLine) {
                                lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                                displayText = text.subSequence(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                                truncatedSpannableString = new SpannableStringBuilder(displayText);
                                startIndex = displayText.indexOf(expandText);
                            }

                            truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                            truncatedSpannableString.setSpan(new ClickableSpan() {
                                @Override
                                public void onClick(View widget) {
                                    // this click event is not firing that's why we are adding click event for text view below.
                                }
                            }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                            textView.setText(truncatedSpannableString);

                            textView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (listener != null)
                                        listener.viewMoreClicked(position);
                                }
                            });

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (showViewMore) {
                        String displayText = text + " " + expandText;
                        startIndex = displayText.indexOf(expandText);
                        truncatedSpannableString = new SpannableStringBuilder(displayText);
                        textView.setText(truncatedSpannableString);

                        truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
