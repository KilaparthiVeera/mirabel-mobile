package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Order;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.OrdersListener;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by venkat on 4/10/18.
 */

public class OrdersAdapter extends BaseAdapter {
    private Context context;
    private OrdersListener listener;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;
    private List<Order> ordersList;
    private boolean isBillingInstallmentSelected = false;

    public OrdersAdapter(Context context, OrdersListener ordersListener, boolean isBillingInstallmentSelected) {
        this.context = context;
        this.listener = ordersListener;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#205889"; // R.color.activity_list_value_color
        this.ordersList = new ArrayList<>();
        this.isBillingInstallmentSelected = isBillingInstallmentSelected;
    }

    public void setOrdersList(List<Order> orders) {
        if (orders == null) {
            if (this.ordersList != null) {
                this.ordersList.clear();
                this.ordersList = null;
            }

            this.ordersList = new ArrayList<>();
        } else {
            if (this.ordersList != null && this.ordersList.size() > 0)
                this.ordersList.addAll(orders);
            else
                this.ordersList = orders;
        }
    }

    static class OrdersListViewHolder {
        ImageView order_type_icon;
        MMTextView order_client, order_product, order_description, order_sales_rep, order_date, order_net, order_barter;
    }

    @Override
    public int getCount() {
        return ordersList.size();
    }

    @Override
    public Order getItem(int position) {
        return ordersList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            OrdersListViewHolder ordersListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.orders_list_row_item, null);

                ordersListViewHolder = new OrdersListViewHolder();
                ordersListViewHolder.order_type_icon = (ImageView) convertView.findViewById(R.id.order_type_icon);
                ordersListViewHolder.order_client = (MMTextView) convertView.findViewById(R.id.order_client);
                ordersListViewHolder.order_product = (MMTextView) convertView.findViewById(R.id.order_product);
                ordersListViewHolder.order_description = (MMTextView) convertView.findViewById(R.id.order_description);
                ordersListViewHolder.order_sales_rep = (MMTextView) convertView.findViewById(R.id.order_sales_rep);
                ordersListViewHolder.order_date = (MMTextView) convertView.findViewById(R.id.order_date);
                ordersListViewHolder.order_net = (MMTextView) convertView.findViewById(R.id.order_net);
                ordersListViewHolder.order_barter = (MMTextView) convertView.findViewById(R.id.order_barter);

                convertView.setTag(ordersListViewHolder);
            } else {
                ordersListViewHolder = (OrdersListViewHolder) convertView.getTag();
            }

            final Order order = getItem(position);

            if (order.getSubProductTypeId().equals(Constants.ORDER_TYPE_PRINT))
                ordersListViewHolder.order_type_icon.setImageResource(R.drawable.order_print);
            else
                ordersListViewHolder.order_type_icon.setImageResource(R.drawable.order_digital);

            ordersListViewHolder.order_client.setText(utility.getFormattedKeyAndColouredTextWithUnderscore("Client", order.getCustomer(), valueColorCode));
            ordersListViewHolder.order_product.setText(utility.getFormattedKeyAndColouredNormalText("Product", order.getProduct(), valueColorCode));
            ordersListViewHolder.order_description.setText(utility.getFormattedKeyAndColouredNormalText("Description", order.getDescription(), valueColorCode));
            ordersListViewHolder.order_sales_rep.setText(utility.getFormattedKeyAndColouredNormalText("Sales Rep", order.getSalesRep(), valueColorCode));
            ordersListViewHolder.order_date.setText(utility.getFormattedKeyAndColouredNormalText("Date", order.getStartDate(), valueColorCode));
            ordersListViewHolder.order_net.setText(utility.getFormattedKeyAndColouredNormalText("Net", String.format("$%,.2f", Float.parseFloat(order.getNet())), valueColorCode));
            ordersListViewHolder.order_barter.setText(utility.getFormattedKeyAndColouredNormalText("Barter", String.format("$%,.2f", Float.parseFloat(order.getBarter())), valueColorCode));

            if (isBillingInstallmentSelected)
                ordersListViewHolder.order_barter.setVisibility(View.GONE);
            else
                ordersListViewHolder.order_barter.setVisibility(View.VISIBLE);

            ordersListViewHolder.order_client.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.openContactDetailPage(Long.parseLong(order.getCustomerId()));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
