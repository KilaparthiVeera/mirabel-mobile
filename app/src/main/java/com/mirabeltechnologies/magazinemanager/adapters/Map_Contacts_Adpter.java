package com.mirabeltechnologies.magazinemanager.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewActivityForContactScreen;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.util.List;

public class Map_Contacts_Adpter extends RecyclerView.Adapter<Map_Contacts_Adpter.Map_contacts_Holder> {

    private Context context;
    private List<NearbyContacts> noteDataList;

    public Map_Contacts_Adpter(Context context, List<NearbyContacts> noteDataList) {
        this.context = context;
        this.noteDataList = noteDataList;
    }
    @NonNull
    @Override
    public Map_contacts_Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem= layoutInflater.inflate(R.layout.map_contacts_list, viewGroup, false);
        return new Map_contacts_Holder(listItem);

    }
    @Override
    public void onBindViewHolder(@NonNull Map_contacts_Holder holder, int i) {
        holder.textView.setText(noteDataList.get(i).getName());
        holder.contacts_layout.setOnClickListener(v -> {
            Activity activity = (Activity) context;
            Intent showCreateActivityForContactIntent = new Intent(context, CreateNewActivityForContactScreen.class);
            Bundle bundle = new Bundle();
            bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID,String.valueOf(noteDataList.get(i).getGsCustomersID()) );
            bundle.putLong(Constants.BUNDLE_CONTACT_REP_ID,  noteDataList.get(i).getRepId());
            bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_NOTES);
            showCreateActivityForContactIntent.putExtras(bundle);
            activity.startActivity(showCreateActivityForContactIntent);
            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
            Toast.makeText(context, ""+noteDataList.get(i).getName(), Toast.LENGTH_SHORT).show();

        });
    }
    @Override
    public int getItemCount() {
        return noteDataList.size();
    }


   public static class Map_contacts_Holder extends RecyclerView.ViewHolder {
        TextView textView;
        LinearLayout contacts_layout;
        Map_contacts_Holder(View itemView) {
            super(itemView);
            contacts_layout = itemView.findViewById(R.id.contacts_layout);
            textView = itemView.findViewById(R.id.textView);

        }
    }
}
