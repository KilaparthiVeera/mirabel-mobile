package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrder;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.List;

/**
 * Created by venkat on 3/22/18.
 */

public class ContactOrdersAdapter extends BaseAdapter {
    private Context context;
    private List<ContactOrder> orders;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;

    public ContactOrdersAdapter(Context context, List<ContactOrder> contactOrders) {
        this.context = context;
        this.orders = contactOrders;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#000000"; // R.color.activity_list_value_color
    }

    static class ContactOrdersListViewHolder {
        MMTextView year_net_count, gross, barter;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public ContactOrder getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            ContactOrdersListViewHolder contactOrdersListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contact_orders_list_row_item, null);

                contactOrdersListViewHolder = new ContactOrdersListViewHolder();
                contactOrdersListViewHolder.year_net_count = (MMTextView) convertView.findViewById(R.id.co_year_net_count);
                contactOrdersListViewHolder.gross = (MMTextView) convertView.findViewById(R.id.co_gross);
                contactOrdersListViewHolder.barter = (MMTextView) convertView.findViewById(R.id.co_barter);

                convertView.setTag(contactOrdersListViewHolder);
            } else {
                contactOrdersListViewHolder = (ContactOrdersListViewHolder) convertView.getTag();
            }

            final ContactOrder order = getItem(position);

            contactOrdersListViewHolder.year_net_count.setText(String.format("%s - $%,.2f (%,d)", order.getYear(), order.getNet(), order.getOrdersCount()));
            contactOrdersListViewHolder.gross.setText(utility.getFormattedKeyAndColouredBoldText("Gross", String.format("$%,.2f", order.getGross()), valueColorCode));
            contactOrdersListViewHolder.barter.setText(utility.getFormattedKeyAndColouredBoldText("Barter", String.format("$%,.2f", order.getBarter()), valueColorCode));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
