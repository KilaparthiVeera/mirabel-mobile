package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.graphics.Typeface;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.RightSideDrawerListWithSearchActivity;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.beans.DateRange;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.OppLossReasonDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppTypeDetails;
import com.mirabeltechnologies.magazinemanager.beans.Probability;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SubContactDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by venkat on 11/9/17.
 */

public class ListViewSearchAdapter extends ArrayAdapter {
    private final Context context;
    private final int resourceId;
    private final LayoutInflater layoutInflater;
    private final List allValues;
    private final List filteredValues;
    private final Constants.SelectionType selectionType;

    public ListViewSearchAdapter(Context ctx, int resource, List objects, Constants.SelectionType selectionType) {
        super(ctx, resource, objects);

        this.context = ctx;
        this.resourceId = resource;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.allValues = objects;
        this.filteredValues = new ArrayList<>();
        this.filteredValues.addAll(allValues);
        this.selectionType = selectionType;
    }

    static class ListViewSearchViewHolder {
        MMTextView textView;
    }

    @Override
    public int getCount() {
        return filteredValues.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredValues.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
        ListViewSearchViewHolder viewHolder;

        try {
            if (convertView == null) {
                convertView = layoutInflater.inflate(resourceId, parent, false);

                viewHolder = new ListViewSearchViewHolder();
                viewHolder.textView = convertView.findViewById(R.id.textView);

                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ListViewSearchViewHolder) convertView.getTag();
            }

            if (selectionType == Constants.SelectionType.REP || selectionType == Constants.SelectionType.ASSIGNED_BY || selectionType == Constants.SelectionType.ASSIGNED_TO || selectionType == Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY || selectionType == Constants.SelectionType.SALES_REP) {
                final RepData repData = (RepData) getItem(position);

                viewHolder.textView.setText(repData.getName());

                if ((repData.getId()).equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) { //if ((repData.getId()).equals(((RepData) RightSideDrawerListWithSearchActivity.previouslySelectedValue).getId())) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                    viewHolder.textView.setTypeface(null, Typeface.BOLD);
                } else {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                    viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                }
            }
            else if (selectionType == Constants.SelectionType.COMPANYNAME || selectionType == Constants.SelectionType.NAME || selectionType == Constants.SelectionType.JOB ||
                    selectionType == Constants.SelectionType.ADDRESS1 || selectionType == Constants.SelectionType.ADDRESS2 || selectionType == Constants.SelectionType.CITY ||
                    selectionType == Constants.SelectionType.COUNTY || selectionType == Constants.SelectionType.STATE || selectionType == Constants.SelectionType.COUNTRY  ) {
                viewHolder.textView.setText(getItem(position).toString());
            }

            else if (selectionType == Constants.SelectionType.DATE_RANGE) {
                final DateRange dateRange = (DateRange) getItem(position);

                viewHolder.textView.setText(dateRange.getValue());

                if (dateRange.getValue().equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                    viewHolder.textView.setTypeface(null, Typeface.BOLD);
                } else {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                    viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                }

            } else if (selectionType == Constants.SelectionType.CLIENT) {
                ClientInfo clientInfo = (ClientInfo) getItem(position);

                viewHolder.textView.setText(clientInfo.getClientName());

                if (clientInfo.getClientId() == ClientInfo.selectedClientId) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.white));
                    viewHolder.textView.setTypeface(null, Typeface.BOLD);
                } else {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.client_list_text_color));
                    viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STATUS) {
                String status = (String) getItem(position);
                if (status != null) {
                    viewHolder.textView.setText(status);
                    if (status.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }
            else if (selectionType == Constants.SelectionType.ADD_PROBABIlILITY) {
                String probability = (String) getItem(position);
                if (probability != null) {
                    viewHolder.textView.setText(probability);
                    if (probability.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STAGE) {
                OppStageDetails stageDetails = (OppStageDetails) getItem(position);
                if (stageDetails != null) {
                    viewHolder.textView.setText(stageDetails.getStage());
                    String id= String.valueOf(stageDetails.getId());
                    if (id.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }

             else if (selectionType == Constants.SelectionType.OPPORTUNITY_TYPE) {
                OppTypeDetails stageDetails = (OppTypeDetails) getItem(position);
                if (stageDetails != null) {
                    viewHolder.textView.setText(stageDetails.getName());
                    String id= String.valueOf(stageDetails.getId());
                    if (id.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }
              else if (selectionType == Constants.SelectionType.OPPORTUNITY_REASON) {
                OppLossReasonDetails stageDetails = (OppLossReasonDetails) getItem(position);
                if (stageDetails != null) {
                    viewHolder.textView.setText(stageDetails.getName());
                    String id= String.valueOf(stageDetails.getId());
                    if (id.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }
              else if (selectionType == Constants.SelectionType.OPPORTUNITY_CONTACT) {
                SubContactDetails stageDetails = (SubContactDetails) getItem(position);
                if (stageDetails != null) {
                    viewHolder.textView.setText(stageDetails.getContactFullName());
                    String id= String.valueOf(stageDetails.getId());
                    if (id.equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                        viewHolder.textView.setTypeface(null, Typeface.BOLD);
                    } else {
                        viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                        viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                    }
                }

            }

            else { //if (selectionType == Constants.SelectionType.CATEGORY || selectionType == Constants.SelectionType.PRIORITY || selectionType == Constants.SelectionType.CONTACT_TYPE || selectionType == Constants.SelectionType.ACTIVITY_TYPE || selectionType == Constants.SelectionType.DEPARTMENT || selectionType == Constants.SelectionType.PROJECT) {
                final MasterData masterData = (MasterData) getItem(position);

                viewHolder.textView.setText(masterData.getName());

                if (masterData.getId() != null && (masterData.getId()).equals(RightSideDrawerListWithSearchActivity.rightSideDrawerSelectedIndex)) {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.color_primary));
                    viewHolder.textView.setTypeface(null, Typeface.BOLD);
                } else {
                    viewHolder.textView.setTextColor(ContextCompat.getColor(context, R.color.right_side_menu_text_color));
                    viewHolder.textView.setTypeface(null, Typeface.NORMAL);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());

        filteredValues.clear();

        if (charText.length() == 0) {
            filteredValues.addAll(allValues);
        } else {
            if (selectionType == Constants.SelectionType.CLIENT) {
                for (ClientInfo clientInfo : (ArrayList<ClientInfo>) allValues) {
                    if (clientInfo.getClientName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(clientInfo);
                    }
                }
            } else if (selectionType == Constants.SelectionType.REP || selectionType == Constants.SelectionType.ASSIGNED_BY || selectionType == Constants.SelectionType.ASSIGNED_TO || selectionType == Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY || selectionType == Constants.SelectionType.SALES_REP) {
                for (RepData repData : (ArrayList<RepData>) allValues) {
                    if (repData.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(repData);
                    }
                }
            } else if (selectionType == Constants.SelectionType.DATE_RANGE) {
                for (DateRange dateRange : (ArrayList<DateRange>) allValues) {
                    if (dateRange.getValue().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(dateRange);
                    }
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STAGE) {
                for (OppStageDetails clientInfo : (ArrayList<OppStageDetails>) allValues) {
                    if (clientInfo.getStage().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(clientInfo);
                    }
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STATUS) {
                for (String status : (ArrayList<String>) allValues) {
                    if (status.toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(status);
                    }
                }
            }
            else if (selectionType == Constants.SelectionType.ADD_PROBABIlILITY) {
                for (String probability : (ArrayList<String>) allValues) {
                    if (probability.toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(probability);
                    }
                }
            }


            else if (selectionType == Constants.SelectionType.OPPORTUNITY_TYPE) {
                for (OppTypeDetails clientInfo : (ArrayList<OppTypeDetails>) allValues) {
                    if (clientInfo.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(clientInfo);
                    }
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_REASON) {
                for (OppLossReasonDetails clientInfo : (ArrayList<OppLossReasonDetails>) allValues) {
                    if (clientInfo.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(clientInfo);
                    }
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_CONTACT) {
                for (SubContactDetails clientInfo : (ArrayList<SubContactDetails>) allValues) {
                    if (clientInfo.getContactFullName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(clientInfo);
                    }
                }
            }

            else if (selectionType == Constants.SelectionType.COMPANYNAME || selectionType == Constants.SelectionType.NAME || selectionType == Constants.SelectionType.JOB ||
                    selectionType == Constants.SelectionType.ADDRESS1 || selectionType == Constants.SelectionType.ADDRESS2 || selectionType == Constants.SelectionType.CITY ||
                    selectionType == Constants.SelectionType.COUNTY || selectionType == Constants.SelectionType.STATE || selectionType == Constants.SelectionType.COUNTRY  ) {
                for (String masterData : (ArrayList<String>) allValues) {
                    if (masterData.toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(masterData);
                    }
                }
            }
            else { //if (selectionType == Constants.SelectionType.CATEGORY || selectionType == Constants.SelectionType.PRIORITY || selectionType == Constants.SelectionType.CONTACT_TYPE || selectionType == Constants.SelectionType.ACTIVITY_TYPE || selectionType == Constants.SelectionType.DEPARTMENT || selectionType == Constants.SelectionType.PROJECT) {
                for (MasterData masterData : (ArrayList<MasterData>) allValues) {
                    if (masterData.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        filteredValues.add(masterData);
                    }
                }
            }
        }
        notifyDataSetChanged();
    }
}
