package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;

public class SpinnerAdapter extends ArrayAdapter<String> {
    private Context context;
    private int textViewResourceId;
    private ArrayList<String> objects;

    public SpinnerAdapter(Context context, int textViewResourceId, ArrayList<String> objects) {
        super(context, textViewResourceId, objects);

        this.context = context;
        this.textViewResourceId = textViewResourceId;
        this.objects = objects;
        this.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    }

    public void setObjects(ArrayList<String> objects) {
        this.objects = objects;
    }

    private class SpinnerViewHolder {
        MMTextView textView;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public String getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup viewGroup) {
        try {
            SpinnerViewHolder spinnerViewHolder;

            if (convertView == null) {
                convertView = View.inflate(context, textViewResourceId, null);

                spinnerViewHolder = new SpinnerViewHolder();
                spinnerViewHolder.textView = (MMTextView) convertView.findViewById(R.id.spinner_row_text);

                convertView.setTag(spinnerViewHolder);

            } else {
                spinnerViewHolder = (SpinnerViewHolder) convertView.getTag();
            }

            spinnerViewHolder.textView.setText(getItem(position));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
