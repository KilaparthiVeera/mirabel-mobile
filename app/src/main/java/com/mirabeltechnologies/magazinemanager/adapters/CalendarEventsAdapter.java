package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEvent;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.ArrayList;
import java.util.List;

public class CalendarEventsAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<CalendarEvent> calendarEvents;

    public CalendarEventsAdapter(Context context) {
        this.context = context;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.calendarEvents = new ArrayList<>();
    }

    public void setCalendarEvents(List<CalendarEvent> calendarEvents) {
        this.calendarEvents = calendarEvents;
    }

    static class CalendarEventsListViewHolder {
        MMTextView time_slot_name, event_title;
    }

    @Override
    public int getCount() {

        return calendarEvents.size();
    }

    @Override
    public CalendarEvent getItem(int position) {

        return calendarEvents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            CalendarEventsListViewHolder calendarEventsListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.calendar_events_list_row_item, null);

                calendarEventsListViewHolder = new CalendarEventsListViewHolder();
                calendarEventsListViewHolder.time_slot_name = (MMTextView) convertView.findViewById(R.id.time_slot_name);
                calendarEventsListViewHolder.event_title = (MMTextView) convertView.findViewById(R.id.event_title);

                convertView.setTag(calendarEventsListViewHolder);
            } else {
                calendarEventsListViewHolder = (CalendarEventsListViewHolder) convertView.getTag();
            }

            CalendarEvent calendarEvent = getItem(position);

            if (calendarEvent.isHeader()) {
                calendarEventsListViewHolder.time_slot_name.setText(calendarEvent.getModifiedTimeSlotName());
                calendarEventsListViewHolder.time_slot_name.setVisibility(View.VISIBLE);
            } else {
                calendarEventsListViewHolder.time_slot_name.setVisibility(View.GONE);
            }

            calendarEventsListViewHolder.event_title.setText(calendarEvent.getTitle());

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
