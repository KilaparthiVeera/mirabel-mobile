package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.QuickSearchContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.fragments.DashboardFragment;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.NetworkManager;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by venkat on 10/27/17.
 */

public class QuickSearchAdapter extends BaseAdapter implements Filterable {
    private Context context;
    public String encryptedClientKey;
    private LayoutInflater layoutInflater;
    private ArrayList<QuickSearchContact> quickSearchFilteredContacts;
    private int employee_name_title_color, employee_name_empty_title_color, employee_type_title_color;

    public QuickSearchAdapter(Context context, String clientKey) {
        this.context = context;
        this.encryptedClientKey = clientKey;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.quickSearchFilteredContacts = new ArrayList<>();

        this.employee_name_title_color = ContextCompat.getColor(context, R.color.quick_search_contacts_employee_name_title_color);
        this.employee_name_empty_title_color = ContextCompat.getColor(context, R.color.quick_search_contacts_employee_name_empty_title_color);
        this.employee_type_title_color = ContextCompat.getColor(context, R.color.quick_search_contacts_employee_type_title_color);
    }

    static class QuickSearchContactsViewHolder {
        ImageView contact_type, rc_right_arrow, phone_icon;
        MMTextView employee_name, employee_type;
    }

    @Override
    public int getCount() {
        return quickSearchFilteredContacts.size();
    }

    @Override
    public QuickSearchContact getItem(int position) {
        return quickSearchFilteredContacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        try {
            QuickSearchContactsViewHolder quickSearchContactsViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.recent_contacts_row_item, null);

                quickSearchContactsViewHolder = new QuickSearchContactsViewHolder();
                quickSearchContactsViewHolder.contact_type = (ImageView) convertView.findViewById(R.id.contact_type);
                quickSearchContactsViewHolder.employee_name = (MMTextView) convertView.findViewById(R.id.company_name);
                quickSearchContactsViewHolder.employee_type = (MMTextView) convertView.findViewById(R.id.employee_name);
                quickSearchContactsViewHolder.rc_right_arrow = (ImageView) convertView.findViewById(R.id.rc_right_arrow);
                quickSearchContactsViewHolder.phone_icon = (ImageView) convertView.findViewById(R.id.phone_icon);

                convertView.setTag(quickSearchContactsViewHolder);
            } else {
                quickSearchContactsViewHolder = (QuickSearchContactsViewHolder) convertView.getTag();
            }

            final QuickSearchContact contact = getItem(position);

            //quickSearchContactsViewHolder.contact_type.setImageResource(getResourceIdForContactType(contact.getType()));
            quickSearchContactsViewHolder.contact_type.setVisibility(View.GONE);
            quickSearchContactsViewHolder.rc_right_arrow.setVisibility(View.GONE);
            quickSearchContactsViewHolder.phone_icon.setVisibility(View.GONE);

            if (contact.getName().isEmpty()) {
                quickSearchContactsViewHolder.employee_name.setText(R.string.name);
                //quickSearchContactsViewHolder.employee_name.setTextColor(employee_name_empty_title_color);
            } else {
                quickSearchContactsViewHolder.employee_name.setText(contact.getName());
                //quickSearchContactsViewHolder.employee_name.setTextColor(employee_name_title_color);
            }

            quickSearchContactsViewHolder.employee_name.setTextColor(Color.parseColor(contact.getColor()));
            quickSearchContactsViewHolder.employee_name.setTypeface(Typeface.DEFAULT_BOLD);

            quickSearchContactsViewHolder.employee_type.setText(contact.getType());
            quickSearchContactsViewHolder.employee_type.setTextColor(employee_type_title_color);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public int getResourceIdForContactType(String type) {
        int resourceId;

        switch (type) {
            case "Rep":
                resourceId = R.drawable.icon_rep;
                break;
            case "Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Company":
                resourceId = R.drawable.icon_company_no_orders;
                break;
            case "Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            default:
                resourceId = R.drawable.icon_sub_contact;
                break;
        }

        return resourceId;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                final FilterResults filterResults = new FilterResults();

                if (charSequence != null) {
                    try {
                        if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                            ArrayList<QuickSearchContact> quickSearchResults = new ArrayList<>();

                            String keyword = charSequence.toString();

                            String responseStr = new GetQuickSearchContactsAsyncTask().execute(keyword).get();

                            if (responseStr != null) {
                                try {
                                    JSONObject response = new JSONObject(responseStr);

                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        Type quickSearchContactsListType = new TypeToken<ArrayList<QuickSearchContact>>() {
                                        }.getType();

                                        quickSearchResults = new Gson().fromJson(response.getString("Contacts"), quickSearchContactsListType);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                // Assign the data to the FilterResults
                                filterResults.values = quickSearchResults;
                                filterResults.count = quickSearchResults.size();
                            }

                        } else {
                            Toast.makeText(context, context.getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults results) {
                if (results != null && results.count > 0) {
                    quickSearchFilteredContacts = (ArrayList<QuickSearchContact>) results.values;
                    notifyDataSetChanged();

                    DashboardFragment.changeQuickSearchVoiceRecorderIcon(2);
                } else {
                    notifyDataSetInvalidated();

                    DashboardFragment.changeQuickSearchVoiceRecorderIcon(1);
                }
            }
        };

        return filter;
    }

    private class GetQuickSearchContactsAsyncTask extends AsyncTask<String, Void, String> {
        String responseString = null;

        @Override
        protected String doInBackground(String... strings) {
            try {
                String keyword = strings[0];
                String dtTicks = new EncryptDecryptStringWithDES(false).getEncryptedValueWithOutReplacingCharacters(String.valueOf(System.currentTimeMillis()));
                String getQuickSearchContactsReqURL = String.format("%s/%s?searchText=%s&dtticks=%s", Constants.MAIN_URL, Constants.QUICK_SEARCH, URLEncoder.encode(keyword, Constants.CHARSET_NAME), dtTicks);
                Log.e("veera",getQuickSearchContactsReqURL);

                Log.e("veera",encryptedClientKey);

                Map<String, String> headers = new HashMap<>();
                headers.put(Constants.AUTHORISATION, encryptedClientKey);

                responseString = new NetworkManager().makeHttpGetConnectionWithHeaders(getQuickSearchContactsReqURL, headers);

            } catch (Exception e) {
                e.printStackTrace();
            }

            return responseString;
        }
    }
}
