package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.SaveList;

public class MySaveListAdpter extends ContactsListViewBaseAdapter {


    public MySaveListAdpter(Context context) {
        super(context);
    }

    @Override
    public int getCount() {
        if (globalContent.getSaveLists() != null && globalContent.getSaveLists().size() > 0)
            return globalContent.getSaveLists().size();
        else
            return 0;
    }

    @Override
    public SaveList getItem(int position) {
        return globalContent.getSaveLists().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactsListViewHolder myContactsListViewHolder;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.save_list, null);

                myContactsListViewHolder = new ContactsListViewHolder();
                myContactsListViewHolder.company_name = convertView.findViewById(R.id.company_name);


                convertView.setTag(myContactsListViewHolder);
            } else {
                myContactsListViewHolder = (ContactsListViewHolder) convertView.getTag();
            }

            final SaveList contact = getItem(position);


            if (contact.getDisplay().isEmpty()) {
                myContactsListViewHolder.company_name.setText(R.string.company_name);
                myContactsListViewHolder.company_name.setTextColor(company_name_empty_title_color);
            } else {
                myContactsListViewHolder.company_name.setText(contact.getDisplay());
                myContactsListViewHolder.company_name.setTextColor(company_name_title_color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
