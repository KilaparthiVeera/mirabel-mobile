package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.SearchActivitiesListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.util.Utility;

/**
 * Created by venkat on 1/2/18.
 */

public class SearchActivitiesAdapter extends BaseAdapter {
    private Context context;
    private SearchActivitiesListener listener;
    private ViewMoreClickListener viewMoreClickListener;
    private String currentActivityType;
    private GlobalContent globalContent;
    private LayoutInflater layoutInflater;
    private Utility utility;
    private String valueColorCode;

    public SearchActivitiesAdapter(Context context, SearchActivitiesListener searchActivitiesListener, ViewMoreClickListener viewMoreClickListener, String activityType) {
        this.context = context;
        this.listener = searchActivitiesListener;
        this.viewMoreClickListener = viewMoreClickListener;
        this.currentActivityType = activityType;
        this.globalContent = GlobalContent.getInstance();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.utility = Utility.getInstance();
        this.valueColorCode = "#205889"; // R.color.activity_list_value_color
    }

    static class SearchActivitiesListViewHolder {
        MMTextView notes, company_name, activity_type, date_scheduled, assigned_to;
        ImageView activity_type_icon, mark_unmark_icon, delete_activity_icon;
        LinearLayout ll_openevent;
    }

    @Override
    public int getCount() {
        if (globalContent.getActivitiesSearchResults() != null && globalContent.getActivitiesSearchResults().size() > 0)
            return globalContent.getActivitiesSearchResults().size();
        else
            return 0;
    }

    @Override
    public ActivityData getItem(int position) {
        return globalContent.getActivitiesSearchResults().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        try {
            SearchActivitiesListViewHolder activitiesListViewHolder = null;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.search_activities_list_row_item, null);

                activitiesListViewHolder = new SearchActivitiesListViewHolder();
                activitiesListViewHolder.activity_type_icon = (ImageView) convertView.findViewById(R.id.activity_type_icon);
                activitiesListViewHolder.notes = (MMTextView) convertView.findViewById(R.id.activity_list_notes);
                activitiesListViewHolder.company_name = (MMTextView) convertView.findViewById(R.id.activity_list_company_name);
                activitiesListViewHolder.activity_type = (MMTextView) convertView.findViewById(R.id.activity_list_activity_type);
                activitiesListViewHolder.date_scheduled = (MMTextView) convertView.findViewById(R.id.activity_list_date_scheduled);
                activitiesListViewHolder.assigned_to = (MMTextView) convertView.findViewById(R.id.activity_list_assigned_to);
                activitiesListViewHolder.mark_unmark_icon = (ImageView) convertView.findViewById(R.id.mark_unmark_icon);
                activitiesListViewHolder.delete_activity_icon = (ImageView) convertView.findViewById(R.id.delete_activity_icon);
                activitiesListViewHolder.ll_openevent = convertView.findViewById(R.id.ll_openevent);


                convertView.setTag(activitiesListViewHolder);


            } else {
                activitiesListViewHolder = (SearchActivitiesListViewHolder) convertView.getTag();
            }

            final ActivityData activityData = getItem(position);

            activitiesListViewHolder.notes.setText(utility.getFormattedKeyAndColouredNormalText("Notes", activityData.getModifiedNotes(), valueColorCode));

            Drawable contactTypeDrawable = context.getResources().getDrawable(getResourceIdForCustomerType(activityData.getCustomerType()));
            contactTypeDrawable.setBounds(0, 0, activitiesListViewHolder.company_name.getLineHeight(), activitiesListViewHolder.company_name.getLineHeight());
            ImageSpan contactTypeImageSpan = new ImageSpan(contactTypeDrawable);
            SpannableStringBuilder companyNameBuilder = new SpannableStringBuilder();
            companyNameBuilder.append("Company Name :  ");
            companyNameBuilder.setSpan(contactTypeImageSpan, companyNameBuilder.length() - 1, companyNameBuilder.length(), 0);
            companyNameBuilder.append(" ");
            companyNameBuilder.append(utility.getFormattedColouredTextWithUnderscore(activityData.getCompanyName(), valueColorCode));
            activitiesListViewHolder.company_name.setText(companyNameBuilder);

            activitiesListViewHolder.activity_type.setText(utility.getFormattedKeyAndColouredNormalText("Activity Type", activityData.getMeetingName(), valueColorCode));
            activitiesListViewHolder.date_scheduled.setText(utility.getFormattedKeyAndColouredNormalText("Date Scheduled", activityData.getDateScheduled(), valueColorCode));
            activitiesListViewHolder.assigned_to.setText(utility.getFormattedKeyAndColouredNormalText("Assigned To", activityData.getAssignedTo(), valueColorCode));

            // adding more to notes text view at end of 3rd line to show complete notes in pop up when we click on it.
            addViewMoreToTextView(position, activitiesListViewHolder.notes, "Notes : ", activityData.getModifiedNotes(), activityData.isHasHTMLTags(), Constants.VIEW_MORE_EXPAND_TEXT, 3, viewMoreClickListener);

            /*if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
                activitiesListViewHolder.activity_type_icon.setVisibility(View.VISIBLE);
            } else {
                activitiesListViewHolder.activity_type_icon.setVisibility(View.GONE);
            }*/

            if (activityData.getType().equals("Note")) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_notes);

                activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);

                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.VISIBLE);
                    else
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                }
            } else if (activityData.getType().equals("Call")) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_call);

                if (activityData.getCanEditNotes() == 0) { // user account level access
                    activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);
                } else {
                    if (activityData.getCanEdit() == 0) { // additional rep security configuration
                        if (activityData.getCompleted().equals("False")) {
                            activitiesListViewHolder.mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                            activitiesListViewHolder.mark_unmark_icon.setVisibility(View.VISIBLE);
                        } else {
                            activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);
                    }
                }

                if (activityData.getCanDeleteNotes() == 0 || activityData.getCanDelete() == 1 || activityData.getCompleted().equals("False")) {
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.GONE); // to align edit icon as vertically center we used Visibility GONE
                } else {
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.VISIBLE);
                }
            } else if (activityData.getType().equals("Meeting")) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_meetings);

                if (activityData.getCanEditNotes() == 0) { // user account level access
                    activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);
                } else {
                    if (activityData.getCanEdit() == 0) { // additional rep security configuration
                        if (activityData.getCompleted().equals("False")) {
                            activitiesListViewHolder.mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                        } else {
                            activitiesListViewHolder.mark_unmark_icon.setImageResource(R.drawable.mark_uncompleted);
                        }

                        activitiesListViewHolder.mark_unmark_icon.setVisibility(View.VISIBLE);
                    } else {
                        activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);
                    }
                }

                if (activityData.getCanDeleteNotes() == 0 || activityData.getCanDelete() == 1 || activityData.getCompleted().equals("False")) {
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.GONE); // to align edit icon as vertically center we used Visibility GONE
                } else {
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.VISIBLE);
                }
            } else if (activityData.getType().equals("Email")) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_email);

                activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);

                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.VISIBLE);
                    else
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                }
            } else if (activityData.getType().equals("Mass Email")) {
                activitiesListViewHolder.activity_type_icon.setImageResource(R.drawable.activity_list_mass_email);

                activitiesListViewHolder.mark_unmark_icon.setVisibility(View.INVISIBLE);

                if (activityData.getCanDeleteNotes() == 0) { // user account level access
                    activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                } else {
                    if (activityData.getCanDelete() == 0) // additional rep security configuration
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.VISIBLE);
                    else
                        activitiesListViewHolder.delete_activity_icon.setVisibility(View.INVISIBLE);
                }
            }

            activitiesListViewHolder.company_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.openContactDetailPage(activityData.getCustomerId(), activityData.getParentId());
                }
            });

            activitiesListViewHolder.mark_unmark_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        if (activityData.getCompleted().equals("False"))
                            listener.markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_COMPLETE);
                        else
                            listener.markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE);
                    }
                }
            });

            activitiesListViewHolder.delete_activity_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.deleteActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_DELETE);
                    }
                }
            });
            activitiesListViewHolder.ll_openevent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.openEventDetailsPage(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_OPEN);
                    }

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }

    public int getResourceIdForCustomerType(String customerType) {
        int resourceId;

        switch (customerType) {
            case "Ad Agency":
                resourceId = R.drawable.icon_agency;
                break;
            case "Agency Contact":
                resourceId = R.drawable.icon_agency_contact;
                break;
            case "Current-Future Contracts":
                resourceId = R.drawable.icon_company_future_orders;
                break;
            case "Past-Expired Contracts":
                resourceId = R.drawable.icon_company_expired_orders;
                break;
            case "Company":
                resourceId = R.drawable.icon_company_no_orders;
                break;
            case "Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            case "Child Contact":
                resourceId = R.drawable.icon_sub_contact;
                break;
            default:
                resourceId = R.drawable.icon_sub_contact;
                break;
        }

        return resourceId;
    }

    public void addViewMoreToTextView(final int position, final TextView textView, final String titleText, String notes, final boolean showViewMore, final String expandText, final int maxLine, final ViewMoreClickListener listener) {
        try {
            final String text = titleText + notes;

            textView.post(new Runnable() {
                @Override
                public void run() {
                    SpannableStringBuilder truncatedSpannableString;
                    int startIndex;

                    if (textView.getLineCount() > maxLine) {
                        int lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                        String displayText = text.substring(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                        startIndex = displayText.indexOf(expandText);

                        truncatedSpannableString = new SpannableStringBuilder(displayText);

                        textView.setText(truncatedSpannableString);

                        // We are rechecking in order to resolve issue with alignment problem i.e. text is aligning in more than max no. of lines.
                        if (textView.getLineCount() > maxLine) {
                            lastCharShown = textView.getLayout().getLineVisibleEnd(maxLine - 1);
                            displayText = text.subSequence(0, lastCharShown - expandText.length() + 1) + " " + expandText;
                            truncatedSpannableString = new SpannableStringBuilder(displayText);
                            startIndex = displayText.indexOf(expandText);
                        }

                        truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });

                    } else if (showViewMore) {
                        String displayText = text + " " + expandText;
                        startIndex = displayText.indexOf(expandText);
                        truncatedSpannableString = new SpannableStringBuilder(displayText);
                        textView.setText(truncatedSpannableString);

                        truncatedSpannableString.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.activity_list_value_color)), titleText.length(), startIndex - 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        truncatedSpannableString.setSpan(new ClickableSpan() {
                            @Override
                            public void onClick(View widget) {
                                // this click event is not firing that's why we are adding click event for text view below.
                            }
                        }, startIndex, startIndex + expandText.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        textView.setText(truncatedSpannableString);

                        textView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (listener != null)
                                    listener.viewMoreClicked(position);
                            }
                        });
                    } else {
                        textView.setOnClickListener(null);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
