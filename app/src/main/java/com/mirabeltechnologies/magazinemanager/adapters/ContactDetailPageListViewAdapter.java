package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactDetailsListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by venkat on 10/12/17.
 */
public class ContactDetailPageListViewAdapter extends BaseAdapter {
    private Context context;
    private ContactDetailsListener listener;
    private Constants.RequestFrom requestFrom;
    private LayoutInflater layoutInflater;
    private List<String> rowTitles;
    //private List<Integer> rowIcons;
    private String siteType, noOfContacts = "0", noOfOrders = "0",noOfOpportunities="0";
    private boolean isPrimaryContact, isAdAgency;

    public ContactDetailPageListViewAdapter(Context ctx, ContactDetailsListener contactDetailsListener, Constants.RequestFrom requestCameFrom, boolean isPrimaryContact, String siteType) {
        this.context = ctx;
        this.listener = contactDetailsListener;
        this.requestFrom = requestCameFrom;
        this.isPrimaryContact = isPrimaryContact;
        this.siteType = siteType;
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (isPrimaryContact) {
            rowTitles = new ArrayList<>(Arrays.asList("General Info", "Contacts", "Orders", "Notes", "Calls", "Meetings","Emails", "Tasks","Opportunities", "Addresses", "Social Network","Check-in"));
        } else {
            rowTitles = new ArrayList<>(Arrays.asList("General Info", "Contacts", "Orders", "Notes", "Calls", "Meetings", "Emails", "Tasks", "Addresses", "Social Network","Check-in"));
        }
        //rowIcons = new ArrayList<>(Arrays.asList(R.drawable.icon_info, R.drawable.icon_contacts, R.drawable.icon_orders, R.drawable.icon_notes, R.drawable.icon_calls, R.drawable.icon_meetings, R.drawable.icon_emails, R.drawable.icon_tasklist, R.drawable.icon_address, R.drawable.icon_social_media));
    }

    public void setNoOfContacts(String noOfContacts) {
        this.noOfContacts = noOfContacts;
    }

    public void setNoOfOrders(String noOfOrders) {
        this.noOfOrders = noOfOrders;
    }

    public void setOpportunities(String noOfOpportunities) {
        this.noOfOpportunities=noOfOpportunities;
    }


    public void setAdAgency(boolean adAgency) {
        isAdAgency = adAgency;
    }

    public void reloadList() {
        if (isPrimaryContact) {
            if (isAdAgency || siteType.equals(Constants.SITE_TYPE_CRM)) {
                // We are not showing Orders for Agency contact and CRM site.
                rowTitles.remove("Orders");
                //rowIcons.remove(2);
            }
        } else {
            // We are not showing Contacts if contact is not a primary contact
            rowTitles.remove("Contacts");
            //rowIcons.remove(1);

            // We are not showing Orders if contact is not a primary contact
            rowTitles.remove("Orders");
            //rowIcons.remove(1);
        }

        notifyDataSetChanged();
    }


    static class ContactDetailPageViewHolder {
        RelativeLayout list_view_row;
        ImageView row_image_icon;
        MMTextView row_title;
    }

    @Override
    public int getCount() {
        return rowTitles.size();
    }

    @Override
    public String getItem(int position) {
        return rowTitles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            ContactDetailPageViewHolder contactDetailPageViewHolder;

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.contact_detail_page_list_view_row_item, null);

                contactDetailPageViewHolder = new ContactDetailPageViewHolder();
                contactDetailPageViewHolder.list_view_row = (RelativeLayout) convertView.findViewById(R.id.contact_detail_list_view_row);
                contactDetailPageViewHolder.row_image_icon = (ImageView) convertView.findViewById(R.id.list_view_row_image_icon);
                contactDetailPageViewHolder.row_title = (MMTextView) convertView.findViewById(R.id.list_view_row_title);

                convertView.setTag(contactDetailPageViewHolder);

            } else {
                contactDetailPageViewHolder = (ContactDetailPageViewHolder) convertView.getTag();
            }

            final String rowTitle = getItem(position);

            if (rowTitle.equals("Contacts")) {
                contactDetailPageViewHolder.row_title.setText(String.format("Contacts (%s)", noOfContacts));
                contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_contacts);
            } else if (rowTitle.equals("Orders")) {
                contactDetailPageViewHolder.row_title.setText(String.format("Orders (%s)", noOfOrders));
                contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_orders);
            }
            else if (rowTitle.equals("Opportunities")) {
                contactDetailPageViewHolder.row_title.setText(String.format("Opportunities (%s)", noOfOpportunities));
                contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_opportunity);
            }
            else {
                contactDetailPageViewHolder.row_title.setText(rowTitle);

                switch (rowTitle) {
                    case "General Info":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_info);
                        break;
                    case "Notes":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_notes);
                        break;
                    case "Calls":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_calls);
                        break;
                    case "Meetings":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_meetings);
                        break;
                    case "Emails":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_emails);
                        break;
                    case "Tasks":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_tasklist);
                        break;
                    case "Addresses":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_address);
                        break;
                    case "Social Network":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.icon_social_media);
                        break;
                    case "Check-in":
                        contactDetailPageViewHolder.row_image_icon.setImageResource(R.drawable.mark_completed);
                        break;
                }
            }

            contactDetailPageViewHolder.list_view_row.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null)
                        listener.showSelectedDetails(rowTitle);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
