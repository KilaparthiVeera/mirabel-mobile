package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.Customer;

import java.util.List;

public class SimilarContactsAdopter extends RecyclerView.Adapter<SimilarContactsAdopter.SimilarContactsHolder> {

    private Context context;
    private List<Customer> list;
    private String loggedInUserName;

    public SimilarContactsAdopter(Context context, List<Customer> list, String loggedInUserName) {
        this.context = context;
        this.list = list;
        this.loggedInUserName = loggedInUserName;
    }

    @NonNull
    @Override
    public SimilarContactsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem= layoutInflater.inflate(R.layout.similar_contacts_layout, viewGroup, false);
        return new SimilarContactsHolder(listItem);

    }

    @Override
    public void onBindViewHolder(@NonNull SimilarContactsHolder holder, int position) {
        holder.txt_1.setText(list.get(position).getName());
        holder.txt_2.setText(list.get(position).getContactName());
        holder.txt_3.setText(list.get(position).getEmail());
        holder.txt_4.setText(list.get(position).getPhone());
        holder.txt_5.setText(list.get(position).getRepName());
        holder.txt_6.setText(list.get(position).getJobDescription());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class SimilarContactsHolder extends RecyclerView.ViewHolder {
        TextView txt_1,txt_2,txt_3,txt_4,txt_5,txt_6;
        SimilarContactsHolder(View itemView) {
            super(itemView);
            txt_1 = itemView.findViewById(R.id.txt_1);
            txt_2 = itemView.findViewById(R.id.txt_2);
            txt_3 = itemView.findViewById(R.id.txt_3);
            txt_4 = itemView.findViewById(R.id.txt_4);
            txt_5 = itemView.findViewById(R.id.txt_5);
            txt_6 = itemView.findViewById(R.id.txt_6);

        }
    }

}
