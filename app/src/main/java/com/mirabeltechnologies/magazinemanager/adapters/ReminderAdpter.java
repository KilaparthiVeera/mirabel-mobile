package com.mirabeltechnologies.magazinemanager.adapters;

import android.content.ContentUris;
import android.content.Context;
import android.net.Uri;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.GoogleCalendar;

import java.util.List;

public class ReminderAdpter extends RecyclerView.Adapter<ReminderAdpter.ReminderHolder> {

    private Context context;
    private List<GoogleCalendar> gCalendar;

    public ReminderAdpter(Context context, List<GoogleCalendar> gCalendar) {
        this.context = context;
        this.gCalendar = gCalendar;
    }

    @NonNull
    @Override
    public ReminderHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View listItem= layoutInflater.inflate(R.layout.remindes_list, viewGroup, false);
        return new ReminderAdpter.ReminderHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull ReminderHolder holder, int position) {
        holder.tv_title.setText(gCalendar.get(position).getTitle());
        holder.tv_start.setText(String.format("%s TO %s", gCalendar.get(position).getDtstart(), gCalendar.get(position).getDtend()));
        holder.tv_end.setText(gCalendar.get(position).getDescerption());
        holder.img_close.setOnClickListener(v -> {

            Uri deleteUri;
            deleteUri = ContentUris.withAppendedId(CalendarContract.Events.CONTENT_URI, gCalendar.get(position).getEvent_id());
           context.getContentResolver().delete(deleteUri, null, null);

            Toast.makeText(context, "Event deleted", Toast.LENGTH_LONG).show();
            gCalendar.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, gCalendar.size());
        });

    }

    @Override
    public int getItemCount() {
        return gCalendar.size();
    }

    public static class ReminderHolder extends RecyclerView.ViewHolder {
        TextView tv_title,tv_start,tv_end;
        ImageView img_close;

        ReminderHolder(View itemView) {
            super(itemView);
            img_close = itemView.findViewById(R.id.img_close);
            tv_title = itemView.findViewById(R.id.tv_title);
            tv_start = itemView.findViewById(R.id.tv_start);
            tv_end = itemView.findViewById(R.id.tv_end);

        }
    }
}
