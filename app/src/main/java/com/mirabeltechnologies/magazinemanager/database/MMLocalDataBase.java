package com.mirabeltechnologies.magazinemanager.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;


public class MMLocalDataBase extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MM_Notifications_DB.db";


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_NOTIFICATION_IDS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_TABLE_NOTIFICATION_IDS);
        onCreate(db);
    }


    public MMLocalDataBase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }


    private static final String TABLE_NOTIFICATION_IDS = "tbl_notifications_ids";
    private static final String ID = "id";
    private static final String NOTIFICATION_ID = "activity_id";
    //notification_type =0 (calls) and notification_type =1 (meetings)
    private static final String NOTIFICATION_TYPE = "notification_type";
    private static final String ORIGINAL_SCHEDULE_TIME_MILLIS = "original_schedule_time_millis";
    private static final String MODIFIED_SCHEDULED_TIME_MILLIS = "modified_scheduled_time_millis";
    //if notification is opened, canceled, call made then IS_CANCELLED= 1 .Otherwise 0.
    private static final String IS_CANCELLED = "is_cancelled";


    private static final String SQL_CREATE_TABLE_NOTIFICATION_IDS =
            "CREATE TABLE " + TABLE_NOTIFICATION_IDS + " (" +
                    ID + " INTEGER PRIMARY KEY," +
                    NOTIFICATION_TYPE + " INTEGER," +
                    ORIGINAL_SCHEDULE_TIME_MILLIS + " INTEGER," +
                    MODIFIED_SCHEDULED_TIME_MILLIS + " INTEGER," +
                    IS_CANCELLED + " INTEGER," +
                    NOTIFICATION_ID + " INTEGER)";


    private static final String SQL_DELETE_TABLE_NOTIFICATION_IDS =
            "DROP TABLE IF EXISTS " + TABLE_NOTIFICATION_IDS;
    private static final String SQL_TRUNCATE_TABLE_NOTIFICATION_IDS =
            "DELETE FROM " + TABLE_NOTIFICATION_IDS;

    //for firsttime notification creation
    public long AddNewNotificationRecordDataIntoTableNotificationIds(int notificationId, int notificationType, String originalScheduleTimeMillis, String modifiedScheduleTimeMillis, int isCancelled) {
        //notification_type =0 (calls) and notification_type =1 (meetings)
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_ID, notificationId);
        values.put(NOTIFICATION_TYPE, notificationType);
        values.put(ORIGINAL_SCHEDULE_TIME_MILLIS, originalScheduleTimeMillis);
        values.put(MODIFIED_SCHEDULED_TIME_MILLIS, modifiedScheduleTimeMillis);
        values.put(IS_CANCELLED, isCancelled);
        Long insertId = sqLiteDatabase.insert(TABLE_NOTIFICATION_IDS, null, values);
        sqLiteDatabase.close();
        return insertId;
    }


    public void updateModifiedTimeOnSnoozeClicked(int notifiationId, String originalScheduledTime, String millis) {
        // update if records exists  and insert if record does not exist
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(MODIFIED_SCHEDULED_TIME_MILLIS, millis);
        values.put(ORIGINAL_SCHEDULE_TIME_MILLIS, originalScheduledTime);
        int insertId = sqLiteDatabase.update(TABLE_NOTIFICATION_IDS, values, NOTIFICATION_ID + " = " + notifiationId, null);
        sqLiteDatabase.close();


    }


    //use this method when user clicks on Notification, call, snooze, cancel
    public void updateCancelledStatusOfNotification(int notifiationId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(IS_CANCELLED, 1);
        int insertId = sqLiteDatabase.update(TABLE_NOTIFICATION_IDS, values, NOTIFICATION_ID + " = " + notifiationId, null);
        sqLiteDatabase.close();


    }


    public boolean isNotificationEligibleForReschedule(int notificationId, String newOriginalTime) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("SELECT   *   FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + NOTIFICATION_ID + "=?", new String[]{String.valueOf(notificationId)});
        if (result != null) {
            while (result.moveToNext()) {
                if ((!result.getString(result.getColumnIndex(ORIGINAL_SCHEDULE_TIME_MILLIS)).equals(newOriginalTime)) && result.getInt(result.getColumnIndex(IS_CANCELLED)) == 0) {
                    return true;
                }
            }
        }
        sqLiteDatabase.close();
        return false;

    }

  /*  public boolean getIsCancelStatusOfNotification(int notifiation_id) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("SELECT " + IS_CANCELLED + " FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + NOTIFICATION_ID + "=?", new String[]{String.valueOf(notifiation_id)});
        if (result != null) {
            while (result.moveToNext()) {
                if (result.getInt(result.getColumnIndex(IS_CANCELLED)) == 0) {
                    return false;
                }
            }
        }
        sqLiteDatabase.close();
        return true;


    }
*/

    public boolean checkRecordExistsOrNotInTable(int notificationId) {
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String sql = "SELECT EXISTS (SELECT * FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + NOTIFICATION_ID + " ='" + notificationId + "' LIMIT 1)";
        Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
        cursor.moveToFirst();
        // cursor.getInt(0) is 1 if column with value exists
        if (cursor.getInt(0) == 1) {
            cursor.close();
            sqLiteDatabase.close();
            return true;
        } else {
            cursor.close();
            sqLiteDatabase.close();
            return false;
        }
    }

    //remove all notifications ORIGINAL_SCHEDULE_TIME_MILLIS and MODIFIED_SCHEDULED_TIME_MILLIS are lesser than curent time.
    public int removeAllScheduledNotificationWhichAreExpired(String millis) {
        int delete_id = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        delete_id = sqLiteDatabase.delete(TABLE_NOTIFICATION_IDS, ORIGINAL_SCHEDULE_TIME_MILLIS + " < ? AND " + MODIFIED_SCHEDULED_TIME_MILLIS + " < ? ", new String[]{millis, millis});
        sqLiteDatabase.close();
        return delete_id;
    }


    //remove all notifications ORIGINAL_SCHEDULE_TIME_MILLIS and MODIFIED_SCHEDULED_TIME_MILLIS are lesser than curent time.
    public void removeScheduledNotificationOnDeviceReebot() {
        int delete_id = 0;
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        delete_id = sqLiteDatabase.delete(TABLE_NOTIFICATION_IDS, IS_CANCELLED + " = ? ", new String[]{"0"});
        sqLiteDatabase.close();

    }

    public ArrayList<Integer> getAllScheduledCallsNotification() {
        ArrayList<Integer> callPendingIntentIds = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + NOTIFICATION_TYPE + "=?", new String[]{"0"});
        if (result != null) {
            while (result.moveToNext()) {
                // move the cursor to next row if there is any to read it's data
                callPendingIntentIds.add(result.getInt(result.getColumnIndex(NOTIFICATION_ID)));
            }
        }
        sqLiteDatabase.close();
        return callPendingIntentIds;
    }

    public ArrayList<Integer> getAllScheduledMeetingsNotification() {
        ArrayList<Integer> meetingPendingIntentIds = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("SELECT * FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + NOTIFICATION_TYPE + "=?", new String[]{"1"});
        if (result != null) {
            while (result.moveToNext()) {
                // move the cursor to next row if there is any to read it's data
                meetingPendingIntentIds.add(result.getInt(result.getColumnIndex(NOTIFICATION_ID)));
            }
        }
        sqLiteDatabase.close();
        return meetingPendingIntentIds;
    }


    public ArrayList<Integer> getAllScheduledNotifications() {
        ArrayList<Integer> pendingIntentIds = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("select * from " + TABLE_NOTIFICATION_IDS, null);
        if (result != null) {
            while (result.moveToNext()) {
                // move the cursor to next row if there is any to read it's data
                pendingIntentIds.add(result.getInt(result.getColumnIndex(NOTIFICATION_ID)));
            }
        }
        sqLiteDatabase.close();
        return pendingIntentIds;
    }


    public void dropTableNotificationIds() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(SQL_DELETE_TABLE_NOTIFICATION_IDS);
        sqLiteDatabase.close();
    }


    public void truncateTableNotificationIds() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL(SQL_TRUNCATE_TABLE_NOTIFICATION_IDS);
        sqLiteDatabase.close();
    }


    /*public ArrayList<Integer> fetchAllScheduledNotificationIdsForRemovingIfRemovedFromServer() {
        //only scheduled ,that means we dont wantn to check rescheduled  because it is rescheduled  intentionally by the user
        ArrayList<Integer> scheduled_notification_ids = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        Cursor result = sqLiteDatabase.rawQuery("SELECT " + NOTIFICATION_ID + " FROM " + TABLE_NOTIFICATION_IDS + " WHERE " + IS_RESCHEDULED + "= ? ", new String[]{"0"});

        if (result != null) {
            while (result.moveToNext()) {
                // move the cursor to next row if there is any to read it's data
                scheduled_notification_ids.add(result.getInt(result.getColumnIndex(NOTIFICATION_ID)));
            }
        }
        sqLiteDatabase.close();
        return scheduled_notification_ids;
    }*/

}
