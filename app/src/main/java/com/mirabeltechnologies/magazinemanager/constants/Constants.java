package com.mirabeltechnologies.magazinemanager.constants;

import android.Manifest;
import android.os.Build;

import androidx.annotation.RequiresApi;

/**
 * Created by venkat on 04/10/17.
 *
 * @author venkat
 */
public class Constants {

    public static final String LOADING_MESSAGE = "Please wait..";

    public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static final String CHARSET_NAME = "UTF-8";
    public static final String AUTHORISATION = "Authorisation";

    public static final int DEVICE_MIN_WIDTH = 1100; // i.e. pixels, based on this, we will consider device as small or large
    public static final int VOLLEY_SOCKET_TIMEOUT = 120000; // in milli secs i.e. 120 seconds.
    public static final int CONNECTION_TIMEOUT = 30000;

    // SHARED PREFERENCE CONSTANTS
    public static final String SHARED_PREFERENCE_KEY = "MagazineManagerPrefs";
    public static String MM_SETTINGS_SHARED_PREFERENCES = "MMSettingsPreferences";
    public static final String SP_ENCRYPTED_CLIENT_KEY = "EncryptedClient";
    public static final String SP_SELECTED_SUB_DOMAIN = "SubDomain";
    public static final String SP_SELECTED_CLIENT_ID = "ClientId";
    public static final String SP_SELECTED_CLIENT_NAME = "ClientName";
    public static final String SP_SELECTED_CLIENT_URL = "ClientURL";
    public static final String SP_SELECTED_SITE_TYPE = "SiteType";
    public static final String SP_IS_DS_USER = "IsDsUser";
    public static final String SP_LOGGED_IN_REP_ID = "LoggedInRepId";
    public static final String SP_LOGGED_IN_REP_NAME = "LoggedInRepName";
    public static final String SP_LOGGED_IN_USER_NAME = "LoggedInUserName"; // This is logged in rep / user formatted name.
    public static final String SP_EMAIL_ID = "EmailId";
    public static final String SP_PASSWORD = "Password";

    public static final String SP_LAST_REFRESHED_TIME_IN_DASHBOARD = "LastRefreshedTimeInDashboard";
    public static final String SP_MY_CONTACTS_SELECTED_CONTACT_INDEX = "MyContactsSelectedContactIndex";
    public static final String SP_MY_LAST_100_CONTACTS_SELECTED_CONTACT_INDEX = "MyLast100ContactsSelectedContactIndex";
    public static final String SP_REP_CONTACTS_SELECTED_CONTACT_INDEX = "RepContactsSelectedContactIndex";
    public static final String SP_IS_WEBSITE_CONFIG_LOADED = "IsWebsiteConfigLoaded";
    public static final String SP_LIMIT_CUSTOMER_SEARCH_BY_REP = "LimitCustomerSearchByRep";
    public static final String SP_LIMIT_CUSTOMER_ADD_BY_REP = "LimitCustomerAddByRep";

    // Bundle Constants
    public static final String BUNDLE_REQUEST_FROM = "RequestFrom";
    public static final String BUNDLE_RSD_SELECTION_TYPE = "RightSideDrawerSelectionType";
    public static final String BUNDLE_RSD_SELECTED_INDEX = "RightSideDrawerSelectedIndex";
    public static final String BUNDLE_RSD_SELECTED_VALUE = "RightSideDrawerSelectedValue";
    public static final String BUNDLE_QUICK_SEARCH_CONTACT_TYPE = "QuickSearchContactType";
    public static final String BUNDLE_SELECTED_CONTACT = "SelectedContact";
    public static final String BUNDLE_SELECTED_CONTACT_DETAILS = "SelectedContactDetails";
    public static final String BUNDLE_SELECTED_CUSTOMER_ID = "SelectedCustomerId";
    public static final String BUNDLE_ACTIVITY_TYPE = "ActivityType";
    public static final String BUNDLE_OPPORTUNITY_DETAILS = "OpportunityDetails";

    public static final String BUNDLE_SELECTED_REP_ID = "SelectedRepId";
    public static final String BUNDLE_ALL_REPS_DATA = "AllRepsData";
    public static final String BUNDLE_LOADING_URL = "LoadingURL";
    public static final String BUNDLE_ALL_MASTERS_DATA = "AllMastersData";
    public static final String BUNDLE_CURRENT_OPERATION = "CurrentOperation";
    public static final String BUNDLE_IS_PRIMARY_CONTACT = "isPrimaryContact";
    public static final String BUNDLE_IS_COMPANY_ALREADY_HAVE_BILLING_CONTACT = "isCompanyAlreadyHaveBillingContact";
    public static final String BUNDLE_CUSTOMER_ID = "CustomerId";
    public static final String BUNDLE_PARENT_ID = "ParentId";
    public static final String BUNDLE_CONTACT_REP_ID = "ContactRepId";
    public static final String BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT = "SearchActivitiesFilterObject";
    public static final String BUNDLE_IS_NOTES_SETTING_PRIVATE = "isNotesSettingPrivate";
    public static final String BUNDLE_SEARCH_TASKS_FILTER_OBJECT = "SearchTasksFilterObject";
    public static final String BUNDLE_SELECTED_ORDER = "SelectedOrder";
    public static final String BUNDLE_SEARCH_ORDERS_FILTER_OBJECT = "SearchOrdersFilterObject";
    public static final String BUNDLE_SELECTED_CALENDAR_DATE = "SelectedCalendarDate";
    public static final String BUNDLE_IS_CALENDAR_EVENT_EDITING = "IsCalendarEventEditing";
    public static final String BUNDLE_SELECTED_CALENDAR_EVENT_ID = "SelectedCalendarEventId";
    public static final String BUNDLE_SELECTED_CALENDAR_EVENT_DETAILS = "SelectedCalendarEventDetails";

    // Local Broadcast Manager Intent Action Constants
    public static final String LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION = "Right_Side_Drawer_Selection";
    public static final String LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE = "Right_Side_Drawer_Selection_From_My_Contacts";
    public static final String LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE = "Right_Side_Drawer_Selection_From_Create_Contact_Page";
    public static final String LBM_ACTION_REFRESH_VIEW = "RefreshView";
    public static final String LBM_ACTION_RSDS_FROM_CREATE_ACTIVITY_PAGE = "RSDS_From_Create_Activity_Page";
    public static final String LBM_ACTION_REFRESH_ACTIVITIES = "RefreshActivities";
    public static final String LBM_ACTION_RSDS_FROM_SEARCH_ACTIVITIES_FILTERS_PAGE = "RSDS_From_Search_Activities_Filters_Page";
    public static final String LBM_ACTION_RSDS_FROM_SEARCH_TASKS_FILTERS_PAGE = "RSDS_From_Search_Tasks_Filters_Page";
    public static final String LBM_ACTION_RSDS_FROM_CREATE_NEW_TASK_PAGE = "RSDS_From_Create_New_Task_Page";
    public static final String LBM_ACTION_REFRESH_TASKS = "RefreshTasks";
    public static final String LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE = "RSDS_From_Search_Orders_Filters_Page";
    public static final String LBM_ACTION_RSDS_FROM_CREATE_CALENDAR_EVENT_PAGE = "RSDS_From_Create_Calendar_Event_Page";
    public static final String LBM_ACTION_REFRESH_CALENDAR_EVENTS = "RefreshCalendarEvents";

    // Permissions
    public static String[] PERMISSIONS_RECORD_AUDIO = {Manifest.permission.RECORD_AUDIO};
    public static String PERMISSION_RECORD_AUDIO = Manifest.permission.RECORD_AUDIO;

    public static String[] PERMISSIONS_READ_CONTACTS = {Manifest.permission.READ_CONTACTS};
    public static String PERMISSION_READ_CONTACTS = Manifest.permission.READ_CONTACTS;

    public static String[] PERMISSIONS_CALL_PHONE = {Manifest.permission.CALL_PHONE};
    public static String PERMISSION_CALL_PHONE = Manifest.permission.CALL_PHONE;

    public static String[] PERMISSIONS_READ_EXTERNAL_STORAGE = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
    public static String PERMISSION_READ_EXTERNAL_STORAGE = Manifest.permission.READ_EXTERNAL_STORAGE;

    public static String[] PERMISSIONS_READ_IMAGE = {Manifest.permission.READ_MEDIA_IMAGES};
    public static String PERMISSION_READ_IMAGE = Manifest.permission.READ_MEDIA_IMAGES;

    public static String[] storge_permissions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };

    @RequiresApi(api = Build.VERSION_CODES.TIRAMISU)
    public static String[] storge_permissions_33 = {
            Manifest.permission.READ_MEDIA_IMAGES,
            Manifest.permission.READ_MEDIA_AUDIO,
    };

    public static String[] permissions() {
        String[] p;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            p = storge_permissions_33;
        } else {
            p = storge_permissions;
        }
        return p;
    }


    // Activity Request Codes
    public static final int RECORD_AUDIO_REQUEST_CODE = 100;
    public static final int READ_CONTACTS_REQUEST_CODE = 101;
    public static final int CHOOSE_FILTERS_REQUEST_CODE = 102;
    public static final int MAKE_PHONE_CALL_REQUEST_CODE = 103;
    public static final int READ_EXTERNAL_STORAGE_REQUEST_CODE = 104;
    public static final int CHOOSE_DOCUMENT_TO_UPLOAD_REQUEST_CODE = 105;
    public static final int REQUEST_BATTERY_OPTIMISATION_CODE = 106;
    public static final int OPEN_APP_NOTIFICATIONS_SETTINGS = 107;
    public static final int OPEN_APP_NOTIFICATION_CHANNEL_SETTINGS = 108;
    public static final int OPEN_DEVICE_SETTINGS = 109;
    public static final int OPEN_APPLICATION_SETTINGS = 110;

    // Alert Dialog Request Codes
    public static final int ALERT_DIALOG_REQUEST_CODE_COMMON = 0;
    public static final int ALERT_DIALOG_REQUEST_CODE_READ_CONTACTS = 100;
    public static final int ALERT_DIALOG_REQUEST_CODE_CONTACT_SAVED = 101;
    public static final int ALERT_DIALOG_REQUEST_CODE_UPDATE_SUB_CONTACTS = 102;
    public static final int ALERT_DIALOG_REQUEST_CODE_COMPANY_NAME_ALREADY_EXISTS = 103;
    public static final int ALERT_DIALOG_REQUEST_CODE_CONTACT_EDITED = 104;
    public static final int ALERT_DIALOG_REQUEST_CODE_NEW_ACTIVITY_ADDED = 105;
    public static final int ALERT_DIALOG_REQUEST_CODE_DELETE_ACTIVITY = 106;
    public static final int ALERT_DIALOG_REQUEST_CODE_MARK_COMPLETE_ACTIVITY = 107;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL = 108;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_MAKE_ALT_PHONE_CALL = 115;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION = 109;
    public static final int ALERT_DIALOG_REQUEST_CODE_DELETE_TASK = 110;
    public static final int ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED = 111;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_COMPLETE_TASK = 112;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_RECORD_AUDIO = 113;
    public static final int ALERT_DIALOG_REQUEST_CODE_TO_READ_EXTERNAL_STORAGE = 114;

    // Dashboard Notification Activity Types
    public static final String NOTIFICATION_ALL = "All";
    public static final String NOTIFICATION_CALLS = "Callbacks";
    public static final String NOTIFICATION_MEETINGS = "Meetings";
    public static final String NOTIFICATION_TASKS = "Tasks";

    // Contact Activity Types
    public static final String ACTIVITY_TYPE_NOTES = "Notes";
    public static final String ACTIVITY_TYPE_CALLS = "Calls";
    public static final String ACTIVITY_TYPE_MEETINGS = "Meetings";
    public static final String ACTIVITY_TYPE_OPPORTUNITY = "Opportunity";

    public static final String ACTIVITY_TYPE_TASKS = "Tasks";
    public static final String ACTIVITY_TYPE_EMAILS = "Emails";
    public static final String ACTIVITY_TYPE_ALL_ACTIVITIES = "All Activities";
    public static final String ACTIVITY_TYPE_THIS_WEEK_CALLBACKS = "ThisWeek";
    public static final String ACTIVITY_TYPE_TODAY_CALLBACKS = "Today";
    public static final String ACTIVITY_TYPE_ALL_CALLBACKS = "All";
    public static final String ACTIVITY_TYPE_CALLS_MEETINGS = "Calls_Meetings";

    // Activity Actions
    public static final String ACTIVITY_ACTION_DELETE = "delete";
    public static final String ACTIVITY_ACTION_MARK_COMPLETE = "accept";
    public static final String ACTIVITY_ACTION_MARK_UN_COMPLETE = "cross";
    public static final String ACTIVITY_ACTION_OPEN = "open";

    // Task Actions
    public static final String TASK_ACTION_DELETE = "delete";
    public static final String TASK_ACTION_MARK_COMPLETE = "markcompleted";
    public static final String TASK_ACTION_MARK_UN_COMPLETE = "markuncompleted";

    // Current Operation Types
    public static final String CURRENT_OPERATION_CREATE_PRIMARY_CONTACT = "CreatePrimaryContact";
    public static final String CURRENT_OPERATION_EDIT_CONTACT = "EditContact";
    public static final String CURRENT_OPERATION_CREATE_SUB_CONTACT = "CreateSubContact";

    // Contact Permissions for Rep
    public static final String CONTACT_NO_ACCESS = "0";
    public static final String CONTACT_READ_WRITE_ACCESS = "1";
    public static final String CONTACT_READ_ONLY_ACCESS = "2";

    // Order Types
    public static final String ORDER_TYPE_PRINT = "1";
    public static final String ORDER_TYPE_DIGITAL = "3";

    public static final String CONTACT_TYPE_BILLING = "Billing Contact";
    public static final String CONTACT_TYPE_BILLING_WITH_STAR = "Billing Contact*";

    public static final String VIEW_MORE_EXPAND_TEXT = "..more";

    public static final String SITE_TYPE_CRM = "CRM";

    // Get Master Data Different Fields
    public static final String GET_MASTER_DATA_DEPARTMENT = "Department";
    public static final String GET_MASTER_DATA_ACTIVITY_TYPES = "ActivityTypes";
    public static final String GET_MASTER_DATA_REQ_FIELDS = "reqfields";
    public static final String GET_MASTER_DATA_EMPLOYEE = "Employee";
    public static final String GET_MASTER_DATA_CATEGORIES = "Categories";
    public static final String GET_MASTER_DATA_PRIORITIES = "Priority";
    public static final String GET_MASTER_DATA_CONTACT_TYPES = "ContactTypes";
    public static final String GET_MASTER_DATA_PROJECTS = "ProjectTasks";
    public static final String GET_MASTER_DATA_BUSINESS_UNITS = "businessunit";
    public static final String GET_MASTER_DATA_PRODUCTS = "product";
    public static final String GET_ADMIN_DATA_STAGE = "stage";
    public static final String GET_ADMIN_DATA_TYPE = "type";
    public static final String GET_ADMIN_DATA_REASON = "reason";

    public static final String GET_MASTER_DATA_MAGAZINES = "magazine";
    public static final String GET_MASTER_DATA_RATE_CARDS = "ratecard";
    public static final String GET_MASTER_DATA_ISSUE_YEARS = "issueyear";
    public static final String GET_MASTER_DATA_ISSUES = "issues";
    public static final String GET_MASTER_DATA_TASK_PRIORITIES = "taskpriority";
    public static final String GET_MASTER_DATA_WEBSITE_CONFIG = "config";

    // Array List Types With different types of Custom Objects
    public static final String LIST_TYPE_MASTER_DATA = "MasterData";
    public static final String LIST_TYPE_REPS_DATA = "RepData";

    public enum OrientationType {
        PORTRAIT,
        LANDSCAPE
    }
    public enum ButtonType {
        POSITIVE,
        NEGATIVE
    }
    public enum RequestCode {
        DEFAULT
    }
    public enum SelectionType {
        CLIENT,
        REP,
        CATEGORY,
        PRIORITY,
        CONTACT_TYPE,
        ACTIVITY_TYPE,
        ASSIGNED_BY,
        ASSIGNED_TO,
        CREATED_BY_OR_ASSIGNED_BY,
        DEPARTMENT,
        FROM_DATE,
        TO_DATE,
        PROJECT,
        ADD_PROBABIlILITY,
        OPPORTUNITY_STATUS,
        OPPORTUNITY_STAGE,
        OPPORTUNITY_TYPE,
        OPPORTUNITY_REASON,
        OPPORTUNITY_CONTACT,
        OPPORTUNITY_ADD,
        OPPORTUNITY_EDIT,
        OPPORTUNITY_SEARCH,
        SALES_REP,
        BUSINESS_UNIT,
        PRODUCT,
        MAGAZINE,
        RATE_CARD,
        ISSUE_YEAR,
        ISSUE,
        DATE_RANGE,
        MEETING_TYPE,
        START_DATE,
        END_DATE,
        START_TIME,
        END_TIME,
        RECURRENCE_UNTIL_DATE,
        MAP_ADDRESS,
        BUSINESS_CARD,
        COMPANYNAME,
        NAME,
        JOB,
        ADDRESS1,
        ADDRESS2,
        CITY,
        COUNTY,
        STATE,
        COUNTRY,
    }

    public enum RequestFrom {
        LOGIN_PAGE,
        CHANGE_CLIENT,
        DASHBOARD,
        DASHBOARD_QUICK_SEARCH,
        DASHBOARD_RECENT_CONTACTS,
        CONTACT_DETAIL_PAGE,
        CONTACT_DETAIL_GENERAL_INFO_PAGE,
        CONTACT_SUB_CONTACTS_PAGE,
        MY_CONTACTS_PAGE,
        OPPORTUNITY,
        ADD_OPPORTUNITY,
        SEARCH_OPPORTUNITY,
        MY_LAST_100_CONTACTS_PAGE,
        REP_CONTACTS_PAGE,
        CREATE_CONTACT_PAGE,
        CREATE_ACTIVITY_PAGE,
        SEARCH_ACTIVITIES_PAGE,
        SEARCH_ACTIVITIES_FILTERS_PAGE,
        SEARCH_TASKS_PAGE,
        SEARCH_TASKS_FILTERS_PAGE,
        CREATE_NEW_TASK_PAGE,
        CONTACT_ORDERS_PAGE,
        ORDERS_PAGE,
        SEARCH_ORDERS_FILTERS_PAGE,
        CALENDAR,
        ADD_CALENDAR_EVENT,
        CALLBACKS_PAGE,
        NOTIFICATION_SERVICE,
        EVENTS_JOB_SCHEDULE_SERVICE,
        DASHBOARD_NOTIFICATIONS,
        BUILD_NOTIFICATION,
        EVENT_DETAILS_PAGE,
        EVENT_ACTIVITIES,
        MARK_DELETE_FOREGROUND_SERVICE,
        NEARBY_CONTACTS,
        SAVE_LIST,
    }


    //notifications
    public static final String CALLS_AND_MEETINGS_CHANNEL_ID = "channel-CMA";
    public static final String CALLS_AND_MEETINGS_CHANNEL_NAME = "Calls and Meetings";
    //firebase job dispatcher
    public static final String FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG = "com.MM.fetch_Data_And_Schedule_Notifications_Job";
    //pending intent actions
    public static final String ACTION_SCHEDULE_ALARM_PENDING_INTENT = "COM.MM.SCHEDULE_NOTIFICATIONS";
    //power manager tag
    public static final String ACTION_NOTIFICATION_POWER_MANAGER = "ACTION_MM_NOTIFICATION_POWER_TAG";
    //for notification feature
    public static final String EVENT_DATA = "event_data";
    public static final String ACTION_CLOSE = "com.close";
    public static final String ACTION_NOTIFICATION_FORCE_CLOSE = "com.force_close";
    public static final String ACTION_CALL = "com.actioncall";
    public static final String ACTION_DETAILS = "com.open_notification";
    public static final String ACTION_SNOOZE = "com.snooze";
    public static final String ACTION_MARK = "com.mark";
    public static final String ACTION_UNMARK = "com.unmark";
    public static final String ACTION_EVENT_DELETE = "com.delete";
    public static final String NOTIFICATION_ID = "notification_id";
    public static final String NOTIFICATION_TYPE = "notification_type";
    public static final String CANCEL = "CANCEL";
    public static final String SNOOZE = "SNOOZE";
    public static final String MEETING = "Meeting";
    public static final String CALL = "Call";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String REPEATS_SWITCH = "allowrepeats_switch";
    public static final String MEETINGS_SWITCH = "allowmeeting_switch";
    public static final String CALLS_SWITCH = "allowcalls_switch";
    public static final String FINGERPRINT = "allowfingerprint_switch";
    public static final String ALLOW_CALENDER = "allow_calender";
    public static final String ALLOW_ALARM = "allow_alarm";
    public static final String ALLOW_LOCATION = "allow_location";


    public static final String SNOOZE_TIME_PICKER = "snooze_time_picker";
    public static final int SNOOZE_TIME_PICKER_MINIMUM_VALUE = 5;
    public static final int SNOOZE_TIME_PICKER_MAXIMUM_VALUE = 60;

    public static final String RANGE_PICKER = "range_picker";
    public static final int RANGE_PICKER_MINIMUM_VALUE = 2;
    public static final int RANGE_PICKER_MAXIMUM_VALUE = 15;

    public static final int CALL_ADVANCE_SCHEDULE_TIME = 5;
    public static final int MEETING_ADVANCE_SCHEDULE_TIME = 15;


    public static final String SERVER_DATE_FORMAT = "y-MM-dd";
    public static final String SELECTED_DATE_DISPLAY_FORMAT = "MM/dd/y";
    public static final String CALENDAR_DATE_DISPLAY_FORMAT = "E MMMM dd, y";
    public static final String TIME_FORMAT = "HH:mm";
    public static final String CALENDAR_SELECTED_DATE_WITH_TIME_FORMAT = "y-MM-dd HH:mm";
    public static final String CALENDAR_NEW_EVENT_DATE_TIME_FORMAT = "MM/dd/y HH:mm";
    public static final String CALENDAR_EDIT_EVENT_DATE_TIME_FORMAT = "MM/dd/y hh:mm a";

    public static final String APP_TYPE = "A";
    public static final String PRODUCT_NAME = "TMM";


    public static final float SAVE_PAGE_SIZE_FLOAT = 100.0f; // here float is taken bcoz to calculate total no. of pages while loading data in list view.
    public static final int SAVE_PAGE_SIZE_INT = 100;
    public static final float PAGE_SIZE_FLOAT = 50.0f; // here float is taken bcoz to calculate total no. of pages while loading data in list view.
    public static final int PAGE_SIZE_INT = 50;
    public static final float ACTIVITY_PAGE_SIZE_FLOAT = 30.0f;
    public static final int ACTIVITY_PAGE_SIZE_INT = 30;

    public static final float OPPORTUNITY_PAGE_SIZE_FLOAT = 25.0f;
    public static final int OPPORTUNITY_PAGE_SIZE_INT = 25;

    public static final float ORDERS_PAGE_SIZE_FLOAT = 15.0f;
    public static final int ORDERS_PAGE_SIZE_INT = 15;

    //google maps and location apis                                                        // AIzaSyDY57Y-vX-K7HgVDXz939-jr2sJG-GAVoE
    public static final String GOOGLE_API_KEY = "AIzaSyCt4iMNT2Nl9r6lV4eUMKR1rnR3IZAMFdk"; // AIzaSyDBo0SmnDQyEbVE4N6RXb1A8vrVB2puasc
    public static final String GOOGLE_MAPS_URL = "https://maps.googleapis.com/maps/api/geocode/json?key=%s&address=%s";

    /* All Application Service URL's Listed Below */

    //Tier 1
  /*  public static final String MAIN_URL = "https://tier1-dev.magazinemanager.com/CRMService/CRMService.svc";
    public static final String LOGIN_URL = "https://tier1-dev.magazinemanager.com/AuthServices/AuthenticationService.svc";
    public static final String New_MAIN_URL = "https://mirabeldev.magazinemanager.com/services";
*/
    //Tier 2
    //public static final String MAIN_URL = "https://tier2.magazinemanager.com/intranet/CRMService/CRMService.svc";
    //public static final String LOGIN_URL = "https://tier2.magazinemanager.com/intranet/AuthServices/AuthenticationService.svc";


    //for testing by Staging_biz
  /*  public static final String MAIN_URL = "https://accounts.mirabeltechnologies.de/CRMService/CRMService.svc";
    public static final String LOGIN_URL = "https://accounts.mirabeltechnologies.de/AuthServices/AuthenticationService.svc";
    public static final String New_MAIN_URL = "https://tsqb.magazinemanager.biz/services";
*/
    //Production
    public static final String MAIN_URL = "https://accounts.mirabeltechnologies.com/CRMService/CRMService.svc";
    public static final String LOGIN_URL = "https://accounts.mirabeltechnologies.com/AuthServices/AuthenticationService.svc";
    public static final String New_MAIN_URL = "https://mirabelapi.magazinemanager.com/services";
    //Test Environment
    //  public static final String MAIN_URL = "https://tier1-dev20.magazinemanager.com/CRMService/CRMService.svc";
    //  public static final String LOGIN_URL = "https://tier1-dev20.magazinemanager.com/AuthServices/AuthenticationService.svc";

    //Production testing
   /* public static final String MAIN_URL = "http://accounts-test.mirabeltechnologies.com/CRMService/CRMService.svc";
    public static final String LOGIN_URL ="http://accounts-test.mirabeltechnologies.com/AuthServices/AuthenticationService.svc";
*/


    // testing tier1-feature
  //  public static final String MAIN_URL = "https://tier1-feature.magazinemanager.com/CRMService/CRMService.svc";
  //  public static final String LOGIN_URL = "https://tier1-feature.magazinemanager.com/AuthServices/AuthenticationService.svc";
 //   public static final String GET_NEARBY_CONTACTS_URL = "https://tier1-feature.magazinemanager.com/services/crm/contacts/search/SearchResults";


    public static final String GET_LOGOUT_URL = New_MAIN_URL+"/User/Logins/LogOut";
    public static final String GET_NEARBY_CONTACTS_URL = New_MAIN_URL+"/crm/contacts/search/SearchResults";
    public static final String GET_SAVE_LIST_URL = New_MAIN_URL+"/Admin/Masters/MasterData/MyLists";
    public static final String GET_SAVE_CONTACTS_URL = New_MAIN_URL+"/crm/contacts/advancesearch/SearchResults";
    public static final String GET_OPPORTUNITY_URL = New_MAIN_URL+"/opportunities/report/all/";
    public static final String GET_SAVE_OPPORTUNITY_URL = New_MAIN_URL+"/Opportunities";
    public static final String CHECK_IN_URL = New_MAIN_URL+"/Users/CheckInTrack";
    public static final String CHECK_IN_HISTORY = New_MAIN_URL+"/Users/CheckIn";
    public static final String SIMILAR_CONTACTS_URL = New_MAIN_URL+"/Crm/Contacts/Similar/1/10";


    public static final String BUSINESS_BASE_URL="http://businesscard.emailnow.info/";

    // Magazine Manager LOCATION SETTINGS Codes
    public static final int LOCATION_SETTINGS_REQUEST_CODE=115;
    public static final int REQUEST_LOCATION_PERMISSIONS_REQUEST_CODE = 14;

    // Magazine Manager Log-In Response Codes
    public static final int MM_LOGIN_SUCCESS = 200; // Operation Successful. Please use the response object.
    public static final int MM_LOGIN_SUCCESS_BUT_NO_CLIENTS = 110; // Successfully authenticated but does n’t have access to any of the sites.
    public static final int MM_LOGIN_ERROR = 400; // Bad Request. Error occurred while processing the request.
    public static final int MM_INVALID_LOGIN = 401; // Invalid Login credentials.
    public static final int MM_INVALID_REQUEST = 117; // Invalid request due to buffer time (3 minutes) expiry
    // Custom Defined Log-In Response Codes
    public static final int MM_PASSWORD_EXPIRED = 402; // Password Expired.
    public static final int MM_PASSWORD_STRONG = 403; // Password Strong.
    public static final int MM_PENDING_VERIFICATION = 404; // Pending Verification.
    public static final int MM_RESPONSE_CODE_SUCCESS = 100;
    public static final int MM_RESPONSE_CODE_NO_RECORDS = 105;

    public static final String MM_ERROR_CODE = "ErrorCode";
    public static final String MM_ERROR_MESSAGE = "ErrorMessage";
    public static final String MM_IS_VALID_RESPONSE = "IsValid";
    public static final String MM_MESSAGE = "Message";

    public static final String STATUS_SUCCESS = "success";
    public static final String STATUS_FAILURE = "failure";
    public static final String STATUS_ERROR = "error";

    public static final String QUICK_SEARCH = "QuickSearch";
    public static final String GET_RECENT_OR_ALL_CONTACTS = "GetContactDetailsAllOrRecent";
    public static final String GET_DASHBOARD_NOTIFICATIONS = "GetRepActivities";
    public static final String GET_CUSTOMER_BASIC_DETAILS = "GetCustomerBasicDetails";
    public static final String GET_CUSTOMER_SUB_CONTACTS = "GetSubContacts";
    public static final String GET_CUSTOMER_ACTIVITIES = "GetCustomerActivities";
    public static final String GET_CUSTOMER_ADDRESSES = "GetCustomerAddress";
    public static final String SEARCH_CUSTOMER = "SearchCustomer";
    public static final String GET_MASTER_DATA_REP = "GetMasterDataRep";
    public static final String GET_REPS = "GetReps";
    public static final String GET_QUERY = "GetQuery";
    public static final String GET_MASTERS_DATA = "GetMasters";

    public static final String CREATE_NEW_CONTACT = "CreateCustomer";
    public static final String GET_ACTIVITY_NOTES_BY_ID = "GetActivityNotes";
    public static final String CREATE_NEW_ACTIVITY = "SaveNotification";
    public static final String SEARCH_ACTIVITY = "SearchActivity";
    public static final String ACTIVITY_ACTION = "ActivityAction";
    public static final String SEARCH_TASKS = "Task/GET";
    public static final String CREATE_NEW_TASK = "Task/CREATEUPDATE";
    public static final String TASK_ACTIONS = "Task/actions";
    public static final String GET_CUSTOMER_ORDERS = "GetCustomerOrders";
    public static final String GET_CASCADING_MASTER = "GetCMaster";
    public static final String GET_ALL_ORDERS = "GetAllOrdersBySearch";
    public static final String CALENDAR_GET_EVENTS = "Calendar/Get";
    public static final String CALENDAR_GET_EVENT_DETAILS = "Calendar/Event/Get";
    public static final String CALENDAR_CREATE_EVENT = "Calendar/Create";
    public static final String CALENDAR_DELETE_EVENT = "Calendar/Delete";
    public static final String ADMIN_STAGE="/Admin/Opportunities/Stage";
    public static final String ADMIN_SOURCE="/Admin/Opportunities/Opportunity/Source/";
    public static final String ADMIN_PRODUCT="/Admin/Products/ByCriteria/";

    public static final String ADMIN_TYPE="/Admin/Opportunities/Type";
    public static final String ADMIN_RESON="/Admin/Opportunities/lossreason";
    public static final String ADMIN_CITY="/Admin/Masters/MasterData/ContactCities";
    public static final String ADMIN_COUNTY="/Admin/Masters/MasterData/ContactCounties";
    public static final String ADMIN_STATE="/Admin/Masters/MasterData/ContactStates";
    public static final String ADMIN_COUNTRY="/Admin/Masters/MasterData/ContactCountries";

    public static final String OPPORTUNITY_STATUS="OpportunityStatus";
    public static final String CONTACT_OPPORTUNITY="/Opportunities/Contact/";

    public static final String OPPORTUNITY_CONTACT="/Crm/Contacts/Contacts/";
    public static final String ADD_PROBABIlILITY="AddProbability";


    // Jira REST API Details
    public static final String JIRA_AUTHORIZATION_HEADER = "Basic aXJpbGV5QG1pcmFiZWx0ZWNobm9sb2dpZXMuY29tOjY3RnhTUzRtN1k4NU1FVXNtMG9NRkM0Mg=="; // we have converted this string => iriley@mirabeltechnologies.com:67FxSS4m7Y85MEUsm0oMFC42
    public static final String JIRA_CREATE_NEW_TICKET_URL = "https://mirabel.atlassian.net/rest/api/2/issue";
    public static final String JIRA_ADD_ATTACHMENT_FOR_TICKET_URL = "https://mirabel.atlassian.net/rest/api/2/issue/%s/attachments";

    //google maps and location apis
    public static final String ADDRESS_API = "https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=";
    public static final String DIRECTIONS_API = "https://maps.googleapis.com/maps/api/directions/";
    public static String BaseUrl = "https://maps.googleapis.com/maps/api/place/";



}
