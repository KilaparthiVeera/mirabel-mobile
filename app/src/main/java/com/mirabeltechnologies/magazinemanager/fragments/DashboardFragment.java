package com.mirabeltechnologies.magazinemanager.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.DashboardNotificationsAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.QuickSearchAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.RecentContactsAdapter;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.Notification;
import com.mirabeltechnologies.magazinemanager.beans.QuickSearchContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.DelayAutoCompleteTextView;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.QuickSearchContactSelectionListener;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;

import java.util.ArrayList;
import java.util.List;

public class DashboardFragment extends Fragment {
    private MMSharedPreferences sharedPreferences;
    public String encryptedClientKey;
    private RelativeLayout currentLayout;
    private LayoutInflater layoutInflater;
    private DelayAutoCompleteTextView quick_search_auto_complete_text_view;
    public static ImageView voice_recorder;
    private MMTextView recent_contacts_results_title, no_records_found_text_view;
    private RadioButton rb_notifications_all, rb_notifications_calls, rb_notifications_meetings, rb_notifications_tasks;
    private ListView recent_contacts_list_view, dashboard_notifications_list_view;
    private RecentContactsAdapter recentContactsAdapter;
    private DashboardNotificationsAdapter notificationsAdapter;
    private QuickSearchAdapter quickSearchAdapter;
    private QuickSearchContactSelectionListener quickSearchContactSelectionListener;

    public void setQuickSearchContactSelectionListener(QuickSearchContactSelectionListener quickSearchContactSelectionListener) {
        this.quickSearchContactSelectionListener = quickSearchContactSelectionListener;
    }

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        currentLayout = (RelativeLayout) inflater.inflate(R.layout.fragment_dashboard, container, false);

        return currentLayout;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        try {
            sharedPreferences = new MMSharedPreferences(getContext());
            encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);

            layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            no_records_found_text_view = (MMTextView) layoutInflater.inflate(R.layout.no_records_found_textview, null);

            quick_search_auto_complete_text_view = (DelayAutoCompleteTextView) currentLayout.findViewById(R.id.quick_search_auto_complete_text_view);
            voice_recorder = (ImageView) currentLayout.findViewById(R.id.voice_recorder);

            quickSearchAdapter = new QuickSearchAdapter(getContext(), encryptedClientKey);
            quick_search_auto_complete_text_view.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            quick_search_auto_complete_text_view.setAdapter(quickSearchAdapter);
            quick_search_auto_complete_text_view.setLoadingIndicator((android.widget.ProgressBar) currentLayout.findViewById(R.id.quick_search_pb_loading_indicator));

            quick_search_auto_complete_text_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    QuickSearchContact selectedContact = (QuickSearchContact) adapterView.getItemAtPosition(position);
                    quick_search_auto_complete_text_view.setAdapter(null); // to stop filtering after selecting row from drop down
                    quick_search_auto_complete_text_view.setText(selectedContact.getName());
                    quick_search_auto_complete_text_view.setAdapter(quickSearchAdapter);

                    hideKeyboard();

                    if (quickSearchContactSelectionListener != null) {
                        quickSearchContactSelectionListener.goToContactDetailPage(selectedContact);
                    }
                }
            });

            quick_search_auto_complete_text_view.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int start, int count, int after) {
                    voice_recorder.setVisibility(View.GONE);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });

            recent_contacts_results_title = (MMTextView) currentLayout.findViewById(R.id.recent_contacts_results_title);

            recent_contacts_list_view = (ListView) currentLayout.findViewById(R.id.recent_contacts_list_view);
            recentContactsAdapter = new RecentContactsAdapter(getContext(), (DashboardActivity) getActivity());
            recent_contacts_list_view.setAdapter(recentContactsAdapter);

            rb_notifications_all = (RadioButton) currentLayout.findViewById(R.id.rb_notifications_all);
            rb_notifications_calls = (RadioButton) currentLayout.findViewById(R.id.rb_notifications_calls);
            rb_notifications_meetings = (RadioButton) currentLayout.findViewById(R.id.rb_notifications_meetings);
            rb_notifications_tasks = (RadioButton) currentLayout.findViewById(R.id.rb_notifications_tasks);

            dashboard_notifications_list_view = (ListView) currentLayout.findViewById(R.id.dashboard_notifications_list_view);
            notificationsAdapter = new DashboardNotificationsAdapter(getContext());
            dashboard_notifications_list_view.setAdapter(notificationsAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getApplicationContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(quick_search_auto_complete_text_view.getWindowToken(), 0);
    }

    public void resetQuickSearchTextFiled(boolean changeVoiceSearchIcon) {
        quick_search_auto_complete_text_view.setText("");
        hideKeyboard();

        if (changeVoiceSearchIcon) {
            voice_recorder.setImageResource(R.drawable.voice_recorder);
            voice_recorder.setTag(1);
            voice_recorder.setVisibility(View.VISIBLE);
        }
    }

    public void updateRecentContacts(int count, final List<Contact> contactsList) {
        //recent_contacts_results_title.setText(String.format("%,d Records", count));

        if (recent_contacts_list_view.getHeaderViewsCount() > 0) {
            recent_contacts_list_view.removeHeaderView(no_records_found_text_view);
        }

        if (contactsList == null || contactsList.isEmpty()) {
            recent_contacts_results_title.setText("0 Records");
            recent_contacts_list_view.addHeaderView(no_records_found_text_view);
        } else {
            recent_contacts_results_title.setText(String.format("%,d Records", contactsList.size()));

            recentContactsAdapter.setContactsList(contactsList);
            recentContactsAdapter.notifyDataSetChanged();

            recent_contacts_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    openContactDetailPage(contactsList.get(position));
                }
            });
        }
    }

    public void openContactDetailPage(Contact contact) {
        if (contact.getCanViewEmployeeId().equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(getContext(), getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            Intent openContactDetailsIntent = new Intent(getContext(), ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, contact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD_RECENT_CONTACTS);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            getActivity().overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateNotifications(List<Notification> notifications) {
        // Removing no records found message if already added to list view.
        if (dashboard_notifications_list_view.getHeaderViewsCount() > 0) {
            dashboard_notifications_list_view.removeHeaderView(no_records_found_text_view);
        }

        if (notifications == null || notifications.isEmpty()) {
            dashboard_notifications_list_view.addHeaderView(no_records_found_text_view);
        }

        notificationsAdapter.setNotifications(notifications);
        notificationsAdapter.notifyDataSetChanged();

        MMProgressDialog.hideProgressDialog();
    }

    public void resetNotificationsRadioButtons(String currentActivity) {
        switch (currentActivity) {
            case Constants.NOTIFICATION_ALL:
                rb_notifications_all.setChecked(true);
                break;
            case Constants.NOTIFICATION_CALLS:
                rb_notifications_calls.setChecked(true);
                break;
            case Constants.NOTIFICATION_MEETINGS:
                rb_notifications_meetings.setChecked(true);
                break;
            case Constants.NOTIFICATION_TASKS:
                rb_notifications_tasks.setChecked(true);
                break;
        }
    }

    public void updateQuickSearchTextFiledWithVoiceSearchResults(ArrayList<String> speechResults) {
        if (speechResults.size() > 0) {
            quick_search_auto_complete_text_view.setText(speechResults.get(0));
        }
    }

    public static void changeQuickSearchVoiceRecorderIcon(int tag) {
        if (tag == 1) {
            voice_recorder.setImageResource(R.drawable.voice_recorder);
        } else {
            voice_recorder.setImageResource(R.drawable.close);
        }

        voice_recorder.setTag(tag);
        voice_recorder.setVisibility(View.VISIBLE);
    }
}
