package com.mirabeltechnologies.magazinemanager.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.RequiresPermissionAlertDialogListener;


/**
 * Created by venkat on 04/10/17.
 */
public class MMAlertDialog {
    public static AlertDialog alertDialog = null;
    public static boolean isAlertDialogShown = false;
    public static AlertDialogSelectionListener listener = null;
    private static int requestCode = 0;

    public static RequiresPermissionAlertDialogListener permissionListener = null;

    public static void setListener(AlertDialogSelectionListener listener) {
        MMAlertDialog.listener = listener;
    }

    public static void setPermissionListener(RequiresPermissionAlertDialogListener permissionListener) {
        MMAlertDialog.permissionListener = permissionListener;
    }

    public static void setRequestCode(int requestCode) {
        MMAlertDialog.requestCode = requestCode;
    }

    /**
     * @param title
     * @param message
     */
    public static void showAlertDialog(Context context, String title, String message) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void showAlertDialogWithCallback(Context context, String title, String message, boolean showCancelButton) {
        try {
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okCallback(Constants.ButtonType.POSITIVE);
                }
            });

            if (showCancelButton) {
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(context, R.color.gray));

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void showAlertDialogWithCustomButtonsAndWithCallback(Context context, AlertDialogSelectionListener alertDialogSelectionListener, int alertDialogRequestCode, String title, String message, String positiveButtonName, String negativeButtonName) {
        try {
            setListener(alertDialogSelectionListener);
            setRequestCode(alertDialogRequestCode);

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            alertDialogBuilder.setPositiveButton(positiveButtonName, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okCallback(Constants.ButtonType.POSITIVE);
                }
            });

            alertDialogBuilder.setNegativeButton(negativeButtonName, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    okCallback(Constants.ButtonType.NEGATIVE);
                }
            });

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(context, R.color.gray));

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

    public static void okCallback(Constants.ButtonType buttonType) {
        if (listener != null) {
            if (requestCode > 0)
                listener.alertDialogCallback(buttonType, requestCode);
            else
                listener.alertDialogCallback();
        }
    }

    public static void dismiss() {
        if (alertDialog != null)
            alertDialog.dismiss();
    }


    public static void showAlertDialogWithCallbackAndCustomView(Context context, String title, String message, boolean showCancelButton, int layout_view_id, final RequiresPermissionAlertDialogListener listnr, final String dialogType) {
        try {
            setPermissionListener(listnr);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
            alertDialogBuilder.setTitle(title);
            alertDialogBuilder.setMessage(message);
            alertDialogBuilder.setCancelable(false);

            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View dialogView = inflater.inflate(layout_view_id, null);
            alertDialogBuilder.setView(dialogView);

            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okCallback(Constants.ButtonType.POSITIVE);
                }
            });

            if (showCancelButton) {
                alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
            }

            alertDialog = alertDialogBuilder.create();
            alertDialog.show();

            CheckBox remember_permission_checkbox = alertDialog.findViewById(R.id.remember_permission_checkbox);
            remember_permission_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    listnr.rememberCheckboxStatus(isChecked,dialogType);
                }
            });

            TextView msgTxt = (TextView) alertDialog.findViewById(android.R.id.message);
            msgTxt.setTextSize(context.getResources().getDimension(R.dimen.alert_dialog_text_font_size));

            isAlertDialogShown = true;

            Button cancelButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
            if (cancelButton != null)
                cancelButton.setTextColor(ContextCompat.getColor(context, R.color.gray));

        } catch (Exception e) {
            isAlertDialogShown = false;
            e.printStackTrace();
        }
    }

}
