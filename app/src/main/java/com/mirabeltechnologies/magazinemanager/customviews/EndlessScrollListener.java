package com.mirabeltechnologies.magazinemanager.customviews;

import android.widget.AbsListView;

import com.mirabeltechnologies.magazinemanager.constants.Constants;


/**
 * Created by venkat on 9/19/16.
 */
public abstract class EndlessScrollListener implements AbsListView.OnScrollListener {

    // The minimum number of items to have below your current scroll position before loading more.
    private int visibleThreshold = 5;

    // The current offset index of data you have loaded
    private int currentPage = 0;

    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;

    // True if we are still waiting for the last set of data to load.
    private boolean loading = true;

    private boolean isAllRecordsLoaded = false;

    // Sets the starting page index
    private int startingPageIndex = 0;

    // Last updated visible item index, this is used in order to prevent frequent hits on main UI
    private int lastVisibleItemIndex = 0;
    private int currentVisiblePageNo = 0, lastUpdatedVisibleItemPageNo = 0;

    private float PAGE_SIZE = Constants.PAGE_SIZE_FLOAT;

    public EndlessScrollListener() {
    }

    public EndlessScrollListener(float pageSize) {
        this.PAGE_SIZE = pageSize;
    }

    /*public EndlessScrollListener(int visibleThreshold) {
        this.visibleThreshold = visibleThreshold;
    }*/

    public EndlessScrollListener(int visibleThreshold, int startingPageIndex) {
        this.visibleThreshold = visibleThreshold;
        this.startingPageIndex = startingPageIndex;
        this.currentPage = startingPageIndex;
    }

    public void setAllRecordsLoaded(boolean allRecordsLoaded) {
        isAllRecordsLoaded = allRecordsLoaded;
    }

    @Override
    public void onScrollStateChanged(AbsListView absListView, int i) {
        // Don't take any action on changed
    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        try {
            // If the total item count is zero and the previous isn't, assume the list is invalidated and should be reset back to initial state
            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                    this.isAllRecordsLoaded = false;
                    this.lastVisibleItemIndex = 0;
                    this.currentVisiblePageNo = 0;
                    this.lastUpdatedVisibleItemPageNo = 0;
                }
            }

            // If it's still loading, we check to see if the data set count has changed, if so we conclude it has finished loading and update the current page number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
                currentPage++;
            }

            // If it isn't currently loading, we check to see if we have reached the visibleThreshold and need to reload more data. If we do need to reload some more data, we execute onLoadMore to fetch the data.
            if (!loading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                if (!isAllRecordsLoaded) {
                    loading = onLoadMore(currentPage + 1, totalItemCount);
                }

                if (loading == true)
                    isAllRecordsLoaded = false;
                else
                    isAllRecordsLoaded = true;
            }

            if (!loading && firstVisibleItem != lastVisibleItemIndex) {
                lastVisibleItemIndex = firstVisibleItem;

                currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / PAGE_SIZE);

                if (currentVisiblePageNo != 0) {
                    lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                    updatePageNumber(lastUpdatedVisibleItemPageNo);
                }

                // to update last page number when we have less no. of records in last page
                if (firstVisibleItem + visibleItemCount == totalItemCount) {
                    lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                    updatePageNumber(lastUpdatedVisibleItemPageNo);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Returns true if more data is being loaded; returns false if there is no more data to load.
    public abstract boolean onLoadMore(int page, int totalItemsCount);

    // Update current visible page number to activity while scrolling list view
    public abstract void updatePageNumber(int currentVisiblePageNo);
}
