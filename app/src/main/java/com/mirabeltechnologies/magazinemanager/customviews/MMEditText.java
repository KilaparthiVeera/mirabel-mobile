package com.mirabeltechnologies.magazinemanager.customviews;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by venkat on 04/10/17.
 */
public class MMEditText extends AppCompatEditText {
    public MMEditText(Context context) {
        super(context);

        CustomFontUtils.applyCustomFont(this, context, null);
    }

    public MMEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }

    public MMEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        CustomFontUtils.applyCustomFont(this, context, attrs);
    }
}
