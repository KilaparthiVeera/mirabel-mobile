package com.mirabeltechnologies.magazinemanager.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.util.FontCache;

/**
 * Created by venkat on 04/10/17.
 */
public class CustomFontUtils {

    public static final String ANDROID_SCHEMA = "http://schemas.android.com/apk/res/android";

    public final static int LIGHT = 4;

    public static void applyCustomFont(TextView customFontTextView, Context context, AttributeSet attrs) {
        try {
            TypedArray attributesArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);

            String fontName = attributesArray.getString(R.styleable.CustomFontTextView_fontName);

            // check if a special textStyle was used (e.g. extra bold)
            int textStyle = attributesArray.getInt(R.styleable.CustomFontTextView_textStyle, 0);

            // if nothing extra was used, fall back to regular android:textStyle parameter
            if (textStyle == 0 && attrs != null) {
                textStyle = attrs.getAttributeIntValue(ANDROID_SCHEMA, "textStyle", Typeface.NORMAL);
            }

            Typeface customFont = selectTypeface(context, textStyle);
            customFontTextView.setTypeface(customFont);

            attributesArray.recycle();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Typeface selectTypeface(Context context, int textStyle) {
        switch (textStyle) {
            case Typeface.BOLD:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Bold.ttf");

            case Typeface.ITALIC:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Italic.ttf");

            case Typeface.BOLD_ITALIC:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Bold-Italic.ttf");

            case Typeface.NORMAL:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Roman.ttf");

            case LIGHT:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Light.ttf");

            default:
                return FontCache.getTypeface(context, "fonts/Swiss-721-BT-Roman.ttf");
        }
    }
}
