package com.mirabeltechnologies.magazinemanager.customviews;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;

/**
 * @author venkat
 */
public class MMProgressDialog {
    public static AlertDialog progressDialog = null;
    public static boolean isProgressDialogShown = false;

    /**
     * @param ctx Context
     */
    public static void showProgressDialog(Context ctx) {
        try {
            AlertDialog.Builder progressDialogBuilder = new AlertDialog.Builder(ctx);
            progressDialogBuilder.setTitle("");
            progressDialogBuilder.setCancelable(false);

            WebView webView = new WebView(ctx);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
            webView.setBackgroundColor(Color.TRANSPARENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                // chromium, enable hardware acceleration
                webView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
            } else {
                // older android version, disable hardware acceleration
                webView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
            }

            webView.loadDataWithBaseURL("file:///android_asset/", ctx.getResources().getString(R.string.loading_indicator_html), "text/html", Constants.CHARSET_NAME, "");

            progressDialogBuilder.setView(webView);

            progressDialog = progressDialogBuilder.create();
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            progressDialog.show();

            isProgressDialogShown = true;

        } catch (Exception e) {
            isProgressDialogShown = false;
            e.printStackTrace();
        }
    }

    /**
     * Method to hide the dialog.
     */
    public static void hideProgressDialog() {
        try {
            if (progressDialog != null) {
                progressDialog.dismiss();
                isProgressDialogShown = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
