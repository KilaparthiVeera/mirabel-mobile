package com.mirabeltechnologies.magazinemanager.customviews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import org.jsoup.parser.Parser;

/**
 * Created by venkat on 11/23/17.
 */

public class CustomDialog extends Dialog {
    private Context context;
    private View contentView;
    private MMTextView pop_up_title, pop_up_text_view;
    private ImageView pop_up_close_icon;
    private String title, text;
    private boolean showAsHTML = false;

    public CustomDialog(Context context, View contentView, String title, String text, boolean showAsHTML) {
        super(context, R.style.DialogWithListViewStyle);

        this.context = context;
        this.contentView = contentView;
        this.title = title;
        this.text = text;
        this.showAsHTML = showAsHTML;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(contentView);

        try {
            // Setting dialog background to transparent in order to remove window edges outside radius i.e. for smooth radius.
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setCancelable(true);
            pop_up_title = (MMTextView) findViewById(R.id.pop_up_title);
            pop_up_close_icon = (ImageView) findViewById(R.id.pop_up_close_icon);
            pop_up_text_view = (MMTextView) findViewById(R.id.pop_up_text_view);

            pop_up_title.setText(title);

            if (showAsHTML)
                pop_up_text_view.setText(Utility.fromHtml(Parser.unescapeEntities(text, false)));
            else
                pop_up_text_view.setText(text);

            pop_up_close_icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dismiss();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
