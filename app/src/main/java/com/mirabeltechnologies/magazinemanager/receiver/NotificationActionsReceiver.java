package com.mirabeltechnologies.magazinemanager.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;

import com.mirabeltechnologies.magazinemanager.activities.EventDetailsActivity;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.NewActivity;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.services.MarkAsCompleteForegroundService;
import com.mirabeltechnologies.magazinemanager.services.SnoozeService;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.IOException;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_CLOSE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_DETAILS;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_EVENT_DELETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_MARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_NOTIFICATION_FORCE_CLOSE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_SNOOZE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_UNMARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_ID;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_TYPE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.PHONE_NUMBER;

public class NotificationActionsReceiver extends BroadcastReceiver {

    int notification_id;
    Context context;
    MMLocalDataBase mmLocalDataBase;
    String type;
    ActivityData activityData;
    MMSettingsPreferences mmSettingsPreferences;
    Utility utility;
    MMSharedPreferences sharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        this.context = context;
        mmLocalDataBase = new MMLocalDataBase(context);
        notification_id = intent.getExtras().getInt(NOTIFICATION_ID);
        switch (intent.getAction()) {
            case ACTION_CLOSE:
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                removePendingIntentsAndNotifications();
                break;
            case ACTION_NOTIFICATION_FORCE_CLOSE:
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                removePendingIntentsAndNotifications();
                break;
            case ACTION_CALL:
                //to dismiss the notification panel when tapped on the call action.
                Intent closeIntent = new Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS);
                context.sendBroadcast(closeIntent);

                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);

                String mobileNumber = intent.getExtras().getString(PHONE_NUMBER).trim();
                Intent calIntent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + mobileNumber));
                calIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                PackageManager packageManager = context.getPackageManager();
                if (calIntent.resolveActivity(packageManager) != null) {
                    context.startActivity(calIntent);
                    //update the call status to the server
                    PostCallStatusToServer(activityData);
                }
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                removeNotifications(notification_id);


                break;
            case ACTION_SNOOZE:
                utility = new Utility();
                mmSettingsPreferences = new MMSettingsPreferences(context);
                type = intent.getExtras().getString(NOTIFICATION_TYPE);
                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                Intent i = new Intent(context, SnoozeService.class);
                try {
                    i.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                i.putExtra(NOTIFICATION_ID, notification_id);
                i.putExtra(NOTIFICATION_TYPE, type);
                context.startService(i);
                removePendingIntentsAndNotifications();

                break;


            case ACTION_DETAILS: {
                removePendingIntentsAndNotifications();
                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);
                Intent eventDetails = new Intent(context, EventDetailsActivity.class);
                try {
                    eventDetails.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                eventDetails.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.RequestFrom.BUILD_NOTIFICATION);
                eventDetails.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                context.startActivity(eventDetails);
                break;
            }
            case ACTION_MARK: {
                utility = new Utility();
                mmSettingsPreferences = new MMSettingsPreferences(context);
                type = intent.getExtras().getString(NOTIFICATION_TYPE);
                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                //start a foreground service and mark this activity as "done". and remove it from notification tray.
                Intent foreGroundService = new Intent(context, MarkAsCompleteForegroundService.class);
                try {
                    foreGroundService.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                foreGroundService.putExtra(NOTIFICATION_ID, notification_id);
                foreGroundService.setAction(ACTION_MARK);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(foreGroundService);
                } else {
                    context.startService(foreGroundService);
                }
                removePendingIntentsAndNotifications();

                break;
            }
            case ACTION_EVENT_DELETE: {
                utility = new Utility();
                mmSettingsPreferences = new MMSettingsPreferences(context);
                type = intent.getExtras().getString(NOTIFICATION_TYPE);
                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                //start a foreground service and mark this activity as "done". and remove it from notification tray.
                Intent foreGroundService = new Intent(context, MarkAsCompleteForegroundService.class);
                try {
                    foreGroundService.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                foreGroundService.putExtra(NOTIFICATION_ID, notification_id);
                foreGroundService.setAction(ACTION_EVENT_DELETE);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(foreGroundService);
                } else {
                    context.startService(foreGroundService);
                }

                removePendingIntentsAndNotifications();

                break;
            }

            case ACTION_UNMARK: {
                utility = new Utility();
                mmSettingsPreferences = new MMSettingsPreferences(context);
                type = intent.getExtras().getString(NOTIFICATION_TYPE);
                activityData = (ActivityData) intent.getExtras().getSerializable(EVENT_DATA);
                mmLocalDataBase.updateCancelledStatusOfNotification(notification_id);
                //start a foreground service and mark this activity as "done". and remove it from notification tray.
                Intent foreGroundService = new Intent(context, MarkAsCompleteForegroundService.class);
                try {
                    foreGroundService.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                foreGroundService.putExtra(NOTIFICATION_ID, notification_id);
                foreGroundService.setAction(ACTION_UNMARK);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    context.startForegroundService(foreGroundService);
                } else {
                    context.startService(foreGroundService);
                }
                removePendingIntentsAndNotifications();

                break;
            }

        }

    }


    private void removePendingIntentsAndNotifications() {
        removeNotifications(notification_id);

       /* PendingIntent pendingIntent = PendingIntent.getBroadcast(context, notification_id, new Intent(context, AlarmReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);*/

    }


    private void removeNotifications(int notificationId) {
        BuildNotification buildNotification = new BuildNotification(context);
        buildNotification.removeNotificationFromNotificationTray(notificationId);
    }


    private void PostCallStatusToServer(ActivityData activitydata) {
        Long customerId = activitydata.getCustomerId();
        makePhoneCall(String.valueOf(customerId), activitydata.getPhone());

    }

    private void makePhoneCall(String customerId, String phoneNumber) {
        if (Utility.isValidPhoneNumber(phoneNumber)) {
            String activityNotes;
            sharedPreferences = new MMSharedPreferences(context);
            String encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);
            int loggedInRepId = sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID);
            String loggedInRepName = sharedPreferences.getString(Constants.SP_LOGGED_IN_REP_NAME);
            activityNotes = String.format("A call was initiated to (%s) from mobile app.", phoneNumber);
            NewActivity newActivityBean = new NewActivity(customerId);
            newActivityBean.setNotes(activityNotes);
            newActivityBean.setActionType("call");
            newActivityBean.setAssignedByRepId(String.valueOf(loggedInRepId));
            newActivityBean.setAssignedByRepName(loggedInRepName);
            newActivityBean.setAssignedToRepId(String.valueOf(loggedInRepId));
            newActivityBean.setAssignedToRepName(loggedInRepName);
            newActivityBean.setPrivate(false); // all activity logs are public by default when we dial phone number through out app.
            ActivitiesModel activitiesModel = new ActivitiesModel(context);
            activitiesModel.createNewActivityFromNotification(encryptedClientKey, Utility.getDTTicks(), newActivityBean);

        }


    }


}
