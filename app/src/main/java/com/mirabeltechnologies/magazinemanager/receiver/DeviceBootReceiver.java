package com.mirabeltechnologies.magazinemanager.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.services.NotificationService;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;


public class DeviceBootReceiver extends BroadcastReceiver {

    //if the device reboot, then we have to again reset the alarms . As the reboot clears all the alarms .
    MMSettingsPreferences mmSettingsPreferences;
    Context context;
    MMSharedPreferences mmSharedPreferences;

    @Override
    public void onReceive(Context context, Intent intent) {
        try {
            mmSettingsPreferences = new MMSettingsPreferences(context);
        mmSharedPreferences = new MMSharedPreferences(context);
        this.context = context;
        //check weather user is loggedin or not .
        //fetch the notifications data from server based on user permissions like "enable calls" and "enable meetings".
        //schedule them with the AlarmManager.


        //first clear all the notifications except cancelled ones. Because those which are cancelled should not be repeated again. But all not cancelled can repeat
        MMLocalDataBase mmLocalDataBase = new MMLocalDataBase(context);
        mmLocalDataBase.removeScheduledNotificationOnDeviceReebot();
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED") || intent.getAction().equals("android.intent.action.QUICKBOOT_POWERON")) {
            if (mmSharedPreferences.isUserLoggedIn()) {
                Intent serintent = new Intent(context, NotificationService.class);
                context.startService(serintent);
            }
        }
        } catch (IllegalStateException e) {

        } catch (Exception e) {


        }
    }


}
