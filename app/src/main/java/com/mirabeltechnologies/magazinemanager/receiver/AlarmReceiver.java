package com.mirabeltechnologies.magazinemanager.receiver;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.services.FireNotificationsService;

import static androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService;


public class AlarmReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        //for mi mobiles , app is crashing here .
        // ContextCompat.startForegroundService(context, i );
        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, Constants.ACTION_NOTIFICATION_POWER_MANAGER);
        //wakeup for 15 seconds when the alarm triggers for notification
        wakeLock.acquire(15000);
        //This will send a notification message and show notification in notification tray
        ComponentName comp = new ComponentName(context.getPackageName(), FireNotificationsService.class.getName());
        startWakefulService(context, (intent.setComponent(comp)));
    }
}
