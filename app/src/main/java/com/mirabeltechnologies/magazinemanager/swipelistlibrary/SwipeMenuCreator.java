package com.mirabeltechnologies.magazinemanager.swipelistlibrary;

public interface SwipeMenuCreator {
    void create(SwipeMenu menu);

}
