package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 3/8/18.
 */

public interface ContactDetailsListener {
    void showSelectedDetails(String sectionTitle);
}
