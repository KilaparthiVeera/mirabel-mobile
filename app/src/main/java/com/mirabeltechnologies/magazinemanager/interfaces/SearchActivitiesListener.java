package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 1/9/18.
 */

public interface SearchActivitiesListener {
    void openContactDetailPage(Long customerId, Long parentId);

    void deleteActivity(int position, String activityType, Long activityId, String action);

    void markActivity(int position, String activityType, Long activityId, String action);

    void openEventDetailsPage(int position, String activityType, Long activityId, String action);
}
