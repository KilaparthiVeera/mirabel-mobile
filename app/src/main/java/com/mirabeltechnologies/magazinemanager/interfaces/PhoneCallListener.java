package com.mirabeltechnologies.magazinemanager.interfaces;

public interface PhoneCallListener {
    void takeConfirmationToMakePhoneCall(String customerId, String phoneNumber, String extension);
}
