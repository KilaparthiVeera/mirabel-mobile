package com.mirabeltechnologies.magazinemanager.interfaces;

import com.mirabeltechnologies.magazinemanager.beans.Post_BusinessCard;
import com.mirabeltechnologies.magazinemanager.beans.ScanCardDetails;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface BusinessCardApi {
    @Headers("Content-Type: application/json")
    @POST("processBusinessCard")
    Call<ScanCardDetails> getResponce(@Body Post_BusinessCard post_businessCard);
}
