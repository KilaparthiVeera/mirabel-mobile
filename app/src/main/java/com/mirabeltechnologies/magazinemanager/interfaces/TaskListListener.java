package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 1/9/18.
 */

public interface TaskListListener {
    void deleteTask(int position, Long taskId, String action);

    void markTask(int position, Long taskId, String action);
}
