package com.mirabeltechnologies.magazinemanager.interfaces;

import com.mirabeltechnologies.magazinemanager.beans.PostOfficeAddressRes;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

/**
 * Created by veera on 10/30/2017.
 */

public interface PostOfficeResApi {
    @GET
    Call<PostOfficeAddressRes> getData(@Url String url);
}
