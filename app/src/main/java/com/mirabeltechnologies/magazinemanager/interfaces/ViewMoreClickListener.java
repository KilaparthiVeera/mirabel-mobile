package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 12/8/17.
 */

public interface ViewMoreClickListener {
    void viewMoreClicked(int index);
}
