package com.mirabeltechnologies.magazinemanager.interfaces;

import com.mirabeltechnologies.magazinemanager.beans.QuickSearchContact;

/**
 * Created by venkat on 11/1/17.
 */

public interface QuickSearchContactSelectionListener {
    void goToContactDetailPage(QuickSearchContact contact);
}
