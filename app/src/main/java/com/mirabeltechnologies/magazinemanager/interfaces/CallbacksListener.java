package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 05/28/18.
 */

public interface CallbacksListener {
    void openContactDetailPage(Long customerId, Long parentId, String canViewEmployeeId);

    void markActivity(int position, Long activityId, String canViewEmployeeId);

    void chooseEmailClient(String customerId, String emailId);
}
