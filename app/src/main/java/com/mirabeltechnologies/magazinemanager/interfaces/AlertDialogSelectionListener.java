package com.mirabeltechnologies.magazinemanager.interfaces;

import com.mirabeltechnologies.magazinemanager.constants.Constants;

/**
 * Created by venkat on 04/10/17.
 */
public interface AlertDialogSelectionListener {
    void alertDialogCallback();

    void alertDialogCallback(Constants.ButtonType buttonType, int requestCode);
}
