package com.mirabeltechnologies.magazinemanager.interfaces;

public interface RequiresPermissionAlertDialogListener {

    public void rememberCheckboxStatus(boolean checkboxStatus,String dialogType);
}
