package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 11/6/17.
 */

public interface ContactsAdvSearchListener {
    void updateSearchKeyword(String keyword, int typeOfFilter);
}
