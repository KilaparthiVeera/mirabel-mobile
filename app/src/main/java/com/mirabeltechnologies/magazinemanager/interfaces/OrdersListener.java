package com.mirabeltechnologies.magazinemanager.interfaces;

/**
 * Created by venkat on 1/9/18.
 */

public interface OrdersListener {
    void openContactDetailPage(Long customerId);
}
