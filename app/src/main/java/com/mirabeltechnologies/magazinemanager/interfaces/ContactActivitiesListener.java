package com.mirabeltechnologies.magazinemanager.interfaces;

public interface ContactActivitiesListener {
    void markTask(int position, Long taskId, String action);
}
