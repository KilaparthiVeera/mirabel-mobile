package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.app.Service;
import android.content.Context;

import androidx.annotation.RequiresApi;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.CallbacksActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewActivityForContactScreen;
import com.mirabeltechnologies.magazinemanager.activities.EventDetailsActivity;
import com.mirabeltechnologies.magazinemanager.activities.SearchActivitiesScreen;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactActivitiesActivity;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.SearchActivitiesFilter;
import com.mirabeltechnologies.magazinemanager.beans.NewActivity;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;

import com.mirabeltechnologies.magazinemanager.services.MarkAsCompleteForegroundService;
import com.mirabeltechnologies.magazinemanager.services.NotificationEventsScheduleJobService;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.RequestFrom.MARK_DELETE_FOREGROUND_SERVICE;

/**
 * Created by venkat on 12/8/17.
 */

public class ActivitiesModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;
    private Service service;

    public ActivitiesModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }


    public ActivitiesModel(Context context, Service service, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.service = service;
        this.requestFrom = requestFrom;
        this.TAG = service.getClass().getSimpleName();
    }

    public ActivitiesModel(Context context) {
        this.context = context;
        TAG = "From Notification";

    }

    public void getActivityNotesById(final String encryptedClientKey, String activityId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getActivityNotesByIdReqURL = String.format("%s/%s?activityId=%s", Constants.MAIN_URL, Constants.GET_ACTIVITY_NOTES_BY_ID, activityId);
                Log.e("veera",getActivityNotesByIdReqURL);
                JsonObjectRequest getActivityNotesByIdReq = new JsonObjectRequest(Request.Method.GET, getActivityNotesByIdReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        String activityNotes = response.getString("Message");

                                        if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).updateActivityNotes(activityNotes);
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).updateActivityNotes(activityNotes);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                    ((ContactActivitiesActivity) activity).onVolleyErrorResponse(error);

                                else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                    ((SearchActivitiesScreen) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getActivityNotesByIdReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewActivity(final String encryptedClientKey, String dtTicks, NewActivity newActivity) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String createNewActivityReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.CREATE_NEW_ACTIVITY, dtTicks);
                Log.e("veera","Meeting and Calls"+createNewActivityReqURL);
                JSONObject reqObject = new JSONObject();
                reqObject.put("CustomersId", newActivity.getCustomerId());
                reqObject.put("ActionType", newActivity.getActionType());
                reqObject.put("ActivityType", newActivity.getActivityTypeId());

                if (newActivity.getDate().length() > 0)
                    reqObject.put("MeetingDate", newActivity.getDate());

                if (newActivity.getTime().length() > 0)
                    reqObject.put("MeetingTime", newActivity.getTime());

                reqObject.put("ActivityRep", newActivity.getAssignedByRepId());
                reqObject.put("RepAssignedTo", newActivity.getAssignedToRepId());
                reqObject.put("Notes", newActivity.getNotes());
                reqObject.put("IsPrivate", newActivity.isPrivate());
                reqObject.put("IsAddtoTaskList", newActivity.isAddToTaskList());

                Log.e("ram",reqObject.toString());
                Log.e("ram",encryptedClientKey);

                JsonObjectRequest createNewActivityReq = new JsonObjectRequest(Request.Method.POST, createNewActivityReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.e("veera",response.toString());

                                System.out.println("====response = " + response);
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).updateActivityCreateStatus();
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                        if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)

                                        ((CreateNewActivityForContactScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {

                                        if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {

                                System.out.println("===responseerror = " + error.getMessage());
                                if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                    ((CreateNewActivityForContactScreen) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(createNewActivityReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchActivities(final String encryptedClientKey, String dtTicks, SearchActivitiesFilter filter, int pageNumber, int limitCharacters) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.ACTIVITY_PAGE_SIZE_INT;
                String searchActivitiesReqURL = String.format("%s/%s?startIndex=%d&endIndex=%d&dtTicks=%s&limitcharacters=%d", Constants.MAIN_URL, Constants.SEARCH_ACTIVITY, startIndex, endIndex, dtTicks, limitCharacters);

                Log.e("venkat",searchActivitiesReqURL);
                JSONObject reqObject = new JSONObject();
                reqObject.put("FilterOption", filter.getFilterOption());

                if (filter.getCompanyName().length() > 0)
                    reqObject.put("Customer", filter.getCompanyName());

                if (filter.getCompanyName().length() > 0 && filter.getCompanyId().length() > 0)
                    reqObject.put("CustomerID", filter.getCompanyId());

                if (filter.getFromDate().length() > 0 || filter.getToDate().length() > 0) {
                    reqObject.put("DateType", filter.getDateType());

                    if (filter.getFromDate().length() > 0)
                        reqObject.put("StrFromDate", filter.getFromDate());

                    if (filter.getToDate().length() > 0)
                        reqObject.put("StrToDate", filter.getToDate());
                }

                if (filter.getKeyword().length() > 0)
                    reqObject.put("Keyword", filter.getKeyword());

                if (filter.getAssignedToRepId().length() > 0)
                    reqObject.put("SelectedEmployeeID", filter.getAssignedToRepId());
                else
                    reqObject.put("SelectedEmployeeID", "-1");

                if (filter.getCreatedByOrAssignedByRepId().length() > 0 && !filter.getCreatedByOrAssignedByRepId().equals("-1")) {
                    if (filter.getIsCreatedByOrAssignedBy() == 0)
                        reqObject.put("WhereQuery", "createdbyID = " + filter.getCreatedByOrAssignedByRepId());
                    else
                        reqObject.put("WhereQuery", "assignedbyID = " + filter.getCreatedByOrAssignedByRepId());
                }

                if (filter.getDepartmentId().length() > 0)
                    reqObject.put("DeptID", filter.getDepartmentId());

                if (filter.getActivityTypeId().length() > 0)
                    reqObject.put("MeetingType", filter.getActivityTypeId());

                Log.e("venkat",reqObject.toString());

                Log.e("venkat",encryptedClientKey);

                JsonObjectRequest searchActivitiesReq = new JsonObjectRequest(Request.Method.POST, searchActivitiesReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        JSONObject activityData = response.getJSONObject("activityData");
                                        if (activityData != null) {
                                            int noOfActivities = activityData.getInt("Count");
                                            int canDeleteNotes = activityData.getInt("CanDeleteNotes");
                                            int canEditNotes = activityData.getInt("CanEditNotes");

                                            Type activitiesListType = new TypeToken<ArrayList<ActivityData>>() {
                                            }.getType();

                                            List<ActivityData> searchActivitiesResultsList = new Gson().fromJson(activityData.getString("Results"), activitiesListType);
                                            ((SearchActivitiesScreen) activity).updateActivities(searchActivitiesResultsList, noOfActivities, canDeleteNotes, canEditNotes);
                                        }
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((SearchActivitiesScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            ((SearchActivitiesScreen) activity).updateActivities(null, 0, 0, 0);
                                        } else {
                                            ((SearchActivitiesScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((SearchActivitiesScreen) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);

                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(searchActivitiesReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void getDetailsByActivityID(final String encryptedClientKey, String dtTicks, JSONObject jsonObj) {

        String searchActivitiesReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.SEARCH_ACTIVITY, dtTicks);
        JsonObjectRequest searchActivitiesReq = new JsonObjectRequest(Request.Method.POST, searchActivitiesReqURL, jsonObj,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                JSONObject activityData = response.getJSONObject("activityData");
                                if (activityData != null) {
                                    int noOfActivities = activityData.getInt("Count");
                                    int canDeleteNotes = activityData.getInt("CanDeleteNotes");
                                    int canEditNotes = activityData.getInt("CanEditNotes");

                                    Type activitiesListType = new TypeToken<ArrayList<ActivityData>>() {
                                    }.getType();

                                    List<ActivityData> searchActivitiesResultsList = new Gson().fromJson(activityData.getString("Results"), activitiesListType);
                                    ((EventDetailsActivity) activity).updateData(searchActivitiesResultsList.get(0), canDeleteNotes, canEditNotes);
                                }
                            }
                            else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                ((EventDetailsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                            }
                            else {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                    ((EventDetailsActivity) activity).updateData(null, 0, 0);
                                } else {
                                    ((EventDetailsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            }
                        } catch (Exception e) {

                            e.printStackTrace();

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        ((EventDetailsActivity) activity).onVolleyErrorResponse(error);
                    }
                }
        ) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put(Constants.AUTHORISATION, encryptedClientKey);
                return headers;
            }
        };

        AppController.getInstance(context).addToRequestQueue(searchActivitiesReq, TAG);
    }


    public void modifyActivity(final String encryptedClientKey, String dtTicks, Long activityId, final String action) {


        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String modifyActivityReqURL = String.format("%s/%s?activityId=%d&action=%s&dtTicks=%s", Constants.MAIN_URL, Constants.ACTIVITY_ACTION, activityId, action, dtTicks);
                Log.e("veera",modifyActivityReqURL);
                JsonObjectRequest modifyActivityReq = new JsonObjectRequest(Request.Method.GET, modifyActivityReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {

                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).updateActivitiesAfterModify(action);
                                        else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                                            ((CallbacksActivity) activity).updateCallbacksAfterModify(action);
                                        else if (requestFrom == Constants.RequestFrom.EVENT_DETAILS_PAGE) {
                                            ((EventDetailsActivity) activity).updateActivitiesAfterModify(action);
                                        } else if (requestFrom == MARK_DELETE_FOREGROUND_SERVICE) {
                                            ((MarkAsCompleteForegroundService) service).updateServiceAfterModify(action);
                                        }
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                                            ((CallbacksActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.EVENT_DETAILS_PAGE) {
                                            ((EventDetailsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        } else if (requestFrom == MARK_DELETE_FOREGROUND_SERVICE) {
                                            ((MarkAsCompleteForegroundService) service).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }

                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                            ((SearchActivitiesScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                                            ((CallbacksActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.EVENT_DETAILS_PAGE) {
                                            ((EventDetailsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        } else if (requestFrom == MARK_DELETE_FOREGROUND_SERVICE) {
                                            ((MarkAsCompleteForegroundService) service).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                                    ((SearchActivitiesScreen) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                                    ((CallbacksActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.EVENT_DETAILS_PAGE) {
                                    ((EventDetailsActivity) activity).onVolleyErrorResponse(error);
                                } else if (requestFrom == Constants.RequestFrom.MARK_DELETE_FOREGROUND_SERVICE) {
                                    ((MarkAsCompleteForegroundService) service).onVolleyErrorResponse(error);
                                }
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(modifyActivityReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void fetchCallsAndMeetingsActivities(final String encryptedClientKey, String dtTicks, SearchActivitiesFilter filter, int pageNumber, int limitCharacters) {
        final Utility utility = new Utility();
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * 50) + 1;
                int endIndex = pageNumber * 50;

                String searchActivitiesReqURL = String.format("%s/%s?startIndex=%d&endIndex=%d&dtTicks=%s&limitcharacters=%d", Constants.MAIN_URL, Constants.SEARCH_ACTIVITY, startIndex, endIndex, dtTicks, limitCharacters);

                JSONObject reqObject = new JSONObject();
                reqObject.put("FilterOption", filter.getFilterOption());

                if (filter.getCompanyName().length() > 0)
                    reqObject.put("Customer", filter.getCompanyName());

                if (filter.getCompanyName().length() > 0 && filter.getCompanyId().length() > 0)
                    reqObject.put("CustomerID", filter.getCompanyId());

                if (filter.getFromDate().length() > 0 || filter.getToDate().length() > 0) {
                    reqObject.put("DateType", filter.getDateType());

                    if (filter.getFromDate().length() > 0)
                        reqObject.put("StrFromDate", filter.getFromDate());

                    if (filter.getToDate().length() > 0)
                        reqObject.put("StrToDate", filter.getToDate());
                }

                if (filter.getKeyword().length() > 0)
                    reqObject.put("Keyword", filter.getKeyword());

                if (filter.getAssignedToRepId().length() > 0)
                    reqObject.put("SelectedEmployeeID", filter.getAssignedToRepId());
                else
                    reqObject.put("SelectedEmployeeID", "-1");

                if (filter.getCreatedByOrAssignedByRepId().length() > 0 && !filter.getCreatedByOrAssignedByRepId().equals("-1")) {
                    if (filter.getIsCreatedByOrAssignedBy() == 0)
                        reqObject.put("WhereQuery", "createdbyID = " + filter.getCreatedByOrAssignedByRepId());
                    else
                        reqObject.put("WhereQuery", "assignedbyID = " + filter.getCreatedByOrAssignedByRepId());
                }

                if (filter.getDepartmentId().length() > 0)
                    reqObject.put("DeptID", filter.getDepartmentId());

                if (filter.getActivityTypeId().length() > 0)
                    reqObject.put("MeetingType", filter.getActivityTypeId());

                JsonObjectRequest searchActivitiesReq = new JsonObjectRequest(Request.Method.POST, searchActivitiesReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        JSONObject activityData = response.getJSONObject("activityData");
                                        if (activityData != null) {
                                            int noOfActivities = activityData.getInt("Count");
                                            int canDeleteNotes = activityData.getInt("CanDeleteNotes");
                                            int canEditNotes = activityData.getInt("CanEditNotes");

                                            Type activitiesListType = new TypeToken<ArrayList<ActivityData>>() {
                                            }.getType();

                                            List<ActivityData> searchActivitiesResultsList = new Gson().fromJson(activityData.getString("Results"), activitiesListType);
                                            ((NotificationEventsScheduleJobService) service).updateActivities(searchActivitiesResultsList, noOfActivities, canDeleteNotes, canEditNotes);
                                        }
                                    }

                                    else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            ((NotificationEventsScheduleJobService) service).updateActivities(null, 0, 0, 0);
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(searchActivitiesReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    public void createNewActivityFromNotification(final String encryptedClientKey, String dtTicks, NewActivity newActivity) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String createNewActivityReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.CREATE_NEW_ACTIVITY, dtTicks);

                JSONObject reqObject = new JSONObject();
                reqObject.put("CustomersId", newActivity.getCustomerId());
                reqObject.put("ActionType", newActivity.getActionType());
                reqObject.put("ActivityType", newActivity.getActivityTypeId());

                if (newActivity.getDate().length() > 0)
                    reqObject.put("MeetingDate", newActivity.getDate());

                if (newActivity.getTime().length() > 0)
                    reqObject.put("MeetingTime", newActivity.getTime());

                reqObject.put("ActivityRep", newActivity.getAssignedByRepId());
                reqObject.put("RepAssignedTo", newActivity.getAssignedToRepId());
                reqObject.put("Notes", newActivity.getNotes());
                reqObject.put("IsPrivate", newActivity.isPrivate());
                reqObject.put("IsAddtoTaskList", newActivity.isAddToTaskList());

                JsonObjectRequest createNewActivityReq = new JsonObjectRequest(Request.Method.POST, createNewActivityReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {


                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(createNewActivityReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*

    public void fetchCallsAndMeetingsActivities123(final String encryptedClientKey, String dtTicks, SearchActivitiesFilter filter, int pageNumber, int limitCharacters) {
        final Utility utility = new Utility();
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * 50) + 1;
                int endIndex = pageNumber * 50;

                String searchActivitiesReqURL = String.format("%s/%s?startIndex=%d&endIndex=%d&dtTicks=%s&limitcharacters=%d", Constants.MAIN_URL, Constants.SEARCH_ACTIVITY, startIndex, endIndex, dtTicks, limitCharacters);

                JSONObject reqObject = new JSONObject();
                reqObject.put("FilterOption", filter.getFilterOption());

                if (filter.getCompanyName().length() > 0)
                    reqObject.put("Customer", filter.getCompanyName());

                if (filter.getCompanyName().length() > 0 && filter.getCompanyId().length() > 0)
                    reqObject.put("CustomerID", filter.getCompanyId());

                if (filter.getFromDate().length() > 0 || filter.getToDate().length() > 0) {
                    reqObject.put("DateType", filter.getDateType());

                    if (filter.getFromDate().length() > 0)
                        reqObject.put("StrFromDate", filter.getFromDate());

                    if (filter.getToDate().length() > 0)
                        reqObject.put("StrToDate", filter.getToDate());
                }

                if (filter.getKeyword().length() > 0)
                    reqObject.put("Keyword", filter.getKeyword());

                if (filter.getAssignedToRepId().length() > 0)
                    reqObject.put("SelectedEmployeeID", filter.getAssignedToRepId());
                else
                    reqObject.put("SelectedEmployeeID", "-1");

                if (filter.getCreatedByOrAssignedByRepId().length() > 0 && !filter.getCreatedByOrAssignedByRepId().equals("-1")) {
                    if (filter.getIsCreatedByOrAssignedBy() == 0)
                        reqObject.put("WhereQuery", "createdbyID = " + filter.getCreatedByOrAssignedByRepId());
                    else
                        reqObject.put("WhereQuery", "assignedbyID = " + filter.getCreatedByOrAssignedByRepId());
                }

                if (filter.getDepartmentId().length() > 0)
                    reqObject.put("DeptID", filter.getDepartmentId());

                if (filter.getActivityTypeId().length() > 0)
                    reqObject.put("MeetingType", filter.getActivityTypeId());

                JsonObjectRequest searchActivitiesReq = new JsonObjectRequest(Request.Method.POST, searchActivitiesReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        JSONObject activityData = response.getJSONObject("activityData");
                                        if (activityData != null) {
                                            int noOfActivities = activityData.getInt("Count");
                                            int canDeleteNotes = activityData.getInt("CanDeleteNotes");
                                            int canEditNotes = activityData.getInt("CanEditNotes");

                                            Type activitiesListType = new TypeToken<ArrayList<ActivityData>>() {
                                            }.getType();

                                            List<ActivityData> searchActivitiesResultsList = new Gson().fromJson(activityData.getString("Results"), activitiesListType);
                                            ((TestJobService) service).updateActivities(searchActivitiesResultsList, noOfActivities, canDeleteNotes, canEditNotes);
                                        }
                                    } else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            ((TestJobService) service).updateActivities(null, 0, 0, 0);
                                        }
                                    }
                                    Log.i("cron data from server 8", utility.getCurrntTime() + "   " + response.toString());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.i("cron data from server 7", utility.getCurrntTime() + "   " + e.toString());
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.i("cron data from server 6", utility.getCurrntTime() + "   " + error.toString());
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(searchActivitiesReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("cron data from server 5", utility.getCurrntTime() + "   " + e.toString());
        }
    }
*/


}

