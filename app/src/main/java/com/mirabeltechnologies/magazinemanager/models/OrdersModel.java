package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.AddOpportunityActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateContactActivity;
import com.mirabeltechnologies.magazinemanager.activities.LoginActivity;
import com.mirabeltechnologies.magazinemanager.activities.OpportunitiesSearchActivity;
import com.mirabeltechnologies.magazinemanager.activities.OrdersActivity;
import com.mirabeltechnologies.magazinemanager.activities.SearchOrdersFiltersActivity;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.Order;
import com.mirabeltechnologies.magazinemanager.beans.SearchOrdersFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class OrdersModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;

    public OrdersModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void getCascadingMasterData(final String encryptedClientKey, String dtTicks, SearchOrdersFilter filter, final String masterDataOf) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getCascadingMasterDataReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.GET_CASCADING_MASTER, dtTicks);

                JSONObject reqObject = new JSONObject();
                reqObject.put("masterDataOf", masterDataOf);

                switch (masterDataOf) {
                    case Constants.GET_MASTER_DATA_BUSINESS_UNITS:
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("-1") ? 0 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("productId", filter.getProductId().equals("-1") ? 0 : Integer.parseInt(filter.getProductId()));
                        break;
                    case Constants.GET_MASTER_DATA_PRODUCTS:
                        reqObject.put("productType", filter.getProductTypeValue() == -1 ? 0 : filter.getProductTypeValue());
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("-1") ? 0 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("rateCardId", Integer.parseInt(filter.getRateCardId())); // Rate card default & initialized value is '0' so we are not checking it.
                        break;
                    case Constants.GET_MASTER_DATA_MAGAZINES:
                        reqObject.put("productType", filter.getProductTypeValue() == -1 ? 0 : filter.getProductTypeValue());
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("-1") ? 0 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("productId", filter.getProductId().equals("-1") ? 0 : Integer.parseInt(filter.getProductId()));
                        break;
                    case Constants.GET_MASTER_DATA_RATE_CARDS:
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("0") ? -1 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("productId", filter.getProductId().equals("-1") ? 0 : Integer.parseInt(filter.getProductId()));
                        break;
                    case Constants.GET_MASTER_DATA_ISSUE_YEARS:
                        reqObject.put("productType", filter.getProductTypeValue()); // Product Type default & initialized value is '-1' so we are not checking it.
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("0") ? -1 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("productId", filter.getProductId().equals("0") ? -1 : Integer.parseInt(filter.getProductId()));
                        break;
                    case Constants.GET_MASTER_DATA_ISSUES:
                        reqObject.put("productType", filter.getProductTypeValue()); // Product Type default & initialized value is '-1' so we are not checking it.
                        reqObject.put("businessUnit", filter.getBusinessUnitId().equals("0") ? -1 : Integer.parseInt(filter.getBusinessUnitId()));
                        reqObject.put("productId", filter.getProductId().equals("0") ? -1 : Integer.parseInt(filter.getProductId()));
                        reqObject.put("issueYear", filter.getIssueYearId().equals("0") ? -2 : Integer.parseInt(filter.getIssueYearId()));
                        break;
                }

                JsonObjectRequest getCascadingMasterDataReq = new JsonObjectRequest(Request.Method.POST, getCascadingMasterDataReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    Type masterDataListType = new TypeToken<ArrayList<MasterData>>() {
                                    }.getType();

                                    JSONObject responseMasterData = response.getJSONArray("Master").getJSONObject(0);
                                    List<MasterData> masterDataList = new Gson().fromJson(responseMasterData.getString("Value"), masterDataListType);

                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY) {
                                        ((AddOpportunityActivity) activity).updateMasterData(masterDataList, masterDataOf);
                                    }
                                    else  if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY) {
                                        ((OpportunitiesSearchActivity) activity).updateMasterData(masterDataList, masterDataOf);
                                    } else {
                                        ((SearchOrdersFiltersActivity) activity).updateMasterData(masterDataList, masterDataOf);
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                    ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                                else  if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                    ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                                else
                                ((SearchOrdersFiltersActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCascadingMasterDataReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void searchOrders(final String encryptedClientKey, boolean isDSUser, String dtTicks, SearchOrdersFilter filter, int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String searchOrdersReqURL = String.format("%s/%s?pageNumber=%d&pageSize=%d&checkCreditBalance=false&markNew=-1&isDSUser=%b&type=%d&dtTicks=%s", Constants.MAIN_URL, Constants.GET_ALL_ORDERS, pageNumber, Constants.ORDERS_PAGE_SIZE_INT, isDSUser, filter.getAmountTypeValue(), dtTicks);

                JSONObject reqObject = new JSONObject();
                reqObject.put("EmployeeID", Integer.parseInt(filter.getSalesRepId()));
                reqObject.put("ProductTypeID", filter.getProductTypeValue());
                reqObject.put("BusinessUnitID", filter.getBusinessUnitId().equals("0") ? -1 : Integer.parseInt(filter.getBusinessUnitId()));
                reqObject.put("ProductID", filter.getProductId().equals("0") ? -1 : Integer.parseInt(filter.getProductId()));
                reqObject.put("MagazineID", filter.getMagazineId().equals("0") ? -1 : Integer.parseInt(filter.getMagazineId()));
                reqObject.put("RateCardId", Integer.parseInt(filter.getRateCardId()));
                reqObject.put("IssueYear", Integer.parseInt(filter.getIssueYearId()));
                reqObject.put("IssueID", Integer.parseInt(filter.getIssueId()));
                reqObject.put("StrFromDate", filter.getFromDate());
                reqObject.put("StrToDate", filter.getToDate());

                JsonObjectRequest searchOrdersReq = new JsonObjectRequest(Request.Method.POST, searchOrdersReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        int noOfOrders = response.getInt("Count");

                                        Type ordersListType = new TypeToken<ArrayList<Order>>() {
                                        }.getType();

                                        List<Order> searchResultOrdersList = new Gson().fromJson(response.getString("ListData"), ordersListType);

                                        ((OrdersActivity) activity).updateOrders(searchResultOrdersList, noOfOrders);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((OrdersActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            ((OrdersActivity) activity).updateOrders(null, 0);
                                        } else {
                                            ((OrdersActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((OrdersActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(searchOrdersReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
