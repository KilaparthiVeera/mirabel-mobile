package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.AddOpportunityActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateCalendarEventActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewActivityForContactScreen;
import com.mirabeltechnologies.magazinemanager.activities.CreateContactActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewTaskActivity;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.MyContactsActivity;
import com.mirabeltechnologies.magazinemanager.activities.OpportunitiesActivity;
import com.mirabeltechnologies.magazinemanager.activities.OpportunitiesSearchActivity;
import com.mirabeltechnologies.magazinemanager.activities.SearchActivitiesFiltersScreen;
import com.mirabeltechnologies.magazinemanager.activities.SearchOrdersFiltersActivity;
import com.mirabeltechnologies.magazinemanager.activities.SearchTaskListFiltersActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.beans.AssignRepData;
import com.mirabeltechnologies.magazinemanager.beans.CampaignSource;
import com.mirabeltechnologies.magazinemanager.beans.CreateOpportunity;
import com.mirabeltechnologies.magazinemanager.beans.EmployeeData;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.OppLossReasonDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppTypeDetails;
import com.mirabeltechnologies.magazinemanager.beans.OpportunitiesSearchFilter;
import com.mirabeltechnologies.magazinemanager.beans.Opportunity;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityAddress;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityResult;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.ReqFields;
import com.mirabeltechnologies.magazinemanager.beans.SelectOpportunity;
import com.mirabeltechnologies.magazinemanager.beans.SubContactDetails;
import com.mirabeltechnologies.magazinemanager.beans.WebsiteConfig;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.DELETE;
import static com.android.volley.Request.Method.POST;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_CITY;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_COUNTRY;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_COUNTY;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_PRODUCT;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_RESON;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_SOURCE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_STAGE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_STATE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ADMIN_TYPE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.CONTACT_OPPORTUNITY;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.GET_OPPORTUNITY_URL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.GET_SAVE_OPPORTUNITY_URL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.New_MAIN_URL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.OPPORTUNITY_CONTACT;

/**
 * Created by venkat on 11/6/17.
 */

public class CommonModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;
    String message="No Records Found";
    List<OpportunityAddress> opportunityAddresses;

    public CommonModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void getAllRepsData(final String encryptedClientKey, String dtTicks, boolean isContactEdit, long contactRepId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getAllRepsDataReqURL = String.format("%s/%s?dtTicks=%s&ContactEdit=%b&customerRepId=%d", Constants.MAIN_URL, Constants.GET_MASTER_DATA_REP, dtTicks, isContactEdit, contactRepId);
                Log.e("veera",getAllRepsDataReqURL);
                Log.e("veera",encryptedClientKey);

                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getAllRepsDataReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {

                                        Type repsDataListType = new TypeToken<ArrayList<RepData>>() {
                                        }.getType();

                                        List<RepData> repsList = new Gson().fromJson(response.getString("Reps"), repsDataListType);

                                        int referenceId = 0;

                                        // When we are getting reps list, in create new activity for contact screen only we will get ReferenceId in response.
                                        if (response.has("ReferenceId"))
                                            referenceId = response.getInt("ReferenceId");

                                        if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                            ((MyContactsActivity) activity).updateAllRepsData(repsList);

                                        else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).updateAllRepsData(repsList, referenceId);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                            ((MyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                            ((MyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                    ((MyContactsActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                    ((CreateContactActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                    ((CreateNewActivityForContactScreen) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getRepsListWithRepSecurity(final String encryptedClientKey, String dtTicks, boolean applyRepSecurity) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getRepsListWithRepSecurityReqURL = String.format("%s/%s?dtTicks=%s&applyRepSecurity=%b", Constants.MAIN_URL, Constants.GET_REPS, dtTicks, applyRepSecurity);

                Log.e("veera",getRepsListWithRepSecurityReqURL);
                JsonObjectRequest getRepsListWithRepSecurityReq = new JsonObjectRequest(Request.Method.GET, getRepsListWithRepSecurityReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        Type repsDataListType = new TypeToken<ArrayList<RepData>>() {
                                        }.getType();
                                        Type DataListType = new TypeToken<ArrayList<AssignRepData>>() {
                                        }.getType();

                                        List<RepData> repsList = new Gson().fromJson(response.getString("Reps"), repsDataListType);
                                        List<AssignRepData> repDataList = new Gson().fromJson(response.getString("Reps"), DataListType);

                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                            ((SearchActivitiesFiltersScreen) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                            ((SearchTaskListFiltersActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                            ((CreateNewTaskActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE)
                                            ((SearchOrdersFiltersActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                            ((AddOpportunityActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                            ((OpportunitiesActivity) activity).updateAllRepsData(repsList);
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                            ((OpportunitiesSearchActivity) activity).updateAllCreateRepsData(repsList);
                                           ((OpportunitiesSearchActivity) activity).updateAllAssignRepsData(repDataList);

                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                            ((SearchActivitiesFiltersScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                            ((SearchTaskListFiltersActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                            ((CreateNewTaskActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE)
                                            ((SearchOrdersFiltersActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                            ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                            ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                            ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                            ((SearchActivitiesFiltersScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                            ((SearchTaskListFiltersActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                            ((CreateNewTaskActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE)
                                            ((SearchOrdersFiltersActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                            ((AddOpportunityActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                            ((OpportunitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                            ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));


                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                    ((SearchActivitiesFiltersScreen) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                    ((SearchTaskListFiltersActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                    ((CreateNewTaskActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE)
                                    ((SearchOrdersFiltersActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                    ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                    ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                    ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);

                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getRepsListWithRepSecurityReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getMasterData(final String encryptedClientKey, int clientId, int repId, final String fieldType) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getMasterDataReqURL = String.format("%s/%s?Clientid=%d&repId=%d&masterDataOf=%s", Constants.MAIN_URL, Constants.GET_MASTERS_DATA, clientId, repId, fieldType);
               Log.e("veera",getMasterDataReqURL);
                Log.e("veera",encryptedClientKey);

                JsonObjectRequest getMasterDataReq = new JsonObjectRequest(Request.Method.GET, getMasterDataReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (fieldType.equals(Constants.GET_MASTER_DATA_REQ_FIELDS)) {
                                            Type reqFieldsListType = new TypeToken<ArrayList<ReqFields>>() {
                                            }.getType();

                                            List<ReqFields> reqFieldsList = new Gson().fromJson(response.getString("reqfields"), reqFieldsListType);

                                            if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                                ((CreateContactActivity) activity).updateReqFieldsArrayList(reqFieldsList);

                                        } else if (fieldType.equals(Constants.GET_MASTER_DATA_EMPLOYEE)) {
                                            EmployeeData employeeData = new Gson().fromJson(response.getString("employee"), EmployeeData.class);

                                            if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                                ((CreateNewActivityForContactScreen) activity).updateEmployeeData(employeeData);
                                            else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                                ((ContactDetailsActivity) activity).updateEmployeeData(employeeData);
                                        } else if (fieldType.equals(Constants.GET_MASTER_DATA_WEBSITE_CONFIG)) {
                                            WebsiteConfig websiteConfig = new Gson().fromJson(response.getString("config"), WebsiteConfig.class);

                                            ((DashboardActivity) activity).updateWebsiteConfigData(websiteConfig);
                                        } else {
                                            Type masterDataListType = new TypeToken<ArrayList<MasterData>>() {
                                            }.getType();

                                            JSONObject responseMasterData = response.getJSONArray("Master").getJSONObject(0);
                                             List<MasterData> masterDataList = new Gson().fromJson(responseMasterData.getString("Value"), masterDataListType);

                                            if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                                ((CreateContactActivity) activity).updateMasterData(masterDataList, fieldType);
                                            else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                                ((CreateNewActivityForContactScreen) activity).updateMasterData(masterDataList, fieldType);
                                            else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                                ((SearchActivitiesFiltersScreen) activity).updateMasterData(masterDataList, fieldType);
                                            else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                                ((SearchTaskListFiltersActivity) activity).updateMasterData(masterDataList, fieldType);
                                            else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                                ((CreateNewTaskActivity) activity).updateMasterData(masterDataList, fieldType);
                                            else if (requestFrom == Constants.RequestFrom.ADD_CALENDAR_EVENT)
                                                ((CreateCalendarEventActivity) activity).updateMasterData(masterDataList, fieldType);
                                        }
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){


                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                            ((SearchActivitiesFiltersScreen) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactDetailsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                            ((SearchTaskListFiltersActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                            ((CreateNewTaskActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.ADD_CALENDAR_EVENT)
                                            ((CreateCalendarEventActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                            ((DashboardActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                            ((CreateNewActivityForContactScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                            ((SearchActivitiesFiltersScreen) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactDetailsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                            ((SearchTaskListFiltersActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                            ((CreateNewTaskActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.ADD_CALENDAR_EVENT)
                                            ((CreateCalendarEventActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                            ((DashboardActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                    ((CreateContactActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE)
                                    ((CreateNewActivityForContactScreen) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE)
                                    ((SearchActivitiesFiltersScreen) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                    ((ContactDetailsActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE)
                                    ((SearchTaskListFiltersActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE)
                                    ((CreateNewTaskActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.ADD_CALENDAR_EVENT)
                                    ((CreateCalendarEventActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                    ((DashboardActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                         headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getMasterDataReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getLocationDetailsForAddress(String address) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getLocationDetailsReqURL = String.format(Constants.GOOGLE_MAPS_URL, Constants.GOOGLE_API_KEY, address);

                Log.e("veera",getLocationDetailsReqURL);
                JsonObjectRequest getLocationDetailsReq = new JsonObjectRequest(Request.Method.GET, getLocationDetailsReqURL, null,

                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("OK")) {

                                        JSONArray resultsArray = response.getJSONArray("results");

                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).updateUIWithAddressDetails(resultsArray);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CreateContactActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).addressNotFoundForZipCode();
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                    ((CreateContactActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                );

                AppController.getInstance(context).addToRequestQueue(getLocationDetailsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSelectOpportunity(String encryptedId, String domain, int userId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getOpportunitySelect= New_MAIN_URL+"/crm/contacts/search/SavedSearchesList/22"+"/"+userId+"/"+"1";
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type repsDataListType = new TypeToken<ArrayList<SelectOpportunity>>() {
                                    }.getType();

                                    List<SelectOpportunity> selectOpportunities = new Gson().fromJson(contentObject.getString("List"), repsDataListType);
                                    if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                        if(selectOpportunities!=null && !selectOpportunities.isEmpty()){
                                            ((OpportunitiesActivity) activity).updateOpportunity(selectOpportunities);
                                        }
                                    else {
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                        }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((OpportunitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);
                })
                {
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getAllMyOpportunities(String encryptedId, String domain, OpportunitiesSearchFilter searchFilter, int currentPageNumber, int opportunityPageSizeInt) {

        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                searchFilter.setCurPage(currentPageNumber);
                searchFilter.setPageSize(opportunityPageSizeInt);
                String jsonInString = new Gson().toJson(searchFilter);
                JSONObject reqObject = new JSONObject();
              //  JSONObject reqObject = new JSONObject(jsonInString);
               // Log.e("veera",reqObject.toString());

                JSONObject addObject = new JSONObject();
                addObject.put("Email",searchFilter.getAdvSearch().getEmail());
                addObject.put("Phone",searchFilter.getAdvSearch().getPhone());
                addObject.put("City",searchFilter.getAdvSearch().getCity());
                addObject.put("State",searchFilter.getAdvSearch().getState());
                addObject.put("County",searchFilter.getAdvSearch().getCounty());
                addObject.put("Country",searchFilter.getAdvSearch().getCountry());
                addObject.put("Zip",searchFilter.getAdvSearch().getZip());
                reqObject.put("CustomerID",searchFilter.getCustomerID());
                reqObject.put("CustomerName",searchFilter.getCustomerName());
                reqObject.put("OppName",searchFilter.getOppName());
                reqObject.put("Type",searchFilter.getType());
                reqObject.put("BusinessUnit",searchFilter.getBusinessUnit());
                reqObject.put("Source",searchFilter.getSource());
                reqObject.put("Products",searchFilter.getProducts());
                reqObject.put("LossReason",searchFilter.getLossReason());
                reqObject.put("Arth","");
                reqObject.put("CloseFrom",searchFilter.getCloseFrom());
                reqObject.put("CloseTo",searchFilter.getCloseTo());
                reqObject.put("CreatedBy",searchFilter.getCreatedBy());
                reqObject.put("CreatedFrom",searchFilter.getCreatedFrom());
                reqObject.put("CloseFrom",searchFilter.getCloseFrom());
                reqObject.put("Status",searchFilter.getStatus());
                reqObject.put("SortBy","");
                reqObject.put("ViewType",0);
                reqObject.put("AssignedTo", searchFilter.getAssignedTo());
                reqObject.put("UserID", searchFilter.getUserID());
                reqObject.put("Probability", searchFilter.getProbability());
                reqObject.put("ListId", searchFilter.getListId());
                reqObject.put("PageSize", searchFilter.getPageSize());
                reqObject.put("CurPage", searchFilter.getCurPage());
                reqObject.put("AdvSearch", addObject);
                reqObject.put("Action",null);

                Log.e("veera",GET_OPPORTUNITY_URL);
                Log.e("veera",reqObject.toString());

                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(POST, GET_OPPORTUNITY_URL, reqObject,
                        response -> {
                            try {
                                if (MMProgressDialog.isProgressDialogShown)
                                    MMProgressDialog.hideProgressDialog();
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    JSONObject dataObject = contentObject.getJSONObject("Data");
                                    int noOfRecentContacts = dataObject.getInt("Total");
                                    Type opportunitiesType = new TypeToken<ArrayList<Opportunity>>() {
                                    }.getType();
                                    List<Opportunity> selectOpportunities = new Gson().fromJson(dataObject.getString("Opportunities"), opportunitiesType);

                                    Type OpportunityResultType = new TypeToken<ArrayList<OpportunityResult>>() {
                                    }.getType();
                                    List<OpportunityResult> OpportunityResult = new Gson().fromJson(dataObject.getString("OpportunityResult"), OpportunityResultType);


                                    ((OpportunitiesActivity) activity).updateGetOpportunityList(selectOpportunities,noOfRecentContacts,OpportunityResult);
                                    Log.e("veera",""+noOfRecentContacts);


                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                        ((OpportunitiesActivity) activity).updateGetOpportunityList(null,0,null);

                                    } else {

                                        ((OpportunitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllStageUnits(String encryptedId, String domain) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getOpportunitySelect= New_MAIN_URL+ADMIN_STAGE;
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type repsDataListType = new TypeToken<ArrayList<OppStageDetails>>() {
                                    }.getType();

                                    List<OppStageDetails> stageDetails = new Gson().fromJson(contentObject.getString("List"), repsDataListType);
                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY){
                                        if(stageDetails!=null && !stageDetails.isEmpty()){
                                            ((AddOpportunityActivity) activity).updateStageDetails(stageDetails);
                                        }
                                        else {
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                        }
                                    }else  if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY){
                                        if(stageDetails!=null && !stageDetails.isEmpty()){
                                            ((OpportunitiesSearchActivity) activity).updateStageDetails(stageDetails);
                                        }
                                        else {
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                        }
                                    }
                                    else if (requestFrom == Constants.RequestFrom.OPPORTUNITY){
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                        if(stageDetails!=null && !stageDetails.isEmpty()){
                                            ((OpportunitiesActivity) activity).updateStageDetails(stageDetails);
                                        }
                                        else {
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                       if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                        ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                        ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        ((AddOpportunityActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                        ((OpportunitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                        ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                            else if (requestFrom == Constants.RequestFrom.OPPORTUNITY)
                                ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);
                            else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getAllTypeAndReason(String encryptedId, String domain, String getAdminDataType) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getOpportunitySelect;
                if(getAdminDataType.equals(Constants.GET_ADMIN_DATA_TYPE)){
                     getOpportunitySelect= New_MAIN_URL+ADMIN_TYPE;
                }
                else {
                     getOpportunitySelect= New_MAIN_URL+ADMIN_RESON;
                }
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    if(getAdminDataType.equals(Constants.GET_ADMIN_DATA_TYPE)){

                                        Type repsDataListType = new TypeToken<ArrayList<OppTypeDetails>>() {
                                        }.getType();

                                        List<OppTypeDetails> stageDetails = new Gson().fromJson(contentObject.getString("List"), repsDataListType);
                                        if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY){
                                            if(stageDetails!=null && !stageDetails.isEmpty()){
                                                ((AddOpportunityActivity) activity).updateTypeDetails(stageDetails);
                                            }
                                            else {
                                                if (MMProgressDialog.isProgressDialogShown)
                                                    MMProgressDialog.hideProgressDialog();
                                                MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                            }
                                        }
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY){
                                            if(stageDetails!=null && !stageDetails.isEmpty()){
                                                ((OpportunitiesSearchActivity) activity).updateTypeDetails(stageDetails);
                                            }
                                            else {
                                                if (MMProgressDialog.isProgressDialogShown)
                                                    MMProgressDialog.hideProgressDialog();
                                                MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                            }
                                        }
                                    }

                                    else {
                                        Type repsDataListType = new TypeToken<ArrayList<OppLossReasonDetails>>() {
                                        }.getType();

                                        List<OppLossReasonDetails> stageDetails = new Gson().fromJson(contentObject.getString("List"), repsDataListType);
                                        if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY){
                                            if(stageDetails!=null && !stageDetails.isEmpty()){
                                                ((AddOpportunityActivity) activity).updateLossReason(stageDetails);
                                            }
                                            else {
                                                if (MMProgressDialog.isProgressDialogShown)
                                                    MMProgressDialog.hideProgressDialog();
                                                MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                            }
                                        }
                                        else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY){
                                            if(stageDetails!=null && !stageDetails.isEmpty()){
                                                ((OpportunitiesSearchActivity) activity).updateLossReason(stageDetails);
                                            }
                                            else {
                                                if (MMProgressDialog.isProgressDialogShown)
                                                    MMProgressDialog.hideProgressDialog();
                                                MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                            }
                                        }

                                    }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                        ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        ((AddOpportunityActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                        ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                            else if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY)
                                ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getAllSubContacts(String encryptedId, String domain, String company_id) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getOpportunitySelect= New_MAIN_URL+OPPORTUNITY_CONTACT+company_id;
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type repsDataListType = new TypeToken<ArrayList<SubContactDetails>>() {
                                    }.getType();

                                    List<SubContactDetails> subContactDetails = new Gson().fromJson(contentObject.getString("List"), repsDataListType);
                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        if(subContactDetails!=null && !subContactDetails.isEmpty()){
                                            ((AddOpportunityActivity) activity).updateSubContactDetails(subContactDetails,company_id);
                                        }
                                        else {
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                        }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {

                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((AddOpportunityActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                            ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }


    }

    public void saveOpportunity(String encryptedId, String domain, CreateOpportunity createOpportunity) {

        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String jsonInString = new Gson().toJson(createOpportunity);
                JSONObject mJSONObject = new JSONObject(jsonInString);
                Log.e("veera",GET_SAVE_OPPORTUNITY_URL);
                Log.e("veera",mJSONObject.toString());
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(POST, GET_SAVE_OPPORTUNITY_URL, mJSONObject,
                        response -> {
                            try {
                                if (MMProgressDialog.isProgressDialogShown)
                                    MMProgressDialog.hideProgressDialog();
                                Log.e("veera",response.toString() );
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    int value = contentObject.getInt("Value");
                                    ((AddOpportunityActivity) activity).updateOpportunity(value);
                                }
                                else {
                                    ((AddOpportunityActivity) activity).showMMErrorResponse(contentObject.getString(Constants.MM_ERROR_MESSAGE));
                                    if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                        ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);

                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void deleteOpportunity(String encryptedId, String domain, Integer id, Integer userId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String deleteUrl=GET_SAVE_OPPORTUNITY_URL+"/"+id+"/"+userId;
                JSONObject mJSONObject = new JSONObject();
                Log.e("veera",deleteUrl);
                Log.e("veera",mJSONObject.toString());
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(DELETE, deleteUrl, mJSONObject,
                        response -> {
                            try {
                                if (MMProgressDialog.isProgressDialogShown)
                                    MMProgressDialog.hideProgressDialog();
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    int value = contentObject.getInt("Value");
                                    ((OpportunitiesActivity) activity).updateDeleteOpportunity();
                                }
                                else {
                                    ((OpportunitiesActivity) activity).showMMErrorResponse(contentObject.getString(Constants.MM_ERROR_MESSAGE));
                                    if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                        ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);

                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void getSelectOpportunity(String encryptedId, String domain, String opportunity_id) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getOpportunitySelect=GET_SAVE_OPPORTUNITY_URL+"/"+opportunity_id;
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {
                                    Type repsDataListType = new TypeToken<Opportunity>() {
                                    }.getType();
                                    Opportunity selectOpportunity = new Gson().fromJson(contentObject.getString("Data"), repsDataListType);
                                    if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY)
                                        if(selectOpportunity!=null ){
                                            ((AddOpportunityActivity) activity).updateSelectOpportunity(selectOpportunity);
                                        }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((AddOpportunityActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {

                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((AddOpportunityActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                            ((AddOpportunityActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getContactOpportunities(String encryptedId, String domain, String company_id) {
        try {

            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getOpportunitySelect= New_MAIN_URL+CONTACT_OPPORTUNITY+company_id;
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type opportunitiesType = new TypeToken<ArrayList<Opportunity>>() {
                                    }.getType();

                                    List<Opportunity> selectOpportunities = new Gson().fromJson(contentObject.getString("List"), opportunitiesType);

                                    ((OpportunitiesActivity) activity).updateContactOpportunityList(selectOpportunities);
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                        ((OpportunitiesActivity) activity).updateContactOpportunityList(null);

                                    } else {
                                        ((OpportunitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                            ((OpportunitiesActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void getAllSources(String encryptedId, String domain) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getOpportunitySelect= New_MAIN_URL+ADMIN_SOURCE;
                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type opportunitiesType = new TypeToken<ArrayList<CampaignSource>>() {
                                    }.getType();
                                    List<CampaignSource>  campaignSourceList = new Gson().fromJson(contentObject.getString("List"), opportunitiesType);
                                        if(campaignSourceList!=null && !campaignSourceList.isEmpty()){
                                            ((OpportunitiesSearchActivity) activity).updateSourceList(campaignSourceList);
                                        }
                                        else {
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                        }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                            ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                        }) {
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }


    }
    public void getAllOpportunitiesAddress(String encryptedId, String domain, String type) {
        try {

            String getOpportunitySelect = "";
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                switch (type) {
                    case "city":
                        getOpportunitySelect = New_MAIN_URL + ADMIN_CITY;
                        break;
                    case "county":
                        getOpportunitySelect = New_MAIN_URL + ADMIN_COUNTY;
                        break;
                    case "state":
                        getOpportunitySelect = New_MAIN_URL + ADMIN_STATE;
                        break;
                    case "country":
                        getOpportunitySelect = New_MAIN_URL + ADMIN_COUNTRY;
                        break;
                }

                Log.e("veera",getOpportunitySelect);
                Log.e("veera",encryptedId);
                JsonObjectRequest getAllRepsDataReq = new JsonObjectRequest(Request.Method.GET, getOpportunitySelect, null,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {

                                    JSONObject dataObject = contentObject.getJSONObject("Data");
                                    Type opportunitiesType = new TypeToken<ArrayList<OpportunityAddress>>() {
                                    }.getType();
                                    switch (type) {
                                        case "city":
                                            opportunityAddresses = new Gson().fromJson(dataObject.getString("ContactCities"), opportunitiesType);
                                            break;
                                        case "county":
                                            opportunityAddresses = new Gson().fromJson(dataObject.getString("ContactCounties"), opportunitiesType);
                                            break;
                                        case "state":
                                            opportunityAddresses = new Gson().fromJson(dataObject.getString("ContactStates"), opportunitiesType);
                                            break;
                                        case "country":
                                            opportunityAddresses = new Gson().fromJson(dataObject.getString("ContactCountries"), opportunitiesType);
                                            break;
                                    }

                                    if(opportunityAddresses!=null && !opportunityAddresses.isEmpty()){
                                        ((OpportunitiesSearchActivity) activity).updateOpportunityAddressList(opportunityAddresses,type);
                                    }
                                    else {
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                        MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                    }


                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {

                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {

                            ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {

                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllRepsDataReq, TAG);
            }

        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    public void getAllProducts(String encryptedId, String domain, String select_businessId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                JSONObject reqObject = new JSONObject();
                reqObject.put("BusinessUnitIDs",select_businessId);
                reqObject.put("IsApplySecurity",true);
                reqObject.put("RateCardIDs","-1");
                reqObject.put("ScheduleIDs","-1");
                reqObject.put("SubProductTypeIDs",-1);
                Log.e("veera",reqObject.toString());
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(POST, New_MAIN_URL+ADMIN_PRODUCT, reqObject,
                        response -> {
                            try {
                                JSONObject contentObject = response.getJSONObject("content");

                                if (contentObject.getString("Status").equals("Success")) {

                                    Type opportunitiesType = new TypeToken<ArrayList<MasterData>>() {
                                    }.getType();
                                    List<MasterData>  masterDataList = new Gson().fromJson(contentObject.getString("List"), opportunitiesType);
                                      Log.e("veera",""+masterDataList.toString());
                                    if(masterDataList!=null && !masterDataList.isEmpty()){
                                        ((OpportunitiesSearchActivity) activity).updateProducts(masterDataList);
                                    }
                                    else {
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                        MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_alert), message);
                                    }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((OpportunitiesSearchActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (MMProgressDialog.isProgressDialogShown)
                                        MMProgressDialog.hideProgressDialog();
                                    ((OpportunitiesSearchActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            }
                            catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((OpportunitiesSearchActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedId);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
