package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.CallbacksActivity;
import com.mirabeltechnologies.magazinemanager.activities.CheckInHistoryActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateContactActivity;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.MyContactsActivity;
import com.mirabeltechnologies.magazinemanager.activities.NearbyContactsActivity;
import com.mirabeltechnologies.magazinemanager.activities.RepContactsActivity;
import com.mirabeltechnologies.magazinemanager.activities.SaveListActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactActivitiesActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactAddressesActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactOrderItemsListActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactSubContactsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.CheckInHistoryAdopter;
import com.mirabeltechnologies.magazinemanager.beans.ActivityDetails;
import com.mirabeltechnologies.magazinemanager.beans.CallbackData;
import com.mirabeltechnologies.magazinemanager.beans.CheckInSubmitResponse;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.ContactData;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrder;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrderItem;
import com.mirabeltechnologies.magazinemanager.beans.ContactSearchFilter;
import com.mirabeltechnologies.magazinemanager.beans.CustomerAddress;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.beans.NewContact;
import com.mirabeltechnologies.magazinemanager.beans.SaveList;
import com.mirabeltechnologies.magazinemanager.beans.SimilarContactsResponse;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.POST;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.GET_NEARBY_CONTACTS_URL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.GET_SAVE_CONTACTS_URL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SIMILAR_CONTACTS_URL;

/**
 * Created by venkat on 10/11/17.
 */

public class ContactsModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;

    public ContactsModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void getCustomerBasicDetails(final String encryptedClientKey, String dtTicks, String customerId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getCustomerBasicDetailsReqURL = String.format("%s/%s?customerID=%s&dtTicks=%s", Constants.MAIN_URL, Constants.GET_CUSTOMER_BASIC_DETAILS, customerId, dtTicks);

                Log.e("veera",getCustomerBasicDetailsReqURL);
                Log.e("veera",encryptedClientKey);

                JsonObjectRequest getCustomerBasicDetailsReq = new JsonObjectRequest(Request.Method.GET, getCustomerBasicDetailsReqURL, null,
                        response -> {

                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    DetailContact contact = new Gson().fromJson(response.getString("Customer"), DetailContact.class);

                                    ContactData contactSubContactsData = new Gson().fromJson(response.getString("data"), ContactData.class);
                                    contact.setContactData(contactSubContactsData);

                                    Type contactOrderListType = new TypeToken<ArrayList<ContactOrder>>() {
                                    }.getType();

                                    List<ContactOrder> contactOrderList = new Gson().fromJson(response.getString("orders"), contactOrderListType);
                                    contact.setContactOrders(contactOrderList);

                                    if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                        ((ContactDetailsActivity) activity).updateCustomerBasicDetails(contact);
                                    else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                        ((DashboardActivity) activity).updateCustomerBasicDetails(contact);
                                    else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)
                                        ((NearbyContactsActivity) activity).updateCustomerBasicDetails(contact);
                                    else if (requestFrom == Constants.RequestFrom.SAVE_LIST)
                                        ((SaveListActivity) activity).updateCustomerBasicDetails(contact);

                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                        ((ContactDetailsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                        ((DashboardActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)
                                        ((NearbyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                }

                                else {
                                    if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                        ((ContactDetailsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    else if (requestFrom == Constants.RequestFrom.DASHBOARD)
                                        ((DashboardActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    else if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)
                                        ((NearbyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((ContactDetailsActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCustomerBasicDetailsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSubContactsForCustomerId(final String encryptedClientKey, String dtTicks, String customerId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getSubContactsReqURL = String.format("%s/%s?customerID=%s&dtTicks=%s", Constants.MAIN_URL, Constants.GET_CUSTOMER_SUB_CONTACTS, customerId, dtTicks);

                JsonObjectRequest getSubContactsReq = new JsonObjectRequest(Request.Method.GET, getSubContactsReqURL, null,
                        response -> {
                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    Type subContactsListType = new TypeToken<ArrayList<Contact>>() {
                                    }.getType();

                                    List<Contact> subContactsList = new Gson().fromJson(response.getString("SubContacts"), subContactsListType);

                                    if (requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE)
                                        ((ContactSubContactsActivity) activity).updateSubContactsList(subContactsList);
                                    else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                        ((CreateContactActivity) activity).updateSubContactsList(subContactsList);
                                } else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    if (requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE)
                                        ((ContactSubContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                        ((CreateContactActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                        if (requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE)
                                            ((ContactSubContactsActivity) activity).updateSubContactsList(null);
                                        else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).updateSubContactsList(null);
                                    } else {
                                        if (requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE)
                                            ((ContactSubContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((ContactSubContactsActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getSubContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCustomerActivities(final String encryptedClientKey, String dtTicks, String customerId, String activityType) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getCustomerActivitiesReqURL = String.format("%s/%s?customerID=%s&activityType=%s&dtTicks=%s", Constants.MAIN_URL, Constants.GET_CUSTOMER_ACTIVITIES, customerId, activityType, dtTicks);
                Log.e("veera",getCustomerActivitiesReqURL);
                Log.e("veera",encryptedClientKey);

                JsonObjectRequest getCustomerActivitiesReq = new JsonObjectRequest(Request.Method.GET, getCustomerActivitiesReqURL, null,
                        response -> {
                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    Type activitiesListType = new TypeToken<ArrayList<ActivityDetails>>() {
                                    }.getType();

                                    List<ActivityDetails> activitiesList = new Gson().fromJson(response.getString("ActivityDetails"), activitiesListType);
                                    ((ContactActivitiesActivity) activity).updateCustomerActivities(activitiesList);
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    ((ContactActivitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                        ((ContactActivitiesActivity) activity).updateCustomerActivities(null);
                                    else
                                        ((ContactActivitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((ContactActivitiesActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCustomerActivitiesReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCustomerAddresses(final String encryptedClientKey, String dtTicks, String customerId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getCustomerAddressesReqURL = String.format("%s/%s?customerID=%s&dtTicks=%s", Constants.MAIN_URL, Constants.GET_CUSTOMER_ADDRESSES, customerId, dtTicks);

                JsonObjectRequest getCustomerAddressesReq = new JsonObjectRequest(Request.Method.GET, getCustomerAddressesReqURL, null,
                        response -> {
                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    Type addressesListType = new TypeToken<ArrayList<CustomerAddress>>() {
                                    }.getType();

                                    List<CustomerAddress> addressesList = new Gson().fromJson(response.getString("Addresses"), addressesListType);

                                    List<CustomerAddress> finalAddressesList = new ArrayList<>();
                                    for (CustomerAddress customerAddress : addressesList) {
                                        if (!customerAddress.getAddressType().contains("Empty"))
                                            finalAddressesList.add(customerAddress);
                                    }

                                    ((ContactAddressesActivity) activity).updateCustomerAddresses(finalAddressesList);
                                } else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    ((ContactAddressesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                        ((ContactAddressesActivity) activity).updateCustomerAddresses(null);
                                    else
                                        ((ContactAddressesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((ContactAddressesActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCustomerAddressesReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllMyContacts(final String encryptedClientKey, String dtTicks, final int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.PAGE_SIZE_INT;

                String getAllMyContactsReqURL = String.format("%s/%s?contacttype=ALL&startindex=%d&lastindex=%d&dtticks=%s", Constants.MAIN_URL, Constants.GET_RECENT_OR_ALL_CONTACTS, startIndex, endIndex, dtTicks);
                Log.e("veera",getAllMyContactsReqURL);
                JsonObjectRequest getAllMyContactsReq = new JsonObjectRequest(Request.Method.GET, getAllMyContactsReqURL, null,
                        response -> {
                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    int noOfAllMyContacts = response.getInt("Count");

                                    Type myContactsListType = new TypeToken<ArrayList<Contact>>() {
                                    }.getType();

                                    List<Contact> myContactsList = new Gson().fromJson(response.getString("Contacts"), myContactsListType);
                                    ((MyContactsActivity) activity).updateContactsList(myContactsList, noOfAllMyContacts);

                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((MyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                        ((MyContactsActivity) activity).updateContactsList(null, 0);
                                    else
                                        ((MyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((MyContactsActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        Log.e("veera",headers.toString());

                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllMyContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAllSaveList(String encryptedClientKey, String domain, int currentPageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {

                String getAllMySaveListReqURL = Constants.GET_SAVE_LIST_URL;

                Log.e("veera",getAllMySaveListReqURL);

                JsonObjectRequest getAllMyContactsReq = new JsonObjectRequest(Request.Method.GET, getAllMySaveListReqURL, null,
                        response -> {
                            Log.e("veera",response.toString());
                            try {
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    Type saveListType = new TypeToken<ArrayList<SaveList>>() {
                                    }.getType();
                                    JSONObject dataObject = contentObject.getJSONObject("Data");

                                    List<SaveList> saveContactsList = new Gson().fromJson(dataObject.getString("MyLists"), saveListType);

                                    if(saveContactsList!=null && !saveContactsList.isEmpty()){
                                        int noOfAllMyContacts = saveContactsList.size();
                                        Log.e("veera",saveContactsList.toString());
                                        ((MyContactsActivity) activity).updateSaveList(saveContactsList, noOfAllMyContacts);

                                    }
                                    else {
                                        ((MyContactsActivity) activity).updateSaveList(null, 0);
                                        Toast.makeText((MyContactsActivity) activity, "No List Found", Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    ((MyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                        ((MyContactsActivity) activity).updateSaveList(null, 0);
                                    else
                                        ((MyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((MyContactsActivity) activity).onVolleyErrorResponse(error);

                           // ((NearbyContactsActivity) activity).closeSmallProcessBar();
                        }
                )  {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", encryptedClientKey);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        Log.e("chinna",headers.toString());

                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAllMyContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void getAdvancedSearchContacts(final String encryptedClientKey, String dtTicks, ContactSearchFilter filter, final int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.PAGE_SIZE_INT;

                String getAdvancedSearchContactsReqURL = String.format("%s/%s?customer=%s&name=%s&phone=%s&email=%s&selectedRepId=%s&startindex=%d&endindex=%d&dtticks=%s", Constants.MAIN_URL, Constants.SEARCH_CUSTOMER, URLEncoder.encode(filter.getCompany(), Constants.CHARSET_NAME),
                        URLEncoder.encode(filter.getName(), Constants.CHARSET_NAME), URLEncoder.encode(filter.getPhone(), Constants.CHARSET_NAME), URLEncoder.encode(filter.getEmail(), Constants.CHARSET_NAME), filter.getRepId(), startIndex, endIndex, dtTicks);

                Log.e("veera",getAdvancedSearchContactsReqURL);
                Log.e("veera",encryptedClientKey);
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(Request.Method.GET, getAdvancedSearchContactsReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        int noOfContacts = response.getInt("Count");

                                        Type contactsListType = new TypeToken<ArrayList<Contact>>() {
                                        }.getType();

                                        List<Contact> searchResultContactsList = new Gson().fromJson(response.getString("Contacts"), contactsListType);

                                        if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE)
                                            ((RepContactsActivity) activity).updateRepContactsList(searchResultContactsList, noOfContacts);
                                        else if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)

                                            ((MyContactsActivity) activity).updateContactsList(searchResultContactsList, noOfContacts);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE)
                                            ((RepContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                            ((MyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE)
                                                ((RepContactsActivity) activity).updateRepContactsList(null, 0);
                                            else if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                                ((MyContactsActivity) activity).updateContactsList(null, 0);
                                        } else {
                                            if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE)
                                                ((RepContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                            else if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                                ((MyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        error -> {
                            if (requestFrom == Constants.RequestFrom.REP_CONTACTS_PAGE)
                                ((RepContactsActivity) activity).onVolleyErrorResponse(error);
                            else if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE)
                                ((MyContactsActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void createNewContact(final String encryptedClientKey, NewContact contact, boolean isContactInEditMode, boolean isAddingSubContact) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String createNewContactReqURL = String.format("%s/%s", Constants.MAIN_URL, Constants.CREATE_NEW_CONTACT);

                final JSONObject reqObject = new JSONObject();
                reqObject.put("ClientId", contact.getClientId());
                reqObject.put("CompanyName", contact.getCompanyName());
                reqObject.put("FirstName", contact.getFirstName());
                reqObject.put("LastName", contact.getLastName());
                reqObject.put("UserID", Integer.parseInt(contact.getRepId()));
                reqObject.put("CustomerType", contact.getCategory().isEmpty() ? "MM Android App" : contact.getCategory());
                reqObject.put("Priority", contact.getPriorityId());
                reqObject.put("JobDescription", contact.getContactType().replace("*", ""));
                reqObject.put("Title", contact.getJobTitle());
                reqObject.put("Email", contact.getEmailId());
                reqObject.put("Phone", contact.getPhoneNumber());
                reqObject.put("Address1", contact.getAddress1());
                reqObject.put("Address2", contact.getAddress2());
                reqObject.put("ZipCode", contact.getZipCode());
                reqObject.put("City", contact.getCity());
                reqObject.put("RepID",contact.getRepId());
                reqObject.put("State", contact.getState());
                reqObject.put("County", contact.getCounty());
                reqObject.put("Country", contact.getCountry());
                reqObject.put("WebSiteUrl", contact.getWebsiteURL());
                reqObject.put("FacebookId", contact.getFacebookId());
                reqObject.put("GooglePlus", contact.getGooglePlusURL());
                reqObject.put("LinkedIn", contact.getLinkedInURL());
                reqObject.put("TwitterHandle", contact.getTwitterURL());

                if (isContactInEditMode) {
                    reqObject.put("gsCustomersID", contact.getCustomerId());
                    reqObject.put("Notes", "Customer updated successfully from MM Android App.");
                    if (contact.isUpdateSubContacts()) {
                        reqObject.put("UpdateSubContacts", true);
                    }

                } else {
                    reqObject.put("Notes", "Customer created successfully from MM Android App.");
                }

                if (isAddingSubContact) {
                    reqObject.put("ParentID", contact.getParentId());
                }

                JSONArray requestObjInJSONArrayFormat = new JSONArray();
                requestObjInJSONArrayFormat.put(reqObject);
                Log.e("veera",createNewContactReqURL);
                Log.e("veera",contact.getRepId());
                Log.e("veera","     "+requestObjInJSONArrayFormat.toString());

                JsonArrayRequest createNewContactReq = new JsonArrayRequest(POST, createNewContactReqURL, requestObjInJSONArrayFormat,
                        response -> {

                            if(!response.isNull(0)){
                                Log.e("veera",response.toString());
                                try {
                                    JSONObject responseObject = response.getJSONObject(0);

                                    if (responseObject.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && responseObject.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                                            ((CreateContactActivity) activity).updateContactCreateStatus(responseObject);
                                    }
                                    else  if(responseObject.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CreateContactActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else  if(responseObject.getInt(Constants.MM_ERROR_CODE) == Constants.READ_EXTERNAL_STORAGE_REQUEST_CODE){
                                        MMProgressDialog.hideProgressDialog();
                                        ((CreateContactActivity) activity).showMMErrorResponse(responseObject.getString(Constants.MM_MESSAGE)+" :"+Constants.READ_EXTERNAL_STORAGE_REQUEST_CODE);
                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                                            if (responseObject.getString(Constants.MM_MESSAGE).equals("Duplicate Record Exists"))
                                                ((CreateContactActivity) activity).showMMErrorResponse("Email address already exists.");
                                            else
                                                ((CreateContactActivity) activity).showMMErrorResponse(responseObject.getString(Constants.MM_MESSAGE));
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            else {
                                MMProgressDialog.hideProgressDialog();
                                ((CreateContactActivity) activity).showMMErrorResponse("Something Went Wrong. please Try After Sometime....");
                            }


                        },
                        error -> {
                            ((CreateContactActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(createNewContactReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCustomerOrders(final String encryptedClientKey, int clientId, int customerId, int pageNumber, int year) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.ORDERS_PAGE_SIZE_INT) + 1;
                int limit = 9999;

                String getCustomerOrdersReqURL = String.format("%s/%s?customerId=%s&clientid=%d&startindex=%d&limit=%d&year=%d", Constants.MAIN_URL, Constants.GET_CUSTOMER_ORDERS, customerId, clientId, startIndex, limit, year);

                JsonArrayRequest getCustomerOrdersReq = new JsonArrayRequest(Request.Method.GET, getCustomerOrdersReqURL, null,
                        response -> {
                            try {
                                if (response.length() > 0) {
                                    JSONObject responseObject = response.getJSONObject(0);

                                    int noOfItems = responseObject.getInt("OrdersCount");

                                    Type itemsListType = new TypeToken<ArrayList<ContactOrderItem>>() {
                                    }.getType();

                                    List<ContactOrderItem> itemsList = new Gson().fromJson(response.toString(), itemsListType);
                                    ((ContactOrderItemsListActivity) activity).updateOrderItemsList(itemsList, noOfItems);
                                } else {
                                    ((ContactOrderItemsListActivity) activity).updateOrderItemsList(null, 0);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((ContactOrderItemsListActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCustomerOrdersReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getCustomerCallbacks(final String encryptedClientKey, String dtTicks, String activityType, int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.ACTIVITY_PAGE_SIZE_INT;

                String getCustomerCallbacksReqURL = String.format("%s/%s?customerID=0&activityType=%s&dtTicks=%s&startIndex=%d&endIndex=%d", Constants.MAIN_URL, Constants.GET_CUSTOMER_ACTIVITIES, activityType, dtTicks, startIndex, endIndex);

                JsonObjectRequest getCustomerCallbacksReq = new JsonObjectRequest(Request.Method.GET, getCustomerCallbacksReqURL, null,
                        response -> {
                            try {
                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    int noOfCallbacks = response.getInt("Count");

                                    Type callbacksListType = new TypeToken<ArrayList<CallbackData>>() {
                                    }.getType();

                                    List<CallbackData> callbacksList = new Gson().fromJson(response.getString("Contacts"), callbacksListType);
                                    ((CallbacksActivity) activity).updateCustomerCallbacks(callbacksList, noOfCallbacks);
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    ((CallbacksActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS)
                                        ((CallbacksActivity) activity).updateCustomerCallbacks(null, 0);
                                    else
                                        ((CallbacksActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> ((CallbacksActivity) activity).onVolleyErrorResponse(error)
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getCustomerCallbacksReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //new implementation for fetching contacts nearby
    public void getNearbyContacts(JSONObject jsonObject, final String authToken, final String domain) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                Log.e("veera",GET_NEARBY_CONTACTS_URL);
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(POST, GET_NEARBY_CONTACTS_URL, jsonObject,
                        response -> {

                            Log.e("veera",response.toString());
                            try {
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    Type nearbyContactsListType = new TypeToken<ArrayList<NearbyContacts>>() {
                                    }.getType();
                                    JSONObject dataObject = contentObject.getJSONObject("Data");


                                    List<NearbyContacts> nearByContactsList = new Gson().fromJson(dataObject.getString("Table"), nearbyContactsListType);
                                    List<NearbyContacts> fullnearByContacts= new ArrayList<>();
                                    List<NearbyContacts> halfnearByContacts= new ArrayList<>();
                                    if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)

                                        if(nearByContactsList!=null && !nearByContactsList.isEmpty()){
                                            for (int i=0; i<nearByContactsList.size();i++){

                                                if((nearByContactsList.get(i).getAddress1()!=null && !nearByContactsList.get(i).getAddress1().isEmpty())|| ( nearByContactsList.get(i).getAddress2()!=null && !nearByContactsList.get(i).getAddress2().isEmpty())){
                                                    fullnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else {
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                            }
                                            fullnearByContacts.addAll(halfnearByContacts);
                                            JSONArray jsonArray= dataObject.getJSONArray("Table1");
                                            JSONObject count= jsonArray.getJSONObject(0);
                                            int noOfRecentContacts = count.getInt("Column1");
                                            ((NearbyContactsActivity) activity).updateRepContactsList(fullnearByContacts,noOfRecentContacts);
                                            // ((NearbyContactsActivity) activity).displayLongToast("success");

                                        }
                                        else {
                                            // ((NearbyContactsActivity) activity).displayToast("No Data");
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            ((NearbyContactsActivity) activity).updateIndex();
                                        }
                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                    //response.getString(Constants.MM_ERROR_MESSAGE);
                                    ((NearbyContactsActivity) activity).closeSmallProcessBar();
                                    ((NearbyContactsActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                }
                                else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                        if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)
                                            //  ((NearbyContactsActivity) activity).displayLongToast("No Data");

                                            ((NearbyContactsActivity) activity).updateRepContactsList(null,0);

                                    } else {
                                        if (requestFrom == Constants.RequestFrom.NEARBY_CONTACTS)

                                            ((NearbyContactsActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            Log.e("veera",error.toString());
                            ((NearbyContactsActivity) activity).onVolleyErrorResponse(error);

                            ((NearbyContactsActivity) activity).closeSmallProcessBar();
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", authToken);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                      //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSaveContacts(JSONObject jsonObject,  final String authToken, String domain) {
        Log.e("veera",authToken);
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                JsonObjectRequest getAdvancedSearchContactsReq = new JsonObjectRequest(POST, GET_SAVE_CONTACTS_URL, jsonObject,
                        response -> {
                            Log.e("veera",response.toString());
                            try {
                                JSONObject contentObject = response.getJSONObject("content");
                                if (contentObject.getString("Status").equals("Success")) {
                                    Type nearbyContactsListType = new TypeToken<ArrayList<NearbyContacts>>() {
                                    }.getType();
                                    JSONObject dataObject = contentObject.getJSONObject("Data");
                                    List<NearbyContacts> nearByContactsList = new Gson().fromJson(dataObject.getString("Table"), nearbyContactsListType);
                                    JSONArray jsonArray= dataObject.getJSONArray("Table1");
                                    JSONObject count= jsonArray.getJSONObject(0);
                                    int noOfRecentContacts = count.getInt("Column1");

                                    List<NearbyContacts> fullnearByContacts= new ArrayList<>();
                                    List<NearbyContacts> halfnearByContacts= new ArrayList<>();
                                    List<NearbyContacts> nonearByContacts= new ArrayList<>();

                                    if(nearByContactsList!=null && !nearByContactsList.isEmpty()){
                                            for (int i=0; i<nearByContactsList.size();i++){

                                                if(nearByContactsList.get(i).getAddress1()!=null && !nearByContactsList.get(i).getAddress1().isEmpty()){

                                                    fullnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getAddress2()!=null && !nearByContactsList.get(i).getAddress2().isEmpty()){
                                                    fullnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getCity()!=null && !nearByContactsList.get(i).getCity().isEmpty()){
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getCounty()!=null && !nearByContactsList.get(i).getCounty().isEmpty()){
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getStateRegion()!=null && !nearByContactsList.get(i).getStateRegion().isEmpty()){
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getCounty()!=null && !nearByContactsList.get(i).getCountry().isEmpty()){
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else if(nearByContactsList.get(i).getZipPostalCode()!=null && !nearByContactsList.get(i).getZipPostalCode().isEmpty()){
                                                    halfnearByContacts.add(nearByContactsList.get(i));
                                                }
                                                else {
                                                    nonearByContacts.add(nearByContactsList.get(i));
                                                }
                                            }

                                        fullnearByContacts.addAll(halfnearByContacts);
                                        fullnearByContacts.addAll(nonearByContacts);

                                            ((SaveListActivity) activity).updateRepContactsList(fullnearByContacts,noOfRecentContacts);
                                            // ((NearbyContactsActivity) activity).displayLongToast("success");
                                        }
                                        else {
                                            // ((NearbyContactsActivity) activity).displayToast("No Data");
                                            if (MMProgressDialog.isProgressDialogShown)
                                                MMProgressDialog.hideProgressDialog();
                                            ((SaveListActivity) activity).updateRepContactsList(null,0);
                                        }

                                }
                                else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                    //response.getString(Constants.MM_ERROR_MESSAGE);
                                    ((SaveListActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                }
                                else {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                      //  if (requestFrom == Constants.RequestFrom.SAVE_LIST)
                                            //  ((NearbyContactsActivity) activity).displayLongToast("No Data");
                                        if (MMProgressDialog.isProgressDialogShown)
                                            MMProgressDialog.hideProgressDialog();
                                            ((SaveListActivity) activity).updateRepContactsList(null,0);

                                    } else {
                                       // if (requestFrom == Constants.RequestFrom.SAVE_LIST)

                                            ((SaveListActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));

                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((SaveListActivity) activity).onVolleyErrorResponse(error);

                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", authToken);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");

                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getAdvancedSearchContactsReq, TAG);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void createSimilarContact(JSONObject jsonObject, String authToken, String domain) {
        Log.e("veera",authToken);
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                JsonObjectRequest getSimilarContactsReq = new JsonObjectRequest(POST, SIMILAR_CONTACTS_URL, jsonObject,
                        response -> {
                            Log.e("veera",response.toString());
                            try {
                            Gson gson = new Gson();
                            SimilarContactsResponse submitResponse = gson.fromJson(response.toString(), SimilarContactsResponse.class);
                            if(submitResponse!=null && submitResponse.getContent()!=null){
                                if(!CollectionUtils.isEmpty(submitResponse.getContent().getList())) {
                                    ((CreateContactActivity) activity).updateSimilarContactsList(submitResponse.getContent().getList());
                                } else {
                                    ((CreateContactActivity) activity).updateSimilarContactsList(null);
                                }

                            } else {
                                ((CreateContactActivity) activity).updateSimilarContactsList(null);
                               // ((CreateContactActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                            }
                            } catch (Exception e) {
                                ((CreateContactActivity) activity).updateSimilarContactsList(null);
                                e.printStackTrace();
                            }
                        },
                        error -> {
                            ((CreateContactActivity) activity).updateSimilarContactsList(null);
                           // ((CreateContactActivity) activity).onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", authToken);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        //  Log.e("chinna",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(context).addToRequestQueue(getSimilarContactsReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

