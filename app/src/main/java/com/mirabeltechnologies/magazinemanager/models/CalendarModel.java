package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.CalendarActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateCalendarEventActivity;
import com.mirabeltechnologies.magazinemanager.activities.LoginActivity;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEvent;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEventDetails;
import com.mirabeltechnologies.magazinemanager.beans.NewCalendarEvent;
import com.mirabeltechnologies.magazinemanager.beans.RecurrenceRules;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalendarModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;

    public CalendarModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void getEventsByDate(final String encryptedClientKey, String dtTicks, int employeeId, String selectedDate) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getEventsByDateReqURL = String.format("%s/%s?employeeId=%d&date=%s&dtTicks=%s", Constants.MAIN_URL, Constants.CALENDAR_GET_EVENTS, employeeId, selectedDate, dtTicks);

                Log.e("veera",getEventsByDateReqURL);
                JsonObjectRequest getEventsByDateReq = new JsonObjectRequest(Request.Method.GET, getEventsByDateReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        Type calendarEventsType = new TypeToken<ArrayList<CalendarEvent>>() {
                                        }.getType();

                                        List<CalendarEvent> calendarEvents = new Gson().fromJson(response.getString("calendar"), calendarEventsType);
                                        ((CalendarActivity) activity).updateCalendarEventsForSelectedDate(calendarEvents);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CalendarActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        ((CalendarActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((CalendarActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getEventsByDateReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getEventDetailsByEventId(final String encryptedClientKey, String dtTicks, int employeeId, String eventDate, final String eventId) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String getEventDetailsByEventIdReqURL = String.format("%s/%s?employeeId=%d&eventId=%s&date=%s&dtTicks=%s", Constants.MAIN_URL, Constants.CALENDAR_GET_EVENT_DETAILS, employeeId, eventId, eventDate, dtTicks);

                JsonObjectRequest getEventDetailsByEventIdReq = new JsonObjectRequest(Request.Method.GET, getEventDetailsByEventIdReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        CalendarEventDetails calendarEventDetails = new Gson().fromJson(response.getString("event"), CalendarEventDetails.class);
                                        ((CalendarActivity) activity).editCalendarEvent(eventId, calendarEventDetails);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CalendarActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        ((CalendarActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((CalendarActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(getEventDetailsByEventIdReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addCalendarEvent(final String encryptedClientKey, String dtTicks, NewCalendarEvent calendarEvent, final boolean isEditEvent) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String addCalendarEventReqURL = "";

                if (isEditEvent)
                    addCalendarEventReqURL = String.format("%s/%s?eventId=%s&eventDate=%s&dtTicks=%s", Constants.MAIN_URL, Constants.CALENDAR_CREATE_EVENT, calendarEvent.getEventId(), calendarEvent.getEventDate(), dtTicks);
                else
                    addCalendarEventReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.CALENDAR_CREATE_EVENT, dtTicks);

                JSONObject reqObject = new JSONObject();

                if (isEditEvent)
                    reqObject.put("ID", calendarEvent.getEventId());

                reqObject.put("Title", calendarEvent.getTitle());
                reqObject.put("MeetingType", calendarEvent.getMeetingTypeId());
                reqObject.put("Notes", calendarEvent.getNotes());
                reqObject.put("IsAllDay", calendarEvent.isAllDay());

                if (calendarEvent.isAllDay()) {
                    reqObject.put("StartDate", calendarEvent.getStartDate() + " 00:00");
                    reqObject.put("EndDate", calendarEvent.getEndDate() + " 23:59");
                } else {
                    reqObject.put("StartDate", calendarEvent.getStartDate() + " " + calendarEvent.getStartTime());
                    reqObject.put("EndDate", calendarEvent.getEndDate() + " " + calendarEvent.getEndTime());
                }

                reqObject.put("RFlag", calendarEvent.isRecurrenceEvent());

                if (calendarEvent.isRecurrenceEvent()) {
                    JSONObject recurrenceRulesObject = new JSONObject();

                    RecurrenceRules recurrenceRules = calendarEvent.getRecurrenceRules();

                    if (recurrenceRules != null) {
                        recurrenceRulesObject.put("Pattern", recurrenceRules.getPattern());
                        recurrenceRulesObject.put("RepeatEvery", recurrenceRules.getRepeatEvery());

                        if (recurrenceRules.getPattern().equalsIgnoreCase("Weekly")) {
                            recurrenceRulesObject.put("RepeatDays", recurrenceRules.getRepeatDays());
                        }

                        if (recurrenceRules.getPattern().equalsIgnoreCase("Monthly")) {
                            recurrenceRulesObject.put("RepeatBy", recurrenceRules.getRepeatBy());

                            if (recurrenceRules.getRepeatBy() == 1)
                                recurrenceRulesObject.put("RepeatByOption", recurrenceRules.getRepeatByOption());
                        }

                        recurrenceRulesObject.put("RuleType", recurrenceRules.getRuleType());

                        if (recurrenceRules.getRuleType() == 0)
                            recurrenceRulesObject.put("UntilDate", recurrenceRules.getUntilDate());

                        if (recurrenceRules.getRuleType() == 1)
                            recurrenceRulesObject.put("Occurrences", recurrenceRules.getOccurrences());
                    }

                    reqObject.put("RRuleDetails", recurrenceRulesObject);
                }

                JsonObjectRequest addCalendarEventReq = new JsonObjectRequest(Request.Method.POST, addCalendarEventReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        ((CreateCalendarEventActivity) activity).updateCalendarEventAddingStatus();
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CreateCalendarEventActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (isEditEvent)
                                            ((CreateCalendarEventActivity) activity).showMMErrorResponse("Event editing failed.");
                                        else
                                            ((CreateCalendarEventActivity) activity).showMMErrorResponse("Event adding failed.");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((CreateCalendarEventActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(addCalendarEventReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCalendarEvent(final String encryptedClientKey, String dtTicks, String eventId, String eventDate) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String deleteCalendarEventReqURL = String.format("%s/%s?eventId=%s&eventDate=%s&dtTicks=%s", Constants.MAIN_URL, Constants.CALENDAR_DELETE_EVENT, eventId, eventDate, dtTicks);

                JsonObjectRequest deleteCalendarEventReq = new JsonObjectRequest(Request.Method.GET, deleteCalendarEventReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        ((CreateCalendarEventActivity) activity).updateDeleteCalendarEventStatus();
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CreateCalendarEventActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        ((CreateCalendarEventActivity) activity).showMMErrorResponse("Event deleting failed.");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((CreateCalendarEventActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(deleteCalendarEventReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
