package com.mirabeltechnologies.magazinemanager.models;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.LoginActivity;
import com.mirabeltechnologies.magazinemanager.activities.NewTicketActivity;
import com.mirabeltechnologies.magazinemanager.beans.NewTicket;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class NewTicketModel {
    public static final String TAG = NewTicketActivity.class.getSimpleName();

    private Context context;
    private NewTicketActivity activity;

    public NewTicketModel(Context context, NewTicketActivity activity) {
        this.context = context;
        this.activity = activity;
    }

    public void createNewTicket(NewTicket newTicket) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                JSONObject project = new JSONObject();
                project.put("key", "TS");

                JSONObject issueType = new JSONObject();
                issueType.put("name", "Support");

                JSONObject category = new JSONObject();
                category.put("id", newTicket.getCategoryValue());

                JSONArray labelsArray = new JSONArray();
                labelsArray.put(0, "MobileApp");

                JSONObject fieldsObject = new JSONObject();
                fieldsObject.put("project", project);
                fieldsObject.put("issuetype", issueType);
                fieldsObject.put("labels", labelsArray);
                fieldsObject.put("customfield_10054", category);
                fieldsObject.put("summary", newTicket.getSubject());
                fieldsObject.put("description", newTicket.getDescription());
                fieldsObject.put("customfield_10056", newTicket.getCcEmail());
                fieldsObject.put("customfield_10059", newTicket.getName());
                fieldsObject.put("customfield_10060", newTicket.getEmail());
                fieldsObject.put("customfield_10055", newTicket.getCompanyName());
                fieldsObject.put("customfield_10057", newTicket.getUrl());
                fieldsObject.put("customfield_10068", newTicket.getBrowser());

                JSONObject reqObject = new JSONObject();
                reqObject.put("fields", fieldsObject);

                JsonObjectRequest createNewTicketReq = new JsonObjectRequest(Request.Method.POST, Constants.JIRA_CREATE_NEW_TICKET_URL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                      if(response.getString("id") != null && !response.getString("id").isEmpty() ){
                                          activity.updateCreateNewTicketStatus(response);
                                    }
                                      else {
                                          activity.doLogout(activity.getResources().getString(R.string.login_error));
                                      }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                activity.createNewTicketFailed();
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        /*
                         * Supplying basic auth headers
                         *
                         * If you need to you may construct and send basic auth headers yourself. To do this you need to perform the following steps:
                         *
                         * Generate an API token for Jira using your Atlassian Account: https://id.atlassian.com/manage/api-tokens.
                         * Build a string of the form username:api_token.
                         * BASE64 encode the string.
                         * Supply an Authorization header with content Basic followed by the encoded string.
                         *
                         * For example, the string fred:fred encodes to ZnJlZDpmcmVk in base64, so you would make the request as follows:
                         */
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Authorization", Constants.JIRA_AUTHORIZATION_HEADER);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(createNewTicketReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
