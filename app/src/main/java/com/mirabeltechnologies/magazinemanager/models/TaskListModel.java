package com.mirabeltechnologies.magazinemanager.models;

import android.app.Activity;
import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewTaskActivity;
import com.mirabeltechnologies.magazinemanager.activities.LoginActivity;
import com.mirabeltechnologies.magazinemanager.activities.TaskListActivity;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactActivitiesActivity;
import com.mirabeltechnologies.magazinemanager.beans.NewTask;
import com.mirabeltechnologies.magazinemanager.beans.SearchTaskListFilter;
import com.mirabeltechnologies.magazinemanager.beans.Task;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by venkat on 3/16/18.
 */

public class TaskListModel {
    private Context context;
    private Activity activity;
    private Constants.RequestFrom requestFrom;
    private String TAG;

    public TaskListModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
        this.TAG = activity.getClass().getSimpleName();
    }

    public void searchTaskList(final String encryptedClientKey, String dtTicks, SearchTaskListFilter filter, int pageNumber) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                int startIndex = ((pageNumber - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
                int endIndex = pageNumber * Constants.ACTIVITY_PAGE_SIZE_INT;

                String searchTaskListReqURL = String.format("%s/%s?dtTicks=%s&startIndex=%d&endIndex=%d", Constants.MAIN_URL, Constants.SEARCH_TASKS, dtTicks, startIndex, endIndex);

                JSONObject reqObject = new JSONObject();
                reqObject.put("gsEmpID_Assigned", filter.getAssignedToRepId());
                reqObject.put("gsEmpID_Created", filter.getAssignedByRepId());
                reqObject.put("PriorityId", filter.getPriorityId());
                reqObject.put("AssociatedProject", filter.getProjectId());
                reqObject.put("Completed", filter.getStatus());

                JsonObjectRequest searchTaskListReq = new JsonObjectRequest(Request.Method.POST, searchTaskListReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        int noOfTasks = 0;
                                        if (response.has("Count"))
                                            noOfTasks = response.getInt("Count");

                                        Type taskListType = new TypeToken<ArrayList<Task>>() {
                                        }.getType();

                                        List<Task> searchTaskListResultsList = new Gson().fromJson(response.getString("tasks"), taskListType);
                                        ((TaskListActivity) activity).updateTaskList(searchTaskListResultsList, noOfTasks);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((TaskListActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_NO_RECORDS) {
                                            ((TaskListActivity) activity).updateTaskList(null, 0);
                                        } else {
                                            ((TaskListActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        }
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((TaskListActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(searchTaskListReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createNewTask(final String encryptedClientKey, String dtTicks, NewTask task) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String createNewTaskReqURL = String.format("%s/%s?dtTicks=%s", Constants.MAIN_URL, Constants.CREATE_NEW_TASK, dtTicks);

                JSONObject reqObject = new JSONObject();
                reqObject.put("TaskId", task.getTaskId());
                reqObject.put("Name", task.getTaskName());
                reqObject.put("Description", task.getDescription());
                reqObject.put("gsEmpID_Assigned", task.getAssignedToRepId());
                reqObject.put("gsEmpID_Created", task.getAssignedByRepId());
                reqObject.put("Priority", task.getPriorityId());
                reqObject.put("AssociatedProject", task.getProjectId());

                if (task.getDueDate().length() > 0)
                    reqObject.put("DueDate", task.getDueDate());

                reqObject.put("PrivateNotes", task.isPrivate());

                JsonObjectRequest createNewActivityReq = new JsonObjectRequest(Request.Method.POST, createNewTaskReqURL, reqObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        ((CreateNewTaskActivity) activity).updateTaskCreateStatus();
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){

                                        ((CreateNewTaskActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                    }
                                    else {
                                        ((CreateNewTaskActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                ((CreateNewTaskActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(createNewActivityReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void modifyTask(final String encryptedClientKey, String dtTicks, Long taskId, final String action) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                String modifyTaskReqURL = String.format("%s/%s?id=%d&action=%s&dtTicks=%s", Constants.MAIN_URL, Constants.TASK_ACTIONS, taskId, action, dtTicks);

                JsonObjectRequest modifyTaskReq = new JsonObjectRequest(Request.Method.GET, modifyTaskReqURL, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                        if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE)
                                            ((TaskListActivity) activity).updateTasksAfterModify(action);
                                        else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).updateActivitiesAfterModify(action);
                                    }
                                    else  if(response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_LOGIN_ERROR){
                                        if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE)
                                            ((TaskListActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));
                                        else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).doLogout(activity.getResources().getString(R.string.login_error));

                                    }
                                    else {
                                        if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE)
                                            ((TaskListActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                        else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                            ((ContactActivitiesActivity) activity).showMMErrorResponse(response.getString(Constants.MM_ERROR_MESSAGE));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_PAGE)
                                    ((TaskListActivity) activity).onVolleyErrorResponse(error);
                                else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                                    ((ContactActivitiesActivity) activity).onVolleyErrorResponse(error);
                            }
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put(Constants.AUTHORISATION, encryptedClientKey);
                        return headers;
                    }
                };

                AppController.getInstance(context).addToRequestQueue(modifyTaskReq, TAG);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
