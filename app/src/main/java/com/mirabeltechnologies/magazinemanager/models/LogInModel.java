package com.mirabeltechnologies.magazinemanager.models;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.mirabeltechnologies.magazinemanager.activities.ClientsListActivity;
import com.mirabeltechnologies.magazinemanager.activities.DashboardActivity;
import com.mirabeltechnologies.magazinemanager.activities.LoginActivity;
import com.mirabeltechnologies.magazinemanager.asynctasks.MMAsyncRequest;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by venkat on 04/10/17.
 */
public class LogInModel {

    private Context context;
    private Activity activity;
    private String username;
    private Constants.RequestFrom requestFrom;

    public LogInModel(Context context, Activity activity, Constants.RequestFrom requestFrom) {
        this.context = context;
        this.activity = activity;
        this.requestFrom = requestFrom;
    }

    public void validateUserLogin(String email, String pwd) {
        try {
            if (new NetworkConnectionDetector(context).isNetworkConnected()) {
                username = email;
                EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(true);
                String encryptedEmail = des.getEncryptedValueWithOutReplacingCharacters(email);
                String encryptedPassword = des.getEncryptedValueWithOutReplacingCharacters(pwd);
                Date currentDate = new Date(System.currentTimeMillis());
                Log.e("veera",""+currentDate);
                String dtTicks = des.getEncryptedValueWithOutReplacingCharacters(String.valueOf(System.currentTimeMillis()));

              // String logInURL = String.format("%s/GetAuthentication?loginId=%s&password=%s&dtticks=%s&product=%s", Constants.LOGIN_URL, encryptedEmail, encryptedPassword, dtTicks, "");

                String logInURL = String.format("%s/GetAuthentication?loginId=%s&password=%s&dtticks=%s&product=%s", Constants.LOGIN_URL, encryptedEmail, encryptedPassword, dtTicks, "MobileApp_Android");

                Log.e("veera",logInURL);

                @SuppressLint("StaticFieldLeak")
                MMAsyncRequest loginRequest = new MMAsyncRequest(context, logInURL, MMAsyncRequest.MethodType.GET) {
                    @Override
                    public void onResponseReceived(String response) {
                        try {
                            Log.e("veera",response);
                            JSONObject jsonObject = new JSONObject(response);
                            int responseCode = jsonObject.getInt("ResponseCode");
                            Log.e("veera",""+responseCode);

                            if (responseCode == Constants.MM_LOGIN_SUCCESS) {
                                if (!jsonObject.getBoolean("IsPasswordStrong")) {
                                    sendResponseCodeToActivity(Constants.MM_PASSWORD_STRONG);
                                } else if (jsonObject.getBoolean("IsPasswordExpired")) {
                                    sendResponseCodeToActivity(Constants.MM_PASSWORD_EXPIRED);
                                } else if (!jsonObject.getBoolean("IsExistingUser") && jsonObject.getBoolean("IsPendingVerification")) {
                                    sendResponseCodeToActivity(Constants.MM_PENDING_VERIFICATION);
                                } else {
                                    JSONArray clientsList = jsonObject.getJSONArray("ClientsDetails");

                                    ParseClientsList parseClientsList = new ParseClientsList();
                                    parseClientsList.execute(clientsList);
                                }
                                // Updating Logged In User Rep Name
                                if (requestFrom == Constants.RequestFrom.LOGIN_PAGE)
                                    ((LoginActivity) activity).updateLoggedInUserName(jsonObject.getString("FirstName"), jsonObject.getString("LastName"));
                            } else {

                                sendResponseCodeToActivity(responseCode);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };

                loginRequest.execute();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void sendResponseCodeToActivity(int code) {
        if (requestFrom == Constants.RequestFrom.LOGIN_PAGE) {
            ((LoginActivity) activity).logInError(code);
        } else if (requestFrom == Constants.RequestFrom.CHANGE_CLIENT) {
            ((ClientsListActivity) activity).logInError(code);
        }
    }
    @SuppressLint("StaticFieldLeak")
    public class ParseClientsList extends AsyncTask<JSONArray, Void, List<ClientInfo>> {

        List<ClientInfo> clientInfoList = new ArrayList<>();

        @Override
        protected List<ClientInfo> doInBackground(JSONArray... params) {
            try {
                JSONArray clientsList = params[0];


                for (int i = 0; i < clientsList.length(); i++) {
                    JSONObject jsonObject = clientsList.getJSONObject(i);

                    ClientInfo clientInfo = new ClientInfo();
                    clientInfo.setClientId(jsonObject.getInt("ClientID"));
                    clientInfo.setClientName(jsonObject.getString("ClientName"));
                    clientInfo.setClientURL(jsonObject.getString("ClientURLs"));
                    clientInfo.setSubDomain(jsonObject.getString("SubDomain"));
                    clientInfo.setRepUserName(username);
                    clientInfo.setEmployeeId(jsonObject.getInt("EmployeeId"));
                    clientInfo.setSiteType(jsonObject.getString("SiteType"));
                    clientInfo.setDSUser(jsonObject.getBoolean("IsDsUser"));

                    // We are displaying only enabled client list.
                    if (jsonObject.getBoolean("Enabled")) {
                        clientInfoList.add(clientInfo);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return clientInfoList;
        }

        @Override
        protected void onPostExecute(List<ClientInfo> clientInfos) {

            Log.e("client",clientInfos.toString());

            if (requestFrom == Constants.RequestFrom.LOGIN_PAGE) {
                ((LoginActivity) activity).updateClientsList(clientInfos);
            } else if (requestFrom == Constants.RequestFrom.CHANGE_CLIENT) {
                ClientInfo.clientList = clientInfos;
                ((ClientsListActivity) activity).updateClientsList(clientInfos, true);
            } else if (requestFrom == Constants.RequestFrom.DASHBOARD) {
                ClientInfo.clientList = clientInfos;
                ((DashboardActivity) activity).updateClientsList(clientInfos);
            }
        }
    }
}
