package com.mirabeltechnologies.magazinemanager.Modules;

import java.util.List;


/**
 * Created by veera Hiep on 4/3/2016.
 */
public interface DirectionFinderListener {

    void onDirectionFinderStart();
    void onDirectionFinderSuccess(List<Route> route);

}
