package com.mirabeltechnologies.magazinemanager.asynctasks;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;

import com.mirabeltechnologies.magazinemanager.activities.NewTicketActivity;
import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class UploadFileAsyncTask extends AsyncTask<Void, Integer, String> {
    private Context context;
    private Activity activity;
    private String addAttachmentUrl, issueId, ticketKey, fileName, filePath, serverResponseStr = "";

    public UploadFileAsyncTask(Context context, Activity activity, String issueId, String key, String fileName, String filePath) {
        this.context = context;
        this.activity = activity;
        this.issueId = issueId;
        this.ticketKey = key;
        this.fileName = fileName;
        this.filePath = filePath;
    }

    @Override
    protected String doInBackground(Void... voids) {
        try {
            addAttachmentUrl = String.format(Constants.JIRA_ADD_ATTACHMENT_FOR_TICKET_URL, ticketKey);

            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            @SuppressWarnings("PointlessArithmeticExpression")
            int maxBufferSize = 1 * 1024 * 1024;

            URL url = new URL(addAttachmentUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            // Allow Inputs &amp; Outputs.
            connection.setDoInput(true);
            connection.setDoOutput(true);
            connection.setUseCaches(false);

            // Set HTTP method to POST.
            connection.setRequestMethod("POST");

            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Authorization", Constants.JIRA_AUTHORIZATION_HEADER);
            connection.setRequestProperty("X-Atlassian-Token", "no-check"); // don't remove this line & it is mandatory as per Jira REST API Guidelines
            connection.setRequestProperty("Connection", "Keep-Alive");
            connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            FileInputStream fileInputStream;
            DataOutputStream outputStream;

            {
                outputStream = new DataOutputStream(connection.getOutputStream());

                // Extra parameters if you want to pass to server
                /*outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"issueId\"" + lineEnd);
                outputStream.writeBytes("Content-Type: text/plain; charset=UTF-8" + lineEnd);
                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(issueId);
                outputStream.writeBytes(lineEnd);
                outputStream.flush();*/

                outputStream.writeBytes(twoHyphens + boundary + lineEnd);
                outputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + fileName + "\"" + lineEnd);
                outputStream.writeBytes(lineEnd);

                fileInputStream = new FileInputStream(filePath);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);

                buffer = new byte[bufferSize];

                // Read file
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {
                    outputStream.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                }

                outputStream.writeBytes(lineEnd);
                outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
            }

            int serverResponseCode = connection.getResponseCode();
            String serverResponseMessage = connection.getResponseMessage();

            fileInputStream.close();
            outputStream.flush();
            outputStream.close();

            if (serverResponseCode == HttpURLConnection.HTTP_OK) {
                try {
                    BufferedInputStream inputStream = new BufferedInputStream(connection.getInputStream());
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                    StringBuilder serverResponse = new StringBuilder();
                    String line;

                    while ((line = bufferedReader.readLine()) != null) {
                        serverResponse.append(line);
                    }

                    serverResponseStr = serverResponse.toString();

                    bufferedReader.close();

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                serverResponseStr = Constants.STATUS_FAILURE;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return serverResponseStr;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        ((NewTicketActivity) activity).updateFileUploadStatus(response);
    }
}
