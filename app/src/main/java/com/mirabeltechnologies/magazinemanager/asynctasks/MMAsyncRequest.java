package com.mirabeltechnologies.magazinemanager.asynctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;


import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.interfaces.OnAsyncRequestCompleteListener;
import com.mirabeltechnologies.magazinemanager.util.NetworkManager;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by venkat on 04/10/17.
 */
public abstract class MMAsyncRequest extends AsyncTask<Void, Void, String> implements OnAsyncRequestCompleteListener {
    private Context context;
    private String requestURL = null;
    private JSONObject parameters = null;
    private Map<String, String> headers = null;
    private MethodType methodType = MethodType.GET;
    private Constants.RequestCode requestCode = null;

    public enum MethodType {
        GET,
        POST,
        GET_WITH_HEADERS
    }

    // Constructors
    public MMAsyncRequest(Context context) {
        this.context = context;
    }

    public MMAsyncRequest(Context context, String url, MethodType method) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
    }

    public MMAsyncRequest(Context context, String url, MethodType method, JSONObject params) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.parameters = params;
    }

    public MMAsyncRequest(Context context, String url, MethodType method, Map<String, String> headers) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.headers = headers;
    }

    public MMAsyncRequest(Context context, String url, MethodType method, Constants.RequestCode reqCode) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.requestCode = reqCode;
    }

    public MMAsyncRequest(Context context, String url, MethodType method, JSONObject params, Constants.RequestCode reqCode) {
        this.context = context;
        this.requestURL = url;
        this.methodType = method;
        this.parameters = params;
        this.requestCode = reqCode;
    }

    @Override
    protected void onPreExecute() {
        /*if (new NetworkConnectionDetector(context).isNetworkConnected()) {
            MCProgressDialog.showProgressDialog(context, context.getResources().getString(R.string.loading_message));
        }*/
    }

    @Override
    protected String doInBackground(Void... params) {

        String response = null;
        try {
            if (methodType == MethodType.POST) {
                response = new NetworkManager().makeHttpPostConnection(requestURL, parameters);
            } else if (methodType == MethodType.GET_WITH_HEADERS) {
                response = new NetworkManager().makeHttpGetConnectionWithHeaders(requestURL, headers);
            } else {
                response = new NetworkManager().makeHttpGetConnection(requestURL);
            }
        } catch (Exception e) {
            Log.e("veera","error");
            e.printStackTrace();
        }
        Log.e("veera",response);
        return response;
    }

    @Override
    protected void onPostExecute(String responseString) {
        // Sending response back to listener
        if (responseString.equals("failure") || responseString.equals("error")) {
            MMProgressDialog.hideProgressDialog();
            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_error), context.getResources().getString(R.string.error_failure));
        } else if (responseString.equals("timeout")) {
            MMProgressDialog.hideProgressDialog();
            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_error), context.getResources().getString(R.string.error_request_timeout));
        } else {
            if (requestCode == null)
                onResponseReceived(responseString);
            else
                onResponseReceived(responseString, requestCode);
        }
    }

    @Override
    protected void onCancelled(String response) {
        MMProgressDialog.hideProgressDialog();

        // Sending response back to listener
        if (response.equals("failure") || response.equals("error")) {
            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_error), context.getResources().getString(R.string.error_failure));
        } else if (response.equals("timeout")) {
            MMAlertDialog.showAlertDialog(context, context.getResources().getString(R.string.title_error), context.getResources().getString(R.string.error_request_timeout));
        } else {
            if (requestCode == null)
                onResponseReceived(response);
            else
                onResponseReceived(response, requestCode);
        }
    }

    public void onResponseReceived(String response) {
        // Empty body and we can override this method with your implementation in sub class.
    }

    public void onResponseReceived(String response, Constants.RequestCode requestCode) {
        // Empty body and we can override this method with your implementation if you want.
    }
}
