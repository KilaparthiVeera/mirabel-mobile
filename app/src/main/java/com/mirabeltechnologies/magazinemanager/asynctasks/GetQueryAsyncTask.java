package com.mirabeltechnologies.magazinemanager.asynctasks;

import android.os.AsyncTask;

import com.mirabeltechnologies.magazinemanager.beans.QueryObject;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.NetworkManager;

import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by venkat on 12/1/17.
 */

public class GetQueryAsyncTask extends AsyncTask<QueryObject, Void, String> {
    private String responseString = null;
    private String encryptedClientKey;

    public GetQueryAsyncTask(String encryptedClientKey) {
        this.encryptedClientKey = encryptedClientKey;
    }

    @Override
    protected String doInBackground(QueryObject... queryObjects) {
        try {
            QueryObject queryObject = queryObjects[0];
            String dtTicks = new EncryptDecryptStringWithDES(false).getEncryptedValueWithOutReplacingCharacters(String.valueOf(System.currentTimeMillis()));
            String getQueryReqURL = String.format("%s/%s?query=%s&typeOfFilter=%s&dtTicks=%s", Constants.MAIN_URL, Constants.GET_QUERY, URLEncoder.encode(queryObject.getKeyword(), Constants.CHARSET_NAME), queryObject.getTypeOfFilter(), dtTicks);

            Map<String, String> headers = new HashMap<>();
            headers.put(Constants.AUTHORISATION, encryptedClientKey);

            responseString = new NetworkManager().makeHttpGetConnectionWithHeaders(getQueryReqURL, headers);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return responseString;
    }
}
