package com.mirabeltechnologies.magazinemanager.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.mirabeltechnologies.magazinemanager.constants.Constants;

import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALLS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETINGS_SWITCH;


public class NotificationService extends Service {
    protected String encryptedClientKey;
    protected MMSharedPreferences sharedPreferences;
    MMSettingsPreferences mmSettingsPreferences;
    Utility utility;

    @Override
    public void onCreate() {
        super.onCreate();
        utility = new Utility();
        mmSettingsPreferences = new MMSettingsPreferences(this);
        sharedPreferences = new MMSharedPreferences(this);
        encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH) || mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {
            //fetch the notification data from the server when the application is opened and landed into DashboardActivity.
            scheduleFirebaseJobDispatcherToRunInBackgroundfrequently();
        } else {
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }


    public void scheduleFirebaseJobDispatcherToRunInBackgroundfrequently() {
      //  FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(this);
      //  firebaseJobDispatcherClass.scheduleFirebaseJobDispatcher();


       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            System.out.println("====dummy job scheduler scheduled");
            Util.scheduleJob(getApplicationContext());
        }*/
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }


}
