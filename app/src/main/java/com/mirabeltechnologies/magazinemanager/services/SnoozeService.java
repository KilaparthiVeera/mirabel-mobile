package com.mirabeltechnologies.magazinemanager.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;


import androidx.annotation.Nullable;

import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALLS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETING;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETINGS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_ID;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.NOTIFICATION_TYPE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SNOOZE_TIME_PICKER;

public class SnoozeService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    ActivityData activityData;
    MMSettingsPreferences mmSettingsPreferences;
    int notificationId;
    String type;
    Utility utility;

    MMLocalDataBase mmLocalDataBase;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mmSettingsPreferences = new MMSettingsPreferences(this);
        notificationId = intent.getExtras().getInt(NOTIFICATION_ID);
        type = intent.getExtras().getString(NOTIFICATION_TYPE);
        try {
            activityData = (ActivityData) utility.deserializeObject(intent.getExtras().getByteArray(EVENT_DATA));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String time = activityData.getDateScheduled();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
        long originalScheduleTimeMillis = 0;
        try {
            Date date = simpledateformat.parse(time);
            originalScheduleTimeMillis = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Intent alarmintent = new Intent(this, AlarmReceiver.class);
        alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
        try {
            alarmintent.putExtra(EVENT_DATA, utility.serializeObject(activityData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, notificationId, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
        long requiredMillis = System.currentTimeMillis() + (mmSettingsPreferences.getIntegerValue(SNOOZE_TIME_PICKER) * 60 * 1000);
        mmLocalDataBase = new MMLocalDataBase(this);
        //check weather the notification is already present or not , then either update or insert new record.
        // important . before inserting check meetings/calls are enabled or not.
        switch (type) {
            case MEETING:
                if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH)) {
                    if (Build.VERSION.SDK_INT >= 23) {
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                    } else {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                    }
                    //check weather the notification record already exists or not .
                    if (!mmLocalDataBase.checkRecordExistsOrNotInTable(notificationId)) {
                        // And insert only new notifications
                       mmLocalDataBase.AddNewNotificationRecordDataIntoTableNotificationIds(notificationId, 1, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis),1);

                    } else {
                        //update the old notification(alarm time in millis)
                        mmLocalDataBase.updateModifiedTimeOnSnoozeClicked(notificationId, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis));
                    }
                }
                break;
            case CALL:
                if (mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {

                    if (Build.VERSION.SDK_INT >= 23) {
                        alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                    } else {
                        alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                    }
                    //check weather the notification record already exists or not .
                    if (!mmLocalDataBase.checkRecordExistsOrNotInTable(notificationId)) {
                        // And insert only new notifications
                        mmLocalDataBase.AddNewNotificationRecordDataIntoTableNotificationIds(notificationId, 0, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis),1);

                    } else {
                        //update the old notification(alarm time in millis)
                        mmLocalDataBase.updateModifiedTimeOnSnoozeClicked(notificationId, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis));

                    }

                }

                break;
        }


        stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        utility = new Utility();
        // startForeground(1, new Notification());
    }
}
