package com.mirabeltechnologies.magazinemanager.services;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;

import androidx.annotation.NonNull;

import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobService;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.SearchActivitiesFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALL;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALLS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETING;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETINGS_SWITCH;


public class NotificationEventsScheduleJobService extends JobService {
    protected String encryptedClientKey;
    protected MMSharedPreferences sharedPreferences;
    MMSettingsPreferences mmSettingsPreferences;
    private ActivitiesModel activitiesModel;
  //  Utility utility;
    MMLocalDataBase mmLocalDataBase;


    JobParameters jobParameters;
    MMSharedPreferences mmSharedPreferences;

    @SuppressLint("StaticFieldLeak")
    @Override
    public boolean onStartJob(@NonNull JobParameters job) {
        //just to track the cron job work
        // dummyLog();

        /* *************/
        mmSettingsPreferences = new MMSettingsPreferences(this);
        jobParameters = job;
        if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH) || mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {
                    getNotifications();
                    return null;
                }
            }.execute();

        } else {
            stopSelf();
        }

        /*return false;*/ // Answers the question: "Is there still work going on?"
        return true;
    }

    @Override
    public boolean onStopJob(@NonNull JobParameters job) {
        return false; // Answers the question: "Should this job be retried?"

    }

    @Override
    public void onCreate() {
        super.onCreate();
        mmSharedPreferences = new MMSharedPreferences(this);
        //check weather the user is loggedin or not and close the jobservice and cancel the firebase job dispatcher
        if (mmSharedPreferences.isUserLoggedIn()) {
           // utility = new Utility();
            sharedPreferences = new MMSharedPreferences(this);
            encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);
            activitiesModel = new ActivitiesModel(this, this, Constants.RequestFrom.EVENTS_JOB_SCHEDULE_SERVICE);
            mmLocalDataBase = new MMLocalDataBase(this);
        } else {
          //  FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(this);
          //  firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);
            stopSelf();
        }
    }


   /* public void dummyLog() {
        if (utility.isExternalStorageWritable()) {
            File appDirectory = new File(Environment.getExternalStorageDirectory() + "/MyLogFolder");
            File logDirectory = new File(appDirectory + "/log");
            // File logFile = new File(logDirectory, "logcat" + System.currentTimeMillis() + ".txt");
            File logFile = new File(logDirectory, "logcat.txt");

            // create app folder
            if (!appDirectory.exists()) {
                appDirectory.mkdir();
            }
            // create log folder
            if (!logDirectory.exists()) {
                logDirectory.mkdir();
            }
            // clear the previous logcat and then write the new one to the file
            try {
                Process process = Runtime.getRuntime().exec("logcat -c");
                process = Runtime.getRuntime().exec("logcat -f " + logFile);


            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (utility.isExternalStorageReadable()) {
            // only readable
        } else {
            // not accessible
        }
    }*/


    public void getNotifications() {
        int currentPageNumber = 1;
        boolean isTabletDevice = true;
        int loggedInRepId = mmSharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID);
        String loggedInRepName = mmSharedPreferences.getString(Constants.SP_LOGGED_IN_REP_NAME);
        String currentActivityType = Constants.ACTIVITY_TYPE_CALLS_MEETINGS;
        SearchActivitiesFilter searchActivitiesFilter = new SearchActivitiesFilter(currentActivityType);
        // Setting default values
        searchActivitiesFilter.setAssignedToRepId(String.valueOf(loggedInRepId));
        searchActivitiesFilter.setAssignedToRepName(loggedInRepName);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy");
        Date date = new Date();
        //  preSelectedDate = 01/18/2019
        searchActivitiesFilter.setFromDate(simpleDateFormat.format(date));
        activitiesModel.fetchCallsAndMeetingsActivities(encryptedClientKey, Utility.getDTTicks(), searchActivitiesFilter, currentPageNumber, isTabletDevice ? 1000 : 500);

    }


    public void updateActivities(final List<ActivityData> activitiesList, int noOfActivities, int canDeleteNotes, int canEditNotes) {
        //check weather the notification exists with the same id in the table , and has not triggered , then remove that notification from alarmmanager and reschedule it.
        // ( we are doing this because , the user may have updated the data in side the event after it is scheduled in the device)
        try {
            //remove all  notifications based on schedule timestamp .
            Long currentTimeMillis = System.currentTimeMillis();
            mmLocalDataBase.removeAllScheduledNotificationWhichAreExpired(String.valueOf(currentTimeMillis));
            if (activitiesList != null) {
                //  ArrayList<Integer> notification_id_from_server_list = new ArrayList<>();
                for (ActivityData activityData : activitiesList) {
                    activityData.setCanDeleteNotes(canDeleteNotes);
                    activityData.setCanEditNotes(canEditNotes);
                    String activityNotes = activityData.getSpecialNotes();
                    Long activity_id = activityData.getActivityId();
                    String activityOrTaskId = String.valueOf(activity_id);
                    String type = activityData.getType();
                    String time = activityData.getDateScheduled();
                    // maximum a five digit number is generated from the activity_id, for handling the notifications and pendingintents  , as the number should be a unique .
                    int notification_id = Utility.lastFiveDigitsOfTaskId(activityOrTaskId);
                    //  notification_id_from_server_list.add(notification_id);
                    // calculate the time and current system time and compare both .
                    //if the time is equal or greater than the current time then schedule a notification.
                    SimpleDateFormat simpledateformat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.ENGLISH);
                    long originalScheduleTimeMillis = 0;
                    try {
                        Date date = simpledateformat.parse(time);
                        if (date != null) {
                            originalScheduleTimeMillis = date.getTime();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    //schedule only  incompleted activities with upcoming status
                    if (originalScheduleTimeMillis >= System.currentTimeMillis() && activityData.getCompleted().equals("False")) {
                        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                        switch (type) {
                            case MEETING:
                                if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH)) {
                                    Long requiredMillis = Utility.getScheduledMillis(type, originalScheduleTimeMillis);

                                    //check weather the notification record already exists or not .
                                    if (!mmLocalDataBase.checkRecordExistsOrNotInTable(notification_id)) {
                                        PendingIntent pendingIntent = getPendingIntent(activityData, notification_id);
                                        //  insert only new notifications
                                        mmLocalDataBase.AddNewNotificationRecordDataIntoTableNotificationIds(notification_id, 1, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis), 0);
                                        if (Build.VERSION.SDK_INT >= 23) {
                                            if (alarmManager != null) {
                                                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                            }
                                        } else {
                                            if (alarmManager != null) {
                                                alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                            }
                                        }

                                    } else {
                                        //update the old notification(alarm time in millis)
                                        //check wheather is_cancelled or not and act accordingly.
                                        if (mmLocalDataBase.isNotificationEligibleForReschedule(notification_id, String.valueOf(originalScheduleTimeMillis)) /*    check the already saved "original time" with the current "original time" and then decide to reschedule or ignore   */) {
                                            PendingIntent pendingIntent = getPendingIntent(activityData, notification_id);
                                            mmLocalDataBase.updateModifiedTimeOnSnoozeClicked(notification_id, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis));
                                            if (Build.VERSION.SDK_INT >= 23) {
                                                if (alarmManager != null) {
                                                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                                }
                                            } else {
                                                if (alarmManager != null) {
                                                    alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            case CALL:
                                if (mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {
                                    Long requiredMillis = Utility.getScheduledMillis(type, originalScheduleTimeMillis);
                                    //check weather the notification record already exists or not .
                                    if (!mmLocalDataBase.checkRecordExistsOrNotInTable(notification_id)) {
                                        PendingIntent pendingIntent = getPendingIntent(activityData, notification_id);
                                        // insert only new notifications
                                        mmLocalDataBase.AddNewNotificationRecordDataIntoTableNotificationIds(notification_id, 0, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis), 0);
                                        if (Build.VERSION.SDK_INT >= 23) {
                                            if (alarmManager != null) {
                                                alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                            }
                                        } else {
                                            if (alarmManager != null) {
                                                alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                            }
                                        }
                                    } else {
                                        //update the old notification(alarm time in millis)
                                        //check wheather is_cancelled or not and act accordingly.
                                        if (mmLocalDataBase.isNotificationEligibleForReschedule(notification_id, String.valueOf(originalScheduleTimeMillis)) /*    check the already saved "original time" with the current "original time" and then decide to reschedule or ignore   */) {
                                            PendingIntent pendingIntent = getPendingIntent(activityData, notification_id);
                                            mmLocalDataBase.updateModifiedTimeOnSnoozeClicked(notification_id, String.valueOf(originalScheduleTimeMillis), String.valueOf(requiredMillis));
                                            if (Build.VERSION.SDK_INT >= 23) {
                                                if (alarmManager != null) {
                                                    alarmManager.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                                }
                                            } else {
                                                if (alarmManager != null) {
                                                    alarmManager.set(AlarmManager.RTC_WAKEUP, requiredMillis, pendingIntent);
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                        }
                    }

                }

                //If the event is removed from the server and it is scheduled in the device. We have to remove it in device also by checking , weather it is coming or not while fetching the data for scheduling new notifications.
                // check in database for not triggered event . Then try to find it in the data coming from server . If the event is not present in the data , then assume it is removed from the activities in the server.
                //get rescheduled = 0 &  notification_id should be present in notifications list. If not remove from alarmmanager.
               /* List<Integer> scheduled_notification_ids_list;
                scheduled_notification_ids_list = mmLocalDataBase.fetchAllScheduledNotificationIdsForRemovingIfRemovedFromServer();
                scheduled_notification_ids_list.removeAll(notification_id_from_server_list);
                //now remove remaining of scheduled_notification_ids_list from alarmmanager
                PendingIntent pendingIntent;
                AlarmManager manager;
                Intent alarmintent;
                for (Integer id : scheduled_notification_ids_list) {
                    alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                    alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                    pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                    manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    //cancel the alarm manager of the pending intent
                    manager.cancel(pendingIntent);
                }*/

            }

        } catch (Exception ignored) {
        } finally {
            //Tell the framework that the job has completed and doesnot needs to be reschedule
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                jobFinished(jobParameters, false);
            } else {
                jobFinished(jobParameters, true);
            }

        }


    }


    public PendingIntent getPendingIntent(ActivityData activityData, int notification_id) {
        Intent alarmintent = new Intent(this, AlarmReceiver.class);
        alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
        try {
            alarmintent.putExtra(EVENT_DATA, Utility.serializeObject(activityData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return PendingIntent.getBroadcast(this, notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

}