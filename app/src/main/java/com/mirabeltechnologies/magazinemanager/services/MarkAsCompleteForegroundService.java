package com.mirabeltechnologies.magazinemanager.services;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import androidx.annotation.Nullable;

import com.android.volley.VolleyError;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.Utility;


import java.io.IOException;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_EVENT_DELETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_MARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTION_UNMARK;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTIVITY_ACTION_DELETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTIVITY_ACTION_MARK_COMPLETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.RequestFrom.MARK_DELETE_FOREGROUND_SERVICE;

public class MarkAsCompleteForegroundService extends Service {

    private ActivityData activityData;
    // int notificationId;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startForegroundService();
        String action = intent.getAction();
        //  notificationId = intent.getExtras().getInt(NOTIFICATION_ID);
        try {
            activityData = (ActivityData) Utility.deserializeObject(intent.getExtras().getByteArray(EVENT_DATA));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        switch (action) {
            case ACTION_MARK:
                markActivity(activityData.getActivityId(), ACTIVITY_ACTION_MARK_COMPLETE);
                break;
            case ACTION_EVENT_DELETE:
               deleteActivity(activityData.getActivityId(), ACTIVITY_ACTION_DELETE);
                break;

            case ACTION_UNMARK:
                unmarkActivity(activityData.getActivityId(), ACTIVITY_ACTION_MARK_UN_COMPLETE);
                break;
        }
        return super.onStartCommand(intent, flags, startId);
    }


    private void markActivity(Long activityId, String action) {
        modifyActivity(activityId, action);
    }

    private void unmarkActivity(Long activityId, String action) {
        modifyActivity(activityId, action);
    }

    private void deleteActivity(Long activityId, String action) {
        modifyActivity(activityId, action);
    }


    private void modifyActivity(Long activityId, String action) {
        MMSharedPreferences sharedPreferences = new MMSharedPreferences(this);
        String encryptedClientKey = sharedPreferences.getString(Constants.SP_ENCRYPTED_CLIENT_KEY);
        ActivitiesModel activitiesModel = new ActivitiesModel(this, this, MARK_DELETE_FOREGROUND_SERVICE);
        if (new NetworkConnectionDetector(this).checkNetworkConnectionWithOutAlertDialog()) {
            activitiesModel.modifyActivity(encryptedClientKey, Utility.getDTTicks(), activityId, action);
        } else {
            stopForegroundService();
        }
    }

    public void updateServiceAfterModify(String action) {
        if (action.equals(ACTIVITY_ACTION_DELETE)) {
            // On activity delete, we will remove activities in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like activity add / delete.
            stopForegroundService();
        } else {
            stopForegroundService();
        }
    }

    public void showMMErrorResponse(String message) {
    }

    public void onVolleyErrorResponse(VolleyError error) {
    }

    /* Used to build and start foreground service. */
    private void startForegroundService() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            Notification notification = new Notification.Builder(this, Constants.CALLS_AND_MEETINGS_CHANNEL_ID)
                    .setContentTitle("notification_title")
                    .setContentText("notification_message")
                    .setTicker("ticker_text")
                    .build();
            startForeground(1, notification);
        }
    }

    private void stopForegroundService() {
        stopForeground(true);
        // Stop the foreground service.
        stopSelf();
    }
}
