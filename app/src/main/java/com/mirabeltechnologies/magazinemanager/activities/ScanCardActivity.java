package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.text.FirebaseVisionText;
import com.google.firebase.ml.vision.text.FirebaseVisionTextRecognizer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CardBean;
import com.mirabeltechnologies.magazinemanager.beans.Post_BusinessCard;
import com.mirabeltechnologies.magazinemanager.beans.ScanCardDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.interfaces.BusinessCardApi;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;


import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ScanCardActivity extends BaseActivity {

    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    String[] cameraPermission;
    String[] storagePermission;
    Uri image_uri, imagePath;
    List<Uri> urs;
    Uri imageUpload;
    EditText nameEt,companyEt,telEt,address_1Et,emailEt,et_job,et_address2,et_city,et_state,et_country,et_zipcode,et_county;
    ImageView cardIv;
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    ScanCardDetails scanCardDetails;
    Map<String, Integer>  phoneNumberCandidates = new HashMap<>();
    Bitmap mSelectedImage ;

    Map<String, Integer>  emailCandidates = new HashMap<>();

    List<String> emailList = new ArrayList<>();
    List<String> phoneList = new ArrayList<>();
    List<String> zipList = new ArrayList<>();
    List<String> jobTitleList = new ArrayList<>();

    List<String> genericCandidates = new ArrayList<>();
    List<String>  nameCandidates = new ArrayList<>();
    List<String> companyCandidates = new ArrayList<>();
    private ProgressDialog progressDialog;


    private Constants.RequestFrom requestFrom;
    private Constants.SelectionType selectionType;
    public static String rightSideDrawerSelectedIndex = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_card);
        Objects.requireNonNull(getSupportActionBar()).setTitle(" Add Contact ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);   //show back button
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.actionbar_indicator);

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            requestFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            selectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
            rightSideDrawerSelectedIndex = bundle.getString(Constants.BUNDLE_RSD_SELECTED_INDEX);

            imagePath = Uri.parse(bundle.getString("imagePath"));

            Log.e("veera",String.valueOf(imagePath));
            //previouslySelectedValue = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
        }
        FirebaseApp.initializeApp(this);
        companyEt = findViewById(R.id.et_company);
        nameEt = findViewById(R.id.et_name);
        et_job = findViewById(R.id.et_job);
        telEt = findViewById(R.id.et_tel);
        emailEt = findViewById(R.id.et_email);
        address_1Et = findViewById(R.id.et_address1);
        et_address2 = findViewById(R.id.et_address2);
        et_city = findViewById(R.id.et_city);
        et_state = findViewById(R.id.et_state);
        et_country = findViewById(R.id.et_country);
        et_zipcode = findViewById(R.id.et_zipcode);
        et_county = findViewById(R.id.et_county);
        cardIv = findViewById(R.id.iv_card_image);


        ImageView companyCandidatesButton = findViewById(R.id.company_candidates_button);
        ImageView nameCandidatesButton = findViewById(R.id.name_candidates_button);
        ImageView job_title = findViewById(R.id.job_title);
        ImageView telephoneCandidatesButton = findViewById(R.id.telephone_candidates_button);
        ImageView emailCandidatesButton = findViewById(R.id.email_candidates_button);
        ImageView address1_button = findViewById(R.id.address1_button);
        ImageView address2_button = findViewById(R.id.address2_button);
        ImageView city_button = findViewById(R.id.city_button);
        ImageView county_button = findViewById(R.id.county_button);
        ImageView state_button = findViewById(R.id.state_button);
        ImageView country_button = findViewById(R.id.country_button);
        ImageView zipcode_button = findViewById(R.id.zipcode_button);
        LocalBroadcastManager.getInstance(ScanCardActivity.this).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            cameraPermission = new String[] {Manifest.permission.CAMERA, Manifest.permission.READ_MEDIA_IMAGES};
            //Storage Permission
            storagePermission = new String[] {Manifest.permission.READ_MEDIA_IMAGES};
        }
        else {
            cameraPermission = new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            //Storage Permission
            storagePermission = new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        }
        runTextRecognition(imagePath);
        companyCandidatesButton.setOnClickListener(view -> {
            if(companyCandidates!=null ){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(companyCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);

                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.COMPANYNAME);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(companyCandidates, companyEt);
            }
        });
        nameCandidatesButton.setOnClickListener(view -> {
            if(nameCandidates!=null ){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(nameCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);

                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.NAME);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                //popUpCandidates(nameCandidates, nameEt);
            }
        });
        job_title.setOnClickListener(view -> {

            if(jobTitleList!=null && !jobTitleList.isEmpty()){
                popUpCandidates(jobTitleList, et_job);
            }
            else if(nameCandidates!=null ){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(nameCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.JOB);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_job);
            }
        });
        telephoneCandidatesButton.setOnClickListener(view -> {

            if(phoneList!=null && !phoneList.isEmpty()){
                popUpCandidates(phoneList, telEt);
                // popUpCandidates(phoneNumberCandidates.keySet(), telEt);
            } else if(phoneNumberCandidates!=null && !phoneNumberCandidates.isEmpty()){
                 popUpCandidates(phoneNumberCandidates.keySet(), telEt);
            }
            else {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        });
        emailCandidatesButton.setOnClickListener(view -> {
            if(emailList!=null && !emailList.isEmpty()){
                popUpCandidates(emailList, emailEt);
                // popUpCandidates(emailCandidates.keySet(), emailEt);
            }
            else if(emailCandidates!=null && !emailCandidates.keySet().isEmpty()){
                 popUpCandidates(emailCandidates.keySet(), emailEt);
            }
            else {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        });
        address1_button.setOnClickListener(view -> {
            if(genericCandidates!=null){

                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.ADDRESS1);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, address_1Et);
            }

        });
        address2_button.setOnClickListener(view -> {
            if(genericCandidates!=null){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.ADDRESS2);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_address2);
            }

        }); city_button.setOnClickListener(view -> {
            if(genericCandidates!=null){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.CITY);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_city);
            }

        }); county_button.setOnClickListener(view -> {
            if(genericCandidates!=null){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.COUNTY);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_county);
            }

        }); state_button.setOnClickListener(view -> {
            if(genericCandidates!=null){

                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.STATE);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_state);
            }

        }); country_button.setOnClickListener(view -> {
            if(genericCandidates!=null){
                Set<String> primesWithoutDuplicates = new LinkedHashSet<>(genericCandidates);
                List<String> distinct_list = new ArrayList<>(primesWithoutDuplicates);
                Intent allRepsListIntent = new Intent(ScanCardActivity.this, RightSideDrawerListWithSearchActivity.class);
                allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.COUNTRY);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, (Serializable) distinct_list);
                //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
                allRepsListIntent.putExtras(bundle1);
                startActivity(allRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                // popUpCandidates(genericCandidates, et_country);
            }

        }); zipcode_button.setOnClickListener(view -> {
            if(zipList!=null && !zipList.isEmpty()){
                popUpCandidates(zipList, et_zipcode);
            }
            else  if(genericCandidates!=null && !genericCandidates.isEmpty()){
                 popUpCandidates(genericCandidates, et_zipcode);
            }
            else {
                Toast.makeText(this, "No Data Found", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (Objects.requireNonNull(intent.getAction()).equals(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = null;
                    if (bundle != null) {
                        rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    }
                    Serializable selectedObj = null;
                    if (bundle != null) {
                        selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
                    }
                    if (rightSideDrawerSelectionType == Constants.SelectionType.COMPANYNAME) {
                        if (selectedObj != null) {
                            companyEt.setText(selectedObj.toString());
                        }
                    }
                    else  if (rightSideDrawerSelectionType == Constants.SelectionType.NAME) {
                        if (selectedObj != null) {
                            nameEt.setText(selectedObj.toString());
                        }
                    }
                    else  if (rightSideDrawerSelectionType == Constants.SelectionType.JOB) {
                        if (selectedObj != null) {
                            et_job.setText(selectedObj.toString());
                        }
                    }
                    else  if (rightSideDrawerSelectionType == Constants.SelectionType.ADDRESS1) {
                        if (selectedObj != null) {
                            address_1Et.setText(selectedObj.toString());
                        }
                    }  else  if (rightSideDrawerSelectionType == Constants.SelectionType.ADDRESS2) {
                        if (selectedObj != null) {
                            et_address2.setText(selectedObj.toString());
                        }
                    }  else  if (rightSideDrawerSelectionType == Constants.SelectionType.CITY) {
                        if (selectedObj != null) {
                            et_city.setText(selectedObj.toString());
                        }
                    }  else  if (rightSideDrawerSelectionType == Constants.SelectionType.COUNTY) {
                        if (selectedObj != null) {
                            et_county.setText(selectedObj.toString());
                        }
                    }  else  if (rightSideDrawerSelectionType == Constants.SelectionType.STATE) {
                        if (selectedObj != null) {
                            et_state.setText(selectedObj.toString());
                        }
                    }  else  if (rightSideDrawerSelectionType == Constants.SelectionType.COUNTRY) {
                        if (selectedObj != null) {
                            et_country.setText(selectedObj.toString());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void popUpCandidates(Collection<String> candidates, final EditText input){
        if (!candidates.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            final CharSequence[] items = candidates.toArray(new CharSequence[0]);
            builder.setItems(items, (dialogInteface, i) -> input.setText(items[i]));
            builder.show();
        }
    }

    // actionbar menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater awesome = getMenuInflater();
        awesome.inflate(R.menu.scan_card_toolbar_menu, menu);
        for(int i = 0; i < menu.size(); i++) {
            MenuItem item = menu.getItem(i);
            if (item.getItemId() == R.id.done_button)
            {
                SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
                int end = spanString.length();
                spanString.setSpan(new RelativeSizeSpan(1.20f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                item.setTitle(spanString);
            }
            else  if (item.getItemId() == R.id.reset)
            {
                SpannableString spanString = new SpannableString(menu.getItem(i).getTitle().toString());
                int end = spanString.length();
                spanString.setSpan(new RelativeSizeSpan(1.20f), 0, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                item.setTitle(spanString);
            }

        }
        return true;
        /*getMenuInflater().inflate(R.menu.scan_card_toolbar_menu, menu);

        return super.onCreateOptionsMenu(menu);*/
    }

    // handle actionbar item clicks
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.done_button:
                String company= companyEt.getText().toString().trim();
                String name= nameEt.getText().toString().trim();
                String job= et_job.getText().toString().trim();
                String phone= telEt.getText().toString().trim();
                String email= emailEt.getText().toString().trim();
                String add1= address_1Et.getText().toString().trim();
                String add2= et_address2.getText().toString().trim();
                String city= et_city.getText().toString().trim();
                String county= et_county.getText().toString().trim();
                String state= et_state.getText().toString().trim();
                String country= et_country.getText().toString().trim();
                String zipcode= et_zipcode.getText().toString().trim();

                CardBean newContactBean = new CardBean();
                newContactBean.setCompany(company);
                newContactBean.setName(name);
                newContactBean.setJob(job);
                newContactBean.setPhone(phone);
                newContactBean.setEmail(email);
                newContactBean.setAddress1(add1);
                newContactBean.setAddress2(add2);
                newContactBean.setCity(city);
                newContactBean.setCounty(county);
                newContactBean.setState(state);
                newContactBean.setCountry(country);
                newContactBean.setZipcode(zipcode);
                sendBroadcastWithSelectedIndex(newContactBean);
                finish();

                return true;
            case R.id.camera_button:
                showImageImportDialog();
                return true;
            case R.id.reset:

                removeData();
                return true;
            case android.R.id.home:
                //Write your logic here
                closeActivity(null);
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }

    private void removeData() {

        emailList.clear();
        phoneList.clear();
        zipList.clear();
        jobTitleList.clear();
        phoneNumberCandidates.clear();
        emailCandidates.clear();
        genericCandidates.clear();
        nameCandidates.clear();
        companyCandidates.clear();
        companyEt.setText("");
        nameEt.setText("");
        et_job.setText("");
        telEt.setText("");
        emailEt.setText("");
        address_1Et.setText("");
        et_address2.setText("");
        et_city.setText("");
        et_state.setText("");
        et_country.setText("");
        et_county.setText("");
        et_zipcode.setText("");
        cardIv.setImageURI(null);
    }

    private void sendBroadcastWithSelectedIndex(CardBean newContactBean) {
        try {
            Intent intent;
            Bundle bundle = new Bundle();
            if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE);
                if (selectionType == Constants.SelectionType.BUSINESS_CARD)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE,  newContactBean);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE,  newContactBean);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
                intent.putExtras(bundle);
                // Broadcasting Selection Index to Receiver Activity
                LocalBroadcastManager.getInstance(ScanCardActivity.this).sendBroadcast(intent);
                closeActivity(null);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private void showImageImportDialog(){

        String Camera = this.getString(R.string.scan_camera);
        String Gallery = this.getString(R.string.scan_gallery);
        String messageTitle = this.getString(R.string.scan_message);

        TextView textView = new TextView(this);
        textView.setText(messageTitle);
        textView.setPadding(20, 30, 20, 30);
        textView.setTextSize(20F);
        textView.setBackgroundColor(Color.parseColor("#5680a6"));
        textView.setTextColor(Color.WHITE);
        String[] options = {Camera, Gallery};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        // set dialog's title
        dialog.setCustomTitle(textView);
        dialog.setItems(options, (dialog1, which) -> {
            if(which == 0){
                // select camera
                if(!checkCameraPermission()){
                    requestCameraPermission();
                }else{
                    pickCamera();
                }
            }
            if(which == 1){
                // select gallery
                if(!checkStoragePermission()){
                    requestStoragePermission();
                }else{
                    pickGallery();
                }
            }

        });
        dialog.create().show();
    }

    private void pickGallery() {
        // intent to pick image from gallery
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "CardWhere"); // title of picture
        values.put(MediaStore.Images.Media.DESCRIPTION, "Scan by CardWhere"); // description of picture
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, storagePermission, STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) == (PackageManager.PERMISSION_GRANTED);
        }else {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        }
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result_storage_permission;
        boolean result_camera_permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            result_storage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) == (PackageManager.PERMISSION_GRANTED);
        }
        else {
            result_storage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        }
        return result_camera_permission && result_storage_permission;
    }

    // handle permission result
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_REQUEST_CODE:
                if (grantResults.length > 0) {
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted) {
                        pickCamera();
                    } else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case STORAGE_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted) {
                        pickGallery();
                    } else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    // handle image result
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        // got image from camera
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                if (data != null) {
                    CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start(this);
                }
            }
            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(image_uri).setGuidelines(CropImageView.Guidelines.ON).start(this);
            }
        }

        //got cropped image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                // get image uri
                Uri resultUri = null;
                if (result != null) {
                    resultUri = result.getUri();
                }
                imageUpload = resultUri;

                // set image to image view

                runTextRecognition(resultUri);

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // if there is any error
                Exception error = null;
                if (result != null) {
                    error = result.getError();
                }
                Toast.makeText(this, " " + error, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void runTextRecognition(Uri imageUpload) {
        urs= new ArrayList<>();
        urs.add(imageUpload);
        cardIv.setImageURI(imageUpload);

        // get drawable bitmap from text recognition
        BitmapDrawable bitmapDrawable = (BitmapDrawable) cardIv.getDrawable();

        // text recognition
        if(bitmapDrawable.getBitmap()!=null){
             mSelectedImage = bitmapDrawable.getBitmap();
        }
        FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(mSelectedImage);

        FirebaseVisionTextRecognizer recognizer = FirebaseVision.getInstance()
                .getOnDeviceTextRecognizer();

        recognizer.processImage(image)
                .addOnSuccessListener(
                        this::processTextRecognitionResult)
                .addOnFailureListener(
                        e -> {
                            Toast.makeText(ScanCardActivity.this," Your Device is Not Supported ... ", Toast.LENGTH_SHORT).show();
                            // Task failed with an exception
                            // ...
                        });

    }

    private void processTextRecognitionResult(FirebaseVisionText texts) {

        List<FirebaseVisionText.TextBlock> blocks = texts.getTextBlocks();
        ArrayList<String>  outputLine  = new ArrayList<>();
        if (blocks.size() == 0) {
            Toast.makeText(this, "No text found", Toast.LENGTH_LONG).show();
            Log.d("TAG", "No text found");
            return;
        }
        //blocks
        for (int i = 0; i < blocks.size(); i++) {
            List<FirebaseVisionText.Line> lines = blocks.get(i).getLines();
            // lines
            for (int j = 0; j < lines.size(); j++) {
                // get each line for auto input
                outputLine.add(lines.get(j).getText());
            }
        }
        String delim = "\n";
        StringBuilder sb = new StringBuilder();
        int k = 0;
        while (k < outputLine.size() - 1) {
            sb.append(outputLine.get(k));
            sb.append(delim);
            k++;
        }
        sb.append(outputLine.get(k));
        String res = sb.toString();
        JSONObject jsonObject= new JSONObject();
        try {
            jsonObject.put("inputText",res);
            jsonObject.put("inputid",getDTTicks());
            Log.e("veera",jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Post_BusinessCard post_businessCard= new Post_BusinessCard();
        post_businessCard.setInputText(res);
        post_businessCard.setInputid(getDTTicks());

        sendData(post_businessCard);

        for (String snapshot : outputLine){
            for (String text : snapshot.split(",")){   // snapshot.split("\n")){
                int selected;
                selected = selectPhoneNumber(text, phoneNumberCandidates)
                        + selectEmail(text, emailCandidates);
                if (selected == 0) {
                    selectRest(text, genericCandidates);
                }
            }
        }
        String email = getBestCandidate(emailCandidates);
        if (StringUtils.isNotBlank(email)){
            // emailEt.setText(email);
            String namePart = email.substring(0, email.indexOf("@"));
            String companyPart = email.substring(email.indexOf("@")+1);
            companyPart = companyPart.substring(0, companyPart.indexOf("."));
            StringBuilder nameBuilder = new StringBuilder();
            int j = 0;
            for (String str : namePart.split("\\.")){
                j++;
                nameBuilder.append(str.substring(0, 1).toUpperCase());
                if (str.length() > 1){
                    nameBuilder.append(str.substring(1));
                }
                nameBuilder.append(" ");
            }
            if (j > 0) {
                nameCandidates.add(nameBuilder.toString().trim());
            }

            if (companyPart.length() > 1
                    && !companyPart.equals("googlemail")
                    && !companyPart.equals("gmail")
                    && !companyPart.equals("hotmail")
                    && !companyPart.equals("live")){
                companyCandidates.add(companyPart.substring(0, 1).toUpperCase()+companyPart.substring(1));
                companyCandidates.add(companyPart.toUpperCase());
                companyCandidates.add(companyPart);
            }
        }
        nameCandidates.addAll(genericCandidates);
        companyCandidates.addAll(genericCandidates);
        genericCandidates.addAll(phoneNumberCandidates.keySet());
        genericCandidates.addAll(emailCandidates.keySet());
    }

    private void sendData(Post_BusinessCard post_businessCard) {

        httpClient.addInterceptor(chain -> {
            Request original = chain.request();
            Request request = original.newBuilder()
                    // .header("authorization","Bearer "+ Hmf_Customer.getAccessToken())
                    .header("content-type", "application/json")
                    .method(original.method(), original.body())
                    .build();
            return chain.proceed(request);
        });
        OkHttpClient client = httpClient.build();
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BUSINESS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(client)
                .build();
        progressDialog = ProgressDialog.show(ScanCardActivity.this, "Please wait.", "Service Loading ..!", true);
        BusinessCardApi api = retrofit.create(BusinessCardApi.class);
        Call<ScanCardDetails> call = api.getResponce(post_businessCard);
        call.enqueue(new Callback<ScanCardDetails>() {
            @Override
            public void onResponse(@NonNull Call<ScanCardDetails> call, @NonNull Response<ScanCardDetails> response) {
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    if(response.body()!=null ){
                        Log.e("veera",response.body().toString());

                        scanCardDetails= response.body();
                        if(scanCardDetails.getCompanyName()!=null && !scanCardDetails.getCompanyName().isEmpty()){
                            companyEt.setText(scanCardDetails.getCompanyName());
                        }
                        if(scanCardDetails.getName()!=null && !scanCardDetails.getName().isEmpty()){
                            nameEt.setText(scanCardDetails.getName());
                        }
                        if(scanCardDetails.getTitle()!=null && !scanCardDetails.getTitle().isEmpty()){
                            jobTitleList.addAll(scanCardDetails.getTitle());
                            et_job.setText(scanCardDetails.getTitle().get(0));
                        }
                        if(scanCardDetails.getPhone()!=null && !scanCardDetails.getPhone().isEmpty()){
                            phoneList.addAll(scanCardDetails.getPhone());
                            telEt.setText(scanCardDetails.getPhone().get(0));
                        }
                        if(scanCardDetails.getEmail()!=null && !scanCardDetails.getEmail().isEmpty()){

                            emailList.addAll(scanCardDetails.getEmail());

                            emailEt.setText(scanCardDetails.getEmail().get(0));
                        }
                        if(scanCardDetails.getCity()!=null && !scanCardDetails.getCity().isEmpty()){
                            et_city.setText(scanCardDetails.getCity());
                        }

                        if(scanCardDetails.getStateCode()!=null && !scanCardDetails.getStateCode().equals("INTL")){
                            et_state.setText(scanCardDetails.getStateCode());
                        }else if(scanCardDetails.getState()!=null && !scanCardDetails.getState().isEmpty()){
                            et_state.setText(scanCardDetails.getState());
                        }

                        if(scanCardDetails.getCountry()!=null && !scanCardDetails.getCountry().isEmpty()){
                            et_country.setText(scanCardDetails.getCountry());
                        }
                        if(scanCardDetails.getZipCode()!=null && !scanCardDetails.getZipCode().isEmpty()){

                            zipList.addAll(scanCardDetails.getZipCode());
                            et_zipcode.setText(scanCardDetails.getZipCode().get(0));
                        }

                    }
                    else {
                        internalData();
                      //  MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), getResources().getString(R.string.error_failure));
                    }
                }
                else {
                    internalData();
                    // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), response.message());
                }
            }
            @Override
            public void onFailure(@NonNull Call<ScanCardDetails> call, @NonNull Throwable t) {
                progressDialog.dismiss();
              //  MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_error), getResources().getString(R.string.error_failure));
                internalData();

                 Toast.makeText(ScanCardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                 Log.e("veera",t.getMessage());

            }
        });
    }

    private void internalData() {

        String phoneNumber = getBestCandidate(phoneNumberCandidates);
        if (StringUtils.isNotBlank(phoneNumber)){
            telEt.setText(phoneNumber);
        }

        String email = getBestCandidate(emailCandidates);
        if (StringUtils.isNotBlank(email)) {
            emailEt.setText(email);
        }
    }


    private void selectRest(String text, List<String> genericCandidates) {
        List<String> toFilter = new ArrayList<>();
        boolean filter = false;
        for (String candidate : genericCandidates){
            if (candidate.contains(text)){
                filter = true;
                break;
            }
            if (text.contains(candidate)){
                toFilter.add(candidate);
            }
        }
        if (!filter){
            genericCandidates.add(text);
        }
        genericCandidates.removeAll(toFilter);
    }

    private int selectPhoneNumber(String text, Map<String, Integer> phoneNumberCandidates) {
        //At least 6 numbers, allow other characters
        String trimmed = text.toLowerCase().replaceAll("tel:","").replaceAll("mob:","").trim();
        if (phoneNumberCandidates.containsKey(trimmed)) {
            phoneNumberCandidates.put(trimmed, phoneNumberCandidates.get(trimmed) + 1);
        } else {
            int numCount = 0;

            for (char c : trimmed.toCharArray()) {
                if (Character.isDigit(c)) {
                    numCount++;
                }
                if (numCount == 7) {
                    phoneNumberCandidates.put(trimmed, 1);
                    return 1;
                }
            }
        }
        return 0;
    }
    private int selectEmail(String text, Map<String, Integer> emailCandidates) {
        int atPos = text.indexOf("@");
        int dotPos = text.lastIndexOf(".");
        //Very basic check to see if a text COULD BE an email address
        if (atPos != -1 && dotPos > atPos){
            String trimmed = text.trim();
            if (emailCandidates.containsKey(trimmed)){
                emailCandidates.put(trimmed, emailCandidates.get(trimmed)+1);
            } else {
                emailCandidates.put(trimmed, 1);
            }
            return 1;
        }
        return 0;
    }

    private String getBestCandidate(Map<String, Integer> candidates){
        int maxValue = 0;
        String bestCandidate ="";
        for (Map.Entry<String, Integer> candidate : candidates.entrySet()){
            if (candidate.getValue() > maxValue){
                maxValue = candidate.getValue();
                bestCandidate = candidate.getKey();
            }
        }
        //candidates.remove(bestCandidate);
        return bestCandidate;
    }
}

