package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;

public class SavedSearchesActivity extends BaseActivity implements View.OnClickListener {
    public static final String TAG = SavedSearchesActivity.class.getSimpleName();

    private RelativeLayout ss_this_week_callbacks, ss_today_callbacks, ss_all_callbacks, ss_my_last_100_contacts, ss_my_contacts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_searches);

        try {
            activityContext = SavedSearchesActivity.this;

            // Initializing UI References
            ss_this_week_callbacks = (RelativeLayout) findViewById(R.id.ss_this_week_callbacks);
            ss_today_callbacks = (RelativeLayout) findViewById(R.id.ss_today_callbacks);
            ss_all_callbacks = (RelativeLayout) findViewById(R.id.ss_all_callbacks);
            ss_my_last_100_contacts = (RelativeLayout) findViewById(R.id.ss_my_last_100_contacts);
            ss_my_contacts = (RelativeLayout) findViewById(R.id.ss_my_contacts);

            ss_this_week_callbacks.setOnClickListener(this);
            ss_today_callbacks.setOnClickListener(this);
            ss_all_callbacks.setOnClickListener(this);
            ss_my_last_100_contacts.setOnClickListener(this);
            ss_my_contacts.setOnClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.ss_this_week_callbacks:
                intent = new Intent(activityContext, CallbacksActivity.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_THIS_WEEK_CALLBACKS);
                break;
            case R.id.ss_today_callbacks:
                intent = new Intent(activityContext, CallbacksActivity.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_TODAY_CALLBACKS);
                break;
            case R.id.ss_all_callbacks:
                intent = new Intent(activityContext, CallbacksActivity.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_ALL_CALLBACKS);
                break;
            case R.id.ss_my_last_100_contacts:
                intent = new Intent(activityContext, MyLast100ContactsActivity.class);
                break;
            case R.id.ss_my_contacts:
                intent = new Intent(activityContext, MyContactsActivity.class);
                break;
        }

        if (intent != null) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }
}
