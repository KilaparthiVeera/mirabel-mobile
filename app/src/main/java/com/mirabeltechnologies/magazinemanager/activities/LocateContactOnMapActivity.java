package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.mirabeltechnologies.magazinemanager.Modules.DirectionFinder;
import com.mirabeltechnologies.magazinemanager.Modules.DirectionFinderListener;
import com.mirabeltechnologies.magazinemanager.Modules.Route;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.Map_Contacts_Adpter;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.beans.MyItem;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.services.DirectionsJSONParser;
import com.mirabeltechnologies.magazinemanager.util.GPSTracker;
import com.mirabeltechnologies.magazinemanager.util.MarkerClusterRenderer;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;
import java.util.TreeSet;

public class LocateContactOnMapActivity extends FragmentActivity implements OnMapReadyCallback, ClusterManager.OnClusterInfoWindowClickListener<MyItem>, ClusterManager.OnClusterItemInfoWindowClickListener<MyItem>, DirectionFinderListener {
    private LocationCallback mLocationCallback;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location lastLocation;
    private LocationRequest locationRequest;
    LatLng clientLocation;
    LatLng myLocation;
    boolean isAll;
    public ImageView img_waze,img_refresh;
    RelativeLayout footer_lay;
    LinearLayout distancelay;
    public MMTextView page_number_display;
    public ImageView right_nav_arrow, google_test;
    Button btn_note;
    private GoogleMap mMap;
    int count = 12;
    ArrayList<NearbyContacts> nearByContactsList;
    List<NearbyContacts> checkDataList;
    List<NearbyContacts> noteDataList;
    Polyline polyline;
    TextView txt_dis, txt_time, txt_title, txt_clear;
    public ArrayList<MyItem> itemlist;
    List<String> nameList;
    ArrayList<LatLng> markerPoints;
    GPSTracker gpsTracker;
    public ClusterManager<MyItem> mClusterManager;
    AlertDialog alertDialog;
    RecyclerView recyclerView;
    Map_Contacts_Adpter adapter;
    private List<Polyline> polylinePaths = new ArrayList<>();
    int start = 0;
    int end = Constants.ACTIVITY_PAGE_SIZE_INT;
    int display;
    String str_origin;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locate_contact_on_map);
        GlobalContent globalContent = GlobalContent.getInstance();
        gpsTracker = new GPSTracker(this);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
        distancelay = findViewById(R.id.distancelay);
        txt_dis = findViewById(R.id.txt_dis);
        txt_time = findViewById(R.id.txt_time);
        txt_title = findViewById(R.id.txt_title);
        img_waze = findViewById(R.id.img_waze);
        img_refresh=findViewById(R.id.img_refresh);
        btn_note = findViewById(R.id.btn_note);
        google_test = findViewById(R.id.google_test);
        txt_clear = findViewById(R.id.txt_clear);
        txt_clear.setVisibility(View.GONE);
        footer_lay = findViewById(R.id.footer);
        itemlist = new ArrayList<>();
        markerPoints = new ArrayList<>();
        savedInstanceState = getIntent().getExtras();
        page_number_display = findViewById(R.id.near_contacts_page_number_display);
        right_nav_arrow = findViewById(R.id.near_contacts_right_nav_arrow);

        google_test.setOnClickListener(v -> {
            String srcAdd = "&origin=" + markerPoints.get(0).latitude + "," + markerPoints.get(0).longitude;
            String desAdd = "&destination=" + markerPoints.get(markerPoints.size() - 1).latitude + "," + markerPoints.get(markerPoints.size() - 1).longitude;
            StringBuilder wayPoints = new StringBuilder();
            for (int j = 1; j < markerPoints.size() - 1; j++) {
                wayPoints.append(wayPoints.toString().equals("") ? "" : "%7C").append(markerPoints.get(j).latitude).append(",").append(markerPoints.get(j).longitude);
            }
            wayPoints.insert(0, "&waypoints=");
            String link="https://www.google.com/maps/dir/?api=1&travelmode=driving"+srcAdd+desAdd+wayPoints;
           // String url= "https://www.google.com/maps/dir/?api=1&origin=Paris,France&destination=Cherbourg,France&travelmode=driving&waypoints=Versailles,France%7CChartres,France%7CLe+Mans,France%7CCaen,France";

            Log.e("veera",link);
            final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(link));
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        });

        btn_note.setOnClickListener(v -> {
            if (nearByContactsList.size() < 0) {
                return;
            }
            if (nearByContactsList.size() == 1) {
                Intent showCreateActivityForContactIntent = new Intent(LocateContactOnMapActivity.this, CreateNewActivityForContactScreen.class);
                Bundle bundle = new Bundle();
                bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, String.valueOf(nearByContactsList.get(0).getGsCustomersID()));
                bundle.putLong(Constants.BUNDLE_CONTACT_REP_ID, nearByContactsList.get(0).getRepId());
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_NOTES);
                showCreateActivityForContactIntent.putExtras(bundle);
                startActivity(showCreateActivityForContactIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
               // Toast.makeText(this, "" + nearByContactsList.get(0).getName(), Toast.LENGTH_SHORT).show();

            } else if (nearByContactsList.size() > 1) {
                noteDataList = new ArrayList<>();
                for (int i = 0; i < nearByContactsList.size(); i++) {
                    for (int j = 0; j < nameList.size(); j++) {
                        if (nameList.get(j).equals(String.valueOf(nearByContactsList.get(i).getGsCustomersID()))) {
                            if (noteDataList.isEmpty()) {
                                noteDataList.add(nearByContactsList.get(i));
                            } else {
                                for (int k = 0; k < noteDataList.size(); k++) {
                                    if (!noteDataList.get(k).getGsCustomersID().equals(nearByContactsList.get(i).getGsCustomersID())) {
                                        noteDataList.add(nearByContactsList.get(i));
                                    }
                                }
                            }
                        }
                    }
                }

                if (noteDataList.size() == 1) {
                    Intent showCreateActivityForContactIntent = new Intent(LocateContactOnMapActivity.this, CreateNewActivityForContactScreen.class);
                    Bundle bundle = new Bundle();
                    bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, String.valueOf(noteDataList.get(0).getGsCustomersID()));
                    bundle.putLong(Constants.BUNDLE_CONTACT_REP_ID, noteDataList.get(0).getRepId());
                    bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_NOTES);
                    showCreateActivityForContactIntent.putExtras(bundle);
                    startActivity(showCreateActivityForContactIntent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                    Toast.makeText(this, "" + noteDataList.get(0).getName(), Toast.LENGTH_SHORT).show();
                } else if (noteDataList.size() > 1) {

                    /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
     noteDataList = noteDataList.stream()
             .collect(collectingAndThen(toCollection(() -> new TreeSet<>(comparingInt(NearbyContacts::getGsCustomersID))),
                     ArrayList::new));
 }*/

                    Set set = new TreeSet((o1, o2) -> {
                        if (((NearbyContacts) o1).getGsCustomersID().equals(((NearbyContacts) o2).getGsCustomersID())) {
                            return 0;
                        }
                        return 1;
                    });
                    set.addAll(noteDataList);
                    List newList = new ArrayList(set);
                    showInputDialog(newList);
                }
            }
        });

        img_refresh.setOnClickListener(v -> {
            mMap.clear();
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(LocateContactOnMapActivity.this);
            startMarkerRoute(markerPoints);
        });
        txt_clear.setOnClickListener(v -> {
            if (polylinePaths != null) {
                for (Polyline polyline : polylinePaths) {
                    polyline.remove();
                    txt_dis.setText("");
                    txt_time.setText("");
                    distancelay.setVisibility(View.GONE);
                    txt_clear.setVisibility(View.GONE);
                }
            }
        });
        right_nav_arrow.setOnClickListener(v -> {
            int diff = nearByContactsList.size() - end;
            if (diff <= 10) {
                mMap.clear();
                start = display;
                end = end + diff;
                right_nav_arrow.setVisibility(View.GONE);
                if (!MMProgressDialog.isProgressDialogShown)
                    MMProgressDialog.showProgressDialog(LocateContactOnMapActivity.this);
                DownloadLocation downloadLocCoordinates = new DownloadLocation();
                downloadLocCoordinates.execute();
            } else {
                mMap.clear();
                start = display;
                end = end + 10;
                if (!MMProgressDialog.isProgressDialogShown)
                    MMProgressDialog.showProgressDialog(LocateContactOnMapActivity.this);
                DownloadLocation downloadLocCoordinates = new DownloadLocation();
                downloadLocCoordinates.execute();
            }
        });

        if (savedInstanceState != null) {
            if (!Objects.requireNonNull(savedInstanceState.getString("Type")).equals("PlotAll")) {
                isAll = false;
                nearByContactsList = new ArrayList<>();
                NearbyContacts nearbyContacts = new NearbyContacts();
                nearbyContacts.setAddress1(savedInstanceState.getString("address"));
                nearbyContacts.setName(savedInstanceState.getString("name"));
                nearbyContacts.setCustomer(savedInstanceState.getString("company"));
                nearbyContacts.setGsCustomersID(savedInstanceState.getInt("custId"));
                nearbyContacts.setRepId(savedInstanceState.getInt("repId"));
                nearbyContacts.setCheck(true);
                nearByContactsList.add(0, nearbyContacts);
                if (savedInstanceState.getString("name") != null && !savedInstanceState.getString("name").isEmpty()) {

                    txt_title.setText(savedInstanceState.getString("name"));
                } else {
                    txt_title.setText(savedInstanceState.getString("company"));
                }
            } else {
                checkDataList=new ArrayList<>();
                nearByContactsList = new ArrayList<>();

               // checkDataList.addAll(savedInstanceState.getParcelableArrayList("Data")) ;
                checkDataList.addAll(globalContent.getNearestContactList()) ;
                for(int i=0;i<checkDataList.size();i++){

                    if(checkDataList.get(i).isCheck()){

                        nearByContactsList.add(checkDataList.get(i));
                    }
                }

                Log.e("hima", "" + nearByContactsList.toString());
                isAll = true;
                txt_title.setText("Contacts Near You");
            }
        }
        if (nearByContactsList.size() <= Constants.ACTIVITY_PAGE_SIZE_INT) {
            footer_lay.setVisibility(View.GONE);
        } else {
            footer_lay.setVisibility(View.VISIBLE);
        }
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        if (location != null) {
                            myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        }
                    });
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(5000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        myLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    }
                }


            };
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showInputDialog(List<NearbyContacts> noteDataList) {
        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.map_contacts_dialog, viewGroup, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        alertDialog.show();
        int s = noteDataList.size();
        recyclerView = dialogView.findViewById(R.id.contacts_recycler);
        adapter = new Map_Contacts_Adpter(LocateContactOnMapActivity.this, noteDataList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(s);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setAllGesturesEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setTiltGesturesEnabled(true);
            mMap.getUiSettings().setRotateGesturesEnabled(true);
            // mMap.getUiSettings().setMapToolbarEnabled(false);
            mMap.setOnMapClickListener(latLng -> img_waze.setVisibility(View.GONE));
        }
        // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(26.12231, -80.14338),11));
        if (lastLocation != null) {
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lastLocation.getLatitude(), lastLocation.getLongitude()), 11));
            }
        } else if (gpsTracker.getLocation() != null) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude()), 11));
        }

        if (!MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.showProgressDialog(LocateContactOnMapActivity.this);

        DownloadLocation downloadLocCoordinates = new DownloadLocation();
        downloadLocCoordinates.execute();
    }

    @Override
    public void onClusterItemInfoWindowClick(MyItem myItem) {
        System.out.println("== oncluster item window clicked");
        clientLocation = myItem.getPosition();
        img_waze.setVisibility(View.GONE);
        // String str_origin = (26.12231 + "," + -80.14338);
        if (lastLocation != null) {
            str_origin = (lastLocation.getLatitude() + "," + lastLocation.getLongitude());
        } else if (gpsTracker.getLocation() != null) {

            str_origin = (gpsTracker.getLatitude() + "," + gpsTracker.getLongitude());
        }
        // Destination of route
        String str_dest = (clientLocation.latitude + "," + clientLocation.longitude);
        //draw route map from my current location to the client location
        if (polyline == null) {
            if (lastLocation != null) {
                try {
                    new DirectionFinder(this, str_origin, str_dest).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            } else {
                Toast.makeText(LocateContactOnMapActivity.this, "Please enable gps.", Toast.LENGTH_SHORT).show();
            }
        } else {
            polyline.remove();
            if (lastLocation != null) {
                try {
                    new DirectionFinder(this, str_origin, str_dest).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(LocateContactOnMapActivity.this, "Please enable gps.", Toast.LENGTH_SHORT).show();
            }
        }
    }
    @Override
    public void onDirectionFinderStart() {
        if (polylinePaths != null) {
            for (Polyline polyline : polylinePaths) {
                polyline.remove();
            }
        }
    }
    @Override
    public void onDirectionFinderSuccess(List<Route> route) {
        polylinePaths = new ArrayList<>();
        if (route != null && !route.isEmpty()) {
            distancelay.setVisibility(View.VISIBLE);
            txt_clear.setVisibility(View.VISIBLE);
            for (Route routes : route) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(routes.startLocation, 14));
                txt_time.setText(routes.duration.text);
                txt_dis.setText(routes.distance.text);
                // String desname= PlaceDetails.plname;
                PolylineOptions polylineOptions = new PolylineOptions().
                        geodesic(true).
                        color(Color.parseColor("#4285f4")).
                        width(12);
                for (int i = 0; i < routes.points.size(); i++)
                    polylineOptions.add(routes.points.get(i));
                polylinePaths.add(mMap.addPolyline(polylineOptions));

            }
        } else {
            distancelay.setVisibility(View.GONE);
            txt_clear.setVisibility(View.GONE);
            Toast.makeText(LocateContactOnMapActivity.this, "Something Went wrong ..please check the Location Once Again..!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onClusterInfoWindowClick(Cluster<MyItem> cluster) {
        clientLocation = cluster.getPosition();
        // String str_origin = (26.12231 + "," + -80.14338);
        if (lastLocation != null) {
            str_origin = (lastLocation.getLatitude() + "," + lastLocation.getLongitude());
        } else if (gpsTracker.getLocation() != null) {
            str_origin = (gpsTracker.getLatitude() + "," + gpsTracker.getLongitude());
        }
        // Destination of route
        String str_dest = (clientLocation.latitude + "," + clientLocation.longitude);
        //draw route map from my current location to the client location
        if (polyline == null) {
            if (lastLocation != null) {
                try {
                    new DirectionFinder(this, str_origin, str_dest).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(LocateContactOnMapActivity.this, "Please enable gps.", Toast.LENGTH_SHORT).show();
            }
        } else {
            polyline.remove();
            if (lastLocation != null) {
                try {
                    new DirectionFinder(this, str_origin, str_dest).execute();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(LocateContactOnMapActivity.this, "Please enable gps.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    class DownloadLocation extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... voids) {
            if (nearByContactsList.size() <= Constants.ACTIVITY_PAGE_SIZE_INT) {
                for (NearbyContacts obj : nearByContactsList) {
                    String address1 = "";
                    String name1;
                    String company;
                    String cId;
                    boolean check;
                    if (isAll) {
                        if (obj.getAddress1() != null && !obj.getAddress1().isEmpty()) {
                            address1 = obj.getAddress1();

                        }
                        if (obj.getAddress2() != null && !obj.getAddress2().isEmpty()) {
                            address1 = address1.concat(obj.getAddress2()).concat(",");
                        }
                        if (obj.getCity() != null && !obj.getCity().isEmpty()) {
                            address1 = address1.concat(obj.getCity()).concat(",");
                        }
                        if (obj.getCountry() != null && !obj.getCountry().isEmpty()) {
                            address1 = address1.concat(obj.getCountry()).concat(",");
                        }
                        if (obj.getZipPostalCode() != null && !obj.getZipPostalCode().isEmpty()) {
                            address1 = address1.concat(obj.getZipPostalCode());
                        }
                        name1 = obj.getName();
                        company = obj.getCustomer();
                        cId = String.valueOf(obj.getGsCustomersID());
                        check=obj.isCheck();
                    } else {
                        address1 = obj.getAddress1();
                        name1 = obj.getName();
                        company = obj.getCustomer();
                        cId = String.valueOf(obj.getGsCustomersID());
                        check=obj.isCheck();
                    }
                    getLocationFromAddress(name1, address1, company, cId,check);
                }
            } else {
                for (display = start; display < end; display++) {
                    NearbyContacts obj = nearByContactsList.get(display);
                    String address1 = "";
                    String name1;
                    String company;
                    String cId;
                    boolean check;
                    if (isAll) {
                        if (obj.getAddress1() != null && !obj.getAddress1().isEmpty()) {
                            address1 = obj.getAddress1();
                        }
                        if (obj.getAddress2() != null && !obj.getAddress2().isEmpty()) {
                            address1 = address1.concat(obj.getAddress2()).concat(",");
                        }
                        if (obj.getCity() != null && !obj.getCity().isEmpty()) {
                            address1 = address1.concat(obj.getCity()).concat(",");
                        }
                        if (obj.getCountry() != null && !obj.getCountry().isEmpty()) {
                            address1 = address1.concat(obj.getCountry()).concat(",");
                        }
                        if (obj.getZipPostalCode() != null && !obj.getZipPostalCode().isEmpty()) {
                            address1 = address1.concat(obj.getZipPostalCode());
                        }
                        name1 = obj.getName();
                        company = obj.getCustomer();
                        cId = String.valueOf(obj.getGsCustomersID());
                        check=obj.isCheck();

                    } else {
                        address1 = obj.getAddress1();
                        name1 = obj.getName();
                        company = obj.getCustomer();
                        cId = String.valueOf(obj.getGsCustomersID());
                        check=obj.isCheck();
                    }
                    getLocationFromAddress(name1, address1, company, cId,check);
                }
            }
            return display;
        }
        @SuppressLint("DefaultLocale")
        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);
            Log.e("ram", String.valueOf(nearByContactsList.size()));
            page_number_display.setText(String.format(" %d - %d of %d", start + 1, display, nearByContactsList.size()));
            if (markerPoints.size() >= 10) {
                return;
            }
            // Adding new item to the ArrayList
            markerPoints.add(myLocation);
            if (itemlist != null && itemlist.size() > 0) {
                markerPoints.add(itemlist.get(itemlist.size()-1).getPosition());
                for (int i = 0; i < itemlist.size()-1; i++) {
                    markerPoints.add(itemlist.get(i).getPosition());
                }
            }
            else {

                Toast.makeText(LocateContactOnMapActivity.this, "No valid Address Found", Toast.LENGTH_SHORT).show();
            }
            // markerPoints.add(itemlist.get(itemlist.size()-1).getPosition());
           
            startMarkerRoute(markerPoints);
        }
    }

    public void getLocationFromAddress(String name, String addres, String company, String cId, boolean check) {
        Geocoder coder = new Geocoder(this, Locale.getDefault());
        List<Address> address;
        LatLng p1;
        try {
            address = coder.getFromLocationName(addres, 1);
            if (address == null) {
                return;
            }
            System.out.println("==1==address = " + address);
            for (Address location : address) {
                location.getLatitude();
                location.getLongitude();
                p1 = new LatLng(location.getLatitude(),
                        location.getLongitude());
                System.out.println("====print new address== = " + p1);
                if(check){

                    if (name != null && !name.isEmpty()) {
                        itemlist.add(new MyItem(p1, name, addres, cId));
                    } else {
                        itemlist.add(new MyItem(p1, company, addres, cId));
                    }
                }
            }
            // mMap.addMarker(new MarkerOptions().position(p1).title(locname).snippet(addres)).setTag(locname);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        lastLocation = task.getResult();
                    }
                });
    }
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }

    @Override
    protected void onStart() {
        super.onStart();
        getLastLocation();
        startLocationUpdates();
    }

    @Override
    protected void onPause() {
        stopLocationUpdates();
        super.onPause();
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    protected void startMarkerRoute(ArrayList<LatLng> markerPoints) {
        MMProgressDialog.hideProgressDialog();
        mClusterManager = new ClusterManager<>(this, mMap);
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mMap.setOnInfoWindowClickListener(mClusterManager); //added
        mMap.setOnCameraIdleListener(mClusterManager);
        mClusterManager.setOnClusterItemInfoWindowClickListener(this);
        mClusterManager.setOnClusterInfoWindowClickListener(this);

        if (markerPoints.size() >= 2) {
            MarkerOptions options = new MarkerOptions();
            options.position(markerPoints.get(0));
            options.icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location_on));
            options.title("My Location");
            mMap.addMarker(options);
            LatLng origin = markerPoints.get(0);
            LatLng dest = markerPoints.get(1);
            // Getting URL to the Google Directions API
            String url = getDirectionsUrl(origin, dest);
            Log.e("hima", url);
            DownloadTask downloadTask = new DownloadTask();
            // Start downloading json data from Google Directions API
            downloadTask.execute(url);
        }

//new ly added
        mClusterManager.setOnClusterClickListener(cluster -> {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cluster.getPosition(), (float) Math.floor(mMap
                    .getCameraPosition().zoom + 1)));
            count++;
            return false;
        });

        mClusterManager.setOnClusterItemClickListener(item -> {
            System.out.println("cluster item clicked");
            // set info window data
            new MyItem(item.getPosition(), item.getTitle(), item.getSnippet(), item.getcId());
            img_waze.setVisibility(View.VISIBLE);
            img_waze.setOnClickListener(v -> {
                try {
                    // Launch Waze to look for address:
                    String url = "https://waze.com/ul?q=" + item.getSnippet() + "&navigate=yes&zoom=17";
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    intent.setPackage("com.waze");
                    startActivity(intent);
                } catch (ActivityNotFoundException ex) {
                    // If Waze is not installed, open it in Google Play:
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.waze"));
                    startActivity(intent);
                }
            });
            return false;
        });
        mClusterManager.addItems(this.itemlist);
        mClusterManager.cluster();
        mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowGoogleMap(this, this.itemlist, "item"));
        mClusterManager.getClusterMarkerCollection().setOnInfoWindowAdapter(new CustomInfoWindowGoogleMap(this, this.itemlist, "cluster"));
        // custom markar Add line
        mClusterManager.setRenderer(new MarkerClusterRenderer(this, mMap, mClusterManager));
    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Sensor enabled
        String sensor = "sensor=false";
        // Waypoints
        StringBuilder waypoints = new StringBuilder();
        for (int i = 2; i < markerPoints.size(); i++) {
            LatLng point = markerPoints.get(i);
            if (i == 2)
                waypoints = new StringBuilder("waypoints=optimize:true|");
            waypoints.append(point.latitude).append(",").append(point.longitude).append('|');
        }
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + waypoints;
        // Output format
        String output = "json";
        // Building the url to the web service
        return String.format("https://maps.googleapis.com/maps/api/directions/%s?%s&key=%s", output, parameters, Constants.GOOGLE_API_KEY);
    }

    private String downloadUrl(String strUrl) throws IOException {
        Log.e("veera", strUrl);
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb;
            sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception downloading", e.toString());
        } finally {
            assert iStream != null;
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @SuppressLint("StaticFieldLeak")
    private class DownloadTask extends AsyncTask<String, Void, String> {
        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }
        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }
    /**
     * A class to parse the Google Places in JSON format
     */
    @SuppressLint("StaticFieldLeak")
    private class ParserTask extends AsyncTask<String, Integer, List<Route>> {
        // Parsing the data in non-ui thread
        @Override
        protected List<Route> doInBackground(String... jsonData) {
            List<Route> routes;
            routes = null;
            try {
                DirectionsJSONParser parser = new DirectionsJSONParser();
                // Starts parsing data
                routes = parser.parse(jsonData[0]);
            } catch (Exception e) {

                e.printStackTrace();
            }
            return routes;
        }
        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<Route> routes) {
            if(routes!=null && !routes.isEmpty()){
                for (Route route : routes) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(route.endLocation, 14));
                    PolylineOptions polylineOptions = new PolylineOptions().
                            geodesic(true).
                            color(Color.parseColor("#4285f4")).
                            width(12);
                    for (int i = 0; i < route.points.size(); i++)
                        polylineOptions.add(route.points.get(i));
                    polylinePaths.add(mMap.addPolyline(polylineOptions));
                }
            }
            else {

                Toast.makeText(LocateContactOnMapActivity.this, "Results are Not Found..", Toast.LENGTH_SHORT).show();
            }

        }
    }
    public class CustomInfoWindowGoogleMap implements GoogleMap.InfoWindowAdapter {
        private Context context;
        List<MyItem> itemlist;
        String type;
        CustomInfoWindowGoogleMap(Context ctx, ArrayList<MyItem> itemlist, String type) {

            this.context = ctx;
            this.itemlist = itemlist;
            this.type = type;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            return null;
        }

        @Override
        public View getInfoContents(Marker marker) {
            @SuppressLint("InflateParams") View myContentsView = ((Activity) context).getLayoutInflater()
                    .inflate(R.layout.custom_marker_details_layout, null);
            TextView title = myContentsView.findViewById(R.id.title);
            TextView address_text = myContentsView.findViewById(R.id.address_text);
            if (type.equals("item")) {
                nameList = new ArrayList<>();
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < itemlist.size(); i++) {
                    if (marker.getPosition().equals(itemlist.get(i).getPosition())) {
                        // Log.e("ram",itemlist.get(i).getTitle());
                        nameList.add(itemlist.get(i).getcId());
                        result.append(itemlist.get(i).getTitle()).append(", ");
                        Log.e("ram", nameList.toString());
                    }
                }
                String title_withoutLastComma = result.substring(0, result.length() - ", ".length());
                title.setText(title_withoutLastComma);
                address_text.setText(marker.getSnippet());
            } else {
                StringBuilder result = new StringBuilder();
                for (int i = 0; i < itemlist.size(); i++) {
                    result.append(itemlist.get(i).getTitle()).append(", ");
                }
                String title_withoutLastComma = result.substring(0, result.length() - ", ".length());
                title.setText(title_withoutLastComma);
            }
            return myContentsView;
        }
    }
}