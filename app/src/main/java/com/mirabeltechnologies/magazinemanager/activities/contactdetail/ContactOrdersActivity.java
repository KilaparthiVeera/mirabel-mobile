package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.adapters.ContactOrdersAdapter;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrder;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;

import java.util.List;

public class ContactOrdersActivity extends BaseActivity {
    public static final String TAG = ContactOrdersActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom;
    private ListView contact_orders_list_view;
    private MMTextView co_total_orders_count, co_total_barter_value, co_total_gross_value, co_total_net_value;
    private DetailContact selectedContactDetails = null;
    private String selectedCustomerId;
    private List<ContactOrder> contactOrders;
    private ContactOrdersAdapter contactOrdersAdapter;

    private int totalOrdersCount = 0;
    private float totalGross = 0.0f, totalBarter = 0.0f, totalNet = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_orders);

        try {
            activityContext = ContactOrdersActivity.this;

            requestFrom = Constants.RequestFrom.CONTACT_ORDERS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
                selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
            }

            if (selectedContactDetails != null) {
                contactOrders = selectedContactDetails.getContactOrders();
            }

            // Initializing UI References
            contact_orders_list_view = (ListView) findViewById(R.id.contact_orders_list_view);
            co_total_orders_count = (MMTextView) findViewById(R.id.co_total_orders_count);
            co_total_barter_value = (MMTextView) findViewById(R.id.co_total_barter_value);
            co_total_gross_value = (MMTextView) findViewById(R.id.co_total_gross_value);
            co_total_net_value = (MMTextView) findViewById(R.id.co_total_net_value);

            contactOrdersAdapter = new ContactOrdersAdapter(activityContext, contactOrders);
            contact_orders_list_view.setAdapter(contactOrdersAdapter);

            contact_orders_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    showOrdersForSelectedYear(position);
                }
            });

            if (contactOrders == null || contactOrders.isEmpty()) {
                contact_orders_list_view.addHeaderView(no_records_found_text_view);
            } else {
                for (ContactOrder order : contactOrders) {
                    totalOrdersCount = totalOrdersCount + order.getOrdersCount();
                    totalGross = totalGross + order.getGross();
                    totalBarter = totalBarter + order.getBarter();
                    totalNet = totalNet + order.getNet();
                }

                co_total_orders_count.setText(String.valueOf(totalOrdersCount));
                co_total_barter_value.setText(String.format("$%,.2f", totalBarter));
                co_total_gross_value.setText(String.format("$%,.2f", totalGross));
                co_total_net_value.setText(String.format("$%,.2f", totalNet));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void showOrdersForSelectedYear(int index) {
        ContactOrder order = contactOrders.get(index);

        Intent showOrderDetailsIntent = new Intent(activityContext, ContactOrderItemsListActivity.class);
        showOrderDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CUSTOMER_ID, selectedCustomerId);
        showOrderDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_ORDER, order);
        showOrderDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        startActivity(showOrderDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
}
