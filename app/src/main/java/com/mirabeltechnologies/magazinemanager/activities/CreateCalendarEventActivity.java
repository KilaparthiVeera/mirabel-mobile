package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TimePicker;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEventDetails;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.NewCalendarEvent;
import com.mirabeltechnologies.magazinemanager.beans.RecurrenceRules;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.CalendarModel;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CreateCalendarEventActivity extends BaseActivity implements AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {
    public static final String TAG = CreateCalendarEventActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom;
    private MMEditText calendar_event_title, calendar_event_notes, calendar_event_recurrence_repeat_every, calendar_event_recurrence_occurrences;
    MMTextView calendar_event_screen_title, recurrence_event_not_editable_title, calendar_event_meeting_type, calendar_event_start_date, calendar_event_start_time, calendar_event_end_date, calendar_event_end_time,
            calendar_event_recurrence_repeat_every_title, calendar_event_recurrence_repeat_by_result, calendar_event_recurrence_until_date;
    public Switch calendar_event_all_day_switch, calendar_event_recurrence_switch;
    private LinearLayout calendar_event_recurrence_layout, calendar_event_recurrence_repeat_on_layout, calendar_event_recurrence_repeat_by_layout, calendar_event_recurrence_range_result_layout, calendar_event_recurrence_occurrences_layout;
    private Spinner calendar_event_recurrence_pattern_spinner, calendar_event_recurrence_repeat_by_spinner, calendar_event_recurrence_monthly_day_of_week_spinner, calendar_event_recurrence_range_spinner;
    private CheckBox repeat_on_cb_all, repeat_on_cb_sun, repeat_on_cb_mon, repeat_on_cb_tue, repeat_on_cb_wed, repeat_on_cb_thu, repeat_on_cb_fri, repeat_on_cb_sat;
    MMButton calendar_event_reset_button, calendar_event_delete_button, calendar_event_save_button;

    private CalendarModel calendarModel;
    private CommonModel commonModel;
    private ArrayList<MasterData> allMeetingTypes = null;
    private NewCalendarEvent newCalendarEvent = null;
    private RecurrenceRules recurrenceRules = new RecurrenceRules();
    private SimpleDateFormat dateParser, dateTimeParser;
    private String selectedCalendarDate, selectedCalendarEventId, currentDate, preSelectedDate = "", preSelectedTime = "", currentTime = "";
    private boolean isEventEditing = false, isRecurrenceEvent = false, isSpinnersResetInitially = false;
    private CalendarEventDetails calendarEventDetails = null;

    private Map<String, String> patternTypesHashMap;
    private ArrayList<String> patternTypesKeyValues, repeatByOptionsArrayList, dayOfWeekArrayList, rangeTypesArrayList;
    SpinnerAdapter patternSpinnerAdapter, repeatBySpinnerAdapter, dayOfWeekSpinnerAdapter, rangeSpinnerAdapter;
    ArrayList<String> selectedWeekDays = new ArrayList<>();

    private final String PATTERN_DAILY = "Daily";
    private final String PATTERN_WEEKLY = "Weekly";
    private final String PATTERN_MONTHLY = "Monthly";
    private final String PATTERN_YEARLY = "Yearly";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_calendar_event);

        try {
            activityContext = CreateCalendarEventActivity.this;

            // initializing super class variables
            requestFrom = Constants.RequestFrom.ADD_CALENDAR_EVENT;

            calendarModel = new CalendarModel(activityContext, this, requestFrom);
            commonModel = new CommonModel(activityContext, this, requestFrom);

            currentDate = Utility.formatDate(new Date(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
            dateParser = new SimpleDateFormat(Constants.CALENDAR_SELECTED_DATE_WITH_TIME_FORMAT,Locale.getDefault());
            dateTimeParser = new SimpleDateFormat(Constants.CALENDAR_NEW_EVENT_DATE_TIME_FORMAT,Locale.getDefault());

            newCalendarEvent = new NewCalendarEvent();

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCalendarDate = bundle.getString(Constants.BUNDLE_SELECTED_CALENDAR_DATE);
                isEventEditing = bundle.getBoolean(Constants.BUNDLE_IS_CALENDAR_EVENT_EDITING);
            }

            if (isEventEditing) {
                selectedCalendarEventId = bundle.getString(Constants.BUNDLE_SELECTED_CALENDAR_EVENT_ID);
                calendarEventDetails = (CalendarEventDetails) bundle.getSerializable(Constants.BUNDLE_SELECTED_CALENDAR_EVENT_DETAILS);

                if (calendarEventDetails != null && !calendarEventDetails.getRecurrenceRule().isEmpty()) {
                    isRecurrenceEvent = true;
                }
            }

            // Initializing UI References
            calendar_event_screen_title = findViewById(R.id.calendar_event_screen_title);
            recurrence_event_not_editable_title = findViewById(R.id.recurrence_event_not_editable_title);
            calendar_event_title = findViewById(R.id.calendar_event_title);
            calendar_event_meeting_type = findViewById(R.id.calendar_event_meeting_type);
            calendar_event_start_date = findViewById(R.id.calendar_event_start_date);
            calendar_event_start_time = findViewById(R.id.calendar_event_start_time);
            calendar_event_end_date = findViewById(R.id.calendar_event_end_date);
            calendar_event_end_time = findViewById(R.id.calendar_event_end_time);
            calendar_event_all_day_switch = findViewById(R.id.calendar_event_all_day_switch);
            calendar_event_notes = findViewById(R.id.calendar_event_notes);
            calendar_event_recurrence_switch = findViewById(R.id.calendar_event_recurrence_switch);

            calendar_event_recurrence_layout = findViewById(R.id.calendar_event_recurrence_layout);
            calendar_event_recurrence_pattern_spinner = findViewById(R.id.calendar_event_recurrence_pattern_spinner);
            calendar_event_recurrence_repeat_every = findViewById(R.id.calendar_event_recurrence_repeat_every);
            calendar_event_recurrence_repeat_every_title = findViewById(R.id.calendar_event_recurrence_repeat_every_title);
            calendar_event_recurrence_repeat_on_layout = findViewById(R.id.calendar_event_recurrence_repeat_on_layout);
            repeat_on_cb_all = findViewById(R.id.repeat_on_cb_all);
            repeat_on_cb_sun = findViewById(R.id.repeat_on_cb_sun);
            repeat_on_cb_mon = findViewById(R.id.repeat_on_cb_mon);
            repeat_on_cb_tue = findViewById(R.id.repeat_on_cb_tue);
            repeat_on_cb_wed = findViewById(R.id.repeat_on_cb_wed);
            repeat_on_cb_thu = findViewById(R.id.repeat_on_cb_thu);
            repeat_on_cb_fri = findViewById(R.id.repeat_on_cb_fri);
            repeat_on_cb_sat = findViewById(R.id.repeat_on_cb_sat);
            calendar_event_recurrence_repeat_by_layout = findViewById(R.id.calendar_event_recurrence_repeat_by_layout);
            calendar_event_recurrence_repeat_by_spinner = findViewById(R.id.calendar_event_recurrence_repeat_by_spinner);
            calendar_event_recurrence_monthly_day_of_week_spinner = findViewById(R.id.calendar_event_recurrence_monthly_day_of_week_spinner);
            calendar_event_recurrence_repeat_by_result = findViewById(R.id.calendar_event_recurrence_repeat_by_result);
            calendar_event_recurrence_range_spinner = findViewById(R.id.calendar_event_recurrence_range_spinner);
            calendar_event_recurrence_range_result_layout = findViewById(R.id.calendar_event_recurrence_range_result_layout);
            calendar_event_recurrence_occurrences_layout = findViewById(R.id.calendar_event_recurrence_occurrences_layout);
            calendar_event_recurrence_occurrences = findViewById(R.id.calendar_event_recurrence_occurrences);
            calendar_event_recurrence_until_date = findViewById(R.id.calendar_event_recurrence_until_date);
            calendar_event_reset_button = findViewById(R.id.calendar_event_reset_button);
            calendar_event_delete_button = findViewById(R.id.calendar_event_delete_button);
            calendar_event_save_button = findViewById(R.id.calendar_event_save_button);

            // Preparing Data for Spinners
            patternTypesHashMap = new LinkedHashMap<>();
            patternTypesHashMap.put("Daily", "Daily");
                /*patternTypesHashMap.put("Weekdays (Mon-Fri)", "Weekly");
                patternTypesHashMap.put("Mon, Wed, Fri", "Weekly");
                patternTypesHashMap.put("Tues, Thur", "Weekly");*/
            patternTypesHashMap.put("Weekly", "Weekly");
            patternTypesHashMap.put("Monthly", "Monthly");
            patternTypesHashMap.put("Yearly", "Yearly");

            patternTypesKeyValues = new ArrayList<>(patternTypesHashMap.keySet());

            repeatByOptionsArrayList = new ArrayList<>();
            repeatByOptionsArrayList.add("Day of Month");
            repeatByOptionsArrayList.add("Day of Week");

            dayOfWeekArrayList = new ArrayList<>();
            dayOfWeekArrayList.add("First");
            dayOfWeekArrayList.add("Second");
            dayOfWeekArrayList.add("Third");
            dayOfWeekArrayList.add("Fourth");
            dayOfWeekArrayList.add("Last");

            rangeTypesArrayList = new ArrayList<>();
            rangeTypesArrayList.add("Until Date");
            rangeTypesArrayList.add("For");
            rangeTypesArrayList.add("Forever");

            patternSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, patternTypesKeyValues);
            patternSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            calendar_event_recurrence_pattern_spinner.setAdapter(patternSpinnerAdapter);
            calendar_event_recurrence_pattern_spinner.setSelection(0, false);

            repeatBySpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, repeatByOptionsArrayList);
            repeatBySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            calendar_event_recurrence_repeat_by_spinner.setAdapter(repeatBySpinnerAdapter);
            calendar_event_recurrence_repeat_by_spinner.setSelection(0, false);

            dayOfWeekSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, dayOfWeekArrayList);
            dayOfWeekSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            calendar_event_recurrence_monthly_day_of_week_spinner.setAdapter(dayOfWeekSpinnerAdapter);
            calendar_event_recurrence_monthly_day_of_week_spinner.setSelection(0, false);

            rangeSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, rangeTypesArrayList);
            rangeSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            calendar_event_recurrence_range_spinner.setAdapter(rangeSpinnerAdapter);
            calendar_event_recurrence_range_spinner.setSelection(2, false);

            // We are restricting click event for all fields if current event is recurrence.
            if (isRecurrenceEvent) {
                calendar_event_title.setEnabled(false);
                calendar_event_all_day_switch.setEnabled(false);
                calendar_event_notes.setEnabled(false);
                calendar_event_recurrence_switch.setEnabled(false);
                calendar_event_recurrence_pattern_spinner.setEnabled(false);
                calendar_event_recurrence_repeat_every.setEnabled(false);
                calendar_event_recurrence_repeat_by_spinner.setEnabled(false);
                calendar_event_recurrence_monthly_day_of_week_spinner.setEnabled(false);
                calendar_event_recurrence_range_spinner.setEnabled(false);
                calendar_event_recurrence_occurrences.setEnabled(false);
            } else {
                calendar_event_meeting_type.setOnClickListener(v -> {
                    hideKeyboard();

                    showAllMeetingTypes(v);
                });

                calendar_event_start_date.setOnClickListener(v -> showDatePicker(Constants.SelectionType.START_DATE));

                calendar_event_end_date.setOnClickListener(v -> showDatePicker(Constants.SelectionType.END_DATE));

                calendar_event_start_time.setOnClickListener(v -> showTimePicker(Constants.SelectionType.START_TIME));

                calendar_event_end_time.setOnClickListener(v -> showTimePicker(Constants.SelectionType.END_TIME));

                calendar_event_all_day_switch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    hideKeyboard();

                    newCalendarEvent.setAllDay(isChecked);

                    if (isChecked) {
                        calendar_event_start_time.setVisibility(View.GONE);
                        calendar_event_end_time.setVisibility(View.GONE);
                    } else {
                        calendar_event_start_time.setVisibility(View.VISIBLE);
                        calendar_event_end_time.setVisibility(View.VISIBLE);
                    }
                });

                calendar_event_recurrence_switch.setOnCheckedChangeListener((buttonView, isChecked) -> {
                    hideKeyboard();

                    newCalendarEvent.setRecurrenceEvent(isChecked);

                    if (isChecked) {
                        calendar_event_recurrence_layout.setVisibility(View.VISIBLE);

                        calendar_event_recurrence_repeat_by_result.setText(String.format("%s day", getOrdinalForNumber(Integer.parseInt(getDayOfMonthFromDate(newCalendarEvent.getStartDateObj())))));

                        if (!isSpinnersResetInitially) {
                            setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_pattern_spinner, 0);
                            setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_range_spinner, recurrenceRules.getRuleType());

                            isSpinnersResetInitially = true;
                        }
                    } else {
                        calendar_event_recurrence_layout.setVisibility(View.GONE);
                    }
                });

                calendar_event_recurrence_pattern_spinner.setOnItemSelectedListener(this);
                calendar_event_recurrence_repeat_by_spinner.setOnItemSelectedListener(this);
                calendar_event_recurrence_monthly_day_of_week_spinner.setOnItemSelectedListener(this);
                calendar_event_recurrence_range_spinner.setOnItemSelectedListener(this);

                repeat_on_cb_all.setOnCheckedChangeListener(this);
                repeat_on_cb_sun.setOnCheckedChangeListener(this);
                repeat_on_cb_mon.setOnCheckedChangeListener(this);
                repeat_on_cb_tue.setOnCheckedChangeListener(this);
                repeat_on_cb_wed.setOnCheckedChangeListener(this);
                repeat_on_cb_thu.setOnCheckedChangeListener(this);
                repeat_on_cb_fri.setOnCheckedChangeListener(this);
                repeat_on_cb_sat.setOnCheckedChangeListener(this);

                calendar_event_recurrence_until_date.setOnClickListener(v -> showDatePicker(Constants.SelectionType.RECURRENCE_UNTIL_DATE));

                calendar_event_reset_button.setOnClickListener(v -> {
                    hideKeyboard();

                    resetCalendarEvent();
                });

                calendar_event_delete_button.setOnClickListener(v -> {
                    hideKeyboard();

                    deleteCalendarEvent();
                });

                calendar_event_save_button.setOnClickListener(v -> {
                    hideKeyboard();

                    saveCalendarEvent();
                });
            }

            if (isEventEditing) {
                if (isRecurrenceEvent) {
                    calendar_event_screen_title.setText("View Event");

                    recurrence_event_not_editable_title.setVisibility(View.VISIBLE);

                    calendar_event_reset_button.setVisibility(View.GONE);
                    calendar_event_delete_button.setVisibility(View.GONE);
                    calendar_event_save_button.setVisibility(View.GONE);
                } else {
                    calendar_event_screen_title.setText("Edit Event");

                    calendar_event_reset_button.setVisibility(View.GONE);
                    calendar_event_delete_button.setVisibility(View.VISIBLE);
                }

                updateCalendarEventDetails();

            } else {
                calendar_event_screen_title.setText("Add Event");

                calendar_event_reset_button.setVisibility(View.VISIBLE);
                calendar_event_delete_button.setVisibility(View.GONE);

                prePopulateDateTimeFields();
            }

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_CREATE_CALENDAR_EVENT_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void updateCalendarEventDetails() {

        try {
            if (calendarEventDetails != null) {

                Log.e("veera",calendarEventDetails.toString());
                newCalendarEvent.setEventId(selectedCalendarEventId);
                newCalendarEvent.setEventDate(selectedCalendarDate);
                newCalendarEvent.setTitle(calendarEventDetails.getTitle());
                newCalendarEvent.setMeetingType(calendarEventDetails.getMeetingType() == null ? "None" : calendarEventDetails.getMeetingType());
                newCalendarEvent.setMeetingTypeId(calendarEventDetails.getMeetingTypeId() == -1 ? "0" : String.valueOf(calendarEventDetails.getMeetingTypeId()));
                // We are adding Locale to resolve ParseException (Unparseable date error)
                DateFormat editEventDateTimeParser = new SimpleDateFormat(Constants.CALENDAR_EDIT_EVENT_DATE_TIME_FORMAT, Locale.US);
                Date eventStartDateTime = editEventDateTimeParser.parse(calendarEventDetails.getStartDate());
                Date eventEndDateTime = editEventDateTimeParser.parse(calendarEventDetails.getEndDate());

                // Updating Start Date in UI
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(eventStartDateTime);

                newCalendarEvent.setStartDateObj(calendar.getTime());
                newCalendarEvent.setStartDate(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));

                currentTime = String.format("%tH:%tM", calendar, calendar);
                newCalendarEvent.setStartTime(currentTime);

                currentTime = String.format("%tI:%tM %tp", calendar, calendar, calendar);
                currentTime = currentTime.toUpperCase().replace(".", ""); // in order to convert a.m. to AM
                newCalendarEvent.setStartTimeText(currentTime);

                // Updating End Date in UI
                calendar.setTime(eventEndDateTime);

                newCalendarEvent.setEndDateObj(calendar.getTime());
                newCalendarEvent.setEndDate(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));

                currentTime = String.format("%tH:%tM", calendar, calendar);
                newCalendarEvent.setEndTime(currentTime);

                currentTime = String.format("%tI:%tM %tp", calendar, calendar, calendar);
                currentTime = currentTime.toUpperCase().replace(".", ""); // in order to convert a.m. to AM
                newCalendarEvent.setEndTimeText(currentTime);

                newCalendarEvent.setAllDay(calendarEventDetails.isAllDay());
                newCalendarEvent.setNotes(calendarEventDetails.getNotes());
                newCalendarEvent.setRecurrenceEvent(isRecurrenceEvent);
                newCalendarEvent.setRecurrenceRulesStr(calendarEventDetails.getRecurrenceRule());
                newCalendarEvent.setRecurrenceRules(calendarEventDetails.getRecurrenceRuleDetails());
            }
            updateUIWithLatestValues(newCalendarEvent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAllMeetingTypes(View view) {
        if (allMeetingTypes == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_ACTIVITY_TYPES);
        } else {
            Intent allActivityTypesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allActivityTypesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allMeetingTypes);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.MEETING_TYPE);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newCalendarEvent.getMeetingTypeId());
            allActivityTypesIntent.putExtras(bundle);
            startActivity(allActivityTypesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        if (fieldType.equals(Constants.GET_MASTER_DATA_ACTIVITY_TYPES)) {
            allMeetingTypes = (ArrayList) masterData;
            allMeetingTypes.set(0, new MasterData("None", "0")); // We are replacing Empty with None.
            MMProgressDialog.hideProgressDialog();

            showAllMeetingTypes(null);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private final BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_CREATE_CALENDAR_EVENT_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.MEETING_TYPE) {
                        didMeetingTypeChanged((MasterData) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didMeetingTypeChanged(MasterData selectedMeetingType) {
        newCalendarEvent.setMeetingType(selectedMeetingType.getName());
        newCalendarEvent.setMeetingTypeId(selectedMeetingType.getId());

        calendar_event_meeting_type.setText(newCalendarEvent.getMeetingType());
    }

    public void prePopulateDateTimeFields() {
        try {
            Calendar calendar = Calendar.getInstance();

            Date selectedDate = dateParser.parse(String.format("%s %tH:%tM", selectedCalendarDate, calendar, calendar));
            calendar.setTime(selectedDate);

            // Rounding time to nearest 30 minutes.
            int mMinute = calendar.get(Calendar.MINUTE);
            if (mMinute < 30) {
                calendar.set(Calendar.MINUTE, 30);
            } else {
                calendar.add(Calendar.HOUR, 1);
                calendar.set(Calendar.MINUTE, 0);
            }

            newCalendarEvent.setStartDateObj(calendar.getTime());
            newCalendarEvent.setStartDate(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));

            currentTime = String.format("%tH:%tM", calendar, calendar);
            newCalendarEvent.setStartTime(currentTime);

            currentTime = String.format("%tI:%tM %tp", calendar, calendar, calendar);
            currentTime = currentTime.toUpperCase().replace(".", ""); // in order to convert a.m. to AM
            newCalendarEvent.setStartTimeText(currentTime);

            calendar_event_start_date.setText(newCalendarEvent.getStartDate());
            calendar_event_start_time.setText(newCalendarEvent.getStartTimeText());

            // Calculating End Time i.e. adding 1 Hour to Start Time.
            calendar.add(Calendar.HOUR_OF_DAY, 1);

            newCalendarEvent.setEndDateObj(calendar.getTime());
            newCalendarEvent.setEndDate(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));

            currentTime = String.format("%tH:%tM", calendar, calendar);
            newCalendarEvent.setEndTime(currentTime);

            currentTime = String.format("%tI:%tM %tp", calendar, calendar, calendar);
            currentTime = currentTime.toUpperCase().replace(".", ""); // in order to convert a.m. to AM
            newCalendarEvent.setEndTimeText(currentTime);

            calendar_event_end_date.setText(newCalendarEvent.getEndDate());
            calendar_event_end_time.setText(newCalendarEvent.getEndTimeText());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDatePicker(final Constants.SelectionType selectionType) {
        try {
            hideKeyboard();

            if (selectionType == Constants.SelectionType.START_DATE)
                preSelectedDate = newCalendarEvent.getStartDate();
            else if (selectionType == Constants.SelectionType.END_DATE)
                preSelectedDate = newCalendarEvent.getEndDate();
            else if (selectionType == Constants.SelectionType.RECURRENCE_UNTIL_DATE)
                preSelectedDate = recurrenceRules.getUntilDate();

            if (preSelectedDate.isEmpty()) {
                preSelectedDate = currentDate;
            }

            String[] dateValues = preSelectedDate.split("/");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, (datePicker, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);

                preSelectedDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);

                if (selectionType == Constants.SelectionType.START_DATE) {
                    calendar_event_start_date.setText(preSelectedDate);
                    newCalendarEvent.setStartDate(preSelectedDate);
                    newCalendarEvent.setStartDateObj(calendar.getTime());

                    if (newCalendarEvent.getStartDateObj().after(newCalendarEvent.getEndDateObj())) {
                        // Calculating End Time i.e. adding 1 Hour to Start Time.
                        calendar.add(Calendar.HOUR_OF_DAY, 1);

                        newCalendarEvent.setEndDateObj(calendar.getTime());
                        newCalendarEvent.setEndDate(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));

                        calendar_event_end_date.setText(newCalendarEvent.getEndDate());
                    }

                    if (recurrenceRules != null) {
                        if (recurrenceRules.getRepeatBy() == 0)
                            calendar_event_recurrence_repeat_by_result.setText(String.format("%s day", getOrdinalForNumber(Integer.parseInt(getDayOfMonthFromDate(calendar.getTime())))));
                        else if (recurrenceRules.getRepeatBy() == 1) {
                            calendar_event_recurrence_monthly_day_of_week_spinner.setSelection(getDayOfWeekInMonthFromDate(newCalendarEvent.getStartDateObj()));
                            calendar_event_recurrence_repeat_by_result.setText(getDayOfWeekFromDate(calendar.getTime()));
                        }
                    }
                } else if (selectionType == Constants.SelectionType.END_DATE) {
                    calendar_event_end_date.setText(preSelectedDate);
                    newCalendarEvent.setEndDate(preSelectedDate);
                    newCalendarEvent.setEndDateObj(calendar.getTime());

                    if (newCalendarEvent.getStartDateObj().after(newCalendarEvent.getEndDateObj())) {
                        calendar_event_start_date.setText(newCalendarEvent.getEndDate());
                        newCalendarEvent.setStartDate(newCalendarEvent.getEndDate());
                        newCalendarEvent.setStartDateObj(newCalendarEvent.getEndDateObj());
                    }
                } else if (selectionType == Constants.SelectionType.RECURRENCE_UNTIL_DATE) {
                    calendar_event_recurrence_until_date.setText(preSelectedDate);
                    recurrenceRules.setUntilDate(preSelectedDate);
                }
            }, Integer.parseInt(dateValues[2]), Integer.parseInt(dateValues[0]) - 1, Integer.parseInt(dateValues[1]));

            if (selectionType == Constants.SelectionType.RECURRENCE_UNTIL_DATE) {
                datePickerDialog.getDatePicker().setMinDate(newCalendarEvent.getStartDateObj().getTime());
            }

            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showTimePicker(final Constants.SelectionType selectionType) {
        try {
            hideKeyboard();

            if (selectionType == Constants.SelectionType.START_TIME)
                preSelectedTime = newCalendarEvent.getStartTime();
            else if (selectionType == Constants.SelectionType.END_TIME)
                preSelectedTime = newCalendarEvent.getEndTime();

            if (preSelectedTime.isEmpty()) {
                Calendar calendar = Calendar.getInstance();
                preSelectedTime = Utility.formatDate(calendar.getTime(), Constants.TIME_FORMAT);
            }

            String[] timeValues = preSelectedTime.split(":");

            TimePickerDialog timePickerDialog = new TimePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, (view, hourOfDay, minute) -> {
                try {
                    preSelectedTime = String.format("%02d:%02d", hourOfDay, minute);

                    String amPm = hourOfDay > 11 ? "PM" : "AM";

                    //Make the 24 hour time format to 12 hour time format
                    if (hourOfDay > 12)
                        hourOfDay = hourOfDay - 12;

                    String selectedTimeText = String.format("%02d:%02d %s", hourOfDay, minute, amPm);

                    if (selectionType == Constants.SelectionType.START_TIME) {
                        calendar_event_start_time.setText(selectedTimeText);
                        newCalendarEvent.setStartTimeText(selectedTimeText);
                        newCalendarEvent.setStartTime(preSelectedTime);

                        Date startTimeDate = dateTimeParser.parse(newCalendarEvent.getStartDate() + " " + newCalendarEvent.getStartTime());
                        Date endTimeDate = dateTimeParser.parse(newCalendarEvent.getEndDate() + " " + newCalendarEvent.getEndTime());

                        if (startTimeDate.after(endTimeDate)) {
                            calendar_event_end_time.setText(newCalendarEvent.getStartTimeText());
                            newCalendarEvent.setEndTimeText(newCalendarEvent.getStartTimeText());
                            newCalendarEvent.setEndTime(newCalendarEvent.getStartTime());
                        }

                    } else if (selectionType == Constants.SelectionType.END_TIME) {
                        calendar_event_end_time.setText(selectedTimeText);
                        newCalendarEvent.setEndTimeText(selectedTimeText);
                        newCalendarEvent.setEndTime(preSelectedTime);

                        Date startTimeDate = dateTimeParser.parse(newCalendarEvent.getStartDate() + " " + newCalendarEvent.getStartTime());
                        Date endTimeDate = dateTimeParser.parse(newCalendarEvent.getEndDate() + " " + newCalendarEvent.getEndTime());

                        if (startTimeDate.after(endTimeDate)) {
                            calendar_event_start_time.setText(newCalendarEvent.getEndTimeText());
                            newCalendarEvent.setStartTimeText(newCalendarEvent.getEndTimeText());
                            newCalendarEvent.setStartTime(newCalendarEvent.getEndTime());
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }, Integer.parseInt(timeValues[0]), Integer.parseInt(timeValues[1]), false);

            timePickerDialog.setCancelable(false);
            timePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUIWithLatestValues(NewCalendarEvent event) {

        calendar_event_title.setText(event.getTitle());
        calendar_event_meeting_type.setText(event.getMeetingType());
        calendar_event_start_date.setText(event.getStartDate());
        calendar_event_start_time.setText(event.getStartTimeText());
        calendar_event_end_date.setText(event.getEndDate());
        calendar_event_end_time.setText(event.getEndTimeText());
        calendar_event_all_day_switch.setChecked(event.isAllDay());
        calendar_event_notes.setText(event.getNotes());
        calendar_event_recurrence_switch.setChecked(event.isRecurrenceEvent());

        if (event.isRecurrenceEvent()) {
            calendar_event_recurrence_layout.setVisibility(View.VISIBLE);
        } else {
            calendar_event_recurrence_layout.setVisibility(View.GONE);
            calendar_event_recurrence_repeat_by_result.setText(String.format("%s day", getOrdinalForNumber(Integer.parseInt(getDayOfMonthFromDate(newCalendarEvent.getStartDateObj())))));
        }

        if (event.getRecurrenceRules() != null) {
            recurrenceRules = event.getRecurrenceRules();

            String selectedPattern = recurrenceRules.getPattern();

            int patternIndex = 0;
            boolean isWeeklyPatternSelected = false, isMonthlyPatternSelected = false;

            if (selectedPattern.equalsIgnoreCase(PATTERN_DAILY)) {
                calendar_event_recurrence_repeat_every_title.setText("Day(s)");
                patternIndex = 0;
            } else if (selectedPattern.equalsIgnoreCase(PATTERN_WEEKLY)) {
                calendar_event_recurrence_repeat_every_title.setText("Week(s)");
                patternIndex = 1;
                isWeeklyPatternSelected = true;
            } else if (selectedPattern.equalsIgnoreCase(PATTERN_MONTHLY)) {
                calendar_event_recurrence_repeat_every_title.setText("Month(s)");
                patternIndex = 2;
                isMonthlyPatternSelected = true;
            } else if (selectedPattern.equalsIgnoreCase(PATTERN_YEARLY)) {
                calendar_event_recurrence_repeat_every_title.setText("Year(s)");
                patternIndex = 3;
            }

            if (isWeeklyPatternSelected) {
                calendar_event_recurrence_repeat_on_layout.setVisibility(View.VISIBLE);
            } else {
                calendar_event_recurrence_repeat_on_layout.setVisibility(View.GONE);
            }

            if (isMonthlyPatternSelected) {
                calendar_event_recurrence_repeat_by_layout.setVisibility(View.VISIBLE);
            } else {
                calendar_event_recurrence_repeat_by_layout.setVisibility(View.GONE);
            }

            setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_pattern_spinner, patternIndex);

            calendar_event_recurrence_repeat_every.setText(String.valueOf(recurrenceRules.getRepeatEvery()));

            if (isWeeklyPatternSelected) {
                String repeatDays = recurrenceRules.getRepeatDays();

                boolean isAllDaysSelected = false;

                if (repeatDays.contains("SU")) {
                    repeat_on_cb_sun.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_sun.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("MO")) {
                    repeat_on_cb_mon.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_mon.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("TU")) {
                    repeat_on_cb_tue.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_tue.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("WE")) {
                    repeat_on_cb_wed.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_wed.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("TH")) {
                    repeat_on_cb_thu.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_thu.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("FR")) {
                    repeat_on_cb_fri.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_fri.setChecked(false);
                    isAllDaysSelected = false;
                }

                if (repeatDays.contains("SA")) {
                    repeat_on_cb_sat.setChecked(true);
                    isAllDaysSelected = true;
                } else {
                    repeat_on_cb_sat.setChecked(false);
                    isAllDaysSelected = false;
                }

                repeat_on_cb_all.setChecked(isAllDaysSelected);
            }

            if (isMonthlyPatternSelected) {
                int selectedRepeatBy = recurrenceRules.getRepeatBy();

                setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_repeat_by_spinner, selectedRepeatBy);

                if (selectedRepeatBy == 0) {
                    calendar_event_recurrence_repeat_by_result.setText(recurrenceRules.getRepeatByText());
                } else if (selectedRepeatBy == 1) {
                    int selectedRepeatByOption = recurrenceRules.getRepeatByOption();

                    // As we are getting 1 extra from service for other options, we are subtracting it.
                    if (recurrenceRules.getRepeatByOption() < 4)
                        selectedRepeatByOption = recurrenceRules.getRepeatByOption() - 1;

                    setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_monthly_day_of_week_spinner, selectedRepeatByOption);
                    calendar_event_recurrence_monthly_day_of_week_spinner.setVisibility(View.VISIBLE);

                    calendar_event_recurrence_repeat_by_result.setText(recurrenceRules.getRepeatByText().trim());
                }
            }

            int selectedRange = recurrenceRules.getRuleType();

            boolean isUntilDateRangeSelected = false, isForRangeSelected = false;

            if (selectedRange == 0) {
                isUntilDateRangeSelected = true;
            } else if (selectedRange == 1) {
                isForRangeSelected = true;
            }

            setSpinnerSelectionWithoutCallingListener(calendar_event_recurrence_range_spinner, selectedRange);

            if (isUntilDateRangeSelected) {
                calendar_event_recurrence_until_date.setVisibility(View.VISIBLE);
                calendar_event_recurrence_until_date.setText(recurrenceRules.getUntilDate());
            } else {
                calendar_event_recurrence_until_date.setVisibility(View.GONE);
            }

            if (isForRangeSelected) {
                calendar_event_recurrence_occurrences_layout.setVisibility(View.VISIBLE);
                calendar_event_recurrence_occurrences.setText(String.valueOf(recurrenceRules.getOccurrences()));
            } else {
                calendar_event_recurrence_occurrences_layout.setVisibility(View.GONE);
            }

            if (isUntilDateRangeSelected || isForRangeSelected) {
                calendar_event_recurrence_range_result_layout.setVisibility(View.VISIBLE);
            } else {
                calendar_event_recurrence_range_result_layout.setVisibility(View.GONE);
            }
        }
    }

    public void resetCalendarEvent() {
        newCalendarEvent = new NewCalendarEvent();

        recurrenceRules = new RecurrenceRules();
        newCalendarEvent.setRecurrenceRules(recurrenceRules);

        prePopulateDateTimeFields();

        updateUIWithLatestValues(newCalendarEvent);
    }

    public void saveCalendarEvent() {
        try {
            Date meetingStartTime = dateTimeParser.parse(newCalendarEvent.getStartDate() + " " + newCalendarEvent.getStartTime());
            Date meetingEndTime = dateTimeParser.parse(newCalendarEvent.getEndDate() + " " + newCalendarEvent.getEndTime());

            if (meetingStartTime.after(meetingEndTime)) {
                MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Meeting start time must be less than or equal to end time.");
            } else {
                newCalendarEvent.setTitle(calendar_event_title.getText().toString().trim());
                newCalendarEvent.setNotes(calendar_event_notes.getText().toString().trim());
                newCalendarEvent.setEventDate(selectedCalendarDate);

                if (newCalendarEvent.isRecurrenceEvent()) {

                    if (recurrenceRules != null) {

                        recurrenceRules.setRepeatEvery(Integer.parseInt(calendar_event_recurrence_repeat_every.getText().toString().trim()));

                        // If Weekly Pattern selected then we are forming selected week days with Comma (,) Separated.
                        if (recurrenceRules.getPattern().equals(PATTERN_WEEKLY)) {
                            String selectedDays = "";
                            for (String weekDay : selectedWeekDays)
                                selectedDays = String.format("%s,%s", selectedDays, weekDay);

                            selectedDays = selectedDays.substring(1);
                            recurrenceRules.setRepeatDays(selectedDays);
                        }

                        if (recurrenceRules.getRuleType() == 0) {
                            recurrenceRules.setUntilDate(calendar_event_recurrence_until_date.getText().toString());
                        }

                        if (recurrenceRules.getRuleType() == 1) {
                            recurrenceRules.setOccurrences(Integer.parseInt(calendar_event_recurrence_occurrences.getText().toString().trim()));
                        }
                    }

                    newCalendarEvent.setRecurrenceRules(recurrenceRules);
                }

                MMProgressDialog.showProgressDialog(activityContext);
                calendarModel.addCalendarEvent(encryptedClientKey, getDTTicks(), newCalendarEvent, isEventEditing);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateCalendarEventAddingStatus() {
        MMProgressDialog.hideProgressDialog();

        try {
            String title = "", message = "";

            if (isEventEditing) {
                title = "";
                message = "Event edited successfully.";
            } else {
                title = "";
                message = "Event added successfully.";
            }

            // Broadcasting Intent to Receiver Activity
            Intent refreshCalendarEventsBroadcast = new Intent(Constants.LBM_ACTION_REFRESH_CALENDAR_EVENTS);
            refreshCalendarEventsBroadcast.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_DATE, selectedCalendarDate);
            LocalBroadcastManager.getInstance(activityContext).sendBroadcast(refreshCalendarEventsBroadcast);


            showAlertMessage(title, message);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCalendarEvent() {
        MMProgressDialog.showProgressDialog(activityContext);
        calendarModel.deleteCalendarEvent(encryptedClientKey, getDTTicks(), selectedCalendarEventId, selectedCalendarDate);
    }

    public void updateDeleteCalendarEventStatus() {
        MMProgressDialog.hideProgressDialog();

        // Broadcasting Intent to Receiver Activity
        Intent refreshCalendarEventsBroadcast = new Intent(Constants.LBM_ACTION_REFRESH_CALENDAR_EVENTS);
        refreshCalendarEventsBroadcast.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_DATE, selectedCalendarDate);
        LocalBroadcastManager.getInstance(activityContext).sendBroadcast(refreshCalendarEventsBroadcast);

        showAlertMessage("", "Event deleted successfully.");
    }

    public void showAlertMessage(String title, String message) {
        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
        MMAlertDialog.showAlertDialogWithCallback(this, title, message, false);
        MMAlertDialog.setListener(new AlertDialogSelectionListener() {
            @Override
            public void alertDialogCallback() {
                onBackPressed();
            }

            @Override
            public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

            }
        });
    }

    /* AdapterView.OnItemSelectedListener Methods Starts Here */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            hideKeyboard();

            int spinnersTag = Integer.parseInt(parent.getTag().toString());

            if (spinnersTag == 1) { // Pattern Spinner
                String selectedPattern = patternTypesKeyValues.get(position);

                recurrenceRules.setPattern(patternTypesHashMap.get(selectedPattern));

                boolean isWeeklyPatternSelected = false, isMonthlyPatternSelected = false;

                switch (selectedPattern) {
                    case PATTERN_DAILY:
                        calendar_event_recurrence_repeat_every_title.setText("Day(s)");
                        break;
                    case PATTERN_WEEKLY:
                        calendar_event_recurrence_repeat_every_title.setText("Week(s)");

                        isWeeklyPatternSelected = true;

                        break;
                    case PATTERN_MONTHLY:
                        calendar_event_recurrence_repeat_every_title.setText("Month(s)");

                        isMonthlyPatternSelected = true;

                        break;
                    case PATTERN_YEARLY:
                        calendar_event_recurrence_repeat_every_title.setText("Year(s)");
                        break;
                }

                if (isWeeklyPatternSelected) {
                    calendar_event_recurrence_repeat_on_layout.setVisibility(View.VISIBLE);
                } else {
                    calendar_event_recurrence_repeat_on_layout.setVisibility(View.GONE);
                }

                if (isMonthlyPatternSelected) {
                    calendar_event_recurrence_repeat_by_layout.setVisibility(View.VISIBLE);

                    // We are calling listener by selecting in order to update day or week
                    //calendar_event_recurrence_repeat_by_spinner.setSelection(1, true);
                } else {
                    calendar_event_recurrence_repeat_by_layout.setVisibility(View.GONE);
                }

            } else if (spinnersTag == 2) { // Repeat By Spinner
                String selectedRepeatByOption = repeatByOptionsArrayList.get(position);

                if (selectedRepeatByOption.equals("Day of Month")) {
                    recurrenceRules.setRepeatBy(0);

                    calendar_event_recurrence_monthly_day_of_week_spinner.setVisibility(View.GONE);

                    calendar_event_recurrence_repeat_by_result.setText(String.format("%s day", getOrdinalForNumber(Integer.parseInt(getDayOfMonthFromDate(newCalendarEvent.getStartDateObj())))));
                } else {
                    recurrenceRules.setRepeatBy(1);

                    calendar_event_recurrence_monthly_day_of_week_spinner.setVisibility(View.VISIBLE);

                    calendar_event_recurrence_monthly_day_of_week_spinner.setSelection(getDayOfWeekInMonthFromDate(newCalendarEvent.getStartDateObj()));
                    calendar_event_recurrence_repeat_by_result.setText(getDayOfWeekFromDate(newCalendarEvent.getStartDateObj()));
                }

            } else if (spinnersTag == 3) { // Month Day of Week Spinner
                String selectedDayOfWeek = dayOfWeekArrayList.get(position);

                recurrenceRules.setRepeatByOption(position);

            } else if (spinnersTag == 4) { // Range Spinner
                String selectedRange = rangeTypesArrayList.get(position);

                recurrenceRules.setRuleType(position);

                boolean isUntilDateRangeSelected = false, isForRangeSelected = false;

                switch (selectedRange) {
                    case "Until Date":
                        isUntilDateRangeSelected = true;
                        break;
                    case "For":
                        isForRangeSelected = true;
                        break;
                    case "Forever":

                        break;
                }

                if (isUntilDateRangeSelected) {
                    calendar_event_recurrence_until_date.setVisibility(View.VISIBLE);
                    recurrenceRules.setUntilDate(newCalendarEvent.getStartDate());
                    calendar_event_recurrence_until_date.setText(recurrenceRules.getUntilDate());
                } else {
                    calendar_event_recurrence_until_date.setVisibility(View.GONE);
                }

                if (isForRangeSelected) {
                    calendar_event_recurrence_occurrences_layout.setVisibility(View.VISIBLE);
                } else {
                    calendar_event_recurrence_occurrences_layout.setVisibility(View.GONE);
                }

                if (isUntilDateRangeSelected || isForRangeSelected) {
                    calendar_event_recurrence_range_result_layout.setVisibility(View.VISIBLE);
                } else {
                    calendar_event_recurrence_range_result_layout.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);
        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(() -> {
            spinner.setSelection(selection);
            spinner.post(() -> spinner.setOnItemSelectedListener(listener));
        });
    }

    /* AdapterView.OnItemSelectedListener Methods Ends Here */

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            int checkBoxTag = Integer.parseInt(buttonView.getTag().toString());

            switch (checkBoxTag) {
                case 1:
                    repeat_on_cb_sun.setChecked(isChecked);
                    repeat_on_cb_mon.setChecked(isChecked);
                    repeat_on_cb_tue.setChecked(isChecked);
                    repeat_on_cb_wed.setChecked(isChecked);
                    repeat_on_cb_thu.setChecked(isChecked);
                    repeat_on_cb_fri.setChecked(isChecked);
                    repeat_on_cb_sat.setChecked(isChecked);
                    break;
                case 2:
                    if (isChecked)
                        selectedWeekDays.add("SU");
                    else
                        selectedWeekDays.remove("SU");
                    break;
                case 3:
                    if (isChecked)
                        selectedWeekDays.add("MO");
                    else
                        selectedWeekDays.remove("MO");
                    break;
                case 4:
                    if (isChecked)
                        selectedWeekDays.add("TU");
                    else
                        selectedWeekDays.remove("TU");
                    break;
                case 5:
                    if (isChecked)
                        selectedWeekDays.add("WE");
                    else
                        selectedWeekDays.remove("WE");
                    break;
                case 6:
                    if (isChecked)
                        selectedWeekDays.add("TH");
                    else
                        selectedWeekDays.remove("TH");
                    break;
                case 7:
                    if (isChecked)
                        selectedWeekDays.add("FR");
                    else
                        selectedWeekDays.remove("FR");
                    break;
                case 8:
                    if (isChecked)
                        selectedWeekDays.add("SA");
                    else
                        selectedWeekDays.remove("SA");
                    break;
            }

            if (selectedWeekDays.size() == 7) {
                repeat_on_cb_all.setOnCheckedChangeListener(null);
                repeat_on_cb_all.setChecked(true);
                repeat_on_cb_all.setOnCheckedChangeListener(this);
            } else {
                repeat_on_cb_all.setOnCheckedChangeListener(null);
                repeat_on_cb_all.setChecked(false);
                repeat_on_cb_all.setOnCheckedChangeListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideKeyboard() {
        MMEditText focusedEditText = null;

        if (calendar_event_title.hasFocus())
            focusedEditText = calendar_event_title;
        else if (calendar_event_notes.hasFocus())
            focusedEditText = calendar_event_notes;
        else if (calendar_event_recurrence_repeat_every.hasFocus())
            focusedEditText = calendar_event_recurrence_repeat_every;
        else if (calendar_event_recurrence_occurrences.hasFocus())
            focusedEditText = calendar_event_recurrence_occurrences;

        if (focusedEditText != null) {
            hideKeyboard(focusedEditText);
            focusedEditText.clearFocus();
        }
    }

    public int getDayOfWeekInMonthFromDate(Date date) {
        String dayOfWeekInMonth = new SimpleDateFormat("F",Locale.getDefault()).format(date);
        return Integer.parseInt(dayOfWeekInMonth) - 1; // we are subtracting 1 as our spinner values starts from index 0.
    }

    public String getDayOfWeekFromDate(Date date) {
        return new SimpleDateFormat("EEEE",Locale.getDefault()).format(date);
    }

    public String getDayOfMonthFromDate(Date date) {
        return new SimpleDateFormat("dd",Locale.getDefault()).format(date);
    }

    public String getOrdinalForNumber(int i) {
        String[] suffixes = new String[]{"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};

        switch (i % 100) {
            case 11:
            case 12:
            case 13:
                return i + "th";
            default:
                return i + suffixes[i % 10];

        }
    }
}
