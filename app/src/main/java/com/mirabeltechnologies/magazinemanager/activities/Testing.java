package com.mirabeltechnologies.magazinemanager.activities;

import android.os.Bundle;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.mirabeltechnologies.magazinemanager.R;

public class Testing extends AppCompatActivity {

    TextView test;
    String data;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        test= findViewById(R.id.test);
        Bundle bundle = getIntent().getExtras();
        data= bundle.getString("veera");
        test.setText(data);
    }
}
