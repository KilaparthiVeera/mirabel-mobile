package com.mirabeltechnologies.magazinemanager.activities;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.PrimaryProbabilityMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OpportunitiesSearchFilter;
import com.mirabeltechnologies.magazinemanager.beans.Opportunity;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityAdvSearch;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityResult;
import com.mirabeltechnologies.magazinemanager.beans.PrimaryProbability;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SelectOpportunity;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.OPPORTUNITY_PAGE_SIZE_INT;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SELECTED_DATE_DISPLAY_FORMAT;

public class OpportunitiesActivity extends ListViewBaseActivity {

    private OpportunitiesSearchFilter searchFilter;
    Spinner sp_opportunities;
    private TextView txt_repName,txt_probability;
    TextView txt_1,txt_2,txt_3,txt_4,txt_5,txt_6,txt_7;
    RelativeLayout page_footer;

    PrimaryProbabilityMultipleSearchFilter sp_probability;
    Activity activity;
    boolean isLoggedInRepExistsInAllRepsList = false;

    private ArrayList<RepData> allRepsList = null;
    private ArrayList<OppStageDetails> limitedStageDetails = null;
    List <String> stringList;
    private CommonModel commonModel;
    ArrayList<String> selectOpportunities_array;
    List<SelectOpportunity> selectOpportunityList;
    SpinnerAdapter categoriesSpinnerAdapter;
    String encryptedId,domain;
    Integer userId,listId;
    private OpportunityAdapter opportunityAdapter;
    private List<Opportunity> allOpportunities;

    ArrayList<PrimaryProbability> probabilityArrayList;

    public static Opportunity selectOpportunity;
    LinearLayout adv_search_header_layout;
    private DetailContact selectedContactDetails = null;
    private String selectedCustomerId, activityType;
    String selectRepName="",selectProbName="Choose Probability",getProbName="";
    List<String> selectReturnRepList,selectReturnProbList;

    String[] listItems;
    boolean[] checkedItems;
    ArrayList<Integer> mUserItems = new ArrayList<>();
    List<String> selectRepList,selectProbList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunities);
        page_footer=findViewById(R.id.page_footer);
        allRepsList= new ArrayList<>();
        probabilityArrayList= new ArrayList<>();
        limitedStageDetails=new ArrayList<>();
        stringList= new ArrayList<>();
        selectRepList=new ArrayList<>();
        selectProbList=new ArrayList<>();
        selectReturnProbList= new ArrayList<>();
        selectReturnRepList= new ArrayList<>();
        activity=OpportunitiesActivity.this;
        activityContext = OpportunitiesActivity.this;
        currentActivity = this;

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
            selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
            activityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
        }

        sp_probability=findViewById(R.id.sp_probability);

        for (int i=0;i<=100;i+= 10){
            PrimaryProbability probability= new PrimaryProbability();
            probability.setProbabilityId(String.valueOf(i));
            probability.setProbabilityName(i+"%");
            probability.setSelected(false);
            probabilityArrayList.add(probability);
        }

        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        domain=sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
        userId=sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID);

        list_view =  findViewById(R.id.contacts_list_view);
        adv_search_header_layout =  findViewById(R.id.adv_search_header_layout);

        if(activityType.equals(Constants.ACTIVITY_TYPE_OPPORTUNITY)){
            page_footer.setVisibility(View.GONE);
            adv_search_header_layout.setVisibility(View.GONE);
        }
        else {
            page_footer.setVisibility(View.VISIBLE);
            adv_search_header_layout.setVisibility(View.VISIBLE);
        }

        requestFrom = Constants.RequestFrom.OPPORTUNITY;
        commonModel = new CommonModel(activityContext, this, requestFrom);
        searchFilter= new OpportunitiesSearchFilter();

        txt_repName = findViewById(R.id.adv_search_rep_name);
        txt_repName.setOnClickListener(this::showAllRepsData);

        txt_probability=findViewById(R.id.txt_probability);
        txt_probability.setOnClickListener(v -> showAllProbability());

        txt_1=findViewById(R.id.txt_1);
        txt_2=findViewById(R.id.txt_2);
        txt_3=findViewById(R.id.txt_3);
        txt_4=findViewById(R.id.txt_4);
        txt_5=findViewById(R.id.txt_5);
        txt_6=findViewById(R.id.txt_6);
        txt_7=findViewById(R.id.txt_7);

        MMButton search_button = findViewById(R.id.adv_search_search_button);
        search_button.setOnClickListener(this::applySearchForSelectedValues);

        MMButton reset_button = findViewById(R.id.adv_search_reset_button);
        reset_button.setOnClickListener(this::resetAdvSearchAutoCompleteTextViews);

        page_number_display = findViewById(R.id.my_contacts_page_number_display);
        left_nav_arrow = findViewById(R.id.my_contacts_left_nav_arrow);
        right_nav_arrow = findViewById(R.id.my_contacts_right_nav_arrow);

        opportunityAdapter = new OpportunityAdapter(activityContext,limitedStageDetails);
        list_view.setAdapter(opportunityAdapter);

        list_view.setOnItemClickListener((parent, view, position, id) -> {

            selectOpportunity= allOpportunities.get(position);

            Intent showCreateNewTaskPageIntent = new Intent(activityContext, AddOpportunityActivity.class);
            showCreateNewTaskPageIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle1 = new Bundle();
            bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_EDIT);
            bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle1.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, selectedCustomerId); //detailContact.getCustomersId()
            bundle1.putSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS, selectedContactDetails);
            bundle1.putString(Constants.BUNDLE_OPPORTUNITY_DETAILS, String.valueOf(selectOpportunity.getId()));
            bundle1.putString(Constants.BUNDLE_ACTIVITY_TYPE, activityType);

            showCreateNewTaskPageIntent.putExtras(bundle1);
            startActivity(showCreateNewTaskPageIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);

        });
        sp_opportunities=findViewById(R.id.sp_opportunities);
        selectOpportunities_array= new ArrayList<>();
        categoriesSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_row_item, selectOpportunities_array);
        categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_opportunities.setAdapter(categoriesSpinnerAdapter);
        sp_opportunities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                listId=selectOpportunityList.get(position).getId();
                searchFilter.setListId(listId);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

        loadServer();

        LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE));

    }

    private void showAllProbability() {

        if (probabilityArrayList != null && probabilityArrayList.size() > 0) {
            txt_probability.setVisibility(View.GONE);
            sp_probability.setVisibility(View.VISIBLE);
            for(int i=0;i<probabilityArrayList.size();i++){
                probabilityArrayList.get(i).setSelected(false);
                if(selectReturnProbList!=null && !selectReturnProbList.isEmpty()){
                    for(int j=0;j<selectReturnProbList.size();j++){
                        Log.e("veera",selectReturnProbList.toString());
                        if(selectReturnProbList.get(j).equals(probabilityArrayList.get(i).getProbabilityName())){
                            probabilityArrayList.get(i).setSelected(true);
                        }
                    }
                }
            }
            sp_probability.setItems(probabilityArrayList,"All Probability",-1);
            sp_probability.performClick();
        } else {
            sp_probability.setVisibility(View.GONE);
            txt_probability.setVisibility(View.VISIBLE);
            displayLongToast(getResources().getString(R.string.no_prob_found));

        }
    }

    private void loadServer() {

        if (!MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
        commonModel.getSelectOpportunity(encryptedId, domain, userId);
        commonModel.getAllStageUnits(encryptedId,domain);

        totalNoOfRecords = 0;
        totalNoOfPages = 0;
        updateFooterPageLabels(0);
        resetAdvSearchAutoCompleteTextViews(null);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    private void applySearchForSelectedValues(View v) {

        if (listId!=null || searchFilter.getProbability()!=null || searchFilter.getAssignedTo()!=null) {
            hideKeyboard(sp_opportunities);
            resetValues();
            getOpportunities(currentPageNumber);
        } else {
            displayToast(getResources().getString(R.string.select_search_criteria));
        }
    }

    public void getOpportunities(int pageNumber) {
        currentPageNumber = pageNumber;
        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            if(activityType.equals(Constants.ACTIVITY_TYPE_OPPORTUNITY)){
                commonModel.getContactOpportunities(encryptedId,domain,selectedContactDetails.getCustomersId());
            }
            else {
                searchFilter.setListId(listId);
                searchFilter.setUserID(userId);
                if(searchFilter.getAdvSearch()==null ){
                    OpportunityAdvSearch advSearch= new OpportunityAdvSearch();
                    advSearch.setZip("");
                    advSearch.setPhone("");
                    advSearch.setCountry("");
                    advSearch.setState("");
                    advSearch.setCounty("");
                    advSearch.setCity("");
                    advSearch.setEmail("");
                    searchFilter.setAdvSearch(advSearch);
                }

                if(getProbName==null || getProbName.isEmpty()){
                    String ass_Id="";
                    if(sp_probability.getSelectedItems()!=null && !sp_probability.getSelectedItems().isEmpty()) {
                        for (int i = 0; i < sp_probability.getSelectedItems().size(); i++) {
                            ass_Id = String.format("%s%s", ass_Id, "IE=" + sp_probability.getSelectedItems().get(i).getProbabilityId());
                            if (i < sp_probability.getSelectedItems().size()) ass_Id += "~";
                        }
                    }
                    else {
                        ass_Id="";
                    }
                    searchFilter.setProbability(ass_Id);
                }
                else {
                    searchFilter.setProbability(getProbName);
                    getProbName="";
                }
                commonModel.getAllMyOpportunities(encryptedId,domain, searchFilter, currentPageNumber,OPPORTUNITY_PAGE_SIZE_INT);
            }
            isLoading = true;
        }
    }

    private void showAllRepsData(View view) {
        hideKeyboard(view);

        if (allRepsList != null && allRepsList.size() > 0) {
            selectRepList.clear();
            mUserItems.clear();
            listItems = stringList.toArray(new String[0]);
            if(selectReturnRepList!=null && !selectReturnRepList.isEmpty()){
                for(int i=0;i<stringList.size();i++){
                    checkedItems[i] = false;
                    for(int j=0;j<selectReturnRepList.size();j++){
                        if(stringList.get(i).equals(selectReturnRepList.get(j))){
                            checkedItems[i] = true;
                            mUserItems.add(i);
                            selectRepList.add(allRepsList.get(i+1).getId());
                        }
                    }
                }
            }

            AlertDialog.Builder mBuilder = new AlertDialog.Builder(OpportunitiesActivity.this);
            mBuilder.setTitle("Choose Rep");
            mBuilder.setMultiChoiceItems(listItems, checkedItems, (dialogInterface, position, isChecked) -> {
                if(isChecked){
                    selectRepList.add(allRepsList.get(position+1).getId());
                    selectReturnRepList.add(allRepsList.get(position+1).getName());
                    mUserItems.add(position);
                }else{
                    selectRepList.remove(allRepsList.get(position+1).getId());
                    selectReturnRepList.remove(allRepsList.get(position+1).getName());
                    mUserItems.remove((Integer.valueOf(position)));
                }

            });
            mBuilder.setCancelable(false);

            mBuilder.setPositiveButton(R.string.ok_label, (dialogInterface, which) -> {
                StringBuilder item = new StringBuilder();
                Log.e("veera",mUserItems.toString());
                for (int i = 0; i < mUserItems.size(); i++) {
                    item.append(listItems[mUserItems.get(i)]);
                    if (i != mUserItems.size() - 1) {
                        item.append(", ");
                    }
                }

                if( selectRepList!=null && !selectRepList.isEmpty()){
                    String ass_Id="";
                    for (int i = 0; i < selectRepList.size(); i++) {
                        ass_Id= String.format("%s%s", ass_Id, "IE="+selectRepList.get(i));
                        if (i < selectRepList.size()) ass_Id += "~";
                    }
                    Log.e("veera",ass_Id);
                    searchFilter.setAssignedTo(ass_Id);
                    selectRepName=item.toString();
                    txt_repName.setText(item.toString());
                }
                else {
                    searchFilter.setRepId("-1");
                    searchFilter.setAssignedTo("");
                    searchFilter.setRepName("All Reps");
                    selectRepName="";
                    txt_repName.setText(searchFilter.getRepName());
                }
            });
            mBuilder.setNegativeButton(R.string.dismiss_label, (dialogInterface, i) -> dialogInterface.dismiss());
            mBuilder.setNeutralButton(R.string.clear_all_label, (dialogInterface, which) -> {

                for (int i = 0; i < checkedItems.length; i++) {
                    checkedItems[i] = false;
                    mUserItems.clear();
                }
                selectRepList.clear();
                selectReturnRepList.clear();
                searchFilter.setRepId("-1");
                searchFilter.setAssignedTo("");
                searchFilter.setRepName("All Reps");
                selectRepName="";
                txt_repName.setText(searchFilter.getRepName());
            });

            AlertDialog mDialog = mBuilder.create();
            mDialog.show();
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();

    }

    public void closeActivity(View view) {
        onBackPressed();
    }


    public void showSearchOpportunityFilters(View view) {

        for(int i=0;i<sp_probability.getSelectedItems().size();i++){
            selectProbList.add(sp_probability.getSelectedItems().get(i).getProbabilityName());
            selectProbName=PrimaryProbabilityMultipleSearchFilter.spinnerText;
        }
        Bundle bundle=new Bundle();
        bundle.putStringArrayList("repList",(ArrayList<String>)selectRepList);
        bundle.putStringArrayList("probList",(ArrayList<String>) selectProbList);
        bundle.putString("rep_id",searchFilter.getAssignedTo());
        bundle.putString("rep_name",selectRepName);
        bundle.putString("pro_id",searchFilter.getProbability());
        bundle.putString("pro_name",selectProbName);
        Intent showCreateNewTaskPageIntent = new Intent(activityContext, OpportunitiesSearchActivity.class);
        showCreateNewTaskPageIntent.putExtras(bundle);
        startActivity(showCreateNewTaskPageIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);

    }
    public void showCreateNewOpportunity(View view) {

        Intent showCreateNewTaskPageIntent = new Intent(activityContext, AddOpportunityActivity.class);
        showCreateNewTaskPageIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_ADD);
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, selectedCustomerId); //detailContact.getCustomersId()
        bundle.putSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS, selectedContactDetails);
        bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, activityType);
        showCreateNewTaskPageIntent.putExtras(bundle);
        startActivity(showCreateNewTaskPageIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);

    }

    public void updateOpportunity(List<SelectOpportunity> selectOpportunities) {
        if (MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.hideProgressDialog();
        if(selectOpportunities!=null && !selectOpportunities.isEmpty()){
            selectOpportunityList= selectOpportunities;
            for(int i=0; i<selectOpportunities.size();i++){
                selectOpportunities_array.add(selectOpportunities.get(i).getName());
            }
            categoriesSpinnerAdapter = new SpinnerAdapter(OpportunitiesActivity.this, R.layout.spinner_row_item, selectOpportunities_array);
            categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            sp_opportunities.setAdapter(categoriesSpinnerAdapter);

            listId=selectOpportunityList.get(0).getId();

            getOpportunities(currentPageNumber);
        }
    }

    public void updateStageDetails(List<OppStageDetails> stageDetailsList) {
        MMProgressDialog.hideProgressDialog();
        if (stageDetailsList != null && !stageDetailsList.isEmpty()) {
            for(int i=0;i<stageDetailsList.size();i++){
                if(!stageDetailsList.get(i).getStage().equals("Closed Won") && !stageDetailsList.get(i).getStage().equals("Closed Lost")){
                    limitedStageDetails.add(stageDetailsList.get(i));
                }
            }
        }
    }

    public void updateAllRepsData(List<RepData> repsList) {
        for(int i=0;i<repsList.size();i++){
            if(repsList.get(i).getName().equals("------Disabled Reps------")){
                break;
            }else {
                allRepsList.add(repsList.get(i));
            }
        }
        // allRepsList = (ArrayList) repsList;
        for(int i=1; i<allRepsList.size();i++){
            stringList.add(allRepsList.get(i).getName());
        }

        listItems = stringList.toArray(new String[0]);
        checkedItems = new boolean[listItems.length];
        isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));
        if (searchFilter == null)
            searchFilter = new OpportunitiesSearchFilter();

        searchFilter.setRepId(String.valueOf(loggedInRepId));
        searchFilter.setAssignedTo(("IE="+loggedInRepId+"~"));
        selectRepName=loggedInRepName;
        searchFilter.setRepName(loggedInRepName);
        txt_repName.setText(searchFilter.getRepName());
        searchFilter.setProbability("");
        searchFilter.setProbabilityName("");
        selectReturnRepList.add(searchFilter.getRepName());

        MMProgressDialog.hideProgressDialog();

    }


    public void resetValues() {
        super.resetValues();
        totalNoOfRecords = 0;
        totalNoOfPages = 0;
        updateFooterPageLabels(0);

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        if (allOpportunities != null) {
            allOpportunities.clear();
            allOpportunities = null;
        }
        allOpportunities = new ArrayList<>();
        // clearing adapter list
        opportunityAdapter.setOpportunitiesList(null);
        opportunityAdapter.notifyDataSetChanged();
    }

    public void resetAdvSearchAutoCompleteTextViews(View view) {
        resetValues();
        searchFilter = new OpportunitiesSearchFilter();
        sp_opportunities.setSelection(0);
        for(int i=0;i<probabilityArrayList.size();i++){
            probabilityArrayList.get(i).setSelected(false);
        }

        selectProbName="Choose Probability";
        getProbName="";
        selectReturnProbList.clear();
        selectProbList.clear();
        sp_probability.setItems(probabilityArrayList,"All Probability",-1);
        mUserItems.clear();
        listItems = stringList.toArray(new String[0]);
        checkedItems = new boolean[listItems.length];
        selectRepList.clear();
        selectReturnRepList.clear();
        searchFilter.setProbability("");
        searchFilter.setProbabilityName("");
        txt_probability.setText("");
        if (view == null) {
            searchFilter.setRepId(String.valueOf(loggedInRepId));
            searchFilter.setAssignedTo(("IE="+loggedInRepId+"~"));
            searchFilter.setRepName(loggedInRepName);
            selectRepName=loggedInRepName;
        } else {
            searchFilter.setRepId("-1");
            searchFilter.setAssignedTo("");
            searchFilter.setRepName("All Reps");
            selectRepName="";
        }

        txt_repName.setText(searchFilter.getRepName());


    }

    // Local Broadcast Manager Receiver Handler
    private final BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                if (intent.getAction().equals(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_ADD || rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_EDIT ) {
                        didAddOpportunity(selectedObj);
                    }
                    else {

                        selectReturnRepList = bundle.getStringArrayList("repList");
                        selectReturnProbList = bundle.getStringArrayList("probList");
                        searchFilter= (OpportunitiesSearchFilter) selectedObj;
                        getProbName=searchFilter.getProbability();
                        txt_probability.setText(searchFilter.getProbabilityName());
                        Log.e("hima",searchFilter.getProbabilityName());
                        Log.e("hima",selectReturnProbList.toString());
                        if(!searchFilter.getRepName().isEmpty() && searchFilter.getRepName()!=null){
                            if(searchFilter.getRepName().equals("Choose Rep")){
                                searchFilter.setRepName("All Reps");
                                txt_repName.setText(searchFilter.getRepName());
                            }
                            else {
                                txt_repName.setText(searchFilter.getRepName());
                            }
                        }

                        if(!selectReturnProbList.isEmpty() && selectReturnProbList!=null){
                            for(int i=0;i<probabilityArrayList.size();i++){
                                probabilityArrayList.get(i).setSelected(false);
                            }
                            txt_probability.setVisibility(View.VISIBLE);
                            sp_probability.setVisibility(View.GONE);
                        }

                        if(searchFilter.getProbabilityName().equals("Choose Probability")){
                            txt_probability.setText("All Probability");
                            for(int i=0;i<probabilityArrayList.size();i++){
                                probabilityArrayList.get(i).setSelected(false);
                            }
                            txt_probability.setVisibility(View.VISIBLE);
                            sp_probability.setVisibility(View.GONE);
                        }

                        resetValues();
                        getOpportunities(currentPageNumber);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    private void didAddOpportunity(Serializable selectedObj) {
        int value= (Integer) selectedObj;
        // Toast.makeText(applicationContext, " Opportunity Saved  successfully", Toast.LENGTH_SHORT).show();
        resetValues();
        getOpportunities(currentPageNumber);
        // loadServer();
    }

    public void updateGetOpportunityList(List<Opportunity> opportunities, int noOfRecentContacts, List<OpportunityResult> opportunityResult) {
        Log.e("veera",""+noOfRecentContacts);
        txt_1.setText(String.valueOf(opportunityResult.get(0).getTotIds()));
        txt_2.setText(String.format("$ %s", opportunityResult.get(0).getTotOppAmt()));
        txt_3.setText(String.valueOf(opportunityResult.get(0).getWon()));
        txt_4.setText(String.valueOf(opportunityResult.get(0).getOpen()));
        txt_5.setText(String.valueOf(opportunityResult.get(0).getLost()));
        txt_6.setText(String.valueOf(opportunityResult.get(0).getWinTotal()));
        txt_7.setText(String.valueOf(opportunityResult.get(0).getWinRatio()));

        if (currentPageNumber == 1 && (opportunities == null || opportunities.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber)) {
                alreadyLoadedPageNumbersList.add(currentPageNumber);
            }

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + opportunities.size();

            if (noOfRecentContacts != totalNoOfRecords) {
                totalNoOfRecords = noOfRecentContacts;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.OPPORTUNITY_PAGE_SIZE_FLOAT);

                isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.OPPORTUNITY_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 2;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getOpportunities(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.ORDERS_PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            allOpportunities.addAll(opportunities);

            opportunityAdapter.setOpportunitiesList(opportunities);
            opportunityAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }
        isInitialRequest = false;

    }

    public void updateContactOpportunityList(List<Opportunity> selectOpportunities) {
        MMProgressDialog.hideProgressDialog();
        if ((selectOpportunities == null || selectOpportunities.isEmpty())) {
            resetValues();
            noRecordsFound();
        }
        else {

            allOpportunities.addAll(selectOpportunities);
            opportunityAdapter.setOpportunitiesList(selectOpportunities);
            opportunityAdapter.notifyDataSetChanged();

        }
    }

    public class OpportunityAdapter extends BaseAdapter {

        Context context;
        private final LayoutInflater layoutInflater;
        private List<Opportunity> opportunities;
        Utility utility;
        private final String valueColorCode;
        private final ArrayList<OppStageDetails> limitedStageDetails;


        public OpportunityAdapter(Context context, ArrayList<OppStageDetails> limitedStageDetails) {
            this.context = context;
            this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            this.opportunities = new ArrayList<>();
            this.utility = Utility.getInstance();
            this.limitedStageDetails = limitedStageDetails;
            this.valueColorCode = "#205889"; // R.color.activity_list_value_color
        }

        public void setOpportunitiesList(List<Opportunity> opportunity) {

            if (opportunity == null) {
                if (this.opportunities != null) {
                    this.opportunities.clear();
                    this.opportunities = null;
                }
                this.opportunities = new ArrayList<>();
            } else {
                if (this.opportunities != null && this.opportunities.size() > 0)
                    this.opportunities.addAll(opportunity);
                else
                    this.opportunities = opportunity;
            }
        }

        public class OpportunityViewHolder {
            MMTextView order_close_date, order_client, order_product, order_description, order_sales_rep, order_date, order_net, order_status, order_stage;
            TextView txt_1, txt_2, txt_3, txt_4, txt_5, txt_6;
            ImageView order_delete;
        }

        @Override
        public int getCount() {
            return opportunities.size();
        }

        @Override
        public Opportunity getItem(int position) {
            return opportunities.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            try {

                OpportunityViewHolder ordersListViewHolder;
                if (convertView == null) {
                    convertView = layoutInflater.inflate(R.layout.opportunities_list_row_item, null);

                    ordersListViewHolder = new OpportunityViewHolder();
                    ordersListViewHolder.order_close_date = convertView.findViewById(R.id.order_close_date);
                    ordersListViewHolder.order_client = convertView.findViewById(R.id.order_client);
                    ordersListViewHolder.order_product = convertView.findViewById(R.id.order_product);
                    ordersListViewHolder.order_description = convertView.findViewById(R.id.order_description);
                    ordersListViewHolder.order_sales_rep = convertView.findViewById(R.id.order_sales_rep);
                    ordersListViewHolder.order_date = convertView.findViewById(R.id.order_date);
                    ordersListViewHolder.order_net = convertView.findViewById(R.id.order_net);
                    ordersListViewHolder.order_status = convertView.findViewById(R.id.order_status);
                    ordersListViewHolder.order_stage = convertView.findViewById(R.id.order_stage);
                    ordersListViewHolder.order_delete = convertView.findViewById(R.id.order_delete);
                    ordersListViewHolder.txt_1 = convertView.findViewById(R.id.txt_1);
                    ordersListViewHolder.txt_2 = convertView.findViewById(R.id.txt_2);
                    ordersListViewHolder.txt_3 = convertView.findViewById(R.id.txt_3);
                    ordersListViewHolder.txt_4 = convertView.findViewById(R.id.txt_4);
                    ordersListViewHolder.txt_5 = convertView.findViewById(R.id.txt_5);
                    ordersListViewHolder.txt_6 = convertView.findViewById(R.id.txt_6);

                    convertView.setTag(ordersListViewHolder);
                } else {
                    ordersListViewHolder = (OpportunityViewHolder) convertView.getTag();
                }

                final Opportunity opportunity = getItem(position);
                ordersListViewHolder.order_status.setText(opportunity.getStatus());
                ordersListViewHolder.order_client.setText(Utility.getFormattedKeyAndColouredNormalText("Opportunity Name", opportunity.getName(), valueColorCode));
                ordersListViewHolder.order_product.setText(Utility.getFormattedKeyAndColouredNormalText("Company Name", opportunity.getContactDetails().getName(), valueColorCode));
                ordersListViewHolder.order_description.setText(Utility.getFormattedKeyAndColouredNormalText("Contact Name", opportunity.getSubContactDetails().getContactFullName(), valueColorCode));

                if(opportunity.getAssignedTo()==null || opportunity.getAssignedTo().equals("")){
                    for(int i=0;i<allRepsList.size();i++){
                        if(allRepsList.get(i).getId().equals(String.valueOf(opportunity.getAssignedTODetails().getId()))){
                            ordersListViewHolder.order_sales_rep.setVisibility(View.VISIBLE);
                            ordersListViewHolder.order_sales_rep.setText(Utility.getFormattedKeyAndColouredNormalText("Assigned To", allRepsList.get(i).getName(), valueColorCode));
                        }
                        else {
                            ordersListViewHolder.order_sales_rep.setVisibility(View.VISIBLE);
                        }
                    }
                }
                else {
                    ordersListViewHolder.order_sales_rep.setText(Utility.getFormattedKeyAndColouredNormalText("Assigned To", opportunity.getAssignedTo(), valueColorCode));
                }
                ordersListViewHolder.order_date.setText(Utility.getFormattedKeyAndColouredNormalText("Probability (%)", String.valueOf(opportunity.getProbability()), valueColorCode));
                ordersListViewHolder.order_net.setText(Utility.getFormattedKeyAndColouredNormalText("Amount", "$ "+opportunity.getAmount(), valueColorCode));
                ordersListViewHolder.order_stage.setText(Utility.getFormattedKeyAndColouredNormalText("Stage", String.valueOf(opportunity.getOppStageDetails().getStage()), valueColorCode));

                String pattern = "yyyy-MM-dd'T'HH:mm:ss";
                SimpleDateFormat input = new SimpleDateFormat(pattern, Locale.getDefault());
                SimpleDateFormat output = new SimpleDateFormat(SELECTED_DATE_DISPLAY_FORMAT,Locale.getDefault());

                Date oneWayTripDate = input.parse(opportunity.getCloseDate());                 // parse input
                if (oneWayTripDate != null) {
                    String selectedDate = (output.format(oneWayTripDate));
                    ordersListViewHolder.order_close_date.setText(Utility.getFormattedKeyAndColouredNormalText("Proj Close Date", selectedDate, valueColorCode));
                } else {
                    ordersListViewHolder.order_close_date.setText(Utility.getFormattedKeyAndColouredNormalText("Proj Close Date", "", valueColorCode));
                }

                if (opportunity.getStatus().equals("Lost")) {

                    ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_inactive);
                    ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_inactive);
                    ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_inactive);
                    ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_inactive);
                    ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_inactive);
                    ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_1);

                    ordersListViewHolder.order_status.setBackgroundResource(R.drawable.stage_1);


                } else if (opportunity.getStatus().equals("Won")) {

                    ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                    ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_2);
                    ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_3);
                    ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_4);
                    ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_5);
                    ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_6);

                    ordersListViewHolder.order_status.setBackgroundResource(R.drawable.stage_6);

                } else {
                    ordersListViewHolder.order_status.setBackgroundResource(R.drawable.stage_4);
                    int index = 0;
                    for (int i = 0; i < limitedStageDetails.size(); i++) {
                        if (limitedStageDetails.get(i).getStage().trim().equals(opportunity.getOppStageDetails().getStage().trim())) {
                            index = i;
                        }
                    }

                    List<Integer> colorList = getColorCode(limitedStageDetails.size());
                    Log.e("veera","size"+limitedStageDetails.size());
                    Log.e("veera","size"+colorList.size());

                    Log.e("veera",colorList.toString());
                    int result=colorList.get(index);

                    if(result==0){

                        ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                        ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_inactive);

                    }
                    else if(result==1){
                        ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                        ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_2);
                        ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_inactive);

                    }
                    else if(result==2){
                        ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                        ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_2);
                        ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_3);
                        ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_inactive);

                    }
                    else if(result==3){
                        ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                        ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_2);
                        ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_3);
                        ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_4);
                        ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_inactive);
                        ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_inactive);

                    }
                    else if(result==4){

                        ordersListViewHolder.txt_1.setBackgroundResource(R.drawable.stage_1);
                        ordersListViewHolder.txt_2.setBackgroundResource(R.drawable.stage_2);
                        ordersListViewHolder.txt_3.setBackgroundResource(R.drawable.stage_3);
                        ordersListViewHolder.txt_4.setBackgroundResource(R.drawable.stage_4);
                        ordersListViewHolder.txt_5.setBackgroundResource(R.drawable.stage_5);
                        ordersListViewHolder.txt_6.setBackgroundResource(R.drawable.stage_inactive);

                    }
                }

                ordersListViewHolder.order_delete.setOnClickListener(v -> {
                    String message = "Are you sure, You want to delete?";
                    MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, new AlertDialogSelectionListener() {
                        @Override
                        public void alertDialogCallback() {
                        }

                        @Override
                        public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
                            if (buttonType == Constants.ButtonType.POSITIVE) {
                                //  MMAlertDialog.dismiss();
                                deleteOpportunity(opportunity.getId());
                            } else if (buttonType == Constants.ButtonType.NEGATIVE) {
                                MMAlertDialog.dismiss();
                                //set the default according to value
                            }
                        }
                    }, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), message, "Yes", "No");


                });

            } catch (Exception e) {
                e.printStackTrace();
            }
            return convertView;
        }

        private List<Integer> getColorCode(int n) {

            List<Integer> colourList = new ArrayList<>();
            double s = Math.ceil(n / 5.0);
            int remainder = n % 5;
            if (remainder == 0) {
                for (int i = 0; i < 5; i++) {
                    for (int k = 0; k < s; k++)
                        colourList.add(i);
                }
                return colourList;
            }
            for (int i = 0; i < remainder; i++) {
                for (int k = 0; k < s; k++)
                    colourList.add(i);
            }
            for (int i = remainder; i < 5; i++) {
                for (int k = 0; k < s - 1; k++)
                    colourList.add(i);
            }
            return colourList;
        }
    }
    private void deleteOpportunity(Integer id) {
        if (!MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.showProgressDialog(activityContext);
        commonModel.deleteOpportunity(encryptedId,domain,id,userId);
    }


    public void updateDeleteOpportunity() {
        Toast.makeText(applicationContext, "Deleted successfully", Toast.LENGTH_SHORT).show();
        resetValues();
        getOpportunities(currentPageNumber);
    }
}
