package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.SaveListContactAdpter;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SaveListActivity extends ListViewBaseActivity implements AdapterView.OnItemSelectedListener {

    String value,Display;
    ContactsModel contactsModel;
    public static final String TAG = SaveListActivity.class.getSimpleName();
    SaveListContactAdpter saveListAdpter;
    TextView tv_no_contacts_found,txt_title;
    private MMSharedPreferences sharedPreferences;
    public  boolean isAlertDialogShown = true;
    private ArrayList<String> categoriesKeyValues;
    private Map<String, String> categoriesHashMap;
    ImageView icon_search;
    String selectedCategory;
    EditText spinner_tpye;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_list);
        Bundle bundle = getIntent().getExtras();
        value = bundle.getString("value");
        Display=bundle.getString("display");
        init();
    }

    private void init() {

        // initializing super class variables
        txt_title = findViewById(R.id.txt_title);
        txt_title.setText(Display);
        tv_no_contacts_found = findViewById(R.id.tv_no_contacts_found);
        tv_no_contacts_found.setVisibility(View.GONE);
        spinner_tpye=findViewById(R.id.spinner_tpye);
        icon_search=findViewById(R.id.icon_search);
        sharedPreferences = new MMSharedPreferences(applicationContext);
        activityContext = SaveListActivity.this;
        currentActivity = this;
        requestFrom = Constants.RequestFrom.SAVE_LIST;
        contactsModel = new ContactsModel(activityContext, this, requestFrom);
        list_view =  findViewById(R.id.near_contacts_list_view);
        saveListAdpter = new SaveListContactAdpter(activityContext,this);
        globalContent.setNearestContactList(null);
        list_view.setAdapter(saveListAdpter);
        list_view.setDrawingCacheEnabled(true);
        list_view.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        Spinner category_spinner = findViewById(R.id.category_spinner);

        // Preparing Data for Spinners
        categoriesHashMap = new LinkedHashMap<>();
        categoriesHashMap.put("Filter by", "No Filter");
        categoriesHashMap.put("City", DashboardActivity.city);
        categoriesHashMap.put("State", DashboardActivity.state);
        categoriesHashMap.put("Country", DashboardActivity.country);
        categoriesHashMap.put("Zip",DashboardActivity.zipcode);
        categoriesKeyValues = new ArrayList<>(categoriesHashMap.keySet());

        SpinnerAdapter categoriesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, categoriesKeyValues);
        categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        category_spinner.setAdapter(categoriesSpinnerAdapter);
        category_spinner.setSelection(0, false);
        category_spinner.setOnItemSelectedListener(this);
        selectedCategory = categoriesKeyValues.get(0);
        spinner_tpye.setText(categoriesHashMap.get(selectedCategory));
        if(selectedCategory.equals("Filter by")){
            spinner_tpye.setEnabled(false);
        }else {
            spinner_tpye.setEnabled(true);

        }
        list_view.setOnItemClickListener((adapterView, view, position, id) -> {
            Log.e("ram",""+position);
            contactsModel.getCustomerBasicDetails(encryptedClientKey, getDTTicks(),String.valueOf(globalContent.getNearestContactList().get(position).getGsCustomersID()) );
        });
        page_number_display = findViewById(R.id.near_contacts_page_number_display);
        left_nav_arrow = findViewById(R.id.near_contacts_left_nav_arrow);
        right_nav_arrow = findViewById(R.id.near_contacts_right_nav_arrow);

        //  LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshDataBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));
        resetValues(true);

        spinner_tpye.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                isAlertDialogShown=true;
                resetValues(true);
                return true;
            }
            return false;
        });

        icon_search.setOnClickListener(v -> {
            isAlertDialogShown=true;
            resetValues(true);
        });

    }
    public void resetValues(boolean sendRequest) {
        super.resetValues();
        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;
        globalContent.setNearestContactList(null);
        saveListAdpter.notifyDataSetChanged();
        if (sendRequest)
            getsaveList(currentPageNumber);
    }

    public void getsaveList(int pageNumber) {

        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            int startIndex = ((pageNumber - 1) * Constants.SAVE_PAGE_SIZE_INT) + 1;
            int endIndex = pageNumber * Constants.SAVE_PAGE_SIZE_INT;
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("StartIndex", startIndex);
                    jsonObject.put("LastIndex", endIndex);
                    jsonObject.put("Mode","search");
                    jsonObject.put("SavedSearchID",value);
                    JSONObject searchQueryObject = new JSONObject();
                    // searchQueryObject.put("UserID", 188);
                  //  searchQueryObject.put("UserID", sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID));
                    switch (selectedCategory) {
                        case "City":
                            String city_type= spinner_tpye.getText().toString().trim();
                            searchQueryObject.put("City", city_type);
                            break;
                        case "State":
                            String state_type= spinner_tpye.getText().toString().trim();
                            searchQueryObject.put("State", state_type);
                            break;
                        case "Country":
                            String Country_type= spinner_tpye.getText().toString().trim();
                            searchQueryObject.put("Country", Country_type);
                            break;
                        case "Zip":
                            String Zip_type= spinner_tpye.getText().toString().trim();
                            searchQueryObject.put("Zip", Zip_type);
                            break;
                    }
                    // searchQueryObject.put("City", DashboardActivity.city);
                    jsonObject.put("SearchQuery", searchQueryObject);
                    Log.e("chinna",jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
                String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));

                if(isAlertDialogShown){

                    if (!MMProgressDialog.isProgressDialogShown)
                        MMProgressDialog.showProgressDialog(activityContext);
                    isAlertDialogShown=false;
                }
                else {
                    if (!MMProgressDialog.isProgressDialogShown)
                        MMProgressDialog.showProgressDialog(activityContext);
                    // MMProgressDialog.hideProgressDialog();
                }
                contactsModel.getSaveContacts(jsonObject, encryptedId, sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN));
                isLoading = true;
            }
    }


    public void updateRepContactsList(List<NearbyContacts> nearByContactsList, int noOfRecentContacts) {

        if (currentPageNumber == 1 && (nearByContactsList == null || nearByContactsList.isEmpty())) {
            resetValues(false);
            noRecordsFound();
            if (MMProgressDialog.isProgressDialogShown) {
                MMProgressDialog.hideProgressDialog();
            }
        }
        else {
            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + nearByContactsList.size();

            if (noOfRecentContacts != totalNoOfRecords) {
                totalNoOfRecords = noOfRecentContacts;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.SAVE_PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;
                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.SAVE_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }
                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;

                            getsaveList(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.SAVE_PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setNearestContactList(nearByContactsList);
            saveListAdpter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;

        }

    public void plotAllContacts(View view) {
        Intent intent = new Intent(this, LocateContactOnMapActivity.class);
        if (saveListAdpter.arrayList != null && saveListAdpter.arrayList.size() > 0) {
            if(saveListAdpter.arrayList.size() < 9){
                ArrayList<NearbyContacts> nearbyContacts = new ArrayList<>(globalContent.getNearestContactList());
                intent.putParcelableArrayListExtra("Data", nearbyContacts);
                intent.putExtra("Type", "PlotAll");
                startActivity(intent);
            }
            else {
                String title = "Saved Contacts";
                String message = "Please Select Less than 8 Contacts ";
                // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
                MMAlertDialog.showAlertDialog(activityContext, title, message);
                // Toast.makeText(NearbyContactsActivity.this, "Your Selecting more than 8 contacts..", Toast.LENGTH_SHORT).show();
            }

        } else {
            String title = "Saved Contacts";
            String message = "Please Select Atleast One Contact";
            MMAlertDialog.showAlertDialog(activityContext, title, message);
            // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
            //  Toast.makeText(NearbyContactsActivity.this, "Please Select contacts..", Toast.LENGTH_SHORT).show();
        }
    }

    public void updateCustomerBasicDetails(DetailContact detailContact) {
        MMProgressDialog.hideProgressDialog();
        if (String.valueOf(detailContact.getCanViewEmployeeId()).equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, detailContact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD_QUICK_SEARCH);
            //openContactDetailsIntent.putExtra(Constants.BUNDLE_QUICK_SEARCH_CONTACT_TYPE, quickSearchContactType);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Un-registering Local Broadcast Manager
        // LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshDataBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
          Intent  intent = new Intent(activityContext, MyContactsActivity.class);
          startActivity(intent);
          overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }
    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        saveListAdpter.arrayList.clear();
        selectedCategory = categoriesKeyValues.get(position);
        spinner_tpye.setText(categoriesHashMap.get(selectedCategory));
       // Toast.makeText(currentActivity, selectedCategory, Toast.LENGTH_SHORT).show();
        if(selectedCategory.equals("Filter by")){
            spinner_tpye.setEnabled(false);
        }else {
            spinner_tpye.setEnabled(true);
        }
        isAlertDialogShown=true;
        resetValues(true);

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {


    }
}
