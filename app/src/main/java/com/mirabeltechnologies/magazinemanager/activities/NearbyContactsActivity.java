package com.mirabeltechnologies.magazinemanager.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.NearContactAdpter;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.Info;
import com.mirabeltechnologies.magazinemanager.beans.NearbyContacts;
import com.mirabeltechnologies.magazinemanager.beans.POstalLocation;
import com.mirabeltechnologies.magazinemanager.beans.PostOfficeAddressRes;
import com.mirabeltechnologies.magazinemanager.beans.PostalDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.PostOfficeResApi;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.GPSTracker;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.RANGE_PICKER;

public class NearbyContactsActivity extends ListViewBaseActivity {

    ContactsModel contactsModel;
    public static final String TAG = NearbyContactsActivity.class.getSimpleName();
    NearContactAdpter nearContactAdpter;
    ArrayList<NearbyContacts> ContactsList;
    TextView tv_no_contacts_found;
    private MMSharedPreferences sharedPreferences;
    MMSettingsPreferences mmSettingsPreferences;
    String type = "post_office";
    private int PROXIMITY_RADIUS;
    public static String url;
    PostOfficeResApi api;
    List<Info> infoList;
    Button btn_deburg;
    float distance;
    Boolean Pagecount=false;
    List<PostalDetails> distancelist;
    List<String> zipcode_list;
    List<String> distinct_list;
    int cartTotalItems=0;
    private ProgressDialog progressDialog;
    GPSTracker gpsTracker;
    int index=0;
    public  boolean isAlertDialogShown = true;
    String gurl,object;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nearestcontact);
        init();
    }
    private void init() {

        mmSettingsPreferences = new MMSettingsPreferences(getApplicationContext());
        PROXIMITY_RADIUS=mmSettingsPreferences.getIntegerValue(RANGE_PICKER)*1000;
        tv_no_contacts_found = findViewById(R.id.tv_no_contacts_found);
        tv_no_contacts_found.setVisibility(View.GONE);
        btn_deburg=findViewById(R.id.btn_deburg);
        progressBar= findViewById(R.id.progressBarSmall);
        sharedPreferences = new MMSharedPreferences(applicationContext);
        progressBar.setVisibility(View.GONE);
        gpsTracker= new GPSTracker(this);
        activityContext = NearbyContactsActivity.this;
        // initializing super class variables
        currentActivity = this;
        requestFrom = Constants.RequestFrom.NEARBY_CONTACTS;
        contactsModel = new ContactsModel(activityContext, this, requestFrom);
        infoList= new ArrayList<>();
        distancelist=new ArrayList<>();
        zipcode_list= new ArrayList<>();
        list_view =  findViewById(R.id.near_contacts_list_view);
        nearContactAdpter = new NearContactAdpter(activityContext,this);
        globalContent.setNearestContactList(null);
        list_view.setAdapter(nearContactAdpter);

        list_view.setOnItemClickListener((adapterView, view, position, id) -> {
            Log.e("ram",""+position);
            contactsModel.getCustomerBasicDetails(encryptedClientKey, getDTTicks(),String.valueOf(globalContent.getNearestContactList().get(position).getGsCustomersID()) );
        });
        page_number_display = findViewById(R.id.near_contacts_page_number_display);
        left_nav_arrow = findViewById(R.id.near_contacts_left_nav_arrow);
        right_nav_arrow = findViewById(R.id.near_contacts_right_nav_arrow);

        //  LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshDataBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));
        if(gpsTracker.canGetLocation()){

            near(type); }
        else {
            gpsTracker.showSettingsAlert();
        }
        btn_deburg.setOnClickListener(v -> {

            Intent intent = new Intent(NearbyContactsActivity.this, Testing.class);
            intent.putExtra("veera",distinct_list.toString());
            startActivity(intent);


        });
    }


    public void plotAllContacts(View view) {
        Intent intent = new Intent(this, LocateContactOnMapActivity.class);
        if (nearContactAdpter.arrayList != null && nearContactAdpter.arrayList.size() > 0) {
            if(nearContactAdpter.arrayList.size() < 9){
                ArrayList<NearbyContacts> nearbyContacts = new ArrayList<>(globalContent.getNearestContactList());
                intent.putParcelableArrayListExtra("Data", nearbyContacts);
                intent.putExtra("Type", "PlotAll");
                startActivity(intent);
            }
            else {

                String title = "Contacts Near You";
                String message = "Please Select Less than 8 Contacts ";

                // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);

                MMAlertDialog.showAlertDialog(activityContext, title, message);
                // Toast.makeText(NearbyContactsActivity.this, "Your Selecting more than 8 contacts..", Toast.LENGTH_SHORT).show();

            }

        } else {

            String title = "Contacts Near You";
            String message = "Please Select Atleast One Contact";
            MMAlertDialog.showAlertDialog(activityContext, title, message);

            // MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);

            //  Toast.makeText(NearbyContactsActivity.this, "Please Select contacts..", Toast.LENGTH_SHORT).show();
        }
    }
    public void resetValues(boolean sendRequest) {
        super.resetValues();
        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;
        globalContent.setNearestContactList(null);
        nearContactAdpter.notifyDataSetChanged();
        if (sendRequest)
            getNearContacts(currentPageNumber);
    }

    private void near(String type) {
        StringBuilder googlePlacesUrl = new StringBuilder("https://maps.googleapis.com/maps/api/place/nearbysearch/json?");
        if(gpsTracker.getLocation()!=null){

            googlePlacesUrl.append("location=" +gpsTracker.getLatitude()+","+gpsTracker.getLongitude());
        }
        else if(DashboardActivity.lastLocation!=null){
            googlePlacesUrl.append("location=" +DashboardActivity.lastLocation.getLatitude()+","+DashboardActivity.lastLocation.getLongitude());
        }
       // googlePlacesUrl.append("location=" +26.12231+","+ -80.14338);
        //   googlePlacesUrl.append("location=" +37.421998333333335+","+ -122.08400000000002);

        googlePlacesUrl.append("&radius=" + PROXIMITY_RADIUS);
        googlePlacesUrl.append("&type=" + type);
        googlePlacesUrl.append("&sensor=true");                       //AIzaSyDBo0SmnDQyEbVE4N6RXb1A8vrVB2puasc
        googlePlacesUrl.append("&key=" + Constants.GOOGLE_API_KEY);  //AIzaSyDgoqqVDtBmxWdGZYxczmY342RvQSkhVx4
        url=String.valueOf(googlePlacesUrl);
        url.replaceAll("", "%20");

        Log.e("VEERA",url);
        gurl=googlePlacesUrl.toString();

        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                .create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BaseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        progressDialog = ProgressDialog.show(NearbyContactsActivity.this, "Please wait.",
                "Please wait while we fetch contacts near your current location!", true);
        api = retrofit.create(PostOfficeResApi.class);
        Call<PostOfficeAddressRes> call = api.getData(url);
        call.enqueue(new Callback<PostOfficeAddressRes>() {
            @Override
            public void onResponse(Call<PostOfficeAddressRes> call, Response<PostOfficeAddressRes> response) {
                if(response.isSuccessful()) {
                    //System.out.println("response is successful in Completed");
                    PostOfficeAddressRes historyRes = response.body();
                    String s = historyRes.getStatus();
                    if(s.equals("OK")){
                        if(historyRes.getResults()!=null && !historyRes.getResults().isEmpty()){
                            infoList = historyRes.getResults();

                            DownloadLocation downloadLocCoordinates = new DownloadLocation();
                            downloadLocCoordinates.execute();
                        }
                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(NearbyContactsActivity.this,s,Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(NearbyContactsActivity.this,response.message(),Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<PostOfficeAddressRes> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(NearbyContactsActivity.this,t.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
    }

    class DownloadLocation extends AsyncTask<Void, Void, Integer> {
        @Override
        protected Integer doInBackground(Void... voids) {
            for(Info info:infoList){
                getLocationFromAddress(info.getVicinity(),info.getGeometry().getLocation());
            }
            return infoList.size();
        }
        @Override
        protected void onPostExecute(Integer aVoid) {
            super.onPostExecute(aVoid);

            Collections.sort(distancelist);
            Log.e("veera",distancelist.toString());

            for(PostalDetails details:distancelist){
                if(details.getPcode()!=null){
                    zipcode_list.add(details.getPcode());
                }
            }

            Log.e("veera",zipcode_list.toString());
            Set<String> primesWithoutDuplicates = new LinkedHashSet<String>(zipcode_list);
            distinct_list = new ArrayList<>(primesWithoutDuplicates);

            // distinct_list = new ArrayList<>(new HashSet<>(zipcode_list));

            if(DashboardActivity.zipcode!=null && !DashboardActivity.zipcode.equals("")){
                for(int i=0;i<distinct_list.size();i++){

                    if(distinct_list.get(i).equals(DashboardActivity.zipcode)){
                        distinct_list.remove(i);
                    }
                }
                distinct_list.add(0,DashboardActivity.zipcode);
            }
            Log.e("veera",distinct_list.toString());
            progressDialog.dismiss();
            // displayLongToast("Local Service integration  is Started. Please Wait ......");
            resetValues(true);
        }
    }

    private void getLocationFromAddress(String vicinity, POstalLocation post_location) {

        Geocoder coder = new Geocoder(NearbyContactsActivity.this, Locale.getDefault());
        List<Address> address;
        try {
            address = coder.getFromLocation(post_location.getLat(),post_location.getLng(), 1);
            if (address == null) {
                return ;
            }
            Address location = address.get(0);
            // googlePlacesUrl.append("location=" +26.12231+","+ -80.14338);
            Location locationA = new Location("point A");
           locationA.setLatitude(gpsTracker.getLatitude());
            locationA.setLongitude(gpsTracker.getLongitude());
            /*locationA.setLatitude(26.12231);
            locationA.setLongitude(-80.14338);*/
            Location locationB = new Location("point B");
            locationB.setLatitude(post_location.getLat());
            locationB.setLongitude(post_location.getLng());

            distance = locationA.distanceTo(locationB);
            //     System.out.println("====print new address== = " + distance);
            PostalDetails postalDetails= new PostalDetails();
            postalDetails.setDistance(distance);
            if(location.getPostalCode()!=null && !location.getPostalCode().isEmpty()){

                postalDetails.setPcode(location.getPostalCode());
            }

            distancelist.add(postalDetails);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void getNearContacts(int pageNumber) {

        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            int startIndex = ((pageNumber - 1) * Constants.ACTIVITY_PAGE_SIZE_INT) + 1;
            int endIndex = pageNumber * Constants.ACTIVITY_PAGE_SIZE_INT;
            // Log.e("chinna",String.valueOf(index));
            if(distinct_list.isEmpty()){
                MMAlertDialog.showAlertDialog(activityContext, "Device Error", "Device not Support by Fetching Contacts");
            }
            else if(index < distinct_list.size()){
                //  String domain = "tier1-dev"; // testing
                //  String domain = "mirabeldev";// connect GulfStreem
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("StartIndex", startIndex);
                    jsonObject.put("LastIndex", endIndex);
                    JSONObject searchQueryObject = new JSONObject();
                    // searchQueryObject.put("UserID", 188);
                    searchQueryObject.put("UserID", sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID));
                    searchQueryObject.put("Zip", distinct_list.get(index));
                    // searchQueryObject.put("City", DashboardActivity.city);
                    jsonObject.put("SearchQuery", searchQueryObject);
                    object=jsonObject.toString();
                    Log.e("chinna",jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
                String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
                //  Log.e("chinna",encryptedId);
                // Log.e("chinna",sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN));

                progressBar.setVisibility(View.VISIBLE);
                if(isAlertDialogShown){
                    if (!MMProgressDialog.isProgressDialogShown)
                        MMProgressDialog.showProgressDialog(activityContext);
                    isAlertDialogShown=false;
                }
                else {
                    if (!MMProgressDialog.isProgressDialogShown)
                        MMProgressDialog.hideProgressDialog();
                }
                contactsModel.getNearbyContacts(jsonObject, encryptedId, sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN));
                isLoading = true;
            }
            else {
                progressBar.setVisibility(View.GONE);
                if( ContactsList==null){

                    tv_no_contacts_found.setVisibility(View.VISIBLE);
                    list_view.setVisibility(View.GONE);
                }
                else {
                    tv_no_contacts_found.setVisibility(View.GONE);
                    list_view.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    public void closeSmallProcessBar() {
        progressBar.setVisibility(View.GONE);
    }
    public void updateIndex() {
        index++;
        Pagecount=true;
        currentPageNumber=1;
        getNearContacts(currentPageNumber);
    }
    public void updateRepContactsList( final List<NearbyContacts> nearByContactsList, int noOfRecentContacts) {

        if (currentPageNumber == 1 && (nearByContactsList == null || nearByContactsList.isEmpty())) {
            resetValues(false);
            noRecordsFound();
            if (MMProgressDialog.isProgressDialogShown) {
                MMProgressDialog.hideProgressDialog();
            }
        }
        else  {

            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber)){
                alreadyLoadedPageNumbersList.add(currentPageNumber);
                ContactsList= new ArrayList<>();
                ContactsList.addAll(nearByContactsList);
                alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + ContactsList.size();
                if(currentPageNumber==1){
                    if (noOfRecentContacts != totalNoOfRecords) {
                        cartTotalItems = cartTotalItems + noOfRecentContacts;
                        totalNoOfRecords = cartTotalItems;
                        totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);// display page items Count
                        isAllRecordsLoaded = currentPageNumber == totalNoOfPages;
                        updateFooterPageLabels(lastUpdatedFooterPageNumber);
                        // Log.e("chinna","chinna");
                    }
                }
                else {
                    alreadyLoadedPageNumbersList.add(currentPageNumber);
                    ContactsList= new ArrayList<>();
                    ContactsList.addAll(nearByContactsList);
                    if (noOfRecentContacts != totalNoOfRecords) {
                        cartTotalItems = cartTotalItems + ContactsList.size();
                        totalNoOfRecords = cartTotalItems;
                        totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);// display page items Count

                        isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

                        updateFooterPageLabels(lastUpdatedFooterPageNumber);
                        //  Log.e("chinna","veera");
                    }
                }
            }
            else {
                alreadyLoadedPageNumbersList.add(currentPageNumber);
                ContactsList= new ArrayList<>();
                ContactsList.addAll(nearByContactsList);
                alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + ContactsList.size();
                if (noOfRecentContacts != totalNoOfRecords) {
                    cartTotalItems = cartTotalItems + ContactsList.size();
                    totalNoOfRecords = cartTotalItems;
                    totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);// display page items Count

                    isAllRecordsLoaded = currentPageNumber == totalNoOfPages;
                    updateFooterPageLabels(lastUpdatedFooterPageNumber);
                    // Log.e("chinna","venkat");
                }
                else {
                    cartTotalItems = cartTotalItems + ContactsList.size();
                    totalNoOfRecords = cartTotalItems;
                    totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);// display page items Count
                    isAllRecordsLoaded = currentPageNumber == totalNoOfPages;
                    updateFooterPageLabels(lastUpdatedFooterPageNumber);
                    //  Log.e("chinna","venkat");
                }
            }
            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT)) {

                progressBar.setVisibility(View.GONE);
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }
                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                        int visibleThreshold = 5;
                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {

                            isAlertDialogShown=true;
                            //  Log.e("veera",""+cartTotalItems + totalItemCount);

                            if(cartTotalItems==totalNoOfRecords){
                                currentPageNumber++;
                                getNearContacts(currentPageNumber);

                            }
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }
            else {
                progressBar.setVisibility(View.VISIBLE);
                isAlertDialogShown=false;
                index++;
                currentPageNumber=1;
                getNearContacts(currentPageNumber);
            }

            globalContent.setNearestContactList(nearByContactsList);
            nearContactAdpter.notifyDataSetChanged();
            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);
            isAllRecordsLoaded = currentPageNumber == totalNoOfPages;
           /* if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }*/
            isLoading = false;
            if (MMProgressDialog.isProgressDialogShown) {
                MMProgressDialog.hideProgressDialog();
            }
        }
        isInitialRequest = false;
    }
    private void FooterPageLabels(int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + visibleItemCount) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);
        int stat=firstVisibleItem+1;
        int end=firstVisibleItem+visibleItemCount;
        page_number_display.setText(String.format("%d of %d Records ",stat, end));

        //  page_number_display.setText(String.format("Page %d of %d | %d - %d of %d", currentVisiblePageNo, totalNoOfPages, stat, end, totalItemCount));
    }
    public void updateCustomerBasicDetails(DetailContact detailContact) {
        MMProgressDialog.hideProgressDialog();
        if (String.valueOf(detailContact.getCanViewEmployeeId()).equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, detailContact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD_QUICK_SEARCH);
            //openContactDetailsIntent.putExtra(Constants.BUNDLE_QUICK_SEARCH_CONTACT_TYPE, quickSearchContactType);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Un-registering Local Broadcast Manager
        // LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshDataBroadcastReceiver);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    private BroadcastReceiver mRefreshDataBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    String currentContactOperation = bundle.getString(Constants.BUNDLE_CURRENT_OPERATION);
                    if (currentContactOperation.equals(Constants.CURRENT_OPERATION_EDIT_CONTACT)) {
                        /*
                         On contact edit, we will remove all cached my last 100 contacts & we will reload them in order to maintain same displaying order of contacts similar to web
                         and on new contact add (primary / sub contact), we are not refreshing my last 100 contacts data
                          */
                        //
                        //
                        resetValues(true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

}

