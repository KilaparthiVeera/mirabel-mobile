package com.mirabeltechnologies.magazinemanager.activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.AssignRepMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.BusinessMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.CityMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.CountryMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.CountyMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.CreateRepMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.LossReasonMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.ProbabilityMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.ProductMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.SourceMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.StageMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.StateMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.MultiselectionAndSearchfilters.TypeMultipleSearchFilter;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.AdvSearchSuggestionsAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.AssignRepData;
import com.mirabeltechnologies.magazinemanager.beans.CampaignSource;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.OppLossReasonDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppTypeDetails;
import com.mirabeltechnologies.magazinemanager.beans.OpportunitiesSearchFilter;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityAddress;
import com.mirabeltechnologies.magazinemanager.beans.OpportunityAdvSearch;
import com.mirabeltechnologies.magazinemanager.beans.Probability;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SearchOrdersFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.DelayAutoCompleteTextView;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactsAdvSearchListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.OrdersModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.SELECTED_DATE_DISPLAY_FORMAT;

public class OpportunitiesSearchActivity extends BaseActivity implements ContactsAdvSearchListener {

    private OpportunitiesSearchFilter searchFilter;
    private SearchOrdersFilter searchOrdersFilter;
    private OpportunityAdvSearch advSearch;

    Spinner sp_spinner;
    ProbabilityMultipleSearchFilter sp_prob;
    StageMultipleSearchFilter sp_stage;
    TypeMultipleSearchFilter sp_type;
    CreateRepMultipleSearchFilter sp_create_rep;
    AssignRepMultipleSearchFilter sp_assigned_rep;
    SourceMultipleSearchFilter sp_source;
    LossReasonMultipleSearchFilter sp_loss_reason;
    public static ProductMultipleSearchFilter sp_product;
    BusinessMultipleSearchFilter sp_business;
    CityMultipleSearchFilter sp_city;
    CountyMultipleSearchFilter sp_county;
    StateMultipleSearchFilter sp_state;
    CountryMultipleSearchFilter sp_country;

    private DelayAutoCompleteTextView search_company,search_email;
    private AdvSearchSuggestionsAdapter companyNameSuggestionsAdapter;

    private List<CompanyData> companyDataList;
    List<String> selectRepList,selectProbList;
    String selectRep_id,selectPro_id,selectRepName,selectProbName;
    private List<CompanyData> opportunity_List,phone_list,zip_list,emailList;
    private ArrayList<Probability> probabilityArrayList;
    private ArrayList<String> status_array;
    private ArrayList<OppStageDetails> allStageDetails = null;
    private ArrayList<OppTypeDetails> allTypeDetails = null;
    private ArrayList<RepData> allCreateRepsList = null;
    private ArrayList<AssignRepData> allAssignRepsList = null;
    private ArrayList<CampaignSource> allSourceList = null;
    private ArrayList<OppLossReasonDetails> allLossReasonDetail = null;
    public static ArrayList<MasterData> allBusinessUnits = null, allProducts = null;
    private ArrayList<OpportunityAddress> allCityList = null,allCountyList = null,
            allStateList = null,allCountryList = null;
    List<String> selectReturnRepList,selectReturnProbList;
    private OrdersModel ordersModel;
    private CommonModel commonModel;
    private Constants.RequestFrom requestFrom;

    EditText txt_opportunity_name;
    TextView txt_company_name,txt_select_opportunity,txt_prob,txt_stage,txt_type,txt_from_date,txt_to_date,txt_close_from_date,txt_close_to_date,txt_create_rep,txt_assigned_rep,
            txt_source,txt_business,txt_loss_reason,txt_email,txt_city,txt_county,txt_state,txt_country,
            txt_phone,txt_zip,txt_select_zip,txt_select_Phone;
    public static TextView  txt_product;
    Button search_filters_reset_button,search_filters_search_button;

    Calendar createStartDate,createEndDate,closeStartDate,closeEndDate;
    DatePickerDialog createDatePickerDialog,closeDatePickerDialog;

    String encryptedId,domain,company_Name="",opportunity_Name="",pro_Id="",stage_id="",type_id="",create_rep_id="",Assign_rep_id="",
            business_id="",product_id="",source_id="",loose_id="",email="",city="",county="",state="",country="",phone="",zip="";

    AlertDialog alertDialog;
    RecyclerView multi_recycler;
    TextView multi_alert_title;
    MultiSelectAdopter multiSelectAdopter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_opportunities_search);
        applicationContext= this;
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectRepList = bundle.getStringArrayList("repList");
            selectProbList = bundle.getStringArrayList("probList");
            selectRep_id=bundle.getString("rep_id");
            selectPro_id=bundle.getString("pro_id");
            selectRepName=bundle.getString("rep_name");
            selectProbName=bundle.getString("pro_name");
        }
        createStartDate = Calendar.getInstance();
        createEndDate = Calendar.getInstance();
        closeStartDate = Calendar.getInstance();
        closeEndDate = Calendar.getInstance();

        if (searchFilter == null)
        searchFilter= new OpportunitiesSearchFilter();
        if (searchOrdersFilter == null)
            searchOrdersFilter = new SearchOrdersFilter();
        if (advSearch == null)
        advSearch=new OpportunityAdvSearch();

        requestFrom = Constants.RequestFrom.SEARCH_OPPORTUNITY;

        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        domain=sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);

        ordersModel = new OrdersModel(activityContext, this, requestFrom);
        commonModel = new CommonModel(activityContext, this, requestFrom);
        companyNameSuggestionsAdapter = new AdvSearchSuggestionsAdapter(activityContext, this, encryptedClientKey);
        opportunity_List=new ArrayList<>();
        phone_list=new ArrayList<>();
        zip_list=new ArrayList<>();
        selectReturnRepList=new ArrayList<>();
        selectReturnProbList=new ArrayList<>();
        probabilityArrayList=new ArrayList<>();
        allStageDetails=new ArrayList<>();
        allTypeDetails=new ArrayList<>();
        allCreateRepsList=new ArrayList<>();
        allAssignRepsList=new ArrayList<>();
        allBusinessUnits = new ArrayList<>();
        allProducts = new ArrayList<>();
        allSourceList=new ArrayList<>();
        allLossReasonDetail=new ArrayList<>();
        companyDataList=new ArrayList<>();
        emailList=new ArrayList<>();
        allCityList=new ArrayList<>();
        allCountyList=new ArrayList<>();
        allStateList=new ArrayList<>();
        allCountryList=new ArrayList<>();

        sp_spinner=findViewById(R.id.sp_spinner);

        txt_source=findViewById(R.id.txt_source);
        sp_stage=findViewById(R.id.sp_stage);
        sp_prob=findViewById(R.id.sp_prob);
        sp_type=findViewById(R.id.sp_type);
        sp_create_rep=findViewById(R.id.sp_create_rep);
        sp_assigned_rep=findViewById(R.id.sp_assigned_rep);
        sp_source=findViewById(R.id.sp_source);
        sp_loss_reason=findViewById(R.id.sp_loss_reason);
        sp_business=findViewById(R.id.sp_business);
        sp_product=findViewById(R.id.sp_product);
        sp_city=findViewById(R.id.sp_city);
        sp_county=findViewById(R.id.sp_county);
        sp_state=findViewById(R.id.sp_state);
        sp_country=findViewById(R.id.sp_country);

        txt_company_name=findViewById(R.id.txt_company_name);
        txt_opportunity_name=findViewById(R.id.txt_opportunity_name);
        txt_select_opportunity=findViewById(R.id.txt_select_opportunity);

        txt_opportunity_name.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus){
                String opp_name=txt_opportunity_name.getText().toString();
                if(!TextUtils.isEmpty(opp_name)){
                    CompanyData companyData= new CompanyData();
                    companyData.setName(opp_name);
                    opportunity_List.add(companyData);
                    txt_opportunity_name.setText("");
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < opportunity_List.size(); i++) {
                        item.append(opportunity_List.get(i).getName());
                        if (i != opportunity_List.size() - 1) {
                            item.append(", ");
                        }
                    }
                   txt_select_opportunity.setText(item.toString());
                }
            }
        });

        txt_select_opportunity.setOnClickListener(v -> {

            if(opportunity_List!=null && !opportunity_List.isEmpty()){
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(OpportunitiesSearchActivity.this).inflate(R.layout.multi_select_alert, viewGroup, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(OpportunitiesSearchActivity.this);
                builder.setView(dialogView);
                builder.setCancelable(false);
                alertDialog = builder.create();
                alertDialog.show();
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancel);
                Button btn_save=dialogView.findViewById(R.id.btn_save);
                btn_cancel.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < opportunity_List.size(); i++) {
                        item.append(opportunity_List.get(i).getName());
                        if (i != opportunity_List.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_opportunity.setText(item.toString());
                });

                btn_save.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < opportunity_List.size(); i++) {
                        item.append(opportunity_List.get(i).getName());
                        if (i != opportunity_List.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_opportunity.setText(item.toString());
                });
                multi_alert_title=dialogView.findViewById(R.id.txt_title);
                multi_alert_title.setText("Selected opportunities" );
                multi_recycler=dialogView.findViewById(R.id.multi_recycler);
                multiSelectAdopter = new MultiSelectAdopter(this, opportunity_List,"opportunity");
                multi_recycler.setHasFixedSize(true);
                multi_recycler.setItemViewCacheSize(opportunity_List.size());
                multi_recycler.setDrawingCacheEnabled(true);
                multi_recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                multi_recycler.addItemDecoration(new DividerItemDecoration(multi_recycler.getContext(), DividerItemDecoration.VERTICAL));
                multi_recycler.setAdapter(multiSelectAdopter);
                multi_recycler.setLayoutManager(new LinearLayoutManager(this));

            }
            else {
                Toast.makeText(this, "Please Enter opportunity Name", Toast.LENGTH_SHORT).show();
            }
        });

        txt_stage=findViewById(R.id.txt_stage);
        txt_type=findViewById(R.id.txt_type);
        txt_prob=findViewById(R.id.txt_prob);
        if(selectProbName!=null && !selectProbName.isEmpty()){
            txt_prob.setText(selectProbName);
        }
        txt_from_date=findViewById(R.id.txt_from_date);
        txt_to_date=findViewById(R.id.txt_to_date);
        txt_close_from_date=findViewById(R.id.txt_close_from_date);
        txt_close_to_date=findViewById(R.id.txt_close_to_date);

        txt_create_rep=findViewById(R.id.txt_create_rep);
        txt_assigned_rep=findViewById(R.id.txt_assigned_rep);
        txt_business=findViewById(R.id.txt_business);
        txt_product=findViewById(R.id.txt_product);
        txt_loss_reason=findViewById(R.id.txt_loss_reason);
        txt_email=findViewById(R.id.txt_email);
        txt_city=findViewById(R.id.txt_city);
        txt_county=findViewById(R.id.txt_county);
        txt_state=findViewById(R.id.txt_state);
        txt_country=findViewById(R.id.txt_country);

        txt_phone=findViewById(R.id.txt_phone);
        txt_zip=findViewById(R.id.txt_zip);
        txt_select_Phone=findViewById(R.id.txt_select_Phone);
        txt_select_zip=findViewById(R.id.txt_select_zip);

        txt_phone.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus){
                String phone=txt_phone.getText().toString();
                if(!TextUtils.isEmpty(phone)){
                    CompanyData companyData= new CompanyData();
                    companyData.setName(phone);
                    phone_list.add(companyData);
                    txt_phone.setText("");
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < phone_list.size(); i++) {
                        item.append(phone_list.get(i).getName());
                        if (i != phone_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_Phone.setText(item.toString());
                }
            }
        });

        txt_select_Phone.setOnClickListener(v -> {

            if(phone_list!=null && !phone_list.isEmpty()){

                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(OpportunitiesSearchActivity.this).inflate(R.layout.multi_select_alert, viewGroup, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(OpportunitiesSearchActivity.this);
                builder.setView(dialogView);
                builder.setCancelable(false);
                alertDialog = builder.create();
                alertDialog.show();
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancel);
                Button btn_save=dialogView.findViewById(R.id.btn_save);
                btn_cancel.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < phone_list.size(); i++) {
                        item.append(phone_list.get(i).getName());
                        if (i != phone_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_Phone.setText(item.toString());

                });

                btn_save.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < phone_list.size(); i++) {
                        item.append(phone_list.get(i).getName());
                        if (i != phone_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_Phone.setText(item.toString());
                });

                multi_alert_title=dialogView.findViewById(R.id.txt_title);
                multi_alert_title.setText("Selected Phone Numbers" );
                multi_recycler=dialogView.findViewById(R.id.multi_recycler);
                multiSelectAdopter = new MultiSelectAdopter(this, phone_list,"phone");
                multi_recycler.setHasFixedSize(true);
                multi_recycler.setItemViewCacheSize(opportunity_List.size());
                multi_recycler.setDrawingCacheEnabled(true);
                multi_recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                multi_recycler.addItemDecoration(new DividerItemDecoration(multi_recycler.getContext(), DividerItemDecoration.VERTICAL));
                multi_recycler.setAdapter(multiSelectAdopter);
                multi_recycler.setLayoutManager(new LinearLayoutManager(this));
            }
            else {
                Toast.makeText(this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show();
            }

        });


        txt_zip.setOnFocusChangeListener((v, hasFocus) -> {
            if(!hasFocus){
                String zip=txt_zip.getText().toString();
                if(!TextUtils.isEmpty(zip)){
                    CompanyData companyData= new CompanyData();
                    companyData.setName(zip);
                    zip_list.add(companyData);
                    txt_zip.setText("");
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < zip_list.size(); i++) {
                        item.append(zip_list.get(i).getName());
                        if (i != zip_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_zip.setText(item.toString());
                }
            }
        });

        txt_select_zip.setOnClickListener(v -> {
            if(zip_list!=null && !zip_list.isEmpty()){
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(OpportunitiesSearchActivity.this).inflate(R.layout.multi_select_alert, viewGroup, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(OpportunitiesSearchActivity.this);
                builder.setView(dialogView);
                builder.setCancelable(false);
                alertDialog = builder.create();
                alertDialog.show();
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancel);
                Button btn_save=dialogView.findViewById(R.id.btn_save);

                btn_cancel.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < zip_list.size(); i++) {
                        item.append(zip_list.get(i).getName());
                        if (i != zip_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_zip.setText(item.toString());

                });
                btn_save.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    for (int i = 0; i < zip_list.size(); i++) {
                        item.append(zip_list.get(i).getName());
                        if (i != zip_list.size() - 1) {
                            item.append(", ");
                        }
                    }
                    txt_select_zip.setText(item.toString());

                });
                multi_alert_title=dialogView.findViewById(R.id.txt_title);
                multi_alert_title.setText("Selected Zip Codes" );
                multi_recycler=dialogView.findViewById(R.id.multi_recycler);
                multiSelectAdopter = new MultiSelectAdopter(this, zip_list,"zip");
                multi_recycler.setHasFixedSize(true);
                multi_recycler.setItemViewCacheSize(opportunity_List.size());
                multi_recycler.setDrawingCacheEnabled(true);
                multi_recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                multi_recycler.addItemDecoration(new DividerItemDecoration(multi_recycler.getContext(), DividerItemDecoration.VERTICAL));
                multi_recycler.setAdapter(multiSelectAdopter);
                multi_recycler.setLayoutManager(new LinearLayoutManager(this));

            }
            else {
                Toast.makeText(this, "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
            }


        });

        search_filters_reset_button=findViewById(R.id.search_filters_reset_button);
        search_filters_search_button=findViewById(R.id.search_filters_search_button);

        search_company = findViewById(R.id.search_company);
        search_company.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
        search_company.setAdapter(companyNameSuggestionsAdapter);
        search_company.setLoadingIndicator(findViewById(R.id.search_activities_filters_company_name_loading_indicator));
        search_company.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus) {
                companyNameSuggestionsAdapter.setTypeOfFilter(0);
                search_company.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            } else {
                search_company.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_gray, 0);
            }
        });

        search_company.setOnItemClickListener((adapterView, view, position, id) -> {
            CompanyData selectedCompany = (CompanyData) adapterView.getItemAtPosition(position);
            companyDataList.add(selectedCompany);
            search_company.setText("");
            search_company.setAdapter(null); // to stop filtering after selecting row from drop down
            search_company.setAdapter(companyNameSuggestionsAdapter);
            search_company.clearFocus();
            StringBuilder item = new StringBuilder();
            if( companyDataList!=null && !companyDataList.isEmpty()){
                for (int i = 0; i < companyDataList.size(); i++) {
                    item.append(companyDataList.get(i).getName());
                    if (i != companyDataList.size() - 1) {
                        item.append(", ");
                    }
                }
                txt_company_name.setText(item.toString());
            }
            hideKeyboard(search_company);
        });

        txt_company_name.setOnClickListener(v ->{
            if(companyDataList!=null && !companyDataList.isEmpty()){
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(OpportunitiesSearchActivity.this).inflate(R.layout.multi_select_alert, viewGroup, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(OpportunitiesSearchActivity.this);
                builder.setView(dialogView);
                builder.setCancelable(false);
                alertDialog = builder.create();
                alertDialog.show();
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancel);
                Button btn_save=dialogView.findViewById(R.id.btn_save);
                btn_cancel.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    String ass_Id="";
                        for (int i = 0; i < companyDataList.size(); i++) {
                            item.append(companyDataList.get(i).getName());
                            if (i != companyDataList.size() - 1) {
                                item.append(", ");
                            }
                            ass_Id= String.format("%s%s", ass_Id, "SW="+companyDataList.get(i).getName());
                            if (i < companyDataList.size()) ass_Id += "~";
                        }

                        txt_company_name.setText(item.toString());
                });

                btn_save.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    String ass_Id="";
                        for (int i = 0; i < companyDataList.size(); i++) {
                            item.append(companyDataList.get(i).getName());
                            if (i != companyDataList.size() - 1) {
                                item.append(", ");
                            }
                            ass_Id= String.format("%s%s", ass_Id, "SW="+companyDataList.get(i).getName());
                            if (i < companyDataList.size()) ass_Id += "~";
                        }
                        txt_company_name.setText(item.toString());
                });
                multi_alert_title=dialogView.findViewById(R.id.txt_title);
                multi_alert_title.setText("Selected Companies" );
                multi_recycler=dialogView.findViewById(R.id.multi_recycler);
                multiSelectAdopter = new MultiSelectAdopter(this, companyDataList,"Company");
                multi_recycler.setHasFixedSize(true);
                multi_recycler.setItemViewCacheSize(opportunity_List.size());
                multi_recycler.setDrawingCacheEnabled(true);
                multi_recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                multi_recycler.addItemDecoration(new DividerItemDecoration(multi_recycler.getContext(), DividerItemDecoration.VERTICAL));
                multi_recycler.setAdapter(multiSelectAdopter);
                multi_recycler.setLayoutManager(new LinearLayoutManager(this));


            }else {
                Toast.makeText(this, "Please Choose Company", Toast.LENGTH_SHORT).show();
            }

        });

        search_email = findViewById(R.id.search_email);
        search_email.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
        search_email.setAdapter(companyNameSuggestionsAdapter);
        search_email.setLoadingIndicator(findViewById(R.id.search_activities_filters_email_loading_indicator));
        search_email.setOnFocusChangeListener((view, hasFocus) -> {
            if (hasFocus) {
                companyNameSuggestionsAdapter.setTypeOfFilter(2);
                search_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

            } else {
                search_email.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_gray, 0);
            }
        });

        search_email.setOnItemClickListener((adapterView, view, position, id) -> {
            CompanyData companyData= new CompanyData();
            companyData.setName(adapterView.getItemAtPosition(position).toString());
            emailList.add(companyData);
            search_email.setText("");
            search_email.setAdapter(null); // to stop filtering after selecting row from drop down
            search_email.setAdapter(companyNameSuggestionsAdapter);
            search_email.clearFocus();
            StringBuilder item = new StringBuilder();
            if( emailList!=null && !emailList.isEmpty()){
                for (int i = 0; i < emailList.size(); i++) {
                    item.append(emailList.get(i).getName());
                    if (i != emailList.size() - 1) {
                        item.append(", ");
                    }
                }
                txt_email.setText(item.toString());

            }
            hideKeyboard(search_email);
        });

        txt_email.setOnClickListener(v -> {
            if(emailList!=null && !emailList.isEmpty()){
                ViewGroup viewGroup = findViewById(android.R.id.content);
                View dialogView = LayoutInflater.from(OpportunitiesSearchActivity.this).inflate(R.layout.multi_select_alert, viewGroup, false);
                AlertDialog.Builder builder = new AlertDialog.Builder(OpportunitiesSearchActivity.this);
                builder.setView(dialogView);
                builder.setCancelable(false);
                alertDialog = builder.create();
                alertDialog.show();
                Button btn_cancel=dialogView.findViewById(R.id.btn_cancel);
                Button btn_save=dialogView.findViewById(R.id.btn_save);
                btn_cancel.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    String ass_Id="";
                    for (int i = 0; i < emailList.size(); i++) {
                        item.append(emailList.get(i).getName());
                        if (i != emailList.size() - 1) {
                            item.append(", ");
                        }
                        ass_Id= String.format("%s%s", ass_Id, "SW="+emailList.get(i).getName());
                        if (i < emailList.size()) ass_Id += "~";
                    }

                    txt_email.setText(item.toString());
                });
                btn_save.setOnClickListener(view -> {
                    alertDialog.dismiss();
                    StringBuilder item = new StringBuilder();
                    String ass_Id="";
                    for (int i = 0; i < emailList.size(); i++) {
                        item.append(emailList.get(i).getName());
                        if (i != emailList.size() - 1) {
                            item.append(", ");
                        }
                        ass_Id= String.format("%s%s", ass_Id, "SW="+emailList.get(i).getName());
                        if (i < emailList.size()) ass_Id += "~";
                    }
                    txt_email.setText(item.toString());
                });

                multi_alert_title=dialogView.findViewById(R.id.txt_title);
                multi_alert_title.setText("Selected Emails" );
                multi_recycler=dialogView.findViewById(R.id.multi_recycler);
                multiSelectAdopter = new MultiSelectAdopter(this, emailList,"email");
                multi_recycler.setHasFixedSize(true);
                multi_recycler.setItemViewCacheSize(opportunity_List.size());
                multi_recycler.setDrawingCacheEnabled(true);
                multi_recycler.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                multi_recycler.addItemDecoration(new DividerItemDecoration(multi_recycler.getContext(), DividerItemDecoration.VERTICAL));
                multi_recycler.setAdapter(multiSelectAdopter);
                multi_recycler.setLayoutManager(new LinearLayoutManager(this));
            }
            else {

                Toast.makeText(this, "Please Enter Email", Toast.LENGTH_SHORT).show();
            }

        });

      for (int i=0;i<=100;i+= 10){
          Probability probability= new Probability();
          probability.setProbabilityId(String.valueOf(i));
          probability.setProbabilityName(i+"%");
          probability.setSelected(false);
          probabilityArrayList.add(probability);
      }

        status_array= new ArrayList<>();
        status_array.add("All");
        status_array.add("Open");
        status_array.add("Won");
        status_array.add("Lost");
        SpinnerAdapter categoriesSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_row_item, status_array);
        categoriesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_spinner.setAdapter(categoriesSpinnerAdapter);
        sp_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!parent.getItemAtPosition(position).toString().equals(status_array.get(0))) {
                    searchFilter.setStatus(parent.getItemAtPosition(position).toString());
                }
                else {
                    searchFilter.setStatus("");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        txt_stage.setOnClickListener(v -> showAllStageUnits());
        txt_prob.setOnClickListener(v -> showAllProbability());

        txt_type.setOnClickListener(v -> showAllTypeUnits());
        txt_create_rep.setOnClickListener(v -> showAllCreateRepsList());
        txt_assigned_rep.setOnClickListener(v -> showAllAssignRepsList());
        txt_loss_reason.setOnClickListener(v -> showAllLossReason());
        txt_source.setOnClickListener(v-> showAllSources());
        search_filters_search_button.setOnClickListener(v-> calculateData());
        txt_business.setOnClickListener(v -> showAllBusinessUnits());
        txt_product.setOnClickListener(v -> showAllProducts());
        txt_city.setOnClickListener(v -> showAllCity());
        txt_county.setOnClickListener(v -> showAllCounty());
        txt_state.setOnClickListener(v -> showAllState());
        txt_country.setOnClickListener(v -> showAllCountry());

        txt_from_date.setOnClickListener(view -> {
            createDatePickerDialog = new DatePickerDialog(applicationContext,android.R.style.Theme_DeviceDefault_Light_Dialog, (view14, year, monthOfYear, dayOfMonth) -> {
                createStartDate.set(year, monthOfYear, dayOfMonth);
                String result=Utility.formatDate(createStartDate.getTime(), SELECTED_DATE_DISPLAY_FORMAT);
                txt_from_date.setText(result);
                txt_to_date.setText(null);
                searchFilter.setCreatedFrom(result);
            }, createStartDate.get(Calendar.YEAR), createStartDate.get(Calendar.MONTH), createStartDate.get(Calendar.DAY_OF_MONTH));
            createDatePickerDialog.setCancelable(false);
            createDatePickerDialog.show();
        });
        txt_to_date.setOnClickListener(view -> {
                createDatePickerDialog = new DatePickerDialog(applicationContext,android.R.style.Theme_DeviceDefault_Light_Dialog, (view13, year, monthOfYear, dayOfMonth) -> {
                    createEndDate.set(year, monthOfYear, dayOfMonth);
                   // if (startdate.after(enddate) || enddate.equals(startdate)){
                    if (createStartDate.after(createEndDate)){
                        txt_to_date.setText(null);
                    }else{
                        String result=Utility.formatDate(createEndDate.getTime(), SELECTED_DATE_DISPLAY_FORMAT);
                        txt_to_date.setText(result);
                        searchFilter.setCreatedTo(result);
                    }
                }, createEndDate.get(Calendar.YEAR), createStartDate.get(Calendar.MONTH),createStartDate.get(Calendar.DAY_OF_MONTH));
            createDatePickerDialog.getDatePicker().setMinDate(createStartDate.getTimeInMillis());
            createDatePickerDialog.setCancelable(false);
            createDatePickerDialog.show();
        });
        txt_close_from_date.setOnClickListener(view -> {
            closeDatePickerDialog = new DatePickerDialog(applicationContext,android.R.style.Theme_DeviceDefault_Light_Dialog, (view12, year, monthOfYear, dayOfMonth) -> {
                closeStartDate.set(year, monthOfYear, dayOfMonth);
                String result=Utility.formatDate(closeStartDate.getTime(), SELECTED_DATE_DISPLAY_FORMAT);
                txt_close_from_date.setText(result);
                txt_close_to_date.setText(null);
                searchFilter.setCloseFrom(result);
            }, closeStartDate.get(Calendar.YEAR), closeStartDate.get(Calendar.MONTH), closeStartDate.get(Calendar.DAY_OF_MONTH));
            closeDatePickerDialog.setCancelable(false);
            closeDatePickerDialog.show();
        });
        txt_close_to_date.setOnClickListener(view -> {
            closeDatePickerDialog = new DatePickerDialog(applicationContext,android.R.style.Theme_DeviceDefault_Light_Dialog, (view1, year, monthOfYear, dayOfMonth) -> {
                closeEndDate.set(year, monthOfYear, dayOfMonth);
                if (closeStartDate.after(closeEndDate)){
                    txt_close_to_date.setText(null);
                }else{
                    String result=Utility.formatDate(closeEndDate.getTime(), SELECTED_DATE_DISPLAY_FORMAT);
                    txt_close_to_date.setText(result);
                    searchFilter.setCloseTo(result);
                }
            }, closeEndDate.get(Calendar.YEAR), closeEndDate.get(Calendar.MONTH),closeEndDate.get(Calendar.DAY_OF_MONTH));
            closeDatePickerDialog.getDatePicker().setMinDate(closeStartDate.getTimeInMillis());
            closeDatePickerDialog.setCancelable(false);
            closeDatePickerDialog.show();
        });

        search_filters_reset_button.setOnClickListener(v-> onBackPressed());
        loadDataFromServer();
        LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE));

    }

    private void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
    }



    private void showAllAssignRepsList() {
        if (allAssignRepsList != null && allAssignRepsList.size() > 0) {
            txt_assigned_rep.setVisibility(View.GONE);
            sp_assigned_rep.setVisibility(View.VISIBLE);
            sp_assigned_rep.performClick();
        } else {
            sp_assigned_rep.setVisibility(View.GONE);
            txt_assigned_rep.setVisibility(View.VISIBLE);
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    private void showAllCreateRepsList() {
        if (allCreateRepsList != null && allCreateRepsList.size() > 0) {
            txt_create_rep.setVisibility(View.GONE);
            sp_create_rep.setVisibility(View.VISIBLE);
            sp_create_rep.setItems(allCreateRepsList,"Choose Rep",-1);
            sp_create_rep.performClick();
        } else {
            sp_create_rep.setVisibility(View.GONE);
            txt_create_rep.setVisibility(View.VISIBLE);
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    private void showAllStageUnits() {
        if (allStageDetails != null && allStageDetails.size() > 0) {
            txt_stage.setVisibility(View.GONE);
            sp_stage.setVisibility(View.VISIBLE);
            sp_stage.setItems(allStageDetails,"Choose Stage",-1);
            sp_stage.performClick();
        } else {
            sp_stage.setVisibility(View.GONE);
            txt_stage.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllStageUnits(encryptedId,domain);
        }
    }

    private void showAllProbability() {
        if (probabilityArrayList != null && probabilityArrayList.size() > 0) {
            txt_prob.setVisibility(View.GONE);
            sp_prob.setVisibility(View.VISIBLE);
            for(int i=0;i<probabilityArrayList.size();i++){
                if(selectProbList!=null && !selectProbList.isEmpty()){
                    for(int j=0;j<selectProbList.size();j++){
                        if(selectProbList.get(j).equals(probabilityArrayList.get(i).getProbabilityName())){
                            probabilityArrayList.get(i).setSelected(true);
                        }
                    }
                }
            }
            sp_prob.setItems(probabilityArrayList,"Choose Probability",-1);
            sp_prob.performClick();
        } else {
            sp_prob.setVisibility(View.GONE);
            txt_prob.setVisibility(View.VISIBLE);
            displayLongToast(getResources().getString(R.string.no_prob_found));

        }
    }


    private void showAllTypeUnits() {
        if (allTypeDetails != null && allTypeDetails.size() > 0) {
            txt_type.setVisibility(View.GONE);
            sp_type.setVisibility(View.VISIBLE);
            sp_type.setItems(allTypeDetails,"Choose Type",-1);
            sp_type.performClick();
        } else {
            sp_type.setVisibility(View.GONE);
            txt_type.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllTypeAndReason(encryptedId,domain,Constants.GET_ADMIN_DATA_TYPE);
        }
    }

    private void showAllBusinessUnits() {
        if (allBusinessUnits != null && allBusinessUnits.size() > 0) {
            allBusinessUnits.remove(0);
            txt_business.setVisibility(View.GONE);
            sp_business.setVisibility(View.VISIBLE);
            sp_business.setItems(allBusinessUnits,"Choose Business Unit",-1);
            sp_business.performClick();

          //  showAllValuesForSelection(Constants.SelectionType.BUSINESS_UNIT, allBusinessUnits, searchOrdersFilter.getBusinessUnitId());
        } else {
            sp_business.setVisibility(View.GONE);
            txt_business.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_BUSINESS_UNITS);
        }
    }

    public void showAllProducts() {

        if (allProducts != null && allProducts.size() > 0) {
            txt_product.setVisibility(View.GONE);
            sp_product.setVisibility(View.VISIBLE);
            sp_product.setItems(allProducts,"Choose Product",-1);
            sp_product.performClick();

        } else {
            sp_product.setVisibility(View.GONE);
            txt_product.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllProducts(encryptedId,domain,BusinessMultipleSearchFilter.select_businessId);
        }
    }

    private void showAllSources() {
        if (allSourceList != null && allSourceList.size() > 0) {
            txt_source.setVisibility(View.GONE);
            sp_source.setVisibility(View.VISIBLE);
            sp_source.setItems(allSourceList,"Choose Source",-1);
            sp_source.performClick();

        } else {
            sp_source.setVisibility(View.GONE);
            txt_source.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllSources(encryptedId,domain);
        }
    }

    private void showAllLossReason() {
        if (allLossReasonDetail != null && allLossReasonDetail.size() > 0) {
            txt_loss_reason.setVisibility(View.GONE);
            sp_loss_reason.setVisibility(View.VISIBLE);
            sp_loss_reason.setItems(allLossReasonDetail,"Choose Reason",-1);
            sp_loss_reason.performClick();

        } else {
            sp_loss_reason.setVisibility(View.GONE);
            txt_loss_reason.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllTypeAndReason(encryptedId,domain,Constants.GET_ADMIN_DATA_REASON);
        }
    }

    private void showAllCity() {
        if (allCityList != null && allCityList.size() > 0) {
            txt_city.setVisibility(View.GONE);
            sp_city.setVisibility(View.VISIBLE);
            sp_city.setItems(allCityList,"Choose City",-1);
            sp_city.performClick();
        } else {
            sp_city.setVisibility(View.GONE);
            txt_city.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllOpportunitiesAddress(encryptedId,domain,"city");
        }
    }

    private void showAllCounty() {
        if (allCountyList != null && allCountyList.size() > 0) {
            txt_county.setVisibility(View.GONE);
            sp_county.setVisibility(View.VISIBLE);
            sp_county.setItems(allCountyList,"Choose County",-1);
            sp_county.performClick();
        } else {
            sp_county.setVisibility(View.GONE);
            txt_county.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllOpportunitiesAddress(encryptedId,domain,"county");
        }
    }

    private void showAllState() {
        if (allStateList != null && allStateList.size() > 0) {
            txt_state.setVisibility(View.GONE);
            sp_state.setVisibility(View.VISIBLE);
            sp_state.setItems(allStateList,"Choose State",-1);
            sp_state.performClick();
        } else {
            sp_state.setVisibility(View.GONE);
            txt_state.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllOpportunitiesAddress(encryptedId,domain,"state");
        }
    }
    private void showAllCountry() {
        if (allCountryList != null && allCountryList.size() > 0) {
            txt_country.setVisibility(View.GONE);
            sp_country.setVisibility(View.VISIBLE);
            sp_country.setItems(allCountryList,"Choose Country",-1);
            sp_country.performClick();
        } else {
            sp_country.setVisibility(View.GONE);
            txt_country.setVisibility(View.VISIBLE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllOpportunitiesAddress(encryptedId,domain,"country");
        }
    }

    public void showAllValuesForSelection(Constants.SelectionType selectionType, ArrayList<MasterData> masterData, String preSelectedValue) {
        try {
            Intent showAllValuesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllValuesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
            // We may get TransactionTooLargeException as we have more issues around 2160 and we are getting these details in adapter class from static variable #allIssues
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, masterData);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, preSelectedValue);
            showAllValuesIntent.putExtras(bundle);
            startActivity(showAllValuesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void calculateData() {

        if(sp_prob.getSelectedItems()!=null && !sp_prob.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_prob.getSelectedItems().size(); i++) {
                selectReturnProbList.add(sp_prob.getSelectedItems().get(i).getProbabilityName());
                pro_Id = String.format("%s%s", pro_Id, "IE=" + sp_prob.getSelectedItems().get(i).getProbabilityId());
                if (i < sp_prob.getSelectedItems().size()) pro_Id += "~";
            }
        }
        else {
            pro_Id="";
        }
        if(sp_stage.getSelectedItems()!=null && !sp_stage.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_stage.getSelectedItems().size(); i++) {
                stage_id = String.format("%s%s", stage_id, "IE=" + sp_stage.getSelectedItems().get(i).getId());
                if (i < sp_stage.getSelectedItems().size()) stage_id += "~";
            }
        }
        else {
            stage_id="";
        }

        if(sp_type.getSelectedItems()!=null && !sp_type.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_type.getSelectedItems().size(); i++) {
                type_id = String.format("%s%s", type_id, "IE=" + sp_type.getSelectedItems().get(i).getId());
                if (i < sp_type.getSelectedItems().size()) type_id += "~";
            }
        }
        else {
            type_id="";
        }

        if(sp_create_rep.getSelectedItems()!=null && !sp_create_rep.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_create_rep.getSelectedItems().size(); i++) {
                create_rep_id = String.format("%s%s", create_rep_id, "IE=" + sp_create_rep.getSelectedItems().get(i).getId());
                if (i < sp_create_rep.getSelectedItems().size()) create_rep_id += "~";

            }
        }
        else {
            create_rep_id="";
        }


        if(sp_assigned_rep.getSelectedItems()!=null && !sp_assigned_rep.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_assigned_rep.getSelectedItems().size(); i++) {
                selectReturnRepList.add(sp_assigned_rep.getSelectedItems().get(i).getName());
                Assign_rep_id = String.format("%s%s", Assign_rep_id, "IE=" + sp_assigned_rep.getSelectedItems().get(i).getId());
                if (i < sp_assigned_rep.getSelectedItems().size()) Assign_rep_id += "~";

            }
        }
        else {
            Assign_rep_id="";
        }

        if(sp_business.getSelectedItems()!=null && !sp_business.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_business.getSelectedItems().size(); i++) {
                business_id = String.format("%s%s", business_id, "IE=" + sp_business.getSelectedItems().get(i).getId());
                if (i < sp_business.getSelectedItems().size()) business_id += "~";

            }
        }
        else {
            business_id="";
        }

        if(sp_product.getSelectedItems()!=null && !sp_product.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_product.getSelectedItems().size(); i++) {
                product_id = String.format("%s%s", product_id, "IE=" + sp_product.getSelectedItems().get(i).getId());
                if (i < sp_product.getSelectedItems().size()) product_id += "~";

            }
        }
        else {
            product_id="";
        }

        if(sp_source.getSelectedItems()!=null && !sp_source.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_source.getSelectedItems().size(); i++) {
                source_id = String.format("%s%s", source_id, "IE=" + sp_source.getSelectedItems().get(i).getSource());
                if (i < sp_source.getSelectedItems().size()) source_id += "~";

            }
        }
        else {
            source_id="";
        }

        if(sp_loss_reason.getSelectedItems()!=null && !sp_loss_reason.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_loss_reason.getSelectedItems().size(); i++) {
                loose_id = String.format("%s%s", loose_id, "IE=" + sp_loss_reason.getSelectedItems().get(i).getId());
                if (i < sp_loss_reason.getSelectedItems().size()) loose_id += "~";

            }
        }
        else {
            loose_id="";
        }

        if(sp_city.getSelectedItems()!=null && !sp_city.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_city.getSelectedItems().size(); i++) {
                city = String.format("%s%s", city, "IE=" + sp_city.getSelectedItems().get(i).getName());
                if (i < sp_city.getSelectedItems().size()) city += "~";

            }
        }
        else {
            city="";
        }

        if(sp_county.getSelectedItems()!=null && !sp_county.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_county.getSelectedItems().size(); i++) {
                county = String.format("%s%s", county, "IE=" + sp_county.getSelectedItems().get(i).getName());
                if (i < sp_county.getSelectedItems().size()) county += "~";

            }
        }
        else {
            county="";
        }

        if(sp_state.getSelectedItems()!=null && !sp_state.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_state.getSelectedItems().size(); i++) {
                state = String.format("%s%s", state, "IE=" + sp_state.getSelectedItems().get(i).getName());
                if (i < sp_state.getSelectedItems().size()) state += "~";

            }
        }
        else {
            state="";
        }

        if(sp_country.getSelectedItems()!=null && !sp_country.getSelectedItems().isEmpty()) {
            for (int i = 0; i < sp_country.getSelectedItems().size(); i++) {
                country = String.format("%s%s", country, "IE=" + sp_country.getSelectedItems().get(i).getName());
                if (i < sp_country.getSelectedItems().size()) country += "~";
            }
        }
        else {
            country="";
        }


        if(opportunity_List!=null && !opportunity_List.isEmpty()) {
            for (int i = 0; i < opportunity_List.size(); i++) {
                opportunity_Name = String.format("%s%s", opportunity_Name, "SW=" + opportunity_List.get(i).getName());
                if (i < opportunity_List.size()) opportunity_Name += "~";
            }
        }
        else {
            opportunity_Name="";
        }

        if(companyDataList!=null && !companyDataList.isEmpty()) {
            for (int i = 0; i < companyDataList.size(); i++) {
                company_Name = String.format("%s%s", company_Name, "SW=" + opportunity_List.get(i).getName());
                if (i < companyDataList.size()) company_Name += "~";
            }
        }
        else {
            company_Name="";
        }

        if(emailList!=null && !emailList.isEmpty()) {
            for (int i = 0; i < emailList.size(); i++) {
                email = String.format("%s%s", email, "SW=" + emailList.get(i).getName());
                if (i < emailList.size()) email += "~";
            }
        }
        else {
            email="";
        }

        if(phone_list!=null && !phone_list.isEmpty()) {
            for (int i = 0; i < phone_list.size(); i++) {
                phone = String.format("%s%s", phone, "SW=" + phone_list.get(i).getName());
                if (i < phone_list.size()) phone += "~";
            }
        }
        else {
            phone="";
        }

        if(zip_list!=null && !zip_list.isEmpty()) {
            for (int i = 0; i < zip_list.size(); i++) {
                zip = String.format("%s%s", zip, "SW=" + zip_list.get(i).getName());
                if (i < zip_list.size()) zip += "~";
            }
        }
        else {
            zip="";
        }

        if(ProbabilityMultipleSearchFilter.spinnerText!=null){
            searchFilter.setProbabilityName(ProbabilityMultipleSearchFilter.spinnerText);
        }
        else {
            searchFilter.setProbabilityName(selectProbName);
        }
        searchFilter.setProbability(pro_Id);
        searchFilter.setOppName(opportunity_Name);
        searchFilter.setCustomerName(company_Name);
        searchFilter.setStage(stage_id);
        searchFilter.setType(type_id);
        if(AssignRepMultipleSearchFilter.spinnerText!=null){
            searchFilter.setRepName(AssignRepMultipleSearchFilter.spinnerText);
        }
        else {
            searchFilter.setRepName(selectRepName);
        }
        searchFilter.setAssignedTo(Assign_rep_id);
        searchFilter.setCreatedBy(create_rep_id);
        searchFilter.setSource(source_id);
        searchFilter.setLossReason(loose_id);
        searchFilter.setBusinessUnit(business_id);
        searchFilter.setProducts(product_id);
        advSearch.setEmail(email);
        advSearch.setCity(city);
        advSearch.setCounty(county);
        advSearch.setState(state);
        advSearch.setCountry(country);
        advSearch.setPhone(phone);
        advSearch.setZip(zip);
        searchFilter.setAdvSearch(advSearch);
        sendBroadcast(searchFilter);
       // Log.e("veera",searchFilter.toString());


    }
    private void sendBroadcast(OpportunitiesSearchFilter searchFilter) {
        try {
            Intent intent;
            Bundle bundle = new Bundle();
            intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, searchFilter);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_SEARCH);
            bundle.putStringArrayList("repList",(ArrayList<String>)selectReturnRepList);
            bundle.putStringArrayList("probList",(ArrayList<String>) selectReturnProbList);
            intent.putExtras(bundle);
            // Broadcasting Selection Index to Receiver Activity
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
            // Closing Activity
            closeActivity(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }
    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void updateSearchKeyword(String keyword, int typeOfFilter) {

    }
    public void updateStageDetails(List<OppStageDetails> stageDetailsList) {
        MMProgressDialog.hideProgressDialog();
        if (stageDetailsList != null && !stageDetailsList.isEmpty()) {
            allStageDetails.addAll(stageDetailsList);
             showAllStageUnits();
        }
    }
    public void updateTypeDetails(List<OppTypeDetails> oppTypeDetails) {
        MMProgressDialog.hideProgressDialog();
        if(oppTypeDetails!=null && !oppTypeDetails.isEmpty()){
            allTypeDetails.addAll(oppTypeDetails);
            showAllTypeUnits();
        }
    }
    public void updateAllCreateRepsData(List<RepData> repsList) {
        MMProgressDialog.hideProgressDialog();
        // We are removing All Reps from Reps List as we are not displaying them in dropdown (similar to Web)
        for(int i=0;i<repsList.size();i++){
            if(repsList.get(i).getName().equals("------Disabled Reps------")){
                break;
            }else {
                allCreateRepsList.add(repsList.get(i));
                allCreateRepsList.get(i).setSelected(false);

            }
        }
        removeObjectFromRepsList(allCreateRepsList, "-1");
        // allRepsList = (ArrayList) repsList;
      //  resetValues(null);
    }
    public void updateAllAssignRepsData(List<AssignRepData> repDataList) {
        MMProgressDialog.hideProgressDialog();
        // We are removing All Reps from Reps List as we are not displaying them in dropdown (similar to Web)
        for(int i=0;i<repDataList.size();i++){
            if(repDataList.get(i).getName().equals("------Disabled Reps------")){
                break;
            }else {
                allAssignRepsList.add(repDataList.get(i));
                allAssignRepsList.get(i).setSelected(false);
                txt_assigned_rep.setText(selectRepName);
                if(selectRep_id.equals("IE="+loggedInRepId+"~")){
                    selectRepList= new ArrayList<>();
                    selectRepList.add(String.valueOf(loggedInRepId));
                }

                if(selectRepList!=null && !selectRepList.isEmpty()){

                    for(int j=0;j<selectRepList.size();j++){
                        if(selectRepList.get(j).equals(repDataList.get(i).getId())){
                            allAssignRepsList.get(i).setSelected(true);
                        }
                    }
                }
                sp_assigned_rep.setItems(allAssignRepsList,"Choose Rep",-1);
            }
        }
        removeObjectFromAssignRepsList(allAssignRepsList, "-1");
    }
    public void updateMasterData(List<MasterData> masterDataList, String fieldType) {
        switch (fieldType) {
            case Constants.GET_MASTER_DATA_BUSINESS_UNITS:
                MMProgressDialog.hideProgressDialog();
                for(int i=0;i<masterDataList.size();i++){
                    allBusinessUnits.add(masterDataList.get(i));
                    allBusinessUnits.get(i).setSelected(false);
                }
                showAllBusinessUnits();
                break;
            case Constants.GET_MASTER_DATA_PRODUCTS:
                MMProgressDialog.hideProgressDialog();
                for(int i=0;i<masterDataList.size();i++){
                    allProducts.add(masterDataList.get(i));
                    allProducts.get(i).setSelected(false);
                }
                showAllProducts();
                break;
        }
    }

    public void updateProducts(List<MasterData> masterDataList) {
        MMProgressDialog.hideProgressDialog();
        for(int i=0;i<masterDataList.size();i++){
            allProducts.add(masterDataList.get(i));
            allProducts.get(i).setSelected(false);
        }
        showAllProducts();
    }

    public void updateSourceList(List<CampaignSource> campaignSourceList) {
        MMProgressDialog.hideProgressDialog();
        if(campaignSourceList!=null && !campaignSourceList.isEmpty()){
            for(int i=1;i<campaignSourceList.size();i++){
                allSourceList.add(campaignSourceList.get(i));
            }
            showAllSources();
        }
    }
    public void updateLossReason(List<OppLossReasonDetails> lossReasonDetailsList) {
        MMProgressDialog.hideProgressDialog();
        if(lossReasonDetailsList!=null && !lossReasonDetailsList.isEmpty()){
            allLossReasonDetail.addAll(lossReasonDetailsList);
            showAllLossReason();
        }
    }
    public void updateOpportunityAddressList(List<OpportunityAddress> opportunityAddresses, String type) {
        MMProgressDialog.hideProgressDialog();
        if(opportunityAddresses!=null && !opportunityAddresses.isEmpty()){
            switch (type) {
                case "city":
                    for (int i = 0; i < opportunityAddresses.size(); i++) {
                        if(!opportunityAddresses.get(i).getName().equals("")){
                            allCityList.add(opportunityAddresses.get(i));
                        }
                    }
                    showAllCity();
                    break;
                case "county":
                    for (int i = 0; i < opportunityAddresses.size(); i++) {
                        if(!opportunityAddresses.get(i).getName().equals("")){
                            allCountyList.add(opportunityAddresses.get(i));
                        }
                    }
                    showAllCounty();
                    break;
                case "state":
                    allStateList.addAll(opportunityAddresses);
                    showAllState();
                    break;
                case "country":
                    for (int i = 0; i < opportunityAddresses.size(); i++) {
                        if(!opportunityAddresses.get(i).getName().equals("")){
                            allCountryList.add(opportunityAddresses.get(i));
                        }
                    }
                    showAllCountry();
                    break;
            }
        }
    }

    private final BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();
                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE)) {
                    assert bundle != null;
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
              /*      if (rightSideDrawerSelectionType == Constants.SelectionType.BUSINESS_UNIT) {
                        if (selectedObj != null) {
                            didBusinessUnitChanged((MasterData) selectedObj);
                        }
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PRODUCT) {
                        if (selectedObj != null) {
                           // didProductChanged((MasterData) selectedObj);
                        }
                    }*/
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void didBusinessUnitChanged(MasterData selectedBusinessUnit) {
        business_id="IE="+selectedBusinessUnit.getId()+"~";
        searchOrdersFilter.setBusinessUnit(selectedBusinessUnit.getName());
        searchOrdersFilter.setBusinessUnitId(selectedBusinessUnit.getId());
        txt_business.setText(searchOrdersFilter.getBusinessUnit());
        allProducts = new ArrayList<>();
        sp_product.setVisibility(View.GONE);
        txt_product.setVisibility(View.VISIBLE);
    }

    public  class MultiSelectAdopter extends RecyclerView.Adapter<MultiSelectAdopter.MultiSelectHolder> {
        private final Context context;
        private final List<CompanyData> stringList;
        private final String type;

        public MultiSelectAdopter(Context context, List<CompanyData> stringList, String type) {
            this.context = context;
            this.stringList = stringList;
            this.type = type;
        }

        @NonNull
        @Override
        public MultiSelectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
            View listItem= layoutInflater.inflate(R.layout.multiy_delete_row_layout, viewGroup, false);
            return new MultiSelectHolder(listItem);
        }
        @Override
        public void onBindViewHolder(@NonNull MultiSelectHolder holder, int position) {
            holder.alert_name.setText(stringList.get(position).getName());
            holder.alert_close.setOnClickListener(v -> {
                switch (type) {
                    case "opportunity":
                        opportunity_List.remove(position);
                        break;
                    case "Company":
                        companyDataList.remove(position);
                        break;
                    case "email":
                        emailList.remove(position);
                        break;
                    case "phone":
                        phone_list.remove(position);
                        break;
                    case "zip":
                        zip_list.remove(position);
                        break;
                }
               multiSelectAdopter.notifyDataSetChanged();
            });
        }

        @Override
        public int getItemCount() {
            return stringList.size();
        }

        public  class MultiSelectHolder extends RecyclerView.ViewHolder {
            TextView alert_name;
            ImageView alert_close;
            MultiSelectHolder(View itemView) {
                super(itemView);
                alert_name = itemView.findViewById(R.id.alert_name);
                alert_close = itemView.findViewById(R.id.alert_close);

            }
        }

    }

    }
