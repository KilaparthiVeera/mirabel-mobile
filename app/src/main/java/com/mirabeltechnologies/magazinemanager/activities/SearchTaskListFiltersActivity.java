package com.mirabeltechnologies.magazinemanager.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SearchTaskListFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SearchTaskListFiltersActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final String TAG = SearchTaskListFiltersActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom;
    private MMTextView search_tasks_filters_assigned_to, search_tasks_filters_assigned_by, search_tasks_filters_priority, search_tasks_filters_project;
    private Spinner search_tasks_filters_status_type_spinner;
    private MMButton search_tasks_filters_reset_button, search_tasks_filters_search_button;

    private ArrayList<String> statusTypesArrayList;
    private SearchTaskListFilter searchTaskListFilter;
    private CommonModel commonModel;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<MasterData> allPriorities = null, allProjects = null;
    private SpinnerAdapter statusTypesSpinnerAdapter;
    private boolean isLoggedInRepExistsInAllRepsList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_task_list);

        try {
            activityContext = SearchTaskListFiltersActivity.this;

            requestFrom = Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                searchTaskListFilter = (SearchTaskListFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_TASKS_FILTER_OBJECT);
            }

            // When we came from dashboard left menu, we are not sending search filter object so we don't have it.
            if (searchTaskListFilter == null)
                searchTaskListFilter = new SearchTaskListFilter();

            commonModel = new CommonModel(activityContext, this, requestFrom);

            statusTypesArrayList = new ArrayList<>();
            statusTypesArrayList.add("All Status");
            statusTypesArrayList.add("To Do");
            statusTypesArrayList.add("Completed");

            // Initializing UI References
            search_tasks_filters_assigned_to = (MMTextView) findViewById(R.id.search_tasks_filters_assigned_to);
            search_tasks_filters_assigned_by = (MMTextView) findViewById(R.id.search_tasks_filters_assigned_by);
            search_tasks_filters_priority = (MMTextView) findViewById(R.id.search_tasks_filters_priority);
            search_tasks_filters_project = (MMTextView) findViewById(R.id.search_tasks_filters_project);
            search_tasks_filters_status_type_spinner = (Spinner) findViewById(R.id.search_tasks_filters_status_type_spinner);
            search_tasks_filters_reset_button = (MMButton) findViewById(R.id.search_tasks_filters_reset_button);
            search_tasks_filters_search_button = (MMButton) findViewById(R.id.search_tasks_filters_search_button);

            statusTypesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, statusTypesArrayList);
            statusTypesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_tasks_filters_status_type_spinner.setAdapter(statusTypesSpinnerAdapter);
            search_tasks_filters_status_type_spinner.setSelection(0, false); // here animate is false in order to prevent execution of onItemSelected() on initial loading.
            search_tasks_filters_status_type_spinner.setOnItemSelectedListener(this);

            search_tasks_filters_assigned_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_TO);
                }
            });

            search_tasks_filters_assigned_by.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_BY);
                }
            });

            search_tasks_filters_priority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllPriorities(v);
                }
            });

            search_tasks_filters_project.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllProjects(v);
                }
            });

            search_tasks_filters_reset_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetSearchTasksFilters();
                }
            });

            search_tasks_filters_search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    applySearchTasksFilters();
                }
            });

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_SEARCH_TASKS_FILTERS_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;

        MMProgressDialog.hideProgressDialog();

        isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));

        if (!isLoggedInRepExistsInAllRepsList && searchTaskListFilter.getAssignedToRepId() == loggedInRepId) {
            searchTaskListFilter.setAssignedToRepId(-1);
            searchTaskListFilter.setAssignedToRepName("");
        }

        updateUIWithLatestValues(searchTaskListFilter);
    }

    public void showAllRepsList(View view, Constants.SelectionType selectionType) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            if (selectionType == Constants.SelectionType.ASSIGNED_TO)
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(searchTaskListFilter.getAssignedToRepId()));
            else
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(searchTaskListFilter.getAssignedByRepId()));

            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllPriorities(View view) {
        if (allPriorities == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_TASK_PRIORITIES);
        } else {
            Intent allPrioritiesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allPrioritiesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allPriorities);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.PRIORITY);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(searchTaskListFilter.getPriorityId()));
            allPrioritiesIntent.putExtras(bundle);
            startActivity(allPrioritiesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showAllProjects(View view) {
        if (allProjects == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_PROJECTS);
        } else {
            Intent allProjectsIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allProjectsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allProjects);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.PROJECT);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(searchTaskListFilter.getProjectId()));
            allProjectsIntent.putExtras(bundle);
            startActivity(allProjectsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        if (fieldType.equals(Constants.GET_MASTER_DATA_TASK_PRIORITIES)) {
            allPriorities = new ArrayList<>();
            allPriorities.add(new MasterData("All Priorities", "-1"));
            allPriorities.addAll(masterData);
            MMProgressDialog.hideProgressDialog();
            showAllPriorities(null);
        } else if (fieldType.equals(Constants.GET_MASTER_DATA_PROJECTS)) {
            allProjects = new ArrayList<>();
            allProjects.add(new MasterData("All Projects", "-1"));
            allProjects.addAll(masterData);
            MMProgressDialog.hideProgressDialog();
            showAllProjects(null);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_SEARCH_TASKS_FILTERS_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.PRIORITY) {
                        didPriorityChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PROJECT) {
                        didProjectChanged((MasterData) selectedObj);
                    } else {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.ASSIGNED_TO) {
            searchTaskListFilter.setAssignedToRepId(Integer.parseInt(selectedRep.getId()));
            searchTaskListFilter.setAssignedToRepName(selectedRep.getName());

            search_tasks_filters_assigned_to.setText(String.format("Assigned To: %s", searchTaskListFilter.getAssignedToRepName()));
        } else if (selectionType == Constants.SelectionType.ASSIGNED_BY) {
            searchTaskListFilter.setAssignedByRepId(Integer.parseInt(selectedRep.getId()));
            searchTaskListFilter.setAssignedByRepName(selectedRep.getName());

            search_tasks_filters_assigned_by.setText(String.format("Assigned By: %s", searchTaskListFilter.getAssignedByRepName()));
        }
    }

    public void didPriorityChanged(MasterData selectedPriority) {
        searchTaskListFilter.setPriority(selectedPriority.getName());
        searchTaskListFilter.setPriorityId(Integer.parseInt(selectedPriority.getId()));

        search_tasks_filters_priority.setText(String.format("Priority: %s", searchTaskListFilter.getPriority()));
    }

    public void didProjectChanged(MasterData selectedProject) {
        searchTaskListFilter.setProject(selectedProject.getName());
        searchTaskListFilter.setProjectId(Integer.parseInt(selectedProject.getId()));

        search_tasks_filters_project.setText(String.format("Project: %s", searchTaskListFilter.getProject()));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        searchTaskListFilter.setStatus(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);

        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    public void updateUIWithLatestValues(SearchTaskListFilter filter) {
        if (!filter.getAssignedToRepName().isEmpty())
            search_tasks_filters_assigned_to.setText(String.format("Assigned To: %s", filter.getAssignedToRepName()));
        else
            search_tasks_filters_assigned_to.setText("");

        if (!filter.getAssignedByRepName().isEmpty())
            search_tasks_filters_assigned_by.setText(String.format("Assigned By: %s", filter.getAssignedByRepName()));
        else
            search_tasks_filters_assigned_by.setText("");

        if (!filter.getPriority().isEmpty())
            search_tasks_filters_priority.setText(String.format("Priority: %s", filter.getPriority()));
        else
            search_tasks_filters_priority.setText("");

        if (!filter.getProject().isEmpty())
            search_tasks_filters_project.setText(String.format("Project: %s", filter.getProject()));
        else
            search_tasks_filters_project.setText("");

        setSpinnerSelectionWithoutCallingListener(search_tasks_filters_status_type_spinner, filter.getStatus());
    }

    public void resetSearchTasksFilters() {
        searchTaskListFilter = new SearchTaskListFilter();
        searchTaskListFilter.setStatus(0);

        if (isLoggedInRepExistsInAllRepsList) {
            searchTaskListFilter.setAssignedToRepId(loggedInRepId);
            searchTaskListFilter.setAssignedToRepName(loggedInRepName);
        } else {
            searchTaskListFilter.setAssignedToRepId(-1);
            searchTaskListFilter.setAssignedToRepName("");
        }

        updateUIWithLatestValues(searchTaskListFilter);
    }

    public void applySearchTasksFilters() {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_SEARCH_TASKS_FILTER_OBJECT, searchTaskListFilter);
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }
}
