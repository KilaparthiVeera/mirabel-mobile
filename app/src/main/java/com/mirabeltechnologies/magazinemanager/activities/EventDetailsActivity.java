package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.Notification;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;


import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.LBM_ACTION_REFRESH_VIEW;

public class EventDetailsActivity extends BaseActivity implements AlertDialogSelectionListener {

    MMTextView activity_list_from_name, date_scheduled, email, activity_list_phone_number, activity_list_companyname;
    MMEditText activity_list_message;
    ImageView activity_type_icon, mark_unmark_icon, delete_activity_icon;
    MMTextView type, tv_markdone, tv_delete, tv_title;
    LinearLayout li_from;
    Constants.RequestFrom requestFrom;
    ActivityData item = null;
    int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_details);
        li_from = findViewById(R.id.li_from);
        if (getIntent().getExtras().get(Constants.BUNDLE_ACTIVITY_TYPE).equals(Constants.RequestFrom.DASHBOARD_NOTIFICATIONS)) {
            Notification item = (Notification) getIntent().getExtras().getSerializable(EVENT_DATA);
            initialiseElementsFromDashboard(item);

             } else if (getIntent().getExtras().get(Constants.BUNDLE_ACTIVITY_TYPE).equals(Constants.RequestFrom.EVENT_ACTIVITIES)) {
            try {
                item = (ActivityData) Utility.deserializeObject(getIntent().getExtras().getByteArray(EVENT_DATA));
                position = getIntent().getExtras().getInt("Position");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            initialiseElementsFromNotifications(item);
        } else {
            try {
                item = (ActivityData) Utility.deserializeObject(getIntent().getExtras().getByteArray(EVENT_DATA));
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
          initialiseElementsFromNotifications(item);

        }


    }


    public void initialiseElementsFromDashboard(Notification item) {
        try {
            li_from.setVisibility(View.GONE);
            activity_list_companyname = findViewById(R.id.activity_list_companyname);
            //  okay = findViewById(R.id.okay);
            activity_list_phone_number = findViewById(R.id.activity_list_phone_number);
            type = findViewById(R.id.type);
            activity_type_icon = findViewById(R.id.activity_type_icon);
            activity_list_message = findViewById(R.id.activity_list_message);
            activity_list_from_name = findViewById(R.id.activity_list_from_name);
            date_scheduled = findViewById(R.id.activity_list_date_scheduled);
            email = findViewById(R.id.activity_list_email);
            tv_title = findViewById(R.id.tv_title);
            tv_title.setText("Notification Detail");
            mark_unmark_icon = findViewById(R.id.mark_unmark_icon);
            mark_unmark_icon.setVisibility(View.GONE);
            tv_markdone = findViewById(R.id.tv_markdone);
            tv_markdone.setVisibility(View.GONE);
            delete_activity_icon = findViewById(R.id.delete_activity_icon);
            delete_activity_icon.setVisibility(View.GONE);
            tv_delete = findViewById(R.id.tv_delete);
            tv_delete.setVisibility(View.GONE);

            activity_list_message.setText(item.getMessage());
            //  activity_list_from_name.setText(item.getFromName());
            date_scheduled.setText(item.getDateTime().concat(" ").concat(item.getTime()));
            switch (item.getType()) {
                case "Callback":
                    type.setText("Call");
                    break;
                case "Meeting":
                    type.setText("Meeting");

                    break;
                case "Task":
                    type.setText("Task");
                    break;
            }
            activity_list_phone_number.setText(item.getPhone());
            email.setText(item.getEmail());
            activity_list_companyname.setText(item.getFromName());


            switch (item.getType()) {
                case "Callback":
                    getEventDetails(item.getActivityOrTaskId(), 2);
                    break;
                case "Meeting":
                    getEventDetails(item.getActivityOrTaskId(), 3);

                    break;

            }


            switch (item.getType()) {
                case "Callback":
                    type.setText("Call");
                    break;
                case "Meeting":
                    type.setText("Meeting");

                    break;
                case "Task":
                    type.setText("Task");
                    break;
            }

        } catch (Exception ignored) {

        }
    }


    public void getEventDetails(String id, int type) {
        String requiredId = id.split("_")[1];
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("FilterOption", type);
            jsonObj.put("SelectedEmployeeID", String.valueOf(loggedInRepId));
            String whereQuery = String.format("gsActivitiesId=%s", requiredId);
            jsonObj.put("WhereQuery", whereQuery);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        activitiesModel = new ActivitiesModel(activityContext, this, requestFrom);
        activitiesModel.getDetailsByActivityID(encryptedClientKey, getDTTicks(), jsonObj);
        //  isLoading = true;
    }


    public void initialiseElementsFromNotifications(ActivityData item) {
        if (item != null) {
            try {
                activity_list_companyname = findViewById(R.id.activity_list_companyname);
                //  okay = findViewById(R.id.okay);
                activity_list_phone_number = findViewById(R.id.activity_list_phone_number);
                type = findViewById(R.id.type);
                tv_title = findViewById(R.id.tv_title);
                activity_type_icon = findViewById(R.id.activity_type_icon);
                activity_list_message = findViewById(R.id.activity_list_message);
                activity_list_from_name = findViewById(R.id.activity_list_from_name);
                date_scheduled = findViewById(R.id.activity_list_date_scheduled);
                email = findViewById(R.id.activity_list_email);
                tv_title = findViewById(R.id.tv_title);
                tv_title.setText("Activity Detail");
                mark_unmark_icon = findViewById(R.id.mark_unmark_icon);
                tv_markdone = findViewById(R.id.tv_markdone);
                delete_activity_icon = findViewById(R.id.delete_activity_icon);
                tv_delete = findViewById(R.id.tv_delete);
                activity_list_message.setText(item.getSpecialNotes());
                StringBuilder fromName = new StringBuilder(item.getFirstName());
                fromName.append(item.getLastName());
                activity_list_from_name.setText(fromName);
                if (item.getType().equals("Meeting")) {
                    date_scheduled.setText(item.getMeeting());
                } else {
                    date_scheduled.setText(item.getCallback());
                }
                type.setText(item.getType());
                activity_list_phone_number.setText(item.getPhone());
                email.setText(item.getEmail());
                activity_list_companyname.setText(item.getCompanyName());


                implMarkAsComplete(item);
                HandleCompanyNameView(item);


            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (getIntent().getExtras().get(Constants.BUNDLE_ACTIVITY_TYPE).equals(Constants.RequestFrom.EVENT_ACTIVITIES)) {
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
            finish();
        } else {
            Intent i = new Intent(this, DashboardActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
            finish();
        }
    }




    /* mark as complete starts here*/


    private ActivitiesModel activitiesModel;
    private Long currentActivityId;
    private String currentAction = "";


    public void implMarkAsComplete(final ActivityData item) {
        requestFrom = Constants.RequestFrom.EVENT_DETAILS_PAGE;
        activitiesModel = new ActivitiesModel(activityContext, this, requestFrom);
        DisplayIconBasedOnActivity(item);
        mark_unmark_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (item.getCompleted().equals("False"))
                    markActivity(item.getType(), item.getActivityId(), Constants.ACTIVITY_ACTION_MARK_COMPLETE);
                else
                    markActivity(item.getType(), item.getActivityId(), Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE);

            }
        });
        delete_activity_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteActivity(item.getType(), item.getActivityId(), Constants.ACTIVITY_ACTION_DELETE);

            }
        });
    }

    public void HandleCompanyNameView(final ActivityData data) {
        activity_list_companyname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // openContactDetailPage(data.getCustomerId(), data.getParentId());
            }
        });

    }

    public void openContactDetailPage(Long customerId, Long parentId) {
        Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_CUSTOMER_ID, String.valueOf(customerId));
        openContactDetailsIntent.putExtra(Constants.BUNDLE_IS_PRIMARY_CONTACT, parentId <= 0);
        startActivity(openContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void DisplayIconBasedOnActivity(ActivityData item) {

        switch (item.getType()) {
            case "Note":
                activity_type_icon.setImageResource(R.drawable.activity_list_notes);

                mark_unmark_icon.setVisibility(View.INVISIBLE);
                tv_markdone.setVisibility(View.INVISIBLE);

                if (item.getCanDeleteNotes() == 0) { // user account level access
                    delete_activity_icon.setVisibility(View.INVISIBLE);
                    tv_delete.setVisibility(View.INVISIBLE);
                } else {
                    if (item.getCanDelete() == 0) {// additional rep security configuration
                        delete_activity_icon.setVisibility(View.VISIBLE);
                        tv_delete.setVisibility(View.VISIBLE);
                    } else {
                        delete_activity_icon.setVisibility(View.INVISIBLE);
                        tv_delete.setVisibility(View.INVISIBLE);
                    }
                }
                break;
            case "Call":
                activity_type_icon.setImageResource(R.drawable.activity_list_call);

                if (item.getCanEditNotes() == 0) { // user account level access
                    mark_unmark_icon.setVisibility(View.INVISIBLE);
                    tv_markdone.setVisibility(View.INVISIBLE);
                } else {
                    if (item.getCanEdit() == 0) { // additional rep security configuration
                        if (item.getCompleted().equals("False")) {
                            mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                            mark_unmark_icon.setVisibility(View.VISIBLE);
                            tv_markdone.setVisibility(View.VISIBLE);
                        } else {
                            mark_unmark_icon.setVisibility(View.INVISIBLE);
                            tv_markdone.setVisibility(View.INVISIBLE);
                        }
                    } else {
                        mark_unmark_icon.setVisibility(View.INVISIBLE);
                        tv_markdone.setVisibility(View.INVISIBLE);
                    }
                }

                if (item.getCanDeleteNotes() == 0 || item.getCanDelete() == 1 || item.getCompleted().equals("False")) {
                    delete_activity_icon.setVisibility(View.GONE);
                    tv_delete.setVisibility(View.GONE);// to align edit icon as vertically center we used Visibility GONE
                } else {
                    delete_activity_icon.setVisibility(View.VISIBLE);
                    tv_delete.setVisibility(View.VISIBLE);
                }
                break;
            case "Meeting":
                activity_type_icon.setImageResource(R.drawable.activity_list_meetings);

                if (item.getCanEditNotes() == 0) { // user account level access
                    mark_unmark_icon.setVisibility(View.INVISIBLE);
                    tv_markdone.setVisibility(View.INVISIBLE);
                } else {
                    if (item.getCanEdit() == 0) { // additional rep security configuration
                        if (item.getCompleted().equals("False")) {
                            mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                        } else {
                            tv_markdone.setText("Mark Undone");
                            mark_unmark_icon.setImageResource(R.drawable.mark_uncompleted);
                        }

                        mark_unmark_icon.setVisibility(View.VISIBLE);
                        tv_markdone.setVisibility(View.VISIBLE);
                    } else {
                        mark_unmark_icon.setVisibility(View.INVISIBLE);
                        tv_markdone.setVisibility(View.INVISIBLE);
                    }
                }

                if (item.getCanDeleteNotes() == 0 || item.getCanDelete() == 1 || item.getCompleted().equals("False")) {
                    delete_activity_icon.setVisibility(View.GONE); // to align edit icon as vertically center we used Visibility GONE
                    tv_delete.setVisibility(View.GONE);
                } else {
                    delete_activity_icon.setVisibility(View.VISIBLE);
                    tv_delete.setVisibility(View.VISIBLE);
                }
                break;
            case "Email":
                activity_type_icon.setImageResource(R.drawable.activity_list_email);

                mark_unmark_icon.setVisibility(View.INVISIBLE);
                tv_markdone.setVisibility(View.INVISIBLE);

                if (item.getCanDeleteNotes() == 0) { // user account level access
                    delete_activity_icon.setVisibility(View.INVISIBLE);
                    tv_delete.setVisibility(View.INVISIBLE);
                } else {
                    if (item.getCanDelete() == 0) {// additional rep security configuration
                        delete_activity_icon.setVisibility(View.VISIBLE);
                        tv_delete.setVisibility(View.VISIBLE);
                    } else {
                        delete_activity_icon.setVisibility(View.INVISIBLE);
                        tv_delete.setVisibility(View.INVISIBLE);

                    }
                }
                break;
            case "Mass Email":
                activity_type_icon.setImageResource(R.drawable.activity_list_mass_email);

                mark_unmark_icon.setVisibility(View.INVISIBLE);
                tv_markdone.setVisibility(View.INVISIBLE);

                if (item.getCanDeleteNotes() == 0) { // user account level access
                    delete_activity_icon.setVisibility(View.INVISIBLE);
                    tv_delete.setVisibility(View.INVISIBLE);
                } else {
                    if (item.getCanDelete() == 0) {// additional rep security configuration
                        delete_activity_icon.setVisibility(View.VISIBLE);
                        tv_delete.setVisibility(View.VISIBLE);
                    } else {
                        delete_activity_icon.setVisibility(View.INVISIBLE);
                        tv_delete.setVisibility(View.INVISIBLE);
                    }
                }
                break;
        }

    }


    public void updateData(ActivityData searchActivitiesResultsList, int canDeleteNotes, int canEditNotes) {


        StringBuilder fromName = new StringBuilder(searchActivitiesResultsList.getFirstName());
        fromName.append(searchActivitiesResultsList.getLastName());
        activity_list_from_name.setText(fromName);


        searchActivitiesResultsList.setCanDeleteNotes(canDeleteNotes);
        searchActivitiesResultsList.setCanEditNotes(canEditNotes);

        li_from.setVisibility(View.VISIBLE);
        mark_unmark_icon.setVisibility(View.VISIBLE);
        tv_markdone.setVisibility(View.VISIBLE);
        delete_activity_icon.setVisibility(View.VISIBLE);
        tv_delete.setVisibility(View.VISIBLE);

        implMarkAsComplete(searchActivitiesResultsList);
        HandleCompanyNameView(searchActivitiesResultsList);
    }


    public void markActivity(String activityType, Long activityId, String action) {

        currentActivityId = activityId;
        currentAction = action;

        if (activityType.equals("Call") && action.equals(Constants.ACTIVITY_ACTION_MARK_COMPLETE)) {
            MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_MARK_COMPLETE_ACTIVITY, getResources().getString(R.string.title_alert), getResources().getString(R.string.complete_activity), "Yes", "No");
        } else {
            modifyActivity(activityId, action);
        }
    }

    public void deleteActivity(String activityType, Long activityId, String action) {
        currentActivityId = activityId;
        currentAction = action;
        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_ACTIVITY, getResources().getString(R.string.title_alert), getResources().getString(R.string.delete_activity), "Yes", "No");
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (buttonType == Constants.ButtonType.POSITIVE) {
            if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_ACTIVITY) {
                modifyActivity(currentActivityId, currentAction);
            } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_MARK_COMPLETE_ACTIVITY) {
                modifyActivity(currentActivityId, currentAction);
            }
        }
    }


    public void modifyActivity(Long activityId, String action) {
        MMProgressDialog.showProgressDialog(activityContext);
        activitiesModel.modifyActivity(encryptedClientKey, getDTTicks(), activityId, action);
    }

    public void updateActivitiesAfterModify(String action) {
        if (action.equals(Constants.ACTIVITY_ACTION_DELETE)) {
            // On activity delete, we will remove activities in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like activity add / delete.
            finish();
        } else {
            if (action.equals(Constants.ACTIVITY_ACTION_MARK_COMPLETE)) {
                delete_activity_icon.setVisibility(View.VISIBLE);
                tv_delete.setVisibility(View.VISIBLE);
                mark_unmark_icon.setVisibility(View.GONE);
                tv_markdone.setVisibility(View.GONE);
            } else if (action.equals(Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE)) {
                delete_activity_icon.setVisibility(View.GONE);
                tv_delete.setVisibility(View.GONE);
                mark_unmark_icon.setVisibility(View.VISIBLE);
                tv_markdone.setVisibility(View.VISIBLE);
                //if it is meeting , it will have uncheck meeting status
                // Also , check update SearchActivitiesList if activity is deleted or modified wtc.
                if (item.getType().equals("Meeting")) {
                    tv_markdone.setText("Mark as complete");
                    mark_unmark_icon.setImageResource(R.drawable.mark_completed);
                }
            }
            MMProgressDialog.hideProgressDialog();


        }
        //add the operation in shared prefs, so that whom ever want to handle this event in some other screen, will use this.
        Intent broadcast = new Intent(LBM_ACTION_REFRESH_VIEW);
        broadcast.putExtra("ActionType", action);
        broadcast.putExtra("Position", position);
        LocalBroadcastManager.getInstance(activityContext).sendBroadcast(broadcast);
    }
}
