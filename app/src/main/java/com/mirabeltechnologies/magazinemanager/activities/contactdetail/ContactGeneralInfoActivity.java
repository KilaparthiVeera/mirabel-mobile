package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.util.Utility;

public class ContactGeneralInfoActivity extends BaseActivity implements AlertDialogSelectionListener {
    private MMTextView cgi_category, cgi_priority, cgi_contact_type, cgi_job_title, cgi_alternate_email, cgi_mobile_phone, cgi_partner_reps;
    private ImageView cgi_alternate_email_id_icon, cgi_mobile_phone_icon;
    private RelativeLayout cgi_alternate_email_layout, cgi_mobile_phone_layout;
    private DetailContact selectedContactDetails = null;
    private boolean isNotesSettingPrivate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_general_info);

        try {
            activityContext = ContactGeneralInfoActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
                isNotesSettingPrivate = bundle.getBoolean(Constants.BUNDLE_IS_NOTES_SETTING_PRIVATE);
            }

            cgi_category = (MMTextView) findViewById(R.id.cgi_category);
            cgi_priority = (MMTextView) findViewById(R.id.cgi_priority);
            cgi_contact_type = (MMTextView) findViewById(R.id.cgi_contact_type);
            cgi_job_title = (MMTextView) findViewById(R.id.cgi_job_title);
            cgi_alternate_email_layout = (RelativeLayout) findViewById(R.id.cgi_alternate_email_layout);
            cgi_alternate_email = (MMTextView) findViewById(R.id.cgi_alternate_email);
            cgi_alternate_email_id_icon = (ImageView) findViewById(R.id.cgi_alternate_email_id_icon);
            cgi_mobile_phone_layout = (RelativeLayout) findViewById(R.id.cgi_mobile_phone_layout);
            cgi_mobile_phone = (MMTextView) findViewById(R.id.cgi_mobile_phone);
            cgi_mobile_phone_icon = (ImageView) findViewById(R.id.cgi_mobile_phone_icon);
            cgi_partner_reps = (MMTextView) findViewById(R.id.cgi_partner_reps);

            updateUIWithBundleValues();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void updateUIWithBundleValues() {
        if (selectedContactDetails != null) {
            cgi_category.setText(selectedContactDetails.getCustomerType());
            cgi_priority.setText(selectedContactDetails.getPriorityName());
            cgi_contact_type.setText(selectedContactDetails.getJobDescription());
            cgi_job_title.setText(selectedContactDetails.getTitle());
            cgi_alternate_email.setText(selectedContactDetails.getEmail2());
            cgi_mobile_phone.setText(selectedContactDetails.getCellPhone());

            if (selectedContactDetails.getEmail2().length() > 0) {
                cgi_alternate_email_id_icon.setVisibility(View.VISIBLE);

                cgi_alternate_email_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        chooseEmailClient();
                    }
                });
            }

            if (selectedContactDetails.getCellPhone().length() > 0) {
                cgi_mobile_phone_icon.setVisibility(View.VISIBLE);

                cgi_mobile_phone_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        takeConfirmationToMakePhoneCall(selectedContactDetails.getCellPhone());
                    }
                });
            }

            if (selectedContactDetails.getPartnerReps().length() > 0) {
                String[] partnerReps = selectedContactDetails.getPartnerReps().split(" ,");

                String partnerRepsStr = "";
                for (String repName : partnerReps)
                    partnerRepsStr += repName + "\n";
                partnerRepsStr = partnerRepsStr.substring(1, partnerRepsStr.length() - 1); // we are removing , (comma symbol) at first position & \n at end position by sub string result.

                cgi_partner_reps.setText(partnerRepsStr);
            }
        }
    }

    public void takeConfirmationToMakePhoneCall(String phoneNumber) {
        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                checkPermissionsToMakePhoneCall();
            }
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE);
        }
    }

    public void checkPermissionsToMakePhoneCall() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
            makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_GENERAL_INFO_PAGE, selectedContactDetails.getCustomersId(), selectedContactDetails.getCellPhone(), "", isNotesSettingPrivate);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MAKE_PHONE_CALL_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsToMakePhoneCall();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.phone_call_permission_denied));
                    }
                }
                break;
        }
    }

    public void chooseEmailClient() {
        openEmailComposer(this, Constants.RequestFrom.CONTACT_DETAIL_GENERAL_INFO_PAGE, selectedContactDetails.getCustomersId(), selectedContactDetails.getEmail2(), isNotesSettingPrivate);
    }
}
