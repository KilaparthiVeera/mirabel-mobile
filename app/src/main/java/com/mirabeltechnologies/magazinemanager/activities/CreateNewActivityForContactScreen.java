package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TimePicker;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.EmployeeData;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.NewActivity;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class CreateNewActivityForContactScreen extends BaseActivity implements AlertDialogSelectionListener {
    public static final String TAG = CreateNewActivityForContactScreen.class.getSimpleName();

    private LinearLayout create_activity_date_time_layout;
    private MMTextView create_activity_for_contact_screen_title, create_activity_activity_type, create_activity_date, create_activity_time, create_activity_assigned_by, create_activity_assigned_to, create_activity_notes_title;
    private MMEditText create_activity_notes;
    private Switch create_activity_private_switch, create_activity_add_to_task_list_switch;
    private MMButton create_activity_cancel_button, create_activity_add_button;

    private String customerId, currentActivityType, actionType;
    private Constants.RequestFrom requestFrom;
    private CommonModel commonModel;
    private ActivitiesModel activitiesModel;
    private NewActivity newActivityBean;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<MasterData> allActivityTypes = null;
    private EmployeeData employeeData = null;
    private String currentDate, currentTime, selectedDate = "";
    private int selectedHourOfDay = -1, selectedMinutes = -1;
    private long contactRepId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_activity_for_contact_screen);

        try {
            activityContext = CreateNewActivityForContactScreen.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                customerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
                contactRepId = bundle.getLong(Constants.BUNDLE_CONTACT_REP_ID);
                currentActivityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
            }

            newActivityBean = new NewActivity(customerId);

            requestFrom = Constants.RequestFrom.CREATE_ACTIVITY_PAGE;
            commonModel = new CommonModel(activityContext, this, requestFrom);
            activitiesModel = new ActivitiesModel(activityContext, this, requestFrom);

            // Initializing UI References
            create_activity_for_contact_screen_title = findViewById(R.id.create_activity_for_contact_screen_title);
            create_activity_activity_type = findViewById(R.id.create_activity_activity_type);
            create_activity_date_time_layout = findViewById(R.id.create_activity_date_time_layout);
            create_activity_date = findViewById(R.id.create_activity_date);
            create_activity_time = findViewById(R.id.create_activity_time);
            create_activity_assigned_by = findViewById(R.id.create_activity_assigned_by);
            create_activity_assigned_to = findViewById(R.id.create_activity_assigned_to);
            create_activity_notes_title = findViewById(R.id.create_activity_notes_title);
            create_activity_notes = findViewById(R.id.create_activity_notes);
            create_activity_private_switch = findViewById(R.id.create_activity_private_switch);
            create_activity_add_to_task_list_switch = findViewById(R.id.create_activity_add_to_task_list_switch);
            create_activity_cancel_button = findViewById(R.id.create_activity_cancel_button);
            create_activity_add_button = findViewById(R.id.create_activity_add_button);

            create_activity_activity_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllActivityTypes(v);
                }
            });

            create_activity_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker(v);
                }
            });

            create_activity_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showTimePicker(v);
                }
            });

            create_activity_assigned_by.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_BY);
                }
            });

            create_activity_assigned_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_TO);
                }
            });

            create_activity_private_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    newActivityBean.setPrivate(isChecked);
                }
            });

            create_activity_add_to_task_list_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    newActivityBean.setAddToTaskList(isChecked);
                }
            });

            create_activity_cancel_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cancelledCreatingActivityForContact(v);
                }
            });

            create_activity_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createNewActivityForContactByValidateFields(v);
                }
            });

            if (currentActivityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                actionType = "note";
                create_activity_for_contact_screen_title.setText("Add Note");
                create_activity_notes_title.setText("* Notes");
                create_activity_notes.setHint("Notes");
                create_activity_add_button.setText("Add Note");
                create_activity_date_time_layout.setVisibility(View.GONE);
                create_activity_assigned_by.setEnabled(false);
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                actionType = "call";
                create_activity_for_contact_screen_title.setText("Add Call");
                create_activity_notes_title.setText("Call Notes");
                create_activity_notes.setHint("Call Notes");
                create_activity_add_to_task_list_switch.setEnabled(false);
                create_activity_add_button.setText("Add Call");
                create_activity_date_time_layout.setVisibility(View.VISIBLE);

                prePopulateDateTimeFields();
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                actionType = "meeting";
                create_activity_for_contact_screen_title.setText("Add Meeting");
                create_activity_notes_title.setText("Meeting Notes");
                create_activity_notes.setHint("Meeting Notes");
                create_activity_add_button.setText("Add Meeting");
                create_activity_date_time_layout.setVisibility(View.VISIBLE);

                prePopulateDateTimeFields();
            }

            // Setting default values
            newActivityBean.setActionType(actionType);
            newActivityBean.setAssignedByRepId(String.valueOf(loggedInRepId));
            newActivityBean.setAssignedByRepName(loggedInRepName);
            newActivityBean.setAssignedToRepId(String.valueOf(loggedInRepId));
            newActivityBean.setAssignedToRepName(loggedInRepName);

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_CREATE_ACTIVITY_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void prePopulateDateTimeFields() {
        Calendar calendar = Calendar.getInstance();

        currentDate = Utility.formatDate(calendar.getTime(), Constants.SERVER_DATE_FORMAT);

        // Rounding time to nearest 30 minutes.
        int mMinute = calendar.get(Calendar.MINUTE);
        if (mMinute < 30) {
            calendar.set(Calendar.MINUTE, 30);
        } else {
            calendar.add(Calendar.HOUR, 1);
            calendar.set(Calendar.MINUTE, 0);
        }

        currentTime = String.format("%tI:%tM %tp", calendar, calendar, calendar);
        currentTime = currentTime.toUpperCase().replace(".", ""); // in order to convert a.m. to AM

        selectedHourOfDay = calendar.get(Calendar.HOUR_OF_DAY);
        selectedMinutes = calendar.get(Calendar.MINUTE);

        if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
            create_activity_date.setText("");
            create_activity_time.setText(currentTime);

            newActivityBean.setDate("");
            newActivityBean.setTime(currentTime);
        } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
            create_activity_date.setText(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));
            create_activity_time.setText(currentTime);

            newActivityBean.setDate(currentDate);
            newActivityBean.setTime(currentTime);
        }
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_EMPLOYEE);
    }

    public void updateEmployeeData(EmployeeData currentEmployeeData) {
        employeeData = currentEmployeeData;

        boolean showPrivate = false, checkPrivate = false;

        switch (employeeData.getNoteSettings()) {
            case 0: // All notes are Public by default but user can change to Private
                showPrivate = true;
                checkPrivate = false;
                newActivityBean.setPrivate(false);
                break;
            case 1: // All notes are Private by default but user can change to Public
                showPrivate = true;
                checkPrivate = true;
                newActivityBean.setPrivate(true);
                break;
            case 2: // All notes are Public by default
                showPrivate = false;
                checkPrivate = false;
                newActivityBean.setPrivate(false);
                break;
            case 3: // All notes are Private by default
                showPrivate = false;
                checkPrivate = true;
                newActivityBean.setPrivate(true);
                break;
        }

        create_activity_private_switch.setChecked(checkPrivate);

        if (showPrivate)
            create_activity_private_switch.setVisibility(View.VISIBLE);
        else
            create_activity_private_switch.setVisibility(View.GONE);

        commonModel.getAllRepsData(encryptedClientKey, getDTTicks(), true, contactRepId);
    }

    public void updateAllRepsData(List<RepData> repsList, int referenceId) {
        allRepsList = (ArrayList) repsList;

        // We are removing All Reps and Disabled Reps from Reps List as we are not displaying them in dropdown (similar to Web)
        removeObjectFromRepsList(allRepsList, "-1");
        //removeObjectFromRepsList(allRepsList, "-2");

        // If referenceId is 1 then we are adding currently logged in user to reps list as it is not coming in reps list from service.
        if (referenceId == 1) {
            allRepsList.add(new RepData(loggedInRepName, String.valueOf(loggedInRepId)));
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void showAllActivityTypes(View view) {
        if (allActivityTypes == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_ACTIVITY_TYPES);
        } else {
            Intent allActivityTypesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allActivityTypesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allActivityTypes);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.ACTIVITY_TYPE);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newActivityBean.getActivityTypeId());
            allActivityTypesIntent.putExtras(bundle);
            startActivity(allActivityTypesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        if (fieldType.equals(Constants.GET_MASTER_DATA_ACTIVITY_TYPES)) {
            allActivityTypes = (ArrayList) masterData;
            MMProgressDialog.hideProgressDialog();
            showAllActivityTypes(null);
        }
    }

    public void showAllRepsList(View view, Constants.SelectionType selectionType) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent allRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            if (selectionType == Constants.SelectionType.ASSIGNED_BY)
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newActivityBean.getAssignedByRepId());
            else
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newActivityBean.getAssignedToRepId());

            allRepsListIntent.putExtras(bundle);
            startActivity(allRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    // Local Broadcast Manager Receiver Handler
    private final BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_CREATE_ACTIVITY_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.ACTIVITY_TYPE) {
                        didActivityTypeChanged((MasterData) selectedObj);
                    } else {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.ASSIGNED_BY) {
            newActivityBean.setAssignedByRepId(selectedRep.getId());
            newActivityBean.setAssignedByRepName(selectedRep.getName());

            create_activity_assigned_by.setText(newActivityBean.getAssignedByRepName());

        } else if (selectionType == Constants.SelectionType.ASSIGNED_TO) {
            newActivityBean.setAssignedToRepId(selectedRep.getId());
            newActivityBean.setAssignedToRepName(selectedRep.getName());

            create_activity_assigned_to.setText(newActivityBean.getAssignedToRepName());
        }
    }

    public void didActivityTypeChanged(MasterData selectedCategory) {
        newActivityBean.setActivityType(selectedCategory.getName());

        if (selectedCategory.getId() == null)
            newActivityBean.setActivityTypeId("-1");
        else
            newActivityBean.setActivityTypeId(selectedCategory.getId());

        create_activity_activity_type.setText(newActivityBean.getActivityType());
    }

    public void showDatePicker(View view) {
        try {
            selectedDate = newActivityBean.getDate();

            if (selectedDate.isEmpty()) {
                selectedDate = currentDate;
            }

            String[] currentDateValues = selectedDate.split("-");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, monthOfYear, dayOfMonth);

                    create_activity_date.setText(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));
                    selectedDate = Utility.formatDate(calendar.getTime(), Constants.SERVER_DATE_FORMAT);
                    newActivityBean.setDate(selectedDate);

                    if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                        create_activity_add_to_task_list_switch.setEnabled(true);
                    }
                }
            }, Integer.parseInt(currentDateValues[0]), Integer.parseInt(currentDateValues[1]) - 1, Integer.parseInt(currentDateValues[2]));

            //datePickerDialog.setTitle("Select Date");
            //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showTimePicker(View view) {
        try {
            Calendar c = Calendar.getInstance();

            if (selectedHourOfDay == -1)
                selectedHourOfDay = c.get(Calendar.HOUR_OF_DAY);

            if (selectedMinutes == -1)
                selectedMinutes = c.get(Calendar.MINUTE);

            TimePickerDialog timePickerDialog = new TimePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    selectedHourOfDay = hourOfDay;
                    selectedMinutes = minute;

                    String amPm = hourOfDay > 11 ? "PM" : "AM";

                    //Make the 24 hour time format to 12 hour time format
                    if (hourOfDay > 12)
                        hourOfDay = hourOfDay - 12;

                    String selectedTime = String.format("%02d:%02d %s", hourOfDay, minute, amPm);
                    create_activity_time.setText(selectedTime);
                    newActivityBean.setTime(selectedTime);

                }
            }, selectedHourOfDay, selectedMinutes, false);

            //timePickerDialog.setTitle("Select Time");
            timePickerDialog.setCancelable(false);
            timePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelledCreatingActivityForContact(View view) {
        onBackPressed();
    }

    public void createNewActivityForContactByValidateFields(View view) {
        try {
            newActivityBean.setNotes(create_activity_notes.getText().toString().trim());

            if (newActivityBean.getNotes().isEmpty()) {
                if (currentActivityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                    MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Please enter a note.");
                    return;
                } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                    newActivityBean.setNotes("Call");
                } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                    newActivityBean.setNotes("Meeting");
                }
            }

            MMProgressDialog.showProgressDialog(activityContext);
            activitiesModel.createNewActivity(encryptedClientKey, getDTTicks(), newActivityBean);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateActivityCreateStatus() {
        MMProgressDialog.hideProgressDialog();

        try {
            String title = "", message = "";

            if (currentActivityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                title = "Add Notes Status";
                message = "Note added successfully.";
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                title = "Add Call Status";
                message = "Call added successfully.";
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                title = "Add Meeting Status";
                message = "Meeting added successfully.";
            }

            // Broadcasting Intent to Receiver Activity
            Intent refreshActivitiesBroadcast = new Intent(Constants.LBM_ACTION_REFRESH_ACTIVITIES);
            LocalBroadcastManager.getInstance(activityContext).sendBroadcast(refreshActivitiesBroadcast);

            MMAlertDialog.listener = this;
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_ACTIVITY_ADDED);
            MMAlertDialog.showAlertDialogWithCallback(activityContext, title, message, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_NEW_ACTIVITY_ADDED) {
            onBackPressed();
        }
    }
}
