package com.mirabeltechnologies.magazinemanager.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.MyLast100ContactsAdapter;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;
import com.mirabeltechnologies.magazinemanager.models.DashboardModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.List;

public class MyLast100ContactsActivity extends ListViewBaseActivity implements PhoneCallListener, AlertDialogSelectionListener {
    public static final String TAG = MyLast100ContactsActivity.class.getSimpleName();

    private DashboardModel dashboardModel;
    private MyLast100ContactsAdapter myLast100ContactsAdapter;
    private String customerId, phoneNumber, extension;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_last_100_contacts);

        try {
            activityContext = MyLast100ContactsActivity.this;

            // initializing super class variables
            currentActivity = this;
            requestFrom = Constants.RequestFrom.MY_LAST_100_CONTACTS_PAGE;

            dashboardModel = new DashboardModel(activityContext, this, requestFrom);

            list_view =  findViewById(R.id.recent_contacts_list_view);
            myLast100ContactsAdapter = new MyLast100ContactsAdapter(activityContext, this);
            list_view.setAdapter(myLast100ContactsAdapter);

            list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    openContactDetailPage(globalContent.getMyLast100ContactsList().get(position), position);
                }
            });

            page_number_display = (MMTextView) findViewById(R.id.recent_contacts_page_number_display);
            left_nav_arrow = (ImageView) findViewById(R.id.recent_contacts_left_nav_arrow);
            right_nav_arrow = (ImageView) findViewById(R.id.recent_contacts_right_nav_arrow);

            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshDataBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));

            resetValues(true);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshDataBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void resetValues(boolean sendRequest) {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        globalContent.setMyLast100ContactsList(null);
        myLast100ContactsAdapter.notifyDataSetChanged();

        if (sendRequest)
            getMyLast100Contacts(currentPageNumber);
    }

    public void getMyLast100Contacts(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            dashboardModel.getRecentContacts(encryptedClientKey, getDTTicks(), false, currentPageNumber);
            isLoading = true;
        }
    }

    public void updateMyLast100ContactsList(final List<Contact> contactsList, int noOfContacts) {
        if (currentPageNumber == 1 && (contactsList == null || contactsList.isEmpty())) {
            resetValues(false);
            noRecordsFound();
        } else {
            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + contactsList.size();

            if (noOfContacts != totalNoOfRecords) {
                totalNoOfRecords = noOfContacts;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getMyLast100Contacts(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setMyLast100ContactsList(contactsList);
            myLast100ContactsAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    public void openContactDetailPage(Contact contact, int contactIndex) {
        if (contact.getCanViewEmployeeId().equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            // We are saving selected contact index in order to reload contact specific page after editing of that contact.
            sharedPreferences.putInt(Constants.SP_MY_LAST_100_CONTACTS_SELECTED_CONTACT_INDEX, contactIndex);

            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, contact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRefreshDataBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    String currentContactOperation = bundle.getString(Constants.BUNDLE_CURRENT_OPERATION);

                    if (currentContactOperation.equals(Constants.CURRENT_OPERATION_EDIT_CONTACT)) {
                        /*
                         On contact edit, we will remove all cached my last 100 contacts & we will reload them in order to maintain same displaying order of contacts similar to web
                         and on new contact add (primary / sub contact), we are not refreshing my last 100 contacts data
                          */
                        resetValues(true);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void takeConfirmationToMakePhoneCall(String customerId, String phoneNumber, String extension) {
        this.customerId = customerId;
        this.phoneNumber = phoneNumber;
        this.extension = extension;

        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                checkPermissionsToMakePhoneCall();
            }
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MAKE_PHONE_CALL_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsToMakePhoneCall();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.phone_call_permission_denied));
                    }
                }
                break;
        }
    }

    public void checkPermissionsToMakePhoneCall() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
            makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, phoneNumber, extension, false);
        }
    }
}
