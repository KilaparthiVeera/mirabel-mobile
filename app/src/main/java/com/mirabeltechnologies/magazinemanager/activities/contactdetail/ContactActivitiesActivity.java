package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateNewActivityForContactScreen;
import com.mirabeltechnologies.magazinemanager.adapters.ContactActivitiesAdapter;
import com.mirabeltechnologies.magazinemanager.beans.ActivityDetails;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.CustomDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactActivitiesListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.models.TaskListModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;

public class ContactActivitiesActivity extends BaseActivity implements ViewMoreClickListener, ContactActivitiesListener, AlertDialogSelectionListener {
    public static final String TAG = ContactActivitiesActivity.class.getSimpleName();

    private MMTextView current_activity_title;
    private ImageView create_activity_icon;
    private ListView contact_activities_list_view;
    private ContactActivitiesAdapter activitiesAdapter;
    private ContactsModel contactsModel;
    private TaskListModel taskListModel;
    private DetailContact selectedContactDetails = null;
    private String selectedCustomerId, activityType;
    private List<ActivityDetails> customerAllActivitiesList = new ArrayList<>();
    private Long currentTaskId;
    private String currentAction = "";
    private int canViewEmployeeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_activities);

        try {
            activityContext = ContactActivitiesActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
                selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
                activityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
            }

            contactsModel = new ContactsModel(activityContext, this, Constants.RequestFrom.CONTACT_DETAIL_PAGE);
            taskListModel = new TaskListModel(activityContext, this, Constants.RequestFrom.CONTACT_DETAIL_PAGE);

            current_activity_title = (MMTextView) findViewById(R.id.current_activity_title);
            create_activity_icon = (ImageView) findViewById(R.id.create_activity_icon);

            current_activity_title.setText(activityType);

            if (selectedContactDetails != null)
                canViewEmployeeId = selectedContactDetails.getCanViewEmployeeId();

            if (activityType.equals(Constants.ACTIVITY_TYPE_TASKS) || activityType.equals(Constants.ACTIVITY_TYPE_EMAILS) || String.valueOf(canViewEmployeeId).equals(Constants.CONTACT_READ_ONLY_ACCESS))
                create_activity_icon.setVisibility(View.GONE);

            contact_activities_list_view = (ListView) findViewById(R.id.contact_activities_list_view);
            activitiesAdapter = new ContactActivitiesAdapter(activityContext, activityType, this, this, canViewEmployeeId);
            contact_activities_list_view.setAdapter(activitiesAdapter);

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshActivitiesBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_ACTIVITIES));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshActivitiesBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer() {
        Log.e("veera",selectedSubDomain);
        MMProgressDialog.showProgressDialog(activityContext);
        contactsModel.getCustomerActivities(encryptedClientKey, getDTTicks(), selectedCustomerId, activityType.equals(Constants.ACTIVITY_TYPE_CALLS) ? "Callbacks" : activityType);
    }

    public void updateCustomerActivities(final List<ActivityDetails> activitiesList) {
        MMProgressDialog.hideProgressDialog();

        if (activitiesList == null || activitiesList.isEmpty()) {
            activitiesAdapter.setActivitiesList(customerAllActivitiesList);
            activitiesAdapter.notifyDataSetChanged();

            contact_activities_list_view.addHeaderView(no_records_found_text_view);
        } else {
            // Removing no records found message if already added to list view.
            if (contact_activities_list_view.getHeaderViewsCount() > 0) {
                contact_activities_list_view.removeHeaderView(no_records_found_text_view);
            }
            String outputPattern = "yyyy-MM-dd hh:mm:ss a";
            String inputPattern  = "MM/dd/yyyy hh:mm:ss a";
            SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
            SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);
            Date sdate;
            try {
                for (ActivityDetails activity : activitiesList) {
                    String activityNotes = activity.getTaskNameOrNotes();
                    if(activity.getDateTime()!=null && !activity.getDateTime().isEmpty()){
                        sdate = inputFormat.parse(activity.getDateTime());
                        activity.setFormatDate(outputFormat.format(sdate));
                    }

                    Matcher matcher = emailIdPattern.matcher(activityNotes);
                    List<Integer> emailStartingIndexes = new ArrayList<>();

                    while (matcher.find()) {
                        emailStartingIndexes.add(matcher.start());
                        //emails.put(matcher.start(), matcher.group());
                    }

                    String modifiedNotes;

                    if (emailStartingIndexes.isEmpty()) {
                        modifiedNotes = activityNotes;
                    } else {
                        StringBuilder notesStr = new StringBuilder("");
                        for (Integer index : emailStartingIndexes) {
                            notesStr = new StringBuilder(activityNotes.substring(0, index));
                            String email = activityNotes.substring(index).replace("<", "").replace(">", "");
                            notesStr.append(email);
                        }

                        modifiedNotes = notesStr.toString();
                    }

                    modifiedNotes = newLineAndBreakPattern.matcher(modifiedNotes).replaceAll(" ");

                    Matcher htmlTagsMatcher = htmlTagsPattern.matcher(modifiedNotes);
                    if (htmlTagsMatcher.find()) {
                        modifiedNotes = htmlTagsMatcher.replaceAll("");
                        activity.setHasHTMLTags(true);
                    } else {

                        activity.setHasHTMLTags(false);
                    }

                    activity.setModifiedNotes(modifiedNotes);
                }

                this.customerAllActivitiesList = activitiesList;

                Collections.sort(activitiesList, (m1, m2) -> m2.getFormatDate().compareTo(m1.getFormatDate()));
                activitiesAdapter.setActivitiesList(activitiesList);
                activitiesAdapter.notifyDataSetChanged();

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void viewMoreClicked(int index) {
        ActivityDetails selectedActivityDetails = customerAllActivitiesList.get(index);

        // Already we have activity full notes in customer activities list, that's why we are not making service call to get activity full notes here.
        View contentView = layoutInflater.inflate(R.layout.custom_dialog, null);
        CustomDialog customDialog = new CustomDialog(activityContext, contentView, getResources().getString(R.string.full_notes_title), selectedActivityDetails.getTaskNameOrNotes(), selectedActivityDetails.isHasHTMLTags());
        customDialog.show();
    }

    public void updateActivityNotes(String notes) {

    }

    public void showCreateActivityPage(View view) {
        Intent showCreateActivityForContactIntent = new Intent(activityContext, CreateNewActivityForContactScreen.class);
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, selectedCustomerId);
        bundle.putLong(Constants.BUNDLE_CONTACT_REP_ID, selectedContactDetails.getRepId());
        bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, activityType);
        showCreateActivityForContactIntent.putExtras(bundle);
        startActivity(showCreateActivityForContactIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRefreshActivitiesBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_ACTIVITIES)) {
                    loadDataFromServer();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void markTask(int position, Long taskId, String action) {
        currentTaskId = taskId;
        currentAction = action;

        takeConfirmationToCompleteTask();
    }

    public void takeConfirmationToCompleteTask() {
        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_COMPLETE_TASK, getResources().getString(R.string.title_alert), getResources().getString(R.string.complete_task), "Yes", "No");
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (buttonType == Constants.ButtonType.POSITIVE) {
            if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_COMPLETE_TASK) {
                completeTask(currentTaskId, currentAction);
            }
        }
    }

    public void completeTask(Long taskId, String action) {
        MMProgressDialog.showProgressDialog(activityContext);
        taskListModel.modifyTask(encryptedClientKey, getDTTicks(), taskId, action);
    }

    public void updateActivitiesAfterModify(String action) {
        MMProgressDialog.hideProgressDialog();

        // Resetting activities list before reloading them.
        customerAllActivitiesList = new ArrayList<>();

        loadDataFromServer();
    }
}
