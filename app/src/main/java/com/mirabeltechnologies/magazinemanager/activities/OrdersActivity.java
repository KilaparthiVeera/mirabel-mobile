package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.OrdersAdapter;
import com.mirabeltechnologies.magazinemanager.beans.Order;
import com.mirabeltechnologies.magazinemanager.beans.SearchOrdersFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.OrdersListener;
import com.mirabeltechnologies.magazinemanager.models.OrdersModel;

import java.util.ArrayList;
import java.util.List;

public class OrdersActivity extends ListViewBaseActivity implements OrdersListener {
    public static final String TAG = OrdersActivity.class.getSimpleName();

    private OrdersModel ordersModel;
    private SearchOrdersFilter searchOrdersFilter;
    private OrdersAdapter ordersAdapter;
    private List<Order> allOrdersList;
    private LinearLayout orders_total_net_layout, orders_total_barter_layout;
    private MMTextView orders_total_net, orders_total_barter;
    private boolean isBillingInstallmentSelected = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orders);

        try {
            activityContext = OrdersActivity.this;

            currentActivity = this;
            requestFrom = Constants.RequestFrom.ORDERS_PAGE;

            ordersModel = new OrdersModel(activityContext, this, requestFrom);

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                searchOrdersFilter = (SearchOrdersFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ORDERS_FILTER_OBJECT);
            }

            if (searchOrdersFilter != null && searchOrdersFilter.getAmountTypeValue() == 0)
                isBillingInstallmentSelected = true;

            // Initializing UI References
            list_view = findViewById(R.id.orders_list_view);
            page_number_display = (MMTextView) findViewById(R.id.orders_page_number_display);
            left_nav_arrow = (ImageView) findViewById(R.id.orders_left_nav_arrow);
            right_nav_arrow = (ImageView) findViewById(R.id.orders_right_nav_arrow);

            orders_total_net_layout = (LinearLayout) findViewById(R.id.orders_total_net_layout);
            orders_total_net = (MMTextView) findViewById(R.id.orders_total_net);
            orders_total_barter_layout = (LinearLayout) findViewById(R.id.orders_total_barter_layout);
            orders_total_barter = (MMTextView) findViewById(R.id.orders_total_barter);

            if (isBillingInstallmentSelected)
                orders_total_barter_layout.setVisibility(View.GONE);

            ordersAdapter = new OrdersAdapter(activityContext, this, isBillingInstallmentSelected);
            list_view.setAdapter(ordersAdapter);

            refreshData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void showSearchOrdersFilters(View view) {
        onBackPressed();

        /*Intent showSearchActivitiesFiltersIntent = new Intent(activityContext, SearchOrdersFiltersActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putSerializable(Constants.BUNDLE_SEARCH_ORDERS_FILTER_OBJECT, searchOrdersFilter);
        showSearchActivitiesFiltersIntent.putExtras(bundle);
        startActivityForResult(showSearchActivitiesFiltersIntent, Constants.CHOOSE_FILTERS_REQUEST_CODE);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);*/
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.CHOOSE_FILTERS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                searchOrdersFilter = (SearchOrdersFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ORDERS_FILTER_OBJECT);
            }

            refreshData();
        }
    }

    public void resetValues() {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        if (allOrdersList != null) {
            allOrdersList.clear();
            allOrdersList = null;
        }
        allOrdersList = new ArrayList<>();

        // clearing adapter list
        ordersAdapter.setOrdersList(null);
        ordersAdapter.notifyDataSetChanged();
    }

    public void refreshData() {
        resetValues();
        getOrders(currentPageNumber);
    }

    public void getOrders(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            ordersModel.searchOrders(encryptedClientKey, isDSUser, getDTTicks(), searchOrdersFilter, currentPageNumber);
            isLoading = true;
        }
    }

    public void updateOrders(final List<Order> ordersList, int noOfOrders) {

        if (currentPageNumber == 1 && (ordersList == null || ordersList.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            if (currentPageNumber == 1) {
                Order firstOrder = ordersList.get(0);

                orders_total_net.setText(String.format("$%,.2f", Float.parseFloat(firstOrder.getTotalNet())));

                if (!isBillingInstallmentSelected)
                    orders_total_barter.setText(String.format("$%,.2f", Float.parseFloat(firstOrder.getTotalBarter())));
            }

            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber)) {
                alreadyLoadedPageNumbersList.add(currentPageNumber);
            }

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + ordersList.size();

            if (noOfOrders != totalNoOfRecords) {
                totalNoOfRecords = noOfOrders;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ORDERS_PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.ORDERS_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 2;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getOrders(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.ORDERS_PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            allOrdersList.addAll(ordersList);

            ordersAdapter.setOrdersList(ordersList);
            ordersAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    @Override
    public void openContactDetailPage(Long customerId) {
        Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_CUSTOMER_ID, String.valueOf(customerId));
        openContactDetailsIntent.putExtra(Constants.BUNDLE_IS_PRIMARY_CONTACT, true); // We can create orders for company from primary contact only so we are sending this value as true by default.
        startActivity(openContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
}
