package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.AdvSearchSuggestionsAdapter;
import com.mirabeltechnologies.magazinemanager.beans.AssignDetails;
import com.mirabeltechnologies.magazinemanager.beans.BusinessUnitDetails;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.ContactDetails;
import com.mirabeltechnologies.magazinemanager.beans.CreateOpportunity;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.OppLossReasonDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppTypeDetails;
import com.mirabeltechnologies.magazinemanager.beans.Opportunity;
import com.mirabeltechnologies.magazinemanager.beans.OwnerDetails;
import com.mirabeltechnologies.magazinemanager.beans.ProductDetails;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SearchOrdersFilter;
import com.mirabeltechnologies.magazinemanager.beans.SubContactDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.DelayAutoCompleteTextView;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactsAdvSearchListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.OrdersModel;
import com.mirabeltechnologies.magazinemanager.util.DecimalDigitsInputFilter;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.SELECTED_DATE_DISPLAY_FORMAT;

public class AddOpportunityActivity extends BaseActivity  implements ContactsAdvSearchListener{

    public static final String TAG = AddOpportunityActivity.class.getSimpleName();

    private DelayAutoCompleteTextView search_activities_filters_company_name;
    private AdvSearchSuggestionsAdapter companyNameSuggestionsAdapter;
    TextView screen_title,txt_status,txt_probability,txt_revenue,txt_contact,txt_stage,txt_type,txt_loss_reason,txt_created_by,txt_create_date,txt_company_name,search_activities_filters_from_date,
            search_orders_filters_business_unit,search_orders_filters_product,search_orders_filters_sales_rep;
    private ArrayList<MasterData> allBusinessUnits = null, allProducts = null;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<OppStageDetails> limitedStageDetails = null;
    private ArrayList<SubContactDetails> allSubContactDetails = null;
    private ArrayList<OppTypeDetails> allTypeDetails = null;
    private ArrayList<OppLossReasonDetails> allLossReasonDetail = null;
    private OrdersModel ordersModel;
    private CommonModel commonModel;
    private boolean isLoggedInRepExistsInAllRepsList = false;
    private Constants.RequestFrom requestFrom, requestCameFrom;
    private Constants.SelectionType selectionType;
    private SearchOrdersFilter searchOrdersFilter;
    CreateOpportunity createOpportunity;
    OppStageDetails ownStageDetails,closestageDetails;

    String encryptedId,domain,serverDate,currentDate,selectedDate="",probability,status,type_id="",
            loss_id="",company_id="",contact_id="",selectProbability;
    Integer stage_id;
    ArrayList<String> probability_array,status_array;
    EditText opp_description,txt_name,txt_amount,txt_step,txt_source;
    Button opp_cancel,opp_save;
    public static final String SERVER_DATE_WITH_TIME_FORMAT = "y-MM-dd HH:mm:ss";

    private DetailContact selectedContactDetails = null;
    private String selectedCustomerId, activityType;
    FrameLayout company_lay;
    String opportunity_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_opportunities);
        applicationContext= this;
        ownStageDetails=new OppStageDetails();
        closestageDetails=new OppStageDetails();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            selectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
            selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
            selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
            activityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
            opportunity_id=bundle.getString(Constants.BUNDLE_OPPORTUNITY_DETAILS);
        }
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        domain=sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
        currentDate = Utility.formatDate(new Date(), SELECTED_DATE_DISPLAY_FORMAT);
        serverDate = Utility.formatDate(new Date(), SERVER_DATE_WITH_TIME_FORMAT);

        requestFrom = Constants.RequestFrom.ADD_OPPORTUNITY;

        if (createOpportunity == null)
            createOpportunity = new CreateOpportunity();

        if (searchOrdersFilter == null)
            searchOrdersFilter = new SearchOrdersFilter();

        allRepsList=new ArrayList<>();
        allBusinessUnits = new ArrayList<>();
        allProducts = new ArrayList<>();
        limitedStageDetails=new ArrayList<>();
        allTypeDetails=new ArrayList<>();
        allLossReasonDetail=new ArrayList<>();
        allSubContactDetails=new ArrayList<>();

        ordersModel = new OrdersModel(activityContext, this, requestFrom);
        commonModel = new CommonModel(activityContext, this, requestFrom);
        companyNameSuggestionsAdapter = new AdvSearchSuggestionsAdapter(activityContext, this, encryptedClientKey);

        opp_cancel=findViewById(R.id.opp_cancel);
        opp_save=findViewById(R.id.opp_save);
        company_lay=findViewById(R.id.company_lay);


        txt_probability=findViewById(R.id.txt_probability);

        screen_title=findViewById(R.id.screen_title);
        txt_name=findViewById(R.id.txt_name);
        txt_contact=findViewById(R.id.txt_contact);
        opp_description=findViewById(R.id.opp_description);
        txt_stage = findViewById(R.id.txt_stage);
        txt_status=findViewById(R.id.txt_status);
        txt_type=findViewById(R.id.txt_type);
        txt_loss_reason=findViewById(R.id.txt_loss_reason);
        txt_amount=findViewById(R.id.txt_amount);

        txt_amount.setFilters(new InputFilter[] {new DecimalDigitsInputFilter(12,2)});

        txt_source=findViewById(R.id.txt_source);
        txt_step=findViewById(R.id.txt_step);
        txt_revenue=findViewById(R.id.txt_revenue);
        txt_create_date=findViewById(R.id.txt_create_date);
        txt_company_name=findViewById(R.id.txt_company_name);
        txt_created_by=findViewById(R.id.txt_created_by);

        search_activities_filters_from_date=findViewById(R.id.search_activities_filters_from_date);
        search_orders_filters_business_unit = findViewById(R.id.search_orders_filters_business_unit);
        search_orders_filters_product = findViewById(R.id.search_orders_filters_product);
        search_orders_filters_sales_rep = findViewById(R.id.search_orders_filters_sales_rep);
        search_activities_filters_company_name = findViewById(R.id.search_activities_filters_company_name);


        txt_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0){
                    double amount=Double.parseDouble(s.toString());
                    if(probability!=null && !probability.isEmpty()){
                        int weight=Integer.parseInt(probability);
                        double total= amount*weight/100;
                        DecimalFormat df=new DecimalFormat("#.##");
                        txt_revenue.setText(String.format("%s",df.format(total)));
                    }
                    else {
                        txt_revenue.setText("0.00");
                    }
                }
                else {
                    txt_revenue.setText("0.00");
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });

        if(activityType.equals(Constants.ACTIVITY_TYPE_OPPORTUNITY)){

            initVisiable();
            company_lay.setVisibility(View.GONE);
            company_id=selectedContactDetails.getCustomersId();
            txt_company_name.setText(selectedContactDetails.getCompanyName());
            ContactDetails contactDetails= new ContactDetails();
            contactDetails.setSalesRepID(loggedInRepId);
            contactDetails.setId(Integer.parseInt(selectedContactDetails.getCustomersId()));

            createOpportunity.setContactDetails(contactDetails);
        }
        else {
            initGone();
            company_lay.setVisibility(View.VISIBLE);
            search_activities_filters_company_name.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            search_activities_filters_company_name.setAdapter(companyNameSuggestionsAdapter);
            search_activities_filters_company_name.setLoadingIndicator(findViewById(R.id.search_activities_filters_company_name_loading_indicator));

            search_activities_filters_company_name.setOnItemClickListener((adapterView, view, position, id) -> {
                CompanyData selectedCompany = (CompanyData) adapterView.getItemAtPosition(position);

                if(selectedCompany!=null){
                    initVisiable();
                }
                search_activities_filters_company_name.setAdapter(null); // to stop filtering after selecting row from drop down
                search_activities_filters_company_name.setText(selectedCompany.getName());
                txt_company_name.setText(selectedCompany.getName());
                search_activities_filters_company_name.setAdapter(companyNameSuggestionsAdapter);
                search_activities_filters_company_name.clearFocus();
                company_id=selectedCompany.getId();
                txt_contact.setText("");
                allSubContactDetails.clear();

                ContactDetails contactDetails= new ContactDetails();
                contactDetails.setSalesRepID(loggedInRepId);
                contactDetails.setId(Integer.parseInt(selectedCompany.getId()));

                createOpportunity.setContactDetails(contactDetails);

                hideKeyboard(search_activities_filters_company_name);
            });

            search_activities_filters_company_name.setOnFocusChangeListener((view, hasFocus) -> {
                if (hasFocus) {
                    companyNameSuggestionsAdapter.setTypeOfFilter(0);
                    search_activities_filters_company_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);

                } else {
                    search_activities_filters_company_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_gray, 0);
                }
            });

        }

        search_activities_filters_from_date.setOnClickListener(this::showDatePicker);

        txt_contact.setOnClickListener(v -> {
            if(!company_id.isEmpty()){
                showAllSubContacts(company_id);
            }
            else {
           Toast.makeText(AddOpportunityActivity.this, "Please Select Company First", Toast.LENGTH_SHORT).show();
            }
        });

        txt_stage.setOnClickListener(v -> showAllStageUnits());
        txt_status.setOnClickListener(v -> showAllStatusUnits());
        txt_probability.setOnClickListener(v -> showAllProbabilities());
        txt_type.setOnClickListener(v -> showAllTypeUnits());
        txt_loss_reason.setOnClickListener(v -> showAllLossReason());
        search_orders_filters_business_unit.setOnClickListener(v -> showAllBusinessUnits());
        search_orders_filters_product.setOnClickListener(v -> showAllProducts());
        search_orders_filters_sales_rep.setOnClickListener(this::showAllRepsList);


        status_array= new ArrayList<>();
        status_array.add("Open");
        status_array.add("Won");
        status_array.add("Lost");

        probability_array= new ArrayList<>();
        probability_array.add("0%");
        probability_array.add("10%");
        probability_array.add("20%");
        probability_array.add("30%");
        probability_array.add("40%");
        probability_array.add("50%");
        probability_array.add("60%");
        probability_array.add("70%");
        probability_array.add("80%");
        probability_array.add("90%");
        probability_array.add("100%");

        opp_cancel.setOnClickListener(this::closeActivity);

        opp_save.setOnClickListener(v -> {

            String name=txt_name.getText().toString();
            String amount= txt_amount.getText().toString();
            String source= txt_source.getText().toString();
            String step=txt_step.getText().toString();
            String notes=opp_description.getText().toString();
            createOpportunity.setName(name);
            createOpportunity.setAmount(amount);
            createOpportunity.setSource(source);
            createOpportunity.setNextStep(step);
            createOpportunity.setNotes(notes);
            if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){

                OwnerDetails ownerDetails= new OwnerDetails();
                ownerDetails.setId(loggedInRepId);
                ownerDetails.setName(loggedInRepName);
                createOpportunity.setOwnerDetails(ownerDetails);
                createOpportunity.setCreatedDate(serverDate);
            }

            if(createOpportunity.getAssignedTODetails()==null ){
                AssignDetails assignDetails= new AssignDetails();
                assignDetails.setId(loggedInRepId);
                assignDetails.setName(loggedInRepName);
                createOpportunity.setAssignedTODetails(assignDetails);
            }

            if(TextUtils.isEmpty(company_id)){
                Toast.makeText(applicationContext, "Please Enter Company Name", Toast.LENGTH_SHORT).show();
            }
            else if(TextUtils.isEmpty(name)){
                Toast.makeText(applicationContext, "Please Enter Opportunity Name", Toast.LENGTH_SHORT).show();
            }
            else if(stage_id==null){
                Toast.makeText(applicationContext, "Please Select Stage", Toast.LENGTH_SHORT).show();
            }
            else if(TextUtils.isEmpty(amount)){
                Toast.makeText(applicationContext, "Please Enter Amount", Toast.LENGTH_SHORT).show();
            }
            else if(probability==null){
                Toast.makeText(applicationContext, "Please Enter Probability", Toast.LENGTH_SHORT).show();
            }
            else if(TextUtils.isEmpty(createOpportunity.getCloseDate()) || createOpportunity.getCloseDate()==null){
                Toast.makeText(applicationContext, "Please Enter Close Date", Toast.LENGTH_SHORT).show();
            }
            else if(status.equals("Lost")){
                if(loss_id.equals("-1") || loss_id.equals("")){
                    Toast.makeText(applicationContext, "Please Select Closed Reason", Toast.LENGTH_SHORT).show();
                }
                else  if(TextUtils.isEmpty(notes)){
                    Toast.makeText(applicationContext, "Please Enter  Notes ", Toast.LENGTH_SHORT).show();
                }
                else {
                 if (!MMProgressDialog.isProgressDialogShown)
                        MMProgressDialog.showProgressDialog(activityContext);
                        commonModel.saveOpportunity(encryptedId,domain, createOpportunity);
                } }
            else {
                if (!MMProgressDialog.isProgressDialogShown)
                    MMProgressDialog.showProgressDialog(activityContext);
                commonModel.saveOpportunity(encryptedId,domain, createOpportunity);
            }
        });

        loadDataFromServer();
        LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE));

    }

    private void initGone() {
        txt_name.setEnabled(false);
        txt_company_name.setEnabled(false);
        txt_contact.setEnabled(false);
        txt_status.setEnabled(false);
        txt_stage.setEnabled(false);
        txt_amount.setEnabled(false);
        txt_probability.setEnabled(false);
        txt_type.setEnabled(false);
        search_orders_filters_business_unit.setEnabled(false);
        search_orders_filters_product.setEnabled(false);
        txt_source.setEnabled(false);
        search_activities_filters_from_date.setEnabled(false);
        search_orders_filters_sales_rep.setEnabled(false);
        txt_loss_reason.setEnabled(false);
        txt_step.setEnabled(false);
        opp_description.setEnabled(false);
    }

    private void initVisiable() {
        txt_name.setEnabled(true);
        txt_company_name.setEnabled(true);
        txt_contact.setEnabled(true);
        txt_status.setEnabled(true);
        txt_stage.setEnabled(true);
        txt_amount.setEnabled(true);
        txt_probability.setEnabled(true);
        txt_type.setEnabled(true);
        search_orders_filters_business_unit.setEnabled(true);
        search_orders_filters_product.setEnabled(true);
        txt_source.setEnabled(true);
        search_activities_filters_from_date.setEnabled(true);
        search_orders_filters_sales_rep.setEnabled(true);
        txt_loss_reason.setEnabled(true);
        txt_step.setEnabled(true);
        opp_description.setEnabled(true);
    }



    public void showDatePicker(View view) {
        try {
            selectedDate = currentDate;
            if (selectedDate.isEmpty()) {
                selectedDate = Utility.formatDate(new Date(), SELECTED_DATE_DISPLAY_FORMAT);
            }
            String[] currentDateValues = selectedDate.split("/");
            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, (datePicker, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);
                selectedDate = Utility.formatDate(calendar.getTime(), SELECTED_DATE_DISPLAY_FORMAT);
               // currentDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
                search_activities_filters_from_date.setText(selectedDate);

                createOpportunity.setCloseDate(selectedDate);
            }, Integer.parseInt(currentDateValues[2]), Integer.parseInt(currentDateValues[0]) - 1, Integer.parseInt(currentDateValues[1]));

          //  datePickerDialog.setTitle("Select Date");
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
        commonModel.getAllStageUnits(encryptedId,domain);

    }

    private void showAllStatusUnits() {
        Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
        showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_STATUS);
        bundle.putSerializable(Constants.OPPORTUNITY_STATUS, status_array);
        bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, createOpportunity.getStatus());
        showAllRepsListIntent.putExtras(bundle);
        startActivity(showAllRepsListIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void showAllProbabilities() {
        Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
        showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.ADD_PROBABIlILITY);
        bundle.putSerializable(Constants.ADD_PROBABIlILITY, probability_array);
        bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, selectProbability);
        showAllRepsListIntent.putExtras(bundle);
        startActivity(showAllRepsListIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    private void showAllSubContacts(String company_id) {
        if (allSubContactDetails != null && allSubContactDetails.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_CONTACT);
            bundle.putSerializable(Constants.OPPORTUNITY_CONTACT, allSubContactDetails);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, contact_id);
            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);

        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllSubContacts(encryptedId,domain,company_id);
        }
    }
    private void showAllStageUnits() {
        if (limitedStageDetails != null && limitedStageDetails.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_STAGE);
            bundle.putSerializable(Constants.ADMIN_STAGE, limitedStageDetails);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(stage_id));
            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllStageUnits(encryptedId,domain);
        }
    }

    private void showAllTypeUnits() {
        if (allTypeDetails != null && allTypeDetails.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_TYPE);
            bundle.putSerializable(Constants.ADMIN_TYPE, allTypeDetails);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, type_id);
            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getAllTypeAndReason(encryptedId,domain,Constants.GET_ADMIN_DATA_TYPE);
        }
    }

    private void showAllLossReason() {
        if(createOpportunity.getStatus().equals("Open") || createOpportunity.getStatus().equals("Won")){
            String message="Closed Lost Reason' can be selected only when status is set to 'Lost'.";
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
        }
        else {
            if (allLossReasonDetail != null && allLossReasonDetail.size() > 0) {
                Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
                showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.OPPORTUNITY_REASON);
                bundle.putSerializable(Constants.ADMIN_RESON, allLossReasonDetail);
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, loss_id);
                showAllRepsListIntent.putExtras(bundle);
                startActivity(showAllRepsListIntent);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            } else {
                MMProgressDialog.showProgressDialog(activityContext);
                commonModel.getAllTypeAndReason(encryptedId,domain,Constants.GET_ADMIN_DATA_REASON);
            }
        }

    }
    public void showAllProducts() {
        if (allProducts != null && allProducts.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.PRODUCT, allProducts, searchOrdersFilter.getProductId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_PRODUCTS);
        }
    }

    public void showAllBusinessUnits() {
        if (allBusinessUnits != null && allBusinessUnits.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.BUSINESS_UNIT, allBusinessUnits, searchOrdersFilter.getBusinessUnitId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_BUSINESS_UNITS);
        }
    }

    public void showAllRepsList(View view) {

        if (allRepsList != null && allRepsList.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.SALES_REP);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchOrdersFilter.getSalesRepId());
            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllValuesForSelection(Constants.SelectionType selectionType, ArrayList<MasterData> masterData, String preSelectedValue) {
        try {
            Intent showAllValuesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllValuesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            // We may get TransactionTooLargeException as we have more issues around 2160 and we are getting these details in adapter class from static variable #allIssues
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, masterData);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, preSelectedValue);
            showAllValuesIntent.putExtras(bundle);
            startActivity(showAllValuesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetValues(View view) {
        allBusinessUnits = new ArrayList<>();
        allProducts = new ArrayList<>();
        if (view != null) {
            searchOrdersFilter = new SearchOrdersFilter();
        }
        // Setting default values
        if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {
            isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));
            if (isLoggedInRepExistsInAllRepsList) {
                searchOrdersFilter.setSalesRepId(String.valueOf(loggedInRepId));
                searchOrdersFilter.setSalesRepName(loggedInRepName);
            } else {
                searchOrdersFilter.setSalesRepId("-1");
                searchOrdersFilter.setSalesRepName("");
            }
        }
        if (view != null) {
            searchOrdersFilter.setSalesRepId("-1");
            searchOrdersFilter.setSalesRepName("");
            searchOrdersFilter.setBusinessUnit("All");
            searchOrdersFilter.setProduct("All");
        }

      //  updateUIWithLatestValues();
    }


    @Override
    protected void onPause() {
        super.onPause();
        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }
        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }
    @Override
    protected void onStop() {
        super.onStop();
        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }
    public void closeActivity(View view) {
        onBackPressed();
    }


    @Override
    public void updateSearchKeyword(String keyword, int typeOfFilter) {
        /*if (typeOfFilter == 0) {
           // searchFilter.setCompanyName(keyword);
           // searchFilter.setCompanyId("");
        }*/
    }


    public void updateAllRepsData(List<RepData> repsList) {
        for(int i=0;i<repsList.size();i++){
            if(repsList.get(i).getName().equals("All Reps")){
                repsList.get(i).setName("");
            }

            if(repsList.get(i).getName().equals("------Disabled Reps------")){
                break;
            }else {
                allRepsList.add(repsList.get(i));
            }
        }

       // allRepsList = (ArrayList) repsList;
        MMProgressDialog.hideProgressDialog();
        resetValues(null);

        if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){
            screen_title.setText("Add Opportunity");
            status=status_array.get(0);
            createOpportunity.setStatus(status);
            txt_status.setText(createOpportunity.getStatus());
            OppStageDetails stageDetails= new OppStageDetails();
            stageDetails.setStage("");
            didStageChanged(stageDetails);
            txt_created_by.setText(loggedInRepName);
            txt_create_date.setText(currentDate);

        }
        else {
            initVisiable();
            company_lay.setVisibility(View.GONE);
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getSelectOpportunity(encryptedId,domain, opportunity_id);
        }
    }


    public void updateMasterData(List<MasterData> masterDataList, String fieldType) {
        switch (fieldType) {
            case Constants.GET_MASTER_DATA_BUSINESS_UNITS:
                allBusinessUnits = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();
                showAllBusinessUnits();
                break;
            case Constants.GET_MASTER_DATA_PRODUCTS:
                allProducts = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();
                showAllProducts();
                break;
        }
    }


    public void updateStageDetails(List<OppStageDetails> stageDetailsList) {
        MMProgressDialog.hideProgressDialog();
        if (stageDetailsList != null && stageDetailsList.size() > 0) {
            for(int i=0;i<stageDetailsList.size();i++){
                if(!stageDetailsList.get(i).getStage().equals("Closed Won") && !stageDetailsList.get(i).getStage().equals("Closed Lost")){
                    limitedStageDetails.add(stageDetailsList.get(i));
                }
               else if(stageDetailsList.get(i).getStage().equals("Closed Won")){
                    ownStageDetails=stageDetailsList.get(i);
                }
                else if(stageDetailsList.get(i).getStage().equals("Closed Lost")){
                    closestageDetails=stageDetailsList.get(i);
               }
            }
        }

    }
    public void updateTypeDetails(List<OppTypeDetails> stageDetails) {
        MMProgressDialog.hideProgressDialog();
        if(stageDetails!=null && !stageDetails.isEmpty()){
            allTypeDetails = (ArrayList) stageDetails;
            showAllTypeUnits();
        }

    }

    public void updateLossReason(List<OppLossReasonDetails> stageDetails) {

        MMProgressDialog.hideProgressDialog();
        if(stageDetails!=null && !stageDetails.isEmpty()){
            allLossReasonDetail = (ArrayList) stageDetails;
            showAllLossReason();
        }
    }

    public void updateSubContactDetails(List<SubContactDetails> subContactDetails, String company_id) {

        MMProgressDialog.hideProgressDialog();
        if(subContactDetails!=null && !subContactDetails.isEmpty()){
            allSubContactDetails=(ArrayList) subContactDetails;
            showAllSubContacts(company_id);
        }
    }

    private final BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE)) {
                    assert bundle != null;
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.SALES_REP) {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.BUSINESS_UNIT) {
                        if (selectedObj != null) {
                            didBusinessUnitChanged((MasterData) selectedObj);
                        }
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PRODUCT) {
                        if (selectedObj != null) {
                            didProductChanged((MasterData) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_STAGE) {
                        if (selectedObj != null) {
                            didStageChanged((OppStageDetails) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_STATUS) {
                        if (selectedObj != null) {
                            didStatusChanged((String) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.ADD_PROBABIlILITY) {
                        if (selectedObj != null) {
                            didProbabilityChanged((String) selectedObj);
                        }
                    }

                    else if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_TYPE) {
                        if (selectedObj != null) {
                            didTypeChanged((OppTypeDetails) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_REASON) {
                        if (selectedObj != null) {
                            didLossReasonChanged((OppLossReasonDetails) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.OPPORTUNITY_CONTACT) {
                        if (selectedObj != null) {
                            didSubContactChanged((SubContactDetails) selectedObj);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void didProbabilityChanged(String selectedObj) {
        selectProbability= selectedObj;

            if (selectedObj.length() > 0 && selectedObj.charAt(selectedObj.length() - 1) == '%') {
                probability = selectedObj.substring(0, selectedObj.length() - 1);
                if(probability.equals("0")){
                    txt_probability.setText(selectProbability);
                    String message="Changing the Probability to 0% will change the Status to 'Lost'. Please confirm";
                    MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, new AlertDialogSelectionListener() {
                        @Override
                        public void alertDialogCallback() { }
                        @Override
                        public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
                            if (buttonType == Constants.ButtonType.POSITIVE) {
                                createOpportunity.setProbability(Integer.parseInt(probability));
                                status="Lost";
                                createOpportunity.setStatus(status);
                                txt_status.setText(status);
                                txt_stage.setEnabled(false);
                                txt_probability.setText("0%");
                                txt_probability.setEnabled(false);
                                txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));
                                stage_id=closestageDetails.getId();
                                didStageChanged(closestageDetails);

                                int weight=Integer.parseInt(probability);
                                String amount=txt_amount.getText().toString();
                                if(!TextUtils.isEmpty(amount)){
                                    double value=Double.parseDouble(amount);
                                    double total= value*weight/100;
                                    DecimalFormat df=new DecimalFormat("#.##");
                                    txt_revenue.setText(String.format("%s",df.format(total)));
                                }
                                else {
                                    txt_revenue.setText("0.00");
                                }


                            } else if (buttonType == Constants.ButtonType.NEGATIVE)  {
                                MMAlertDialog.dismiss();
                                txt_status.setText(createOpportunity.getStatus());
                                if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){
                                    probability=null;
                                    txt_revenue.setText("0.00");
                                    txt_probability.setText("");
                                }
                                else {
                                    selectProbability=createOpportunity.getProbability()+"%";
                                    txt_probability.setText(selectProbability);
                                    probability=String.valueOf(createOpportunity.getProbability());
                                    createOpportunity.setProbability(Integer.parseInt(probability));
                                    int weight=Integer.parseInt(probability);
                                    String amount=txt_amount.getText().toString();
                                    if(!TextUtils.isEmpty(amount)){
                                        double value=Double.parseDouble(amount);
                                        double total= value*weight/100;
                                        DecimalFormat df=new DecimalFormat("#.##");
                                        txt_revenue.setText(String.format("%s",df.format(total)));
                                    }
                                }
                            }
                        }
                    }, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), message, "Yes", "No");
                }
                else if(probability.equals("100")){
                    txt_probability.setText(selectProbability);
                    String message="Changing the Probability to 100% will change the Status to 'Won'. Please confirm";
                    MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, new AlertDialogSelectionListener() {
                        @Override
                        public void alertDialogCallback() { }
                        @Override
                        public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
                            if (buttonType == Constants.ButtonType.POSITIVE) {
                                createOpportunity.setProbability(Integer.parseInt(probability));
                                status="Won";
                                createOpportunity.setStatus(status);
                                txt_status.setText(status);
                                txt_stage.setEnabled(false);
                                txt_probability.setText("100%");
                                txt_probability.setEnabled(false);
                                txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));
                                stage_id=ownStageDetails.getId();
                                didStageChanged(ownStageDetails);
                                OppLossReasonDetails lossReasonDetails= new OppLossReasonDetails();
                                lossReasonDetails.setId(-1);
                                createOpportunity.setOppLossReasonDetails(lossReasonDetails);
                                didLossReasonChanged(createOpportunity.getOppLossReasonDetails());

                                int weight=Integer.parseInt(probability);
                                String amount=txt_amount.getText().toString();
                                if(!TextUtils.isEmpty(amount)){
                                    double value=Double.parseDouble(amount);
                                    double total= value*weight/100;
                                    DecimalFormat df=new DecimalFormat("#.##");
                                    txt_revenue.setText(String.format("%s",df.format(total)));
                                }
                                else {
                                    txt_revenue.setText("0.00"); }

                            } else if (buttonType == Constants.ButtonType.NEGATIVE)  {
                                MMAlertDialog.dismiss();
                                txt_status.setText(createOpportunity.getStatus());
                                if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){
                                    probability=null;
                                    txt_probability.setText("");
                                    txt_revenue.setText("0.00");
                                }
                                else {
                                    selectProbability=createOpportunity.getProbability()+"%";
                                    txt_probability.setText(selectProbability);
                                    probability=String.valueOf(createOpportunity.getProbability());
                                    createOpportunity.setProbability(Integer.parseInt(probability));
                                    int weight=Integer.parseInt(probability);
                                    String amount=txt_amount.getText().toString();
                                    if(!TextUtils.isEmpty(amount)){
                                        double value=Double.parseDouble(amount);
                                        double total= value*weight/100;
                                        DecimalFormat df=new DecimalFormat("#.##");
                                        txt_revenue.setText(String.format("%s",df.format(total)));
                                    }
                                }
                            }
                        }
                    }, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), message, "Yes", "No");
                }
                else {
                    txt_probability.setText(selectProbability);
                    createOpportunity.setProbability(Integer.parseInt(probability));
                    int weight=Integer.parseInt(probability);
                    String amount=txt_amount.getText().toString();
                    if(!TextUtils.isEmpty(amount)){
                        double value=Double.parseDouble(amount);
                        double total= value*weight/100;
                        DecimalFormat df=new DecimalFormat("#.##");
                        txt_revenue.setText(String.format("%s",df.format(total)));

                    }
                    else {
                        txt_revenue.setText("0.00");
                    }
                }
            }
    }

    private void didStatusChanged(String selectStatus) {
        status=selectStatus;
       /* createOpportunity.setStatus(Status);
        txt_status.setText(status);*/
        if(!status.equals(createOpportunity.getStatus())){
            if(status.equals("Won")){
                String message="Changing the Status to 'Won' will change the Stage to 'Closed Won' and Probability to 100%. Please confirm";
                MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, new AlertDialogSelectionListener() {
                    @Override
                    public void alertDialogCallback() { }
                    @Override
                    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
                        if (buttonType == Constants.ButtonType.POSITIVE) {
                            createOpportunity.setStatus(status);
                            txt_status.setText(status);
                            txt_stage.setEnabled(false);
                            probability="100";
                            createOpportunity.setProbability(Integer.parseInt(probability));
                            selectProbability="100%";
                            txt_probability.setText(selectProbability);
                            txt_probability.setEnabled(false);
                            txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));

                            stage_id=ownStageDetails.getId();
                            didStageChanged(ownStageDetails);
                            OppLossReasonDetails lossReasonDetails= new OppLossReasonDetails();
                            lossReasonDetails.setId(-1);
                            createOpportunity.setOppLossReasonDetails(lossReasonDetails);
                            didLossReasonChanged(createOpportunity.getOppLossReasonDetails());

                            int weight=Integer.parseInt(probability);
                            String amount=txt_amount.getText().toString();
                            if(!TextUtils.isEmpty(amount)){
                                double value=Double.parseDouble(amount);
                                double total= value*weight/100;
                                DecimalFormat df=new DecimalFormat("#.##");
                                txt_revenue.setText(String.format("%s",df.format(total)));
                            }
                            else {
                                txt_revenue.setText("0.00");
                            }
                        } else if (buttonType == Constants.ButtonType.NEGATIVE)  {
                            MMAlertDialog.dismiss();
                            txt_status.setText(createOpportunity.getStatus());
                            if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){
                                if(createOpportunity.getStatus().equals("Open")){
                                    selectProbability="";
                                    probability=null;
                                    txt_probability.setText("");
                                    OppStageDetails stageDetails= new OppStageDetails();
                                    stageDetails.setStage("");
                                    didStageChanged(stageDetails);
                                    txt_revenue.setText("0.00");
                                }
                            }
                            else {
                                selectProbability=createOpportunity.getProbability()+"%";
                                txt_probability.setText(selectProbability);
                                probability=String.valueOf(createOpportunity.getProbability());
                                createOpportunity.setProbability(Integer.parseInt(probability));
                                int weight=Integer.parseInt(probability);
                                String amount=txt_amount.getText().toString();
                                if(!TextUtils.isEmpty(amount)){
                                    double value=Double.parseDouble(amount);
                                    double total= value*weight/100;
                                    DecimalFormat df=new DecimalFormat("#.##");
                                    txt_revenue.setText(String.format("%s",df.format(total)));
                                }
                            }
                        }
                    }
                }, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), message, "Yes", "No");
            }else  if(status.equals("Lost")){
                String message="Changing the Status to 'Lost' will change the Stage to 'Closed Lost' and Probability to 0%. Please confirm";
                MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, new AlertDialogSelectionListener() {
                    @Override
                    public void alertDialogCallback() { }
                    @Override
                    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
                        if (buttonType == Constants.ButtonType.POSITIVE) {
                            createOpportunity.setStatus(status);
                            txt_status.setText(status);
                            txt_stage.setEnabled(false);
                            probability="0";
                            selectProbability="0%";
                            createOpportunity.setProbability(Integer.parseInt(probability));
                            txt_probability.setText(selectProbability);
                            txt_probability.setEnabled(false);
                            txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));
                            txt_loss_reason.setText("");
                            OppLossReasonDetails oppLossReasonDetails= new OppLossReasonDetails();
                            createOpportunity.setOppLossReasonDetails(oppLossReasonDetails);
                            stage_id=closestageDetails.getId();
                            didStageChanged(closestageDetails);
                            int weight=Integer.parseInt(probability);
                            String amount=txt_amount.getText().toString();
                            if(!TextUtils.isEmpty(amount)){
                                double value=Double.parseDouble(amount);
                                double total= value*weight/100;
                                DecimalFormat df=new DecimalFormat("#.##");
                                txt_revenue.setText(String.format("%s",df.format(total)));
                            }
                            else {
                                txt_revenue.setText("0.00");
                            }

                        } else if (buttonType == Constants.ButtonType.NEGATIVE)  {
                            MMAlertDialog.dismiss();
                            txt_status.setText(createOpportunity.getStatus());
                            if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD){
                                if(createOpportunity.getStatus().equals("Open")){
                                    selectProbability="";
                                    probability=null;
                                    txt_probability.setText("");
                                    OppStageDetails stageDetails= new OppStageDetails();
                                    stageDetails.setStage("");
                                    didStageChanged(stageDetails);
                                    txt_revenue.setText("0.00");
                                }
                            }
                            else {
                                selectProbability=createOpportunity.getProbability()+"%";
                                txt_probability.setText(selectProbability);
                                probability=String.valueOf(createOpportunity.getProbability());
                                createOpportunity.setProbability(Integer.parseInt(probability));
                                int weight=Integer.parseInt(probability);
                                String amount=txt_amount.getText().toString();
                                if(!TextUtils.isEmpty(amount)){
                                    double value=Double.parseDouble(amount);
                                    double total= value*weight/100;
                                    DecimalFormat df=new DecimalFormat("#.##");
                                    txt_revenue.setText(String.format("%s",df.format(total)));
                                }
                            }
                        }
                    }
                }, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), message, "Yes", "No");
            }
            else {
                txt_probability.setEnabled(true);
                txt_probability.setBackground(getResources().getDrawable(R.drawable.background_white));

                probability=null;
                selectProbability="";
                txt_probability.setText(selectProbability);
                txt_stage.setEnabled(true);
                txt_loss_reason.setText("");
                OppLossReasonDetails oppLossReasonDetails= new OppLossReasonDetails();
                createOpportunity.setOppLossReasonDetails(oppLossReasonDetails);
                createOpportunity.setStatus(status);
                txt_status.setText(createOpportunity.getStatus());
                OppStageDetails stageDetails= new OppStageDetails();
                stageDetails.setStage("");
                didStageChanged(stageDetails);
                txt_revenue.setText("0.00");
            }
        }
    }
    private void didSubContactChanged(SubContactDetails subContactDetails) {
        SubContactDetails details = new SubContactDetails();
        details.setId(subContactDetails.getId());
        details.setName(subContactDetails.getContactFullName());
        createOpportunity.setSubContactDetails(details);
        contact_id=String.valueOf(subContactDetails.getId());
        txt_contact.setText(subContactDetails.getContactFullName());
    }
    private void didLossReasonChanged(OppLossReasonDetails lossReasonDetails) {
        createOpportunity.setOppLossReasonDetails(lossReasonDetails);
        loss_id=String.valueOf(lossReasonDetails.getId());
        txt_loss_reason.setText(lossReasonDetails.getName());
    }
    private void didTypeChanged(OppTypeDetails typeDetails) {
        createOpportunity.setOppTypeDetails(typeDetails);
        type_id=String.valueOf(typeDetails.getId());
        txt_type.setText(typeDetails.getName());
    }

    private void didStageChanged(OppStageDetails oppStageDetails) {
        createOpportunity.setOppStageDetails(oppStageDetails);
        stage_id=oppStageDetails.getId();
        Log.e("veera",""+stage_id);
        txt_stage.setText(oppStageDetails.getStage());
    }

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.SALES_REP) {
            Log.e("veera",selectedRep.toString());
            AssignDetails assignDetails= new AssignDetails();
            if(selectedRep.getId().equals("-1")){
                assignDetails.setId(loggedInRepId);
                assignDetails.setName(loggedInRepName);
            }
            else {
                assignDetails.setId(Integer.parseInt(selectedRep.getId()));
                assignDetails.setName(selectedRep.getName());
            }

            createOpportunity.setAssignedTODetails(assignDetails);
            searchOrdersFilter.setSalesRepId(selectedRep.getId());
            searchOrdersFilter.setSalesRepName(selectedRep.getName());
            search_orders_filters_sales_rep.setText(searchOrdersFilter.getSalesRepName());
        }
    }

    private void didBusinessUnitChanged(MasterData selectedBusinessUnit) {
        BusinessUnitDetails businessUnitDetails= new BusinessUnitDetails();
        businessUnitDetails.setId(Integer.parseInt(selectedBusinessUnit.getId()));
        businessUnitDetails.setName(selectedBusinessUnit.getName());
        createOpportunity.setBusinessUnitDetails(businessUnitDetails);

        searchOrdersFilter.setBusinessUnit(selectedBusinessUnit.getName());
        searchOrdersFilter.setBusinessUnitId(selectedBusinessUnit.getId());
        search_orders_filters_business_unit.setText(searchOrdersFilter.getBusinessUnit());
        allProducts = new ArrayList<>();

    }

    public void didProductChanged(MasterData selectedProduct) {
        ProductDetails productDetails= new ProductDetails();
        productDetails.setId(Integer.parseInt(selectedProduct.getId()));
        productDetails.setName(selectedProduct.getName());

        createOpportunity.setProductDetails(productDetails);
        searchOrdersFilter.setProduct(selectedProduct.getName());
        searchOrdersFilter.setProductId(selectedProduct.getId());
        search_orders_filters_product.setText(searchOrdersFilter.getProduct());
        allBusinessUnits = new ArrayList<>();
    }
    public void updateOpportunity(int value) {
        if (selectionType == Constants.SelectionType.OPPORTUNITY_ADD ||selectionType == Constants.SelectionType.OPPORTUNITY_EDIT ){
            sendBroadcastWithSelectedIndex(value);
        }
    }
    private void sendBroadcastWithSelectedIndex(int value) {
        try {
            Intent intent = null;
            Bundle bundle = new Bundle();
            if (requestCameFrom == Constants.RequestFrom.OPPORTUNITY) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (int) value);
            }
            else
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
            intent.putExtras(bundle);
            // Broadcasting Selection Index to Receiver Activity
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
            // Closing Activity
            closeActivity(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void updateSelectOpportunity(Opportunity selectOpportunity) {

        MMProgressDialog.hideProgressDialog();
        if (selectOpportunity != null) {
            String pattern = "yyyy-MM-dd'T'HH:mm:ss";
            SimpleDateFormat input = new SimpleDateFormat(pattern);
            SimpleDateFormat output = new SimpleDateFormat(SELECTED_DATE_DISPLAY_FORMAT);
            try {
                screen_title.setText("Update Opportunity");
                Log.e("veera", selectOpportunity.toString());

               Date  oneCloseDate = input.parse(selectOpportunity.getCloseDate());                 // parse input
               Date  oneCreateDate = input.parse(selectOpportunity.getCreatedDateTo());                 // parse input

                if (oneCreateDate != null) {
                    createOpportunity.setCreatedDate(output.format(oneCreateDate));
                }
                else {
                    createOpportunity.setCreatedDate("");
                }
                if (oneCloseDate != null) {
                    selectedDate= (output.format(oneCloseDate));
                    createOpportunity.setCloseDate(selectedDate);
                }
                else {
                    createOpportunity.setCloseDate("");
                }
                status = selectOpportunity.getStatus();
                contact_id = String.valueOf(selectOpportunity.getSubContactDetails().getId());
                stage_id = selectOpportunity.getOppStageDetails().getId();
                probability = String.valueOf(selectOpportunity.getProbability());
                type_id = String.valueOf(selectOpportunity.getOppTypeDetails().getId());
                searchOrdersFilter.setBusinessUnitId(String.valueOf(selectOpportunity.getBusinessUnitDetails().getId()));
                searchOrdersFilter.setProductId(String.valueOf(selectOpportunity.getProductDetails().getId()));
                searchOrdersFilter.setSalesRepId(String.valueOf(selectOpportunity.getAssignedTODetails().getId()));
                if(selectOpportunity.getAssignedTODetails().getId()==-1){
                    selectOpportunity.getAssignedTODetails().setName("");
                }
                createOpportunity.setAssignedTODetails(selectOpportunity.getAssignedTODetails());
                createOpportunity.setId(Integer.parseInt(opportunity_id));
                createOpportunity.setName(selectOpportunity.getName());
                createOpportunity.setStatus(selectOpportunity.getStatus());
                createOpportunity.setAmount(String.valueOf(selectOpportunity.getAmount()));
                selectProbability=selectOpportunity.getProbability() + "%";
                createOpportunity.setProbability(selectOpportunity.getProbability());
                createOpportunity.setSource(selectOpportunity.getSource());
                createOpportunity.setNotes(selectOpportunity.getNotes());
                createOpportunity.setNextStep(selectOpportunity.getNextStep());
                createOpportunity.setModfiedDate(serverDate);
                createOpportunity.setOppStageDetails(selectOpportunity.getOppStageDetails());
                createOpportunity.setContactDetails(selectOpportunity.getContactDetails());
                createOpportunity.setSubContactDetails(selectOpportunity.getSubContactDetails());
                createOpportunity.setProductDetails(selectOpportunity.getProductDetails());
                
                createOpportunity.setOwnerDetails(selectOpportunity.getOwnerDetails());
                createOpportunity.setOppTypeDetails(selectOpportunity.getOppTypeDetails());
                createOpportunity.setBusinessUnitDetails(selectOpportunity.getBusinessUnitDetails());

                company_id = String.valueOf(selectOpportunity.getContactDetails().getId());
                txt_name.setText(createOpportunity.getName());
                txt_company_name.setText(createOpportunity.getContactDetails().getName());
                txt_contact.setText(createOpportunity.getSubContactDetails().getContactFullName());
                txt_amount.setText(String.valueOf(createOpportunity.getAmount()));
                txt_stage.setText(createOpportunity.getOppStageDetails().getStage());
                probability=String.valueOf(createOpportunity.getProbability());
                if(probability.equals("0")){
                    int weight=Integer.parseInt(probability);
                    String amount=txt_amount.getText().toString();
                    if(!TextUtils.isEmpty(amount)){
                        double value=Double.parseDouble(amount);
                        double total= value*weight/100;
                        DecimalFormat df=new DecimalFormat("#.##");
                        txt_revenue.setText(String.format("%s",df.format(total)));
                    }
                    else {
                        txt_revenue.setText("0.00"); }
                }
                else if(probability.equals("100")){
                    int weight=Integer.parseInt(probability);
                    String amount=txt_amount.getText().toString();
                    if(!TextUtils.isEmpty(amount)){
                        double value=Double.parseDouble(amount);
                        double total= value*weight/100;
                        DecimalFormat df=new DecimalFormat("#.##");
                        txt_revenue.setText(String.format("%s",df.format(total)));
                    }
                    else {
                        txt_revenue.setText("0.00"); }
                }
                else {
                    int weight=Integer.parseInt(probability);
                    String amount=txt_amount.getText().toString();
                    if(!TextUtils.isEmpty(amount)){
                        double value=Double.parseDouble(amount);
                        double total= value*weight/100;
                        DecimalFormat df=new DecimalFormat("#.##");
                        txt_revenue.setText(String.format("%s",df.format(total)));
                    }
                    else {
                        txt_revenue.setText("0.00");
                    }
                }
                String probability = createOpportunity.getProbability() + "%";
                txt_probability.setText(probability);
                //set the default according to value
                txt_type.setText(createOpportunity.getOppTypeDetails().getName());
                search_orders_filters_business_unit.setText(createOpportunity.getBusinessUnitDetails().getName());
                search_orders_filters_product.setText(createOpportunity.getProductDetails().getName());
                txt_source.setText(createOpportunity.getSource());
                search_activities_filters_from_date.setText(createOpportunity.getCloseDate());
                search_orders_filters_sales_rep.setText(createOpportunity.getAssignedTODetails().getName());
                txt_step.setText(createOpportunity.getNextStep());
                txt_created_by.setText(createOpportunity.getOwnerDetails().getName());
                txt_create_date.setText(createOpportunity.getCreatedDate());
                opp_description.setText(createOpportunity.getNotes());

                if (status.equals("Won")) {
                    txt_status.setText(createOpportunity.getStatus());
                    txt_stage.setEnabled(false);
                    txt_probability.setEnabled(false);
                    txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));

                    didStageChanged(createOpportunity.getOppStageDetails());
                    createOpportunity.setOppLossReasonDetails(selectOpportunity.getOppLossReasonDetails());
                    didLossReasonChanged(createOpportunity.getOppLossReasonDetails());
                } else if (status.equals("Lost")) {

                    txt_status.setText(createOpportunity.getStatus());
                    txt_stage.setEnabled(false);
                    txt_probability.setEnabled(false);
                    txt_probability.setBackground(getResources().getDrawable(R.drawable.disable_background));

                    didStageChanged(createOpportunity.getOppStageDetails());
                    createOpportunity.setOppLossReasonDetails(selectOpportunity.getOppLossReasonDetails());
                    didLossReasonChanged(createOpportunity.getOppLossReasonDetails());
                } else {
                    txt_probability.setEnabled(true);
                    txt_probability.setBackground(getResources().getDrawable(R.drawable.background_white));

                    txt_stage.setEnabled(true);
                    createOpportunity.setStatus(status);
                    txt_status.setText(createOpportunity.getStatus());
                }
                // format output
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
