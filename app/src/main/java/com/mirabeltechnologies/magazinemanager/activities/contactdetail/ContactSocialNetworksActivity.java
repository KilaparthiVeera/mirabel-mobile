package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.WebViewActivity;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.Utility;

public class ContactSocialNetworksActivity extends AppCompatActivity implements View.OnClickListener {
    private Context activityContext;
    private MMTextView csni_website_url, csni_facebook_url, csni_linked_in_url, csni_twitter_url, csni_google_plus_url;
    private DetailContact selectedContactDetails = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_social_networks);

        try {
            activityContext = ContactSocialNetworksActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
            }

            csni_website_url = (MMTextView) findViewById(R.id.csni_website_url);
            csni_facebook_url = (MMTextView) findViewById(R.id.csni_facebook_url);
            csni_linked_in_url = (MMTextView) findViewById(R.id.csni_linked_in_url);
            csni_twitter_url = (MMTextView) findViewById(R.id.csni_twitter_url);
            csni_google_plus_url = (MMTextView) findViewById(R.id.csni_google_plus_url);

            csni_website_url.setOnClickListener(this);
            csni_facebook_url.setOnClickListener(this);
            csni_linked_in_url.setOnClickListener(this);
            csni_twitter_url.setOnClickListener(this);
            csni_google_plus_url.setOnClickListener(this);

            updateUIWithBundleValues();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void updateUIWithBundleValues() {
        if (selectedContactDetails != null) {
            csni_website_url.setText(Utility.getFormattedTextWithUnderscore(selectedContactDetails.getUrl()));
            csni_facebook_url.setText(Utility.getFormattedTextWithUnderscore(selectedContactDetails.getFacebookId()));
            csni_linked_in_url.setText(Utility.getFormattedTextWithUnderscore(selectedContactDetails.getLinkedIn()));
            csni_twitter_url.setText(Utility.getFormattedTextWithUnderscore(selectedContactDetails.getTwitterHandle()));
            csni_google_plus_url.setText(Utility.getFormattedTextWithUnderscore(selectedContactDetails.getGooglePlus()));
        }
    }

    public void loadURLInWebView(View view) {
        String url = ((MMTextView) view).getText().toString();

        if (url.length() > 0) {
            Intent intent = new Intent(activityContext, WebViewActivity.class);
            intent.putExtra(Constants.BUNDLE_LOADING_URL, url);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
        }
    }

    @Override
    public void onClick(View v) {
        loadURLInWebView(v);
    }
}
