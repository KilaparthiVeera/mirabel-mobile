package com.mirabeltechnologies.magazinemanager.activities;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.GlobalContent;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.Utility;

public class WebViewActivity extends Activity {
    private Context activityContext;
    private MMTextView titleText;
    private ProgressBar progressBar;
    private WebView webView;
    private ImageButton previous;
    private ImageButton next;
    private ImageButton resize;
    private boolean isFullScreen = false;
    private String loadingURL = null;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);

        try {
            activityContext = WebViewActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                loadingURL = bundle.getString(Constants.BUNDLE_LOADING_URL);

                if (!loadingURL.startsWith("http"))
                    loadingURL = "https://" + loadingURL;
            }

            titleText = findViewById(R.id.web_view_title);
            progressBar = findViewById(R.id.web_view_progress_bar);
            webView = findViewById(R.id.web_view);
            previous = findViewById(R.id.web_view_previous_button);
            next = findViewById(R.id.web_view_next_button);
            resize = findViewById(R.id.web_view_resize_button);

            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
                GlobalContent.orientation = Constants.OrientationType.PORTRAIT;
            } else {
                GlobalContent.orientation = Constants.OrientationType.LANDSCAPE;
            }

            progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.white), android.graphics.PorterDuff.Mode.SRC_IN);

            disable(previous);
            disable(next);

            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.setWebViewClient(new CustomWebClient());
            webView.loadUrl(loadingURL);
            webView.getSettings().setSupportZoom(true);
            webView.getSettings().setBuiltInZoomControls(true);

            isFullScreen = true;
            adjustSize(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disable(View v) {
        v.setAlpha(0.3f);
        v.setEnabled(false);
    }

    public void enable(View v) {
        v.setAlpha(1.0f);
        v.setEnabled(true);
    }

    public void previous(View v) {
        webView.goBack();

        if (!webView.canGoBack()) {
            disable(previous);
        }
    }

    public void next(View v) {
        webView.goForward();

        if (!webView.canGoForward()) {
            disable(next);
        }
    }

    public void reloadWebView(View v) {
        if (webView != null) {
            webView.reload();
        }
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
            GlobalContent.orientation = Constants.OrientationType.PORTRAIT;
        } else {
            GlobalContent.orientation = Constants.OrientationType.LANDSCAPE;
        }

        if (!isFullScreen) {
            isFullScreen = true;
            adjustSize(null);
        }
    }

    public void adjustSize(View v) {
        if (!Utility.isTabletDevice(activityContext)) {
            getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            resize.setImageResource(R.drawable.toolbar_icon_minimize);
            isFullScreen = true;
        } else {
            if (isFullScreen) {
                isFullScreen = false;

                WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
                Display display = wm.getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);

                int width = size.x;
                int height = size.y;

                getWindow().setGravity(Gravity.CENTER);

                if (GlobalContent.orientation == Constants.OrientationType.PORTRAIT) {
                    getWindow().setLayout((int) (width * 0.7), (int) (height * 0.6));
                } else {
                    getWindow().setLayout((int) (width * 0.7), (int) (height * 0.9));
                }

                resize.setImageResource(R.drawable.toolbar_icon_maximize);
            } else {
                getWindow().setLayout(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
                resize.setImageResource(R.drawable.toolbar_icon_minimize);
                isFullScreen = true;
            }
        }
    }

    public void onDone(View v) {
        finish();
    }

    @Override
    public void finish() {
        super.finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public class CustomWebClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            showProgressBar();
            view.loadUrl(url);

            return true;
        }

        @TargetApi(Build.VERSION_CODES.N)
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
            return shouldOverrideUrlLoading(view, request.getUrl().toString());
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);

            if (webView.canGoBack()) {
                enable(previous);
            } else {
                disable(previous);
            }

            if (webView.canGoForward()) {
                enable(next);
            } else {
                disable(next);
            }

            titleText.setText("Loading...");
            showProgressBar();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            hideProgressBar();
            titleText.setText(view.getTitle());
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            // Handle the error
            super.onReceivedError(view, errorCode, description, failingUrl);
        }

        @TargetApi(android.os.Build.VERSION_CODES.M)
        @Override
        public void onReceivedError(WebView view, WebResourceRequest req, WebResourceError rerr) {
            // Redirect to deprecated method, so you can use it in all SDK versions
            onReceivedError(view, rerr.getErrorCode(), rerr.getDescription().toString(), req.getUrl().toString());
        }
    }

    public void showProgressBar() {
        if (progressBar != null) {
            if (!progressBar.isShown()) {
                progressBar.setVisibility(View.VISIBLE);
                progressBar.animate();
            }
        }
    }

    public void hideProgressBar() {
        if (progressBar != null) {
            if (progressBar.isShown()) {
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.clearAnimation();
            }
        }
    }
}
