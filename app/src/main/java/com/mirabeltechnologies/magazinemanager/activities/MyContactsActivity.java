package com.mirabeltechnologies.magazinemanager.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.AdvSearchSuggestionsAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.MyContactsAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.MySaveListAdpter;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.ContactSearchFilter;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SaveList;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.DelayAutoCompleteTextView;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactsAdvSearchListener;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyContactsActivity extends ListViewBaseActivity implements ContactsAdvSearchListener, PhoneCallListener, AlertDialogSelectionListener {
    public static final String TAG = MyContactsActivity.class.getSimpleName();

    private LinearLayout adv_search_header_layout;
    private DelayAutoCompleteTextView adv_search_company_name, adv_search_name, adv_search_email, adv_search_rep_name;
    private Drawable downArrow;
    private AdvSearchSuggestionsAdapter advSearchSuggestionsAdapter;
    private MyContactsAdapter myContactsAdapter;
    private MySaveListAdpter saveListAdpter;
    private ContactsModel contactsModel;
    private CommonModel commonModel;
    private int currentOperation = 0; // 0 - My Contacts & 1 - Advanced Search
    private ContactSearchFilter searchFilter;
    private ArrayList<RepData> allRepsList = null;
    private RepData currentlySelectedRepData = null;
    private boolean isLoggedInRepExistsInAllRepsList = false, isLimitCustomerSearchByRepSelected = false;
    private String customerId, phoneNumber, extension;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_contacts);

        // In order to hide keyboard when this screen appears because of AutoCompleteTextView.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        try {
            activityContext = MyContactsActivity.this;

            // initializing super class variables
            currentActivity = this;
            requestFrom = Constants.RequestFrom.MY_CONTACTS_PAGE;

            contactsModel = new ContactsModel(activityContext, this, requestFrom);
            commonModel = new CommonModel(activityContext, this, requestFrom);

            isLimitCustomerSearchByRepSelected = sharedPreferences.getBoolean(Constants.SP_LIMIT_CUSTOMER_SEARCH_BY_REP);

            searchFilter = new ContactSearchFilter();

            advSearchSuggestionsAdapter = new AdvSearchSuggestionsAdapter(activityContext, this, encryptedClientKey);

            downArrow = ContextCompat.getDrawable(activityContext, R.drawable.down_arrow_gray);

            adv_search_header_layout = findViewById(R.id.adv_search_header_layout);

            adv_search_company_name = findViewById(R.id.adv_search_company_name);
            adv_search_company_name.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            adv_search_company_name.setAdapter(advSearchSuggestionsAdapter);
            adv_search_company_name.setLoadingIndicator(findViewById(R.id.adv_search_company_name_loading_indicator));

            adv_search_company_name.setOnItemClickListener((adapterView, view, position, id) -> {
                CompanyData selectedCompany = (CompanyData) adapterView.getItemAtPosition(position);
                adv_search_company_name.setAdapter(null); // to stop filtering after selecting row from drop down
                adv_search_company_name.setText(selectedCompany.getName());
                adv_search_company_name.setAdapter(advSearchSuggestionsAdapter);
                adv_search_company_name.clearFocus();

                searchFilter.setCompany(selectedCompany.getName());

                hideKeyboard(adv_search_company_name);
            });

            adv_search_company_name.setOnFocusChangeListener((view, hasFocus) -> {
                if (hasFocus) {
                    advSearchSuggestionsAdapter.setTypeOfFilter(0);
                    adv_search_company_name.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                } else {
                    adv_search_company_name.setCompoundDrawablesWithIntrinsicBounds(null, null, downArrow, null);
                }
            });

            adv_search_name = findViewById(R.id.adv_search_name);
            adv_search_name.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            adv_search_name.setAdapter(advSearchSuggestionsAdapter);
            adv_search_name.setLoadingIndicator(findViewById(R.id.adv_search_name_loading_indicator));

            adv_search_name.setOnItemClickListener((adapterView, view, position, id) -> {
                String selectedEmployeeName = adapterView.getItemAtPosition(position).toString();
                adv_search_name.setAdapter(null); // to stop filtering after selecting row from drop down
                adv_search_name.setText(selectedEmployeeName);
                adv_search_name.setAdapter(advSearchSuggestionsAdapter);
                adv_search_name.clearFocus();

                searchFilter.setName(selectedEmployeeName);

                hideKeyboard(adv_search_name);
            });

            adv_search_name.setOnFocusChangeListener((view, hasFocus) -> {
                if (hasFocus) {
                    advSearchSuggestionsAdapter.setTypeOfFilter(1);
                    adv_search_name.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                } else {
                    adv_search_name.setCompoundDrawablesWithIntrinsicBounds(null, null, downArrow, null);
                }
            });


            adv_search_email = findViewById(R.id.adv_search_email);
            adv_search_email.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            adv_search_email.setAdapter(advSearchSuggestionsAdapter);
            adv_search_email.setLoadingIndicator(findViewById(R.id.adv_search_email_loading_indicator));

            adv_search_email.setOnItemClickListener((adapterView, view, position, id) -> {
                String selectedEmail = adapterView.getItemAtPosition(position).toString();
                adv_search_email.setAdapter(null); // to stop filtering after selecting row from drop down
                adv_search_email.setText(selectedEmail);
                adv_search_email.setAdapter(advSearchSuggestionsAdapter);
                adv_search_email.clearFocus();

                searchFilter.setEmail(selectedEmail);

                hideKeyboard(adv_search_email);
            });

            adv_search_email.setOnFocusChangeListener((view, hasFocus) -> {
                if (hasFocus) {
                    advSearchSuggestionsAdapter.setTypeOfFilter(2);
                    adv_search_email.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                } else {
                    adv_search_email.setCompoundDrawablesWithIntrinsicBounds(null, null, downArrow, null);
                }
            });

            adv_search_rep_name = findViewById(R.id.adv_search_rep_name);
            adv_search_rep_name.setText(searchFilter.getRepName());
            adv_search_rep_name.setOnClickListener(view -> showAllRepsData(view));

            // In order to work on Amazon kindle device also, we are binding click events programmatically.
            MMButton reset_button = findViewById(R.id.adv_search_reset_button);
            reset_button.setOnClickListener(v -> resetAdvSearchAutoCompleteTextViews(v));

            MMButton search_button = findViewById(R.id.adv_search_search_button);
            search_button.setOnClickListener(v -> applySearchForSelectedValues(v));
            list_view =  findViewById(R.id.contacts_list_view);
            myContactsAdapter = new MyContactsAdapter(activityContext, this);
            saveListAdpter= new MySaveListAdpter(activityContext);
            list_view.setAdapter(myContactsAdapter);

            list_view.setOnItemClickListener((adapterView, view, position, id) -> {
                if(currentOperation==2){
                    Bundle bundle = new Bundle();
                    bundle.putString("display", globalContent.getSaveLists().get(position).getDisplay());
                    bundle.putString("value", globalContent.getSaveLists().get(position).getValue());
                    Intent openContactDetailsIntent = new Intent(activityContext, SaveListActivity.class);
                    openContactDetailsIntent.putExtras(bundle);
                    openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(openContactDetailsIntent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                   // Toast.makeText(MyContactsActivity.this, globalContent.getSaveLists().get(position).getValue(), Toast.LENGTH_SHORT).show();

                }else{
                    openContactDetailPage(globalContent.getMyContacts().get(position), position);

                }
            });

            page_number_display = findViewById(R.id.my_contacts_page_number_display);
            left_nav_arrow = findViewById(R.id.my_contacts_left_nav_arrow);
            right_nav_arrow = findViewById(R.id.my_contacts_right_nav_arrow);

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE));
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));

            resetValues();
            getContacts(currentPageNumber);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void toggleWidgetInMyContacts(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Checking which radio button was clicked
        switch (view.getId()) {
            case R.id.rb_my_contacts:
                if (checked)
                    currentOperation = 0;
                list_view.setAdapter(myContactsAdapter);
                break;
            case R.id.rb_search:
                if (checked)
                    currentOperation = 1;
                list_view.setAdapter(myContactsAdapter);
                break;
            case R.id.rb_save_list:
                if (checked)
                    currentOperation = 2;
                  list_view.setAdapter(saveListAdpter);
                break;
        }

        resetValues();
        if (currentOperation == 0) {
            showMyContacts();
        }
        else if(currentOperation==1)  {
            showAdvSearch();
        }
        else if(currentOperation==2)  {
            resetsaveList();
            showSaveList();
        }
    }

    private void showSaveList() {
        adv_search_header_layout.setVisibility(View.GONE);
        getContacts(currentPageNumber);
    }

    public void showMyContacts() {
        adv_search_header_layout.setVisibility(View.GONE);
        getContacts(currentPageNumber);
    }

    public void showAdvSearch() {
        adv_search_header_layout.setVisibility(View.VISIBLE);

        if (allRepsList == null) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            commonModel.getAllRepsData(encryptedClientKey, getDTTicks(), false, 0);
        }

        totalNoOfRecords = 0;
        totalNoOfPages = 0;
        updateFooterPageLabels(0);
        resetAdvSearchAutoCompleteTextViews(null);
    }

    @Override
    public void updateSearchKeyword(String keyword, int typeOfFilter) {
        if (typeOfFilter == 0) {
            searchFilter.setCompany(keyword);
        } else if (typeOfFilter == 1) {
            searchFilter.setName(keyword);
        } else if (typeOfFilter == 2) {
            searchFilter.setEmail(keyword);
        }
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;

        currentlySelectedRepData = allRepsList.get(0);

        isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));

        if (searchFilter == null)
            searchFilter = new ContactSearchFilter();

        if (isLimitCustomerSearchByRepSelected) {
            if (isLoggedInRepExistsInAllRepsList) {
                searchFilter.setRepId(String.valueOf(loggedInRepId));
                searchFilter.setRepName(loggedInRepName);
            } else {
                searchFilter.setRepId("-1");
                searchFilter.setRepName("");
            }
        } else {
            searchFilter.setRepId("-1");
            searchFilter.setRepName("All Reps");
        }

        adv_search_rep_name.setText(searchFilter.getRepName());

        MMProgressDialog.hideProgressDialog();
    }

    public void showAllRepsData(View view) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent allRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.REP);
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchFilter.getRepId());
            allRepsListIntent.putExtras(bundle);
            startActivity(allRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.REP) {
                        didRepChanged(selectedObj);
                    }
                } else if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    String currentContactOperation = bundle.getString(Constants.BUNDLE_CURRENT_OPERATION);

                    if (currentContactOperation.equals(Constants.CURRENT_OPERATION_EDIT_CONTACT)) {
                        // On contact edit, we will remove contacts in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like contact add / delete.
                        reloadContactsOnContactEdit();
                    } else {
                        // On new contact add (primary / sub contact), we are refreshing all records by resetting them if we are in my contacts page and we are not doing any thing if we are in search page.
                        if (currentOperation == 0) {
                            resetValues();
                            getContacts(currentPageNumber);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Object selectedObj) {
        currentlySelectedRepData = (RepData) selectedObj;

        searchFilter.setRepId(currentlySelectedRepData.getId());
        searchFilter.setRepName(currentlySelectedRepData.getName());

        adv_search_rep_name.setText(searchFilter.getRepName());
    }

    public void applySearchForSelectedValues(View view) {

        if (searchFilter.getCompany().length() > 0 || searchFilter.getName().length() > 0 || searchFilter.getEmail().length() > 0 || searchFilter.getRepId().length() > 0) {
            hideKeyboard(adv_search_company_name);
            resetValues();
            getContacts(currentPageNumber);
        } else {
            displayToast(getResources().getString(R.string.select_search_criteria));
        }
    }

    public void resetAdvSearchAutoCompleteTextViews(View view) {
        resetValues();

        searchFilter = new ContactSearchFilter();

        adv_search_company_name.setText(searchFilter.getCompany());
        adv_search_name.setText(searchFilter.getName());
        adv_search_email.setText(searchFilter.getEmail());

        if (view == null) {
            if (isLimitCustomerSearchByRepSelected) {
                if (isLoggedInRepExistsInAllRepsList) {
                    searchFilter.setRepId(String.valueOf(loggedInRepId));
                    searchFilter.setRepName(loggedInRepName);
                } else {
                    searchFilter.setRepId("-1");
                    searchFilter.setRepName("");
                }
            } else {
                searchFilter.setRepId("-1");
                searchFilter.setRepName("All Reps");
            }

            adv_search_rep_name.setText(searchFilter.getRepName());
        } else {
            adv_search_rep_name.setText("");
        }
    }


    private void resetsaveList() {
        super.resetValues();
        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        globalContent.setMySaveList(null);
        saveListAdpter.notifyDataSetChanged();
    }

    public void resetValues() {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        globalContent.setMyContacts(null);
        myContactsAdapter.notifyDataSetChanged();
    }

    public void getContacts(int pageNumber) {
        if (currentOperation == 0)
            getMyContacts(pageNumber);
        else if (currentOperation == 1)
            getAdvanceSearchContacts(pageNumber);
        else if(currentOperation==2){
            getSaveList(pageNumber);
        }
    }

    private void getSaveList(int pageNumber) {

        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        currentPageNumber = pageNumber;
        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);
            contactsModel.getAllSaveList(encryptedId,sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN),currentPageNumber);
            isLoading = true;
        }
    }

    public void getMyContacts(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            contactsModel.getAllMyContacts(encryptedClientKey, getDTTicks(), currentPageNumber);
            isLoading = true;
        }
    }

    public void getAdvanceSearchContacts(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            contactsModel.getAdvancedSearchContacts(encryptedClientKey, getDTTicks(), searchFilter, currentPageNumber);
            isLoading = true;
        }
    }

    public void updateSaveList(  List<SaveList> saveContactsList, int noOfAllMyContacts) {
        if (currentPageNumber == 1 && (saveContactsList == null || saveContactsList.isEmpty())) {


            resetsaveList();
            noRecordsFound();
        }
        else {
            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + saveContactsList.size();

            if (noOfAllMyContacts != totalNoOfRecords) {
                totalNoOfRecords = noOfAllMyContacts;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.PAGE_SIZE_FLOAT);

                isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }
                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getContacts(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setMySaveList(saveContactsList);
            saveListAdpter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;

    }

    public void updateContactsList(final List<Contact> contactsList, int noOfContacts) {
        if (currentPageNumber == 1 && (contactsList == null || contactsList.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + contactsList.size();

            if (noOfContacts != totalNoOfRecords) {
                totalNoOfRecords = noOfContacts;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.PAGE_SIZE_FLOAT);

                isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getContacts(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setMyContacts(contactsList);
            myContactsAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            isAllRecordsLoaded = currentPageNumber == totalNoOfPages;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    public void reloadContactsOnContactEdit() {
        int editedContactPositionInList = sharedPreferences.getInt(Constants.SP_MY_CONTACTS_SELECTED_CONTACT_INDEX);
        int editedContactPageNo = (int) Math.ceil((editedContactPositionInList + 1) / Constants.PAGE_SIZE_FLOAT);
        int indexOfPageNumber = alreadyLoadedPageNumbersList.indexOf(editedContactPageNo);

        // We are removing all page numbers (starting from this page number) from already loaded page no's list in order to reload them.
        alreadyLoadedPageNumbersList.subList(indexOfPageNumber, alreadyLoadedPageNumbersList.size()).clear();

        alreadyLoadedNoOfRecords = (editedContactPageNo - 1) * Constants.PAGE_SIZE_INT;

        // We are removing all contacts (starting from this page) from myContacts & we will reload them.
        globalContent.getMyContacts().subList(alreadyLoadedNoOfRecords, globalContent.getMyContacts().size()).clear();

        getContacts(editedContactPageNo);
    }

    public void openContactDetailPage(Contact contact, int contactIndex) {
        if (contact.getCanViewEmployeeId().equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            // We are saving selected contact index in order to reload contact specific page after editing of that contact.
            sharedPreferences.putInt(Constants.SP_MY_CONTACTS_SELECTED_CONTACT_INDEX, contactIndex);

            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, contact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showCreateContactPage(View view) {

        Intent showCreateContactIntent = new Intent(activityContext, CreateContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putString(Constants.BUNDLE_CURRENT_OPERATION, Constants.CURRENT_OPERATION_CREATE_PRIMARY_CONTACT);
        bundle.putBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT, true);
        showCreateContactIntent.putExtras(bundle);
        startActivity(showCreateContactIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    public void takeConfirmationToMakePhoneCall(String customerId, String phoneNumber, String extension) {
        this.customerId = customerId;
        this.phoneNumber = phoneNumber;
        this.extension = extension;

        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                checkPermissionsToMakePhoneCall();
            }
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);


        if (requestCode == Constants.MAKE_PHONE_CALL_REQUEST_CODE) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkPermissionsToMakePhoneCall();
            } else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                } else {
                    displayToast(getResources().getString(R.string.phone_call_permission_denied));
                }
            }
        }

  /*      switch (requestCode) {
            case Constants.MAKE_PHONE_CALL_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsToMakePhoneCall();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.phone_call_permission_denied));
                    }
                }
                break;
        }*/
    }

    public void checkPermissionsToMakePhoneCall() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
            makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, phoneNumber, extension, false);
        }
    }



}
