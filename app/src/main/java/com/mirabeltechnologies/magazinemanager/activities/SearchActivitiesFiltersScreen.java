package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.AdvSearchSuggestionsAdapter;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SearchActivitiesFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.DelayAutoCompleteTextView;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactsAdvSearchListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SearchActivitiesFiltersScreen extends BaseActivity implements ContactsAdvSearchListener, AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {
    public static final String TAG = SearchActivitiesFiltersScreen.class.getSimpleName();

    private Constants.RequestFrom requestFrom, requestCameFrom;
    private String currentActivityType;
    private LinearLayout search_activities_filters_activity_type_layout;
    private MMTextView search_activities_filters_screen_title, search_activities_filters_from_date, search_activities_filters_to_date, search_activities_filters_assigned_to, search_activities_filters_created_by, search_activities_filters_department, search_activities_filters_activity_type;
    private CheckBox activity_type_cb_all, activity_type_cb_notes, activity_type_cb_calls, activity_type_cb_meetings, activity_type_cb_email, activity_type_cb_mass_email,activity_type_cb_check;
    private Spinner search_activities_filters_date_types_spinner, search_activities_filters_created_by_assigned_by_spinner;
    private DelayAutoCompleteTextView search_activities_filters_company_name;
    private MMEditText search_activities_filters_keyword;
    private MMButton search_activities_filters_reset_button, search_activities_filters_search_button;
    private AdvSearchSuggestionsAdapter companyNameSuggestionsAdapter;
    private SearchActivitiesFilter searchFilter;
    private CommonModel commonModel;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<MasterData> allDepartments = null, allActivityTypes = null;
    private ArrayList<String> dateTypesMapKeyValues, createdByAssignedByArrayList;
    private Map<String, String> dateTypesMap;
    private SpinnerAdapter actionTypesSpinnerAdapter, dateTypesSpinnerAdapter, createdByAssignedBySpinnerAdapter;
    private String currentDate, preSelectedDate;
    private ArrayList<Integer> selectedActivityTypes = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activities_filters_screen);

        // In order to hide keyboard when this screen appears because of AutoCompleteTextView.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        try {
            activityContext = SearchActivitiesFiltersScreen.this;

            requestFrom = Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                currentActivityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
                requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
                searchFilter = (SearchActivitiesFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT);
            }

            // When we came from dashboard left menu, we are not sending search filter object so we don't have it.
            if (searchFilter == null)
                searchFilter = new SearchActivitiesFilter(currentActivityType);

            commonModel = new CommonModel(activityContext, this, requestFrom);
            companyNameSuggestionsAdapter = new AdvSearchSuggestionsAdapter(activityContext, this, encryptedClientKey);
            currentDate = Utility.formatDate(new Date(), Constants.SELECTED_DATE_DISPLAY_FORMAT);

            dateTypesMap = new LinkedHashMap<>();
            dateTypesMap.put("Date Created", "lstDateCreated");
            dateTypesMap.put("Date Scheduled", "lstDateScheduled");
            dateTypesMap.put("Date Completed", "lstDateCompleted");

            dateTypesMapKeyValues = new ArrayList<>(dateTypesMap.keySet());

            createdByAssignedByArrayList = new ArrayList<>();
            createdByAssignedByArrayList.add("Created By");
            createdByAssignedByArrayList.add("Assigned By");

            // Initializing UI References
            search_activities_filters_activity_type_layout = (LinearLayout) findViewById(R.id.search_activities_filters_activity_type_layout);
            search_activities_filters_screen_title = (MMTextView) findViewById(R.id.search_activities_filters_screen_title);
            activity_type_cb_all = (CheckBox) findViewById(R.id.activity_type_cb_all);
            activity_type_cb_notes = (CheckBox) findViewById(R.id.activity_type_cb_notes);
            activity_type_cb_calls = (CheckBox) findViewById(R.id.activity_type_cb_calls);
            activity_type_cb_meetings = (CheckBox) findViewById(R.id.activity_type_cb_meetings);
            activity_type_cb_email = (CheckBox) findViewById(R.id.activity_type_cb_email);
            activity_type_cb_mass_email = (CheckBox) findViewById(R.id.activity_type_cb_mass_email);
            activity_type_cb_check= (CheckBox) findViewById(R.id.activity_type_cb_check);
            search_activities_filters_date_types_spinner = (Spinner) findViewById(R.id.search_activities_filters_date_types_spinner);
            search_activities_filters_from_date = (MMTextView) findViewById(R.id.search_activities_filters_from_date);
            search_activities_filters_to_date = (MMTextView) findViewById(R.id.search_activities_filters_to_date);
            search_activities_filters_keyword = (MMEditText) findViewById(R.id.search_activities_filters_keyword);
            search_activities_filters_assigned_to = (MMTextView) findViewById(R.id.search_activities_filters_assigned_to);
            search_activities_filters_created_by_assigned_by_spinner = (Spinner) findViewById(R.id.search_activities_filters_created_by_assigned_by_spinner);
            search_activities_filters_created_by = (MMTextView) findViewById(R.id.search_activities_filters_created_by);
            search_activities_filters_department = (MMTextView) findViewById(R.id.search_activities_filters_department);
            search_activities_filters_activity_type = (MMTextView) findViewById(R.id.search_activities_filters_activity_type);
            search_activities_filters_reset_button = (MMButton) findViewById(R.id.search_activities_filters_reset_button);
            search_activities_filters_search_button = (MMButton) findViewById(R.id.search_activities_filters_search_button);

            if (currentActivityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                search_activities_filters_screen_title.setText("Search Notes");
                search_activities_filters_activity_type_layout.setVisibility(View.GONE);
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                search_activities_filters_screen_title.setText("Search Calls");
                search_activities_filters_activity_type_layout.setVisibility(View.GONE);
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                search_activities_filters_screen_title.setText("Search Meetings");
                search_activities_filters_activity_type_layout.setVisibility(View.GONE);
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
                search_activities_filters_screen_title.setText("Notes/Activity Search");
                search_activities_filters_activity_type_layout.setVisibility(View.VISIBLE);
            }

            activity_type_cb_all.setOnCheckedChangeListener(this);
            activity_type_cb_notes.setOnCheckedChangeListener(this);
            activity_type_cb_calls.setOnCheckedChangeListener(this);
            activity_type_cb_meetings.setOnCheckedChangeListener(this);
            activity_type_cb_email.setOnCheckedChangeListener(this);
            activity_type_cb_mass_email.setOnCheckedChangeListener(this);
            activity_type_cb_check.setOnCheckedChangeListener(this);

            dateTypesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, dateTypesMapKeyValues);
            dateTypesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_activities_filters_date_types_spinner.setAdapter(dateTypesSpinnerAdapter);
            search_activities_filters_date_types_spinner.setSelection(0, false);
            search_activities_filters_date_types_spinner.setOnItemSelectedListener(this);

            createdByAssignedBySpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, createdByAssignedByArrayList);
            createdByAssignedBySpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_activities_filters_created_by_assigned_by_spinner.setAdapter(createdByAssignedBySpinnerAdapter);
            search_activities_filters_created_by_assigned_by_spinner.setSelection(0, false);
            search_activities_filters_created_by_assigned_by_spinner.setOnItemSelectedListener(this);

            search_activities_filters_company_name = (DelayAutoCompleteTextView) findViewById(R.id.search_activities_filters_company_name);
            search_activities_filters_company_name.setThreshold(3); // minimum number of characters the user has to type in the edit box before the drop down list is shown
            search_activities_filters_company_name.setAdapter(companyNameSuggestionsAdapter);
            search_activities_filters_company_name.setLoadingIndicator((android.widget.ProgressBar) findViewById(R.id.search_activities_filters_company_name_loading_indicator));

            search_activities_filters_company_name.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    CompanyData selectedCompany = (CompanyData) adapterView.getItemAtPosition(position);
                    search_activities_filters_company_name.setAdapter(null); // to stop filtering after selecting row from drop down
                    search_activities_filters_company_name.setText(selectedCompany.getName());
                    search_activities_filters_company_name.setAdapter(companyNameSuggestionsAdapter);
                    search_activities_filters_company_name.clearFocus();

                    searchFilter.setCompanyName(selectedCompany.getName());
                    searchFilter.setCompanyId(selectedCompany.getId());

                    hideKeyboard(search_activities_filters_company_name);
                }
            });

            search_activities_filters_company_name.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean hasFocus) {
                    if (hasFocus) {
                        companyNameSuggestionsAdapter.setTypeOfFilter(0);
                        search_activities_filters_company_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    } else {
                        search_activities_filters_company_name.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.down_arrow_gray, 0);
                    }
                }
            });

            search_activities_filters_from_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker(v, Constants.SelectionType.FROM_DATE);
                }
            });

            search_activities_filters_to_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker(v, Constants.SelectionType.TO_DATE);
                }
            });

            search_activities_filters_assigned_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_TO);
                }
            });

            search_activities_filters_created_by.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY);
                }
            });

            search_activities_filters_department.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllDepartments(v);
                }
            });

            search_activities_filters_activity_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllActivityTypes(v);
                }
            });

            search_activities_filters_reset_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetSearchActivitiesFilters();
                }
            });

            search_activities_filters_search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    applySearchActivitiesFilters();
                }
            });

            loadDataFromServer();

            // Setting default values
            /*if (requestCameFrom == Constants.RequestFrom.DASHBOARD && searchFilter != null) {
                searchFilter.setAssignedToRepId(String.valueOf(loggedInRepId));
                searchFilter.setAssignedToRepName(loggedInRepName);
            }*/

            updateUIWithLatestValues(searchFilter);

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ACTIVITIES_FILTERS_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void updateSearchKeyword(String keyword, int typeOfFilter) {
        if (typeOfFilter == 0) {
            searchFilter.setCompanyName(keyword);
            searchFilter.setCompanyId("");
        }
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;

        MMProgressDialog.hideProgressDialog();
    }

    public void showAllRepsList(View view, Constants.SelectionType selectionType) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            if (selectionType == Constants.SelectionType.ASSIGNED_TO)
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchFilter.getAssignedToRepId());
            else
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchFilter.getCreatedByOrAssignedByRepId());

            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllDepartments(View view) {
        if (allDepartments == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_DEPARTMENT);
        } else {
            Intent allDepartmentsIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allDepartmentsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allDepartments);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.DEPARTMENT);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchFilter.getDepartmentId());
            allDepartmentsIntent.putExtras(bundle);
            startActivity(allDepartmentsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showAllActivityTypes(View view) {
        if (allActivityTypes == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_ACTIVITY_TYPES);
        } else {
            Intent allActivityTypesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allActivityTypesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allActivityTypes);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.ACTIVITY_TYPE);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchFilter.getActivityTypeId());
            allActivityTypesIntent.putExtras(bundle);
            startActivity(allActivityTypesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        if (fieldType.equals(Constants.GET_MASTER_DATA_DEPARTMENT)) {
            allDepartments = new ArrayList<>();
            allDepartments.add(new MasterData("All Departments", "-1"));
            allDepartments.addAll(masterData);
            MMProgressDialog.hideProgressDialog();
            showAllDepartments(null);
        } else if (fieldType.equals(Constants.GET_MASTER_DATA_ACTIVITY_TYPES)) {
            allActivityTypes = (ArrayList) masterData;
            MMProgressDialog.hideProgressDialog();
            showAllActivityTypes(null);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ACTIVITIES_FILTERS_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.DEPARTMENT) {
                        didDepartmentChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.ACTIVITY_TYPE) {
                        didActivityTypeChanged((MasterData) selectedObj);
                    } else {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.ASSIGNED_TO) {
            searchFilter.setAssignedToRepId(selectedRep.getId());
            searchFilter.setAssignedToRepName(selectedRep.getName());

            search_activities_filters_assigned_to.setText(searchFilter.getAssignedToRepName());
        } else if (selectionType == Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY) {
            searchFilter.setCreatedByOrAssignedByRepId(selectedRep.getId());
            searchFilter.setCreatedByOrAssignedByRepName(selectedRep.getName());

            search_activities_filters_created_by.setText(searchFilter.getCreatedByOrAssignedByRepName());
        }
    }

    public void didDepartmentChanged(MasterData selectedDepartment) {
        searchFilter.setDepartment(selectedDepartment.getName());
        searchFilter.setDepartmentId(selectedDepartment.getId());

        search_activities_filters_department.setText(searchFilter.getDepartment());
    }

    public void didActivityTypeChanged(MasterData selectedActivityType) {
        searchFilter.setActivityType(selectedActivityType.getName());

        if (selectedActivityType.getId() == null)
            searchFilter.setActivityTypeId("-1");
        else
            searchFilter.setActivityTypeId(selectedActivityType.getId());

        search_activities_filters_activity_type.setText(searchFilter.getActivityType());
    }

    /* AdapterView.OnItemSelectedListener Methods */
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            int spinnersTag = Integer.parseInt(parent.getTag().toString());

            if (spinnersTag == 2) { // Date Types Spinner
                String selectedDateType = dateTypesMapKeyValues.get(position);

                searchFilter.setDateType(dateTypesMap.get(selectedDateType));
                searchFilter.setDateTypeIndex(position);

            } else if (spinnersTag == 3) { // Created By or Assigned By Spinner
                String selectedChoice = createdByAssignedByArrayList.get(position);

                if (selectedChoice.equals("Created By"))
                    searchFilter.setIsCreatedByOrAssignedBy(0);
                else
                    searchFilter.setIsCreatedByOrAssignedBy(1);

                searchFilter.setIsCreatedByOrAssignedByIndex(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);
        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    public void showDatePicker(View view, final Constants.SelectionType selectionType) {
        try {
            if (selectionType == Constants.SelectionType.FROM_DATE)
                preSelectedDate = searchFilter.getFromDate();
            else if (selectionType == Constants.SelectionType.TO_DATE)
                preSelectedDate = searchFilter.getToDate();

            if (preSelectedDate.isEmpty()) {
                preSelectedDate = currentDate;
            }

            String[] dateValues = preSelectedDate.split("/");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, monthOfYear, dayOfMonth);

                    preSelectedDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);

                    if (selectionType == Constants.SelectionType.FROM_DATE) {
                        search_activities_filters_from_date.setText(preSelectedDate);
                        searchFilter.setFromDate(preSelectedDate);
                    } else if (selectionType == Constants.SelectionType.TO_DATE) {
                        search_activities_filters_to_date.setText(preSelectedDate);
                        searchFilter.setToDate(preSelectedDate);
                    }

                }
            }, Integer.parseInt(dateValues[2]), Integer.parseInt(dateValues[0]) - 1, Integer.parseInt(dateValues[1]));

            //datePickerDialog.setTitle("Select From Date");
            //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        try {
            int checkBoxTag = Integer.parseInt(buttonView.getTag().toString());

            switch (checkBoxTag) {
                case 1:
                    activity_type_cb_notes.setChecked(isChecked);
                    activity_type_cb_calls.setChecked(isChecked);
                    activity_type_cb_meetings.setChecked(isChecked);
                    activity_type_cb_email.setChecked(isChecked);
                    activity_type_cb_mass_email.setChecked(isChecked);
                    activity_type_cb_check.setChecked(isChecked);
                    break;
                case 2:
                    if (isChecked)
                        selectedActivityTypes.add(1);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(1)); // Java will treat 1 as index so we are converting it as object.
                    break;
                case 3:
                    if (isChecked)
                        selectedActivityTypes.add(2);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(2));
                    break;
                case 4:
                    if (isChecked)
                        selectedActivityTypes.add(3);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(3));
                    break;
                case 5:
                    if (isChecked)
                        selectedActivityTypes.add(5);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(5));
                    break;
                case 6:
                    if (isChecked)
                        selectedActivityTypes.add(6);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(6));
                    break;
                case 7:
                    if (isChecked)
                        selectedActivityTypes.add(7);
                    else
                        selectedActivityTypes.remove(Integer.valueOf(7));
            }

            if (selectedActivityTypes.size() == 6) {
                activity_type_cb_all.setOnCheckedChangeListener(null);
                activity_type_cb_all.setChecked(true);
                activity_type_cb_all.setOnCheckedChangeListener(this);
            } else {
                activity_type_cb_all.setOnCheckedChangeListener(null);
                activity_type_cb_all.setChecked(false);
                activity_type_cb_all.setOnCheckedChangeListener(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateUIWithLatestValues(SearchActivitiesFilter filter) {
        if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
            if (filter.getFilterOption().contains("1"))
                activity_type_cb_notes.setChecked(true);
            else
                activity_type_cb_notes.setChecked(false);

            if (filter.getFilterOption().contains("2"))
                activity_type_cb_calls.setChecked(true);
            else
                activity_type_cb_calls.setChecked(false);

            if (filter.getFilterOption().contains("3"))
                activity_type_cb_meetings.setChecked(true);
            else
                activity_type_cb_meetings.setChecked(false);

            if (filter.getFilterOption().contains("5"))
                activity_type_cb_email.setChecked(true);
            else
                activity_type_cb_email.setChecked(false);

            if (filter.getFilterOption().contains("6"))
                activity_type_cb_mass_email.setChecked(true);
            else
                activity_type_cb_mass_email.setChecked(false);
            if(filter.getFilterOption().contains("7"))
                activity_type_cb_check.setChecked(true);
            else
                activity_type_cb_check.setChecked(false);
        }

        search_activities_filters_company_name.setText(filter.getCompanyName());
        setSpinnerSelectionWithoutCallingListener(search_activities_filters_date_types_spinner, filter.getDateTypeIndex());
        search_activities_filters_from_date.setText(filter.getFromDate());
        search_activities_filters_to_date.setText(filter.getToDate());
        search_activities_filters_keyword.setText(filter.getKeyword());
        search_activities_filters_assigned_to.setText(filter.getAssignedToRepName());
        setSpinnerSelectionWithoutCallingListener(search_activities_filters_created_by_assigned_by_spinner, filter.getIsCreatedByOrAssignedByIndex());
        search_activities_filters_created_by.setText(filter.getCreatedByOrAssignedByRepName());
        search_activities_filters_department.setText(filter.getDepartment());
        search_activities_filters_activity_type.setText(filter.getActivityType());
    }

    public void resetSearchActivitiesFilters() {
        searchFilter = new SearchActivitiesFilter(currentActivityType);

        updateUIWithLatestValues(searchFilter);
    }

    public void applySearchActivitiesFilters() {
        try {
            searchFilter.setCompanyName(search_activities_filters_company_name.getText().toString().trim());
            searchFilter.setKeyword(search_activities_filters_keyword.getText().toString().trim());

            if (isAnyFilterApplied()) {
                if (isValidDateRange()) {
                    if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES) && selectedActivityTypes.isEmpty()) {
                        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Select atleast one activity.");
                    } else {
                        if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
                            String selectedActivities = "";
                            for (Integer activityType : selectedActivityTypes)
                                selectedActivities = selectedActivities + "," + activityType;

                            selectedActivities = selectedActivities.substring(1);
                            searchFilter.setFilterOption(selectedActivities);
                        }

                        Intent intent = null;
                        Bundle bundle = new Bundle();
                        bundle.putSerializable(Constants.BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT, searchFilter);

                        if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {
                            intent = new Intent(activityContext, SearchActivitiesScreen.class);
                            bundle.putSerializable(Constants.BUNDLE_ACTIVITY_TYPE, currentActivityType);
                            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(intent);
                            overridePendingTransition(R.anim.right_in, R.anim.left_out);
                        } else {
                            intent = new Intent();
                            intent.putExtras(bundle);
                            setResult(RESULT_OK, intent);
                            finish();
                            overridePendingTransition(R.anim.left_in, R.anim.right_out);
                        }
                    }
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), getResources().getString(R.string.greater_to_date));
                }
            } else {
                MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Select atleast one search criteria.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isAnyFilterApplied() {
        if (searchFilter.getCompanyName().length() >= 3) {
            return true;
        }

        if (searchFilter.getFromDate().length() > 0 && searchFilter.getToDate().length() > 0) {
            return true;
        }

        if (searchFilter.getKeyword().length() > 0) {
            return true;
        }

        if (searchFilter.getAssignedToRepId().length() > 0 && !searchFilter.getAssignedToRepId().equals("-1")) {
            return true;
        }

        if (searchFilter.getCreatedByOrAssignedByRepId().length() > 0 && !searchFilter.getCreatedByOrAssignedByRepId().equals("-1")) {
            return true;
        }

        if (searchFilter.getDepartmentId().length() > 0 && !searchFilter.getDepartmentId().equals("-1")) {
            return true;
        }

        if (searchFilter.getActivityTypeId().length() > 0 && !searchFilter.getActivityTypeId().equals("-1")) {
            return true;
        }

        return false;
    }

    public boolean isValidDateRange() {
        boolean isValid = true;

        try {
            if (searchFilter.getFromDate().length() > 0 && searchFilter.getToDate().length() > 0) {
                SimpleDateFormat formatter = new SimpleDateFormat(Constants.SELECTED_DATE_DISPLAY_FORMAT);
                Date fromDate = formatter.parse(searchFilter.getFromDate());
                Date toDate = formatter.parse(searchFilter.getToDate());

                if (fromDate.after(toDate)) {
                    isValid = false;
                }
            } else {
                isValid = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            isValid = false;
        }

        return isValid;
    }
}
