package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.RadioButton;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.adapters.ContactOrderItemsListAdapter;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrder;
import com.mirabeltechnologies.magazinemanager.beans.ContactOrderItem;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;

import java.util.ArrayList;
import java.util.List;

public class ContactOrderItemsListActivity extends BaseActivity {
    public static final String TAG = ContactOrderItemsListActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom, requestCameFrom;
    private MMTextView contact_order_items_list_title;
    private ListView list_view;
    private MMTextView coi_total_orders_count, coi_total_prod_charges_value, coi_total_barter_value, coi_total_gross_value, coi_total_net_value;
    private ContactsModel contactsModel;
    private String selectedCustomerId, selectedYear;
    private ContactOrder selectedCustomerOrder;
    private ContactOrderItemsListAdapter orderItemsListAdapter;
    private List<ContactOrderItem> orderItemsList, printOrders, digitalOrders;
    private int currentOperation = 0; // 0 - Print Orders & 1 - Digital Orders
    private int currentPageNumber, printTotalOrdersCount = 0, digitalTotalOrdersCount = 0;
    private float printTotalProdCharges = 0.0f, printTotalBarter = 0.0f, printTotalGross = 0.0f, printTotalNet = 0.0f, digitalTotalProdCharges = 0.0f, digitalTotalBarter = 0.0f, digitalTotalGross = 0.0f, digitalTotalNet = 0.0f;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_order_items_list);

        try {
            activityContext = ContactOrderItemsListActivity.this;

            // initializing super class variables
            requestFrom = Constants.RequestFrom.CONTACT_ORDERS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
                selectedCustomerOrder = (ContactOrder) bundle.getSerializable(Constants.BUNDLE_SELECTED_ORDER);
                requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            }

            if (selectedCustomerOrder != null)
                selectedYear = selectedCustomerOrder.getYear();

            contactsModel = new ContactsModel(activityContext, this, requestFrom);

            // Initializing UI References
            contact_order_items_list_title = (MMTextView) findViewById(R.id.contact_order_items_list_title);
            list_view = (ListView) findViewById(R.id.contact_order_items_list_list_view);
            coi_total_orders_count = (MMTextView) findViewById(R.id.coi_total_orders_count);
            coi_total_prod_charges_value = (MMTextView) findViewById(R.id.coi_total_prod_charges_value);
            coi_total_barter_value = (MMTextView) findViewById(R.id.coi_total_barter_value);
            coi_total_gross_value = (MMTextView) findViewById(R.id.coi_total_gross_value);
            coi_total_net_value = (MMTextView) findViewById(R.id.coi_total_net_value);

            orderItemsListAdapter = new ContactOrderItemsListAdapter(activityContext);
            list_view.setAdapter(orderItemsListAdapter);

            refreshData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void toggleWidgetInContactOrderItemsList(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Checking which radio button was clicked
        switch (view.getId()) {
            case R.id.rb_print_orders:
                if (checked)
                    currentOperation = 0;
                break;
            case R.id.rb_digital_orders:
                if (checked)
                    currentOperation = 1;
                break;
        }

        if (currentOperation == 0) {
            showPrintOrders();
        } else {
            showDigitalOrders();
        }
    }

    public void resetValues() {
        currentPageNumber = 1;

        if (orderItemsList != null) {
            orderItemsList.clear();
            orderItemsList = null;
        }
        orderItemsList = new ArrayList<>();

        if (printOrders != null) {
            printOrders.clear();
            printOrders = null;
        }
        printOrders = new ArrayList<>();

        if (digitalOrders != null) {
            digitalOrders.clear();
            digitalOrders = null;
        }
        digitalOrders = new ArrayList<>();
    }

    public void refreshData() {
        resetValues();
        getContactOrderItemsList(currentPageNumber);
    }

    public void getContactOrderItemsList(int pageNumber) {
        if (!MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.showProgressDialog(activityContext);

        contactsModel.getCustomerOrders(encryptedClientKey, selectedClientId, Integer.parseInt(selectedCustomerId), pageNumber, Integer.parseInt(selectedYear));
    }

    public void updateOrderItemsList(final List<ContactOrderItem> itemsList, int noOfItems) {
        if (currentPageNumber == 1 && (itemsList == null || itemsList.isEmpty())) {
            resetValues();
            list_view.addHeaderView(no_records_found_text_view);
        } else {
            if (currentPageNumber == 1) {
                contact_order_items_list_title.setText(String.format("%s - %s", selectedYear, itemsList.get(0).getCompanyName()));
            }

            // Resetting totals before calculating
            printTotalOrdersCount = 0;
            printTotalProdCharges = 0.0f;
            printTotalBarter = 0.0f;
            printTotalGross = 0.0f;
            printTotalNet = 0.0f;

            digitalTotalOrdersCount = 0;
            digitalTotalProdCharges = 0.0f;
            digitalTotalBarter = 0.0f;
            digitalTotalGross = 0.0f;
            digitalTotalNet = 0.0f;

            for (ContactOrderItem orderItem : itemsList) {
                if (orderItem.getSubProductTypeID().equals(Constants.ORDER_TYPE_PRINT)) {
                    printOrders.add(orderItem);

                    printTotalOrdersCount = printTotalOrdersCount + 1;
                    printTotalProdCharges = printTotalProdCharges + orderItem.getProductionCharges();
                    printTotalBarter = printTotalBarter + orderItem.getBarter();
                    printTotalGross = printTotalGross + orderItem.getGross();
                    printTotalNet = printTotalNet + orderItem.getNet();

                } else if (orderItem.getSubProductTypeID().equals(Constants.ORDER_TYPE_DIGITAL)) {
                    digitalOrders.add(orderItem);

                    digitalTotalOrdersCount = digitalTotalOrdersCount + 1;
                    digitalTotalProdCharges = digitalTotalProdCharges + orderItem.getProductionCharges();
                    digitalTotalBarter = digitalTotalBarter + orderItem.getBarter();
                    digitalTotalGross = digitalTotalGross + orderItem.getGross();
                    digitalTotalNet = digitalTotalNet + orderItem.getNet();
                }
            }

            showPrintOrders();
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void showPrintOrders() {
        if (printOrders.isEmpty()) {
            no_records_found_text_view.setText("No Print Orders Found.");
            list_view.addHeaderView(no_records_found_text_view);
        } else {
            // Removing no records found message if already added to list view.
            if (list_view.getHeaderViewsCount() > 0) {
                list_view.removeHeaderView(no_records_found_text_view);
            }
        }

        orderItemsListAdapter.setOrderItemsList(printOrders);
        orderItemsListAdapter.notifyDataSetChanged();

        updateTotals();
    }

    public void showDigitalOrders() {
        if (digitalOrders.isEmpty()) {
            no_records_found_text_view.setText("No Digital Orders Found.");
            list_view.addHeaderView(no_records_found_text_view);
        } else {
            // Removing no records found message if already added to list view.
            if (list_view.getHeaderViewsCount() > 0) {
                list_view.removeHeaderView(no_records_found_text_view);
            }
        }

        orderItemsListAdapter.setOrderItemsList(digitalOrders);
        orderItemsListAdapter.notifyDataSetChanged();

        updateTotals();
    }

    public void updateTotals() {
        if (currentOperation == 0) {
            coi_total_orders_count.setText(String.format("%,d %s", printTotalOrdersCount, printTotalOrdersCount == 1 ? "Item" : "Items"));
            coi_total_prod_charges_value.setText(String.format("$%,.2f", printTotalProdCharges));
            coi_total_barter_value.setText(String.format("$%,.2f", printTotalBarter));
            coi_total_gross_value.setText(String.format("$%,.2f", printTotalGross));
            coi_total_net_value.setText(String.format("$%,.2f", printTotalNet));
        } else if (currentOperation == 1) {
            coi_total_orders_count.setText(String.format("%,d %s", digitalTotalOrdersCount, digitalTotalOrdersCount == 1 ? "Item" : "Items"));
            coi_total_prod_charges_value.setText(String.format("$%,.2f", digitalTotalProdCharges));
            coi_total_barter_value.setText(String.format("$%,.2f", digitalTotalBarter));
            coi_total_gross_value.setText(String.format("$%,.2f", digitalTotalGross));
            coi_total_net_value.setText(String.format("$%,.2f", digitalTotalNet));
        }
    }
}
