package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.CalendarEventsAdapter;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEvent;
import com.mirabeltechnologies.magazinemanager.beans.CalendarEventDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.models.CalendarModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;


import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import noman.weekcalendar.WeekCalendar;

public class CalendarActivity extends BaseActivity {
    public static final String TAG = CalendarActivity.class.getSimpleName();

    private final int DAY_VIEW = 0;
    private final int WEEK_VIEW = 1;

    private MMTextView selected_date_text_view;
    private WeekCalendar week_calendar;
    private ListView list_view;

    private CalendarModel calendarModel;
    private CalendarEventsAdapter calendarEventsAdapter;
    private Date currentDate, selectedDate;
    private String selectedDateStr = "", selectedDateDisplayStr = "";
    private int currentOperation = DAY_VIEW; // 0 - Day View & 1 - Week View
    private List<CalendarEvent> calendarEvents;
    private String currentTimeSlot = null, previousTimeSlot = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);

        try {
            activityContext = CalendarActivity.this;

            // initializing super class variables
            Constants.RequestFrom requestFrom = Constants.RequestFrom.CALENDAR;

            calendarModel = new CalendarModel(activityContext, this, requestFrom);

            currentDate = new Date();

            // Initializing UI References
            ImageView custom_date_icon = findViewById(R.id.custom_date_icon);
            selected_date_text_view = findViewById(R.id.calendar_selected_date_text_view);
            week_calendar = findViewById(R.id.week_calendar);

            list_view = findViewById(R.id.calendar_events_list_view);
            list_view.setAdapter(calendarEventsAdapter);

            list_view.setOnItemClickListener((parent, view, position, id) -> {
                getEventDetails(calendarEvents.get(position));
            });

            custom_date_icon.setOnClickListener(this::showDatePicker);

            showTodayEvents();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshCalendarEventsBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_CALENDAR_EVENTS));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshCalendarEventsBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void showDatePicker(View view) {
        try {
            String preSelectedDate = selectedDateStr;

            if (selectedDateStr.isEmpty()) {
                preSelectedDate = Utility.formatDate(new Date(), Constants.SERVER_DATE_FORMAT);
            }

            String[] dateValues = preSelectedDate.split("-");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, (datePicker, year, monthOfYear, dayOfMonth) -> {
                Calendar calendar = Calendar.getInstance();
                calendar.set(year, monthOfYear, dayOfMonth);

                selectedDate = calendar.getTime();

                getEvents(selectedDate);

            }, Integer.parseInt(dateValues[0]), Integer.parseInt(dateValues[1]) - 1, Integer.parseInt(dateValues[2]));

            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toggleWidgetInCalendar(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        // Checking which radio button was clicked
        switch (view.getId()) {
            case R.id.rb_calendar_day_view:
                if (checked)
                    currentOperation = DAY_VIEW;
                break;
            case R.id.rb_calendar_week_view:
                if (checked)
                    currentOperation = WEEK_VIEW;
                break;
        }

        if (currentOperation == DAY_VIEW) {
            showTodayEvents();
        } else {
            showWeeklyEvents();
        }
    }

    public void showTodayEvents() {
        week_calendar.setVisibility(View.GONE);

        getEvents(currentDate);
    }

    public void showWeeklyEvents() {
        week_calendar.setVisibility(View.VISIBLE);
        week_calendar.reset();
        week_calendar.setOnDateClickListener(dateTime -> {
            Date selectedWeekDate = dateTime.toDate();

            getEvents(selectedWeekDate);
        });

        //We are loading Today's date events by default when we switch to week view.
        getEvents(currentDate);
    }

    public void loadPastEvents(View view) {
        try {
            if (currentOperation == DAY_VIEW) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(selectedDate);
                calendar.add(Calendar.DATE, -1);

                getEvents(calendar.getTime());
            } else if (currentOperation == WEEK_VIEW) {
                week_calendar.moveToPreviousWeek();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadFutureEvents(View view) {
        try {
            if (currentOperation == DAY_VIEW) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(selectedDate);
                calendar.add(Calendar.DATE, 1);

                getEvents(calendar.getTime());
            } else if (currentOperation == WEEK_VIEW) {
                week_calendar.moveToNextWeek();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetValues() {
        currentTimeSlot = "";
        previousTimeSlot = "";
        selectedDateStr = "";

        if (calendarEvents != null) {
            calendarEvents.clear();
            calendarEvents = null;
        }

        // Removing no records found message if already added to list view.
        if (list_view.getHeaderViewsCount() > 0) {
            list_view.removeHeaderView(no_records_found_text_view);
        }
    }

    public void getEvents(Date date) {
        MMProgressDialog.showProgressDialog(activityContext);

        resetValues();

        selectedDate = date;
        selectedDateDisplayStr = Utility.formatDate(date, Constants.CALENDAR_DATE_DISPLAY_FORMAT);
        selectedDateStr = Utility.formatDate(date, Constants.SERVER_DATE_FORMAT);

        selected_date_text_view.setText(selectedDateDisplayStr);

        calendarModel.getEventsByDate(encryptedClientKey, getDTTicks(), loggedInRepId, selectedDateStr);
    }

    public void updateCalendarEventsForSelectedDate(List<CalendarEvent> events) {
        try {
            if (events != null && events.size() > 0) {
                this.calendarEvents = events;

                selected_date_text_view.setText(String.format("%s (%d Events)", selectedDateDisplayStr, events.size()));

                for (CalendarEvent event : calendarEvents) {
                    // We are checking for any start / continuous events are there in order to show them at first similar to all day events.
                    if (event.getTitle().contains("starts at") || event.getTitle().contains("continued from yesterday")) {
                        event.setTimeSlot("-1");
                        event.setTimeSlotName("All Day");
                    }

                    // Converting time slot to number in order to sort events by time slot.
                    event.setTimeSlotNumber(Integer.parseInt(event.getTimeSlot()));

                    // Removing anchor tag from event title if any
                    event.setTitle(event.getTitle().replaceAll("\\<.*?>", ""));
                }

                // After setting time slot to -1 for continuous events, we are sorting events by time slot in order to move them to first position in list.
                Collections.sort(calendarEvents, (lhs, rhs) -> {
                    //return (int) rhs.getTimeSlotNumber() - (int) lhs.getTimeSlotNumber(); // For descending order
                    return lhs.getTimeSlotNumber() - rhs.getTimeSlotNumber(); // For ascending order
                });

                for (int i = 0; i < calendarEvents.size(); i++) {
                    CalendarEvent event = calendarEvents.get(i);
                    currentTimeSlot = event.getTimeSlotName();

                    if (i == 0) {
                        previousTimeSlot = currentTimeSlot;
                    }

                    if (i == 0 || !previousTimeSlot.equals(currentTimeSlot)) {
                        event.setHeader(true);
                        event.setModifiedTimeSlotName(event.getTimeSlotName());
                    } else {
                        event.setHeader(false);
                    }

                    previousTimeSlot = currentTimeSlot;
                }

                calendarEventsAdapter = new CalendarEventsAdapter(activityContext);
                calendarEventsAdapter.setCalendarEvents(calendarEvents);
                list_view.setAdapter(calendarEventsAdapter);

               // calendarEventsAdapter.notifyDataSetChanged();

            } else {
                no_records_found_text_view.setText(String.format("No Events Scheduled for %s.", Utility.formatDate(selectedDate, Constants.SELECTED_DATE_DISPLAY_FORMAT)));
                list_view.addHeaderView(no_records_found_text_view);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MMProgressDialog.hideProgressDialog();
    }

    // Local Broadcast Manager Receiver Handler
    private final BroadcastReceiver mRefreshCalendarEventsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    if (Objects.equals(intent.getAction(), Constants.LBM_ACTION_REFRESH_CALENDAR_EVENTS)) {

                        if (bundle != null && selectedDateStr.equals(bundle.getString(Constants.BUNDLE_SELECTED_CALENDAR_DATE)))
                            getEvents(selectedDate);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void showCreateNewEventPage(View view) {
        Intent showCreateNewEventPageIntent = new Intent(activityContext, CreateCalendarEventActivity.class);
        showCreateNewEventPageIntent.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_DATE, selectedDateStr);
        showCreateNewEventPageIntent.putExtra(Constants.BUNDLE_IS_CALENDAR_EVENT_EDITING, false);
        startActivity(showCreateNewEventPageIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void getEventDetails(CalendarEvent event) {
        MMProgressDialog.showProgressDialog(activityContext);
        calendarModel.getEventDetailsByEventId(encryptedClientKey, getDTTicks(), loggedInRepId, selectedDateStr, event.getEventId());
    }

    public void editCalendarEvent(String eventId, CalendarEventDetails eventDetails) {
        Intent showEditCalendarEventIntent = new Intent(activityContext, CreateCalendarEventActivity.class);
        showEditCalendarEventIntent.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_DATE, selectedDateStr);
        showEditCalendarEventIntent.putExtra(Constants.BUNDLE_IS_CALENDAR_EVENT_EDITING, true);
        showEditCalendarEventIntent.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_EVENT_ID, eventId);
        showEditCalendarEventIntent.putExtra(Constants.BUNDLE_SELECTED_CALENDAR_EVENT_DETAILS, eventDetails);
        startActivity(showEditCalendarEventIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }
}
