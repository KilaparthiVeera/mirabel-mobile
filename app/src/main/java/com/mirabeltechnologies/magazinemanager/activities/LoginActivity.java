package com.mirabeltechnologies.magazinemanager.activities;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.LinkMovementMethod;
import android.text.method.PasswordTransformationMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.L;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.LogInModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.receiver.DeviceBootReceiver;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog.alertDialog;

import androidx.annotation.NonNull;

/**
 * @author venkat
 */
public class LoginActivity extends BaseActivity implements AlertDialogSelectionListener {

    public static final String TAG = LoginActivity.class.getSimpleName();
    private MMSharedPreferences sharedPreferences;
    private Context activityContext;
    private MMEditText mEmailView, mPasswordView;
    private String emailId, password;
    private boolean isAutoLogIn = false;
    public static boolean isAlertDialogShown = false;
    private LogInModel logInModel;
    CheckBox checkBox;

    private static final int REQ_CODE_VERSION_UPDATE = 530;
    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try {
            Context applicationContext = getApplicationContext();
            activityContext = LoginActivity.this;

            sharedPreferences = new MMSharedPreferences(applicationContext);

            logInModel = new LogInModel(activityContext, this, Constants.RequestFrom.LOGIN_PAGE);
            checkBox = findViewById(R.id.check_box);
            TextView txt_tc = findViewById(R.id.txt_tc);
            SpannableString ss = new SpannableString("By continuing you agree to our privacy policy");
            ClickableSpan clickableSpan = new ClickableSpan() {
                @Override
                public void onClick(@NonNull View textView) {
                    String url ="https://www.mirabeltechnologies.com/privacy-statement-2023/";
                    if (url.length() > 0) {
                        Intent intent = new Intent(LoginActivity.this, WebViewActivity.class);
                        intent.putExtra(Constants.BUNDLE_LOADING_URL, url);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
                    }
                }
                @Override
                public void updateDrawState(@NonNull TextPaint ds) {
                    super.updateDrawState(ds);
                    ds.setUnderlineText(true);
                    if (txt_tc.isPressed()) {
                        ds.setColor(Color.BLUE);
                    } else {
                        ds.setColor(Color.WHITE);
                    }
                    txt_tc.invalidate();
                }
            };
            ss.setSpan(clickableSpan, 31, 45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_tc.setText(ss);
            txt_tc.setMovementMethod(LinkMovementMethod.getInstance());
            txt_tc.setHighlightColor(Color.WHITE);

            final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            boolean agreed = sharedPreferences.getBoolean("agreed",false);
            checkBox.setChecked(agreed);
/*
            checkBox.setOnClickListener(v -> {
                if (checkBox.isChecked()){
                    String url ="https://www.mirabeltechnologies.com/privacy-statement-2023/";
                    if (url.length() > 0) {
                        Intent intent = new Intent(activityContext, WebViewActivity.class);
                        intent.putExtra(Constants.BUNDLE_LOADING_URL, url);
                        startActivity(intent);
                        overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
                    }
                }

            });
*/
           // checkBox.setMovementMethod(LinkMovementMethod.getInstance());
            mEmailView = findViewById(R.id.email);
            mEmailView.setOnEditorActionListener((v, actionId, event) -> {
                if (actionId == R.id.login || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    attemptLogin();
                    return true;
                }

                return false;
            });

            mPasswordView = findViewById(R.id.password);
            mPasswordView.setOnEditorActionListener((textView, actionId, keyEvent) -> {
                if (actionId == R.id.login || actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_UNSPECIFIED) {
                    attemptLogin();
                    return true;
                }

                return false;
            });

            MMButton logInButton = findViewById(R.id.login_button);
            logInButton.setOnClickListener(view -> attemptLogin());

            checkForAppUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void ShowHidePass(View view) {
        if(view.getId()==R.id.show_pass_btn){
            if(mPasswordView.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
                ((ImageView)(view)).setImageResource(R.drawable.ic_visibility_white);
                //Show Password
                mPasswordView.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                int position = mPasswordView.length();
                if(position>0){
                    Selection.setSelection( mPasswordView.getText(), position);
                }
            }
            else{
                ((ImageView)(view)).setImageResource(R.drawable.ic_visibility_off_white);
                //Hide Password
                mPasswordView.setTransformationMethod(PasswordTransformationMethod.getInstance());
                int position = mPasswordView.length();
                if(position>0){
                    Selection.setSelection( mPasswordView.getText(), position);
                }
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {

            final String savedEmailId = sharedPreferences.getString(Constants.SP_EMAIL_ID);
            final String savedPassword = sharedPreferences.getString(Constants.SP_PASSWORD);

            if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                setContentView(R.layout.activity_splash_screen);
                isAutoLogIn = true;
                authenticateUser(savedEmailId, savedPassword);
                /*setContentView(R.layout.activity_autologin);
                MMTextView autoLogInText = findViewById(R.id.autoLogInText);
                autoLogInText.setText(String.format("Do you want to continue with login account: %s", savedEmailId));
                checkNewAppVersionState();
                MMButton continueButton = findViewById(R.id.continue_button);
                continueButton.setOnClickListener(view -> {
                    isAutoLogIn = true;
                    authenticateUser(savedEmailId, savedPassword);

                    Log.e("veera",savedEmailId+"  "+savedPassword);
                });

                MMButton cancelButton = findViewById(R.id.cancel_button);
                cancelButton.setOnClickListener(view -> clearCacheAndNavigateToLogIn());
*/
            } else {
                checkNewAppVersionState();
                isAutoLogIn = false;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && alertDialog != null) {
            alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onDestroy() {
        unregisterInstallStateUpdListener();
        super.onDestroy();
    }
    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (requestCode == REQ_CODE_VERSION_UPDATE) {
            if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                L.d("Update flow failed! Result code: " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                unregisterInstallStateUpdListener();
            }
        }
    }

    private void checkForAppUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(LoginActivity.this);

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Create a listener to track request state updates.
        installStateUpdatedListener = installState -> {
            // Show module progress, log state, or install the update.
            if (installState.installStatus() == InstallStatus.DOWNLOADED)
                // After the update is downloaded, show a notification
                // and request user confirmation to restart the app.
                popupSnackbarForCompleteUpdateAndUnregister();
        };

        // Checks that the platform will allow the specified type of update.
        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // Request the update.
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                    // Before starting an update, register a listener for updates.
                    appUpdateManager.registerListener(installStateUpdatedListener);
                    // Start an update.
                    startAppUpdateFlexible(appUpdateInfo);
                } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE) ) {
                    // Start an update.
                    startAppUpdateImmediate(appUpdateInfo);
                }
            }
        });
    }

    private void startAppUpdateImmediate(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    LoginActivity.REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    private void startAppUpdateFlexible(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    // The current activity making the update request.
                    this,
                    // Include a request code to later monitor this update request.
                    LoginActivity.REQ_CODE_VERSION_UPDATE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            unregisterInstallStateUpdListener();
        }
    }

    /**
     * Displays the snackbar notification and call to action.
     * Needed only for Flexible app update
     */

    private void popupSnackbarForCompleteUpdateAndUnregister() {
        Snackbar snackbar =
                Snackbar.make(getWindow().getDecorView().getRootView(), "An update has just been downloaded.", Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction("RESTART", view -> appUpdateManager.completeUpdate());
        snackbar.setActionTextColor(getResources().getColor(R.color.red));
        View snackBarView = snackbar.getView();
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(10, 10, 10, 10);

        snackBarView.setLayoutParams(params);
        snackbar.show();
        unregisterInstallStateUpdListener();
    }

    private void checkNewAppVersionState() {

        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            //FLEXIBLE:
                            // If the update is downloaded but not installed,
                            // notify the user to complete the update.
                            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                                popupSnackbarForCompleteUpdateAndUnregister();
                            }

                            //IMMEDIATE:
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                startAppUpdateImmediate(appUpdateInfo);
                            }
                        });

    }

    /**
     * Needed only for FLEXIBLE update
     */
    private void unregisterInstallStateUpdListener() {
        if (appUpdateManager != null && installStateUpdatedListener != null)
            appUpdateManager.unregisterListener(installStateUpdatedListener);
    }


    private void attemptLogin() {
        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        emailId = Objects.requireNonNull(mEmailView.getText()).toString();
        password = Objects.requireNonNull(mPasswordView.getText()).toString();

        boolean cancel = false;
        View focusView = null;

        if (TextUtils.isEmpty(emailId)) {
            mEmailView.setError(getString(R.string.email_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!Utility.isValidEmail(emailId)) { // Checking for a valid email address.
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        } else if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.password_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first form field with an error.
            focusView.requestFocus();
        } else {
            if(checkBox.isChecked()){
                authenticateUser(emailId, password);
            } else {
                Toast.makeText(LoginActivity.this,"Please Accept Privacy Policy", Toast.LENGTH_SHORT).show();

            }

        }
    }

    public void authenticateUser(String email, String pwd) {
        MMProgressDialog.showProgressDialog(activityContext);
        logInModel.validateUserLogin(email, pwd);
    }

    public void logInError(int responseCode) {
        MMProgressDialog.hideProgressDialog();
        final String savedEmailId = sharedPreferences.getString(Constants.SP_EMAIL_ID);
        final String savedPassword = sharedPreferences.getString(Constants.SP_PASSWORD);
        switch (responseCode) {
            case Constants.MM_INVALID_LOGIN:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login));
                }
                break;
            case Constants.MM_LOGIN_SUCCESS_BUT_NO_CLIENTS:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients));
                }
                break;
            case Constants.MM_PASSWORD_STRONG:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.login_response_password_not_strong), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_not_strong));
                }
                break;
            case Constants.MM_PASSWORD_EXPIRED:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.login_response_password_expired), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_expired));
                }
                break;
            case Constants.MM_PENDING_VERIFICATION:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.login_response_pending_verification), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_pending_verification));
                }
                break;
            case Constants.MM_LOGIN_ERROR:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.login_error), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.login_error));
                }
                // doLogout(getResources().getString(R.string.login_error));
                break;
            case Constants.MM_INVALID_REQUEST:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.invalid_request), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.invalid_request));
                }
                break;
            default:
                if (!savedEmailId.isEmpty() && !savedPassword.isEmpty()) {
                    MMAlertDialog.listener = this;
                    MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
                    MMAlertDialog.showAlertDialogWithCallback(activityContext, "Login Response", getResources().getString(R.string.common_login_response), false);
                } else {
                    MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.common_login_response));
                }
                break;
        }
    }

    public void updateLoggedInUserName(String firstName, String lastName) {
        String formattedRepName = "", formattedUserName = "";

        if (firstName.length() > 0 && lastName.length() > 0) {
            formattedRepName = lastName + ", " + firstName;
            formattedUserName = firstName + " " + lastName;
        } else if (firstName.length() > 0) {
            formattedRepName = firstName;
            formattedUserName = firstName;
        } else if (lastName.length() > 0) {
            formattedRepName = lastName;
            formattedUserName = lastName;
        }

        sharedPreferences.putString(Constants.SP_LOGGED_IN_REP_NAME, formattedRepName);
        sharedPreferences.putString(Constants.SP_LOGGED_IN_USER_NAME, formattedUserName);
    }

    public void updateClientsList(List<ClientInfo> clientList) {
        try {
            if (clientList.size() == 0) {
                MMProgressDialog.hideProgressDialog();
                MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients_with_mm_access));
            } else {
                ClientInfo.clientList = clientList;

                if (clientList.size() == 1 && clientList.get(0).getClientId() == 0) {
                    MMProgressDialog.hideProgressDialog();
                    sharedPreferences.clear();
                    MMAlertDialog.showAlertDialog(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login));
                } else {
                    //Saving username & password in shared preferences if it's first time log in
                    if (!isAutoLogIn) {
                        sharedPreferences.putString(Constants.SP_EMAIL_ID, emailId);
                        sharedPreferences.putString(Constants.SP_PASSWORD, password);
                    }

                    //Load Dashboard directly with single client
                    if (clientList.size() == 1) {
                        getClientEncryptedId(clientList.get(0));
                    } else {
                        int savedSelectedClientId = sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID);

                        if (savedSelectedClientId <= 0) {
                            MMProgressDialog.hideProgressDialog();

                            Intent clientsListIntent = new Intent(LoginActivity.this, ClientsListActivity.class);
                            clientsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            clientsListIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(clientsListIntent);
                            finish();
                            //overridePendingTransition(R.anim.top_in, R.anim.bottom_out);
                        } else {
                            int savedSelectedClientIndex = getIndexOfClientIdFromArrayList(clientList, savedSelectedClientId);
                            getClientEncryptedId(clientList.get(savedSelectedClientIndex));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getIndexOfClientIdFromArrayList(List<ClientInfo> list, int clientId) {
        int index = 0;

        for (int i = 0; i < list.size(); i++) {
            ClientInfo clientInfo = list.get(i);
            if (clientId == clientInfo.getClientId()) {
                index = i;
                break;
            }
        }

        return index;
    }

    public void getClientEncryptedId(ClientInfo selectedClientDetails) {

        try {

            ClientInfo.selectedClientId = selectedClientDetails.getClientId();

            EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
            String encryptedId = des.getEncryptedStringForSelectedClient(selectedClientDetails);

            sharedPreferences.putString(Constants.SP_ENCRYPTED_CLIENT_KEY, encryptedId);
            sharedPreferences.putString(Constants.SP_SELECTED_SUB_DOMAIN, selectedClientDetails.getSubDomain());
            sharedPreferences.putInt(Constants.SP_SELECTED_CLIENT_ID, selectedClientDetails.getClientId());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_NAME, selectedClientDetails.getClientName());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_URL, selectedClientDetails.getClientURL());
            sharedPreferences.putInt(Constants.SP_LOGGED_IN_REP_ID, selectedClientDetails.getEmployeeId());
            sharedPreferences.putString(Constants.SP_SELECTED_SITE_TYPE, selectedClientDetails.getSiteType());
            sharedPreferences.putBoolean(Constants.SP_IS_DS_USER, selectedClientDetails.isDSUser());
            MMProgressDialog.hideProgressDialog();

            loadMainActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void loadMainActivity() {
        // In order to load website config initially, we are resetting value.
        sharedPreferences.putBoolean(Constants.SP_IS_WEBSITE_CONFIG_LOADED, false);

        Intent mainActivityIntent = new Intent(LoginActivity.this, DashboardActivity.class);
        //mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        //mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK); // if you keep these flags white screen is coming on Intent navigation
        startActivity(mainActivityIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }


    public void clearCacheAndNavigateToLogIn() {
        try {
           // MMSettingsPreferences mmSettingsPreferences = new MMSettingsPreferences(getApplicationContext());
            MMLocalDataBase db = new MMLocalDataBase(LoginActivity.this);
            //removeNotificationsFromNotificationTrayOnUserLogout
            BuildNotification buildNotification = new BuildNotification(LoginActivity.this);
            buildNotification.removeNotificationsFromNotificationTrayOnUserLogout();
            //check and clear running services
            //clear scheduled notifications(alarms) .This can be done , if we have the ids of the scheduled notifications.
            ArrayList<Integer> pendingIntentIdList = db.getAllScheduledNotifications();
            if (pendingIntentIdList != null) {
                PendingIntent pendingIntent;
                AlarmManager manager;
                Intent alarmintent;
                for (Integer notification_id : pendingIntentIdList) {
                    alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                    alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                    pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                    manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                    //cancel the alarm manager of the pending intent
                    if (manager != null) {
                        manager.cancel(pendingIntent);
                    }

                }
            }
            EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
            String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
            LogoutReport(encryptedId, sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN));

            //disable boot receiver after user logout.
            disableDeviceBootReceiver();
            //clear all user store data.
            sharedPreferences.clear();
            //  mmSettingsPreferences.clear();
            //truncate  database tables
            db.truncateTableNotificationIds();
            //clear jobschedulers
          //  FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(LoginActivity.this);
          //  firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);
            ClientInfo.clientList = new ArrayList<>();
            //Reloading LogIn Page
            finish();
            startActivity(getIntent());
            overridePendingTransition(R.anim.bottom_in, R.anim.top_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void disableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED) {
            clearCacheAndNavigateToLogIn();
        }
    }
}
