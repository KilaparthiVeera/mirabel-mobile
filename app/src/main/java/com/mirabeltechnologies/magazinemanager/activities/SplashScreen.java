package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.BuildConfig;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;
import java.util.Calendar;

/**
 * Created by venkat on 04/10/17.
 *
 * @author venkat
 */
public class SplashScreen extends AppCompatActivity {
    private Runnable mRunnable;
    private Handler mHandler = new Handler();
    private MMSharedPreferences sharedPreferences;
    TextView copyright_text;
    Button btn_splash_screen;
    AlertDialog  alertDialog;
    boolean policy = false;


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        checkAndMove();
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        String gggg=des.getDecryptedString("oVaSXnBHAG7YRVnyVDAGtNePp7oXlmeWWPi9xTbQ2YLr0LpTjG42E8tJXFzZBxBs0n7UbvWWQebS7UOOv+eRQ0OqGarWymUv");

        Log.e("chinna",gggg);
        try {
            // We are disabling Fabric in debug / development mode.

            sharedPreferences = new MMSharedPreferences(SplashScreen.this);

            int current_year = Calendar.getInstance().get(Calendar.YEAR);
            btn_splash_screen=findViewById(R.id.btn_splash_screen);
            copyright_text = findViewById(R.id.copyright_text);

            copyright_text.setText(String.format("© 2002- %s Mirabel Technologies, Inc. App Ver : %s", current_year, BuildConfig.VERSION_NAME));
           // copyright_text.setText(String.format("© 2002- %s Mirabel Technologies, Inc.", current_year));

            mRunnable = () -> {
                mHandler.removeCallbacks(mRunnable);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    BuildNotification buildNotification = new BuildNotification(SplashScreen.this);
                    buildNotification.createNotificationChannel();
                }
                sharedPreferences.putString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD, "");

               checkAndMove();

            };

            if (new NetworkConnectionDetector(SplashScreen.this).isNetworkConnected()) {
                mHandler.postDelayed(mRunnable, 2000);
            }
            else {
                btn_splash_screen.setVisibility(View.VISIBLE);
            }

            btn_splash_screen.setOnClickListener(view -> {
                if (new NetworkConnectionDetector(SplashScreen.this).isNetworkConnected()) {
                    mHandler.postDelayed(mRunnable, 100);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showInputDialog() {

        final SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        ViewGroup viewGroup = findViewById(android.R.id.content);
        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(this).inflate(R.layout.terms_and_conditions_layout, viewGroup, false);
        //Now we need an AlertDialog.Builder object
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //setting the view of the builder to our custom view that we already inflated
        builder.setView(dialogView);
        //finally creating the alert dialog and displaying it
        alertDialog = builder.create();
        alertDialog.show();
        TextView txt_tc = dialogView.findViewById(R.id.txt_tc);
        CheckBox ch_privacy = dialogView.findViewById(R.id.ch_privacy);
        ch_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                policy = ch_privacy.isChecked();
            }
        });

        SpannableString ss = new SpannableString("By continuing you agree to our privacy policy");
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View textView) {
                String url ="https://www.mirabeltechnologies.com/privacy-statement-2023/";
                if (url.length() > 0) {
                    Intent intent = new Intent(SplashScreen.this, WebViewActivity.class);
                    intent.putExtra(Constants.BUNDLE_LOADING_URL, url);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_down_out);
                }
            }
            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                if (txt_tc.isPressed()) {
                    ds.setColor(Color.BLUE);
                } else {
                    ds.setColor(Color.WHITE);
                }
                txt_tc.invalidate();
            }
        };
        ss.setSpan(clickableSpan, 31, 45, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_tc.setText(ss);
        txt_tc.setMovementMethod(LinkMovementMethod.getInstance());
        txt_tc.setHighlightColor(Color.WHITE);
        Button save_button = dialogView.findViewById(R.id.create_contact_save_button);
        Button skip_button = dialogView.findViewById(R.id.create_contact_reset_button);
        skip_button.setOnClickListener(v -> {
            askPermission();
            alertDialog.dismiss();
        });
        save_button.setOnClickListener(v -> {
            if(policy){
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putBoolean("agreed", true);
                editor.apply();
                askPermission();
                alertDialog.dismiss();
            } else {
                Toast.makeText(SplashScreen.this,"please accept privacy policy", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void checkAndMove() {
        if (requirePermissionCheck()) {
            Intent logInPage = new Intent(SplashScreen.this, LoginActivity.class);
            logInPage.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(logInPage);
            finish();

        } else {
            showInputDialog();
        }
    }

    private void askPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            int REQ_PERMISSIONS = 7;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.CALL_PHONE,Manifest.permission.CAMERA,
                    Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.READ_MEDIA_IMAGES,Manifest.permission.READ_MEDIA_AUDIO}, REQ_PERMISSIONS);
        }
        else {
            int REQ_PERMISSIONS = 7;
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.CALL_PHONE,Manifest.permission.CAMERA,
                    Manifest.permission.READ_CONTACTS,Manifest.permission.WRITE_CONTACTS,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, REQ_PERMISSIONS);

        }
    }

    private boolean requirePermissionCheck() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_MEDIA_IMAGES) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_MEDIA_AUDIO) == PackageManager.PERMISSION_GRANTED;
        }
        else {
            return ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(SplashScreen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
        }
    }

}
