package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.speech.RecognizerIntent;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.Notification;
import com.mirabeltechnologies.magazinemanager.beans.QuickSearchContact;
import com.mirabeltechnologies.magazinemanager.beans.WebsiteConfig;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.fragments.DashboardFragment;
import com.mirabeltechnologies.magazinemanager.fragments.FragmentDrawer;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.FragmentDrawerListener;
import com.mirabeltechnologies.magazinemanager.interfaces.PhoneCallListener;
import com.mirabeltechnologies.magazinemanager.interfaces.QuickSearchContactSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.RequiresPermissionAlertDialogListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.models.DashboardModel;
import com.mirabeltechnologies.magazinemanager.models.LogInModel;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.receiver.DeviceBootReceiver;
import com.mirabeltechnologies.magazinemanager.services.NotificationService;
import com.mirabeltechnologies.magazinemanager.util.BuildNotification;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALLS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.LOCATION_SETTINGS_REQUEST_CODE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETINGS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.REPEATS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.REQUEST_LOCATION_PERMISSIONS_REQUEST_CODE;

public class DashboardActivity extends BaseActivity implements FragmentDrawerListener, QuickSearchContactSelectionListener, AlertDialogSelectionListener, PhoneCallListener, RequiresPermissionAlertDialogListener {
    public static final String TAG = DashboardActivity.class.getSimpleName();
    private static String[] navMenuTitles;
    MMSettingsPreferences mmSettingsPreferences;
    Intent serintent;
    CheckNotificationSettingsClass checkNotificationSettingsClass;
     FragmentDrawer fragmentDrawer;
     Toolbar mToolbar;
    private Menu dashboardMenu;
     LogInModel logInModel;
    private DashboardModel dashboardModel;
    private ContactsModel contactsModel;
    private CommonModel commonModel;
    private DashboardFragment dashboardFragment;
    private List<ClientInfo> clientsList;
    private long lastRefreshedTime;
     String quickSearchContactType = "";
    private String customerId, phoneNumber, extension;
     FirebaseAnalytics mFirebaseAnalytics;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationRequest locationRequest;
    private LocationCallback mLocationCallback;
    public static String city,state,country,zipcode,fulladdress,shortcode,full_shortcode;
    String[] locationPermissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
    public static Location lastLocation ;
    boolean mLocationPermissionGranted = false;
    // Local Broadcast Manager Receiver Handler
    private final BroadcastReceiver mRefreshRecentContactsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
              //  Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    dashboardModel.getRecentContacts(encryptedClientKey, getDTTicks(), false, 1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        // In order to hide keyboard when this screen appears because of AutoCompleteTextView.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mmSettingsPreferences = new MMSettingsPreferences(getApplicationContext());
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        try {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, location -> {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            lastLocation = location;
                            System.out.println("==my=location_latitude1 = " + location.getLatitude());
                            System.out.println("==my=location_longitude1 = " + location.getLongitude());
                            getCityFromLocation(lastLocation);


                        }
                    });
            locationRequest = LocationRequest.create();
            locationRequest.setInterval(5000);
            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    for (Location location : locationResult.getLocations()) {
                        // Update UI with location data
                        // txtLatitude.setText(String.valueOf(location.getLatitude()));
                        //txtLongitude.setText(String.valueOf(location.getLongitude()));
                        lastLocation = location;
                        System.out.println("==my=location_latitude2 = " + location.getLatitude());
                        System.out.println("==my=location_longitude2 = " + location.getLongitude());
                    }
                }
            };
        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            activityContext = DashboardActivity.this;
           /* if (!(mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH) || mmSettingsPreferences.getBooleanValue(CALLS_SWITCH))) {
                notificationSettingsDialog();
            }*/

            if (mmSettingsPreferences.getBooleanValue("allow_enable_notifications_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_enable_notifications_dialog_switch")) {
                notificationSettingsDialog();
            } else {
                if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH) || mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {
                    CheckAndIgnoreBatteryOptimisation();
                }
            }
            logInModel = new LogInModel(activityContext, this, Constants.RequestFrom.DASHBOARD);
            dashboardModel = new DashboardModel(activityContext, this, Constants.RequestFrom.DASHBOARD);
            contactsModel = new ContactsModel(activityContext, this, Constants.RequestFrom.DASHBOARD);
            commonModel = new CommonModel(activityContext, this, Constants.RequestFrom.DASHBOARD);

            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

            mToolbar = findViewById(R.id.dashboard_toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setTitle(R.string.title_dashboard);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            //getSupportActionBar().setDisplayShowTitleEnabled(true); // if true toolbar title will display

            fragmentDrawer = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
            fragmentDrawer.setUp(R.id.fragment_navigation_drawer, findViewById(R.id.drawer_layout), mToolbar);
            fragmentDrawer.setDrawerListener(this);
            fragmentDrawer.updateLoggedInUserNameAndWebsite(loggedInUserName, selectedClientName);

            clientsList = ClientInfo.clientList;

            // In order to handle displaying of change client option in dashboard
            if (clientsList.isEmpty()) {
                String email = sharedPreferences.getString(Constants.SP_EMAIL_ID);
                String pwd = sharedPreferences.getString(Constants.SP_PASSWORD);
                logInModel.validateUserLogin(email, pwd);
            }

            // display the first navigation drawer view on app launch
            displayView(0);
            checkLocationAccess();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshRecentContactsBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public boolean statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
            return false;
        }
        return true;
    }


    public void buildAlertMessageNoGps() {
        try {
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            MMAlertDialog.showAlertDialogWithCallback(this, "Location Access", getResources().getString(R.string.turn_on_gps_confirmation), true);
            MMAlertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    // startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    Intent locationSettingsIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivityForResult(locationSettingsIntent, LOCATION_SETTINGS_REQUEST_CODE);
                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        refreshData();
    }
    @Override
    protected void onStart() {
        super.onStart();
        startLocationUpdates();
    }
    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        exitApp();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshRecentContactsBroadcastReceiver);


    }
    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful() && task.getResult() != null) {
                        lastLocation = task.getResult();
                        System.out.println("==my=location_latitude3 = " + task.getResult().getLatitude());
                        System.out.println("==my=location_longitude3 = " + task.getResult().getLongitude());
                    }
                });
    }
    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
    }
    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mFusedLocationClient.requestLocationUpdates(locationRequest, mLocationCallback, null);
    }
    public void checkLocationAccess() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, locationPermissions, REQUEST_LOCATION_PERMISSIONS_REQUEST_CODE);
            } else {
                if (statusCheck()) {
                    mLocationPermissionGranted = true;
                    getLastKnownLocation();
                }
            }
        } else {
            if (statusCheck()) {
                mLocationPermissionGranted = true;
                getLastKnownLocation();
            }
        }
    }
    public void getCityFromLocation(Location locat) {
        Geocoder geoCoder = new Geocoder(getBaseContext(), Locale.getDefault());
        try {
            List<Address> addresses = geoCoder.getFromLocation(locat.getLatitude(), locat.getLongitude(), 1);
            if (addresses.size() > 0) {
                fulladdress = addresses.get(0).getAddressLine(0);
                System.out.println("==locality==" + addresses.get(0).getLocality());
                System.out.println("==county==" + addresses.get(0).getCountryName());
                System.out.println("==zipcode==" + addresses.get(0).getPostalCode());
                city= addresses.get(0).getLocality();
                city = city != null ?city : "";
                state=addresses.get(0).getAdminArea();
                state = state != null ?state : "";
                country = addresses.get(0).getCountryName();
                country = country != null ?country : "";
                zipcode=addresses.get(0).getPostalCode();
                zipcode = zipcode != null ?zipcode : "";
                shortcode=addresses.get(0).getCountryCode();
                shortcode = shortcode != null ?shortcode : "";
                full_shortcode=country+" "+"("+ shortcode+")";
                full_shortcode = full_shortcode.replaceAll("\\s", "");
                full_shortcode = full_shortcode != null ?full_shortcode : "";
            }
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
    public void updateClientsList(List<ClientInfo> clientList) {
        try {
            if (MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.hideProgressDialog();

            MenuItem changeClientMenuItem = dashboardMenu.findItem(R.id.action_change_client);

            clientsList = clientList;

            changeClientMenuItem.setVisible(clientsList.size() > 1);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dashboard_toolbar_header, menu);

        dashboardMenu = menu;

        if (clientsList.size() <= 1) {
            MenuItem changeClientMenuItem = menu.findItem(R.id.action_change_client);
            changeClientMenuItem.setVisible(false);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_change_client:
                changeClient();
                return true;
            case R.id.action_refresh:
                loadDataFromServer(true);
                return true;
            case R.id.action_logout:
                doLogout();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    public void displayView(int index) {
        Intent intent;

        switch (index) {
            case 0:
                loadDashboard();
                //refreshData(); -- Commented this as we are refreshing dashboard data in onResume().
                break;
            case 1:
                intent = new Intent(activityContext, MyContactsActivity.class);
                openIntent(intent);
                break;

            case 2:
                //nearby contacts.
                intent = new Intent(activityContext, NearbyContactsActivity.class);
                openIntent(intent);
                break;

            case 3:
                intent = new Intent(activityContext, SavedSearchesActivity.class);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 4:
                intent = new Intent(activityContext, SearchOrdersFiltersActivity.class);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 5:
                intent = new Intent(activityContext, SearchActivitiesScreen.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_NOTES);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);

                break;
            case 6:
                intent = new Intent(activityContext, SearchActivitiesScreen.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_CALLS);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 7:
                intent = new Intent(activityContext, SearchActivitiesScreen.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_MEETINGS);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 8:
                intent = new Intent(activityContext, SearchActivitiesFiltersScreen.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_ALL_ACTIVITIES);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 9:
                intent = new Intent(activityContext, CalendarActivity.class);
                openIntent(intent);
                break;
            case 10:
                intent = new Intent(activityContext, TaskListActivity.class);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 11:
                intent = new Intent(activityContext, OpportunitiesActivity.class);
                intent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_ALL_ACTIVITIES);
                intent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD);
                openIntent(intent);
                break;
            case 12:
                intent = new Intent(activityContext, NewTicketActivity.class);
                openIntent(intent);
                break;
            //for settings screen
            case 13:
                intent = new Intent(activityContext, SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivityForResult(intent, Constants.OPEN_APPLICATION_SETTINGS);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
                break;
            default:
                displayToast("You have selected : " + navMenuTitles[index]);
                break;
        }
       /* if (intent != null)
            openIntent(intent);
*/
    }

    public void openIntent(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void loadDashboard() {
        if (dashboardFragment == null) {
            dashboardFragment = new DashboardFragment();
            dashboardFragment.setQuickSearchContactSelectionListener(this);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_frame_container, dashboardFragment);
        fragmentTransaction.addToBackStack("Dashboard");
        fragmentTransaction.commit();
    }

    public void changeClient() {
        try {
            Intent changeClientIntent = new Intent(activityContext, ClientsListActivity.class);
            changeClientIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(changeClientIntent);
            overridePendingTransition(R.anim.bottom_in, R.anim.top_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void doLogout() {

        Log.e("veera",""+sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID));
        Log.e("veera",""+sharedPreferences.getString(Constants.SP_EMAIL_ID));
        try {
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            MMAlertDialog.showAlertDialogWithCallback(this, "Confirm", getResources().getString(R.string.logout_confirmation), true);
            MMAlertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    MMLocalDataBase db = new MMLocalDataBase(DashboardActivity.this);
                    //removeNotificationsFromNotificationTrayOnUserLogout
                    BuildNotification buildNotification = new BuildNotification(DashboardActivity.this);
                    buildNotification.removeNotificationsFromNotificationTrayOnUserLogout();
                    //check and clear running services

                    //clear scheduled notifications(alarms) .This can be done , if we have the ids of the scheduled notifications.
                    ArrayList<Integer> pendingIntentIdList = db.getAllScheduledNotifications();
                    if (pendingIntentIdList != null) {
                        PendingIntent pendingIntent;
                        AlarmManager manager;
                        Intent alarmintent;
                        for (Integer notification_id : pendingIntentIdList) {
                            alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                            alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                            pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                            manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                            //cancel the alarm manager of the pending intent
                            manager.cancel(pendingIntent);

                        }
                    }
                    EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
                    String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
                    LogoutReport(encryptedId,sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN));
                    //disable boot receiver after user logout.
                    disableDeviceBootReceiver();
                    //clear all user store data.
                    sharedPreferences.clear();
                    // mmSettingsPreferences.clear();


                    //truncate  database tables
                    db.truncateTableNotificationIds();


                    //clear jobschedulers
                  //  FirebaseJobDispatcherClass firebaseJobDispatcherClass = new FirebaseJobDispatcherClass(DashboardActivity.this);
                  //  firebaseJobDispatcherClass.cancelFirebaseJobDispatcher(Constants.FIREBASE_JOB_DISPATCHER_SCHEDULE_NOTIFICATIONS_TAG);

                    ClientInfo.clientList = new ArrayList<>();
                    Intent logInIntent = new Intent(activityContext, LoginActivity.class);
                    logInIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(logInIntent);
                    overridePendingTransition(R.anim.left_in, R.anim.right_out);

                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exitApp() {
        try {
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            MMAlertDialog.showAlertDialogWithCallback(this, "Confirm", getResources().getString(R.string.exit_confirmation), true);
            MMAlertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    Intent intent = new Intent(Intent.ACTION_MAIN);
                    intent.addCategory(Intent.CATEGORY_HOME);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                    finish();
                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void refreshData() {
        String savedTimeStamp = sharedPreferences.getString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD);

        if (!savedTimeStamp.equals("")) {
            lastRefreshedTime = Long.parseLong(savedTimeStamp);
            long currentTimeStamp = System.currentTimeMillis();
            long timeDifferenceInSec = (currentTimeStamp - lastRefreshedTime) / 1000;

            // If time difference is more than 180 sec i.e. 3 min, we will send a request to load data from server o/w. we will display Issues by fetching from GlobalContent.
            if (timeDifferenceInSec > 180) {
                loadDataFromServer(true);
            } else {
                if (globalContent.getRecentContactsList() != null) {
                    updateRecentContacts(globalContent.getRecentContactsList().size(), globalContent.getRecentContactsList(), false, false);
                } else {
                    loadDataFromServer(true);
                }
            }
        } else {
            loadDataFromServer(true);
        }
    }

    public void loadDataFromServer(boolean loadNotifications) {
        MMProgressDialog.showProgressDialog(activityContext);
        dashboardModel.getRecentContacts(encryptedClientKey, getDTTicks(), loadNotifications, 1);

        lastRefreshedTime = System.currentTimeMillis();
        sharedPreferences.putString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD, "" + lastRefreshedTime);
    }

    public void updateRecentContacts(int noOfRecentContacts, List<Contact> recentContactsList, boolean needToCache, boolean loadNotifications) {
        dashboardFragment.updateRecentContacts(noOfRecentContacts, recentContactsList);

        if (needToCache) {
            globalContent.setRecentContactsList(recentContactsList);

            if (loadNotifications) {
                // Getting All Notifications
                getNotifications(Constants.NOTIFICATION_ALL, true);
            } else {
                MMProgressDialog.hideProgressDialog();
            }
        } else {
            if (globalContent.getDashboardNotifications() != null) {
                updateNotifications(globalContent.getDashboardNotifications(), false, false);
            } /*else {
                getNotifications(globalContent.getCurrentlySelectedActivityInDashboard(), false);
            }*/
        }
    }

    public void getDashboardNotifications(View view) {
        boolean checked = ((RadioButton) view).isChecked();

        String activityType = Constants.NOTIFICATION_ALL;

        // Checking which radio button was clicked
        switch (view.getId()) {
            case R.id.rb_notifications_all:
                if (checked)
                    activityType = Constants.NOTIFICATION_ALL;
                break;
            case R.id.rb_notifications_calls:
                if (checked)
                    activityType = Constants.NOTIFICATION_CALLS;
                break;
            case R.id.rb_notifications_meetings:
                if (checked)
                    activityType = Constants.NOTIFICATION_MEETINGS;
                break;
            case R.id.rb_notifications_tasks:
                if (checked)
                    activityType = Constants.NOTIFICATION_TASKS;
                break;
        }

        getNotifications(activityType, false);
    }

    public void getNotifications(String activityType, boolean resetRadioButton) {
        if (!MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.showProgressDialog(activityContext);

        dashboardModel.getDashboardNotifications(encryptedClientKey, getDTTicks(), activityType, resetRadioButton);
        globalContent.setCurrentlySelectedActivityInDashboard(activityType);
    }

    public void updateNotifications(List<Notification> notifications, boolean needToCache, boolean resetRadioButton) {
        dashboardFragment.updateNotifications(notifications);

        if (needToCache)
            globalContent.setDashboardNotifications(notifications);

        if (resetRadioButton)
            dashboardFragment.resetNotificationsRadioButtons(globalContent.getCurrentlySelectedActivityInDashboard());

        // Loading website config details after loading recent contacts & notification to reduce loading time of dashboard (only one time & in background).
        if (!sharedPreferences.getBoolean(Constants.SP_IS_WEBSITE_CONFIG_LOADED)) {
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_WEBSITE_CONFIG);
        }
    }

    public void updateWebsiteConfigData(WebsiteConfig config) {
        sharedPreferences.putBoolean(Constants.SP_LIMIT_CUSTOMER_SEARCH_BY_REP, config.isLimitCustomerSearchByRep());
        sharedPreferences.putBoolean(Constants.SP_LIMIT_CUSTOMER_ADD_BY_REP, config.isLimitCustomerAddByRep());

        sharedPreferences.putBoolean(Constants.SP_IS_WEBSITE_CONFIG_LOADED, true);
    }

    public void showAllMyContacts(View view) {
        displayView(1);
    }

    @Override
    public void goToContactDetailPage(QuickSearchContact contact) {
        if (contact.getType().equals("Rep")) {
            Intent showRepContactsIntent = new Intent(activityContext, RepContactsActivity.class);
            showRepContactsIntent.putExtra(Constants.BUNDLE_SELECTED_REP_ID, contact.getId());
            showRepContactsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(showRepContactsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            contactsModel.getCustomerBasicDetails(encryptedClientKey, getDTTicks(), contact.getId());
            quickSearchContactType = contact.getType();
        }

        if (dashboardFragment != null)
            dashboardFragment.resetQuickSearchTextFiled(false);
    }

    public void updateCustomerBasicDetails(DetailContact detailContact) {
        MMProgressDialog.hideProgressDialog();

        if (Constants.CONTACT_NO_ACCESS.equals(String.valueOf(detailContact.getCanViewEmployeeId()))) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, detailContact);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.DASHBOARD_QUICK_SEARCH);
            //openContactDetailsIntent.putExtra(Constants.BUNDLE_QUICK_SEARCH_CONTACT_TYPE, quickSearchContactType);
            openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void startVoiceRecording(View view) {
        int tag;
        if (view == null)
            tag = 1;
        else
            tag = Integer.parseInt(view.getTag().toString());

        if (tag == 1) {
            if (dashboardFragment != null)
                dashboardFragment.resetQuickSearchTextFiled(false);

            if (Utility.verifyPermissions(this, Constants.PERMISSION_RECORD_AUDIO, Constants.PERMISSIONS_RECORD_AUDIO, Constants.RECORD_AUDIO_REQUEST_CODE)) {
                Intent recordSpeechIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                recordSpeechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");
                recordSpeechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
                recordSpeechIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1);
                recordSpeechIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Listening speak now....");
                //recordSpeechIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, "Speak Now");

                if (recordSpeechIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(recordSpeechIntent, Constants.RECORD_AUDIO_REQUEST_CODE);
                } else {
                    displayToast(getResources().getString(R.string.no_support_for_voice_record));
                }
            }
        } else {
            if (dashboardFragment != null)
                dashboardFragment.resetQuickSearchTextFiled(true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.RECORD_AUDIO_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startVoiceRecording(null);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_RECORD_AUDIO)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_RECORD_AUDIO);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.record_audio_permission_title), getResources().getString(R.string.record_audio_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.record_audio_permission_denied));
                    }
                }
                break;
            case Constants.MAKE_PHONE_CALL_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsToMakePhoneCall();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.phone_call_permission_denied));
                    }
                }
                break;

        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_RECORD_AUDIO) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_RECORD_AUDIO, Constants.RECORD_AUDIO_REQUEST_CODE);
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                checkPermissionsToMakePhoneCall();
            }
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case LOCATION_SETTINGS_REQUEST_CODE:
                System.out.println("====result callback ");
                checkLocationAccess();
                break;

            case Constants.RECORD_AUDIO_REQUEST_CODE:
                if (resultCode == RESULT_OK && data != null) {
                    ArrayList<String> speechResults = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    if (dashboardFragment != null)
                        dashboardFragment.updateQuickSearchTextFiledWithVoiceSearchResults(speechResults);
                }
                break;

            case Constants.OPEN_APPLICATION_SETTINGS:
                if (mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH) || mmSettingsPreferences.getBooleanValue(CALLS_SWITCH)) {
                    CheckAndIgnoreBatteryOptimisation();
                }

                break;
            case Constants.REQUEST_BATTERY_OPTIMISATION_CODE:
                //for dont optimise -->requestCode=106, resultCode=0
                //for optimise -->requestCode=106, resultCode=-1
                if (resultCode == 0) {
                    //resetting settings ,if the battery optimisation message is cancelled by the user
                    mmSettingsPreferences.putBooleanValue(MEETINGS_SWITCH, false);
                    mmSettingsPreferences.putBooleanValue(CALLS_SWITCH, false);
                    mmSettingsPreferences.putBooleanValue(REPEATS_SWITCH, false);
                    //  CheckAndIgnoreBatteryOptimisation();
                } else {
                    startNotificationService();
                }
                break;

            case Constants.OPEN_APP_NOTIFICATIONS_SETTINGS: {
                //if enabled
                if (checkNotificationSettingsClass.checkNotificationsEnabledOrNot()) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        // check channel enabled or not
                        checkNotificationSettingsClass.handleNotificationChannels();
                    } else {
                        if (mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_permissions_dialog_switch")) {
                            showAlertDialog();
                        }
                        startService(serintent);
                    }

                } else {
                    checkNotificationSettingsClass.handleNotifications();
                }


                break;
            }

            case Constants.OPEN_APP_NOTIFICATION_CHANNEL_SETTINGS: {
                if (checkNotificationSettingsClass.checkNotificationChannelsEnabledOrNot()) {
                    if (mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_permissions_dialog_switch")) {
                        showAlertDialog();

                    }
                    startService(serintent);
                } else {
                    checkNotificationSettingsClass.handleNotificationChannels();
                }
                break;
            }


        }
    }

    /* Dial Option feature from Recent Contacts Starts Here */

    @Override
    public void takeConfirmationToMakePhoneCall(String customerId, String phoneNumber, String extension) {
        this.customerId = customerId;
        this.phoneNumber = phoneNumber;
        this.extension = extension;

        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
    }

    public void checkPermissionsToMakePhoneCall() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
            makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, phoneNumber, extension, false);
        }
    }

    /* Dial Option feature from Recent Contacts Ends Here */


    public void openDeviceSettings() {
        startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), Constants.OPEN_DEVICE_SETTINGS);
    }


    public void CheckAndIgnoreBatteryOptimisation() {
        enableDeviceBootReceiver();
        checkNotificationSettingsClass = new CheckNotificationSettingsClass();

        serintent = new Intent(this, NotificationService.class);
        PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
        String packageName = getPackageName();
        Intent intent = new Intent();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            boolean ignoringOptimizations = pm.isIgnoringBatteryOptimizations(packageName);
            if (!ignoringOptimizations) {
                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                intent.setData(Uri.parse("package:" + packageName));
                startActivityForResult(intent, Constants.REQUEST_BATTERY_OPTIMISATION_CODE);
                // intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
                // startActivity(intent);
            } else {
                startNotificationService();
            }
        } else {
            startNotificationService();
        }
    }

    public void startNotificationService() {
        checkNotificationSettingsClass.handleNotifications();
    }


    public void enableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }


    public void disableDeviceBootReceiver() {
        ComponentName receiver = new ComponentName(this, DeviceBootReceiver.class);
        PackageManager pm = getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    public void notificationSettingsDialog() {
        try {
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            MMAlertDialog.showAlertDialogWithCallbackAndCustomView(activityContext, "Enable Notifications", "Please enable the calls and meetings in the settings screen to get notifications about scheduled calls and meetings.", true, R.layout.alert_dialog_checkbox_layout, this, "EnableNotificationsDialog");
            MMAlertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    Intent intent = new Intent(DashboardActivity.this, SettingsActivity.class);
                    startActivityForResult(intent, Constants.OPEN_APPLICATION_SETTINGS);
                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    //if allow meeting or allow call is not set , then pop up alert dialog and divert the user to the settings screen. alert everytime the app is opened ,
    // because scheduling notifications are very important.
    //After setting atleast one value, check the battery optimisation settings.

    public void showAlertDialog() {
        try {
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_COMMON);
            MMAlertDialog.showAlertDialogWithCallbackAndCustomView(activityContext, "Requires Permission", "Please Whitelist MirabelMobile application by Enabling AutoStart/AutoLaunch in Permissions from System Settings for scheduling notifications .", true, R.layout.alert_dialog_checkbox_layout, this, "RequiresPermissionDialog");
            MMAlertDialog.setListener(new AlertDialogSelectionListener() {
                @Override
                public void alertDialogCallback() {
                    openDeviceSettings();
                }

                @Override
                public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void rememberCheckboxStatus(boolean checkboxStatus, String alertDialogType) {

        switch (alertDialogType) {
            case "RequiresPermissionDialog":

                mmSettingsPreferences.putBooleanValue("allow_permissions_dialog_switch", !checkboxStatus);
                break;
            case "EnableNotificationsDialog":
                mmSettingsPreferences.putBooleanValue("allow_enable_notifications_dialog_switch", !checkboxStatus);
                break;
        }
    }

    public class CheckNotificationSettingsClass {

        void handleNotifications() {
            //for checking notifications enabled or not
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                if (!notificationManager.areNotificationsEnabled()) {
                    openNotificationSettings();
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        // check channel enabled or not
                        if (checkNotificationChannelsEnabledOrNot()) {
                            if (mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_permissions_dialog_switch")) {
                                showAlertDialog();
                            }
                            startService(serintent);
                        } else {
                            checkNotificationSettingsClass.handleNotificationChannels();
                        }

                    } else {
                        if (mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_permissions_dialog_switch")) {
                            showAlertDialog();
                        }
                        startService(serintent);
                    }
                }
            } else {
                if (!NotificationManagerCompat.from(getApplicationContext()).areNotificationsEnabled()) {
                    //alert dialog for the user to divert to system notifications screen.
                    openNotificationSettings();
                } else {
                    if (mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch") || !mmSettingsPreferences.checkKeyAvailability("allow_permissions_dialog_switch")) {
                        showAlertDialog();
                    }
                    startService(serintent);


                }
            }

        }

        boolean checkNotificationsEnabledOrNot() {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return notificationManager.areNotificationsEnabled();
            } else {
                return NotificationManagerCompat.from(getApplicationContext()).areNotificationsEnabled();
            }
        }


        void handleNotificationChannels() {
            //for checking notification channels enabled or not
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O &&
                    isChannelBlocked()) {
                openChannelSettings();

            }
        }

        boolean checkNotificationChannelsEnabledOrNot() {
            //for checking notification channels enabled or not
            return Build.VERSION.SDK_INT < Build.VERSION_CODES.O ||
                    !isChannelBlocked();
        }

        private void openNotificationSettings() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Intent intent = new Intent(Settings.ACTION_APP_NOTIFICATION_SETTINGS);
                intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
                // startActivity(intent);
                startActivityForResult(intent, Constants.OPEN_APP_NOTIFICATIONS_SETTINGS);
            } else {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                intent.setData(Uri.parse("package:" + getPackageName()));
                //startActivity(intent);
                startActivityForResult(intent, Constants.OPEN_APP_NOTIFICATIONS_SETTINGS);
            }
        }


        @RequiresApi(26)
        private boolean isChannelBlocked() {
            NotificationManager manager = getSystemService(NotificationManager.class);
            NotificationChannel channel = manager.getNotificationChannel(Constants.CALLS_AND_MEETINGS_CHANNEL_ID);

            return channel != null &&
                    channel.getImportance() == NotificationManager.IMPORTANCE_NONE;
        }

        @RequiresApi(26)
        private void openChannelSettings() {
            Intent intent = new Intent(Settings.ACTION_CHANNEL_NOTIFICATION_SETTINGS);
            intent.putExtra(Settings.EXTRA_APP_PACKAGE, getPackageName());
            intent.putExtra(Settings.EXTRA_CHANNEL_ID, Constants.CALLS_AND_MEETINGS_CHANNEL_ID);
            //startActivity(intent);
            startActivityForResult(intent, Constants.OPEN_APP_NOTIFICATION_CHANNEL_SETTINGS);
        }

    }


}
