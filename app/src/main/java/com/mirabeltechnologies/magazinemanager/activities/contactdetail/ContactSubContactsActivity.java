package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateContactActivity;
import com.mirabeltechnologies.magazinemanager.adapters.ContactSubContactsAdapter;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;

import java.util.List;

public class ContactSubContactsActivity extends BaseActivity {
    public static final String TAG = ContactSubContactsActivity.class.getSimpleName();

    private ImageView create_sub_contact_icon;
    private ListView contact_sub_contacts_list_view;
    private ContactSubContactsAdapter subContactsAdapter;
    private ContactsModel contactsModel;
    private String selectedCustomerId;
    private boolean isCompanyAlreadyHaveBillingContact = false;
    private DetailContact parentContactDetails = null;
    private int canViewEmployeeId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_sub_contacts);

        try {
            activityContext = ContactSubContactsActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
                parentContactDetails = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
            }

            contactsModel = new ContactsModel(activityContext, this, Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE);

            create_sub_contact_icon = (ImageView) findViewById(R.id.create_sub_contact_icon);
            contact_sub_contacts_list_view = (ListView) findViewById(R.id.contact_sub_contacts_list_view);
            subContactsAdapter = new ContactSubContactsAdapter(activityContext);
            contact_sub_contacts_list_view.setAdapter(subContactsAdapter);

            if (parentContactDetails != null)
                canViewEmployeeId = parentContactDetails.getCanViewEmployeeId();

            if (String.valueOf(canViewEmployeeId).equals(Constants.CONTACT_READ_ONLY_ACCESS))
                create_sub_contact_icon.setVisibility(View.GONE);

            loadDataFromServer(true);

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshSubContactsBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshSubContactsBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer(boolean showLoadingIndicator) {
        if (showLoadingIndicator)
            MMProgressDialog.showProgressDialog(activityContext);

        contactsModel.getSubContactsForCustomerId(encryptedClientKey, getDTTicks(), selectedCustomerId);
    }

    public void updateSubContactsList(final List<Contact> subContactsList) {
        if (subContactsList == null || subContactsList.isEmpty()) {
            contact_sub_contacts_list_view.addHeaderView(no_records_found_text_view);
        } else {
            // Removing no records found message if already added to list view.
            if (contact_sub_contacts_list_view.getHeaderViewsCount() > 0) {
                contact_sub_contacts_list_view.removeHeaderView(no_records_found_text_view);
            }

            subContactsAdapter.setContactsList(subContactsList);
            subContactsAdapter.notifyDataSetChanged();

            contact_sub_contacts_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    openContactDetailPage(subContactsList.get(position));
                }
            });

            isCompanyAlreadyHaveBillingContact = checkForBillingContact(subContactsList);
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void openContactDetailPage(Contact contact) {
        Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, contact);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE);
        openContactDetailsIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(openContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void showCreateSubContactPage(View view) {
        Intent showCreateContactIntent = new Intent(activityContext, CreateContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE);
        bundle.putString(Constants.BUNDLE_CURRENT_OPERATION, Constants.CURRENT_OPERATION_CREATE_SUB_CONTACT);
        bundle.putSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS, parentContactDetails);
        bundle.putBoolean(Constants.BUNDLE_IS_COMPANY_ALREADY_HAVE_BILLING_CONTACT, isCompanyAlreadyHaveBillingContact);
        bundle.putBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT, false);
        showCreateContactIntent.putExtras(bundle);
        startActivity(showCreateContactIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRefreshSubContactsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    loadDataFromServer(false);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
