package com.mirabeltechnologies.magazinemanager.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.Manifest;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.CheckInSubmitResponse;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import okhttp3.OkHttpClient;


public class CheckInActivity extends BaseActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    Context mContext;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    LocationRequest mLocationRequest;
    private GoogleMap mMap;
    TextView address_text;
    ImageView img_history;
    EditText ed_description;
    Button submit_button;
    String description;
    String address="";
    Integer userId;
    String selectedCustomerId;
    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private ProgressDialog progressDialog;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_layout);
        getSupportActionBar().hide();
        mContext = this;
        submit_button=findViewById(R.id.submit_button);
        ed_description=findViewById(R.id.ed_description);
        img_history=findViewById(R.id.img_history);
        address_text = findViewById(R.id.address_text);
        userId=sharedPreferences.getInt(Constants.SP_LOGGED_IN_REP_ID);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
        }
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager()
                        .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
        img_history.setOnClickListener(v -> {
           Intent intent = new Intent(activityContext, CheckInHistoryActivity.class);
           intent.putExtra("customerId",selectedCustomerId);
           startActivity(intent);
           overridePendingTransition(R.anim.right_in, R.anim.left_out);
        });
        submit_button.setOnClickListener(v -> {
            description=ed_description.getText().toString();
            if(mLastLocation!=null) {
                if(!TextUtils.isEmpty(description)) {
                    postData(mLastLocation,description,address);
                } else {
                    Toast.makeText(mContext, "Please Enter Description..", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(mContext, "Please wait to fetch the Location..", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ed_description.setText("");
    }

    private void postData(Location mLastLocation, String description, String address) {
        progressDialog = ProgressDialog.show(CheckInActivity.this, "Please wait.", "Service Loading ..!", true);
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        String authToken = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        String domain= sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
        try {
            if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
                Log.e("veera",Constants.CHECK_IN_URL);
                JSONObject reqObject = new JSONObject();
                JSONObject userObject = new JSONObject();
                userObject.put("ID",userId);
                JSONObject cusObject = new JSONObject();
                cusObject.put("ID",Integer.parseInt(selectedCustomerId));
                reqObject.put("user",userObject);
                reqObject.put("Customer",cusObject);
                reqObject.put("Location",address);
                reqObject.put("Latitude",mLastLocation.getLatitude());
                reqObject.put("Longitude",mLastLocation.getLongitude());
                reqObject.put("Note",description);
                Log.e("veera",reqObject.toString());
                JsonObjectRequest createNewActivityReq = new JsonObjectRequest(Request.Method.POST, Constants.CHECK_IN_URL,reqObject,
                        response -> {
                            Log.e("veera",response.toString());
                            progressDialog.dismiss();
                            Gson gson = new Gson();
                            CheckInSubmitResponse submitResponse = gson.fromJson(response.toString(), CheckInSubmitResponse.class);
                            if(submitResponse!=null && submitResponse.getContent()!=null){
                                if("Success".equalsIgnoreCase(submitResponse.getContent().getStatus())){
                                    Toast.makeText(mContext, submitResponse.getContent().getStatus(), Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(activityContext, CheckInHistoryActivity.class);
                                    intent.putExtra("customerId",selectedCustomerId);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                                } else {
                                    Toast.makeText(mContext, submitResponse.getContent().getStatus(), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(mContext, getResources().getString(R.string.error_failure), Toast.LENGTH_SHORT).show();
                            }
                        },
                        error -> {
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            Log.e("veera",error.toString());
                            onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", authToken);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        Log.e("veera",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(activityContext).addToRequestQueue(createNewActivityReq, "veera");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                    mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        mLastLocation = location;
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }
//Showing Current Location Marker on Map
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        LocationManager locationManager = (LocationManager)
                getSystemService(Context.LOCATION_SERVICE);
        String provider = locationManager.getBestProvider(new Criteria(), true);
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location locations = locationManager.getLastKnownLocation(provider);

        List<String> providerList = locationManager.getAllProviders();
        if (null != locations && null != providerList && providerList.size() > 0) {
            double longitude = locations.getLongitude();
            double latitude = locations.getLatitude();
            Geocoder geocoder = new Geocoder(getApplicationContext(),
                    Locale.getDefault());
            try {
                List<Address> listAddresses = geocoder.getFromLocation(latitude, longitude, 1);
                if (null != listAddresses && listAddresses.size() > 0) {

                    address = listAddresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = listAddresses.get(0).getLocality();
                    String state = listAddresses.get(0).getAdminArea();
                    String country = listAddresses.get(0).getCountryName();
                    String postalCode = listAddresses.get(0).getPostalCode();
                    String knownName = listAddresses.get(0).getFeatureName(); // Only if available else return NULL
                    String subLocality = listAddresses.get(0).getSubLocality();
                   // markerOptions.title("" + latLng + "," + subLocality + "," + state + "," + country);
                    markerOptions.title(address);
                    address_text.setText(address);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        mCurrLocationMarker = mMap.addMarker(markerOptions);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient,
                    this);
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    if (mGoogleApiClient == null) {
                        buildGoogleApiClient();
                    }
                    mMap.setMyLocationEnabled(true);
                }
            } else {
                Toast.makeText(this, "permission denied",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
