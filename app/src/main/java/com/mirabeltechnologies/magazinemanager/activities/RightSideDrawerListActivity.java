package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SimpleListAdapter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;

import java.util.List;

public class RightSideDrawerListActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    private MMTextView titleTextView;
    private ListView listView;
    private List listValues;
    private SimpleListAdapter listAdapter;
    private Constants.RequestFrom requestFrom;
    private Constants.SelectionType selectionType;
    public static int rightSideDrawerSelectedIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right_side_drawer_list);

        try {
            activityContext = RightSideDrawerListActivity.this;

            Bundle bundle = getIntent().getExtras();

            if (bundle != null) {
                requestFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
                selectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                rightSideDrawerSelectedIndex = bundle.getInt(Constants.BUNDLE_RSD_SELECTED_INDEX);
            }

            titleTextView = (MMTextView) findViewById(R.id.right_side_drawer_title);

            if (selectionType == Constants.SelectionType.REP) {
                titleTextView.setText("Choose Rep");
                listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
            }

            listView = (ListView) findViewById(R.id.right_side_drawer_list_view);
            listAdapter = new SimpleListAdapter(activityContext, R.layout.right_side_drawer_list_view_item, listValues, selectionType);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        rightSideDrawerSelectedIndex = position;
        listAdapter.notifyDataSetChanged();

        sendBroadcastWithSelectedIndex();
    }

    public void sendBroadcastWithSelectedIndex() {
        try {
            Intent intent = null;
            Bundle bundle = new Bundle();

            if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE);
            } else
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION);

            bundle.putInt(Constants.BUNDLE_RSD_SELECTED_INDEX, rightSideDrawerSelectedIndex);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
            intent.putExtras(bundle);

            // Broadcasting Selection Index to Receiver Activity
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);

            // Closing Activity
            closeActivity(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
