package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Spinner;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.SpinnerAdapter;
import com.mirabeltechnologies.magazinemanager.beans.DateRange;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SearchOrdersFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.OrdersModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SearchOrdersFiltersActivity extends BaseActivity implements AdapterView.OnItemSelectedListener {
    public static final String TAG = SearchOrdersFiltersActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom, requestCameFrom;
    private CommonModel commonModel;
    private OrdersModel ordersModel;
    private SearchOrdersFilter searchOrdersFilter;

    private MMTextView search_orders_filters_sales_rep, search_orders_filters_business_unit, search_orders_filters_product, search_orders_filters_magazine, search_orders_filters_rate_card, search_orders_filters_year, search_orders_filters_issue,
            search_orders_filters_date_range, search_orders_filters_from_date, search_orders_filters_to_date;
    private Spinner search_orders_filters_product_type_spinner, search_orders_filters_products_spinner, search_orders_filters_amount_types_spinner;
    private MMButton search_orders_filters_reset_button, search_orders_filters_search_button;

    private SpinnerAdapter productTypesSpinnerAdapter, productsSpinnerAdapter, amountTypesSpinnerAdapter;
    private Map<String, Integer> productTypesMap, amountTypesMap;
    private ArrayList<String> productTypesMapKeys, productsArrayList, amountTypesMapKeys;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<MasterData> allBusinessUnits = null, allProducts = null, allMagazines = null, allRateCards = null, allIssueYears = null;
    public static ArrayList<MasterData> allIssues = null;
    private ArrayList<DateRange> dateRangesArrayList;
    private String currentDate, preSelectedDate = "", displayFromDate, displayToDate;
    private boolean isLoggedInRepExistsInAllRepsList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_orders_filters_screen);

        try {
            activityContext = SearchOrdersFiltersActivity.this;

            requestFrom = Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
                searchOrdersFilter = (SearchOrdersFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ORDERS_FILTER_OBJECT);
            }

            // When we came from dashboard left menu, we are not sending search orders filter object so we don't have it.
            if (searchOrdersFilter == null)
                searchOrdersFilter = new SearchOrdersFilter();

            commonModel = new CommonModel(activityContext, this, requestFrom);
            ordersModel = new OrdersModel(activityContext, this, requestFrom);

            // Initializing Values
            currentDate = Utility.formatDate(new Date(), Constants.SERVER_DATE_FORMAT);

            productTypesMap = new LinkedHashMap<>();
            productTypesMap.put("All", -1); // default
            productTypesMap.put("Print", 1);
            productTypesMap.put("Digital", 3);

            productTypesMapKeys = new ArrayList<>(productTypesMap.keySet());

            productsArrayList = new ArrayList<>();
            productsArrayList.add("with issues"); // default
            productsArrayList.add("with out issues");
            productsArrayList.add("both");

            amountTypesMap = new LinkedHashMap<>();
            amountTypesMap.put("Use Billing Installment Amounts", 0); // default
            amountTypesMap.put("Use Net on Issue/Start Date", 1);
            amountTypesMap.put("Use Net on Contract Added Date", 2);

            amountTypesMapKeys = new ArrayList<>(amountTypesMap.keySet());

            dateRangesArrayList = new ArrayList<>();
            dateRangesArrayList.add(new DateRange(0, "Today"));
            dateRangesArrayList.add(new DateRange(1, "Tomorrow"));
            dateRangesArrayList.add(new DateRange(2, "Last Week"));
            dateRangesArrayList.add(new DateRange(3, "This Week"));
            dateRangesArrayList.add(new DateRange(4, "Next Week"));
            dateRangesArrayList.add(new DateRange(5, "Last Month"));
            dateRangesArrayList.add(new DateRange(6, "This Month"));
            dateRangesArrayList.add(new DateRange(7, "Next Month"));
            dateRangesArrayList.add(new DateRange(8, "Last Year"));
            dateRangesArrayList.add(new DateRange(9, "This Year"));
            dateRangesArrayList.add(new DateRange(10, "Next Year"));
            dateRangesArrayList.add(new DateRange(11, "All"));

            // Initializing UI References
            search_orders_filters_sales_rep = (MMTextView) findViewById(R.id.search_orders_filters_sales_rep);
            search_orders_filters_product_type_spinner = (Spinner) findViewById(R.id.search_orders_filters_product_type_spinner);
            search_orders_filters_business_unit = (MMTextView) findViewById(R.id.search_orders_filters_business_unit);
            search_orders_filters_product = (MMTextView) findViewById(R.id.search_orders_filters_product);
            search_orders_filters_magazine = (MMTextView) findViewById(R.id.search_orders_filters_magazine);
            search_orders_filters_rate_card = (MMTextView) findViewById(R.id.search_orders_filters_rate_card);
            search_orders_filters_products_spinner = (Spinner) findViewById(R.id.search_orders_filters_products_spinner);
            search_orders_filters_year = (MMTextView) findViewById(R.id.search_orders_filters_year);
            search_orders_filters_issue = (MMTextView) findViewById(R.id.search_orders_filters_issue);
            search_orders_filters_date_range = (MMTextView) findViewById(R.id.search_orders_filters_date_range);
            search_orders_filters_from_date = (MMTextView) findViewById(R.id.search_orders_filters_from_date);
            search_orders_filters_to_date = (MMTextView) findViewById(R.id.search_orders_filters_to_date);
            search_orders_filters_amount_types_spinner = (Spinner) findViewById(R.id.search_orders_filters_amount_types_spinner);
            search_orders_filters_reset_button = (MMButton) findViewById(R.id.search_orders_filters_reset_button);
            search_orders_filters_search_button = (MMButton) findViewById(R.id.search_orders_filters_search_button);

            productTypesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, productTypesMapKeys);
            productTypesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_orders_filters_product_type_spinner.setAdapter(productTypesSpinnerAdapter);
            search_orders_filters_product_type_spinner.setSelection(0, false); // here animate is false in order to prevent execution of onItemSelected() on initial loading.
            search_orders_filters_product_type_spinner.setOnItemSelectedListener(this);

            productsSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, productsArrayList);
            productsSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_orders_filters_products_spinner.setAdapter(productsSpinnerAdapter);
            search_orders_filters_products_spinner.setSelection(0, false);
            search_orders_filters_products_spinner.setOnItemSelectedListener(this);

            amountTypesSpinnerAdapter = new SpinnerAdapter(activityContext, R.layout.spinner_row_item, amountTypesMapKeys);
            amountTypesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            search_orders_filters_amount_types_spinner.setAdapter(amountTypesSpinnerAdapter);
            search_orders_filters_amount_types_spinner.setSelection(1, false);
            search_orders_filters_amount_types_spinner.setOnItemSelectedListener(this);

            search_orders_filters_sales_rep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v);
                }
            });

            search_orders_filters_business_unit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllBusinessUnits();
                }
            });

            search_orders_filters_product.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllProducts();
                }
            });

            search_orders_filters_magazine.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllMagazines();
                }
            });

            search_orders_filters_rate_card.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRateCards();
                }
            });

            search_orders_filters_year.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllIssueYears();
                }
            });

            search_orders_filters_issue.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllIssues();
                }
            });

            search_orders_filters_date_range.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDateRanges();
                }
            });

            search_orders_filters_from_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker(view, Constants.SelectionType.FROM_DATE);
                }
            });

            search_orders_filters_to_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker(view, Constants.SelectionType.TO_DATE);
                }
            });

            search_orders_filters_reset_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    resetValues(view);
                }
            });

            search_orders_filters_search_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    applySearchOrdersFilters();
                }
            });

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void resetValues(View view) {
        allBusinessUnits = new ArrayList<>();
        allProducts = new ArrayList<>();
        allMagazines = new ArrayList<>();
        allRateCards = new ArrayList<>();
        allIssueYears = new ArrayList<>();
        allIssues = new ArrayList<>();

        if (view != null) {
            searchOrdersFilter = new SearchOrdersFilter();
        }

        // Setting default values
        if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {

            isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));

            if (isLoggedInRepExistsInAllRepsList) {
                searchOrdersFilter.setSalesRepId(String.valueOf(loggedInRepId));
                searchOrdersFilter.setSalesRepName(loggedInRepName);
            } else {
                searchOrdersFilter.setSalesRepId("-1");
                searchOrdersFilter.setSalesRepName("");
            }
        }

        if (view != null) {
            searchOrdersFilter.setSalesRepId("-1");
            searchOrdersFilter.setSalesRepName("All Reps");
            searchOrdersFilter.setBusinessUnit("All");
            searchOrdersFilter.setProduct("All");
            searchOrdersFilter.setMagazine("All");
        }

        if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {
            searchOrdersFilter.setProductTypeIndex(0);
            searchOrdersFilter.setProductType(productTypesMapKeys.get(searchOrdersFilter.getProductTypeIndex()));
            searchOrdersFilter.setProductTypeValue(productTypesMap.get(searchOrdersFilter.getProductType()));

            searchOrdersFilter.setProductsIndex(0);
            searchOrdersFilter.setProductsValue(productsArrayList.get(searchOrdersFilter.getProductsIndex()));
        }

        updateUIWithLatestValues();
    }

    public void updateUIWithLatestValues() {
        search_orders_filters_sales_rep.setText(searchOrdersFilter.getSalesRepName());
        setSpinnerSelectionWithoutCallingListener(search_orders_filters_product_type_spinner, searchOrdersFilter.getProductTypeIndex());
        search_orders_filters_business_unit.setText(searchOrdersFilter.getBusinessUnit());
        search_orders_filters_product.setText(searchOrdersFilter.getProduct());
        search_orders_filters_magazine.setText(searchOrdersFilter.getMagazine());
        search_orders_filters_rate_card.setText(searchOrdersFilter.getRateCard());
        setSpinnerSelectionWithoutCallingListener(search_orders_filters_products_spinner, searchOrdersFilter.getProductsIndex());
        search_orders_filters_year.setText(searchOrdersFilter.getIssueYear());
        search_orders_filters_issue.setText(searchOrdersFilter.getIssue());
        search_orders_filters_date_range.setText(searchOrdersFilter.getDateRangeValue());
        search_orders_filters_from_date.setText(searchOrdersFilter.getDisplayFromDate());
        search_orders_filters_to_date.setText(searchOrdersFilter.getDisplayToDate());
        setSpinnerSelectionWithoutCallingListener(search_orders_filters_amount_types_spinner, searchOrdersFilter.getAmountTypeIndex());

        if (searchOrdersFilter.getProductsIndex() == 1 || searchOrdersFilter.getProductsIndex() == 2) {
            disabledField(search_orders_filters_year);
            disabledField(search_orders_filters_issue);
        } else {
            enableField(search_orders_filters_year);
            enableField(search_orders_filters_issue);
        }
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;
        MMProgressDialog.hideProgressDialog();
        resetValues(null);
    }

    public void updateMasterData(List<MasterData> masterDataList, String fieldType) {
        switch (fieldType) {
            case Constants.GET_MASTER_DATA_BUSINESS_UNITS:
                allBusinessUnits = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();
                showAllBusinessUnits();
                break;
            case Constants.GET_MASTER_DATA_PRODUCTS:
                allProducts = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();

                showAllProducts();
                break;
            case Constants.GET_MASTER_DATA_MAGAZINES:
                allMagazines = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();

                showAllMagazines();
                break;
            case Constants.GET_MASTER_DATA_RATE_CARDS:
                allRateCards = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();

                showAllRateCards();
                break;
            case Constants.GET_MASTER_DATA_ISSUE_YEARS:
                allIssueYears = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();

                showAllIssueYears();
                break;
            case Constants.GET_MASTER_DATA_ISSUES:
                allIssues = (ArrayList) masterDataList;
                MMProgressDialog.hideProgressDialog();
                showAllIssues();
                break;
        }
    }

    public void showAllRepsList(View view) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.SALES_REP);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchOrdersFilter.getSalesRepId());
            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllBusinessUnits() {
        if (allBusinessUnits != null && allBusinessUnits.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.BUSINESS_UNIT, allBusinessUnits, searchOrdersFilter.getBusinessUnitId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_BUSINESS_UNITS);
        }
    }

    public void showAllProducts() {
        if (allProducts != null && allProducts.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.PRODUCT, allProducts, searchOrdersFilter.getProductId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_PRODUCTS);
        }
    }

    public void showAllMagazines() {
        if (allMagazines != null && allMagazines.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.MAGAZINE, allMagazines, searchOrdersFilter.getMagazineId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_MAGAZINES);
        }
    }

    public void showAllRateCards() {
        if (allRateCards != null && allRateCards.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.RATE_CARD, allRateCards, searchOrdersFilter.getRateCardId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_RATE_CARDS);
        }
    }

    public void showAllIssueYears() {
        if (allIssueYears != null && allIssueYears.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.ISSUE_YEAR, allIssueYears, searchOrdersFilter.getIssueYearId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_ISSUE_YEARS);
        }
    }

    public void showAllIssues() {
        if (allIssues != null && allIssues.size() > 0) {
            showAllValuesForSelection(Constants.SelectionType.ISSUE, allIssues, searchOrdersFilter.getIssueId());
        } else {
            MMProgressDialog.showProgressDialog(activityContext);
            ordersModel.getCascadingMasterData(encryptedClientKey, getDTTicks(), searchOrdersFilter, Constants.GET_MASTER_DATA_ISSUES);
        }
    }

    public void showAllValuesForSelection(Constants.SelectionType selectionType, ArrayList<MasterData> masterData, String preSelectedValue) {
        try {
            Intent showAllValuesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllValuesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            // We may get TransactionTooLargeException as we have more issues around 2160 and we are getting these details in adapter class from static variable #allIssues
            if (selectionType != Constants.SelectionType.ISSUE)
                bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, masterData);

            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, preSelectedValue);
            showAllValuesIntent.putExtras(bundle);
            startActivity(showAllValuesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDateRanges() {
        Intent showDateRangesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
        showDateRangesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.DATE_RANGE);
        bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, dateRangesArrayList);
        bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, searchOrdersFilter.getDateRangeValue());
        showDateRangesIntent.putExtras(bundle);
        startActivity(showDateRangesIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.SALES_REP) {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.BUSINESS_UNIT) {
                        didBusinessUnitChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PRODUCT) {
                        didProductChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.MAGAZINE) {
                        didMagazineChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.RATE_CARD) {
                        didRateCardChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.ISSUE_YEAR) {
                        didIssueYearChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.ISSUE) {
                        didIssueChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.DATE_RANGE) {
                        didDateRangeChanged((DateRange) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.SALES_REP) {
            searchOrdersFilter.setSalesRepId(selectedRep.getId());
            searchOrdersFilter.setSalesRepName(selectedRep.getName());

            search_orders_filters_sales_rep.setText(searchOrdersFilter.getSalesRepName());
        }
    }

    public void didBusinessUnitChanged(MasterData selectedBusinessUnit) {
        searchOrdersFilter.setBusinessUnit(selectedBusinessUnit.getName());
        searchOrdersFilter.setBusinessUnitId(selectedBusinessUnit.getId());

        search_orders_filters_business_unit.setText(searchOrdersFilter.getBusinessUnit());

        allProducts = new ArrayList<>();
        allRateCards = new ArrayList<>();
        allIssueYears = new ArrayList<>();
        allIssues = new ArrayList<>();
    }

    public void didProductChanged(MasterData selectedProduct) {
        searchOrdersFilter.setProduct(selectedProduct.getName());
        searchOrdersFilter.setProductId(selectedProduct.getId());

        search_orders_filters_product.setText(searchOrdersFilter.getProduct());

        allBusinessUnits = new ArrayList<>();
        allRateCards = new ArrayList<>();
        allMagazines = new ArrayList<>();
        allIssueYears = new ArrayList<>();
        allIssues = new ArrayList<>();

        searchOrdersFilter.setMagazine("All");
        searchOrdersFilter.setMagazineId("-1");
        searchOrdersFilter.setRateCard("");
        searchOrdersFilter.setRateCardId("0");
        searchOrdersFilter.setIssueYear("All Years");
        searchOrdersFilter.setIssueYearId("0");
        searchOrdersFilter.setIssue("All Issues");
        searchOrdersFilter.setIssueId("0");

        updateUIWithLatestValues();
    }

    public void didMagazineChanged(MasterData selectedMagazine) {
        searchOrdersFilter.setMagazine(selectedMagazine.getName());
        searchOrdersFilter.setMagazineId(selectedMagazine.getId());

        search_orders_filters_magazine.setText(searchOrdersFilter.getMagazine());
    }

    public void didRateCardChanged(MasterData selectedRateCard) {
        searchOrdersFilter.setRateCard(selectedRateCard.getName());
        searchOrdersFilter.setRateCardId(selectedRateCard.getId());

        search_orders_filters_rate_card.setText(searchOrdersFilter.getRateCard());
    }

    public void didIssueYearChanged(MasterData selectedIssueYear) {
        searchOrdersFilter.setIssueYear(selectedIssueYear.getName());
        searchOrdersFilter.setIssueYearId(selectedIssueYear.getId());

        search_orders_filters_year.setText(searchOrdersFilter.getIssueYear());

        searchOrdersFilter.setIssue("All Issues");
        searchOrdersFilter.setIssueId("0");

        search_orders_filters_issue.setText(searchOrdersFilter.getIssue());

        allIssues = new ArrayList<>();

        if (selectedIssueYear.getId().equals("-1")) {
            disabledField(search_orders_filters_year);
            disabledField(search_orders_filters_issue);

            searchOrdersFilter.setProductsValue("with out issues");
            searchOrdersFilter.setProductsIndex(1);
            setSpinnerSelectionWithoutCallingListener(search_orders_filters_products_spinner, searchOrdersFilter.getProductsIndex());

        } else {
            enableField(search_orders_filters_year);
            enableField(search_orders_filters_issue);
        }
    }

    public void didIssueChanged(MasterData selectedIssue) {
        searchOrdersFilter.setIssue(selectedIssue.getName());
        searchOrdersFilter.setIssueId(selectedIssue.getId());

        search_orders_filters_issue.setText(searchOrdersFilter.getIssue());

        if (selectedIssue.getId().equals("-1")) {
            searchOrdersFilter.setIssueYear("No Issues");
            searchOrdersFilter.setIssueYearId("-1");

            searchOrdersFilter.setIssue("No Issues");
            searchOrdersFilter.setIssueId("-1");

            search_orders_filters_year.setText(searchOrdersFilter.getIssueYear());

            disabledField(search_orders_filters_year);
            disabledField(search_orders_filters_issue);

            searchOrdersFilter.setProductsValue("with out issues");
            searchOrdersFilter.setProductsIndex(1);
            setSpinnerSelectionWithoutCallingListener(search_orders_filters_products_spinner, searchOrdersFilter.getProductsIndex());

        } else {
            enableField(search_orders_filters_year);
            enableField(search_orders_filters_issue);
        }
    }

    public void didDateRangeChanged(DateRange selectedDateRangeObj) {
        try {
            int selectedDateRangeIndex = selectedDateRangeObj.getIndex();

            Calendar calendar = Calendar.getInstance();
            calendar.setFirstDayOfWeek(Calendar.SUNDAY);

            Date currentDate = new Date();
            Date fromDateObj = null, toDateObj = null;

            switch (selectedDateRangeIndex) {
                case 0:
                    fromDateObj = currentDate;
                    toDateObj = currentDate;
                    break;
                case 1:
                    calendar.add(Calendar.DATE, 1);
                    fromDateObj = calendar.getTime();
                    toDateObj = fromDateObj;
                    break;
                case 2:
                    calendar.setTime(currentDate);
                    int i = calendar.get(Calendar.DAY_OF_WEEK) - calendar.getFirstDayOfWeek();
                    calendar.add(Calendar.DATE, -i - 7);
                    fromDateObj = calendar.getTime();


                    calendar.add(Calendar.DATE, 6);
                    toDateObj = calendar.getTime();
                    break;
                case 3:
                    calendar.add(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek() - calendar.get(Calendar.DAY_OF_WEEK));
                    fromDateObj = calendar.getTime();

                    calendar.add(Calendar.DAY_OF_YEAR, 6);
                    toDateObj = calendar.getTime();
                    break;
                case 4:
                    calendar.setTime(currentDate);
                    i = calendar.get(Calendar.DAY_OF_WEEK) - calendar.getFirstDayOfWeek();
                    calendar.add(Calendar.DATE, -i + 7);
                    fromDateObj = calendar.getTime();

                    calendar.add(Calendar.DAY_OF_YEAR, 6);
                    toDateObj = calendar.getTime();
                    break;
                case 5:
                    calendar.add(Calendar.MONTH, -1);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    toDateObj = calendar.getTime();
                    break;
                case 6:
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    toDateObj = calendar.getTime();
                    break;
                case 7:
                    calendar.add(Calendar.MONTH, 1);
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
                    toDateObj = calendar.getTime();
                    break;
                case 8:
                    calendar.add(Calendar.YEAR, -1);
                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
                    toDateObj = calendar.getTime();
                    break;
                case 9:
                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
                    toDateObj = calendar.getTime();
                    break;
                case 10:
                    calendar.add(Calendar.YEAR, 1);
                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
                    fromDateObj = calendar.getTime();

                    calendar.set(Calendar.DAY_OF_YEAR, calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
                    toDateObj = calendar.getTime();
                    break;
                case 11:
                    fromDateObj = null;
                    toDateObj = null;
                    break;
            }

            searchOrdersFilter.setDateRangeIndex(selectedDateRangeIndex);
            searchOrdersFilter.setDateRangeValue(selectedDateRangeObj.getValue());

            search_orders_filters_date_range.setText(searchOrdersFilter.getDateRangeValue());

            if (fromDateObj != null) {
                searchOrdersFilter.setFromDate(Utility.formatDate(fromDateObj, Constants.SERVER_DATE_FORMAT));
                searchOrdersFilter.setToDate(Utility.formatDate(toDateObj, Constants.SERVER_DATE_FORMAT));

                displayFromDate = Utility.formatDate(fromDateObj, Constants.SELECTED_DATE_DISPLAY_FORMAT);
                displayToDate = Utility.formatDate(toDateObj, Constants.SELECTED_DATE_DISPLAY_FORMAT);

                search_orders_filters_from_date.setText(displayFromDate);
                search_orders_filters_to_date.setText(displayToDate);

                searchOrdersFilter.setDisplayFromDate(displayFromDate);
                searchOrdersFilter.setDisplayToDate(displayToDate);
            } else {
                searchOrdersFilter.setFromDate("");
                searchOrdersFilter.setToDate("");

                searchOrdersFilter.setDisplayFromDate("");
                searchOrdersFilter.setDisplayToDate("");

                search_orders_filters_from_date.setText(searchOrdersFilter.getFromDate());
                search_orders_filters_to_date.setText(searchOrdersFilter.getToDate());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* AdapterView.OnItemSelectedListener Methods Starts Here */

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        try {
            int spinnersTag = Integer.parseInt(parent.getTag().toString());

            if (spinnersTag == 1) { // Product Types Spinner
                String selectedProductType = productTypesMapKeys.get(position);

                searchOrdersFilter.setProductType(selectedProductType);
                searchOrdersFilter.setProductTypeValue(productTypesMap.get(selectedProductType));
                searchOrdersFilter.setProductTypeIndex(position);

                // When product type changed, we need to reset all fields except sales rep
                allBusinessUnits = new ArrayList<>();
                allProducts = new ArrayList<>();
                allMagazines = new ArrayList<>();
                allRateCards = new ArrayList<>();
                allIssueYears = new ArrayList<>();
                allIssues = new ArrayList<>();

                searchOrdersFilter.setBusinessUnit("All");
                searchOrdersFilter.setBusinessUnitId("-1");
                searchOrdersFilter.setProduct("All");
                searchOrdersFilter.setProductId("-1");
                searchOrdersFilter.setMagazine("All");
                searchOrdersFilter.setMagazineId("-1");
                searchOrdersFilter.setRateCard("");
                searchOrdersFilter.setRateCardId("0");
                searchOrdersFilter.setIssueYear("All Years");
                searchOrdersFilter.setIssueYearId("0");
                searchOrdersFilter.setIssue("All Issues");
                searchOrdersFilter.setIssueId("0");

                updateUIWithLatestValues();

            } else if (spinnersTag == 2) { // Products Spinner
                String selectedProduct = productsArrayList.get(position);

                searchOrdersFilter.setProductsValue(selectedProduct);
                searchOrdersFilter.setProductsIndex(position);

                if (selectedProduct.equals("with issues")) {
                    searchOrdersFilter.setIssueYear("All Years");
                    searchOrdersFilter.setIssueYearId("0");

                    searchOrdersFilter.setIssue("All Issues");
                    searchOrdersFilter.setIssueId("0");

                    enableField(search_orders_filters_year);
                    enableField(search_orders_filters_issue);

                    searchOrdersFilter.setDateRangeIndex(0);
                    searchOrdersFilter.setDateRangeValue("");

                    searchOrdersFilter.setFromDate("");
                    searchOrdersFilter.setToDate("");
                } else {
                    if (selectedProduct.equals("with out issues")) {
                        searchOrdersFilter.setIssueYear("No Issues");
                        searchOrdersFilter.setIssueYearId("-1");

                        searchOrdersFilter.setIssue("No Issues");
                        searchOrdersFilter.setIssueId("-1");
                    } else if (selectedProduct.equals("both")) {
                        searchOrdersFilter.setIssueYear(" ");
                        searchOrdersFilter.setIssueYearId("-2");

                        searchOrdersFilter.setIssue(" ");
                        searchOrdersFilter.setIssueId("-2");
                    }

                    disabledField(search_orders_filters_year);
                    disabledField(search_orders_filters_issue);
                }

                search_orders_filters_year.setText(searchOrdersFilter.getIssueYear());
                search_orders_filters_issue.setText(searchOrdersFilter.getIssue());
                search_orders_filters_date_range.setText(searchOrdersFilter.getDateRangeValue());

                if (!searchOrdersFilter.getFromDate().isEmpty()) {
                    search_orders_filters_from_date.setText(Utility.formatDate(Utility.convertStringToDate(searchOrdersFilter.getFromDate(), Constants.SERVER_DATE_FORMAT), Constants.SELECTED_DATE_DISPLAY_FORMAT));
                } else {
                    search_orders_filters_from_date.setText(searchOrdersFilter.getFromDate());
                }

                if (!searchOrdersFilter.getToDate().isEmpty()) {
                    search_orders_filters_to_date.setText(Utility.formatDate(Utility.convertStringToDate(searchOrdersFilter.getToDate(), Constants.SERVER_DATE_FORMAT), Constants.SELECTED_DATE_DISPLAY_FORMAT));
                } else {
                    search_orders_filters_to_date.setText(searchOrdersFilter.getToDate());
                }
            } else if (spinnersTag == 3) { // Amount Type Spinner
                String selectedAmountType = amountTypesMapKeys.get(position);

                searchOrdersFilter.setAmountType(selectedAmountType);
                searchOrdersFilter.setAmountTypeValue(amountTypesMap.get(selectedAmountType));
                searchOrdersFilter.setAmountTypeIndex(position);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void enableField(MMTextView textView) {
        textView.setEnabled(true);
        textView.setClickable(true);
        textView.setTextColor(ContextCompat.getColor(activityContext, R.color.create_contact_edit_text_color));
    }

    public void disabledField(MMTextView textView) {
        textView.setEnabled(false);
        textView.setClickable(false);
        textView.setTextColor(ContextCompat.getColor(activityContext, R.color.gray));
    }

    /* AdapterView.OnItemSelectedListener Methods Ends Here */

    public void setSpinnerSelectionWithoutCallingListener(final Spinner spinner, final int selection) {
        if (spinner.getOnItemSelectedListener() == null)
            spinner.setOnItemSelectedListener(this);

        final AdapterView.OnItemSelectedListener listener = spinner.getOnItemSelectedListener();
        spinner.setOnItemSelectedListener(null);
        spinner.post(new Runnable() {
            @Override
            public void run() {
                spinner.setSelection(selection);
                spinner.post(new Runnable() {
                    @Override
                    public void run() {
                        spinner.setOnItemSelectedListener(listener);
                    }
                });
            }
        });
    }

    public void showDatePicker(View view, final Constants.SelectionType selectionType) {
        try {
            if (selectionType == Constants.SelectionType.FROM_DATE)
                preSelectedDate = searchOrdersFilter.getFromDate();
            else if (selectionType == Constants.SelectionType.TO_DATE)
                preSelectedDate = searchOrdersFilter.getToDate();

            if (preSelectedDate.isEmpty()) {
                preSelectedDate = currentDate;
            }

            String[] dateValues = preSelectedDate.split("-");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, monthOfYear, dayOfMonth);

                    if (selectionType == Constants.SelectionType.FROM_DATE) {
                        displayFromDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
                        preSelectedDate = Utility.formatDate(calendar.getTime(), Constants.SERVER_DATE_FORMAT);

                        search_orders_filters_from_date.setText(displayFromDate);
                        searchOrdersFilter.setFromDate(preSelectedDate);
                        searchOrdersFilter.setDisplayFromDate(displayFromDate);
                    } else if (selectionType == Constants.SelectionType.TO_DATE) {
                        displayFromDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
                        preSelectedDate = Utility.formatDate(calendar.getTime(), Constants.SERVER_DATE_FORMAT);

                        search_orders_filters_to_date.setText(Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT));
                        searchOrdersFilter.setToDate(preSelectedDate);
                        searchOrdersFilter.setDisplayToDate(displayFromDate);
                    }

                }
            }, Integer.parseInt(dateValues[0]), Integer.parseInt(dateValues[1]) - 1, Integer.parseInt(dateValues[2]));

            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void applySearchOrdersFilters() {
        try {
            boolean sendRequest = false;
            String message = "";

            if (Integer.parseInt(searchOrdersFilter.getIssueYearId()) > 0 || Integer.parseInt(searchOrdersFilter.getIssueId()) > 0) {
                sendRequest = true;
            } else {
                if (searchOrdersFilter.getFromDate().length() > 0 || searchOrdersFilter.getToDate().length() > 0) {
                    if ((searchOrdersFilter.getFromDate().length() > 0 && searchOrdersFilter.getToDate().isEmpty()) || (searchOrdersFilter.getFromDate().isEmpty() && searchOrdersFilter.getToDate().length() > 0)) {
                        sendRequest = false;
                        message = "Please enter valid date range.";
                    } else {
                        sendRequest = true;
                    }
                } else {
                    // Similar to Web, we are checking from & to date only when user selected with issues for Products
                    if (searchOrdersFilter.getProductsIndex() == 0) {
                        if (searchOrdersFilter.getFromDate().length() > 0 && searchOrdersFilter.getToDate().length() > 0) {
                            SimpleDateFormat formatter = new SimpleDateFormat(Constants.SERVER_DATE_FORMAT);
                            Date fromDate = formatter.parse(searchOrdersFilter.getFromDate());
                            Date toDate = formatter.parse(searchOrdersFilter.getToDate());

                            if (fromDate.after(toDate)) {
                                sendRequest = false;
                                message = getResources().getString(R.string.greater_to_date);
                            } else {
                                sendRequest = true;
                            }
                        } else {
                            sendRequest = false;
                            message = "Issue or Date Range criteria is mandatory";
                        }
                    } else {
                        sendRequest = true;
                    }
                }
            }

            if (sendRequest) {
                Intent intent = null;
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constants.BUNDLE_SEARCH_ORDERS_FILTER_OBJECT, searchOrdersFilter);

                // Always it will execute bcoz we are calling this screen by closing OrdersActivity and we are not calling this by calling startActivityForResult().
                if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {
                    intent = new Intent(activityContext, OrdersActivity.class);
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.left_out);
                } else {
                    intent = new Intent();
                    intent.putExtras(bundle);
                    setResult(RESULT_OK, intent);
                    overridePendingTransition(R.anim.left_in, R.anim.right_out);
                }
            } else {
                MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
