package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.adapters.ContactAddressesAdapter;
import com.mirabeltechnologies.magazinemanager.beans.CustomerAddress;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;

import java.util.List;

public class ContactAddressesActivity extends BaseActivity {
    public static final String TAG = ContactAddressesActivity.class.getSimpleName();

    private ListView contact_addresses_list_view;
    private ContactAddressesAdapter addressesAdapter;
    private ContactsModel contactsModel;
    private String selectedCustomerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_addresses);

        try {
            activityContext = ContactAddressesActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                selectedCustomerId = bundle.getString(Constants.BUNDLE_SELECTED_CUSTOMER_ID);
            }

            contactsModel = new ContactsModel(activityContext, this, Constants.RequestFrom.CONTACT_DETAIL_PAGE);

            contact_addresses_list_view = (ListView) findViewById(R.id.contact_addresses_list_view);
            addressesAdapter = new ContactAddressesAdapter(activityContext);
            contact_addresses_list_view.setAdapter(addressesAdapter);

            loadDataFromServer();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        contactsModel.getCustomerAddresses(encryptedClientKey, getDTTicks(), selectedCustomerId);
    }

    public void updateCustomerAddresses(final List<CustomerAddress> customerAddresses) {
        if (customerAddresses == null || customerAddresses.isEmpty()) {
            contact_addresses_list_view.addHeaderView(no_records_found_text_view);
        } else {
            // Removing no records found message if already added to list view.
            if (contact_addresses_list_view.getHeaderViewsCount() > 0) {
                contact_addresses_list_view.removeHeaderView(no_records_found_text_view);
            }

            addressesAdapter.setAddressesList(customerAddresses);
            addressesAdapter.notifyDataSetChanged();

            contact_addresses_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    openMapByFormattingAddress(customerAddresses.get(position));
                }
            });
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void openMapByFormattingAddress(CustomerAddress address) {
        String formatted_address = "";

        if (address.getAddress1().length() > 0)
            formatted_address += address.getAddress1() + ", ";

        if (address.getAddress2().length() > 0)
            formatted_address += address.getAddress2() + ", ";

        if (address.getCounty().length() > 0)
            formatted_address += address.getCounty() + ", ";

        if (address.getCity().length() > 0)
            formatted_address += address.getCity() + ", ";

        if (address.getState().length() > 0)
            formatted_address += address.getState() + ", ";

        if (address.getZipcode().length() > 0)
            formatted_address += address.getZipcode() + ", ";

        if (address.getInternational().length() > 0)
            formatted_address += address.getInternational() + ", ";

        if (formatted_address.length() > 0) {
            formatted_address = formatted_address.substring(0, formatted_address.length() - 2); // to remove last 2 characters comma and space.

            openMapWithLocation(formatted_address);
        }
    }
}
