package com.mirabeltechnologies.magazinemanager.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.ListViewSearchAdapter;
import com.mirabeltechnologies.magazinemanager.beans.ClientInfo;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.models.LogInModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;

import java.util.List;
import java.util.Objects;

public class ClientsListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private Context activityContext;

    private MMSharedPreferences sharedPreferences;
    private LogInModel logInModel;
    private SearchView searchView;
    private ListView clientListView;
    Button doneButton;
    ImageView continue_icon;

    private ListViewSearchAdapter listAdapter;
    private List<ClientInfo> clientList;
    private ClientInfo selectedClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clients_list);

        try {
            Context applicationContext = getApplicationContext();
            activityContext = ClientsListActivity.this;

            sharedPreferences = new MMSharedPreferences(applicationContext);

            logInModel = new LogInModel(activityContext, this, Constants.RequestFrom.CHANGE_CLIENT);

            Toolbar mToolbar = findViewById(R.id.clients_list_toolbar);
            setSupportActionBar(mToolbar);
            Objects.requireNonNull(getSupportActionBar()).setDisplayShowHomeEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setTitle("Choose Client");

            clientListView = findViewById(R.id.client_list_view);
            doneButton = findViewById(R.id.doneButton);
            continue_icon = findViewById(R.id.continue_icon);

            doneButton.setOnClickListener(v -> doneButtonClicked());

            continue_icon.setOnClickListener(v -> doneButtonClicked());

            showClientsList();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        // we are preventing back button
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        try {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            if (searchManager != null) {
                searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            }

            searchView.setQueryHint("Search Client");

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    listAdapter.filter(query);
                    return true;
                }
            });

            // Get the search close button image view
            ImageView searchCloseButton = searchView.findViewById(R.id.search_close_btn);
            searchCloseButton.setOnClickListener(view -> {
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.onActionViewCollapsed();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            searchView.setQuery("", false);
            searchView.clearFocus();
            searchView.onActionViewCollapsed();
            return true;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public void showClientsList() {
        clientList = ClientInfo.clientList;

        if (clientList.isEmpty()) {
            MMProgressDialog.showProgressDialog(activityContext);

            String email = sharedPreferences.getString(Constants.SP_EMAIL_ID);
            String pwd = sharedPreferences.getString(Constants.SP_PASSWORD);
            logInModel.validateUserLogin(email, pwd);
        } else {
            updateClientsList(clientList, false);
        }
    }

    public void logInError(int responseCode) {
        MMProgressDialog.hideProgressDialog();

        if (responseCode == Constants.MM_INVALID_LOGIN) {
            MMAlertDialog.showAlertDialog(activityContext, "Invalid Login", getResources().getString(R.string.login_response_invalid_login));
        } else if (responseCode == Constants.MM_LOGIN_SUCCESS_BUT_NO_CLIENTS) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_no_clients));
        } else if (responseCode == Constants.MM_PASSWORD_STRONG) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_not_strong));
        } else if (responseCode == Constants.MM_PASSWORD_EXPIRED) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_password_expired));
        } else if (responseCode == Constants.MM_PENDING_VERIFICATION) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Response", getResources().getString(R.string.login_response_pending_verification));
        } else if (responseCode == Constants.MM_LOGIN_ERROR) {

            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.login_error));
        } else if (responseCode == Constants.MM_INVALID_REQUEST) {
            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.invalid_request));
        } else {
            MMAlertDialog.showAlertDialog(activityContext, "Login Error", getResources().getString(R.string.common_login_response));
        }
    }

    public void updateClientsList(List<ClientInfo> clientList, boolean needToCache) {
        try {
            if (MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.hideProgressDialog();

            if (needToCache)
                ClientInfo.clientList = this.clientList = clientList;

            ClientInfo.selectedClientId = sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID);

            // If client id was not selected previously, we are assigning first client as selected client.
            if (ClientInfo.selectedClientId <= 0) {
                selectedClient = clientList.get(0);
                ClientInfo.selectedClientId = selectedClient.getClientId();
            } else {
                // Checking for previously selected client id in clientList which we get in response now, if it's now available then we are loading client which is at first index.
                int savedSelectedClientIndex = getIndexOfClientIdFromArrayList(clientList, ClientInfo.selectedClientId);
                selectedClient = clientList.get(savedSelectedClientIndex);
            }

            listAdapter = new ListViewSearchAdapter(ClientsListActivity.this, R.layout.clients_list, this.clientList, Constants.SelectionType.CLIENT);
            clientListView.setAdapter(listAdapter);
            clientListView.setOnItemClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            selectedClient = (ClientInfo) parent.getItemAtPosition(position);
            ClientInfo.selectedClientId = selectedClient.getClientId();

            listAdapter.notifyDataSetChanged();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getIndexOfClientIdFromArrayList(List<ClientInfo> list, int clientId) {
        int index = 0;

        for (int i = 0; i < list.size(); i++) {
            ClientInfo clientInfo = list.get(i);
            if (clientId == clientInfo.getClientId()) {
                index = i;
                break;
            }
        }

        return index;
    }

    public void doneButtonClicked() {

        if (ClientInfo.selectedClientId > 0) {
            MMProgressDialog.showProgressDialog(activityContext);

            sharedPreferences.putInt(Constants.SP_SELECTED_CLIENT_ID, ClientInfo.selectedClientId);

            sharedPreferences.putString(Constants.SP_LAST_REFRESHED_TIME_IN_DASHBOARD, "");

            // In order to reload website config on website change, we are resetting value.
            sharedPreferences.putBoolean(Constants.SP_IS_WEBSITE_CONFIG_LOADED, false);
            if(selectedClient!=null){
                getClientEncryptedId(selectedClient);
            }

        } else {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Please select client to proceed.");
        }
    }

    public void getClientEncryptedId(ClientInfo selectedClientDetails) {
        try {
            EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
            String encryptedId = des.getEncryptedStringForSelectedClient(selectedClientDetails);
            Log.e("veera", encryptedId);

            sharedPreferences.putString(Constants.SP_ENCRYPTED_CLIENT_KEY, encryptedId);
            sharedPreferences.putString(Constants.SP_SELECTED_SUB_DOMAIN, selectedClientDetails.getSubDomain());
            sharedPreferences.putInt(Constants.SP_SELECTED_CLIENT_ID, selectedClientDetails.getClientId());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_NAME, selectedClientDetails.getClientName());
            sharedPreferences.putString(Constants.SP_SELECTED_CLIENT_URL, selectedClientDetails.getClientURL());
            sharedPreferences.putInt(Constants.SP_LOGGED_IN_REP_ID, selectedClientDetails.getEmployeeId());
            sharedPreferences.putString(Constants.SP_SELECTED_SITE_TYPE, selectedClientDetails.getSiteType());
            sharedPreferences.putBoolean(Constants.SP_IS_DS_USER, selectedClientDetails.isDSUser());

            loadMainActivity();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadMainActivity() {
        MMProgressDialog.hideProgressDialog();
        Intent mainActivityIntent = new Intent(ClientsListActivity.this, DashboardActivity.class);
        startActivity(mainActivityIntent);
        overridePendingTransition(R.anim.top_in, R.anim.bottom_out);
        finish();
    }
}
