package com.mirabeltechnologies.magazinemanager.activities;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.widget.SwitchCompat;

import android.view.View;
import android.widget.CompoundButton;

import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.database.MMLocalDataBase;
import com.mirabeltechnologies.magazinemanager.receiver.AlarmReceiver;
import com.mirabeltechnologies.magazinemanager.util.MMSettingsPreferences;
import com.mirabeltechnologies.magazinemanager.util.MMSharedPreferences;

import java.util.ArrayList;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.CALLS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.FINGERPRINT;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.MEETINGS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.RANGE_PICKER;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.RANGE_PICKER_MAXIMUM_VALUE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.RANGE_PICKER_MINIMUM_VALUE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.REPEATS_SWITCH;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SNOOZE_TIME_PICKER;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SNOOZE_TIME_PICKER_MAXIMUM_VALUE;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.SNOOZE_TIME_PICKER_MINIMUM_VALUE;

public class SettingsActivity extends BaseActivity implements
        CompoundButton.OnCheckedChangeListener {

    Activity activity;
    SwitchCompat allowmeeting_switch, allowcalls_switch, allowrepeats_switch,allow_fingerprint;
    // SwitchCompat allow_permissions_dialog_switch, allow_enable_notifications_dialog_switch;
    NumberPicker timeIntervalPicker,range_picker;
    MMSettingsPreferences mmSettingsPreferences;
    MMLocalDataBase db;
    RelativeLayout rl_repeats_picker;
    TextView finger_title;
    LinearLayout finger_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        activity = SettingsActivity.this;
        intialiseViews();

    }
    public void intialiseViews() {
        db = new MMLocalDataBase(this);
        mmSettingsPreferences = new MMSettingsPreferences(getApplicationContext());

        timeIntervalPicker = findViewById(R.id.snooze_time_picker);
        timeIntervalPicker.setMinValue(SNOOZE_TIME_PICKER_MINIMUM_VALUE);
        timeIntervalPicker.setMaxValue(SNOOZE_TIME_PICKER_MAXIMUM_VALUE);
        timeIntervalPicker.setWrapSelectorWheel(true);
        timeIntervalPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mmSettingsPreferences.putIntegerValue(SNOOZE_TIME_PICKER, newVal);
            }
        });
        if (mmSettingsPreferences.getIntegerValue(SNOOZE_TIME_PICKER) == 0) {
            mmSettingsPreferences.putIntegerValue(SNOOZE_TIME_PICKER, SNOOZE_TIME_PICKER_MINIMUM_VALUE);
        }
        timeIntervalPicker.setValue(mmSettingsPreferences.getIntegerValue(SNOOZE_TIME_PICKER));
        finger_title=findViewById(R.id.finger_title);
        finger_layout=findViewById(R.id.finger_layout);

        range_picker=findViewById(R.id.range_picker);
        range_picker.setMinValue(RANGE_PICKER_MINIMUM_VALUE);
        range_picker.setMaxValue(RANGE_PICKER_MAXIMUM_VALUE);
        range_picker.setWrapSelectorWheel(true);
        range_picker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                mmSettingsPreferences.putIntegerValue(RANGE_PICKER, newVal);
            }
        });

        if (mmSettingsPreferences.getIntegerValue(RANGE_PICKER) == 0) {

            mmSettingsPreferences.putIntegerValue(RANGE_PICKER, SNOOZE_TIME_PICKER_MINIMUM_VALUE);
        }
        range_picker.setValue(mmSettingsPreferences.getIntegerValue(RANGE_PICKER));

        allowmeeting_switch = findViewById(R.id.allowmeeting_switch);
        allowmeeting_switch.setOnCheckedChangeListener(this);
        allowcalls_switch = findViewById(R.id.allowcalls_switch);
        allowcalls_switch.setOnCheckedChangeListener(this);
        allowrepeats_switch = findViewById(R.id.allowrepeats_switch);
        allowrepeats_switch.setOnCheckedChangeListener(this);
        allow_fingerprint = findViewById(R.id.allow_fingerprint);
        allow_fingerprint.setOnCheckedChangeListener(this);
        rl_repeats_picker = findViewById(R.id.rl_repeats_picker);

       /* allow_permissions_dialog_switch = findViewById(R.id.allow_permissions_dialog_switch);
        allow_permissions_dialog_switch.setOnCheckedChangeListener(this);
        allow_enable_notifications_dialog_switch = findViewById(R.id.allow_enable_notifications_dialog_switch);
        allow_enable_notifications_dialog_switch.setOnCheckedChangeListener(this);*/
        //bind values , if already values are set
        allowmeeting_switch.setChecked(mmSettingsPreferences.getBooleanValue(MEETINGS_SWITCH));
        allowcalls_switch.setChecked(mmSettingsPreferences.getBooleanValue(CALLS_SWITCH));
        allowrepeats_switch.setChecked(mmSettingsPreferences.getBooleanValue(REPEATS_SWITCH));
        allow_fingerprint.setChecked(mmSettingsPreferences.getBooleanValue(FINGERPRINT));

        if (mmSettingsPreferences.getBooleanValue(REPEATS_SWITCH)) {
            rl_repeats_picker.setVisibility(View.VISIBLE);
        } else {
            rl_repeats_picker.setVisibility(View.GONE);
        }

       /* allow_permissions_dialog_switch.setChecked(mmSettingsPreferences.getBooleanValue("allow_permissions_dialog_switch"));
        allow_enable_notifications_dialog_switch.setChecked(mmSettingsPreferences.getBooleanValue("allow_enable_notifications_dialog_switch"));
*/
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //Fingerprint API only available on from Android 6.0 (M)
            FingerprintManager fingerprintManager = (FingerprintManager) this.getSystemService(Context.FINGERPRINT_SERVICE);
            if (!fingerprintManager.isHardwareDetected()) {
                finger_layout.setVisibility(View.GONE);
                finger_title.setVisibility(View.GONE);
                // Device doesn't support fingerprint authentication
            } else if (!fingerprintManager.hasEnrolledFingerprints()) {
                finger_layout.setVisibility(View.VISIBLE);
                finger_title.setVisibility(View.VISIBLE);

                // User hasn't enrolled any fingerprints to authenticate with
            } else {
                finger_layout.setVisibility(View.VISIBLE);
                finger_title.setVisibility(View.VISIBLE);
                // Everything is ready for fingerprint authentication
            }
        }
*/
    }


    public void closeActivity(View view) {
        onBackPressed();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.allowmeeting_switch:
                mmSettingsPreferences.putBooleanValue(MEETINGS_SWITCH, isChecked);
                //clear all scheduled meetings
                if (!isChecked) {
                    clearMeetingNotifications();
                }
                break;
            case R.id.allowcalls_switch:
                mmSettingsPreferences.putBooleanValue(CALLS_SWITCH, isChecked);
                //clear all scheduled calls
                if (!isChecked) {
                    clearCallNotifications();
                }
                break;
            case R.id.allowrepeats_switch:
                mmSettingsPreferences.putBooleanValue(REPEATS_SWITCH, isChecked);
                if (!isChecked) {
                    rl_repeats_picker.setVisibility(View.GONE);
                } else {
                    rl_repeats_picker.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.allow_fingerprint:
                mmSettingsPreferences.putBooleanValue(FINGERPRINT, isChecked);
                break;
           /* case R.id.allow_permissions_dialog_switch:
                mmSettingsPreferences.putBooleanValue("allow_permissions_dialog_switch", isChecked);
                break;

            case R.id.allow_enable_notifications_dialog_switch:
                mmSettingsPreferences.putBooleanValue("allow_enable_notifications_dialog_switch", isChecked);
                break;*/
        }

    }


    public void clearMeetingNotifications() {
        ArrayList<Integer> pendingIntentIdList;
        pendingIntentIdList = db.getAllScheduledMeetingsNotification();

        if (pendingIntentIdList != null) {
            PendingIntent pendingIntent;
            AlarmManager manager;
            Intent alarmintent;
            for (Integer notification_id : pendingIntentIdList) {
                alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                //cancel the alarm manager of the pending intent
                manager.cancel(pendingIntent);

            }
        }

    }


    public void clearCallNotifications() {
        ArrayList<Integer> pendingIntentIdList;
        pendingIntentIdList = db.getAllScheduledCallsNotification();
        if (pendingIntentIdList != null) {
            PendingIntent pendingIntent;
            AlarmManager manager;
            Intent alarmintent;
            for (Integer notification_id : pendingIntentIdList) {
                alarmintent = new Intent(getApplicationContext(), AlarmReceiver.class);
                alarmintent.setAction(Constants.ACTION_SCHEDULE_ALARM_PENDING_INTENT);
                pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), notification_id, alarmintent, PendingIntent.FLAG_UPDATE_CURRENT);
                manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
                //cancel the alarm manager of the pending intent
                manager.cancel(pendingIntent);

            }
        }
    }


}
