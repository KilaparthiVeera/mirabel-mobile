package com.mirabeltechnologies.magazinemanager.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.TaskListAdapter;
import com.mirabeltechnologies.magazinemanager.beans.SearchTaskListFilter;
import com.mirabeltechnologies.magazinemanager.beans.Task;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.CustomDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.TaskListListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.models.TaskListModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class TaskListActivity extends ListViewBaseActivity implements ViewMoreClickListener, TaskListListener, AlertDialogSelectionListener {
    private static final String TAG = TaskListActivity.class.getSimpleName();

    private Constants.RequestFrom requestCameFrom;
    private TaskListModel taskListModel;
    private SearchTaskListFilter searchTaskListFilter = null;
    private TaskListAdapter taskListAdapter;
    private Long currentTaskId;
    private String currentAction = "";
    private int markTaskPositionInList, deleteTaskPositionInList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        try {
            activityContext = TaskListActivity.this;

            // initializing super class variables
            currentActivity = this;
            requestFrom = Constants.RequestFrom.SEARCH_TASKS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            }

            taskListModel = new TaskListModel(activityContext, this, requestFrom);

            // Initializing UI References
            list_view =  findViewById(R.id.task_list_list_view);
            page_number_display = (MMTextView) findViewById(R.id.task_list_page_number_display);
            left_nav_arrow = (ImageView) findViewById(R.id.task_list_left_nav_arrow);
            right_nav_arrow = (ImageView) findViewById(R.id.task_list_right_nav_arrow);

            if (requestCameFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE) {
                searchTaskListFilter = (SearchTaskListFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_TASKS_FILTER_OBJECT);
            } else {
                searchTaskListFilter = new SearchTaskListFilter();

                // Setting default values
                if (searchTaskListFilter != null) {
                    searchTaskListFilter.setAssignedToRepId(loggedInRepId);
                    searchTaskListFilter.setAssignedToRepName(loggedInRepName);
                }
            }

            taskListAdapter = new TaskListAdapter(activityContext, this, this);
            list_view.setAdapter(taskListAdapter);

            refreshData();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshTasksBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_TASKS));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshTasksBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void showCreateNewTaskPage(View view) {
        Intent showCreateNewTaskPageIntent = new Intent(activityContext, CreateNewTaskActivity.class);
        startActivity(showCreateNewTaskPageIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void showSearchTaskListFilters(View view) {
        Intent showSearchTaskListFiltersIntent = new Intent(activityContext, SearchTaskListFiltersActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        bundle.putSerializable(Constants.BUNDLE_SEARCH_TASKS_FILTER_OBJECT, searchTaskListFilter);
        showSearchTaskListFiltersIntent.putExtras(bundle);
        startActivityForResult(showSearchTaskListFiltersIntent, Constants.CHOOSE_FILTERS_REQUEST_CODE);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.CHOOSE_FILTERS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                searchTaskListFilter = (SearchTaskListFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_TASKS_FILTER_OBJECT);
            }

            refreshData();
        }
    }

    public void resetValues() {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        // resetting activities list which are saved in global content
        globalContent.setTaskListSearchResults(null);
        taskListAdapter.notifyDataSetChanged();
    }

    public void refreshData() {
        resetValues();
        getTaskList(currentPageNumber);
    }

    public void getTaskList(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            taskListModel.searchTaskList(encryptedClientKey, getDTTicks(), searchTaskListFilter, currentPageNumber);
            isLoading = true;
        }
    }

    public void updateTaskList(final List<Task> taskList, int noOfTasks) {
        if (currentPageNumber == 1 && (taskList == null || taskList.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            for (Task task : taskList) {
                // Identifying & Removing HTML Tags or symbols from Task Description.
                String description = task.getDescription();

                Matcher matcher = emailIdPattern.matcher(description);
                List<Integer> emailStartingIndexes = new ArrayList<>();

                while (matcher.find()) {
                    emailStartingIndexes.add(matcher.start());
                    //emails.put(matcher.start(), matcher.group());
                }

                String modifiedDescription;

                if (emailStartingIndexes.isEmpty()) {
                    modifiedDescription = description;
                } else {
                    StringBuilder notesStr = new StringBuilder("");
                    for (Integer index : emailStartingIndexes) {
                        notesStr = new StringBuilder(description.substring(0, index));
                        String email = description.substring(index).replace("<", "").replace(">", "");
                        notesStr.append(email);
                    }

                    modifiedDescription = notesStr.toString();
                }

                modifiedDescription = newLineAndBreakPattern.matcher(modifiedDescription).replaceAll(" ");

                Matcher htmlTagsMatcher = htmlTagsPattern.matcher(modifiedDescription);
                if (htmlTagsMatcher.find()) {
                    modifiedDescription = htmlTagsMatcher.replaceAll("");
                    task.setHasHTMLTags(true);
                } else {
                    task.setHasHTMLTags(false);
                }

                task.setModifiedDescription(modifiedDescription);
            }

            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + taskList.size();

            if (noOfTasks != totalNoOfRecords) {
                totalNoOfRecords = noOfTasks;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getTaskList(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setTaskListSearchResults(taskList);
            taskListAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    @Override
    public void viewMoreClicked(int index) {
        Task selectedTask = globalContent.getTaskListSearchResults().get(index);

        // Already we have activity full notes in customer activities list, that's why we are not making service call to get activity full notes here.
        View contentView = layoutInflater.inflate(R.layout.custom_dialog, null);
        CustomDialog customDialog = new CustomDialog(activityContext, contentView, getResources().getString(R.string.full_description_title), selectedTask.getDescription(), selectedTask.isHasHTMLTags());
        customDialog.show();
    }

    @Override
    public void deleteTask(int position, Long taskId, String action) {
        currentTaskId = taskId;
        currentAction = action;
        deleteTaskPositionInList = position;

        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK, getResources().getString(R.string.title_alert), getResources().getString(R.string.delete_task), "Yes", "No");
    }

    @Override
    public void markTask(int position, Long taskId, String action) {
        currentTaskId = taskId;
        currentAction = action;
        markTaskPositionInList = position;

        modifyTask(currentTaskId, currentAction);
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (buttonType == Constants.ButtonType.POSITIVE) {
            if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_TASK) {
                modifyTask(currentTaskId, currentAction);
            }
        }
    }

    public void modifyTask(Long taskId, String action) {
        MMProgressDialog.showProgressDialog(activityContext);
        taskListModel.modifyTask(encryptedClientKey, getDTTicks(), taskId, action);
    }

    public void updateTasksAfterModify(String action) {
        if (action.equals(Constants.TASK_ACTION_DELETE)) {
            // On task delete, we will remove tasks in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like task add / delete.
            reloadTasksAfterModify(deleteTaskPositionInList);
        } else {
            //If Status is All then we are changing only modified task status and reloading adapter instead of reloading all tasks.
            if (searchTaskListFilter.getStatus() == 0) {
                Task taskData = globalContent.getTaskListSearchResults().get(markTaskPositionInList);

                if (action.equals(Constants.TASK_ACTION_MARK_COMPLETE))
                    taskData.setCompleted(1);
                else if (action.equals(Constants.TASK_ACTION_MARK_UN_COMPLETE))
                    taskData.setCompleted(0);

                taskListAdapter.notifyDataSetChanged();
                MMProgressDialog.hideProgressDialog();
            } else {
                // On task modify, we are reloading tasks if status is To Do or Completed.
                reloadTasksAfterModify(markTaskPositionInList);
            }
        }
    }

    public void reloadTasksAfterModify(int taskPosition) {
        int modifiedTaskPageNo = (int) Math.ceil((taskPosition + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

        int indexOfModifiedPageNumber = alreadyLoadedPageNumbersList.indexOf(modifiedTaskPageNo);

        // We are removing all page numbers (starting from this page number) from already loaded page no's list in order to reload them.
        alreadyLoadedPageNumbersList.subList(indexOfModifiedPageNumber, alreadyLoadedPageNumbersList.size()).clear();

        alreadyLoadedNoOfRecords = (modifiedTaskPageNo - 1) * Constants.ACTIVITY_PAGE_SIZE_INT;

        // We are removing all activities (starting from this page) from globalContent activitiesSearchResults & we will reload them.
        globalContent.getTaskListSearchResults().subList(alreadyLoadedNoOfRecords, globalContent.getTaskListSearchResults().size()).clear();

        getTaskList(modifiedTaskPageNo);
    }

    // Local Broadcast Manager Receiver Handler - to refresh tasks after creating.
    private BroadcastReceiver mRefreshTasksBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_TASKS)) {
                    refreshData();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
