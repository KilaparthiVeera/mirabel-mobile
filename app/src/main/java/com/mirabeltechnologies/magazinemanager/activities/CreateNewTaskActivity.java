package com.mirabeltechnologies.magazinemanager.activities;

import android.app.DatePickerDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.Switch;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.NewTask;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.TaskListModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CreateNewTaskActivity extends BaseActivity implements AlertDialogSelectionListener {
    public static final String TAG = CreateNewTaskActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom;
    private MMTextView create_new_task_assigned_to, create_new_task_assigned_by, create_new_task_priority, create_new_task_project, create_new_task_due_date;
    private MMEditText create_new_task_task_name, create_new_task_description;
    private Switch create_new_task_private_switch;
    private MMButton create_new_task_reset_button, create_new_task_add_button;

    private NewTask newTaskBean;
    private TaskListModel taskListModel;
    private CommonModel commonModel;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<MasterData> allPriorities = null, allProjects = null;
    private String selectedDate = "";
    private boolean isLoggedInRepExistsInAllRepsList = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_task);

        try {
            activityContext = CreateNewTaskActivity.this;

            requestFrom = Constants.RequestFrom.CREATE_NEW_TASK_PAGE;

            newTaskBean = new NewTask();

            taskListModel = new TaskListModel(activityContext, this, requestFrom);
            commonModel = new CommonModel(activityContext, this, requestFrom);

            // Initializing UI References
            create_new_task_task_name = (MMEditText) findViewById(R.id.create_new_task_task_name);
            create_new_task_description = (MMEditText) findViewById(R.id.create_new_task_description);
            create_new_task_assigned_to = (MMTextView) findViewById(R.id.create_new_task_assigned_to);
            create_new_task_assigned_by = (MMTextView) findViewById(R.id.create_new_task_assigned_by);
            create_new_task_priority = (MMTextView) findViewById(R.id.create_new_task_priority);
            create_new_task_project = (MMTextView) findViewById(R.id.create_new_task_project);
            create_new_task_due_date = (MMTextView) findViewById(R.id.create_new_task_due_date);
            create_new_task_private_switch = (Switch) findViewById(R.id.create_new_task_private_switch);
            create_new_task_reset_button = (MMButton) findViewById(R.id.create_new_task_reset_button);
            create_new_task_add_button = (MMButton) findViewById(R.id.create_new_task_add_button);

            create_new_task_assigned_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_TO);
                }
            });

            create_new_task_assigned_by.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v, Constants.SelectionType.ASSIGNED_BY);
                }
            });

            create_new_task_priority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllPriorities(v);
                }
            });

            create_new_task_project.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllProjects(v);
                }
            });

            create_new_task_due_date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showDatePicker(v);
                }
            });

            create_new_task_private_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    newTaskBean.setPrivate(isChecked);
                }
            });

            create_new_task_reset_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetNewTaskBean();
                }
            });

            create_new_task_add_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createNewTaskByValidateFields(v);
                }
            });

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RSDS_FROM_CREATE_NEW_TASK_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void updateUIWithLatestValues(NewTask task) {
        create_new_task_task_name.setText(task.getTaskName());
        create_new_task_description.setText(task.getDescription());
        create_new_task_assigned_to.setText(task.getAssignedToRepName());
        create_new_task_assigned_by.setText(task.getAssignedByRepName());
        create_new_task_priority.setText(task.getPriority());
        create_new_task_project.setText(task.getProject());
        create_new_task_due_date.setText(task.getDueDate());
        create_new_task_private_switch.setChecked(task.isPrivate());
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getRepsListWithRepSecurity(encryptedClientKey, getDTTicks(), true);
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;

        // We are removing All Reps from Reps List as we are not displaying them in dropdown (similar to Web)
        removeObjectFromRepsList(allRepsList, "-1");

        MMProgressDialog.hideProgressDialog();

        isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));

        newTaskBean.setAssignedToRepId(loggedInRepId);
        newTaskBean.setAssignedByRepId(loggedInRepId);

        if (isLoggedInRepExistsInAllRepsList) {
            newTaskBean.setAssignedToRepName(loggedInRepName);
            newTaskBean.setAssignedByRepName(loggedInRepName);
        } else {
            newTaskBean.setAssignedToRepName("");
            newTaskBean.setAssignedByRepName("");
        }

        updateUIWithLatestValues(newTaskBean);
    }

    public void showAllRepsList(View view, Constants.SelectionType selectionType) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent showAllRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            showAllRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);

            if (selectionType == Constants.SelectionType.ASSIGNED_TO)
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(newTaskBean.getAssignedToRepId()));
            else
                bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(newTaskBean.getAssignedByRepId()));

            showAllRepsListIntent.putExtras(bundle);
            startActivity(showAllRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllPriorities(View view) {
        if (allPriorities == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_TASK_PRIORITIES);
        } else {
            Intent allPrioritiesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allPrioritiesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allPriorities);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.PRIORITY);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(newTaskBean.getPriorityId()));
            allPrioritiesIntent.putExtras(bundle);
            startActivity(allPrioritiesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showAllProjects(View view) {
        if (allProjects == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_PROJECTS);
        } else {
            Intent allProjectsIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allProjectsIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allProjects);
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.PROJECT);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, String.valueOf(newTaskBean.getProjectId()));
            allProjectsIntent.putExtras(bundle);
            startActivity(allProjectsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        if (fieldType.equals(Constants.GET_MASTER_DATA_TASK_PRIORITIES)) {
            allPriorities = new ArrayList<>();
            allPriorities.add(new MasterData("", "-1"));
            allPriorities.addAll(masterData);
            MMProgressDialog.hideProgressDialog();
            showAllPriorities(null);
        } else if (fieldType.equals(Constants.GET_MASTER_DATA_PROJECTS)) {
            allProjects = new ArrayList<>();
            allProjects.add(new MasterData("", "-1"));
            allProjects.addAll(masterData);
            MMProgressDialog.hideProgressDialog();
            showAllProjects(null);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_RSDS_FROM_CREATE_NEW_TASK_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    Serializable selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);

                    if (rightSideDrawerSelectionType == Constants.SelectionType.PRIORITY) {
                        didPriorityChanged((MasterData) selectedObj);
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PROJECT) {
                        didProjectChanged((MasterData) selectedObj);
                    } else {
                        didRepChanged(rightSideDrawerSelectionType, (RepData) selectedObj);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void didRepChanged(Constants.SelectionType selectionType, RepData selectedRep) {
        if (selectionType == Constants.SelectionType.ASSIGNED_TO) {
            newTaskBean.setAssignedToRepId(Integer.parseInt(selectedRep.getId()));
            newTaskBean.setAssignedToRepName(selectedRep.getName());

            create_new_task_assigned_to.setText(newTaskBean.getAssignedToRepName());
        } else if (selectionType == Constants.SelectionType.ASSIGNED_BY) {
            newTaskBean.setAssignedByRepId(Integer.parseInt(selectedRep.getId()));
            newTaskBean.setAssignedByRepName(selectedRep.getName());

            create_new_task_assigned_by.setText(newTaskBean.getAssignedByRepName());
        }
    }

    public void didPriorityChanged(MasterData selectedPriority) {
        newTaskBean.setPriority(selectedPriority.getName());
        newTaskBean.setPriorityId(Integer.parseInt(selectedPriority.getId()));

        create_new_task_priority.setText(newTaskBean.getPriority());
    }

    public void didProjectChanged(MasterData selectedProject) {
        newTaskBean.setProject(selectedProject.getName());
        newTaskBean.setProjectId(Integer.parseInt(selectedProject.getId()));

        create_new_task_project.setText(newTaskBean.getProject());
    }

    public void showDatePicker(View view) {
        try {
            selectedDate = newTaskBean.getDueDate();

            if (selectedDate.isEmpty()) {
                selectedDate = Utility.formatDate(new Date(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
            }

            String[] currentDateValues = selectedDate.split("/");

            DatePickerDialog datePickerDialog = new DatePickerDialog(activityContext, android.R.style.Theme_DeviceDefault_Light_Dialog, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                    Calendar calendar = Calendar.getInstance();
                    calendar.set(year, monthOfYear, dayOfMonth);

                    selectedDate = Utility.formatDate(calendar.getTime(), Constants.SELECTED_DATE_DISPLAY_FORMAT);
                    create_new_task_due_date.setText(selectedDate);
                    newTaskBean.setDueDate(selectedDate);

                }
            }, Integer.parseInt(currentDateValues[2]), Integer.parseInt(currentDateValues[0]) - 1, Integer.parseInt(currentDateValues[1]));

            //datePickerDialog.setTitle("Select Date");
            //datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            datePickerDialog.setCancelable(false);
            datePickerDialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetNewTaskBean() {
        newTaskBean = new NewTask();
        newTaskBean.setAssignedToRepId(loggedInRepId);
        newTaskBean.setAssignedByRepId(loggedInRepId);

        if (isLoggedInRepExistsInAllRepsList) {
            newTaskBean.setAssignedToRepName(loggedInRepName);
            newTaskBean.setAssignedByRepName(loggedInRepName);
        } else {
            newTaskBean.setAssignedToRepName("");
            newTaskBean.setAssignedByRepName("");
        }

        updateUIWithLatestValues(newTaskBean);
    }

    public void createNewTaskByValidateFields(View view) {
        try {
            newTaskBean.setTaskName(create_new_task_task_name.getText().toString().trim());
            newTaskBean.setDescription(create_new_task_description.getText().toString().trim());

            if (newTaskBean.getTaskName().isEmpty()) {
                MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), "Please enter task name.");
                return;
            }

            MMProgressDialog.showProgressDialog(activityContext);
            taskListModel.createNewTask(encryptedClientKey, getDTTicks(), newTaskBean);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateTaskCreateStatus() {
        MMProgressDialog.hideProgressDialog();

        try {
            String title = "Add New Task Status";
            String message = "Task added successfully.";

            // Broadcasting Intent to Receiver Activity
            Intent refreshTasksBroadcast = new Intent(Constants.LBM_ACTION_REFRESH_TASKS);
            LocalBroadcastManager.getInstance(activityContext).sendBroadcast(refreshTasksBroadcast);

            MMAlertDialog.listener = this;
            MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED);
            MMAlertDialog.showAlertDialogWithCallback(activityContext, title, message, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_NEW_TASK_ADDED) {
            onBackPressed();
        }
    }
}
