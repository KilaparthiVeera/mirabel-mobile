package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Address;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.util.CollectionUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.Map_Contacts_Adpter;
import com.mirabeltechnologies.magazinemanager.adapters.SimilarContactsAdopter;
import com.mirabeltechnologies.magazinemanager.asynctasks.GetQueryAsyncTask;
import com.mirabeltechnologies.magazinemanager.beans.CardBean;
import com.mirabeltechnologies.magazinemanager.beans.CompanyData;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.Customer;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.NewContact;
import com.mirabeltechnologies.magazinemanager.beans.QueryObject;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.ReqFields;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMButton;
import com.mirabeltechnologies.magazinemanager.customviews.MMEditText;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.Utility;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CreateContactActivity extends BaseActivity implements AlertDialogSelectionListener {
    public static final String TAG = CreateContactActivity.class.getSimpleName();

    private Constants.RequestFrom requestFrom, requestCameFromForContactAddOrEdit;
    private MMTextView create_contact_activity_title, create_contact_company_name_title, create_contact_first_name_title, create_contact_last_name_title, create_contact_rep, create_contact_category_title, create_contact_category, create_contact_priority_title,
            create_contact_priority, create_contact_contact_type_title, create_contact_contact_type, create_contact_job_title_tv, create_contact_email_id_title, create_contact_phone_number_title, create_contact_address1_title, create_contact_address2_title,
            create_contact_zip_code_title, create_contact_city_title, create_contact_state_title, create_contact_county_title, create_contact_country_title;
    private MMEditText create_contact_company_name, create_contact_first_name, create_contact_last_name, create_contact_job_title, create_contact_email_id, create_contact_phone_number, create_contact_address1, create_contact_address2, create_contact_zip_code,
            create_contact_city, create_contact_state, create_contact_county, create_contact_country, create_contact_website_url, create_contact_facebook_id, create_contact_google_plus_url, create_contact_linked_in_url, create_contact_twitter_url;
    private MMButton reset_button, save_button;
    private ImageView ic_map,ic_businessCard;
    private ContactsModel contactsModel;
    private CommonModel commonModel;
    private ArrayList<RepData> allRepsList = null;
    private ArrayList<ReqFields> reqFieldsArrayList = null;
    private ArrayList<String> requiredFieldNames = new ArrayList<>();
    private ArrayList<MasterData> allCategoriesList = null, allPrioritiesList = null, allContactTypesList = null;
    private NewContact newContactBean;
    private String selectedRepName, selectedRepId, currentOperation, selectedContactTypeId = "0", newlyCreatedCustomerId;
    private DetailContact detailContact = null;
    private boolean isEditMode = false, isPrimaryContact = false, isAddingSubContact = false, isCompanyAlreadyHaveBillingContact = false;

    private static final int CAMERA_REQUEST_CODE = 200;
    private static final int STORAGE_REQUEST_CODE = 400;
    private static final int IMAGE_PICK_GALLERY_CODE = 1000;
    private static final int IMAGE_PICK_CAMERA_CODE = 1001;
    String[] cameraPermission;
    String[] storagePermission;
    Uri image_uri;
    private Bitmap mSelectedImage;
    Uri imageUpload;
    RecyclerView recyclerView;
    SimilarContactsAdopter adapter;
    AlertDialog  alertDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_contact);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            cameraPermission = new String[] {Manifest.permission.CAMERA, Manifest.permission.READ_MEDIA_IMAGES};
            //Storage Permission
            storagePermission = new String[] {Manifest.permission.READ_MEDIA_IMAGES};
        }
        else {
            cameraPermission = new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};
            //Storage Permission
            storagePermission = new String[] {Manifest.permission.WRITE_EXTERNAL_STORAGE};
        }
        try {
            activityContext = CreateContactActivity.this;

            requestFrom = Constants.RequestFrom.CREATE_CONTACT_PAGE;
            contactsModel = new ContactsModel(activityContext, this, requestFrom);
            commonModel = new CommonModel(activityContext, this, requestFrom);
            ic_map=findViewById(R.id.ic_map);
            ic_businessCard=findViewById(R.id.ic_businessCard);
            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                requestCameFromForContactAddOrEdit = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
                currentOperation = bundle.getString(Constants.BUNDLE_CURRENT_OPERATION);
                isPrimaryContact = bundle.getBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT);

                if (currentOperation.equals(Constants.CURRENT_OPERATION_EDIT_CONTACT)) {
                    ic_businessCard.setVisibility(View.GONE);
                    detailContact = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS);
                    isEditMode = true;
                } else if (currentOperation.equals(Constants.CURRENT_OPERATION_CREATE_PRIMARY_CONTACT)) {
                    isEditMode = false;
                } else if (currentOperation.equals(Constants.CURRENT_OPERATION_CREATE_SUB_CONTACT)) {
                    detailContact = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS); // this is parent contact details.
                    isCompanyAlreadyHaveBillingContact = bundle.getBoolean(Constants.BUNDLE_IS_COMPANY_ALREADY_HAVE_BILLING_CONTACT);
                    isEditMode = false;
                    isAddingSubContact = true;
                }
            }

            // Initializing UI References

            create_contact_activity_title = findViewById(R.id.create_contact_activity_title);
            create_contact_company_name_title = findViewById(R.id.create_contact_company_name_title);
            create_contact_first_name_title = findViewById(R.id.create_contact_first_name_title);
            create_contact_last_name_title = findViewById(R.id.create_contact_last_name_title);
            create_contact_rep = findViewById(R.id.create_contact_rep);
            create_contact_category_title = findViewById(R.id.create_contact_category_title);
            create_contact_category = findViewById(R.id.create_contact_category);
            create_contact_priority_title = findViewById(R.id.create_contact_priority_title);
            create_contact_priority = findViewById(R.id.create_contact_priority);
            create_contact_contact_type_title = findViewById(R.id.create_contact_contact_type_title);
            create_contact_contact_type = findViewById(R.id.create_contact_contact_type);
            create_contact_job_title_tv = findViewById(R.id.create_contact_job_title_tv);
            create_contact_email_id_title = findViewById(R.id.create_contact_email_id_title);
            create_contact_phone_number_title = findViewById(R.id.create_contact_phone_number_title);
            create_contact_address1_title = findViewById(R.id.create_contact_address1_title);
            create_contact_address2_title = findViewById(R.id.create_contact_address2_title);
            create_contact_zip_code_title = findViewById(R.id.create_contact_zip_code_title);
            create_contact_city_title = findViewById(R.id.create_contact_city_title);
            create_contact_state_title = findViewById(R.id.create_contact_state_title);
            create_contact_county_title = findViewById(R.id.create_contact_county_title);
            create_contact_country_title = findViewById(R.id.create_contact_country_title);

            create_contact_company_name = findViewById(R.id.create_contact_company_name);
            create_contact_first_name = findViewById(R.id.create_contact_first_name);
            create_contact_last_name = findViewById(R.id.create_contact_last_name);
            create_contact_job_title = findViewById(R.id.create_contact_job_title);
            create_contact_email_id = findViewById(R.id.create_contact_email_id);
            create_contact_phone_number = findViewById(R.id.create_contact_phone_number);
            create_contact_address1 = findViewById(R.id.create_contact_address1);
            create_contact_address2 = findViewById(R.id.create_contact_address2);
            create_contact_zip_code = findViewById(R.id.create_contact_zip_code);
            create_contact_city = findViewById(R.id.create_contact_city);
            create_contact_state = findViewById(R.id.create_contact_state);
            create_contact_county = findViewById(R.id.create_contact_county);
            create_contact_country = findViewById(R.id.create_contact_country);
            create_contact_website_url = findViewById(R.id.create_contact_website_url);
            create_contact_facebook_id = findViewById(R.id.create_contact_facebook_id);
            create_contact_google_plus_url = findViewById(R.id.create_contact_google_plus_url);
            create_contact_linked_in_url = findViewById(R.id.create_contact_linked_in_url);
            create_contact_twitter_url = findViewById(R.id.create_contact_twitter_url);

            ic_businessCard.setOnClickListener(v -> {

                showImageImportDialog();

              /*  Intent i= new Intent(CreateContactActivity.this,ScanCardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.BUSINESS_CARD);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getCategoryId());
                i.putExtras(bundle1);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);*/
            });


            ic_map.setOnClickListener(v -> {
                Intent i= new Intent(CreateContactActivity.this,AddressActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.MAP_ADDRESS);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                i.putExtras(bundle1);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
            });

            create_contact_rep.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllRepsList(v);
                }
            });

            create_contact_category.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllCategories(v);
                }
            });

            create_contact_priority.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllPriorities(v);
                }
            });

            create_contact_contact_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showAllContactTypes(v);
                }
            });

            reset_button = findViewById(R.id.create_contact_reset_button);
            reset_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resetCreateContactFields(v);
                }
            });

            save_button = findViewById(R.id.create_contact_save_button);
            save_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    createNewContactByValidateFields(v);
                }
            });

            if (isEditMode)
                create_contact_activity_title.setText("Edit Contact");
            else
                create_contact_activity_title.setText("Add Contact");

            if (!isPrimaryContact) {
                if (isEditMode || isAddingSubContact) {
                    create_contact_rep.setEnabled(false);
                    create_contact_rep.setTextColor(ContextCompat.getColor(activityContext, R.color.disabled_text_color));

                    create_contact_category.setEnabled(false);
                    create_contact_category.setTextColor(ContextCompat.getColor(activityContext, R.color.disabled_text_color));

                    create_contact_priority.setEnabled(false);
                    create_contact_priority.setTextColor(ContextCompat.getColor(activityContext, R.color.disabled_text_color));

                    if (isEditMode) {
                        // if current editing contact is billing contact then we are disabling contact type choose option.
                        if (detailContact.getJobDescription().equalsIgnoreCase(Constants.CONTACT_TYPE_BILLING)) {
                            create_contact_contact_type.setEnabled(false);
                            create_contact_contact_type.setTextColor(ContextCompat.getColor(activityContext, R.color.disabled_text_color));
                            isCompanyAlreadyHaveBillingContact = true;
                        } else {
                            /*
                             If we are editing a sub contact then we are checking for any billing contact is available by checking current contact type,
                             If current contact is not billing contact then we are getting all sub contacts of same company & checking for billing contact.
                             While adding a sub contact we know already weather any billing contact is available or not because we have already loaded all sub contacts of company.
                            */
                            contactsModel.getSubContactsForCustomerId(encryptedClientKey, getDTTicks(), String.valueOf(detailContact.getParentId()));
                        }
                    }
                }
            }

            loadDataFromServer();

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRightSideDrawerSelectionReceiver, new IntentFilter(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showImageImportDialog(){

        String Camera = this.getString(R.string.scan_camera);
        String Gallery = this.getString(R.string.scan_gallery);
        String messageTitle = this.getString(R.string.scan_message);

        TextView textView = new TextView(this);
        textView.setText(messageTitle);
        textView.setPadding(20, 30, 20, 30);
        textView.setTextSize(20F);
        textView.setBackgroundColor(Color.parseColor("#5680a6"));
        textView.setTextColor(Color.WHITE);
        String[] options = {Camera, Gallery};
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        // set dialog's title
        dialog.setCustomTitle(textView);
        dialog.setItems(options, (dialog1, which) -> {
            if(which == 0){
                // select camera
                if(!checkCameraPermission()){
                    requestCameraPermission();
                }else{
                    pickCamera();
                }
            }
            if(which == 1){
                // select gallery
                if(!checkStoragePermission()){
                    requestStoragePermission();
                }else{
                    pickGallery();
                }
            }

        });
        dialog.create().show();
    }

    private void pickGallery() {
        // intent to pick image from gallery
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_GALLERY_CODE);
    }

    private void pickCamera() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "CardWhere"); // title of picture
        values.put(MediaStore.Images.Media.DESCRIPTION, "Scan by CardWhere"); // description of picture
        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri);
        startActivityForResult(cameraIntent, IMAGE_PICK_CAMERA_CODE);
    }

    private void requestStoragePermission() {
        ActivityCompat.requestPermissions(this, storagePermission, STORAGE_REQUEST_CODE);
    }

    private boolean checkStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) == (PackageManager.PERMISSION_GRANTED);
        }else {
            return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        }
    }

    private void requestCameraPermission() {
        ActivityCompat.requestPermissions(this, cameraPermission, CAMERA_REQUEST_CODE);
    }

    private boolean checkCameraPermission() {
        boolean result_storage_permission;
        boolean result_camera_permission = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            result_storage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_MEDIA_IMAGES) == (PackageManager.PERMISSION_GRANTED);
        }
        else {
            result_storage_permission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == (PackageManager.PERMISSION_GRANTED);
        }
        return result_camera_permission && result_storage_permission;
    }


    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRightSideDrawerSelectionReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer() {
        MMProgressDialog.showProgressDialog(activityContext);
        commonModel.getAllRepsData(encryptedClientKey, getDTTicks(), true, 0);
        commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_REQ_FIELDS);
    }

    public void updateAllRepsData(List<RepData> repsList) {
        allRepsList = (ArrayList) repsList;

        // We are removing All Reps and Disabled Reps from Reps List as we are not displaying them in dropdown (similar to Web)
        removeObjectFromRepsList(allRepsList, "-1");
        //removeObjectFromRepsList(allRepsList, "-2");

        resetNewContactBean(true, true);
    }

    public void updateSubContactsList(final List<Contact> subContactsList) {
        if (subContactsList == null || subContactsList.isEmpty()) {
            isCompanyAlreadyHaveBillingContact = false;
        } else {
            isCompanyAlreadyHaveBillingContact = checkForBillingContact(subContactsList);
        }
    }

    public void updateMasterData(List<MasterData> masterData, String fieldType) {
        switch (fieldType) {
            case Constants.GET_MASTER_DATA_CATEGORIES:
                allCategoriesList = new ArrayList<>();
                allCategoriesList.add(new MasterData("", "0"));
                allCategoriesList.addAll(masterData);
                MMProgressDialog.hideProgressDialog();
                showAllCategories(null);
                break;
            case Constants.GET_MASTER_DATA_PRIORITIES:
                allPrioritiesList = new ArrayList<>();
                allPrioritiesList.add(new MasterData("", "0"));
                allPrioritiesList.addAll(masterData);
                MMProgressDialog.hideProgressDialog();
                showAllPriorities(null);
                break;
            case Constants.GET_MASTER_DATA_CONTACT_TYPES:
                allContactTypesList = (ArrayList) masterData;

                if (isPrimaryContact || isCompanyAlreadyHaveBillingContact) {
                    allContactTypesList = removeObjectFromArrayListByName(allContactTypesList, Constants.CONTACT_TYPE_BILLING_WITH_STAR, Constants.LIST_TYPE_MASTER_DATA);
                }

                if (isEditMode) {
                    newContactBean.setContactTypeId(getIdForValueFromArrayList(allContactTypesList, newContactBean.getContactType(), Constants.LIST_TYPE_MASTER_DATA));
                    selectedContactTypeId = newContactBean.getContactTypeId();
                }

                MMProgressDialog.hideProgressDialog();
                showAllContactTypes(null);
                break;
        }
    }

    public void updateReqFieldsArrayList(List<ReqFields> reqFieldsList) {
        this.reqFieldsArrayList = (ArrayList) reqFieldsList;

        for (ReqFields reqField : reqFieldsArrayList) {
            if (reqField.isRequired()) {
                requiredFieldNames.add(reqField.getFieldName());
            }
        }

        // Commenting this as per client requirement
        /*if (!isEditMode && !requiredFieldNames.contains("email")) {
            requiredFieldNames.add("email");
        }*/

        if (requiredFieldNames.contains("customer"))
            create_contact_company_name_title.setText("* Company Name");

        if (requiredFieldNames.contains("firstname"))
            create_contact_first_name_title.setText("* First Name");

        if (requiredFieldNames.contains("lastname"))
            create_contact_last_name_title.setText("* Last Name");

        if (requiredFieldNames.contains("category"))
            create_contact_category_title.setText("* Category");

        if (requiredFieldNames.contains("priority"))
            create_contact_priority_title.setText("* Priority");

        if (requiredFieldNames.contains("jobdescription"))
            create_contact_contact_type_title.setText("* Contact Type");

        if (requiredFieldNames.contains("title"))
            create_contact_job_title_tv.setText("* Job Title");

        if (requiredFieldNames.contains("email"))
            create_contact_email_id_title.setText("* Email");

        if (requiredFieldNames.contains("phone"))
            create_contact_phone_number_title.setText("* Phone");

        if (requiredFieldNames.contains("addr1"))
            create_contact_address1_title.setText("* Address 1");

        if (requiredFieldNames.contains("addr2"))
            create_contact_address2_title.setText("* Address 2");

        if (requiredFieldNames.contains("zip"))
            create_contact_zip_code_title.setText("* Zip Code");

        if (requiredFieldNames.contains("city"))
            create_contact_city_title.setText("* City");

        if (requiredFieldNames.contains("st"))
            create_contact_state_title.setText("* State");

        if (requiredFieldNames.contains("county"))
            create_contact_county_title.setText("* County");

        if (requiredFieldNames.contains("country"))
            create_contact_country_title.setText("* Country");

        MMProgressDialog.hideProgressDialog();
    }

    public void resetNewContactBean(boolean resetRepWithLoginUser, boolean updateUI) {
        if (isEditMode) {
            newContactBean = new NewContact(detailContact, false);
            newContactBean.setContactTypeId(selectedContactTypeId);
        } else if (isAddingSubContact) {
            newContactBean = new NewContact(detailContact, true);
        } else {
            newContactBean = new NewContact();

            if (resetRepWithLoginUser && loggedInRepName.length() > 0) {
                newContactBean.setRepId(String.valueOf(loggedInRepId));
                newContactBean.setRepName(loggedInRepName);

                selectedRepName = loggedInRepName;
                selectedRepId = String.valueOf(loggedInRepId);
            } else {
                newContactBean.setRepId(selectedRepId);
                newContactBean.setRepName(selectedRepName);
            }

            boolean isLoggedInRepExistsInAllRepsList = checkForLoggedInRep(allRepsList, String.valueOf(loggedInRepId));

            if (!isLoggedInRepExistsInAllRepsList) {
                newContactBean.setRepId("");
                newContactBean.setRepName("");

                selectedRepName = "";
                selectedRepId = "";
            }

            create_contact_rep.setText(newContactBean.getRepName());
        }

        if (updateUI)
            updateUIWithLatestValues(newContactBean);
    }

    public void resetCreateContactFields(View view) {
        resetNewContactBean(true, true);
    }

    public void resetFieldsErrors() {
        create_contact_company_name.setError(null);
        create_contact_first_name.setError(null);
        create_contact_last_name.setError(null);
        create_contact_job_title.setError(null);
        create_contact_email_id.setError(null);
        create_contact_phone_number.setError(null);
        create_contact_address1.setError(null);
        create_contact_address2.setError(null);
        create_contact_zip_code.setError(null);
        create_contact_city.setError(null);
        create_contact_state.setError(null);
        create_contact_county.setError(null);
        create_contact_country.setError(null);
    }

    public void updateUIWithLatestValues(NewContact contact) {
        resetFieldsErrors();

        create_contact_company_name.setText(contact.getCompanyName());
        create_contact_first_name.setText(contact.getFirstName());
        create_contact_last_name.setText(contact.getLastName());
        create_contact_rep.setText(contact.getRepName());
        create_contact_category.setText(contact.getCategory());
        create_contact_priority.setText(contact.getPriority());
        create_contact_contact_type.setText(contact.getContactType());
        create_contact_job_title.setText(contact.getJobTitle());
        create_contact_email_id.setText(contact.getEmailId());
        create_contact_phone_number.setText(contact.getPhoneNumber());
        create_contact_address1.setText(contact.getAddress1());
        create_contact_address2.setText(contact.getAddress2());
        create_contact_zip_code.setText(contact.getZipCode());
        create_contact_city.setText(contact.getCity());
        create_contact_state.setText(contact.getState());
        create_contact_county.setText(contact.getCounty());
        create_contact_country.setText(contact.getCountry());

        create_contact_website_url.setText(contact.getWebsiteURL());
        create_contact_facebook_id.setText(contact.getFacebookId());
        create_contact_google_plus_url.setText(contact.getGooglePlusURL());
        create_contact_linked_in_url.setText(contact.getLinkedInURL());
        create_contact_twitter_url.setText(contact.getTwitterURL());
    }

    public void showAllRepsList(View view) {
        if (allRepsList != null && allRepsList.size() > 0) {
            Intent allRepsListIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allRepsListIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.REP);
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_REPS_DATA, allRepsList);
            //bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, currentlySelectedRepData);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getRepId());
            allRepsListIntent.putExtras(bundle);
            startActivity(allRepsListIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            displayLongToast(getResources().getString(R.string.no_reps_found));
        }
    }

    public void showAllCategories(View view) {
        if (allCategoriesList == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_CATEGORIES);
        } else {
            Intent allCategoriesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allCategoriesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.CATEGORY);
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allCategoriesList);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getCategoryId());
            allCategoriesIntent.putExtras(bundle);
            startActivity(allCategoriesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showAllPriorities(View view) {
        if (allPrioritiesList == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_PRIORITIES);
        } else {
            Intent allPrioritiesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allPrioritiesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.PRIORITY);
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allPrioritiesList);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getPriorityId());
            allPrioritiesIntent.putExtras(bundle);
            startActivity(allPrioritiesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showAllContactTypes(View view) {
        if (allContactTypesList == null) {
            MMProgressDialog.showProgressDialog(activityContext);
            commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_CONTACT_TYPES);
        } else {
            Intent allContactTypesIntent = new Intent(activityContext, RightSideDrawerListWithSearchActivity.class);
            allContactTypesIntent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.CONTACT_TYPE);
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ALL_MASTERS_DATA, allContactTypesList);
            bundle.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getContactTypeId());
            allContactTypesIntent.putExtras(bundle);
            startActivity(allContactTypesIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRightSideDrawerSelectionReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (Objects.requireNonNull(intent.getAction()).equals(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE)) {
                    Constants.SelectionType rightSideDrawerSelectionType = null;
                    if (bundle != null) {
                        rightSideDrawerSelectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                    }
                    Serializable selectedObj = null;
                    if (bundle != null) {
                        selectedObj = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
                    }
                    if (rightSideDrawerSelectionType == Constants.SelectionType.REP) {
                        if (selectedObj != null) {
                            didRepChanged((RepData) selectedObj);
                        }
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.CATEGORY) {
                        if (selectedObj != null) {
                            didCategoryChanged((MasterData) selectedObj);
                        }
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.PRIORITY) {
                        if (selectedObj != null) {
                            didPriorityChanged((MasterData) selectedObj);
                        }
                    } else if (rightSideDrawerSelectionType == Constants.SelectionType.CONTACT_TYPE) {
                        if (selectedObj != null) {
                            didContactTypeChanged((MasterData) selectedObj);
                        }
                    }
                    else if (rightSideDrawerSelectionType == Constants.SelectionType.BUSINESS_CARD) {
                        if(selectedObj!=null){
                            CardBean cardBean= (CardBean) selectedObj;
                            updateBusinessInUI(cardBean);
                        }
                    }

                    else  if(rightSideDrawerSelectionType==Constants.SelectionType.MAP_ADDRESS){
                        List<Address> addresses = bundle.getParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE);
                        if(addresses!=null){
                            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                            String address1=addresses.get(0).getFeatureName();
                            String adress2=addresses.get(0).getSubLocality();
                            String city = addresses.get(0).getLocality();
                            String state = addresses.get(0).getAdminArea();
                            String country = addresses.get(0).getCountryName();
                            String county=addresses.get(0).getSubAdminArea();
                            String postalCode = addresses.get(0).getPostalCode();
                            if(adress2==null){
                                String[] parts =address.split(",");
                                String part1=parts[0];
                                newContactBean.setAddress2(part1);
                            }
                            else {
                                String[] parts =address.split(",");
                                String part1=parts[0];
                                String part2=parts[1];
                                newContactBean.setAddress2(part1+","+part2+","+adress2);
                            }
                            newContactBean.setCity(city);
                            newContactBean.setState(state);
                            newContactBean.setCounty(county);
                            newContactBean.setCountry(country);
                            create_contact_zip_code.setText(postalCode);
                            create_contact_address2.setText(newContactBean.getAddress2());
                            updateAddressInUI();
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void updateBusinessInUI(CardBean cardBean) {

        if(!TextUtils.isEmpty(cardBean.getCompany())){
            create_contact_company_name.setText(cardBean.getCompany());
        }
        if(!TextUtils.isEmpty(cardBean.getName())){
            create_contact_first_name.setText(cardBean.getName());
        }
        if(!TextUtils.isEmpty(cardBean.getJob())){
            create_contact_job_title.setText(cardBean.getJob());
        }
        if(!TextUtils.isEmpty(cardBean.getPhone())){
            create_contact_phone_number.setText(cardBean.getPhone());
        }
        if(!TextUtils.isEmpty(cardBean.getEmail())){
            create_contact_email_id.setText(cardBean.getEmail());
        }
        if(!TextUtils.isEmpty(cardBean.getAddress1())){
            create_contact_address1.setText(cardBean.getAddress1());
        }
        if(!TextUtils.isEmpty(cardBean.getAddress2())){
            create_contact_address2.setText(cardBean.getAddress2());
        }
        if(!TextUtils.isEmpty(cardBean.getCity())){
            create_contact_city.setText(cardBean.getCity());
        }

        if(!TextUtils.isEmpty(cardBean.getState())){
            create_contact_state.setText(cardBean.getState());
        }
        if(!TextUtils.isEmpty(cardBean.getCounty())){
            create_contact_county.setText(cardBean.getCounty());
        }
        if(!TextUtils.isEmpty(cardBean.getCountry())){
            create_contact_country.setText(cardBean.getCountry());
        }
        if(!TextUtils.isEmpty(cardBean.getZipcode())){
            create_contact_zip_code.setText(cardBean.getZipcode());
        }
    }

    public void didRepChanged(RepData selectedRep) {
        newContactBean.setRepId(selectedRep.getId());
        newContactBean.setRepName(selectedRep.getName());
        selectedRepName = newContactBean.getRepName();
        selectedRepId = newContactBean.getRepId();
        create_contact_rep.setText(newContactBean.getRepName());
    }

    public void didCategoryChanged(MasterData selectedCategory) {
        newContactBean.setCategory(selectedCategory.getName());
        newContactBean.setCategoryId(selectedCategory.getId());

        create_contact_category.setText(newContactBean.getCategory());
    }

    public void didPriorityChanged(MasterData selectedPriority) {
        newContactBean.setPriority(selectedPriority.getName());
        newContactBean.setPriorityId(selectedPriority.getId());

        create_contact_priority.setText(newContactBean.getPriority());
    }

    public void didContactTypeChanged(MasterData selectedContactType) {
        newContactBean.setContactType(selectedContactType.getName());
        newContactBean.setContactTypeId(selectedContactType.getId());

        create_contact_contact_type.setText(newContactBean.getContactType());
    }

    public void pickContact(View view) {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_READ_CONTACTS, Constants.PERMISSIONS_READ_CONTACTS, Constants.READ_CONTACTS_REQUEST_CODE)) {
            Intent readContactsIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);

            if (readContactsIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(readContactsIntent, Constants.READ_CONTACTS_REQUEST_CODE);
            } else {
                displayToast(getResources().getString(R.string.no_support_for_read_contacts));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case CAMERA_REQUEST_CODE:
                if(grantResults.length >0){
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted){
                        pickCamera();
                    }else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case STORAGE_REQUEST_CODE:
                if(grantResults.length >0){

                    boolean writeStorageAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (writeStorageAccepted){
                        pickGallery();
                    }else {
                        Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;

            case Constants.READ_CONTACTS_REQUEST_CODE :// If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pickContact(null);
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_READ_CONTACTS)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_READ_CONTACTS);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.read_contacts_permission_title), getResources().getString(R.string.read_contacts_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.read_contacts_permission_denied));
                    }
                }
                break;
        }


    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_READ_CONTACTS) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_READ_CONTACTS, Constants.READ_CONTACTS_REQUEST_CODE);
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_CONTACT_SAVED) {
            openNewlyCreatedContactDetailPage();
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_CONTACT_EDITED) {
            onBackPressed();
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_UPDATE_SUB_CONTACTS) {
            if (buttonType == Constants.ButtonType.POSITIVE)
                newContactBean.setUpdateSubContacts(true);
            else
                newContactBean.setUpdateSubContacts(false);

            saveContact();
        } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_COMPANY_NAME_ALREADY_EXISTS) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                isPrimaryContact = true;
                saveContact();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.READ_CONTACTS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    Uri selectedContactData = data.getData();
                    populateInfoWithPickedContact(selectedContactData);
                }
            }
        }else if (resultCode == RESULT_OK) {

            if (requestCode == IMAGE_PICK_GALLERY_CODE) {
                if (data != null) {
                    CropImage.activity(data.getData()).setGuidelines(CropImageView.Guidelines.ON).start(this);
                }
            }
            if (requestCode == IMAGE_PICK_CAMERA_CODE) {
                CropImage.activity(image_uri).setGuidelines(CropImageView.Guidelines.ON).start(this);
            }
        }

        //got cropped image
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                // get image uri
                Uri resultUri = null;
                if (result != null) {
                    resultUri = result.getUri();
                }
                imageUpload = resultUri;


                Intent i= new Intent(CreateContactActivity.this,ScanCardActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                Bundle bundle1 = new Bundle();
                bundle1.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, Constants.SelectionType.BUSINESS_CARD);
                bundle1.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
                bundle1.putString(Constants.BUNDLE_RSD_SELECTED_INDEX, newContactBean.getCategoryId());
                bundle1.putString("imagePath", String.valueOf(imageUpload));
                i.putExtras(bundle1);
                startActivity(i);
                overridePendingTransition(R.anim.right_in, R.anim.left_out);
              /*  // set image to image view
                cardIv.setImageURI(resultUri);

                // get drawable bitmap from text recognition
                BitmapDrawable bitmapDrawable = (BitmapDrawable) cardIv.getDrawable();

                // text recognition
                mSelectedImage = bitmapDrawable.getBitmap();
                runTextRecognition(mSelectedImage);*/

            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                // if there is any error
                Exception error = null;
                if (result != null) {
                    error = result.getError();
                }
                Toast.makeText(this, " " + error, Toast.LENGTH_LONG).show();
            }
        }
    }

    public void populateInfoWithPickedContact(Uri contactData) {
        try {
            if (contactData != null) {
                // Resetting newContactBean before injecting new values.
                resetNewContactBean(false, false);

                Cursor cursor = getContentResolver().query(contactData, null, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                    String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                    //Log.d("Contact Cursor", "======= cursor = " + DatabaseUtils.dumpCursorToString(cursor));
                    newContactBean.setFirstName(name);

                    Cursor phoneNumberCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{contactId}, null);
                    if (phoneNumberCursor != null) {
                        while (phoneNumberCursor.moveToNext()) {
                            String phoneNumber = phoneNumberCursor.getString(phoneNumberCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            if (phoneNumber.length() > 0) {
                                newContactBean.setPhoneNumber(phoneNumber);
                                break;
                            }
                        }
                    }
                    if (phoneNumberCursor != null) {
                        phoneNumberCursor.close();
                    }

                    Cursor emailCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contactId}, null);
                    if (emailCursor != null) {
                        while (emailCursor.moveToNext()) {
                            String email = emailCursor.getString(emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS));
                            if (email.length() > 0) {
                                newContactBean.setEmailId(email);
                                break;
                            }
                        }
                    }
                    if (emailCursor != null) {
                        emailCursor.close();
                    }

                    Cursor organizationCursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? ", new String[]{contactId, ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE}, null);
                    if (organizationCursor != null && organizationCursor.moveToFirst()) {
                        String companyName = organizationCursor.getString(organizationCursor.getColumnIndex(ContactsContract.CommonDataKinds.Organization.COMPANY));
                        if (companyName.length() > 0) {
                            newContactBean.setCompanyName(companyName);
                        }
                        String jobTitle = organizationCursor.getString(organizationCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.Organization.TITLE));
                        if (jobTitle != null && jobTitle.length() > 0) {
                            newContactBean.setJobTitle(jobTitle);
                        }
                    }
                    if (organizationCursor != null) {
                        organizationCursor.close();
                    }

                    /*Cursor addressCursor = getContentResolver().query(ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_URI, null, ContactsContract.CommonDataKinds.StructuredPostal.CONTACT_ID + " = ? ", new String[]{contactId}, null);
                    if (addressCursor.moveToFirst()) {
                        String formattedAddress = addressCursor.getString(addressCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredPostal.FORMATTED_ADDRESS));
                        String street = addressCursor.getString(addressCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
                        String city = addressCursor.getString(addressCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
                        String country = addressCursor.getString(addressCursor.getColumnIndexOrThrow(ContactsContract.CommonDataKinds.StructuredPostal.COUNTRY));
                    }
                    addressCursor.close();*/

                    Cursor websitesCursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ? ", new String[]{contactId, ContactsContract.CommonDataKinds.Website.CONTENT_ITEM_TYPE}, null);
                    if (websitesCursor != null && websitesCursor.moveToFirst()) {
                        String website = websitesCursor.getString(websitesCursor.getColumnIndex(ContactsContract.CommonDataKinds.Website.DATA1));
                        if (website.length() > 0) {
                            newContactBean.setWebsiteURL(website);
                        }
                    }
                    if (websitesCursor != null) {
                        websitesCursor.close();
                    }

                    // Closing Contact cursor
                    cursor.close();
                }

                updateUIWithLatestValues(newContactBean);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getAddressForZipCode(View view) {
        String enteredZipCode = Objects.requireNonNull(create_contact_zip_code.getText()).toString().trim();

        if (enteredZipCode.length() > 0) {
            MMProgressDialog.showProgressDialog(activityContext);


            newContactBean.setAddress2("");
            if(create_contact_city.getText().length()<0){
                newContactBean.setCity("");
                create_contact_city.setError(null);

            }
            newContactBean.setState("");
            newContactBean.setCounty("");
            newContactBean.setCountry("");

            create_contact_address2.setError(null);
            create_contact_state.setError(null);
            create_contact_county.setError(null);
            create_contact_country.setError(null);

            //hideKeyboard(create_contact_zip_code);

            commonModel.getLocationDetailsForAddress(enteredZipCode);
        }
    }

    public void updateUIWithAddressDetails(JSONArray addressResultsArray) {
        try {
            if (addressResultsArray.length() > 0) {
                JSONArray addressComponents = addressResultsArray.getJSONObject(0).getJSONArray("address_components");

                for (int i = 0; i < addressComponents.length(); i++) {
                    JSONObject address = addressComponents.getJSONObject(i);

                    String long_name = address.getString("long_name");
                    JSONArray addressTypesArray = address.getJSONArray("types");
                    String addressTypesStr = addressTypesArray.getString(0);

                    if (addressTypesStr.equalsIgnoreCase("locality")) {
                        newContactBean.setCity(long_name);
                    } else if (addressTypesStr.equalsIgnoreCase("administrative_area_level_1")) {
                        newContactBean.setState(long_name);
                    } else if (addressTypesStr.equalsIgnoreCase("administrative_area_level_2")) {
                        newContactBean.setCounty(long_name);
                    } else if (addressTypesStr.equalsIgnoreCase("country")) {
                        newContactBean.setCountry(long_name);
                    }
                }

                updateAddressInUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        MMProgressDialog.hideProgressDialog();
    }

    public void addressNotFoundForZipCode() {
        displayToast("No address found for given zip code.");
        updateAddressInUI();
        MMProgressDialog.hideProgressDialog();
    }

    public void updateAddressInUI() {

        if(create_contact_city.getText().length()<=0){
            create_contact_city.setText(newContactBean.getCity());
        }
        if(create_contact_state.getText().length()<= 0){
            create_contact_state.setText(newContactBean.getState());
        }
        create_contact_county.setText(newContactBean.getCounty());
        create_contact_country.setText(newContactBean.getCountry());
    }

    public void showValidationAlert(String message) {
        MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_alert), message);
    }

    public void createNewContactByValidateFields(View view) {
        try {
            newContactBean.setCompanyName(create_contact_company_name.getText().toString().trim());
            newContactBean.setFirstName(create_contact_first_name.getText().toString().trim());
            newContactBean.setLastName(create_contact_last_name.getText().toString().trim());
            newContactBean.setRepName(create_contact_rep.getText().toString());
            newContactBean.setCategory(create_contact_category.getText().toString());
            newContactBean.setPriority(create_contact_priority.getText().toString());
            newContactBean.setContactType(create_contact_contact_type.getText().toString());
            newContactBean.setEmailId(create_contact_email_id.getText().toString().trim());
            newContactBean.setPhoneNumber(create_contact_phone_number.getText().toString().trim());
            newContactBean.setJobTitle(create_contact_job_title.getText().toString().trim());
            newContactBean.setAddress1(create_contact_address1.getText().toString().trim());
            newContactBean.setAddress2(create_contact_address2.getText().toString().trim());
            newContactBean.setZipCode(create_contact_zip_code.getText().toString().trim());
            newContactBean.setCity(create_contact_city.getText().toString().trim());
            newContactBean.setState(create_contact_state.getText().toString().trim());
            newContactBean.setCounty(create_contact_county.getText().toString().trim());
            newContactBean.setCountry(create_contact_country.getText().toString().trim());
            newContactBean.setWebsiteURL(create_contact_website_url.getText().toString().trim());
            newContactBean.setFacebookId(create_contact_facebook_id.getText().toString().trim());
            newContactBean.setGooglePlusURL(create_contact_google_plus_url.getText().toString().trim());
            newContactBean.setLinkedInURL(create_contact_linked_in_url.getText().toString().trim());
            newContactBean.setTwitterURL(create_contact_twitter_url.getText().toString().trim());

            resetFieldsErrors();

            boolean isValidPhoneNumber = true;
            if (newContactBean.getPhoneNumber().length() > 0) {
                if (Utility.isValidPhoneNumber(newContactBean.getPhoneNumber())) {
                    if (newContactBean.getPhoneNumber().length() < 7 || newContactBean.getPhoneNumber().length() > 14)
                        isValidPhoneNumber = false;
                } else {
                    isValidPhoneNumber = false;
                }
            }

            boolean canWeSendRequest = false;
            String errorMessage = null;
            MMEditText focusView = null;

            if (requiredFieldNames.isEmpty() && (newContactBean.getCompanyName().isEmpty() && newContactBean.getFirstName().isEmpty() && newContactBean.getLastName().isEmpty())) {
                errorMessage = "Please enter a company name or first name or last name to save this contact.";
                showValidationAlert(errorMessage);
                return;
            } else if (requiredFieldNames.contains("customer") && newContactBean.getCompanyName().isEmpty()) {
                errorMessage = "Please enter company name.";
                focusView = create_contact_company_name;
            } else if (requiredFieldNames.contains("firstname") && newContactBean.getFirstName().isEmpty()) {
                errorMessage = "Please enter first name.";
                focusView = create_contact_first_name;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("lastname") && newContactBean.getLastName().isEmpty()) {
                errorMessage = "Please enter last name.";
                focusView = create_contact_last_name;
                canWeSendRequest = false;
            } else if (newContactBean.getRepId().isEmpty() || newContactBean.getRepName().isEmpty()) {
                errorMessage = "Please choose sales rep.";
                showValidationAlert(errorMessage);
                return;
            } else if (requiredFieldNames.contains("category") && newContactBean.getCategory().isEmpty()) {
                errorMessage = "Please choose category.";
                showValidationAlert(errorMessage);
                return;
            } else if (requiredFieldNames.contains("priority") && newContactBean.getPriority().isEmpty()) {
                errorMessage = "Please choose priority.";

                showValidationAlert(errorMessage);
                return;
            } else if (requiredFieldNames.contains("jobdescription") && newContactBean.getContactType().isEmpty()) {
                errorMessage = "Please choose contact type.";

                showValidationAlert(errorMessage);
                return;
            } else if (requiredFieldNames.contains("title") && newContactBean.getJobTitle().isEmpty()) {
                errorMessage = "Please enter job title.";
                focusView = create_contact_job_title;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("email") && newContactBean.getEmailId().isEmpty()) {
                errorMessage = "Please enter email.";
                focusView = create_contact_email_id;
                canWeSendRequest = false;
            } else if (newContactBean.getEmailId().length() > 0 && !Utility.isValidEmail(newContactBean.getEmailId())) {
                errorMessage = "Please enter valid email.";
                focusView = create_contact_email_id;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("phone") && newContactBean.getPhoneNumber().isEmpty()) {
                errorMessage = "Please enter phone number.";
                focusView = create_contact_phone_number;
                canWeSendRequest = false;
            } else if (newContactBean.getPhoneNumber().length() > 0 && !isValidPhoneNumber) {
                errorMessage = "Please enter valid phone number.";
                focusView = create_contact_phone_number;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("addr1") && newContactBean.getAddress1().isEmpty()) {
                errorMessage = "Please enter address 1.";
                focusView = create_contact_address1;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("addr2") && newContactBean.getAddress2().isEmpty()) {
                errorMessage = "Please enter address 2.";
                focusView = create_contact_address2;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("zip") && newContactBean.getZipCode().isEmpty()) {
                errorMessage = "Please enter zip code.";
                focusView = create_contact_zip_code;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("city") && newContactBean.getCity().isEmpty()) {
                errorMessage = "Please enter city.";
                focusView = create_contact_city;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("st") && newContactBean.getState().isEmpty()) {
                errorMessage = "Please enter state.";
                focusView = create_contact_state;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("county") && newContactBean.getCounty().isEmpty()) {
                errorMessage = "Please enter county.";
                focusView = create_contact_county;
                canWeSendRequest = false;
            } else if (requiredFieldNames.contains("country") && newContactBean.getCountry().isEmpty()) {
                errorMessage = "Please enter country.";
                focusView = create_contact_country;
                canWeSendRequest = false;
            } else {
                canWeSendRequest = true;
            }

            if (!canWeSendRequest) {
                //showValidationAlert(errorMessage);

                if (focusView != null) {
                    focusView.setError(errorMessage);
                    focusView.requestFocus();
                }
            } else {
                /*
                 If currently we are adding primary contact then we will check weather already any company (primary contact) is existed with same or not.
                 If you are changing company name while editing primary contact, then we are taking confirmation about weather you want to change sub contacts company name also if primary contact have at least one sub contact.
                  */
                if (isPrimaryContact) {
                    if (isEditMode) {
                        if (detailContact.getCompanyName().equalsIgnoreCase(newContactBean.getCompanyName())) {
                            saveContact();
                        } else {
                            if (detailContact.getHasContacts()) {
                                MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_UPDATE_SUB_CONTACTS, getResources().getString(R.string.title_alert), getResources().getString(R.string.update_sub_contacts_company_name_also_alert), "Yes", "No");
                            } else {
                                saveContact();
                            }
                        }
                    } else {
                        boolean isCompanyNameAlreadyExisted = false;

                        if (newContactBean.getCompanyName().length() > 0) {
                            MMProgressDialog.showProgressDialog(activityContext);
                            QueryObject queryObject = new QueryObject(newContactBean.getCompanyName(), "company");
                            String responseStr = new GetQueryAsyncTask(encryptedClientKey).execute(queryObject).get();

                            if (responseStr != null) {
                                JSONObject response = new JSONObject(responseStr);

                                if (response.getInt(Constants.MM_ERROR_CODE) == Constants.MM_RESPONSE_CODE_SUCCESS && response.getBoolean(Constants.MM_IS_VALID_RESPONSE)) {
                                    Type companyNameSuggestionsListType = new TypeToken<ArrayList<CompanyData>>() {
                                    }.getType();

                                    ArrayList<CompanyData> companyNamesList = new Gson().fromJson(response.getString("CustomerQuery"), companyNameSuggestionsListType);
                                    for (CompanyData companyData : companyNamesList) {
                                        if (companyData.getName().equalsIgnoreCase(newContactBean.getCompanyName())) {
                                            isCompanyNameAlreadyExisted = true;
                                            break;
                                        }
                                    }
                                }
                            }
                            MMProgressDialog.hideProgressDialog();
                        }

                        if (isCompanyNameAlreadyExisted) {
                            MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_COMPANY_NAME_ALREADY_EXISTS, getResources().getString(R.string.title_alert), getResources().getString(R.string.primary_company_name_already_existed), "Continue", "Cancel");
                        } else {
                            saveContact();
                        }
                    }
                } else {
                    saveContact();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveContact() {
        MMProgressDialog.showProgressDialog(activityContext);
        newContactBean.setClientId(selectedClientId);
        similarContacts();
    }

    private void similarContacts() {
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        String encryptedId = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        String domain = sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("ID", -1);
            jsonObject.put("Name", newContactBean.getCompanyName());
            jsonObject.put("FirstName", newContactBean.getFirstName());
            jsonObject.put("LastName", newContactBean.getLastName());
            jsonObject.put("Email", newContactBean.getEmailId());
            contactsModel.createSimilarContact(jsonObject,encryptedId,domain);
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateContactCreateStatus(JSONObject response) {
        MMProgressDialog.hideProgressDialog();

        try {
            String message = response.getString("Message");
            newlyCreatedCustomerId = response.getString("CustomerId");
            String title = "";

            if (message.equalsIgnoreCase("Customer Created")) {
                title = "Contact Create Status";
                message = "Customer Created Successfully.";
            } else if (message.equalsIgnoreCase("Update")) {
                title = "Contact Update Status";
                message = "Customer Details Updated Successfully.";
            }

            if(alertDialog != null){
                alertDialog.dismiss();
            }

            Intent refreshDataBroadcast = new Intent(Constants.LBM_ACTION_REFRESH_VIEW);
            refreshDataBroadcast.putExtra(Constants.BUNDLE_CUSTOMER_ID, newlyCreatedCustomerId);
            refreshDataBroadcast.putExtra(Constants.BUNDLE_CURRENT_OPERATION, currentOperation);

            if (currentOperation.equals(Constants.CURRENT_OPERATION_CREATE_SUB_CONTACT)) {
                refreshDataBroadcast.putExtra(Constants.BUNDLE_PARENT_ID, detailContact.getCustomersId());
            }

            // Broadcasting Intent to Receiver Activity
            LocalBroadcastManager.getInstance(activityContext).sendBroadcast(refreshDataBroadcast);

            MMAlertDialog.listener = this;

            if (isEditMode)
                MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_CONTACT_EDITED);
            else
                MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_CONTACT_SAVED);

            MMAlertDialog.showAlertDialogWithCallback(activityContext, title, message, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void openNewlyCreatedContactDetailPage() {
        Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openContactDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_CUSTOMER_ID, newlyCreatedCustomerId);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_IS_PRIMARY_CONTACT, isPrimaryContact);
        startActivity(openContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
        finish();
    }

    public void updateSimilarContactsList(List<Customer> list) {
        if (MMProgressDialog.isProgressDialogShown)
            MMProgressDialog.hideProgressDialog();
        if(!CollectionUtils.isEmpty(list)) {
            showInputDialog(list);
        } else {
            contactsModel.createNewContact(encryptedClientKey, newContactBean, isEditMode, isAddingSubContact);
        }
    }

    private void showInputDialog(List<Customer> list) {
            ViewGroup viewGroup = findViewById(android.R.id.content);
            //then we will inflate the custom alert dialog xml that we created
            View dialogView = LayoutInflater.from(this).inflate(R.layout.duplicate_contacts_dialog, viewGroup, false);
            //Now we need an AlertDialog.Builder object
           AlertDialog.Builder builder = new AlertDialog.Builder(this);
            //setting the view of the builder to our custom view that we already inflated
            builder.setView(dialogView);
            //finally creating the alert dialog and displaying it
            alertDialog = builder.create();
            alertDialog.show();
            int s = list.size();
            recyclerView = dialogView.findViewById(R.id.contacts_recycler);
            adapter = new SimilarContactsAdopter(CreateContactActivity.this, list,loggedInUserName);
            recyclerView.setHasFixedSize(true);
            recyclerView.setItemViewCacheSize(s);
            recyclerView.setDrawingCacheEnabled(true);
            recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            Button save_button = dialogView.findViewById(R.id.create_contact_save_button);
            save_button.setOnClickListener(v -> contactsModel.createNewContact(encryptedClientKey, newContactBean, isEditMode, isAddingSubContact));
            Button cancel_button = dialogView.findViewById(R.id.create_contact_reset_button);
            cancel_button.setOnClickListener(v -> alertDialog.dismiss());
    }
}
