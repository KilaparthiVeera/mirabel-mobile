package com.mirabeltechnologies.magazinemanager.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.Toast;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.SearchActivitiesAdapter;
import com.mirabeltechnologies.magazinemanager.beans.ActivityData;
import com.mirabeltechnologies.magazinemanager.beans.SearchActivitiesFilter;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.CustomDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.SearchActivitiesListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.swipelistlibrary.SwipeMenu;
import com.mirabeltechnologies.magazinemanager.swipelistlibrary.SwipeMenuCreator;
import com.mirabeltechnologies.magazinemanager.swipelistlibrary.SwipeMenuItem;
import com.mirabeltechnologies.magazinemanager.swipelistlibrary.SwipeMenuListView;
import com.mirabeltechnologies.magazinemanager.util.SwipeActivityReturnType;
import com.mirabeltechnologies.magazinemanager.util.Utility;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static com.mirabeltechnologies.magazinemanager.constants.Constants.EVENT_DATA;
import static com.mirabeltechnologies.magazinemanager.constants.Constants.LBM_ACTION_REFRESH_VIEW;

public class SearchActivitiesScreen extends SwipeListViewBaseActivity implements SearchActivitiesListener, AlertDialogSelectionListener, ViewMoreClickListener {
    public static final String TAG = SearchActivitiesScreen.class.getSimpleName();
    SwipeActivityReturnType swipeActivityReturnType;
    private Constants.RequestFrom requestCameFrom;
    private MMTextView search_activities_screen_title;
    private String currentActivityType;
    private SearchActivitiesFilter searchActivitiesFilter = null;
    private ActivitiesModel activitiesModel;
    private SearchActivitiesAdapter searchActivitiesAdapter;
    private boolean isTabletDevice = false, viewMoreActivityNotesHasHTMLTags = false;
    private Long currentActivityId;
    private String currentAction = "";
    private int markActivityPositionInList, deleteActivityPositionInList, viewMoreActivityPositionInList;

    int positionn = 0;
    String Type = null;
    Long activityId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activities_screen);

        try {
            activityContext = SearchActivitiesScreen.this;

            // initializing super class variables
            currentActivity = this;
            requestFrom = Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE;

            isTabletDevice = Utility.isTabletDevice(activityContext);

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                currentActivityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
                requestCameFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            }

            activitiesModel = new ActivitiesModel(activityContext, this, requestFrom);

            // Initializing UI References
            search_activities_screen_title = (MMTextView) findViewById(R.id.search_activities_screen_title);
            list_view = findViewById(R.id.search_activities_list_view);
            page_number_display = (MMTextView) findViewById(R.id.search_activities_page_number_display);
            left_nav_arrow = (ImageView) findViewById(R.id.search_activities_left_nav_arrow);
            right_nav_arrow = (ImageView) findViewById(R.id.search_activities_right_nav_arrow);

            if (currentActivityType.equals(Constants.ACTIVITY_TYPE_NOTES)) {
                search_activities_screen_title.setText("Notes");
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_CALLS)) {
                search_activities_screen_title.setText("Calls");
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_MEETINGS)) {
                search_activities_screen_title.setText("Meetings");
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_ACTIVITIES)) {
                search_activities_screen_title.setText("All Activities");
            }

            if (requestCameFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE) {
                searchActivitiesFilter = (SearchActivitiesFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT);
            } else {
                searchActivitiesFilter = new SearchActivitiesFilter(currentActivityType);

                // Setting default values
                if (searchActivitiesFilter != null) {
                    searchActivitiesFilter.setAssignedToRepId(String.valueOf(loggedInRepId));
                    searchActivitiesFilter.setAssignedToRepName(loggedInRepName);
                }
            }

            searchActivitiesAdapter = new SearchActivitiesAdapter(activityContext, this, this, currentActivityType);
            list_view.setAdapter(searchActivitiesAdapter);

            refreshData();


            addActionsMethod();

            //register local broadcastReceiver
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(refreshScreenBroadcastReceiver, new IntentFilter(LBM_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(refreshScreenBroadcastReceiver);
    }


    public void addActionsMethod() {
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                switch (menu.getViewType()) {
                    case 0:
                        break;
                    case 1:
                        actionMarkAsComplete(menu);
                        actionRemimder(menu);
                        break;
                    case 3:
                        actionDelete(menu);
                        actionRemimder(menu);

                        break;
                    case 5:
                        actionMarkAsComplete(menu);
                        actionRemimder(menu);

                        break;
                    case 6:
                        actionUnmark(menu);
                        actionDelete(menu);
                        actionRemimder(menu);
                        break;
                }
            }
        };


        list_view.setMenuCreator(creator);
        list_view.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                int returnType;
                ActivityData activityData = globalContent.getActivitiesSearchResults().get(position);
                if (swipeActivityReturnType == null) {
                    swipeActivityReturnType = new SwipeActivityReturnType();
                }
                returnType = swipeActivityReturnType.getItemViewTypeForSwiping(activityData);

                //manage the cases according to the scenarios like index 0 may be "mark as complete"
                // and sometimes index 0 may be "delete activity" , which is purely based on the cases from SwipeMenuAdapter.
                //So, first validate on the basis of index, then type,
                switch (index) {
                    case 0:
                        if (returnType == 1) {
                            if (activityData.getCompleted().equals("False"))
                                markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_COMPLETE);
                            else
                                markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE);
                        } else if (returnType == 3) {
                            deleteActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_DELETE);
                        } else if (returnType == 5) {
                            if (activityData.getCompleted().equals("False"))
                                markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_COMPLETE);
                        } else if (returnType == 6) {
                            if (activityData.getCompleted().equals("False"))
                                markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_COMPLETE);
                            else
                                markActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE);
                        }
                        break;
                    case 1:

                        if(menu.getMenuItem(index).getTitle().equals("Delete")){
                            deleteActivity(position, activityData.getType(), activityData.getActivityId(), Constants.ACTIVITY_ACTION_DELETE);
                        }
                        else {
                            Toast.makeText(currentActivity, "Reminder", Toast.LENGTH_SHORT).show();
                        }
                        break;

                    case 2:
                        Toast.makeText(currentActivity, "Reminder", Toast.LENGTH_SHORT).show();
                        break;

                }
                // false : close the menu; true : not close the menu
                return false;
            }
        });

    }

    /* public void actionOpen(SwipeMenu menu) {
         SwipeMenuItem openItem = new SwipeMenuItem(
                 getApplicationContext());
         // set item background
         openItem.setBackground(new ColorDrawable(Color.BLUE));
         // set item width
         openItem.setWidth(170);
         // set item title
         openItem.setTitle("Open");
         // set item title fontsize
         openItem.setTitleSize(18);
         // set item title font color
         openItem.setTitleColor(Color.WHITE);
         // add to menu
         menu.addMenuItem(openItem);
     }
 */
    public void actionMarkAsComplete(SwipeMenu menu) {
        // mark as complete
        SwipeMenuItem mark_complete_status = new SwipeMenuItem(
                getApplicationContext());
      /*  // set item background
        mark_complete_status.setBackground(new ColorDrawable(Color.WHITE));
        // set item width
        mark_complete_status.setWidth(170);
        // set a icon
        mark_complete_status.setIcon(R.drawable.mark_completed);*/
        // set item background
        mark_complete_status.setBackground(new ColorDrawable(getResources().getColor(R.color.mark_done_event_color)));
        // set item width
        mark_complete_status.setWidth(170);
        // set item title
        mark_complete_status.setTitle("Mark Done");
        // set item title fontsize
        mark_complete_status.setTitleSize(18);
        // set item title font color
        mark_complete_status.setTitleColor(Color.WHITE);
        // add to menu
        menu.addMenuItem(mark_complete_status);
    }

    public void actionUnmark(SwipeMenu menu) {
        // mark as complete
        SwipeMenuItem mark_uncomplete_status = new SwipeMenuItem(
                getApplicationContext());
       /* // set item background
        mark_complete_status.setBackground(new ColorDrawable(Color.WHITE));
        // set item width
        mark_complete_status.setWidth(170);
        // set a icon
        mark_complete_status.setIcon(R.drawable.mark_uncompleted);*/

        // set item background
        mark_uncomplete_status.setBackground(new ColorDrawable(getResources().getColor(R.color.mark_undone_event_color)));
        // set item width
        mark_uncomplete_status.setWidth(170);
        // set item title
        mark_uncomplete_status.setTitle("Mark Undone");
        // set item title fontsize
        mark_uncomplete_status.setTitleSize(18);
        // set item title font color
        mark_uncomplete_status.setTitleColor(Color.WHITE);

        // add to menu
        menu.addMenuItem(mark_uncomplete_status);
    }

    public void actionDelete(SwipeMenu menu) {
        // create "delete" item
        SwipeMenuItem deleteItem = new SwipeMenuItem(
                getApplicationContext());
      /*  // set item background
        deleteItem.setBackground(new ColorDrawable(Color.WHITE));
        // set item width
        deleteItem.setWidth(170);
        // set a icon
        deleteItem.setIcon(R.drawable.delete_activity);*/
        // set item background
        deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.delete_event_color)));
        // set item width
        deleteItem.setWidth(170);
        // set item title
        deleteItem.setTitle("Delete");
        // set item title fontsize
        deleteItem.setTitleSize(18);
        // set item title font color
        deleteItem.setTitleColor(Color.WHITE);
        // add to menu
        menu.addMenuItem(deleteItem);
    }


    public void actionRemimder(SwipeMenu menu) {
        // create "delete" item
        SwipeMenuItem deleteItem = new SwipeMenuItem(
                getApplicationContext());
      /*  // set item background
        deleteItem.setBackground(new ColorDrawable(Color.WHITE));
        // set item width
        deleteItem.setWidth(170);
        // set a icon
        deleteItem.setIcon(R.drawable.delete_activity);*/
        // set item background
        deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.green)));
        // set item width
        deleteItem.setWidth(170);
        // set item title
        deleteItem.setTitle("Reminder");
        // set item title fontsize
        deleteItem.setTitleSize(18);
        // set item title font color
        deleteItem.setTitleColor(Color.WHITE);
        // add to menu
        menu.addMenuItem(deleteItem);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.left_in, R.anim.right_out);
        finish();
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void showSearchActivitiesFilters(View view) {

        if (requestCameFrom == Constants.RequestFrom.DASHBOARD) {
            Intent showSearchActivitiesFiltersIntent = new Intent(activityContext, SearchActivitiesFiltersScreen.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            bundle.putSerializable(Constants.BUNDLE_ACTIVITY_TYPE, currentActivityType);
            bundle.putSerializable(Constants.BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT, searchActivitiesFilter);
            showSearchActivitiesFiltersIntent.putExtras(bundle);
            startActivityForResult(showSearchActivitiesFiltersIntent, Constants.CHOOSE_FILTERS_REQUEST_CODE);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } else {
            finish();
            overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.CHOOSE_FILTERS_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Bundle bundle = data.getExtras();
                searchActivitiesFilter = (SearchActivitiesFilter) bundle.getSerializable(Constants.BUNDLE_SEARCH_ACTIVITIES_FILTER_OBJECT);
            }

            refreshData();
        }
    }

    public void resetValues() {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        // resetting activities list which are saved in global content
        globalContent.setActivitiesSearchResults(null);
        searchActivitiesAdapter.notifyDataSetChanged();
    }

    public void refreshData() {
        resetValues();
        getActivities(currentPageNumber);
    }

    public void getActivities(int pageNumber) {

        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            activitiesModel.searchActivities(encryptedClientKey, getDTTicks(), searchActivitiesFilter, currentPageNumber, isTabletDevice ? 1000 : 500);
            isLoading = true;
        }
    }

    public void updateActivities(final List<ActivityData> activitiesList, int noOfActivities, int canDeleteNotes, int canEditNotes) {
        if (currentPageNumber == 1 && (activitiesList == null || activitiesList.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            for (ActivityData activityData : activitiesList) {
                activityData.setCanDeleteNotes(canDeleteNotes);
                activityData.setCanEditNotes(canEditNotes);

                // Identifying & Removing HTML Tags or symbols from Activity Notes.
                String activityNotes = activityData.getSpecialNotes();

                Matcher matcher = emailIdPattern.matcher(activityNotes);
                List<Integer> emailStartingIndexes = new ArrayList<>();

                while (matcher.find()) {
                    emailStartingIndexes.add(matcher.start());
                    //emails.put(matcher.start(), matcher.group());
                }

                String modifiedNotes;

                if (emailStartingIndexes.isEmpty()) {
                    modifiedNotes = activityNotes;
                } else {
                    StringBuilder notesStr = new StringBuilder("");
                    for (Integer index : emailStartingIndexes) {
                        notesStr = new StringBuilder(activityNotes.substring(0, index));
                        String email = activityNotes.substring(index).replace("<", "").replace(">", "");
                        notesStr.append(email);
                    }

                    modifiedNotes = notesStr.toString();
                }

                modifiedNotes = newLineAndBreakPattern.matcher(modifiedNotes).replaceAll(" ");

                Matcher htmlTagsMatcher = htmlTagsPattern.matcher(modifiedNotes);
                if (htmlTagsMatcher.find()) {
                    modifiedNotes = htmlTagsMatcher.replaceAll("");
                    activityData.setHasHTMLTags(true);
                } else {
                    activityData.setHasHTMLTags(false);
                }

                activityData.setModifiedNotes(modifiedNotes);
            }

            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + activitiesList.size();

            if (noOfActivities != totalNoOfRecords) {
                totalNoOfRecords = noOfActivities;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getActivities(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);


                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                Log.e("veera","Page Index"+lastUpdatedVisibleItemPageNo);
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }


                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                Log.e("veera","Page Index"+lastUpdatedVisibleItemPageNo);
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setActivitiesSearchResults(activitiesList);
            searchActivitiesAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    @Override
    public void openContactDetailPage(Long customerId, Long parentId) {
        Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
        openContactDetailsIntent.putExtra(Constants.BUNDLE_CUSTOMER_ID, String.valueOf(customerId));
        openContactDetailsIntent.putExtra(Constants.BUNDLE_IS_PRIMARY_CONTACT, parentId > 0 ? false : true);
        startActivity(openContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    @Override
    public void deleteActivity(int position, String activityType, Long activityId, String action) {
        currentActivityId = activityId;
        currentAction = action;
        deleteActivityPositionInList = position;

        MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_ACTIVITY, getResources().getString(R.string.title_alert), getResources().getString(R.string.delete_activity), "Yes", "No");
    }

    @Override
    public void markActivity(int position, String activityType, Long activityId, String action) {
        currentActivityId = activityId;
        currentAction = action;
        markActivityPositionInList = position;

        if (activityType.equals("Call") && action.equals(Constants.ACTIVITY_ACTION_MARK_COMPLETE)) {
            MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_MARK_COMPLETE_ACTIVITY, getResources().getString(R.string.title_alert), getResources().getString(R.string.complete_activity), "Yes", "No");
        } else {
            modifyActivity(activityId, action);
        }
    }

    @Override
    public void openEventDetailsPage(int position, String activityType, Long activityIdd, String action) {
        ActivityData data = globalContent.getActivitiesSearchResults().get(position);
        //open the activity with EventDetailsActivity and perform the operations if needed.
        positionn = position;
        Type = data.getType();
        activityId = data.getActivityId();
        try {
            Intent openEventDetailsIntent = new Intent(SearchActivitiesScreen.this, EventDetailsActivity.class);
            openEventDetailsIntent.putExtra(EVENT_DATA, Utility.serializeObject(data));
            openEventDetailsIntent.putExtra(Constants.BUNDLE_ACTIVITY_TYPE, Constants.RequestFrom.EVENT_ACTIVITIES);
            openEventDetailsIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            openEventDetailsIntent.putExtra("Position", position);
            startActivity(openEventDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (buttonType == Constants.ButtonType.POSITIVE) {
            if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_DELETE_ACTIVITY) {
                modifyActivity(currentActivityId, currentAction);
            } else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_MARK_COMPLETE_ACTIVITY) {
                modifyActivity(currentActivityId, currentAction);
            }
        }
    }

    public void modifyActivity(Long activityId, String action) {
        MMProgressDialog.showProgressDialog(activityContext);
        activitiesModel.modifyActivity(encryptedClientKey, getDTTicks(), activityId, action);
    }

    public void updateActivitiesAfterModify(String action) {
        if (action.equals(Constants.ACTIVITY_ACTION_DELETE)) {
            // On activity delete, we will remove activities in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like activity add / delete.
            reloadActivitiesAfterDelete();
        } else {
            ActivityData activityData = globalContent.getActivitiesSearchResults().get(markActivityPositionInList);

            if (action.equals(Constants.ACTIVITY_ACTION_MARK_COMPLETE))
                activityData.setCompleted("True");
            else if (action.equals(Constants.ACTIVITY_ACTION_MARK_UN_COMPLETE))
                activityData.setCompleted("False");

            searchActivitiesAdapter.notifyDataSetChanged();
            MMProgressDialog.hideProgressDialog();
        }
    }

    public void reloadActivitiesAfterDelete() {
        int deletedActivityPageNo = (int) Math.ceil((deleteActivityPositionInList + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

        int indexOfDeletedPageNumber = alreadyLoadedPageNumbersList.indexOf(deletedActivityPageNo);

        // We are removing all page numbers (starting from this page number) from already loaded page no's list in order to reload them.
        alreadyLoadedPageNumbersList.subList(indexOfDeletedPageNumber, alreadyLoadedPageNumbersList.size()).clear();

        alreadyLoadedNoOfRecords = (deletedActivityPageNo - 1) * Constants.ACTIVITY_PAGE_SIZE_INT;

        // We are removing all activities (starting from this page) from globalContent activitiesSearchResults & we will reload them.
        globalContent.getActivitiesSearchResults().subList(alreadyLoadedNoOfRecords, globalContent.getActivitiesSearchResults().size()).clear();

        getActivities(deletedActivityPageNo);
    }

    @Override
    public void viewMoreClicked(int index) {
        try {
            ActivityData activityData = globalContent.getActivitiesSearchResults().get(index);
            String activityFullNotes = activityData.getFullNotes();
            viewMoreActivityPositionInList = index;
            viewMoreActivityNotesHasHTMLTags = activityData.isHasHTMLTags();

            // We don't have full notes here because we are mentioning limit while getting activities data that's why we are making service call to get activity full notes.
            if (activityFullNotes == null || activityFullNotes.isEmpty()) {
                MMProgressDialog.showProgressDialog(activityContext);
                activitiesModel.getActivityNotesById(encryptedClientKey, String.valueOf(activityData.getActivityId()));
            } else {
                showActivityNotes(activityFullNotes);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateActivityNotes(String fullNotes) {
        try {
            // Caching activity full notes in order to show next time when you click on it.
            globalContent.getActivitiesSearchResults().get(viewMoreActivityPositionInList).setFullNotes(fullNotes);
        } catch (Exception e) {
            e.printStackTrace();
        }

        MMProgressDialog.hideProgressDialog();

        showActivityNotes(fullNotes);
    }

    public void showActivityNotes(String notes) {
        View contentView = layoutInflater.inflate(R.layout.custom_dialog, null);
        CustomDialog customDialog = new CustomDialog(activityContext, contentView, getResources().getString(R.string.full_notes_title), notes, viewMoreActivityNotesHasHTMLTags);
        customDialog.show();
    }

    boolean refreshadaptor = false;
    String activityType = null;
    private BroadcastReceiver refreshScreenBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // perform screen refresh if any event happened in EventDetailsActivity
            if (intent.getAction().equals(LBM_ACTION_REFRESH_VIEW)) {
                refreshadaptor = true;
                activityType = intent.getExtras().getString("ActionType");
                markActivityPositionInList = intent.getExtras().getInt("Position");

            }
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        if (refreshadaptor && activityType != null) {
            updateActivitiesAfterModify(activityType);
        }
    }
}
