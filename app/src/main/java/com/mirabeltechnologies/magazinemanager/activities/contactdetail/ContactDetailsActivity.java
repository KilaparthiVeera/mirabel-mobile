package com.mirabeltechnologies.magazinemanager.activities.contactdetail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.BaseActivity;
import com.mirabeltechnologies.magazinemanager.activities.CheckInActivity;
import com.mirabeltechnologies.magazinemanager.activities.CreateContactActivity;
import com.mirabeltechnologies.magazinemanager.activities.OpportunitiesActivity;
import com.mirabeltechnologies.magazinemanager.adapters.ContactDetailPageListViewAdapter;
import com.mirabeltechnologies.magazinemanager.beans.Contact;
import com.mirabeltechnologies.magazinemanager.beans.DetailContact;
import com.mirabeltechnologies.magazinemanager.beans.EmployeeData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.AlertDialogSelectionListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ContactDetailsListener;
import com.mirabeltechnologies.magazinemanager.models.CommonModel;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;
import com.mirabeltechnologies.magazinemanager.util.Utility;

public class ContactDetailsActivity extends BaseActivity implements ContactDetailsListener, AlertDialogSelectionListener {
    public static final String TAG = ContactDetailsActivity.class.getSimpleName();

    private MMTextView contact_detail_activity_title, company_name, employee_name, rep_name, phone_number, email_id, location, phone_number2, email_id2;
    private ImageView img_refresh,phone_number_icon, email_id_icon, location_icon, contact_type_icon, edit_contact_icon, phone_number_icon2, email_id_icon2;
    private RelativeLayout cd_phone_number_layout, cd_email_id_layout, cd_location_layout, cd_email_id_layout2;
    RelativeLayout cd_phone_number_layout2;
    private ListView contact_detail_page_list_view;
    private ContactDetailPageListViewAdapter listViewAdapter;
    private Contact selectedContact;
    private String customerId;
    private Constants.RequestFrom requestFrom;
    private ContactsModel contactsModel;
    private DetailContact detailContact = null;
    private String completeAddress = "", noOfContacts = "0", noOfOrders = "0",noOfOpportunities="0";
    private boolean isPrimaryContact = false, isAdAgency = false;
    private int canViewEmployeeId = 0;
    private CommonModel commonModel;
    private boolean isNotesSettingPrivate = false;
    public  String businessType="business";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_details);

        try {
            activityContext = ContactDetailsActivity.this;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                requestFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);

                if (requestFrom == Constants.RequestFrom.DASHBOARD_QUICK_SEARCH || requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE) {
                    detailContact = (DetailContact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT);
                } else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                    // We don't have Contact details when new contact created, we have only customer id for newly created contact & we will load contact details by sending request to service with customer id.
                } else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE) {
                    // We don't have Contact details when we came from search activities screen, we have only customer id & we will load contact details by sending request to service with customer id.
                } else if (requestFrom == Constants.RequestFrom.ORDERS_PAGE) {
                    // We don't have Contact details when we came from orders search screen, we have only customer id & we will load contact details by sending request to service with customer id.
                } else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE) {
                    // We don't have Contact details when we came from callbacks screen, we have only customer id & we will load contact details by sending request to service with customer id.
                } else {
                    selectedContact = (Contact) bundle.getSerializable(Constants.BUNDLE_SELECTED_CONTACT);
                }
            }

            if (requestFrom == Constants.RequestFrom.DASHBOARD_QUICK_SEARCH) {
                if (detailContact.getParentId() > 0) {
                    isPrimaryContact = false;
                } else {
                    isPrimaryContact = true;
                }
            } else if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE) {
                isPrimaryContact = true;
            } else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                isPrimaryContact = bundle.getBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT);
            } else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE) {
                isPrimaryContact = bundle.getBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT);
            } else if (requestFrom == Constants.RequestFrom.ORDERS_PAGE) {
                isPrimaryContact = bundle.getBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT);
            } else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE) {
                isPrimaryContact = bundle.getBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT);
            } else {
                if (Long.parseLong(selectedContact.getParentId()) > 0 || requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE) {
                    isPrimaryContact = false;
                } else {
                    isPrimaryContact = true;
                }
            }

            contactsModel = new ContactsModel(activityContext, this, Constants.RequestFrom.CONTACT_DETAIL_PAGE);
            commonModel = new CommonModel(activityContext, this, Constants.RequestFrom.CONTACT_DETAIL_PAGE);

            contact_detail_activity_title = (MMTextView) findViewById(R.id.contact_detail_activity_title);
            edit_contact_icon = (ImageView) findViewById(R.id.edit_contact_icon);
            company_name = (MMTextView) findViewById(R.id.company_name);
            employee_name = (MMTextView) findViewById(R.id.employee_name);
            rep_name = (MMTextView) findViewById(R.id.rep_name);
            contact_type_icon = (ImageView) findViewById(R.id.contact_type_icon);
            cd_phone_number_layout = (RelativeLayout) findViewById(R.id.cd_phone_number_layout);
            cd_phone_number_layout2 = findViewById(R.id.cd_phone_number_layout2);
            img_refresh=findViewById(R.id.img_refresh);
            phone_number_icon = (ImageView) findViewById(R.id.phone_number_icon);
            phone_number_icon2 = findViewById(R.id.phone_number_icon2);
            phone_number = (MMTextView) findViewById(R.id.phone_number);
            phone_number2 = findViewById(R.id.phone_number2);
            cd_email_id_layout = (RelativeLayout) findViewById(R.id.cd_email_id_layout);
            cd_email_id_layout2 = findViewById(R.id.cd_email_id_layout2);
            email_id_icon = (ImageView) findViewById(R.id.email_id_icon);
            email_id_icon2 = findViewById(R.id.email_id_icon2);
            email_id = (MMTextView) findViewById(R.id.email_id);
            email_id2 = findViewById(R.id.email_id2);
            cd_location_layout = (RelativeLayout) findViewById(R.id.cd_location_layout);
            location_icon = (ImageView) findViewById(R.id.location_icon);
            location = (MMTextView) findViewById(R.id.location);
            contact_detail_page_list_view = (ListView) findViewById(R.id.contact_detail_page_list_view);

            if (isPrimaryContact) {
                contact_detail_activity_title.setText("Company Details");
            } else {
                contact_detail_activity_title.setText("Contact Details");
            }

            listViewAdapter = new ContactDetailPageListViewAdapter(activityContext, this, requestFrom, isPrimaryContact, siteType);
            contact_detail_page_list_view.setAdapter(listViewAdapter);
            /*contact_detail_page_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    if (isPrimaryContact)
                        showSelectedDetails(position);
                    else
                        showSelectedDetails(position > 0 ? (position + 1) : position); // because we are not showing Contacts for sub contact detail page.

                }
            });*/

            img_refresh.setOnClickListener(v -> {

                loadDataFromServer(true);
            });

            if (requestFrom == Constants.RequestFrom.DASHBOARD_QUICK_SEARCH) {
                customerId = detailContact.getCustomersId();
                updateCustomerBasicDetails(detailContact);
            } else {
                if (requestFrom == Constants.RequestFrom.CONTACT_DETAIL_PAGE)
                    customerId = String.valueOf(detailContact.getParentId());
                else if (requestFrom == Constants.RequestFrom.CONTACT_SUB_CONTACTS_PAGE)
                    customerId = selectedContact.getCustomersId();
                else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE)
                    customerId = bundle.getString(Constants.BUNDLE_CUSTOMER_ID);
                else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_PAGE)
                    customerId = bundle.getString(Constants.BUNDLE_CUSTOMER_ID);
                else if (requestFrom == Constants.RequestFrom.ORDERS_PAGE)
                    customerId = bundle.getString(Constants.BUNDLE_CUSTOMER_ID);
                else if (requestFrom == Constants.RequestFrom.CALLBACKS_PAGE)
                    customerId = bundle.getString(Constants.BUNDLE_CUSTOMER_ID);
                else
                    customerId = selectedContact.getCustomerId();

                loadDataFromServer(true);
            }

            // Registering Local Broadcast Manager
            LocalBroadcastManager.getInstance(activityContext).registerReceiver(mRefreshContactDetailsBroadcastReceiver, new IntentFilter(Constants.LBM_ACTION_REFRESH_VIEW));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        // Un-registering Local Broadcast Manager
        LocalBroadcastManager.getInstance(activityContext).unregisterReceiver(mRefreshContactDetailsBroadcastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void loadDataFromServer(boolean showLoadingIndicator) {
        if (showLoadingIndicator)
            MMProgressDialog.showProgressDialog(activityContext);

        contactsModel.getCustomerBasicDetails(encryptedClientKey, getDTTicks(), customerId);

    }

    public void updateCustomerBasicDetails(DetailContact contactDetails) {
        try {
            this.detailContact = contactDetails;

            if (detailContact != null) {

                canViewEmployeeId = contactDetails.getCanViewEmployeeId();
                isAdAgency = detailContact.getAdAgency();

                if (isPrimaryContact) {
                    noOfContacts = detailContact.getContactData().getNoOfContacts();
                    noOfOpportunities=detailContact.getContactData().getOpportunities();
                    // Log.e("chinna",detailContact.getContactData().getContactGroups());

                    if (!isAdAgency && !siteType.equals(Constants.SITE_TYPE_CRM))
                        noOfOrders = detailContact.getContactData().getNoOfOrders();
                    else
                        noOfOrders = "0";
                }

                if (String.valueOf(canViewEmployeeId).equals(Constants.CONTACT_READ_WRITE_ACCESS)) {
                    edit_contact_icon.setVisibility(View.VISIBLE);
                } else {
                    edit_contact_icon.setVisibility(View.GONE);
                }

                if (detailContact.getCompanyName().isEmpty()) {
                    company_name.setText(R.string.company_name);
                    company_name.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_company_name_empty_text_color));
                } else {
                    company_name.setText(detailContact.getCompanyName());
                    company_name.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_company_name_text_color));
                }

                if (detailContact.getFirstName().isEmpty() && detailContact.getLastName().isEmpty()) {
                    employee_name.setText(R.string.name);
                    employee_name.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_employee_name_empty_text_color));
                } else {
                    String nameStr = String.format("%s%s%s%s", (detailContact.getPrefix().isEmpty() ? "" : detailContact.getPrefix() + " "), (detailContact.getFirstName().isEmpty() ? "" : detailContact.getFirstName() + " "), (detailContact.getLastName().isEmpty() ? "" : detailContact.getLastName() + " "), (detailContact.getSuffix().isEmpty() ? "" : detailContact.getSuffix()));
                    employee_name.setText(nameStr);
                    employee_name.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_employee_name_text_color));
                }

                rep_name.setText(String.format("Rep: %s", detailContact.getRepName()));

                contact_type_icon.setImageResource(getContactTypeIcon(detailContact.getParentId(), detailContact.getAdAgency(), detailContact.getIsLastContractStatus()));

                if (!isPrimaryContact) {
                    contact_type_icon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openParentContactDetailPage(detailContact);
                        }
                    });
                }

                if (detailContact.getPhone().isEmpty()) {
                    phone_number_icon.setImageResource(R.drawable.phone_icon);
                    phone_number.setText(R.string.phone_number);
                    phone_number.setTextColor(ContextCompat.getColor(activityContext, R.color.light_gray));
                } else {
                    phone_number_icon.setImageResource(R.drawable.phone_icon_enabled);
                    phone_number.setText(detailContact.getPhone());
                    phone_number.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_phone_no_text_color));

                    if (detailContact.getPhoneExt().length() > 0)
                        phone_number.setText(String.format("%s Ext. %s", detailContact.getPhone(), detailContact.getPhoneExt()));

                    cd_phone_number_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            takeConfirmationToMakePhoneCall(detailContact.getPhone(),"phone");
                        }
                    });

                }

                if (detailContact.getCellPhone().isEmpty()) {
                    phone_number_icon2.setImageResource(R.drawable.phone_icon);
                    phone_number2.setHint(R.string.alternate_phone_number);
                    phone_number2.setTextColor(ContextCompat.getColor(activityContext, R.color.light_gray));

                } else {
                    phone_number_icon2.setImageResource(R.drawable.phone_icon_enabled);
                    phone_number2.setText(detailContact.getCellPhone());
                    phone_number2.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_phone_no_text_color));

                   /* if (detailContact.getPhoneExt().length() > 0)
                        phone_number.setText(String.format("%s Ext. %s", detailContact.getPhone(), detailContact.getPhoneExt()));
*/

                    cd_phone_number_layout2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            takeConfirmationToMakePhoneCall(detailContact.getCellPhone(),"alt_phone");
                        }
                    });

                }

                if (detailContact.getEmail().isEmpty()) {
                    email_id_icon.setImageResource(R.drawable.email_icon);
                    email_id.setText(R.string.email);
                    email_id.setTextColor(ContextCompat.getColor(activityContext, R.color.light_gray));
                } else {
                    email_id_icon.setImageResource(R.drawable.email_icon_enabled);
                    email_id.setText(detailContact.getEmail());
                    email_id.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_phone_no_text_color));

                    cd_email_id_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            chooseEmailClient(detailContact.getEmail());
                        }
                    });
                }


                if (detailContact.getEmail2().isEmpty()) {
                    email_id_icon2.setImageResource(R.drawable.email_icon);
                    email_id2.setText(R.string.alternate_email);
                    email_id2.setTextColor(ContextCompat.getColor(activityContext, R.color.light_gray));
                } else {
                    email_id_icon2.setImageResource(R.drawable.email_icon_enabled);
                    email_id2.setText(detailContact.getEmail2());
                    email_id2.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_phone_no_text_color));

                    cd_email_id_layout2.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            chooseEmailClient(detailContact.getEmail2());
                        }
                    });
                }


                if (completeAddress.length() > 0)
                    completeAddress = "";

                if (detailContact.getAddress1().length() > 0)
                    completeAddress += detailContact.getAddress1() + ", ";

                if (detailContact.getAddress2().length() > 0)
                    completeAddress += detailContact.getAddress2() + ", ";

                if (detailContact.getCity().length() > 0)
                    completeAddress += detailContact.getCity() + ", ";

                if (detailContact.getState().length() > 0)
                    completeAddress += detailContact.getState() + ", ";

                if (detailContact.getZip().length() > 0)
                    completeAddress += detailContact.getZip() + ", ";

                if (detailContact.getCounty().length() > 0)
                    completeAddress += detailContact.getCounty() + ", ";

                if (detailContact.getCountry().length() > 0)
                    completeAddress += detailContact.getCountry() + ", ";

                if (completeAddress.length() > 0) {
                    completeAddress = completeAddress.substring(0, completeAddress.length() - 2); // to remove last 2 characters comma and space.

                    location_icon.setImageResource(R.drawable.location_icon_enabled);
                    location.setText(completeAddress);
                    location.setTextColor(ContextCompat.getColor(activityContext, R.color.contact_detail_phone_no_text_color));

                    cd_location_layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            openMapWithLocation(completeAddress);
                        }
                    });

                } else {
                    location_icon.setImageResource(R.drawable.location_icon);
                    location.setText(R.string.location);
                    location.setTextColor(ContextCompat.getColor(activityContext, R.color.light_gray));
                }

                // We are commenting below code bcoz we need not to check Notes Setting when we are creating activities in background i.e. all system generated messages should be public similar to web.
                // we are loading employee data in order to know notes settings and we are using it while tracking call / email log in activity report.
                /*if (!detailContact.getPhone().isEmpty() || !detailContact.getCellPhone().isEmpty() || !detailContact.getEmail().isEmpty() || !detailContact.getEmail2().isEmpty()) {
                    getEmployeeDataFromServer();
                }*/
            }

            listViewAdapter.setAdAgency(isAdAgency);
            listViewAdapter.setNoOfContacts(noOfContacts);
            listViewAdapter.setNoOfOrders(noOfOrders);
            listViewAdapter.setOpportunities(noOfOpportunities);
            listViewAdapter.reloadList();

        } catch (Exception e) {
            e.printStackTrace();
        }

        MMProgressDialog.hideProgressDialog();
    }

    @Override
    public void showSelectedDetails(String sectionTitle) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BUNDLE_SELECTED_CUSTOMER_ID, customerId); //detailContact.getCustomersId()
        bundle.putSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS, detailContact);

        Intent intent = null;

        switch (sectionTitle) {
            case "General Info":
                intent = new Intent(activityContext, ContactGeneralInfoActivity.class);
                bundle.putBoolean(Constants.BUNDLE_IS_NOTES_SETTING_PRIVATE, isNotesSettingPrivate);
                break;
            case "Contacts":
                intent = new Intent(activityContext, ContactSubContactsActivity.class);
                break;
            case "Orders":
                intent = new Intent(activityContext, ContactOrdersActivity.class);
                break;
            case "Notes":
                intent = new Intent(activityContext, ContactActivitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_NOTES);
                break;
            case "Calls":
                intent = new Intent(activityContext, ContactActivitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_CALLS);
                break;
            case "Meetings":
                intent = new Intent(activityContext, ContactActivitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_MEETINGS);
                break;
            case "Opportunities":
                intent = new Intent(activityContext, OpportunitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_OPPORTUNITY);
                break;
            case "Emails":
                intent = new Intent(activityContext, ContactActivitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_EMAILS);
                break;
            case "Tasks":
                intent = new Intent(activityContext, ContactActivitiesActivity.class);
                bundle.putString(Constants.BUNDLE_ACTIVITY_TYPE, Constants.ACTIVITY_TYPE_TASKS);
                break;
            case "Addresses":
                intent = new Intent(activityContext, ContactAddressesActivity.class);
                break;
            case "Social Network":
                intent = new Intent(activityContext, ContactSocialNetworksActivity.class);
                break;
            case "Check-in":
                intent = new Intent(activityContext, CheckInActivity.class);
                break;
            default:
                break;
        }

        if (intent != null && detailContact != null) {
            intent.putExtras(bundle);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    public void showEditContactPage(View view) {
        Intent showEditContactPageIntent = new Intent(activityContext, CreateContactActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(Constants.BUNDLE_SELECTED_CONTACT_DETAILS, detailContact);
        bundle.putSerializable(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.CONTACT_DETAIL_PAGE);
        bundle.putString(Constants.BUNDLE_CURRENT_OPERATION, Constants.CURRENT_OPERATION_EDIT_CONTACT);
        bundle.putBoolean(Constants.BUNDLE_IS_PRIMARY_CONTACT, isPrimaryContact);
        showEditContactPageIntent.putExtras(bundle);
        startActivity(showEditContactPageIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    // Local Broadcast Manager Receiver Handler
    private BroadcastReceiver mRefreshContactDetailsBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                Bundle bundle = intent.getExtras();

                if (intent.getAction().equals(Constants.LBM_ACTION_REFRESH_VIEW)) {
                    String currentContactOperation = bundle.getString(Constants.BUNDLE_CURRENT_OPERATION);
                    String updatedContactCustomerId = bundle.getString(Constants.BUNDLE_CUSTOMER_ID);

                    // We are checking customer id before refreshing data in order to resolve issue with updating of all activities data which are in back stack.
                    if (updatedContactCustomerId.equals(customerId)) {
                        loadDataFromServer(false);
                    }

                    if (currentContactOperation.equals(Constants.CURRENT_OPERATION_CREATE_SUB_CONTACT)) {
                        String updatedContactParentId = bundle.getString(Constants.BUNDLE_PARENT_ID);
                        if (updatedContactParentId.equals(customerId)) {
                            detailContact.setHasContacts(true);
                        }

                        // When new sub contact added, In order to increase sub contacts count in primary contact page we are reloading primary contact.
                        if (isPrimaryContact && updatedContactParentId.equals(customerId))
                            loadDataFromServer(false);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void openParentContactDetailPage(DetailContact contact) {
        Intent openParentContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
        openParentContactDetailsIntent.putExtra(Constants.BUNDLE_SELECTED_CONTACT, contact);
        openParentContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, Constants.RequestFrom.CONTACT_DETAIL_PAGE);
        startActivity(openParentContactDetailsIntent);
        overridePendingTransition(R.anim.right_in, R.anim.left_out);
    }

    public void getEmployeeDataFromServer() {
        commonModel.getMasterData(encryptedClientKey, selectedClientId, loggedInRepId, Constants.GET_MASTER_DATA_EMPLOYEE);
    }

    public void updateEmployeeData(EmployeeData currentEmployeeData) {
        switch (currentEmployeeData.getNoteSettings()) {
            case 0: // All notes are Public by default but user can change to Private
                isNotesSettingPrivate = false;
                break;
            case 1: // All notes are Private by default but user can change to Public
                isNotesSettingPrivate = true;
                break;
            case 2: // All notes are Public by default
                isNotesSettingPrivate = false;
                break;
            case 3: // All notes are Private by default
                isNotesSettingPrivate = true;
                break;
        }
    }

    public void takeConfirmationToMakePhoneCall(String phoneNumber, String type) {
        if(type.equals("phone")){
            MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
        }
        else {
            MMAlertDialog.showAlertDialogWithCustomButtonsAndWithCallback(activityContext, this, Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_ALT_PHONE_CALL, "", phoneNumber, "Call", "Cancel");
        }
    }

    @Override
    public void alertDialogCallback() {

    }

    @Override
    public void alertDialogCallback(Constants.ButtonType buttonType, int requestCode) {
        if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL) {
            if (buttonType == Constants.ButtonType.POSITIVE) {
                if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
                    makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, detailContact.getPhone(), detailContact.getPhoneExt(), isNotesSettingPrivate);
                }
            }
        }
        else  if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_ALT_PHONE_CALL) {

            if (buttonType == Constants.ButtonType.POSITIVE) {
                if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
                    makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, detailContact.getCellPhone(), detailContact.getPhoneExt(), isNotesSettingPrivate);
                }
            }
        }
        else if (requestCode == Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION) {
            ActivityCompat.requestPermissions(this, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE);
        }
    }

    public void checkPermissionsToMakePhoneCall() {
        if (Utility.verifyPermissions(this, Constants.PERMISSION_CALL_PHONE, Constants.PERMISSIONS_CALL_PHONE, Constants.MAKE_PHONE_CALL_REQUEST_CODE)) {
            makePhoneCall(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, detailContact.getPhone(), detailContact.getPhoneExt(), isNotesSettingPrivate);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case Constants.MAKE_PHONE_CALL_REQUEST_CODE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkPermissionsToMakePhoneCall();
                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Constants.PERMISSION_CALL_PHONE)) {
                        MMAlertDialog.listener = this;
                        MMAlertDialog.setRequestCode(Constants.ALERT_DIALOG_REQUEST_CODE_TO_MAKE_PHONE_CALL_PERMISSION);
                        MMAlertDialog.showAlertDialogWithCallback(activityContext, getResources().getString(R.string.phone_call_permission_title), getResources().getString(R.string.phone_call_permission_explanation), true);
                    } else {
                        displayToast(getResources().getString(R.string.phone_call_permission_denied));
                    }
                }
                break;
        }
    }

    public void chooseEmailClient(String email) {

        openEmailComposer(this, Constants.RequestFrom.CONTACT_DETAIL_PAGE, customerId, email, isNotesSettingPrivate);

    }
}
