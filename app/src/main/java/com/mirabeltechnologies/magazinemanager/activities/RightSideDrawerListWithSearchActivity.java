package com.mirabeltechnologies.magazinemanager.activities;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.os.Bundle;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.ListViewSearchAdapter;
import com.mirabeltechnologies.magazinemanager.beans.DateRange;
import com.mirabeltechnologies.magazinemanager.beans.MasterData;
import com.mirabeltechnologies.magazinemanager.beans.OppLossReasonDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppStageDetails;
import com.mirabeltechnologies.magazinemanager.beans.OppTypeDetails;
import com.mirabeltechnologies.magazinemanager.beans.RepData;
import com.mirabeltechnologies.magazinemanager.beans.SubContactDetails;
import com.mirabeltechnologies.magazinemanager.constants.Constants;

import java.util.List;

public class RightSideDrawerListWithSearchActivity extends BaseActivity implements AdapterView.OnItemClickListener {
    Toolbar mToolbar;
    private SearchView searchView;
    ListView listView;
    private List listValues;
    private ListViewSearchAdapter listAdapter;
    private Constants.RequestFrom requestFrom;
    private Constants.SelectionType selectionType;
    //public static Object previouslySelectedValue;
    public static String rightSideDrawerSelectedIndex = "";
    private String screenTitle = "";
    RepData repData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right_side_drawer_list_with_search);

        try {
            activityContext = RightSideDrawerListWithSearchActivity.this;

            mToolbar = findViewById(R.id.right_side_drawer_with_search_toolbar);
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.icon_back_arrow_toolbar);

            Bundle bundle = getIntent().getExtras();

            if (bundle != null) {
                requestFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
                selectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
                //previouslySelectedValue = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
                rightSideDrawerSelectedIndex = bundle.getString(Constants.BUNDLE_RSD_SELECTED_INDEX);
            }

            if (selectionType == Constants.SelectionType.REP) {
                screenTitle = "Choose Rep";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.CATEGORY) {
                screenTitle = "Choose Category";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.PRIORITY) {
                screenTitle = "Choose Priority";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.CONTACT_TYPE) {
                screenTitle = "Choose Contact Type";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.ACTIVITY_TYPE) {
                screenTitle = "Choose Activity Type";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.ASSIGNED_BY) {
                screenTitle = "Choose Assigned By";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.ASSIGNED_TO) {
                screenTitle = "Choose Assigned To";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY) {
                screenTitle = "Choose User";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.DEPARTMENT) {
                screenTitle = "Choose Department";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.PROJECT) {
                screenTitle = "Choose Project";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.SALES_REP) {
                screenTitle = "Choose Sales Rep";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_CONTACT) {
                screenTitle = "Choose Contact";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.OPPORTUNITY_CONTACT);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STATUS) {
                screenTitle = "Choose Status";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.OPPORTUNITY_STATUS);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_STAGE) {
                screenTitle = "Choose Stage";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.ADMIN_STAGE);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_TYPE) {
                screenTitle = "Choose Type";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.ADMIN_TYPE);
                }
            }

            else if (selectionType == Constants.SelectionType.ADD_PROBABIlILITY) {
                screenTitle = "Choose probability";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.ADD_PROBABIlILITY);
                }
            }
            else if (selectionType == Constants.SelectionType.OPPORTUNITY_REASON) {
                screenTitle = "Choose Loss Reason";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.ADMIN_RESON);
                }
            }
            else if (selectionType == Constants.SelectionType.BUSINESS_UNIT) {
                screenTitle = "Choose Business Unit";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.PRODUCT) {
                screenTitle = "Choose Product";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.MAGAZINE) {
                screenTitle = "Choose Magazine";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.RATE_CARD) {
                screenTitle = "Choose Rate Card";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.ISSUE_YEAR) {
                screenTitle = "Choose Year";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.ISSUE) {
                screenTitle = "Choose Issue";
                listValues = SearchOrdersFiltersActivity.allIssues; // (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA); -- We are getting TransactionTooLargeException so we are accessing issues data from static variable i.e. SearchOrdersFiltersActivity.allIssues
            } else if (selectionType == Constants.SelectionType.DATE_RANGE) {
                screenTitle = "Choose Date Range";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            } else if (selectionType == Constants.SelectionType.MEETING_TYPE) {
                screenTitle = "Choose Meeting Type";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_MASTERS_DATA);
                }
            }
            else if (selectionType == Constants.SelectionType.COMPANYNAME || selectionType == Constants.SelectionType.NAME || selectionType == Constants.SelectionType.JOB ||
                    selectionType == Constants.SelectionType.ADDRESS1 || selectionType == Constants.SelectionType.ADDRESS2 || selectionType == Constants.SelectionType.CITY ||
                    selectionType == Constants.SelectionType.COUNTY || selectionType == Constants.SelectionType.STATE || selectionType == Constants.SelectionType.COUNTRY  ) {
                screenTitle = "Choose Type ";
                if (bundle != null) {
                    listValues = (List) bundle.getSerializable(Constants.BUNDLE_ALL_REPS_DATA);
                }
            }

            getSupportActionBar().setTitle(screenTitle);

            listView = findViewById(R.id.right_side_drawer_list_view_with_search);
            listAdapter = new ListViewSearchAdapter(activityContext, R.layout.right_side_drawer_list_view_item, listValues, selectionType);
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);

        try {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

            if (selectionType == Constants.SelectionType.REP) {
                searchView.setQueryHint(getResources().getString(R.string.search_rep_hint));
            }

            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String query) {
                    listAdapter.filter(query);
                    return true;
                }
            });

            // Get the search close button image view
            ImageView searchCloseButton = searchView.findViewById(R.id.search_close_btn);
            searchCloseButton.setOnClickListener(view -> {
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.onActionViewCollapsed();
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            if (searchView.isIconified()) {
                onBackPressed();
            } else {
                searchView.setQuery("", false);
                searchView.clearFocus();
                searchView.onActionViewCollapsed();
            }
            return true;
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        try {
            Object selectedValue = parent.getItemAtPosition(position);
            // Toast.makeText(applicationContext, selectedValue.toString(), Toast.LENGTH_SHORT).show();

            //previouslySelectedValue = selectedValue;
            //listAdapter.notifyDataSetChanged();
            hideKeyboard(searchView);
            if (selectionType == Constants.SelectionType.REP || selectionType == Constants.SelectionType.ASSIGNED_BY || selectionType == Constants.SelectionType.ASSIGNED_TO || selectionType == Constants.SelectionType.CREATED_BY_OR_ASSIGNED_BY || selectionType == Constants.SelectionType.SALES_REP) {
                if (!((RepData) selectedValue).getName().equals("------Disabled Reps------"))
                    sendBroadcastWithSelectedIndex(selectedValue);
            } else if (selectionType == Constants.SelectionType.COMPANYNAME || selectionType == Constants.SelectionType.NAME || selectionType == Constants.SelectionType.JOB ||
                    selectionType == Constants.SelectionType.ADDRESS1 || selectionType == Constants.SelectionType.ADDRESS2 || selectionType == Constants.SelectionType.CITY ||
                    selectionType == Constants.SelectionType.COUNTY || selectionType == Constants.SelectionType.STATE || selectionType == Constants.SelectionType.COUNTRY  ) {
                sendBroadcastWithSelectedIndex(selectedValue);
            }
            else {
                sendBroadcastWithSelectedIndex(selectedValue);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendBroadcastWithSelectedIndex(Object object) {
        try {
            Intent intent;
            Bundle bundle = new Bundle();

            if (requestFrom == Constants.RequestFrom.MY_CONTACTS_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
            }
            else  if (requestFrom == Constants.RequestFrom.OPPORTUNITY) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_MY_CONTACTS_PAGE);
                if (selectionType == Constants.SelectionType.REP){
                    repData=(RepData) object;
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
                }

            }
            else if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE);

                if (selectionType == Constants.SelectionType.REP)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
                else if (selectionType == Constants.SelectionType.COMPANYNAME || selectionType == Constants.SelectionType.NAME || selectionType == Constants.SelectionType.JOB ||
                        selectionType == Constants.SelectionType.ADDRESS1 || selectionType == Constants.SelectionType.ADDRESS2 || selectionType == Constants.SelectionType.CITY ||
                        selectionType == Constants.SelectionType.COUNTY || selectionType == Constants.SelectionType.STATE || selectionType == Constants.SelectionType.COUNTRY  ) {
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (String) object);
                }
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
            } else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_CREATE_ACTIVITY_PAGE);

                if (selectionType == Constants.SelectionType.ACTIVITY_TYPE)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
            }

            else if (requestFrom == Constants.RequestFrom.SEARCH_ACTIVITIES_FILTERS_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ACTIVITIES_FILTERS_PAGE);

                if (selectionType == Constants.SelectionType.ACTIVITY_TYPE || selectionType == Constants.SelectionType.DEPARTMENT)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
            } else if (requestFrom == Constants.RequestFrom.SEARCH_TASKS_FILTERS_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_SEARCH_TASKS_FILTERS_PAGE);

                if (selectionType == Constants.SelectionType.PRIORITY || selectionType == Constants.SelectionType.PROJECT)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
            } else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_CREATE_NEW_TASK_PAGE);

                if (selectionType == Constants.SelectionType.PRIORITY || selectionType == Constants.SelectionType.PROJECT)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
            }

            else if (requestFrom == Constants.RequestFrom.ADD_OPPORTUNITY) {

                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE);

                if (selectionType == Constants.SelectionType.SALES_REP)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
                else if (selectionType == Constants.SelectionType.OPPORTUNITY_STATUS)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (String) object);
                else if (selectionType == Constants.SelectionType.ADD_PROBABIlILITY)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (String) object);

                else if (selectionType == Constants.SelectionType.OPPORTUNITY_STAGE)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (OppStageDetails) object);
                else if (selectionType == Constants.SelectionType.OPPORTUNITY_TYPE)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (OppTypeDetails) object);
                else if (selectionType == Constants.SelectionType.OPPORTUNITY_REASON)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (OppLossReasonDetails) object);
                else if (selectionType == Constants.SelectionType.OPPORTUNITY_CONTACT)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (SubContactDetails) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
            }

            else  if (requestFrom == Constants.RequestFrom.SEARCH_OPPORTUNITY) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE);
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
            }

            else if (requestFrom == Constants.RequestFrom.SEARCH_ORDERS_FILTERS_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_SEARCH_ORDERS_FILTERS_PAGE);

                if (selectionType == Constants.SelectionType.SALES_REP)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (RepData) object);
                else if (selectionType == Constants.SelectionType.DATE_RANGE)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (DateRange) object);
                else
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
            }

            else if (requestFrom == Constants.RequestFrom.ADD_CALENDAR_EVENT) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_CREATE_CALENDAR_EVENT_PAGE);

                if (selectionType == Constants.SelectionType.MEETING_TYPE)
                    bundle.putSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE, (MasterData) object);
            }

            else
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION);

            bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
            intent.putExtras(bundle);
            LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
            closeActivity(null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
