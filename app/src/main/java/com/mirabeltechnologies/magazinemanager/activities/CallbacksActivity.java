package com.mirabeltechnologies.magazinemanager.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;

import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.activities.contactdetail.ContactDetailsActivity;
import com.mirabeltechnologies.magazinemanager.adapters.CallbacksAdapter;
import com.mirabeltechnologies.magazinemanager.beans.CallbackData;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.CustomDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMAlertDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMProgressDialog;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.interfaces.CallbacksListener;
import com.mirabeltechnologies.magazinemanager.interfaces.ViewMoreClickListener;
import com.mirabeltechnologies.magazinemanager.models.ActivitiesModel;
import com.mirabeltechnologies.magazinemanager.models.ContactsModel;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

public class CallbacksActivity extends ListViewBaseActivity implements CallbacksListener, ViewMoreClickListener {
    public static final String TAG = CallbacksActivity.class.getSimpleName();

    private String currentActivityType;
    private MMTextView callbacks_screen_title;
    private ContactsModel contactsModel;
    private ActivitiesModel activitiesModel;
    private CallbacksAdapter callbacksAdapter;
    private int markActivityPositionInList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_callbacks);

        try {
            activityContext = CallbacksActivity.this;

            // initializing super class variables
            currentActivity = this;
            requestFrom = Constants.RequestFrom.CALLBACKS_PAGE;

            Bundle bundle = getIntent().getExtras();
            if (bundle != null) {
                currentActivityType = bundle.getString(Constants.BUNDLE_ACTIVITY_TYPE);
            }

            contactsModel = new ContactsModel(activityContext, this, requestFrom);
            activitiesModel = new ActivitiesModel(activityContext, this, requestFrom);

            // Initializing UI References
            callbacks_screen_title = (MMTextView) findViewById(R.id.callbacks_screen_title);
            list_view = (ListView) findViewById(R.id.callbacks_list_view);
            page_number_display = (MMTextView) findViewById(R.id.callbacks_page_number_display);
            left_nav_arrow = (ImageView) findViewById(R.id.callbacks_left_nav_arrow);
            right_nav_arrow = (ImageView) findViewById(R.id.callbacks_right_nav_arrow);

            if (currentActivityType.equals(Constants.ACTIVITY_TYPE_THIS_WEEK_CALLBACKS)) {
                callbacks_screen_title.setText("This Week's Callbacks");
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_TODAY_CALLBACKS)) {
                callbacks_screen_title.setText("Today's Callbacks");
            } else if (currentActivityType.equals(Constants.ACTIVITY_TYPE_ALL_CALLBACKS)) {
                callbacks_screen_title.setText("All Callbacks");
            }

            callbacksAdapter = new CallbacksAdapter(activityContext, this, this);
            list_view.setAdapter(callbacksAdapter);

            refreshData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (MMAlertDialog.isAlertDialogShown && MMAlertDialog.alertDialog != null) {
            MMAlertDialog.alertDialog.dismiss();
        }

        if (MMProgressDialog.isProgressDialogShown) {
            MMProgressDialog.hideProgressDialog();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        AppController.getInstance(applicationContext).cancelPendingRequests(TAG);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    public void closeActivity(View view) {
        onBackPressed();
    }

    public void resetValues() {
        super.resetValues();

        list_view.setOnScrollListener(null);
        isScrollListenerAssignedToListView = false;

        // resetting activities list which are saved in global content
        globalContent.setCallbacks(null);
        callbacksAdapter.notifyDataSetChanged();
    }

    public void refreshData() {
        resetValues();
        getCallbacks(currentPageNumber);
    }

    public void getCallbacks(int pageNumber) {
        currentPageNumber = pageNumber;

        if (isInitialRequest || currentPageNumber <= totalNoOfPages) {
            if (!MMProgressDialog.isProgressDialogShown)
                MMProgressDialog.showProgressDialog(activityContext);

            contactsModel.getCustomerCallbacks(encryptedClientKey, getDTTicks(), currentActivityType, currentPageNumber);
            isLoading = true;
        }
    }

    public void updateCustomerCallbacks(final List<CallbackData> callbacksList, int noOfCallbacks) {
        if (currentPageNumber == 1 && (callbacksList == null || callbacksList.isEmpty())) {
            resetValues();
            noRecordsFound();
        } else {
            for (CallbackData activityData : callbacksList) {
                // Identifying & Removing HTML Tags or symbols from Activity Notes.
                String activityNotes = activityData.getNotes();

                Matcher matcher = emailIdPattern.matcher(activityNotes);
                List<Integer> emailStartingIndexes = new ArrayList<>();

                while (matcher.find()) {
                    emailStartingIndexes.add(matcher.start());
                    //emails.put(matcher.start(), matcher.group());
                }

                String modifiedNotes;

                if (emailStartingIndexes.isEmpty()) {
                    modifiedNotes = activityNotes;
                } else {
                    StringBuilder notesStr = new StringBuilder("");
                    for (Integer index : emailStartingIndexes) {
                        notesStr = new StringBuilder(activityNotes.substring(0, index));
                        String email = activityNotes.substring(index).replace("<", "").replace(">", "");
                        notesStr.append(email);
                    }

                    modifiedNotes = notesStr.toString();
                }

                modifiedNotes = newLineAndBreakPattern.matcher(modifiedNotes).replaceAll(" ");

                Matcher htmlTagsMatcher = htmlTagsPattern.matcher(modifiedNotes);
                if (htmlTagsMatcher.find()) {
                    modifiedNotes = htmlTagsMatcher.replaceAll("");
                    activityData.setHasHTMLTags(true);
                } else {
                    activityData.setHasHTMLTags(false);
                }

                activityData.setModifiedNotes(modifiedNotes);
            }

            if (!alreadyLoadedPageNumbersList.contains(currentPageNumber))
                alreadyLoadedPageNumbersList.add(currentPageNumber);

            alreadyLoadedNoOfRecords = alreadyLoadedNoOfRecords + callbacksList.size();

            if (noOfCallbacks != totalNoOfRecords) {
                totalNoOfRecords = noOfCallbacks;
                totalNoOfPages = (int) Math.ceil(totalNoOfRecords / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

                if (currentPageNumber == totalNoOfPages)
                    isAllRecordsLoaded = true;
                else
                    isAllRecordsLoaded = false;

                updateFooterPageLabels(lastUpdatedFooterPageNumber);
            }

            if (!isScrollListenerAssignedToListView && (totalNoOfRecords > (int) Constants.ACTIVITY_PAGE_SIZE_FLOAT)) {
                //Assigning custom scroll listener to expandable list view
                list_view.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView absListView, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        int visibleThreshold = 5;

                        if (!isAllRecordsLoaded && !isLoading && (firstVisibleItem + visibleItemCount + visibleThreshold >= totalItemCount)) {
                            currentPageNumber++;
                            getCallbacks(currentPageNumber);
                        }

                        if (!isLoading && firstVisibleItem != lastVisibleItemIndex) {
                            lastVisibleItemIndex = firstVisibleItem;

                            int currentVisiblePageNo = (int) Math.ceil((firstVisibleItem + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

                            if (currentVisiblePageNo != 0) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }

                            // to update last page number when we have less no. of records in last page
                            if (firstVisibleItem + visibleItemCount == totalItemCount) {
                                lastUpdatedVisibleItemPageNo = currentVisiblePageNo + 1;
                                updatePageNumber(lastUpdatedVisibleItemPageNo);
                            }
                        }
                    }

                    public void updatePageNumber(int currentVisiblePageNo) {
                        if (!isNavigationButtonClicked && currentVisiblePageNo != lastUpdatedFooterPageNumber && currentVisiblePageNo <= totalNoOfPages) {
                            updateFooterPageLabels(currentVisiblePageNo);
                        }
                    }
                });

                isScrollListenerAssignedToListView = true;
            }

            globalContent.setCallbacks(callbacksList);
            callbacksAdapter.notifyDataSetChanged();

            if (currentPageNumber == 1)
                updateFooterPageLabels(currentPageNumber);

            if (currentPageNumber == totalNoOfPages)
                isAllRecordsLoaded = true;
            else
                isAllRecordsLoaded = false;

            if (isNavigationButtonClicked) {
                navigateToListViewPosition(navigationPageNumber);
            }

            isLoading = false;
            MMProgressDialog.hideProgressDialog();
        }

        isInitialRequest = false;
    }

    @Override
    public void viewMoreClicked(int index) {
        try {
            CallbackData callbackData = globalContent.getCallbacks().get(index);

            View contentView = layoutInflater.inflate(R.layout.custom_dialog, null);
            CustomDialog customDialog = new CustomDialog(activityContext, contentView, getResources().getString(R.string.full_notes_title), callbackData.getNotes(), callbackData.isHasHTMLTags());
            customDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void openContactDetailPage(Long customerId, Long parentId, String canViewEmployeeId) {
        if (canViewEmployeeId.equals(Constants.CONTACT_NO_ACCESS)) {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.contact_no_access));
        } else {
            Intent openContactDetailsIntent = new Intent(activityContext, ContactDetailsActivity.class);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_REQUEST_FROM, requestFrom);
            openContactDetailsIntent.putExtra(Constants.BUNDLE_CUSTOMER_ID, String.valueOf(customerId));
            openContactDetailsIntent.putExtra(Constants.BUNDLE_IS_PRIMARY_CONTACT, parentId > 0 ? false : true);
            startActivity(openContactDetailsIntent);
            overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }
    }

    @Override
    public void markActivity(int position, Long activityId, String canViewEmployeeId) {
        if (canViewEmployeeId.equals(Constants.CONTACT_READ_WRITE_ACCESS)) {
            markActivityPositionInList = position;

            MMProgressDialog.showProgressDialog(activityContext);
            activitiesModel.modifyActivity(encryptedClientKey, getDTTicks(), activityId, Constants.ACTIVITY_ACTION_MARK_COMPLETE);
        } else {
            MMAlertDialog.showAlertDialog(activityContext, getResources().getString(R.string.title_warning), getResources().getString(R.string.no_access_to_mark_activity));
        }
    }

    public void updateCallbacksAfterModify(String action) {
        MMProgressDialog.hideProgressDialog();

        // When we mark any callback as completed then we will remove callbacks in current + all next pages & we will reload them in order to resolve conflicts due to changes may happen at service side like callback add / delete.
        reloadCallbacksAfterModify();
    }

    public void reloadCallbacksAfterModify() {
        int modifiedCallbackPageNo = (int) Math.ceil((markActivityPositionInList + 1) / Constants.ACTIVITY_PAGE_SIZE_FLOAT);

        int indexOfModifiedCallbackPageNumber = alreadyLoadedPageNumbersList.indexOf(modifiedCallbackPageNo);

        // We are removing all page numbers (starting from this page number) from already loaded page no's list in order to reload them.
        alreadyLoadedPageNumbersList.subList(indexOfModifiedCallbackPageNumber, alreadyLoadedPageNumbersList.size()).clear();

        alreadyLoadedNoOfRecords = (modifiedCallbackPageNo - 1) * Constants.ACTIVITY_PAGE_SIZE_INT;

        // We are removing all callbacks (starting from this page) from globalContent getCallbacks & we will reload them.
        globalContent.getCallbacks().subList(alreadyLoadedNoOfRecords, globalContent.getCallbacks().size()).clear();

        getCallbacks(modifiedCallbackPageNo);
    }

    @Override
    public void chooseEmailClient(String customerId, String emailId) {
        openEmailComposer(this, requestFrom, customerId, emailId, false);
    }
}
