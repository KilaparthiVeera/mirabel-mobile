package com.mirabeltechnologies.magazinemanager.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.services.FetchAddressIntentService;
import com.mirabeltechnologies.magazinemanager.util.AppUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AddressActivity extends BaseActivity implements OnMapReadyCallback,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MAP LOCATION";
    Context mContext;
    TextView mLocationMarkerText;
    public LatLng mCenterLatLong;
    Geocoder geocoder;
    List<Address> addresses;
    String add;
    private AddressResultReceiver mResultReceiver;
    protected String mAddressOutput;
    protected String mAreaOutput;
    protected String mCityOutput;
    protected String mStateOutput;
    private static final int REQUEST_CODE_AUTOCOMPLETE = 1;
    private Constants.RequestFrom requestFrom;
    private Constants.SelectionType selectionType;
    TextView add_button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.address_activity);

        mContext = this;

        Bundle bundle = getIntent().getExtras();

        if (bundle != null) {
            requestFrom = (Constants.RequestFrom) bundle.getSerializable(Constants.BUNDLE_REQUEST_FROM);
            selectionType = (Constants.SelectionType) bundle.getSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE);
            //previouslySelectedValue = bundle.getSerializable(Constants.BUNDLE_RSD_SELECTED_VALUE);
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }

        mLocationMarkerText = (TextView) findViewById(R.id.locationMarkertext);
        add_button=findViewById(R.id.add_button);

        mResultReceiver = new AddressResultReceiver(new Handler());

        if (checkPlayServices()) {

            if (!AppUtils.isLocationEnabled(mContext)) {
                // notify user
                AlertDialog.Builder dialog = new AlertDialog.Builder(mContext);
                dialog.setMessage("Location not enabled!");
                dialog.setPositiveButton("Open location settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                });
                dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        // TODO Auto-generated method stub

                    }
                });
                dialog.show();
            }
            buildGoogleApiClient();
        } else {
            Toast.makeText(mContext, "Location not supported in this device", Toast.LENGTH_SHORT).show();
        }

        add_button.setOnClickListener(v -> {
            sendBroadcastWithSelectedIndex(addresses);

        });
    }
    public void closeActivity(View view) {
        onBackPressed();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    private void sendBroadcastWithSelectedIndex(List<Address> addresses) {

        try {
            Intent intent;
            Bundle bundle = new Bundle();
            if (requestFrom == Constants.RequestFrom.CREATE_CONTACT_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RIGHT_SIDE_DRAWER_SELECTION_FROM_CREATE_CONTACT_PAGE);
                if (selectionType == Constants.SelectionType.MAP_ADDRESS)
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                else
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
                intent.putExtras(bundle);

                // Broadcasting Selection Index to Receiver Activity
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
                closeActivity(null);

            }
            else if (requestFrom == Constants.RequestFrom.CREATE_ACTIVITY_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_CREATE_ACTIVITY_PAGE);
                if (selectionType == Constants.SelectionType.MAP_ADDRESS)
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                else
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
                intent.putExtras(bundle);
                // Broadcasting Selection Index to Receiver Activity
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
                closeActivity(null);

            }
            else if (requestFrom == Constants.RequestFrom.CREATE_NEW_TASK_PAGE) {
                intent = new Intent(Constants.LBM_ACTION_RSDS_FROM_CREATE_NEW_TASK_PAGE);
                if (selectionType == Constants.SelectionType.MAP_ADDRESS)
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                else
                    bundle.putParcelableArrayList(Constants.BUNDLE_RSD_SELECTED_VALUE, (ArrayList<? extends Parcelable>) addresses);
                bundle.putSerializable(Constants.BUNDLE_RSD_SELECTION_TYPE, selectionType);
                intent.putExtras(bundle);
                // Broadcasting Selection Index to Receiver Activity
                LocalBroadcastManager.getInstance(applicationContext).sendBroadcast(intent);
                closeActivity(null);

            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "OnMapReady");
        mMap = googleMap;

        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                Log.d("Camera postion change" + "", cameraPosition + "");
                mCenterLatLong = cameraPosition.target;

                mMap.clear();

                try {

                    Location mLocation = new Location("");
                    mLocation.setLatitude(mCenterLatLong.latitude);
                    mLocation.setLongitude(mCenterLatLong.longitude);
                    startIntentService(mLocation);
                    geocoder = new Geocoder(AddressActivity.this, Locale.getDefault());

                    try {
                        addresses = geocoder.getFromLocation(mCenterLatLong.latitude,mCenterLatLong.longitude, 1);
                        add = addresses.get(0).getAddressLine(0);

                        // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                  /*  if (mMarker != null) {
                        mMarker.remove();

                        mMarker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mCenterLatLong.latitude,mCenterLatLong.longitude))
                                .title(add)
                                .anchor(0.5f, 0.5f)
                                .flat(true)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                                .draggable(true)
                                .visible(true));

                    } else {
                        mMarker = mMap.addMarker(new MarkerOptions()
                                .position(new LatLng(mCenterLatLong.latitude,mCenterLatLong.longitude))
                                .title(add)
                                .anchor(0.5f, 0.5f)
                                .flat(true)
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                                .draggable(true)
                                .visible(true));
                    }
*/

                    mLocationMarkerText.setText(add);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }


        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            changeMap(mLastLocation);
            Log.d(TAG, "ON connected");

        } else
            try {
                LocationServices.FusedLocationApi.removeLocationUpdates(
                        mGoogleApiClient, this);

            } catch (Exception e) {
                e.printStackTrace();
            }
        try {
            LocationRequest mLocationRequest = new LocationRequest();
            mLocationRequest.setInterval(10000);
            mLocationRequest.setFastestInterval(5000);
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            LocationServices.FusedLocationApi.requestLocationUpdates(
                    mGoogleApiClient, mLocationRequest, this);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            if (location != null)
                changeMap(location);
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            mGoogleApiClient.connect();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }

    }


    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            }
            return false;
        }
        return true;
    }

    private void changeMap(Location location) {

        Log.d(TAG, "Reaching map" + mMap);


        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // check if map is created successfully or not
        if (mMap != null) {
            mMap.getUiSettings().setZoomControlsEnabled(true);
            LatLng latLong;


            latLong = new LatLng(location.getLatitude(), location.getLongitude());

            geocoder = new Geocoder(AddressActivity.this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                add = addresses.get(0).getAddressLine(0);

                // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            } catch (IOException e) {
                e.printStackTrace();
            }


            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLong).zoom(15).build();

            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
            mMap.animateCamera(CameraUpdateFactory
                    .newCameraPosition(cameraPosition));

           /* if (mMarker != null) {
                mMarker.remove();

                mMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLong)
                        .title(add)
                        .anchor(0.5f, 0.5f)
                        .flat(true)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        .draggable(true)
                        .visible(true));

            } else {
                mMarker = mMap.addMarker(new MarkerOptions()
                        .position(latLong)
                        .title(add)
                        .anchor(0.5f, 0.5f)
                        .flat(true)
                        .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                        .draggable(true)
                        .visible(true));
            }*/

            mLocationMarkerText.setText(add);
            startIntentService(location);


        } else {
            Toast.makeText(getApplicationContext(),
                    "Sorry! unable to create maps", Toast.LENGTH_SHORT)
                    .show();
        }

    }


    class AddressResultReceiver extends ResultReceiver {
        public AddressResultReceiver(Handler handler) {
            super(handler);
        }

        /**
         * Receives data sent from
         * FetchAddressIntentService and updates the UI in MainActivity.
         */
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            // Display the address string or an error message sent from the intent service.
            mAddressOutput = resultData.getString(AppUtils.LocationConstants.RESULT_DATA_KEY);

            mAreaOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_AREA);

            mCityOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_CITY);
            mStateOutput = resultData.getString(AppUtils.LocationConstants.LOCATION_DATA_STREET);
            // Show a toast message if an address was found.
            if (resultCode == AppUtils.LocationConstants.SUCCESS_RESULT) {
                //  showToast(getString(R.string.address_found));
            }

        }

    }

    /**
     * Creates an intent, adds location data to it as an extra, and starts the intent service for
     * fetching an address.
     */
    protected void startIntentService(Location mLocation) {
        // Create an intent for passing to the intent service responsible for fetching the address.
        Intent intent = new Intent(this, FetchAddressIntentService.class);

        // Pass the result receiver as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.RECEIVER, mResultReceiver);

        // Pass the location data as an extra to the service.
        intent.putExtra(AppUtils.LocationConstants.LOCATION_DATA_EXTRA, mLocation);

        // Start the service. If the service isn't already running, it is instantiated and started
        // (creating a process for it if needed); if it is running then it remains running. The
        // service kills itself automatically once all intents are processed.
        startService(intent);
    }

}
