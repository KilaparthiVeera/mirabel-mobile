package com.mirabeltechnologies.magazinemanager.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.util.Pair;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.util.CollectionUtils;
import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.gson.Gson;
import com.mirabeltechnologies.magazinemanager.Listeners.ClickListener;
import com.mirabeltechnologies.magazinemanager.Listeners.RecyclerTouchListener;
import com.mirabeltechnologies.magazinemanager.R;
import com.mirabeltechnologies.magazinemanager.adapters.CheckInHistoryAdopter;
import com.mirabeltechnologies.magazinemanager.beans.CheckInList;
import com.mirabeltechnologies.magazinemanager.beans.CheckInSubmitResponse;
import com.mirabeltechnologies.magazinemanager.constants.Constants;
import com.mirabeltechnologies.magazinemanager.controller.AppController;
import com.mirabeltechnologies.magazinemanager.customviews.MMTextView;
import com.mirabeltechnologies.magazinemanager.util.EncryptDecryptStringWithDES;
import com.mirabeltechnologies.magazinemanager.util.NetworkConnectionDetector;

import org.json.JSONObject;
import org.jsoup.helper.StringUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class CheckInHistoryActivity extends BaseActivity {
    Context mContext;
    MMTextView selectedDate,btn_go;
    String selectedDateRange;
    private ProgressDialog progressDialog;
    RecyclerView rv_check_in;
    CheckInHistoryAdopter checkInHistoryAdopter;
    List<CheckInList> list;
    String server_start,server_end,selectedCustomerId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkin_histoty);
        mContext = this;
        getSupportActionBar().hide();
        if(getIntent()!=null){
            selectedCustomerId= getIntent().getStringExtra("customerId");
        }
        selectedDate = findViewById(R.id.selectedDate);
        btn_go = findViewById(R.id.btn_go);
        selectedDate.setOnClickListener(view -> DatePickerDialog());
        btn_go.setOnClickListener(v -> {
            if(!TextUtils.isEmpty(selectedDateRange)){
                loadData(selectedDateRange);
            } else {
                Toast.makeText(mContext, "Please select a date range..", Toast.LENGTH_SHORT).show();
            }
        });
        list= new ArrayList<>();
        rv_check_in = findViewById(R.id.rv_check_in);
        rv_check_in.setHasFixedSize(true);
        rv_check_in.setAdapter(checkInHistoryAdopter);
        rv_check_in.setLayoutManager(new LinearLayoutManager(mContext));
        rv_check_in.addOnItemTouchListener(new RecyclerTouchListener(mContext, rv_check_in, new ClickListener() {
            public void onClick(View view, int position) {
                if(!CollectionUtils.isEmpty(list)){
                    if(!StringUtil.isBlank(list.get(position).getLocation())){
                        openMapWithLocation(list.get(position).getLocation());
                    } else {
                        Toast.makeText(mContext, "Location not Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
            public void onLongClick(View view, int position) {
            }
        }));

        loadData(selectedDateRange);
    }

    private void loadData(String selectedDateRange) {
        progressDialog = ProgressDialog.show(mContext, "Please wait.", "Fetch the Records  ..!", true);
        EncryptDecryptStringWithDES des = new EncryptDecryptStringWithDES(false);
        String authToken = des.getEncryptedStringForSelectedClientSimilarToWeb(sharedPreferences.getInt(Constants.SP_SELECTED_CLIENT_ID), sharedPreferences.getString(Constants.SP_EMAIL_ID));
        String domain= sharedPreferences.getString(Constants.SP_SELECTED_SUB_DOMAIN);
        try {
            if (new NetworkConnectionDetector(activityContext).isNetworkConnected()) {
                Log.e("veera",Constants.CHECK_IN_HISTORY);
                JSONObject reqObject = new JSONObject();
                reqObject.put("CustomerID",Integer.parseInt(selectedCustomerId));
                if(!TextUtils.isEmpty(selectedDateRange)){
                   reqObject.put("FromDate",server_start);
                    reqObject.put("ToDate",server_end);
                }
                Log.e("veera",reqObject.toString());
                JsonObjectRequest createNewActivityReq = new JsonObjectRequest(Request.Method.POST, Constants.CHECK_IN_HISTORY,reqObject,
                        response -> {
                            Log.e("veera",response.toString());
                            progressDialog.dismiss();
                            Gson gson = new Gson();
                            CheckInSubmitResponse submitResponse = gson.fromJson(response.toString(), CheckInSubmitResponse.class);
                            if(submitResponse!=null && submitResponse.getContent()!=null){
                                if(!CollectionUtils.isEmpty(submitResponse.getContent().getList())) {
                                    rv_check_in.setVisibility(View.VISIBLE);
                                    list = submitResponse.getContent().getList();
                                    int s = list.size();
                                    checkInHistoryAdopter = new CheckInHistoryAdopter(CheckInHistoryActivity.this, list, mContext,loggedInUserName);
                                    rv_check_in.setHasFixedSize(true);
                                    rv_check_in.setItemViewCacheSize(s);
                                    rv_check_in.setDrawingCacheEnabled(true);
                                    rv_check_in.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
                                    rv_check_in.setAdapter(checkInHistoryAdopter);
                                } else {
                                    rv_check_in.setAdapter(null);
                                    rv_check_in.setVisibility(View.GONE);
                                    Toast.makeText(mContext, "No Data Found", Toast.LENGTH_SHORT).show();
                                }
                                list=submitResponse.getContent().getList();
                            } else {
                                Toast.makeText(mContext, getResources().getString(R.string.error_failure), Toast.LENGTH_SHORT).show();
                            }
                        },
                        error -> {
                            Toast.makeText(mContext, error.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            Log.e("veera",error.toString());
                            onVolleyErrorResponse(error);
                        }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        HashMap<String, String> headers = new HashMap<>();
                        headers.put("Token", authToken);
                        headers.put("Domain", domain);
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        Log.e("veera",headers.toString());
                        return headers;
                    }
                };
                AppController.getInstance(activityContext).addToRequestQueue(createNewActivityReq, "veera");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void DatePickerDialog() {
        // Creating a MaterialDatePicker builder for selecting a date range
        MaterialDatePicker.Builder<Pair<Long, Long>> builder = MaterialDatePicker.Builder.dateRangePicker();
        builder.setTitleText("Select a date range");
        // Building the date picker dialog
        MaterialDatePicker<Pair<Long, Long>> datePicker = builder.build();
        datePicker.addOnPositiveButtonClickListener(selection -> {
            // Retrieving the selected start and end dates
            Long startDate = selection.first;
            Long endDate = selection.second;
            // Formating the selected dates as strings

            SimpleDateFormat server_sdf = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
             server_start = server_sdf.format(new Date(startDate));
             server_end = server_sdf.format(new Date(endDate));

            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            String startDateString = sdf.format(new Date(startDate));
            String endDateString = sdf.format(new Date(endDate));
            // Creating the date range string
            selectedDateRange = startDateString + " - " + endDateString;
            // Displaying the selected date range in the TextView
            selectedDate.setText(selectedDateRange);
        });
        // Showing the date picker dialog
        datePicker.show(getSupportFragmentManager(), "DATE_PICKER");
    }


    public void closeActivity(View view) {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

}
